DEV-PORT = 9000

default:
	docker-compose down
	lein uberjar
	docker-compose build
	docker-compose up

lint:
	docker run -v $(PWD)/src:/src --rm borkdude/clj-kondo clj-kondo --lint src

report:
	tree -i -f -P '*.edn' --prune -h --du
	ccat env/dev/resources/config.edn
	ccat env/prod/resources/config.edn
	ccat env/test/resources/config.edn

rebuild:
	docker-compose down
	docker-compose build
	docker-compose up

build-front-prod-separately:
	lein shadow release app
	ls -lh target/cljsbuild/public/js

frontend-dev:
	lein shadow watch wapp

backend-dev:
	LEIN_USE_BOOTCLASSPATH=no;\
	DATABASE_URL='postgresql://localhost/ask?user=postgres&password=postgres';\
	lein with-profile dev repl

database-up:
	docker-compose up db

.PHONY : dependency-graph
dependency-graph:
    # NOTE: dependency graph, see: https://github.com/hilverd/lein-ns-dep-graph/issues/5
    # added above documented dep to ~/.lein/profile.clj to make the graphing of the dependncy graph work (for now)

	# rm -f depgraph-*.png
	mkdir -p graphs
	LEIN_USE_BOOTCLASSPATH=no;\
	lein ns-dep-graph -platform :clj -name graphs/depgraph-web
	#lein ns-dep-graph -platform :cljs -name depgraph-acme -parents back-httpkit.core
	#lein ns-dep-graph -platform :cljc -name depgraph-core -parents back-httpkit.core
	open graphs/depgraph-*.png

test:
	shadow-cljs watch test

nrpk-site:
	ssh -i ~/.ssh/nrpk nrpknusz@cpanel64.proisp.no
