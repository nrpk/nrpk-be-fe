---
originator: Chris Schreiner
title: Readme
---

# Notes

```
# each in a separate terminal
docker-compose up db
make backend-dev
make frontend-dev -> lein shadow watch wapp
```

Serves from :9000
Swagger is available

For additional details regarding configuration

```bash
make report
```

In cursive, connect to repl via port :7002
(shadow/repl :wapp)

## Dev-notes for :wapp – resources

The :wapp entry in project.clj points to a specific build that is used while developing the front-end without the back-end.

The entry-uri is `localhost:8050/wapp` and it is named like this to let the system contain its own index.html.

The defintion in project.clj looks like this:

```clojure
  :wapp
  {:target           :browser
  :compiler-options {:infer-externs :auto}
  :output-dir       "resources/public/wapp/js"
  :asset-path       "/wapp/js"
  :modules          {:wapp {:init-fn website.core/init!
                            :entries [website.core]}}
  :build-hooks      []
  :release          {:compiler-options {:optimizations :advanced}}
  :devtools
  {:http-port 8050
    :http-root "resources/public"
    ;:http-root "resources/public"
    :watch-dir "resources/public" :preloads [re-frisk.preload]}
  :dev
  {:closure-defines {"re_frame.trace.trace_enabled_QMARK_" true}}}
```

Please note the values for `:output-dir` and `:asset-path` as they are set with <u>unconventional</u> values. Since the entry-uri is shifted from `/` to `/wapp` the css can be accessed normally from `/css` while keeping each target in a separate folder (**with a separate index.html**)

## How to setup development-environment and how to connect to shadow inspect

In nrepl-connection running at 7002 (shadow):
```clojure
(shadow/repl :wapp) ;; for instance...
(require 'shadow.remote.runtime.cljs.browser)

```

Open the browser at :9630 and go to the inspect tab, select the appropriate instance and:

```clojure
(tap> @re-frame.db/app-db) ;; for instance...
```

You can now traverse the structure in the browser.



---

Tags: #wapp #config #progressnotes #notes #readme
