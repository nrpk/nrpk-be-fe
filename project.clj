(defproject back_httpkit "0.0.1"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[buddy "2.0.0"]
                 [ch.qos.logback/logback-classic "1.2.3"]
                 [cheshire "5.9.0"]
                 [clj-oauth "1.5.5"]
                 [cljs-ajax "0.8.0"]
                 [clojure.java-time "0.3.2"]
                 [com.cognitect/transit-clj "0.8.319"]
                 [com.google.javascript/closure-compiler-unshaded "v20191027" :scope "provided"]
                 [com.walmartlabs/lacinia "0.35.0"]
                 [conman "0.8.4"]
                 [cprop "0.1.14"]
                 [day8.re-frame/http-fx "0.1.6"]
                 ;[expound "0.7.2"]
                 [re-pressed "0.3.1"]
                 [funcool/struct "1.4.0"]
                 [luminus-http-kit "0.1.6"]
                 [luminus-migrations "0.6.6"]
                 [luminus-transit "0.1.2"]
                 [luminus/ring-ttl-session "0.3.3"]
                 [markdown-clj "1.10.0"]
                 [metosin/muuntaja "0.6.5"]
                 [metosin/reitit "0.3.10"]
                 [metosin/ring-http-response "0.9.1"]
                 ;; front-end routing
                 [metosin/reitit-schema "0.3.10"]
                 [metosin/reitit-frontend "0.3.10"]
                 [mount "0.1.16"]
                 [nrepl "0.6.0"]
                 ;[clojure.java-time "0.3.1"]
                 [org.clojure/core.match "0.3.0"]
                 [org.clojure/spec.alpha "0.2.176"]
                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.520" :scope "provided"]
                 [org.clojure/core.async "0.4.500"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/google-closure-library "0.0-20190213-2033d5d9" :scope "provided"]
                 [org.clojure/tools.cli "0.4.2"]
                 [org.clojure/tools.logging "0.5.0"]
                 [org.postgresql/postgresql "42.2.8"]
                 [org.webjars.npm/bulma "0.7.5"]
                 [org.webjars.npm/material-icons "0.3.1"]
                 [org.webjars/webjars-locator "0.37"]
                 [re-frame "0.10.9"]
                 [reagent "0.8.1"]
                 [ring-webjars "0.2.0"]
                 [ring/ring-core "1.7.1"]
                 [ring/ring-defaults "0.3.2"]
                 [selmer "1.12.17"]
                 [thheller/shadow-cljs "2.8.69" :scope "provided"]
                 [re-graph "0.1.11"]
                 [com.andrewmcveigh/cljs-time "0.5.2"]
                 ;; sci
                 [borkdude/sci "0.0.11-alpha.6"]
                 ;; colorsamples
                 [org.clojure/math.combinatorics "0.1.6"]
                 [thi.ng/color "1.4.0"]
                 ;; parsing etc
                 [hiccup "1.0.5"]
                 [instaparse "1.4.10"]
                 ;; related to the integrant integration with re-frame
                 [integrant "0.7.0"]
                 [simple-time "0.2.0"]
                 [binaryage/oops "0.6.4"]
                 ; benchmark
                 [criterium "0.4.5"]]

  :min-lein-version "2.0.0"
  :source-paths ["src/clj" "src/cljs" "src/cljc"]
  :test-paths ["test/cljs" "test/cljc" "test/clj"]
  :resource-paths ["resources" "target/cljsbuild"]
  :target-path "target/%s/"

  ;;
  ;; Why does pedestal skip AOT?   https://groups.google.com/forum/#!topic/pedestal-users/RqgDJZoAip0
  ;; Reason for skipping AOT? 			https://stackoverflow.com/questions/11174459/reason-for-skipping-aot
  ;;
  ;; All (System/getenv "VARIABLE_NAME") will fail if AOT so this is why.
  ;; Let Heroku do the AOT step for you instead.
  ;;
  :main ^:skip-aot back-httpkit.core

  :ring {:auto-refresh? true
         :auto-reload? true
         :refresh-paths ["src" "resources"]
         :reload-paths ["src" "resources"]}

  :plugins [[lein-shadow "0.1.6"]]
  :clean-targets ^{:protect false} [:target-path "target/cljsbuild"]
  :shadow-cljs {:nrepl        {:port 7002}
                :dependencies [;; related to the integrant integration with re-frame
                               ;; todo: Å forklare hvorfor denne er definert her:
                               [integrant "0.7.0"]]
                :builds
                {:ssr
                 {:target           :browser
                  :main             ^:skip-aot back-httpkit.sample
                  :compiler-options {:infer-externs :auto}
                  :output-dir       "target/cljsbuild/public/ssr-js"
                  :asset-path       "/js"
                  :modules          {:ssr {:init-fn back-httpkit.sample/init!
                                           :entries [back-httpkit.sample]}}
                  :build-hooks      []
                  :release          {:compiler-options {:optimizations :advanced}}
                  :devtools
                  {:watch-dir "resources/public"}
                  :dev
                  {:closure-defines {"re_frame.trace.trace_enabled_QMARK_" true}}}

                 :app
                 {:target           :browser
                  :compiler-options {:infer-externs :auto}
                  :output-dir       "target/cljsbuild/public/js"
                  :asset-path       "/js"
                  :modules          {:app {:entries [back-httpkit.app]}}
                  :build-hooks      []
                  :release          {:compiler-options {:optimizations :advanced}}
                  :devtools
                  {:watch-dir "resources/public" :_preloads [re-frisk.preload]}
                  :dev
                  {:closure-defines {"re_frame.trace.trace_enabled_QMARK_" true}}}

                 :wapp
                 {:target           :browser
                  :compiler-options {:infer-externs :auto}
                  :output-dir       "resources/public/wapp/js"
                  :asset-path       "/wapp/js"
                  :modules          {:wapp {:init-fn website.core/init!
                                            :entries [website.core]}}
                  :build-hooks      []
                  :release          {:compiler-options {:optimizations :advanced}}
                  :devtools
                  {:http-port 8050
                   :http-root "resources/public"
                   :watch-dir "resources/public" :preloads [re-frisk.preload]}
                  :dev
                  {:closure-defines {"re_frame.trace.trace_enabled_QMARK_" true}}}

                 :skjema
                 {:target           :browser
                  :compiler-options {:infer-externs :auto}
                  :output-dir       "resources/public/skjema/js"
                  :asset-path       "/skjema/js"
                  :modules          {:wapp {:init-fn website.core/init!
                                            :entries [website.core]}}
                  :build-hooks      []
                  :release          {:compiler-options {:optimizations :advanced}}
                  :devtools
                  {:http-port 8050
                   :http-root "resources/public"
                   :watch-dir "resources/public" :_preloads [re-frisk.preload]}
                  :dev
                  {:closure-defines {"re_frame.trace.trace_enabled_QMARK_" true}}}

                 :test
                 {:target   :browser-test
                  :test-dir "resources/public/js/test"
                  :devtools {:http-port 8021
                             :http-root "resources/public/js/test"}
                  :autorun  true}}}

  :npm-deps [[shadow-cljs "2.8.69"]
             [codemirror "^5.44.0-1"]
             [parinfer-codemirror "^1.4.1-2"]
             [create-react-class "15.6.3"]
             [react "16.8.6"]
             [react-dom "16.8.6"]]

  :profiles {:uberjar {:omit-source true
                       :prep-tasks ["compile" ["shadow" "release" "app"]]

                       :aot :all
                       :uberjar-name "back_httpkit.jar"
                       :source-paths ["env/prod/clj" "env/prod/cljs"]
                       :resource-paths ["env/prod/resources"]}

             :dev           [:project/dev :profiles/dev]
             :test          [:project/dev :project/test :profiles/test]

             :project/dev  {:jvm-opts ["-Dconf=dev-config.edn"]
                            :dependencies [[lein-ancient "0.6.15"]
                                           [binaryage/devtools "0.9.10"]
                                           [cider/piggieback "0.4.2"]
                                           [pjstadig/humane-test-output "0.10.0"]
                                           [prone "2019-07-08"]
                                           [re-frisk "0.5.4.1"]
                                           [ring/ring-devel "1.7.1"]
                                           [ring/ring-mock "0.4.0"]
                                           #_[proto-repl-charts "0.3.2"]
                                           #_[proto-repl "0.3.1"]]
                            :plugins      [[com.jakemccrary/lein-test-refresh "0.24.1"]
                                           [lein-ns-dep-graph "0.2.0-SNAPSHOT"]]

                            :source-paths ["env/dev/clj" "env/dev/cljs" "test/cljs" "test/cljc"]
                            :resource-paths ["env/dev/resources"]
                            :repl-options {:init-ns user
                                           :init (require '[clojure.repl :refer :all])}
                            :injections [(require 'pjstadig.humane-test-output)
                                         (pjstadig.humane-test-output/activate!)]}
             :project/test {:jvm-opts ["-Dconf=test-config.edn"]
                            :resource-paths ["env/test/resources"]} :profiles/dev {}
             :profiles/test {}})
