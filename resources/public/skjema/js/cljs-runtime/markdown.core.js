goog.provide('markdown.core');
goog.require('cljs.core');
goog.require('markdown.common');
goog.require('markdown.links');
goog.require('markdown.transformers');
markdown.core.init_transformer = (function markdown$core$init_transformer(p__89217){
var map__89218 = p__89217;
var map__89218__$1 = (((((!((map__89218 == null))))?(((((map__89218.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__89218.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__89218):map__89218);
var replacement_transformers = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89218__$1,new cljs.core.Keyword(null,"replacement-transformers","replacement-transformers",-2028552897));
var custom_transformers = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89218__$1,new cljs.core.Keyword(null,"custom-transformers","custom-transformers",1440601790));
var inhibit_separator = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89218__$1,new cljs.core.Keyword(null,"inhibit-separator","inhibit-separator",1268116222));
return ((function (map__89218,map__89218__$1,replacement_transformers,custom_transformers,inhibit_separator){
return (function (html,line,next_line,state){
var _STAR_inhibit_separator_STAR__orig_val__89220 = markdown.common._STAR_inhibit_separator_STAR_;
var _STAR_inhibit_separator_STAR__temp_val__89221 = inhibit_separator;
markdown.common._STAR_inhibit_separator_STAR_ = _STAR_inhibit_separator_STAR__temp_val__89221;

try{var vec__89222 = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (_STAR_inhibit_separator_STAR__orig_val__89220,_STAR_inhibit_separator_STAR__temp_val__89221,map__89218,map__89218__$1,replacement_transformers,custom_transformers,inhibit_separator){
return (function (p__89225,transformer){
var vec__89226 = p__89225;
var text = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89226,(0),null);
var state__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89226,(1),null);
var G__89229 = text;
var G__89230 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(state__$1,new cljs.core.Keyword(null,"next-line","next-line",-1187000287),next_line);
return (transformer.cljs$core$IFn$_invoke$arity$2 ? transformer.cljs$core$IFn$_invoke$arity$2(G__89229,G__89230) : transformer.call(null,G__89229,G__89230));
});})(_STAR_inhibit_separator_STAR__orig_val__89220,_STAR_inhibit_separator_STAR__temp_val__89221,map__89218,map__89218__$1,replacement_transformers,custom_transformers,inhibit_separator))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [line,state], null),(function (){var or__4131__auto__ = replacement_transformers;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(markdown.transformers.transformer_vector,custom_transformers);
}
})());
var text = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89222,(0),null);
var new_state = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89222,(1),null);
html.append(text);

return new_state;
}finally {markdown.common._STAR_inhibit_separator_STAR_ = _STAR_inhibit_separator_STAR__orig_val__89220;
}});
;})(map__89218,map__89218__$1,replacement_transformers,custom_transformers,inhibit_separator))
});
/**
 * Removed from cljs.core 0.0-1885, Ref. http://goo.gl/su7Xkj
 */
markdown.core.format = (function markdown$core$format(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89323 = arguments.length;
var i__4731__auto___89324 = (0);
while(true){
if((i__4731__auto___89324 < len__4730__auto___89323)){
args__4736__auto__.push((arguments[i__4731__auto___89324]));

var G__89325 = (i__4731__auto___89324 + (1));
i__4731__auto___89324 = G__89325;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return markdown.core.format.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

markdown.core.format.cljs$core$IFn$_invoke$arity$variadic = (function (fmt,args){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(goog.string.format,fmt,args);
});

markdown.core.format.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
markdown.core.format.cljs$lang$applyTo = (function (seq89233){
var G__89234 = cljs.core.first(seq89233);
var seq89233__$1 = cljs.core.next(seq89233);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__89234,seq89233__$1);
});

markdown.core.parse_references = (function markdown$core$parse_references(lines){
var references = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var seq__89238_89326 = cljs.core.seq(lines);
var chunk__89239_89327 = null;
var count__89240_89328 = (0);
var i__89241_89329 = (0);
while(true){
if((i__89241_89329 < count__89240_89328)){
var line_89330 = chunk__89239_89327.cljs$core$IIndexed$_nth$arity$2(null,i__89241_89329);
markdown.links.parse_reference_link(line_89330,references);


var G__89331 = seq__89238_89326;
var G__89332 = chunk__89239_89327;
var G__89333 = count__89240_89328;
var G__89334 = (i__89241_89329 + (1));
seq__89238_89326 = G__89331;
chunk__89239_89327 = G__89332;
count__89240_89328 = G__89333;
i__89241_89329 = G__89334;
continue;
} else {
var temp__5735__auto___89335 = cljs.core.seq(seq__89238_89326);
if(temp__5735__auto___89335){
var seq__89238_89336__$1 = temp__5735__auto___89335;
if(cljs.core.chunked_seq_QMARK_(seq__89238_89336__$1)){
var c__4550__auto___89337 = cljs.core.chunk_first(seq__89238_89336__$1);
var G__89338 = cljs.core.chunk_rest(seq__89238_89336__$1);
var G__89339 = c__4550__auto___89337;
var G__89340 = cljs.core.count(c__4550__auto___89337);
var G__89341 = (0);
seq__89238_89326 = G__89338;
chunk__89239_89327 = G__89339;
count__89240_89328 = G__89340;
i__89241_89329 = G__89341;
continue;
} else {
var line_89343 = cljs.core.first(seq__89238_89336__$1);
markdown.links.parse_reference_link(line_89343,references);


var G__89344 = cljs.core.next(seq__89238_89336__$1);
var G__89345 = null;
var G__89346 = (0);
var G__89347 = (0);
seq__89238_89326 = G__89344;
chunk__89239_89327 = G__89345;
count__89240_89328 = G__89346;
i__89241_89329 = G__89347;
continue;
}
} else {
}
}
break;
}

return cljs.core.deref(references);
});
markdown.core.parse_footnotes = (function markdown$core$parse_footnotes(lines){
var footnotes = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"next-fn-id","next-fn-id",738579636),(1),new cljs.core.Keyword(null,"processed","processed",800622264),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"unprocessed","unprocessed",766771972),cljs.core.PersistentArrayMap.EMPTY], null));
var seq__89244_89350 = cljs.core.seq(lines);
var chunk__89245_89351 = null;
var count__89246_89352 = (0);
var i__89247_89353 = (0);
while(true){
if((i__89247_89353 < count__89246_89352)){
var line_89354 = chunk__89245_89351.cljs$core$IIndexed$_nth$arity$2(null,i__89247_89353);
markdown.links.parse_footnote_link(line_89354,footnotes);


var G__89356 = seq__89244_89350;
var G__89357 = chunk__89245_89351;
var G__89358 = count__89246_89352;
var G__89359 = (i__89247_89353 + (1));
seq__89244_89350 = G__89356;
chunk__89245_89351 = G__89357;
count__89246_89352 = G__89358;
i__89247_89353 = G__89359;
continue;
} else {
var temp__5735__auto___89360 = cljs.core.seq(seq__89244_89350);
if(temp__5735__auto___89360){
var seq__89244_89361__$1 = temp__5735__auto___89360;
if(cljs.core.chunked_seq_QMARK_(seq__89244_89361__$1)){
var c__4550__auto___89362 = cljs.core.chunk_first(seq__89244_89361__$1);
var G__89363 = cljs.core.chunk_rest(seq__89244_89361__$1);
var G__89364 = c__4550__auto___89362;
var G__89365 = cljs.core.count(c__4550__auto___89362);
var G__89366 = (0);
seq__89244_89350 = G__89363;
chunk__89245_89351 = G__89364;
count__89246_89352 = G__89365;
i__89247_89353 = G__89366;
continue;
} else {
var line_89367 = cljs.core.first(seq__89244_89361__$1);
markdown.links.parse_footnote_link(line_89367,footnotes);


var G__89368 = cljs.core.next(seq__89244_89361__$1);
var G__89369 = null;
var G__89370 = (0);
var G__89371 = (0);
seq__89244_89350 = G__89368;
chunk__89245_89351 = G__89369;
count__89246_89352 = G__89370;
i__89247_89353 = G__89371;
continue;
}
} else {
}
}
break;
}

return cljs.core.deref(footnotes);
});
markdown.core.parse_metadata = (function markdown$core$parse_metadata(lines){
var vec__89252 = cljs.core.split_with((function (p1__89251_SHARP_){
return cljs.core.not_empty(p1__89251_SHARP_.trim());
}),lines);
var metadata = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89252,(0),null);
var lines__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89252,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [markdown.transformers.parse_metadata_headers(metadata),lines__$1], null);
});
/**
 * processes input text line by line and outputs an HTML string
 */
markdown.core.md_to_html_string_STAR_ = (function markdown$core$md_to_html_string_STAR_(text,params){
var _STAR_substring_STAR__orig_val__89256 = markdown.common._STAR_substring_STAR_;
var _STAR_formatter_STAR__orig_val__89257 = markdown.transformers._STAR_formatter_STAR_;
var _STAR_substring_STAR__temp_val__89258 = ((function (_STAR_substring_STAR__orig_val__89256,_STAR_formatter_STAR__orig_val__89257){
return (function (s,n){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.drop.cljs$core$IFn$_invoke$arity$2(n,s));
});})(_STAR_substring_STAR__orig_val__89256,_STAR_formatter_STAR__orig_val__89257))
;
var _STAR_formatter_STAR__temp_val__89259 = markdown.core.format;
markdown.common._STAR_substring_STAR_ = _STAR_substring_STAR__temp_val__89258;

markdown.transformers._STAR_formatter_STAR_ = _STAR_formatter_STAR__temp_val__89259;

try{var params__$1 = (cljs.core.truth_(params)?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.assoc,cljs.core.PersistentArrayMap.EMPTY),params):null);
var lines = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text),"\n"].join('').split("\n");
var html = (new goog.string.StringBuffer(""));
var references = (cljs.core.truth_(new cljs.core.Keyword(null,"reference-links?","reference-links?",-2003778981).cljs$core$IFn$_invoke$arity$1(params__$1))?markdown.core.parse_references(lines):null);
var footnotes = (cljs.core.truth_(new cljs.core.Keyword(null,"footnotes?","footnotes?",-1590157845).cljs$core$IFn$_invoke$arity$1(params__$1))?markdown.core.parse_footnotes(lines):null);
var vec__89261 = (cljs.core.truth_(new cljs.core.Keyword(null,"parse-meta?","parse-meta?",-1938948742).cljs$core$IFn$_invoke$arity$1(params__$1))?markdown.core.parse_metadata(lines):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,lines], null));
var metadata = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89261,(0),null);
var lines__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89261,(1),null);
var transformer = markdown.core.init_transformer(params__$1);
var G__89267_89372 = lines__$1;
var vec__89268_89373 = G__89267_89372;
var seq__89269_89374 = cljs.core.seq(vec__89268_89373);
var first__89270_89375 = cljs.core.first(seq__89269_89374);
var seq__89269_89376__$1 = cljs.core.next(seq__89269_89374);
var line_89377 = first__89270_89375;
var more_89378 = seq__89269_89376__$1;
var state_89379 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"clojurescript","clojurescript",-299769403),true,new cljs.core.Keyword(null,"references","references",882562509),references,new cljs.core.Keyword(null,"footnotes","footnotes",-1842778205),footnotes,new cljs.core.Keyword(null,"last-line-empty?","last-line-empty?",1279111527),true], null),params__$1], 0));
var G__89267_89384__$1 = G__89267_89372;
var state_89385__$1 = state_89379;
while(true){
var vec__89288_89386 = G__89267_89384__$1;
var seq__89289_89387 = cljs.core.seq(vec__89288_89386);
var first__89290_89388 = cljs.core.first(seq__89289_89387);
var seq__89289_89389__$1 = cljs.core.next(seq__89289_89387);
var line_89390__$1 = first__89290_89388;
var more_89391__$1 = seq__89289_89389__$1;
var state_89392__$2 = state_89385__$1;
var line_89394__$2 = (cljs.core.truth_(new cljs.core.Keyword(null,"skip-next-line?","skip-next-line?",1683617749).cljs$core$IFn$_invoke$arity$1(state_89392__$2))?"":line_89390__$1);
var state_89395__$3 = (cljs.core.truth_(new cljs.core.Keyword(null,"buf","buf",-213913340).cljs$core$IFn$_invoke$arity$1(state_89392__$2))?(function (){var G__89291 = html;
var G__89292 = new cljs.core.Keyword(null,"buf","buf",-213913340).cljs$core$IFn$_invoke$arity$1(state_89392__$2);
var G__89293 = new cljs.core.Keyword(null,"next-line","next-line",-1187000287).cljs$core$IFn$_invoke$arity$1(state_89392__$2);
var G__89294 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(state_89392__$2,new cljs.core.Keyword(null,"buf","buf",-213913340),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"lists","lists",-884730684),new cljs.core.Keyword(null,"next-line","next-line",-1187000287)], 0)),new cljs.core.Keyword(null,"last-line-empty?","last-line-empty?",1279111527),true);
return (transformer.cljs$core$IFn$_invoke$arity$4 ? transformer.cljs$core$IFn$_invoke$arity$4(G__89291,G__89292,G__89293,G__89294) : transformer.call(null,G__89291,G__89292,G__89293,G__89294));
})():state_89392__$2);
if(cljs.core.truth_(cljs.core.not_empty(more_89391__$1))){
var G__89399 = more_89391__$1;
var G__89400 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__89297 = html;
var G__89298 = line_89394__$2;
var G__89299 = cljs.core.first(more_89391__$1);
var G__89300 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(state_89395__$3,new cljs.core.Keyword(null,"skip-next-line?","skip-next-line?",1683617749));
return (transformer.cljs$core$IFn$_invoke$arity$4 ? transformer.cljs$core$IFn$_invoke$arity$4(G__89297,G__89298,G__89299,G__89300) : transformer.call(null,G__89297,G__89298,G__89299,G__89300));
})(),new cljs.core.Keyword(null,"last-line-empty?","last-line-empty?",1279111527),cljs.core.empty_QMARK_(line_89394__$2.trim()));
G__89267_89384__$1 = G__89399;
state_89385__$1 = G__89400;
continue;
} else {
var G__89301_89401 = html.append(markdown.transformers.footer(new cljs.core.Keyword(null,"footnotes","footnotes",-1842778205).cljs$core$IFn$_invoke$arity$1(state_89395__$3)));
var G__89302_89402 = line_89394__$2;
var G__89303_89403 = "";
var G__89304_89404 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(state_89395__$3,new cljs.core.Keyword(null,"eof","eof",-489063237),true);
(transformer.cljs$core$IFn$_invoke$arity$4 ? transformer.cljs$core$IFn$_invoke$arity$4(G__89301_89401,G__89302_89402,G__89303_89403,G__89304_89404) : transformer.call(null,G__89301_89401,G__89302_89402,G__89303_89403,G__89304_89404));
}
break;
}

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"metadata","metadata",1799301597),metadata,new cljs.core.Keyword(null,"html","html",-998796897),html.toString()], null);
}finally {markdown.transformers._STAR_formatter_STAR_ = _STAR_formatter_STAR__orig_val__89257;

markdown.common._STAR_substring_STAR_ = _STAR_substring_STAR__orig_val__89256;
}});
markdown.core.md__GT_html = (function markdown$core$md__GT_html(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89405 = arguments.length;
var i__4731__auto___89406 = (0);
while(true){
if((i__4731__auto___89406 < len__4730__auto___89405)){
args__4736__auto__.push((arguments[i__4731__auto___89406]));

var G__89407 = (i__4731__auto___89406 + (1));
i__4731__auto___89406 = G__89407;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return markdown.core.md__GT_html.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

markdown.core.md__GT_html.cljs$core$IFn$_invoke$arity$variadic = (function (text,params){
return new cljs.core.Keyword(null,"html","html",-998796897).cljs$core$IFn$_invoke$arity$1(markdown.core.md_to_html_string_STAR_(text,params));
});

markdown.core.md__GT_html.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
markdown.core.md__GT_html.cljs$lang$applyTo = (function (seq89305){
var G__89306 = cljs.core.first(seq89305);
var seq89305__$1 = cljs.core.next(seq89305);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__89306,seq89305__$1);
});

markdown.core.md__GT_html_with_meta = (function markdown$core$md__GT_html_with_meta(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89409 = arguments.length;
var i__4731__auto___89410 = (0);
while(true){
if((i__4731__auto___89410 < len__4730__auto___89409)){
args__4736__auto__.push((arguments[i__4731__auto___89410]));

var G__89411 = (i__4731__auto___89410 + (1));
i__4731__auto___89410 = G__89411;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return markdown.core.md__GT_html_with_meta.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

markdown.core.md__GT_html_with_meta.cljs$core$IFn$_invoke$arity$variadic = (function (text,params){
return markdown.core.md_to_html_string_STAR_(text,cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"parse-meta?","parse-meta?",-1938948742),true], null),params));
});

markdown.core.md__GT_html_with_meta.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
markdown.core.md__GT_html_with_meta.cljs$lang$applyTo = (function (seq89309){
var G__89310 = cljs.core.first(seq89309);
var seq89309__$1 = cljs.core.next(seq89309);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__89310,seq89309__$1);
});

/**
 * Js accessible wrapper
 */
markdown.core.mdToHtml = (function markdown$core$mdToHtml(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89412 = arguments.length;
var i__4731__auto___89413 = (0);
while(true){
if((i__4731__auto___89413 < len__4730__auto___89412)){
args__4736__auto__.push((arguments[i__4731__auto___89413]));

var G__89414 = (i__4731__auto___89413 + (1));
i__4731__auto___89413 = G__89414;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return markdown.core.mdToHtml.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});
goog.exportSymbol('markdown.core.mdToHtml', markdown.core.mdToHtml);

markdown.core.mdToHtml.cljs$core$IFn$_invoke$arity$variadic = (function (params){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(markdown.core.md__GT_html,params);
});

markdown.core.mdToHtml.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
markdown.core.mdToHtml.cljs$lang$applyTo = (function (seq89315){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq89315));
});

/**
 * Js accessible wrapper
 */
markdown.core.mdToHtmlWithMeta = (function markdown$core$mdToHtmlWithMeta(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89415 = arguments.length;
var i__4731__auto___89416 = (0);
while(true){
if((i__4731__auto___89416 < len__4730__auto___89415)){
args__4736__auto__.push((arguments[i__4731__auto___89416]));

var G__89417 = (i__4731__auto___89416 + (1));
i__4731__auto___89416 = G__89417;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return markdown.core.mdToHtmlWithMeta.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});
goog.exportSymbol('markdown.core.mdToHtmlWithMeta', markdown.core.mdToHtmlWithMeta);

markdown.core.mdToHtmlWithMeta.cljs$core$IFn$_invoke$arity$variadic = (function (params){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(markdown.core.md__GT_html_with_meta,params);
});

markdown.core.mdToHtmlWithMeta.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
markdown.core.mdToHtmlWithMeta.cljs$lang$applyTo = (function (seq89318){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq89318));
});


//# sourceMappingURL=markdown.core.js.map
