goog.provide('website.keyboard');
goog.require('cljs.core');
goog.require('re_frame.core');
goog.require('re_pressed.core');
goog.require('website.keyboard_defs');
website.keyboard.define_key = (function website$keyboard$define_key(var_args){
var args__4736__auto__ = [];
var len__4730__auto___86888 = arguments.length;
var i__4731__auto___86889 = (0);
while(true){
if((i__4731__auto___86889 < len__4730__auto___86888)){
args__4736__auto__.push((arguments[i__4731__auto___86889]));

var G__86896 = (i__4731__auto___86889 + (1));
i__4731__auto___86889 = G__86896;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return website.keyboard.define_key.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

website.keyboard.define_key.cljs$core$IFn$_invoke$arity$variadic = (function (shortcut,msg){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.vec(cljs.core.first(msg)),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keyCode","keyCode",1964303637),shortcut,new cljs.core.Keyword(null,"metaKey","metaKey",1006742180),true], null)], null)], null);
});

website.keyboard.define_key.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
website.keyboard.define_key.cljs$lang$applyTo = (function (seq86867){
var G__86868 = cljs.core.first(seq86867);
var seq86867__$1 = cljs.core.next(seq86867);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__86868,seq86867__$1);
});

website.keyboard.keydata = (function website$keyboard$keydata(){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.concat.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__86882_SHARP_){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(website.keyboard.define_key,(function (){var fexpr__86883 = cljs.core.juxt.cljs$core$IFn$_invoke$arity$2(cljs.core.first,cljs.core.rest);
return (fexpr__86883.cljs$core$IFn$_invoke$arity$1 ? fexpr__86883.cljs$core$IFn$_invoke$arity$1(p1__86882_SHARP_) : fexpr__86883.call(null,p1__86882_SHARP_));
})());
}),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.keyboard_defs.RETURN,new cljs.core.Keyword("some-handler","eval-buffer","some-handler/eval-buffer",404267966)], null)], null))));
});
website.keyboard.default_shortcuts = (function website$keyboard$default_shortcuts(){
var G__86886_86900 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("re-pressed.core","add-keyboard-event-listener","re-pressed.core/add-keyboard-event-listener",719500381),"keydown"], null);
(re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1(G__86886_86900) : re_frame.core.dispatch_sync.call(null,G__86886_86900));

var G__86887 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("re-pressed.core","set-keydown-rules","re-pressed.core/set-keydown-rules",1002257871),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"event-keys","event-keys",-835966631),website.keyboard.keydata(),new cljs.core.Keyword(null,"clear-keys","clear-keys",-1035470539),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"keyCode","keyCode",1964303637),website.keyboard_defs.ESC], null)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keyCode","keyCode",1964303637),website.keyboard_defs.G,new cljs.core.Keyword(null,"metaKey","metaKey",1006742180),true], null)], null)], null),new cljs.core.Keyword(null,"prevent-default-keys","prevent-default-keys",1744205310),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keyCode","keyCode",1964303637),website.keyboard_defs.RETURN,new cljs.core.Keyword(null,"ctrlKey","ctrlKey",430760908),true], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keyCode","keyCode",1964303637),website.keyboard_defs.G,new cljs.core.Keyword(null,"ctrlKey","ctrlKey",430760908),true], null)], null),new cljs.core.Keyword(null,"always-listen-keys","always-listen-keys",497991050),cljs.core.PersistentVector.EMPTY], null)], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__86887) : re_frame.core.dispatch.call(null,G__86887));
});

//# sourceMappingURL=website.keyboard.js.map
