goog.provide('website.pages.colorsamples');
goog.require('cljs.core');
goog.require('clojure.math.combinatorics');
goog.require('thi.ng.color.core');
goog.require('website.toolbar');
website.pages.colorsamples.niceBlue = new cljs.core.Keyword(null,"#55C6F1","#55C6F1",2033091245);
website.pages.colorsamples.primBlue = new cljs.core.Keyword(null,"#61839E","#61839E",-501414644);
website.pages.colorsamples.primRed = new cljs.core.Keyword(null,"#CD7F8A","#CD7F8A",2080847326);
website.pages.colorsamples.primBrown = new cljs.core.Keyword(null,"#7E6868","#7E6868",649286982);
website.pages.colorsamples.palette = cljs.core.flatten(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var iter__4523__auto__ = (function website$pages$colorsamples$iter__93551(s__93552){
return (new cljs.core.LazySeq(null,(function (){
var s__93552__$1 = s__93552;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93552__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var c = cljs.core.first(xs__6292__auto__);
var iterys__4519__auto__ = ((function (s__93552__$1,c,xs__6292__auto__,temp__5735__auto__){
return (function website$pages$colorsamples$iter__93551_$_iter__93553(s__93554){
return (new cljs.core.LazySeq(null,((function (s__93552__$1,c,xs__6292__auto__,temp__5735__auto__){
return (function (){
var s__93554__$1 = s__93554;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__93554__$1);
if(temp__5735__auto____$1){
var s__93554__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__93554__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93554__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93556 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93555 = (0);
while(true){
if((i__93555 < size__4522__auto__)){
var e = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93555);
cljs.core.chunk_append(b__93556,cljs.core.deref(thi.ng.color.core.as_css(thi.ng.color.core.as_int24(thi.ng.color.core.css(cljs.core.name(c)).thi$ng$color$core$IColorOps$adjust_luminance$arity$2(null,(((5) - e) / (8)))))));

var G__93589 = (i__93555 + (1));
i__93555 = G__93589;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93556),website$pages$colorsamples$iter__93551_$_iter__93553(cljs.core.chunk_rest(s__93554__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93556),null);
}
} else {
var e = cljs.core.first(s__93554__$2);
return cljs.core.cons(cljs.core.deref(thi.ng.color.core.as_css(thi.ng.color.core.as_int24(thi.ng.color.core.css(cljs.core.name(c)).thi$ng$color$core$IColorOps$adjust_luminance$arity$2(null,(((5) - e) / (8)))))),website$pages$colorsamples$iter__93551_$_iter__93553(cljs.core.rest(s__93554__$2)));
}
} else {
return null;
}
break;
}
});})(s__93552__$1,c,xs__6292__auto__,temp__5735__auto__))
,null,null));
});})(s__93552__$1,c,xs__6292__auto__,temp__5735__auto__))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2((1),(10))));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,website$pages$colorsamples$iter__93551(cljs.core.rest(s__93552__$1)));
} else {
var G__93591 = cljs.core.rest(s__93552__$1);
s__93552__$1 = G__93591;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.pages.colorsamples.niceBlue,website.pages.colorsamples.primBlue,website.pages.colorsamples.primBlue,website.pages.colorsamples.primRed,website.pages.colorsamples.primBrown,new cljs.core.Keyword(null,"#121012","#121012",2096242860),new cljs.core.Keyword(null,"#e1e5e1","#e1e5e1",-1442791856)], null));
})()], null));
website.pages.colorsamples.page = (function website$pages$colorsamples$page(){
return null;
});
website.pages.colorsamples.colors = clojure.math.combinatorics.permutations(website.pages.colorsamples.palette);
website.pages.colorsamples.colors_SINGLEQUOTE_ = cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.list,website.pages.colorsamples.palette,cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1(cljs.core.constantly(new cljs.core.Keyword(null,"#fff","#fff",461169500))));
website.pages.colorsamples.render = (function website$pages$colorsamples$render(){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-12","div.mt-12",-1953716378),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-1","div.px-1",1865452376),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1.text-6xl.text-white.font-black","h1.text-6xl.text-white.font-black",-1125337802),"Tigerstad"], null),(function (){var iter__4523__auto__ = (function website$pages$colorsamples$render_$_iter__93562(s__93563){
return (new cljs.core.LazySeq(null,(function (){
var s__93563__$1 = s__93563;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93563__$1);
if(temp__5735__auto__){
var s__93563__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__93563__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93563__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93565 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93564 = (0);
while(true){
if((i__93564 < size__4522__auto__)){
var e = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93564);
cljs.core.chunk_append(b__93565,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-white","p.text-white",661910817),"Some text ",e], null));

var G__93596 = (i__93564 + (1));
i__93564 = G__93596;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93565),website$pages$colorsamples$render_$_iter__93562(cljs.core.chunk_rest(s__93563__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93565),null);
}
} else {
var e = cljs.core.first(s__93563__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-white","p.text-white",661910817),"Some text ",e], null),website$pages$colorsamples$render_$_iter__93562(cljs.core.rest(s__93563__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1((20)));
})()], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-gray-300.text-black.h-10.sticky.shadow-6.w-full","div.bg-gray-300.text-black.h-10.sticky.shadow-6.w-full",250312829),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"top","top",-1856271961),"3rem"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-black","div.text-black",1236204861),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.toolbar.controls], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.p-1._mt-10.text-white","div.p-1._mt-10.text-white",-1917744970),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"4px","4px",200148326),new cljs.core.Keyword(null,"grid-template-columns","grid-template-columns",-594112133),"repeat(auto-fit,minmax(88px,1fr))",new cljs.core.Keyword(null,"grid-auto-rows","grid-auto-rows",-113194069),"minmax(1fr,auto-fill)"], null)], null),(function (){var iter__4523__auto__ = (function website$pages$colorsamples$render_$_iter__93567(s__93568){
return (new cljs.core.LazySeq(null,(function (){
var s__93568__$1 = s__93568;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93568__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var x = cljs.core.first(xs__6292__auto__);
var iterys__4519__auto__ = ((function (s__93568__$1,x,xs__6292__auto__,temp__5735__auto__){
return (function website$pages$colorsamples$render_$_iter__93567_$_iter__93569(s__93570){
return (new cljs.core.LazySeq(null,((function (s__93568__$1,x,xs__6292__auto__,temp__5735__auto__){
return (function (){
var s__93570__$1 = s__93570;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__93570__$1);
if(temp__5735__auto____$1){
var s__93570__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__93570__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93570__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93572 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93571 = (0);
while(true){
if((i__93571 < size__4522__auto__)){
var y = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93571);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(x,y)){
cljs.core.chunk_append(b__93572,(function (){var a = x;
var b = y;
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"place-self","place-self",-639907301),new cljs.core.Keyword(null,"stretch","stretch",-1888837380),new cljs.core.Keyword(null,"background","background",-863952629),a,new cljs.core.Keyword(null,"color","color",1011675173),b], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg._p-2","svg._p-2",2008730627),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"width","width",-384071477),"100%",new cljs.core.Keyword(null,"height","height",1025178622),"auto",new cljs.core.Keyword(null,"preserveAspectRatio","preserveAspectRatio",1832131817),"xMaxYMax slice",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 10 10"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"circle","circle",1903212362),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"cx","cx",1272694324),(10),new cljs.core.Keyword(null,"cy","cy",755331060),(10),new cljs.core.Keyword(null,"r","r",-471384190),(10),new cljs.core.Keyword(null,"fill","fill",883462889),b], null)], null)], null)], null);
})());

var G__93598 = (i__93571 + (1));
i__93571 = G__93598;
continue;
} else {
var G__93599 = (i__93571 + (1));
i__93571 = G__93599;
continue;
}
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93572),website$pages$colorsamples$render_$_iter__93567_$_iter__93569(cljs.core.chunk_rest(s__93570__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93572),null);
}
} else {
var y = cljs.core.first(s__93570__$2);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(x,y)){
return cljs.core.cons((function (){var a = x;
var b = y;
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"place-self","place-self",-639907301),new cljs.core.Keyword(null,"stretch","stretch",-1888837380),new cljs.core.Keyword(null,"background","background",-863952629),a,new cljs.core.Keyword(null,"color","color",1011675173),b], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg._p-2","svg._p-2",2008730627),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"width","width",-384071477),"100%",new cljs.core.Keyword(null,"height","height",1025178622),"auto",new cljs.core.Keyword(null,"preserveAspectRatio","preserveAspectRatio",1832131817),"xMaxYMax slice",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 10 10"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"circle","circle",1903212362),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"cx","cx",1272694324),(10),new cljs.core.Keyword(null,"cy","cy",755331060),(10),new cljs.core.Keyword(null,"r","r",-471384190),(10),new cljs.core.Keyword(null,"fill","fill",883462889),b], null)], null)], null)], null);
})(),website$pages$colorsamples$render_$_iter__93567_$_iter__93569(cljs.core.rest(s__93570__$2)));
} else {
var G__93600 = cljs.core.rest(s__93570__$2);
s__93570__$1 = G__93600;
continue;
}
}
} else {
return null;
}
break;
}
});})(s__93568__$1,x,xs__6292__auto__,temp__5735__auto__))
,null,null));
});})(s__93568__$1,x,xs__6292__auto__,temp__5735__auto__))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(website.pages.colorsamples.palette));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,website$pages$colorsamples$render_$_iter__93567(cljs.core.rest(s__93568__$1)));
} else {
var G__93601 = cljs.core.rest(s__93568__$1);
s__93568__$1 = G__93601;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(website.pages.colorsamples.palette);
})()], null)], null)], null);
});

//# sourceMappingURL=website.pages.colorsamples.js.map
