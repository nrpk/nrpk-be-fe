goog.provide('website.pure.utils');
goog.require('cljs.core');
goog.require('reitit.frontend.easy');
/**
 * Return relative url for given route. Url can be used in HTML links.
 */
website.pure.utils.href = (function website$pure$utils$href(var_args){
var G__89017 = arguments.length;
switch (G__89017) {
case 1:
return website.pure.utils.href.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return website.pure.utils.href.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return website.pure.utils.href.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

website.pure.utils.href.cljs$core$IFn$_invoke$arity$1 = (function (k){
return website.pure.utils.href.cljs$core$IFn$_invoke$arity$3(k,null,null);
});

website.pure.utils.href.cljs$core$IFn$_invoke$arity$2 = (function (k,params){
return website.pure.utils.href.cljs$core$IFn$_invoke$arity$3(k,params,null);
});

website.pure.utils.href.cljs$core$IFn$_invoke$arity$3 = (function (k,params,query){
return reitit.frontend.easy.href.cljs$core$IFn$_invoke$arity$3(k,params,query);
});

website.pure.utils.href.cljs$lang$maxFixedArity = 3;

website.pure.utils.inset_x = (function website$pure$utils$inset_x(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89043 = arguments.length;
var i__4731__auto___89044 = (0);
while(true){
if((i__4731__auto___89044 < len__4730__auto___89043)){
args__4736__auto__.push((arguments[i__4731__auto___89044]));

var G__89047 = (i__4731__auto___89044 + (1));
i__4731__auto___89044 = G__89047;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return website.pure.utils.inset_x.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

website.pure.utils.inset_x.cljs$core$IFn$_invoke$arity$variadic = (function (xs){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-2","div.px-2",1607047380),xs], null);
});

website.pure.utils.inset_x.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
website.pure.utils.inset_x.cljs$lang$applyTo = (function (seq89023){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq89023));
});

website.pure.utils.sprint = (function website$pure$utils$sprint(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89069 = arguments.length;
var i__4731__auto___89070 = (0);
while(true){
if((i__4731__auto___89070 < len__4730__auto___89069)){
args__4736__auto__.push((arguments[i__4731__auto___89070]));

var G__89072 = (i__4731__auto___89070 + (1));
i__4731__auto___89070 = G__89072;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return website.pure.utils.sprint.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

website.pure.utils.sprint.cljs$core$IFn$_invoke$arity$variadic = (function (xs){
var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__89028_89075 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__89029_89076 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__89030_89077 = true;
var _STAR_print_fn_STAR__temp_val__89031_89078 = ((function (_STAR_print_newline_STAR__orig_val__89028_89075,_STAR_print_fn_STAR__orig_val__89029_89076,_STAR_print_newline_STAR__temp_val__89030_89077,sb__4661__auto__){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__89028_89075,_STAR_print_fn_STAR__orig_val__89029_89076,_STAR_print_newline_STAR__temp_val__89030_89077,sb__4661__auto__))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__89030_89077;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__89031_89078;

try{cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.print,xs);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__89029_89076;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__89028_89075;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
});

website.pure.utils.sprint.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
website.pure.utils.sprint.cljs$lang$applyTo = (function (seq89024){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq89024));
});

website.pure.utils.default_sample_value = (function website$pure$utils$default_sample_value(n){
var G__89035 = n;
switch (G__89035) {
case (2):
return "<iframe width=\"300\" height=\"350\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://www.openstreetmap.org/export/embed.html?bbox=10.845168828964235%2C59.874600424194256%2C10.869405269622803%2C59.881357587063434&amp;layer=mapnik\" style=\"border: 1px solid black\"></iframe><br/><small><a href=\"https://www.openstreetmap.org/#map=17/59.87798/10.85729\">View Larger Map</a></small>";

break;
case (1):
return "# This is some stuffn\n\n![link](http://nrpk.no/wp-content/uploads/2019/10/Dugnad-1000x420.jpg)\n> Stuff **bold**\n\n- [ ] gj\u00F8r ditt\n- [ ] gj\u00F8r datt\n\n## Tags\n@topic @leaving-nothing\n    \n\n\n\nEnd\n- 1\n- 2\n- 3\n- 4\n- 5\n- 5\n- 6\n- 7\n    Ting\n    du ma\n    huske\n    pa\n\n    ";

break;
default:
return website.pure.utils.sprint.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["default-sample-value",n,"not found"], 0));

}
});
website.pure.utils.linked = (function website$pure$utils$linked(s,l){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),s,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a.border-blue-400.border-b-2.text-blue-700.font-bold","a.border-blue-400.border-b-2.text-blue-700.font-bold",181742995),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),website.pure.utils.href.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword("website.core","about","website.core/about",1598500653))], null),l], null)], null);
});
website.pure.utils.log_fn = (function website$pure$utils$log_fn(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89084 = arguments.length;
var i__4731__auto___89085 = (0);
while(true){
if((i__4731__auto___89085 < len__4730__auto___89084)){
args__4736__auto__.push((arguments[i__4731__auto___89085]));

var G__89087 = (i__4731__auto___89085 + (1));
i__4731__auto___89085 = G__89087;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return website.pure.utils.log_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

website.pure.utils.log_fn.cljs$core$IFn$_invoke$arity$variadic = (function (params){
return (function (_){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(console.log,params);
});
});

website.pure.utils.log_fn.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
website.pure.utils.log_fn.cljs$lang$applyTo = (function (seq89036){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq89036));
});

website.pure.utils.dangerous = (function website$pure$utils$dangerous(var_args){
var G__89039 = arguments.length;
switch (G__89039) {
case 2:
return website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$2 = (function (comp,content){
return website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$3(comp,null,content);
});

website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$3 = (function (comp,props,content){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [comp,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(props,new cljs.core.Keyword(null,"dangerouslySetInnerHTML","dangerouslySetInnerHTML",-554971138),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"__html","__html",674048345),content], null))], null);
});

website.pure.utils.dangerous.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=website.pure.utils.js.map
