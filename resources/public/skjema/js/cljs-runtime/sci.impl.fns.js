goog.provide('sci.impl.fns');
goog.require('cljs.core');
goog.require('sci.impl.utils');
sci.impl.fns.parse_fn_args_PLUS_body = (function sci$impl$fns$parse_fn_args_PLUS_body(interpret,ctx,p__88186,fn_name,macro_QMARK_,single_arity_QMARK_){
var map__88187 = p__88186;
var map__88187__$1 = (((((!((map__88187 == null))))?(((((map__88187.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__88187.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__88187):map__88187);
var _m = map__88187__$1;
var fixed_arity = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88187__$1,new cljs.core.Keyword("sci.impl","fixed-arity","sci.impl/fixed-arity",-1251617052));
var fixed_names = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88187__$1,new cljs.core.Keyword("sci.impl","fixed-names","sci.impl/fixed-names",-1163180677));
var var_arg_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88187__$1,new cljs.core.Keyword("sci.impl","var-arg-name","sci.impl/var-arg-name",1800498100));
var destructure_vec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88187__$1,new cljs.core.Keyword("sci.impl","destructure-vec","sci.impl/destructure-vec",-692349508));
var _arg_list = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88187__$1,new cljs.core.Keyword("sci.impl","_arg-list","sci.impl/_arg-list",694182669));
var body = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88187__$1,new cljs.core.Keyword("sci.impl","body","sci.impl/body",-1493886648));
var min_var_args_arity = (cljs.core.truth_(var_arg_name)?fixed_arity:null);
var m = (cljs.core.truth_(min_var_args_arity)?new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("sci.impl","min-var-args-arity","sci.impl/min-var-args-arity",-1081358721),min_var_args_arity], null):new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("sci.impl","fixed-arity","sci.impl/fixed-arity",-1251617052),fixed_arity], null));
return cljs.core.with_meta(((function (min_var_args_arity,m,map__88187,map__88187__$1,_m,fixed_arity,fixed_names,var_arg_name,destructure_vec,_arg_list,body){
return (function() { 
var G__88234__delegate = function (args){
while(true){
if(cljs.core.truth_(single_arity_QMARK_)){
var arity_88236 = cljs.core.count(args);
if(cljs.core.truth_((function (){var or__4131__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(arity_88236,fixed_arity);
if(or__4131__auto__){
return or__4131__auto__;
} else {
var and__4120__auto__ = min_var_args_arity;
if(cljs.core.truth_(and__4120__auto__)){
return (arity_88236 >= min_var_args_arity);
} else {
return and__4120__auto__;
}
}
})())){
} else {
throw (new Error((function (){var actual_count = (cljs.core.truth_(macro_QMARK_)?(arity_88236 - (2)):arity_88236);
return ["Cannot call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_name)," with ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(actual_count)," arguments"].join('');
})()));
}
} else {
}

var runtime_bindings = cljs.core.vec(cljs.core.interleave.cljs$core$IFn$_invoke$arity$2(fixed_names,cljs.core.take.cljs$core$IFn$_invoke$arity$2(fixed_arity,args)));
var runtime_bindings__$1 = (cljs.core.truth_(var_arg_name)?cljs.core.conj.cljs$core$IFn$_invoke$arity$variadic(runtime_bindings,var_arg_name,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.drop.cljs$core$IFn$_invoke$arity$2(fixed_arity,args)], 0)):runtime_bindings);
var let_bindings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(runtime_bindings__$1,destructure_vec);
var form = cljs.core.list_STAR_.cljs$core$IFn$_invoke$arity$3(new cljs.core.Symbol(null,"let","let",358118826,null),let_bindings,body);
var ret = (function (){var G__88200 = ctx;
var G__88201 = sci.impl.utils.mark_eval_call(form);
return (interpret.cljs$core$IFn$_invoke$arity$2 ? interpret.cljs$core$IFn$_invoke$arity$2(G__88200,G__88201) : interpret.call(null,G__88200,G__88201));
})();
var m__$1 = cljs.core.meta(ret);
var recur_QMARK_ = new cljs.core.Keyword("sci.impl","recur","sci.impl/recur",-1324363695).cljs$core$IFn$_invoke$arity$1(m__$1);
if(cljs.core.truth_(recur_QMARK_)){
var G__88241 = ret;
args = G__88241;
continue;
} else {
return ret;
}
break;
}
};
var G__88234 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__88242__i = 0, G__88242__a = new Array(arguments.length -  0);
while (G__88242__i < G__88242__a.length) {G__88242__a[G__88242__i] = arguments[G__88242__i + 0]; ++G__88242__i;}
  args = new cljs.core.IndexedSeq(G__88242__a,0,null);
} 
return G__88234__delegate.call(this,args);};
G__88234.cljs$lang$maxFixedArity = 0;
G__88234.cljs$lang$applyTo = (function (arglist__88243){
var args = cljs.core.seq(arglist__88243);
return G__88234__delegate(args);
});
G__88234.cljs$core$IFn$_invoke$arity$variadic = G__88234__delegate;
return G__88234;
})()
;})(min_var_args_arity,m,map__88187,map__88187__$1,_m,fixed_arity,fixed_names,var_arg_name,destructure_vec,_arg_list,body))
,m);
});
sci.impl.fns.lookup_by_arity = (function sci$impl$fns$lookup_by_arity(arities,arity){
return cljs.core.some((function (f){
var map__88203 = cljs.core.meta(f);
var map__88203__$1 = (((((!((map__88203 == null))))?(((((map__88203.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__88203.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__88203):map__88203);
var fixed_arity = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88203__$1,new cljs.core.Keyword("sci.impl","fixed-arity","sci.impl/fixed-arity",-1251617052));
var min_var_args_arity = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88203__$1,new cljs.core.Keyword("sci.impl","min-var-args-arity","sci.impl/min-var-args-arity",-1081358721));
if(cljs.core.truth_((function (){var or__4131__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(arity,fixed_arity);
if(or__4131__auto__){
return or__4131__auto__;
} else {
var and__4120__auto__ = min_var_args_arity;
if(cljs.core.truth_(and__4120__auto__)){
return (arity >= min_var_args_arity);
} else {
return and__4120__auto__;
}
}
})())){
return f;
} else {
return null;
}
}),arities);
});
sci.impl.fns.eval_fn = (function sci$impl$fns$eval_fn(ctx,interpret,p__88223){
var map__88224 = p__88223;
var map__88224__$1 = (((((!((map__88224 == null))))?(((((map__88224.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__88224.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__88224):map__88224);
var f = map__88224__$1;
var fn_bodies = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88224__$1,new cljs.core.Keyword("sci.impl","fn-bodies","sci.impl/fn-bodies",134751661));
var fn_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88224__$1,new cljs.core.Keyword("sci.impl","fn-name","sci.impl/fn-name",-1172300569));
var macro_QMARK_ = new cljs.core.Keyword("sci","macro","sci/macro",-868536151).cljs$core$IFn$_invoke$arity$1(f);
var self_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var call_self = ((function (macro_QMARK_,self_ref,map__88224,map__88224__$1,f,fn_bodies,fn_name){
return (function() { 
var G__88254__delegate = function (args){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self_ref),args);
};
var G__88254 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__88255__i = 0, G__88255__a = new Array(arguments.length -  0);
while (G__88255__i < G__88255__a.length) {G__88255__a[G__88255__i] = arguments[G__88255__i + 0]; ++G__88255__i;}
  args = new cljs.core.IndexedSeq(G__88255__a,0,null);
} 
return G__88254__delegate.call(this,args);};
G__88254.cljs$lang$maxFixedArity = 0;
G__88254.cljs$lang$applyTo = (function (arglist__88256){
var args = cljs.core.seq(arglist__88256);
return G__88254__delegate(args);
});
G__88254.cljs$core$IFn$_invoke$arity$variadic = G__88254__delegate;
return G__88254;
})()
;})(macro_QMARK_,self_ref,map__88224,map__88224__$1,f,fn_bodies,fn_name))
;
var ctx__$1 = (cljs.core.truth_(fn_name)?cljs.core.assoc_in(ctx,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bindings","bindings",1271397192),fn_name], null),call_self):ctx);
var single_arity_QMARK_ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),cljs.core.count(fn_bodies));
var arities = cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (macro_QMARK_,self_ref,call_self,ctx__$1,single_arity_QMARK_,map__88224,map__88224__$1,f,fn_bodies,fn_name){
return (function (p1__88213_SHARP_){
return sci.impl.fns.parse_fn_args_PLUS_body(interpret,ctx__$1,p1__88213_SHARP_,fn_name,macro_QMARK_,single_arity_QMARK_);
});})(macro_QMARK_,self_ref,call_self,ctx__$1,single_arity_QMARK_,map__88224,map__88224__$1,f,fn_bodies,fn_name))
,fn_bodies);
var f__$1 = cljs.core.vary_meta.cljs$core$IFn$_invoke$arity$2(((single_arity_QMARK_)?cljs.core.first(arities):((function (macro_QMARK_,self_ref,call_self,ctx__$1,single_arity_QMARK_,arities,map__88224,map__88224__$1,f,fn_bodies,fn_name){
return (function() { 
var G__88264__delegate = function (args){
var arg_count = cljs.core.count(args);
var temp__5733__auto__ = sci.impl.fns.lookup_by_arity(arities,arg_count);
if(cljs.core.truth_(temp__5733__auto__)){
var f__$1 = temp__5733__auto__;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f__$1,args);
} else {
throw (new Error((function (){var actual_count = (cljs.core.truth_(macro_QMARK_)?(arg_count - (2)):arg_count);
return ["Cannot call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_name)," with ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(actual_count)," arguments"].join('');
})()));
}
};
var G__88264 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__88265__i = 0, G__88265__a = new Array(arguments.length -  0);
while (G__88265__i < G__88265__a.length) {G__88265__a[G__88265__i] = arguments[G__88265__i + 0]; ++G__88265__i;}
  args = new cljs.core.IndexedSeq(G__88265__a,0,null);
} 
return G__88264__delegate.call(this,args);};
G__88264.cljs$lang$maxFixedArity = 0;
G__88264.cljs$lang$applyTo = (function (arglist__88266){
var args = cljs.core.seq(arglist__88266);
return G__88264__delegate(args);
});
G__88264.cljs$core$IFn$_invoke$arity$variadic = G__88264__delegate;
return G__88264;
})()
;})(macro_QMARK_,self_ref,call_self,ctx__$1,single_arity_QMARK_,arities,map__88224,map__88224__$1,f,fn_bodies,fn_name))
),((function (macro_QMARK_,self_ref,call_self,ctx__$1,single_arity_QMARK_,arities,map__88224,map__88224__$1,f,fn_bodies,fn_name){
return (function (p1__88216_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__88216_SHARP_,new cljs.core.Keyword("sci","macro","sci/macro",-868536151),macro_QMARK_);
});})(macro_QMARK_,self_ref,call_self,ctx__$1,single_arity_QMARK_,arities,map__88224,map__88224__$1,f,fn_bodies,fn_name))
);
cljs.core.reset_BANG_(self_ref,f__$1);

return f__$1;
});

//# sourceMappingURL=sci.impl.fns.js.map
