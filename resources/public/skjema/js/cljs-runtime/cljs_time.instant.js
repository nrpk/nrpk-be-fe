goog.provide('cljs_time.instant');
goog.require('cljs.core');
goog.require('goog.date.DateTime');
goog.require('cljs_time.format');
goog.date.UtcDateTime.prototype.cljs$core$IPrintWithWriter$ = cljs.core.PROTOCOL_SENTINEL;

goog.date.UtcDateTime.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (obj,writer,opts){
var obj__$1 = this;
cljs.core._write(writer,"#inst ");

return cljs.core.pr_writer(cljs_time.format.unparse(new cljs.core.Keyword(null,"date-time","date-time",177938180).cljs$core$IFn$_invoke$arity$1(cljs_time.format.formatters),obj__$1),writer,opts);
});

goog.date.DateTime.prototype.cljs$core$IPrintWithWriter$ = cljs.core.PROTOCOL_SENTINEL;

goog.date.DateTime.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (obj,writer,opts){
var obj__$1 = this;
cljs.core._write(writer,"#inst ");

return cljs.core.pr_writer(cljs_time.format.unparse(new cljs.core.Keyword(null,"date-time","date-time",177938180).cljs$core$IFn$_invoke$arity$1(cljs_time.format.formatters),obj__$1),writer,opts);
});

goog.date.Date.prototype.cljs$core$IPrintWithWriter$ = cljs.core.PROTOCOL_SENTINEL;

goog.date.Date.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (obj,writer,opts){
var obj__$1 = this;
cljs.core._write(writer,"#inst ");

return cljs.core.pr_writer(cljs_time.format.unparse(new cljs.core.Keyword(null,"date","date",-1463434462).cljs$core$IFn$_invoke$arity$1(cljs_time.format.formatters),obj__$1),writer,opts);
});

//# sourceMappingURL=cljs_time.instant.js.map
