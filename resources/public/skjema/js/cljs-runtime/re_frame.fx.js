goog.provide('re_frame.fx');
goog.require('cljs.core');
goog.require('re_frame.router');
goog.require('re_frame.db');
goog.require('re_frame.interceptor');
goog.require('re_frame.interop');
goog.require('re_frame.events');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
goog.require('re_frame.trace');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_((re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1 ? re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1(re_frame.fx.kind) : re_frame.registrar.kinds.call(null,re_frame.fx.kind)))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
/**
 * Register the given effect `handler` for the given `id`.
 * 
 *   `id` is keyword, often namespaced.
 *   `handler` is a side-effecting function which takes a single argument and whose return
 *   value is ignored.
 * 
 *   Example Use
 *   -----------
 * 
 *   First, registration ... associate `:effect2` with a handler.
 * 
 *   (reg-fx
 *   :effect2
 *   (fn [value]
 *      ... do something side-effect-y))
 * 
 *   Then, later, if an event handler were to return this effects map ...
 * 
 *   {...
 * :effect2  [1 2]}
 * 
 * ... then the `handler` `fn` we registered previously, using `reg-fx`, will be
 * called with an argument of `[1 2]`.
 */
re_frame.fx.reg_fx = (function re_frame$fx$reg_fx(id,handler){
return re_frame.registrar.register_handler(re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
if(re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__77778 = re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__77779 = re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));
re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__77779;

try{try{var seq__77780 = cljs.core.seq(new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__77781 = null;
var count__77782 = (0);
var i__77783 = (0);
while(true){
if((i__77783 < count__77782)){
var vec__77794 = chunk__77781.cljs$core$IIndexed$_nth$arity$2(null,i__77783);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77794,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77794,(1),null);
var temp__5733__auto___77860 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___77860)){
var effect_fn_77861 = temp__5733__auto___77860;
(effect_fn_77861.cljs$core$IFn$_invoke$arity$1 ? effect_fn_77861.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_77861.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__77862 = seq__77780;
var G__77863 = chunk__77781;
var G__77864 = count__77782;
var G__77865 = (i__77783 + (1));
seq__77780 = G__77862;
chunk__77781 = G__77863;
count__77782 = G__77864;
i__77783 = G__77865;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__77780);
if(temp__5735__auto__){
var seq__77780__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__77780__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__77780__$1);
var G__77866 = cljs.core.chunk_rest(seq__77780__$1);
var G__77867 = c__4550__auto__;
var G__77868 = cljs.core.count(c__4550__auto__);
var G__77869 = (0);
seq__77780 = G__77866;
chunk__77781 = G__77867;
count__77782 = G__77868;
i__77783 = G__77869;
continue;
} else {
var vec__77800 = cljs.core.first(seq__77780__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77800,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77800,(1),null);
var temp__5733__auto___77874 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___77874)){
var effect_fn_77878 = temp__5733__auto___77874;
(effect_fn_77878.cljs$core$IFn$_invoke$arity$1 ? effect_fn_77878.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_77878.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__77880 = cljs.core.next(seq__77780__$1);
var G__77881 = null;
var G__77882 = (0);
var G__77883 = (0);
seq__77780 = G__77880;
chunk__77781 = G__77881;
count__77782 = G__77882;
i__77783 = G__77883;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(re_frame.trace.is_trace_enabled_QMARK_()){
var end__77489__auto___77888 = re_frame.interop.now();
var duration__77490__auto___77889 = (end__77489__auto___77888 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__77490__auto___77889,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now()], 0)));

re_frame.trace.run_tracing_callbacks_BANG_(end__77489__auto___77888);
} else {
}
}}finally {re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__77778;
}} else {
var seq__77803 = cljs.core.seq(new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__77804 = null;
var count__77805 = (0);
var i__77806 = (0);
while(true){
if((i__77806 < count__77805)){
var vec__77818 = chunk__77804.cljs$core$IIndexed$_nth$arity$2(null,i__77806);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77818,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77818,(1),null);
var temp__5733__auto___77890 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___77890)){
var effect_fn_77891 = temp__5733__auto___77890;
(effect_fn_77891.cljs$core$IFn$_invoke$arity$1 ? effect_fn_77891.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_77891.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__77892 = seq__77803;
var G__77893 = chunk__77804;
var G__77894 = count__77805;
var G__77895 = (i__77806 + (1));
seq__77803 = G__77892;
chunk__77804 = G__77893;
count__77805 = G__77894;
i__77806 = G__77895;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__77803);
if(temp__5735__auto__){
var seq__77803__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__77803__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__77803__$1);
var G__77896 = cljs.core.chunk_rest(seq__77803__$1);
var G__77897 = c__4550__auto__;
var G__77898 = cljs.core.count(c__4550__auto__);
var G__77899 = (0);
seq__77803 = G__77896;
chunk__77804 = G__77897;
count__77805 = G__77898;
i__77806 = G__77899;
continue;
} else {
var vec__77821 = cljs.core.first(seq__77803__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77821,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77821,(1),null);
var temp__5733__auto___77900 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___77900)){
var effect_fn_77901 = temp__5733__auto___77900;
(effect_fn_77901.cljs$core$IFn$_invoke$arity$1 ? effect_fn_77901.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_77901.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__77902 = cljs.core.next(seq__77803__$1);
var G__77903 = null;
var G__77904 = (0);
var G__77905 = (0);
seq__77803 = G__77902;
chunk__77804 = G__77903;
count__77805 = G__77904;
i__77806 = G__77905;
continue;
}
} else {
return null;
}
}
break;
}
}
})], 0));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
var seq__77824 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__77825 = null;
var count__77826 = (0);
var i__77827 = (0);
while(true){
if((i__77827 < count__77826)){
var map__77836 = chunk__77825.cljs$core$IIndexed$_nth$arity$2(null,i__77827);
var map__77836__$1 = (((((!((map__77836 == null))))?(((((map__77836.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__77836.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__77836):map__77836);
var effect = map__77836__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77836__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77836__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
re_frame.interop.set_timeout_BANG_(((function (seq__77824,chunk__77825,count__77826,i__77827,map__77836,map__77836__$1,effect,ms,dispatch){
return (function (){
return re_frame.router.dispatch(dispatch);
});})(seq__77824,chunk__77825,count__77826,i__77827,map__77836,map__77836__$1,effect,ms,dispatch))
,ms);
}


var G__77908 = seq__77824;
var G__77909 = chunk__77825;
var G__77910 = count__77826;
var G__77911 = (i__77827 + (1));
seq__77824 = G__77908;
chunk__77825 = G__77909;
count__77826 = G__77910;
i__77827 = G__77911;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__77824);
if(temp__5735__auto__){
var seq__77824__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__77824__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__77824__$1);
var G__77912 = cljs.core.chunk_rest(seq__77824__$1);
var G__77913 = c__4550__auto__;
var G__77914 = cljs.core.count(c__4550__auto__);
var G__77915 = (0);
seq__77824 = G__77912;
chunk__77825 = G__77913;
count__77826 = G__77914;
i__77827 = G__77915;
continue;
} else {
var map__77838 = cljs.core.first(seq__77824__$1);
var map__77838__$1 = (((((!((map__77838 == null))))?(((((map__77838.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__77838.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__77838):map__77838);
var effect = map__77838__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77838__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77838__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
re_frame.interop.set_timeout_BANG_(((function (seq__77824,chunk__77825,count__77826,i__77827,map__77838,map__77838__$1,effect,ms,dispatch,seq__77824__$1,temp__5735__auto__){
return (function (){
return re_frame.router.dispatch(dispatch);
});})(seq__77824,chunk__77825,count__77826,i__77827,map__77838,map__77838__$1,effect,ms,dispatch,seq__77824__$1,temp__5735__auto__))
,ms);
}


var G__77918 = cljs.core.next(seq__77824__$1);
var G__77919 = null;
var G__77920 = (0);
var G__77921 = (0);
seq__77824 = G__77918;
chunk__77825 = G__77919;
count__77826 = G__77920;
i__77827 = G__77921;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if((!(cljs.core.vector_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value], 0));
} else {
return re_frame.router.dispatch(value);
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if((!(cljs.core.sequential_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-n value. Expected a collection, but got:",value], 0));
} else {
var seq__77845 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__77846 = null;
var count__77847 = (0);
var i__77848 = (0);
while(true){
if((i__77848 < count__77847)){
var event = chunk__77846.cljs$core$IIndexed$_nth$arity$2(null,i__77848);
re_frame.router.dispatch(event);


var G__77923 = seq__77845;
var G__77924 = chunk__77846;
var G__77925 = count__77847;
var G__77926 = (i__77848 + (1));
seq__77845 = G__77923;
chunk__77846 = G__77924;
count__77847 = G__77925;
i__77848 = G__77926;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__77845);
if(temp__5735__auto__){
var seq__77845__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__77845__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__77845__$1);
var G__77928 = cljs.core.chunk_rest(seq__77845__$1);
var G__77929 = c__4550__auto__;
var G__77930 = cljs.core.count(c__4550__auto__);
var G__77931 = (0);
seq__77845 = G__77928;
chunk__77846 = G__77929;
count__77847 = G__77930;
i__77848 = G__77931;
continue;
} else {
var event = cljs.core.first(seq__77845__$1);
re_frame.router.dispatch(event);


var G__77932 = cljs.core.next(seq__77845__$1);
var G__77933 = null;
var G__77934 = (0);
var G__77935 = (0);
seq__77845 = G__77932;
chunk__77846 = G__77933;
count__77847 = G__77934;
i__77848 = G__77935;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.cljs$core$IFn$_invoke$arity$2(re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_(value)){
var seq__77851 = cljs.core.seq(value);
var chunk__77852 = null;
var count__77853 = (0);
var i__77854 = (0);
while(true){
if((i__77854 < count__77853)){
var event = chunk__77852.cljs$core$IIndexed$_nth$arity$2(null,i__77854);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__77936 = seq__77851;
var G__77937 = chunk__77852;
var G__77938 = count__77853;
var G__77939 = (i__77854 + (1));
seq__77851 = G__77936;
chunk__77852 = G__77937;
count__77853 = G__77938;
i__77854 = G__77939;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__77851);
if(temp__5735__auto__){
var seq__77851__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__77851__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__77851__$1);
var G__77940 = cljs.core.chunk_rest(seq__77851__$1);
var G__77941 = c__4550__auto__;
var G__77942 = cljs.core.count(c__4550__auto__);
var G__77943 = (0);
seq__77851 = G__77940;
chunk__77852 = G__77941;
count__77853 = G__77942;
i__77854 = G__77943;
continue;
} else {
var event = cljs.core.first(seq__77851__$1);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__77944 = cljs.core.next(seq__77851__$1);
var G__77945 = null;
var G__77946 = (0);
var G__77947 = (0);
seq__77851 = G__77944;
chunk__77852 = G__77945;
count__77853 = G__77946;
i__77854 = G__77947;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return (clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(value) : clear_event.call(null,value));
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if((!((cljs.core.deref(re_frame.db.app_db) === value)))){
return cljs.core.reset_BANG_(re_frame.db.app_db,value);
} else {
return null;
}
}));

//# sourceMappingURL=re_frame.fx.js.map
