goog.provide('thi.ng.typedarrays.core');
goog.require('cljs.core');
/**
 * Returns true if JS runtime supports typed arrays
 */
thi.ng.typedarrays.core.typed_arrays_supported_QMARK_ = (function thi$ng$typedarrays$core$typed_arrays_supported_QMARK_(){
return (!(((window["ArrayBuffer"]) == null)));
});
thi.ng.typedarrays.core.array_types = cljs.core.PersistentHashMap.fromArrays(["Float64Array","Uint8Array","Int8Array","Uint8ClampedArray","Uint16Array","Int16Array","Uint32Array","Float32Array","Int32Array"],[new cljs.core.Keyword(null,"float64","float64",1881838306),new cljs.core.Keyword(null,"uint8","uint8",956521151),new cljs.core.Keyword(null,"int8","int8",-1834023920),new cljs.core.Keyword(null,"uint8-clamped","uint8-clamped",1439331936),new cljs.core.Keyword(null,"uint16","uint16",-588869202),new cljs.core.Keyword(null,"int16","int16",-188764863),new cljs.core.Keyword(null,"uint32","uint32",-418789486),new cljs.core.Keyword(null,"float32","float32",-2119815775),new cljs.core.Keyword(null,"int32","int32",1718804896)]);
/**
 * Returns truthy value if the given arg is a typed array instance
 */
thi.ng.typedarrays.core.typed_array_QMARK_ = (function thi$ng$typedarrays$core$typed_array_QMARK_(x){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("object",goog.typeOf(x))){
if(typeof x.BYTES_PER_ELEMENT === 'number'){
return x.buffer;
} else {
return null;
}
} else {
return null;
}
});
thi.ng.typedarrays.core.array_type = (function thi$ng$typedarrays$core$array_type(x){
if(cljs.core.array_QMARK_(x)){
return new cljs.core.Keyword(null,"array","array",-2080713842);
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(x))){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(thi.ng.typedarrays.core.array_types,cljs.core.first(cljs.core.re_find(/((Uint|Int|Float)\d+(Clamped)?Array)/,cljs.core.str.cljs$core$IFn$_invoke$arity$1(x.constructor))));
} else {
return null;
}
}
});
/**
 * Creates a native Int8Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.int8 = (function thi$ng$typedarrays$core$int8(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Int8Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Int8Array(len));
var i_92177 = (0);
var coll_92178 = size_or_coll;
while(true){
if((i_92177 < len)){
(buf[i_92177] = cljs.core.first(coll_92178));

var G__92179 = (i_92177 + (1));
var G__92180 = cljs.core.next(coll_92178);
i_92177 = G__92179;
coll_92178 = G__92180;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Uint8Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint8 = (function thi$ng$typedarrays$core$uint8(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint8Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Uint8Array(len));
var i_92181 = (0);
var coll_92182 = size_or_coll;
while(true){
if((i_92181 < len)){
(buf[i_92181] = cljs.core.first(coll_92182));

var G__92183 = (i_92181 + (1));
var G__92184 = cljs.core.next(coll_92182);
i_92181 = G__92183;
coll_92182 = G__92184;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Uint8ClampedArray of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint8_clamped = (function thi$ng$typedarrays$core$uint8_clamped(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint8ClampedArray(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Uint8ClampedArray(len));
var i_92188 = (0);
var coll_92189 = size_or_coll;
while(true){
if((i_92188 < len)){
(buf[i_92188] = cljs.core.first(coll_92189));

var G__92190 = (i_92188 + (1));
var G__92191 = cljs.core.next(coll_92189);
i_92188 = G__92190;
coll_92189 = G__92191;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Int16Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.int16 = (function thi$ng$typedarrays$core$int16(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Int16Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Int16Array(len));
var i_92199 = (0);
var coll_92200 = size_or_coll;
while(true){
if((i_92199 < len)){
(buf[i_92199] = cljs.core.first(coll_92200));

var G__92201 = (i_92199 + (1));
var G__92202 = cljs.core.next(coll_92200);
i_92199 = G__92201;
coll_92200 = G__92202;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Uint16Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint16 = (function thi$ng$typedarrays$core$uint16(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint16Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Uint16Array(len));
var i_92203 = (0);
var coll_92204 = size_or_coll;
while(true){
if((i_92203 < len)){
(buf[i_92203] = cljs.core.first(coll_92204));

var G__92205 = (i_92203 + (1));
var G__92206 = cljs.core.next(coll_92204);
i_92203 = G__92205;
coll_92204 = G__92206;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Int32Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.int32 = (function thi$ng$typedarrays$core$int32(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Int32Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Int32Array(len));
var i_92215 = (0);
var coll_92216 = size_or_coll;
while(true){
if((i_92215 < len)){
(buf[i_92215] = cljs.core.first(coll_92216));

var G__92221 = (i_92215 + (1));
var G__92222 = cljs.core.next(coll_92216);
i_92215 = G__92221;
coll_92216 = G__92222;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Uint32Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint32 = (function thi$ng$typedarrays$core$uint32(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint32Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Uint32Array(len));
var i_92229 = (0);
var coll_92230 = size_or_coll;
while(true){
if((i_92229 < len)){
(buf[i_92229] = cljs.core.first(coll_92230));

var G__92232 = (i_92229 + (1));
var G__92233 = cljs.core.next(coll_92230);
i_92229 = G__92232;
coll_92230 = G__92233;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Float32Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.float32 = (function thi$ng$typedarrays$core$float32(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Float32Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Float32Array(len));
var i_92234 = (0);
var coll_92235 = size_or_coll;
while(true){
if((i_92234 < len)){
(buf[i_92234] = cljs.core.first(coll_92235));

var G__92236 = (i_92234 + (1));
var G__92237 = cljs.core.next(coll_92235);
i_92234 = G__92236;
coll_92235 = G__92237;
continue;
} else {
}
break;
}

return buf;

}
}
});
/**
 * Creates a native Float64Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.float64 = (function thi$ng$typedarrays$core$float64(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Float64Array(size_or_coll));
} else {
if(cljs.core.truth_(thi.ng.typedarrays.core.typed_array_QMARK_(size_or_coll))){
return size_or_coll;
} else {
var len = cljs.core.count(size_or_coll);
var buf = (new Float64Array(len));
var i_92241 = (0);
var coll_92242 = size_or_coll;
while(true){
if((i_92241 < len)){
(buf[i_92241] = cljs.core.first(coll_92242));

var G__92247 = (i_92241 + (1));
var G__92248 = cljs.core.next(coll_92242);
i_92241 = G__92247;
coll_92242 = G__92248;
continue;
} else {
}
break;
}

return buf;

}
}
});

//# sourceMappingURL=thi.ng.typedarrays.core.js.map
