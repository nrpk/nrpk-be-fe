goog.provide('instaparse.util');
goog.require('cljs.core');
instaparse.util.throw_runtime_exception = (function instaparse$util$throw_runtime_exception(var_args){
var args__4736__auto__ = [];
var len__4730__auto___92454 = arguments.length;
var i__4731__auto___92459 = (0);
while(true){
if((i__4731__auto___92459 < len__4730__auto___92454)){
args__4736__auto__.push((arguments[i__4731__auto___92459]));

var G__92460 = (i__4731__auto___92459 + (1));
i__4731__auto___92459 = G__92460;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return instaparse.util.throw_runtime_exception.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

instaparse.util.throw_runtime_exception.cljs$core$IFn$_invoke$arity$variadic = (function (message){
var text = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,message);
throw text;
});

instaparse.util.throw_runtime_exception.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
instaparse.util.throw_runtime_exception.cljs$lang$applyTo = (function (seq92431){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq92431));
});

instaparse.util.throw_illegal_argument_exception = (function instaparse$util$throw_illegal_argument_exception(var_args){
var args__4736__auto__ = [];
var len__4730__auto___92470 = arguments.length;
var i__4731__auto___92471 = (0);
while(true){
if((i__4731__auto___92471 < len__4730__auto___92470)){
args__4736__auto__.push((arguments[i__4731__auto___92471]));

var G__92472 = (i__4731__auto___92471 + (1));
i__4731__auto___92471 = G__92472;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return instaparse.util.throw_illegal_argument_exception.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

instaparse.util.throw_illegal_argument_exception.cljs$core$IFn$_invoke$arity$variadic = (function (message){
var text = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,message);
throw text;
});

instaparse.util.throw_illegal_argument_exception.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
instaparse.util.throw_illegal_argument_exception.cljs$lang$applyTo = (function (seq92434){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq92434));
});

instaparse.util.regexp_flags = (function instaparse$util$regexp_flags(re){
var G__92440 = "";
var G__92440__$1 = (cljs.core.truth_(re.ignoreCase)?[G__92440,"i"].join(''):G__92440);
var G__92440__$2 = (cljs.core.truth_(re.multiline)?[G__92440__$1,"m"].join(''):G__92440__$1);
if(cljs.core.truth_(re.unicode)){
return [G__92440__$2,"u"].join('');
} else {
return G__92440__$2;
}
});

//# sourceMappingURL=instaparse.util.js.map
