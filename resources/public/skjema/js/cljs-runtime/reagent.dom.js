goog.provide('reagent.dom');
goog.require('cljs.core');
var module$node_modules$react_dom$index=shadow.js.require("module$node_modules$react_dom$index", {});
goog.require('reagent.impl.util');
goog.require('reagent.impl.template');
goog.require('reagent.impl.batching');
goog.require('reagent.ratom');
goog.require('reagent.debug');
goog.require('reagent.interop');
if((typeof reagent !== 'undefined') && (typeof reagent.dom !== 'undefined') && (typeof reagent.dom.imported !== 'undefined')){
} else {
reagent.dom.imported = null;
}
if((typeof reagent !== 'undefined') && (typeof reagent.dom !== 'undefined') && (typeof reagent.dom.roots !== 'undefined')){
} else {
reagent.dom.roots = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
reagent.dom.unmount_comp = (function reagent$dom$unmount_comp(container){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(reagent.dom.roots,cljs.core.dissoc,container);

return module$node_modules$react_dom$index.unmountComponentAtNode(container);
});
reagent.dom.render_comp = (function reagent$dom$render_comp(comp,container,callback){
var _STAR_always_update_STAR__orig_val__77114 = reagent.impl.util._STAR_always_update_STAR_;
var _STAR_always_update_STAR__temp_val__77115 = true;
reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__temp_val__77115;

try{var G__77117 = (comp.cljs$core$IFn$_invoke$arity$0 ? comp.cljs$core$IFn$_invoke$arity$0() : comp.call(null));
var G__77118 = container;
var G__77119 = ((function (G__77117,G__77118,_STAR_always_update_STAR__orig_val__77114,_STAR_always_update_STAR__temp_val__77115){
return (function (){
var _STAR_always_update_STAR__orig_val__77123 = reagent.impl.util._STAR_always_update_STAR_;
var _STAR_always_update_STAR__temp_val__77124 = false;
reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__temp_val__77124;

try{cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(reagent.dom.roots,cljs.core.assoc,container,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [comp,container], null));

reagent.impl.batching.flush_after_render();

if((!((callback == null)))){
return (callback.cljs$core$IFn$_invoke$arity$0 ? callback.cljs$core$IFn$_invoke$arity$0() : callback.call(null));
} else {
return null;
}
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__orig_val__77123;
}});})(G__77117,G__77118,_STAR_always_update_STAR__orig_val__77114,_STAR_always_update_STAR__temp_val__77115))
;
return module$node_modules$react_dom$index.render(G__77117,G__77118,G__77119);
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__orig_val__77114;
}});
reagent.dom.re_render_component = (function reagent$dom$re_render_component(comp,container){
return reagent.dom.render_comp(comp,container,null);
});
/**
 * Render a Reagent component into the DOM. The first argument may be
 *   either a vector (using Reagent's Hiccup syntax), or a React element. The second argument should be a DOM node.
 * 
 *   Optionally takes a callback that is called when the component is in place.
 * 
 *   Returns the mounted component instance.
 */
reagent.dom.render = (function reagent$dom$render(var_args){
var G__77129 = arguments.length;
switch (G__77129) {
case 2:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$2 = (function (comp,container){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3(comp,container,null);
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$3 = (function (comp,container,callback){
reagent.ratom.flush_BANG_();

var f = (function (){
return reagent.impl.template.as_element(((cljs.core.fn_QMARK_(comp))?(comp.cljs$core$IFn$_invoke$arity$0 ? comp.cljs$core$IFn$_invoke$arity$0() : comp.call(null)):comp));
});
return reagent.dom.render_comp(f,container,callback);
});

reagent.dom.render.cljs$lang$maxFixedArity = 3;

reagent.dom.unmount_component_at_node = (function reagent$dom$unmount_component_at_node(container){
return reagent.dom.unmount_comp(container);
});
/**
 * Returns the root DOM node of a mounted component.
 */
reagent.dom.dom_node = (function reagent$dom$dom_node(this$){
return module$node_modules$react_dom$index.findDOMNode(this$);
});
reagent.impl.template.find_dom_node = reagent.dom.dom_node;
/**
 * Force re-rendering of all mounted Reagent components. This is
 *   probably only useful in a development environment, when you want to
 *   update components in response to some dynamic changes to code.
 * 
 *   Note that force-update-all may not update root components. This
 *   happens if a component 'foo' is mounted with `(render [foo])` (since
 *   functions are passed by value, and not by reference, in
 *   ClojureScript). To get around this you'll have to introduce a layer
 *   of indirection, for example by using `(render [#'foo])` instead.
 */
reagent.dom.force_update_all = (function reagent$dom$force_update_all(){
reagent.ratom.flush_BANG_();

var seq__77131_77148 = cljs.core.seq(cljs.core.vals(cljs.core.deref(reagent.dom.roots)));
var chunk__77132_77149 = null;
var count__77133_77150 = (0);
var i__77134_77151 = (0);
while(true){
if((i__77134_77151 < count__77133_77150)){
var v_77155 = chunk__77132_77149.cljs$core$IIndexed$_nth$arity$2(null,i__77134_77151);
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(reagent.dom.re_render_component,v_77155);


var G__77156 = seq__77131_77148;
var G__77157 = chunk__77132_77149;
var G__77158 = count__77133_77150;
var G__77159 = (i__77134_77151 + (1));
seq__77131_77148 = G__77156;
chunk__77132_77149 = G__77157;
count__77133_77150 = G__77158;
i__77134_77151 = G__77159;
continue;
} else {
var temp__5735__auto___77160 = cljs.core.seq(seq__77131_77148);
if(temp__5735__auto___77160){
var seq__77131_77162__$1 = temp__5735__auto___77160;
if(cljs.core.chunked_seq_QMARK_(seq__77131_77162__$1)){
var c__4550__auto___77163 = cljs.core.chunk_first(seq__77131_77162__$1);
var G__77165 = cljs.core.chunk_rest(seq__77131_77162__$1);
var G__77166 = c__4550__auto___77163;
var G__77167 = cljs.core.count(c__4550__auto___77163);
var G__77168 = (0);
seq__77131_77148 = G__77165;
chunk__77132_77149 = G__77166;
count__77133_77150 = G__77167;
i__77134_77151 = G__77168;
continue;
} else {
var v_77169 = cljs.core.first(seq__77131_77162__$1);
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(reagent.dom.re_render_component,v_77169);


var G__77170 = cljs.core.next(seq__77131_77162__$1);
var G__77171 = null;
var G__77172 = (0);
var G__77173 = (0);
seq__77131_77148 = G__77170;
chunk__77132_77149 = G__77171;
count__77133_77150 = G__77172;
i__77134_77151 = G__77173;
continue;
}
} else {
}
}
break;
}

return "Updated";
});

//# sourceMappingURL=reagent.dom.js.map
