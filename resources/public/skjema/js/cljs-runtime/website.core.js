goog.provide('website.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('re_frame.core');
goog.require('re_frame.db');
goog.require('cljs.pprint');
goog.require('website.keyboard');
goog.require('website.page_header');
goog.require('website.editors');
goog.require('website.pure.utils');
goog.require('reitit.frontend');
goog.require('reitit.frontend.easy');
goog.require('reitit.frontend.controllers');
goog.require('reitit.coercion');
goog.require('reitit.coercion.spec');
goog.require('website.pages.login');
goog.require('website.pages.kurs');
goog.require('website.pages.colorsamples');
goog.require('website.pages.instaparser');
goog.require('playground.core1');
goog.require('nv.core');
goog.require('re_frisk.core');
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.core","some-result","website.core/some-result",-1556154747),(function (db,p__94386){
var vec__94387 = p__94386;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__94387,(0),null);
var arg = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__94387,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"some-result","some-result",399944115),arg);
}));
var G__94390_94427 = new cljs.core.Keyword("website.core","some-result","website.core/some-result",-1556154747);
var G__94391_94428 = ((function (G__94390_94427){
return (function (db){
return new cljs.core.Keyword(null,"some-result","some-result",399944115).cljs$core$IFn$_invoke$arity$2(db,new cljs.core.Keyword(null,"empty","empty",767870958));
});})(G__94390_94427))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__94390_94427,G__94391_94428) : re_frame.core.reg_sub.call(null,G__94390_94427,G__94391_94428));
website.core.render_result = (function website$core$render_result(){
var data = cljs.core.deref((function (){var G__94392 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.core","some-result","website.core/some-result",-1556154747)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__94392) : re_frame.core.subscribe.call(null,G__94392));
})());
return ((function (data){
return (function (){
return website.pure.utils.inset_x.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.my-1.debug.border","div.my-1.debug.border",-298159396),((cljs.core.map_QMARK_(data))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.core.render_result,data], null):((cljs.core.vector_QMARK_(data))?new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"SOME VECTOR ",cljs.core.count(data),(function (){var seq__94393 = cljs.core.seq(data);
var chunk__94394 = null;
var count__94395 = (0);
var i__94396 = (0);
while(true){
if((i__94396 < count__94395)){
var e = chunk__94394.cljs$core$IIndexed$_nth$arity$2(null,i__94396);
new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-blue-500","div.bg-blue-500",1828043265),e], null);


var G__94429 = seq__94393;
var G__94430 = chunk__94394;
var G__94431 = count__94395;
var G__94432 = (i__94396 + (1));
seq__94393 = G__94429;
chunk__94394 = G__94430;
count__94395 = G__94431;
i__94396 = G__94432;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__94393);
if(temp__5735__auto__){
var seq__94393__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__94393__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__94393__$1);
var G__94433 = cljs.core.chunk_rest(seq__94393__$1);
var G__94434 = c__4550__auto__;
var G__94435 = cljs.core.count(c__4550__auto__);
var G__94436 = (0);
seq__94393 = G__94433;
chunk__94394 = G__94434;
count__94395 = G__94435;
i__94396 = G__94436;
continue;
} else {
var e = cljs.core.first(seq__94393__$1);
new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-blue-500","div.bg-blue-500",1828043265),e], null);


var G__94437 = cljs.core.next(seq__94393__$1);
var G__94438 = null;
var G__94439 = (0);
var G__94440 = (0);
seq__94393 = G__94437;
chunk__94394 = G__94438;
count__94395 = G__94439;
i__94396 = G__94440;
continue;
}
} else {
return null;
}
}
break;
}
})()], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"ELSE ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(data)], null)
)),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xl.bg-red-500.text-yellow.p-2.inline","p.text-xl.bg-red-500.text-yellow.p-2.inline",713903988),"s= ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(data)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#result.h-16.text-white.cm-s-default.mono.inline","div#result.h-16.text-white.cm-s-default.mono.inline",-1172844162),"result here"], null)], 0));
});
;})(data))
});
website.core.home_page = (function website$core$home_page(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.pt-64.bg-gray-200","div.pt-64.bg-gray-200",107127839),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.p-1.text-gray-600.text-xs.font-thin","div.p-1.text-gray-600.text-xs.font-thin",1782594455),"Oversikt, forskjellige tall-data her, +arc"], null)], null);
});
website.core.about = (function website$core$about(match){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-white.mt-24","div.text-white.mt-24",947609064),"ABOUT",cljs.core.str.cljs$core$IFn$_invoke$arity$1(match)], null);
});
website.core.items_page2 = (function website$core$items_page2(match){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-white","div.text-white",1926076215),"HERE ARE SOME ITEMS"], null);
});
website.core.current_page = (function website$core$current_page(){
return null;
});
if((typeof website !== 'undefined') && (typeof website.core !== 'undefined') && (typeof website.core.match !== 'undefined')){
} else {
website.core.match = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
website.core.routes = new cljs.core.PersistentVector(null, 9, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","home","website.core/home",-1607046767),new cljs.core.Keyword(null,"view","view",1247994814),website.core.home_page,new cljs.core.Keyword(null,"link-text","link-text",224432076),"Home",new cljs.core.Keyword(null,"controllers","controllers",-1120410624),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"start","start",-355208981),(function() { 
var G__94441__delegate = function (params){
return console.log("Entering home page");
};
var G__94441 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94442__i = 0, G__94442__a = new Array(arguments.length -  0);
while (G__94442__i < G__94442__a.length) {G__94442__a[G__94442__i] = arguments[G__94442__i + 0]; ++G__94442__i;}
  params = new cljs.core.IndexedSeq(G__94442__a,0,null);
} 
return G__94441__delegate.call(this,params);};
G__94441.cljs$lang$maxFixedArity = 0;
G__94441.cljs$lang$applyTo = (function (arglist__94443){
var params = cljs.core.seq(arglist__94443);
return G__94441__delegate(params);
});
G__94441.cljs$core$IFn$_invoke$arity$variadic = G__94441__delegate;
return G__94441;
})()
,new cljs.core.Keyword(null,"stop","stop",-2140911342),(function() { 
var G__94444__delegate = function (params){
return console.log("Leaving home page");
};
var G__94444 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94445__i = 0, G__94445__a = new Array(arguments.length -  0);
while (G__94445__i < G__94445__a.length) {G__94445__a[G__94445__i] = arguments[G__94445__i + 0]; ++G__94445__i;}
  params = new cljs.core.IndexedSeq(G__94445__a,0,null);
} 
return G__94444__delegate.call(this,params);};
G__94444.cljs$lang$maxFixedArity = 0;
G__94444.cljs$lang$applyTo = (function (arglist__94446){
var params = cljs.core.seq(arglist__94446);
return G__94444__delegate(params);
});
G__94444.cljs$core$IFn$_invoke$arity$variadic = G__94444__delegate;
return G__94444;
})()
], null)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["instaparser",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","instaparser","website.core/instaparser",-1480129989),new cljs.core.Keyword(null,"view","view",1247994814),(function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-20","div.mt-20",1983464913),website.pages.instaparser.render()], null);
}),new cljs.core.Keyword(null,"link-text","link-text",224432076),"instaparser",new cljs.core.Keyword(null,"controllers","controllers",-1120410624),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"start","start",-355208981),(function() { 
var G__94447__delegate = function (params){
return console.log("Entering instaparser");
};
var G__94447 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94448__i = 0, G__94448__a = new Array(arguments.length -  0);
while (G__94448__i < G__94448__a.length) {G__94448__a[G__94448__i] = arguments[G__94448__i + 0]; ++G__94448__i;}
  params = new cljs.core.IndexedSeq(G__94448__a,0,null);
} 
return G__94447__delegate.call(this,params);};
G__94447.cljs$lang$maxFixedArity = 0;
G__94447.cljs$lang$applyTo = (function (arglist__94449){
var params = cljs.core.seq(arglist__94449);
return G__94447__delegate(params);
});
G__94447.cljs$core$IFn$_invoke$arity$variadic = G__94447__delegate;
return G__94447;
})()
,new cljs.core.Keyword(null,"stop","stop",-2140911342),(function() { 
var G__94450__delegate = function (params){
return console.log("Leaving instaparser");
};
var G__94450 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94451__i = 0, G__94451__a = new Array(arguments.length -  0);
while (G__94451__i < G__94451__a.length) {G__94451__a[G__94451__i] = arguments[G__94451__i + 0]; ++G__94451__i;}
  params = new cljs.core.IndexedSeq(G__94451__a,0,null);
} 
return G__94450__delegate.call(this,params);};
G__94450.cljs$lang$maxFixedArity = 0;
G__94450.cljs$lang$applyTo = (function (arglist__94452){
var params = cljs.core.seq(arglist__94452);
return G__94450__delegate(params);
});
G__94450.cljs$core$IFn$_invoke$arity$variadic = G__94450__delegate;
return G__94450;
})()
], null)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["Darkside",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","darkside","website.core/darkside",98569851),new cljs.core.Keyword(null,"view","view",1247994814),website.core.home_page], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["sub-page1",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","sub-page1","website.core/sub-page1",-1225116075),new cljs.core.Keyword(null,"view","view",1247994814),website.core.items_page2,new cljs.core.Keyword(null,"link-text","link-text",224432076),"Sub page 1",new cljs.core.Keyword(null,"controllers","controllers",-1120410624),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"start","start",-355208981),(function() { 
var G__94453__delegate = function (params){
return console.log("Entering sub-page 1");
};
var G__94453 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94454__i = 0, G__94454__a = new Array(arguments.length -  0);
while (G__94454__i < G__94454__a.length) {G__94454__a[G__94454__i] = arguments[G__94454__i + 0]; ++G__94454__i;}
  params = new cljs.core.IndexedSeq(G__94454__a,0,null);
} 
return G__94453__delegate.call(this,params);};
G__94453.cljs$lang$maxFixedArity = 0;
G__94453.cljs$lang$applyTo = (function (arglist__94455){
var params = cljs.core.seq(arglist__94455);
return G__94453__delegate(params);
});
G__94453.cljs$core$IFn$_invoke$arity$variadic = G__94453__delegate;
return G__94453;
})()
,new cljs.core.Keyword(null,"stop","stop",-2140911342),(function() { 
var G__94456__delegate = function (params){
return console.log("Leaving sub-page 1");
};
var G__94456 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94457__i = 0, G__94457__a = new Array(arguments.length -  0);
while (G__94457__i < G__94457__a.length) {G__94457__a[G__94457__i] = arguments[G__94457__i + 0]; ++G__94457__i;}
  params = new cljs.core.IndexedSeq(G__94457__a,0,null);
} 
return G__94456__delegate.call(this,params);};
G__94456.cljs$lang$maxFixedArity = 0;
G__94456.cljs$lang$applyTo = (function (arglist__94458){
var params = cljs.core.seq(arglist__94458);
return G__94456__delegate(params);
});
G__94456.cljs$core$IFn$_invoke$arity$variadic = G__94456__delegate;
return G__94456;
})()
], null)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["about",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","about","website.core/about",1598500653),new cljs.core.Keyword(null,"view","view",1247994814),website.core.about,new cljs.core.Keyword(null,"link-text","link-text",224432076),"about",new cljs.core.Keyword(null,"controllers","controllers",-1120410624),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"start","start",-355208981),(function() { 
var G__94459__delegate = function (params){
return console.log("Entering about");
};
var G__94459 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94460__i = 0, G__94460__a = new Array(arguments.length -  0);
while (G__94460__i < G__94460__a.length) {G__94460__a[G__94460__i] = arguments[G__94460__i + 0]; ++G__94460__i;}
  params = new cljs.core.IndexedSeq(G__94460__a,0,null);
} 
return G__94459__delegate.call(this,params);};
G__94459.cljs$lang$maxFixedArity = 0;
G__94459.cljs$lang$applyTo = (function (arglist__94461){
var params = cljs.core.seq(arglist__94461);
return G__94459__delegate(params);
});
G__94459.cljs$core$IFn$_invoke$arity$variadic = G__94459__delegate;
return G__94459;
})()
,new cljs.core.Keyword(null,"stop","stop",-2140911342),(function() { 
var G__94462__delegate = function (params){
return console.log("Leaving about");
};
var G__94462 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94463__i = 0, G__94463__a = new Array(arguments.length -  0);
while (G__94463__i < G__94463__a.length) {G__94463__a[G__94463__i] = arguments[G__94463__i + 0]; ++G__94463__i;}
  params = new cljs.core.IndexedSeq(G__94463__a,0,null);
} 
return G__94462__delegate.call(this,params);};
G__94462.cljs$lang$maxFixedArity = 0;
G__94462.cljs$lang$applyTo = (function (arglist__94464){
var params = cljs.core.seq(arglist__94464);
return G__94462__delegate(params);
});
G__94462.cljs$core$IFn$_invoke$arity$variadic = G__94462__delegate;
return G__94462;
})()
], null)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["login",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","login","website.core/login",-809327047),new cljs.core.Keyword(null,"view","view",1247994814),website.pages.login.render,new cljs.core.Keyword(null,"link-text","link-text",224432076),"login",new cljs.core.Keyword(null,"controllers","controllers",-1120410624),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"start","start",-355208981),(function() { 
var G__94465__delegate = function (params){
return console.log("Entering login");
};
var G__94465 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94466__i = 0, G__94466__a = new Array(arguments.length -  0);
while (G__94466__i < G__94466__a.length) {G__94466__a[G__94466__i] = arguments[G__94466__i + 0]; ++G__94466__i;}
  params = new cljs.core.IndexedSeq(G__94466__a,0,null);
} 
return G__94465__delegate.call(this,params);};
G__94465.cljs$lang$maxFixedArity = 0;
G__94465.cljs$lang$applyTo = (function (arglist__94467){
var params = cljs.core.seq(arglist__94467);
return G__94465__delegate(params);
});
G__94465.cljs$core$IFn$_invoke$arity$variadic = G__94465__delegate;
return G__94465;
})()
,new cljs.core.Keyword(null,"stop","stop",-2140911342),(function() { 
var G__94468__delegate = function (params){
return console.log("Leaving login");
};
var G__94468 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94469__i = 0, G__94469__a = new Array(arguments.length -  0);
while (G__94469__i < G__94469__a.length) {G__94469__a[G__94469__i] = arguments[G__94469__i + 0]; ++G__94469__i;}
  params = new cljs.core.IndexedSeq(G__94469__a,0,null);
} 
return G__94468__delegate.call(this,params);};
G__94468.cljs$lang$maxFixedArity = 0;
G__94468.cljs$lang$applyTo = (function (arglist__94470){
var params = cljs.core.seq(arglist__94470);
return G__94468__delegate(params);
});
G__94468.cljs$core$IFn$_invoke$arity$variadic = G__94468__delegate;
return G__94468;
})()
], null)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["colorsamples",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","colorsamples","website.core/colorsamples",-1584068926),new cljs.core.Keyword(null,"view","view",1247994814),website.pages.colorsamples.render,new cljs.core.Keyword(null,"link-text","link-text",224432076),"colorsamples",new cljs.core.Keyword(null,"controllers","controllers",-1120410624),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"start","start",-355208981),(function() { 
var G__94471__delegate = function (params){
return console.log("Entering colorsamples");
};
var G__94471 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94472__i = 0, G__94472__a = new Array(arguments.length -  0);
while (G__94472__i < G__94472__a.length) {G__94472__a[G__94472__i] = arguments[G__94472__i + 0]; ++G__94472__i;}
  params = new cljs.core.IndexedSeq(G__94472__a,0,null);
} 
return G__94471__delegate.call(this,params);};
G__94471.cljs$lang$maxFixedArity = 0;
G__94471.cljs$lang$applyTo = (function (arglist__94473){
var params = cljs.core.seq(arglist__94473);
return G__94471__delegate(params);
});
G__94471.cljs$core$IFn$_invoke$arity$variadic = G__94471__delegate;
return G__94471;
})()
,new cljs.core.Keyword(null,"stop","stop",-2140911342),(function() { 
var G__94474__delegate = function (params){
return console.log("Leaving colorsamples");
};
var G__94474 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94475__i = 0, G__94475__a = new Array(arguments.length -  0);
while (G__94475__i < G__94475__a.length) {G__94475__a[G__94475__i] = arguments[G__94475__i + 0]; ++G__94475__i;}
  params = new cljs.core.IndexedSeq(G__94475__a,0,null);
} 
return G__94474__delegate.call(this,params);};
G__94474.cljs$lang$maxFixedArity = 0;
G__94474.cljs$lang$applyTo = (function (arglist__94476){
var params = cljs.core.seq(arglist__94476);
return G__94474__delegate(params);
});
G__94474.cljs$core$IFn$_invoke$arity$variadic = G__94474__delegate;
return G__94474;
})()
], null)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["kurs",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword("website.core","kurs","website.core/kurs",-266572828),new cljs.core.Keyword(null,"view","view",1247994814),website.pages.kurs.render,new cljs.core.Keyword(null,"link-text","link-text",224432076),"kurs",new cljs.core.Keyword(null,"controllers","controllers",-1120410624),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"start","start",-355208981),(function() { 
var G__94477__delegate = function (params){
return console.log("Entering kurs");
};
var G__94477 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94478__i = 0, G__94478__a = new Array(arguments.length -  0);
while (G__94478__i < G__94478__a.length) {G__94478__a[G__94478__i] = arguments[G__94478__i + 0]; ++G__94478__i;}
  params = new cljs.core.IndexedSeq(G__94478__a,0,null);
} 
return G__94477__delegate.call(this,params);};
G__94477.cljs$lang$maxFixedArity = 0;
G__94477.cljs$lang$applyTo = (function (arglist__94479){
var params = cljs.core.seq(arglist__94479);
return G__94477__delegate(params);
});
G__94477.cljs$core$IFn$_invoke$arity$variadic = G__94477__delegate;
return G__94477;
})()
,new cljs.core.Keyword(null,"stop","stop",-2140911342),(function() { 
var G__94480__delegate = function (params){
return console.log("Leaving kurs");
};
var G__94480 = function (var_args){
var params = null;
if (arguments.length > 0) {
var G__94481__i = 0, G__94481__a = new Array(arguments.length -  0);
while (G__94481__i < G__94481__a.length) {G__94481__a[G__94481__i] = arguments[G__94481__i + 0]; ++G__94481__i;}
  params = new cljs.core.IndexedSeq(G__94481__a,0,null);
} 
return G__94480__delegate.call(this,params);};
G__94480.cljs$lang$maxFixedArity = 0;
G__94480.cljs$lang$applyTo = (function (arglist__94482){
var params = cljs.core.seq(arglist__94482);
return G__94480__delegate(params);
});
G__94480.cljs$core$IFn$_invoke$arity$variadic = G__94480__delegate;
return G__94480;
})()
], null)], null)], null)], null)], null);
var G__94404_94483 = new cljs.core.Keyword("website.core","navigate!","website.core/navigate!",1330437210);
var G__94405_94484 = ((function (G__94404_94483){
return (function (route){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(reitit.frontend.easy.push_state,route);
});})(G__94404_94483))
;
(re_frame.core.reg_fx.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_fx.cljs$core$IFn$_invoke$arity$2(G__94404_94483,G__94405_94484) : re_frame.core.reg_fx.call(null,G__94404_94483,G__94405_94484));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.core","initialize-db","website.core/initialize-db",1362235562),(function (_,___$1){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"input-field-1","input-field-1",-1037608564),"input-field-value",new cljs.core.Keyword(null,"input-field-2","input-field-2",450671259),"1234",new cljs.core.Keyword(null,"toggler","toggler",-533887283),true], null),new cljs.core.Keyword(null,"current-route","current-route",2067529448),null], null);
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.core","navigate","website.core/navigate",-1706397549),(function (db,p__94407){
var vec__94408 = p__94407;
var seq__94409 = cljs.core.seq(vec__94408);
var first__94410 = cljs.core.first(seq__94409);
var seq__94409__$1 = cljs.core.next(seq__94409);
var _ = first__94410;
var route = seq__94409__$1;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("website.core","navigate!","website.core/navigate!",1330437210),route], null);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.core","navigated","website.core/navigated",-735730394),(function (db,p__94411){
var vec__94412 = p__94411;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__94412,(0),null);
var new_match = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__94412,(1),null);
var old_match = new cljs.core.Keyword(null,"current-route","current-route",2067529448).cljs$core$IFn$_invoke$arity$1(db);
var controllers = reitit.frontend.controllers.apply_controllers(new cljs.core.Keyword(null,"controllers","controllers",-1120410624).cljs$core$IFn$_invoke$arity$1(old_match),new_match);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"current-route","current-route",2067529448),cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(new_match,new cljs.core.Keyword(null,"controllers","controllers",-1120410624),controllers));
}));
var G__94415_94485 = new cljs.core.Keyword("website.core","current-route","website.core/current-route",802451478);
var G__94416_94486 = ((function (G__94415_94485){
return (function (db){
return new cljs.core.Keyword(null,"current-route","current-route",2067529448).cljs$core$IFn$_invoke$arity$1(db);
});})(G__94415_94485))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__94415_94485,G__94416_94486) : re_frame.core.reg_sub.call(null,G__94415_94485,G__94416_94486));
website.core.on_navigate = (function website$core$on_navigate(new_match){
if(cljs.core.truth_(new_match)){
var G__94417 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.core","navigated","website.core/navigated",-735730394),new_match], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__94417) : re_frame.core.dispatch.call(null,G__94417));
} else {
return null;
}
});
website.core.router = reitit.frontend.router.cljs$core$IFn$_invoke$arity$2(website.core.routes,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"coercion","coercion",904067157),reitit.coercion.spec.coercion], null)], null));
website.core.init_routes_BANG_ = (function website$core$init_routes_BANG_(){
console.log("initializing routes");

return reitit.frontend.easy.start_BANG_(website.core.router,website.core.on_navigate,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"use-fragment","use-fragment",-1617737154),true], null));
});
website.core.root_component = (function website$core$root_component(p__94418){
var map__94419 = p__94418;
var map__94419__$1 = (((((!((map__94419 == null))))?(((((map__94419.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94419.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94419):map__94419);
var router = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94419__$1,new cljs.core.Keyword(null,"router","router",1091916230));
var current_route = cljs.core.deref((function (){var G__94421 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.core","current-route","website.core/current-route",802451478)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__94421) : re_frame.core.subscribe.call(null,G__94421));
})());
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["current-route>",new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(current_route)], 0));

return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-screen.w-screen","div.h-screen.w-screen",182972769),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.page_header.render], null),(cljs.core.truth_(current_route)?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"view","view",1247994814).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(current_route))], null):null)], null);
});
website.core.root_component_SINGLEQUOTE_ = (function website$core$root_component_SINGLEQUOTE_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___94487 = arguments.length;
var i__4731__auto___94488 = (0);
while(true){
if((i__4731__auto___94488 < len__4730__auto___94487)){
args__4736__auto__.push((arguments[i__4731__auto___94488]));

var G__94489 = (i__4731__auto___94488 + (1));
i__4731__auto___94488 = G__94489;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return website.core.root_component_SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

website.core.root_component_SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$variadic = (function (_){

return nv.core.render();
});

website.core.root_component_SINGLEQUOTE_.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
website.core.root_component_SINGLEQUOTE_.cljs$lang$applyTo = (function (seq94422){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq94422));
});

website.core.mount_root = (function website$core$mount_root(){
(re_frame.core.clear_subscription_cache_BANG_.cljs$core$IFn$_invoke$arity$0 ? re_frame.core.clear_subscription_cache_BANG_.cljs$core$IFn$_invoke$arity$0() : re_frame.core.clear_subscription_cache_BANG_.call(null));

website.core.init_routes_BANG_();

var temp__5735__auto__ = document.getElementById("app");
if(cljs.core.truth_(temp__5735__auto__)){
var el = temp__5735__auto__;
var G__94423 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.core.root_component_SINGLEQUOTE_,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"router","router",1091916230),website.core.router], null)], null);
var G__94424 = el;
var G__94425 = ((function (G__94423,G__94424,el,temp__5735__auto__){
return (function (){
return cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["FRONTEND READY AND MOUNTED"], 0));
});})(G__94423,G__94424,el,temp__5735__auto__))
;
return (reagent.core.render_component.cljs$core$IFn$_invoke$arity$3 ? reagent.core.render_component.cljs$core$IFn$_invoke$arity$3(G__94423,G__94424,G__94425) : reagent.core.render_component.call(null,G__94423,G__94424,G__94425));
} else {
return null;
}
});
website.core.init_BANG_ = (function website$core$init_BANG_(){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["init!"], 0));

var G__94426_94490 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.core","initialize-db","website.core/initialize-db",1362235562)], null);
(re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1(G__94426_94490) : re_frame.core.dispatch_sync.call(null,G__94426_94490));

return website.core.mount_root();
});
website.core.render_home = (function website$core$render_home(){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.w-full.bg-red-900","div.w-full.bg-red-900",654231807),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-white.bg-gray-500>div.py-0.my-0","div.text-white.bg-gray-500>div.py-0.my-0",-631150220),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"grid-template-columns","grid-template-columns",-594112133),"2fr repeat(auto-fit,minmax(320px,1fr))",new cljs.core.Keyword(null,"grid-template-rows","grid-template-rows",-372292629),"auto",new cljs.core.Keyword(null,"grid-auto-rows","grid-auto-rows",-113194069),"auto",new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"4px","4px",200148326)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex","div.flex",-396986231),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-base.h-full.w-full","div.text-base.h-full.w-full",-1454885207),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.editors.editor_markdown,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"markdown","markdown",1227225089),new cljs.core.Keyword(null,"default-value","default-value",232220170),website.pure.utils.default_sample_value((1))], null)], null)], null)], null)], null)], null);
});
});

//# sourceMappingURL=website.core.js.map
