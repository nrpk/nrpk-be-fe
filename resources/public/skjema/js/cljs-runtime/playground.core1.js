goog.provide('playground.core1');
goog.require('cljs.core');
goog.require('integrant.core');
goog.require('re_frame.core');
goog.require('cljs.pprint');
goog.require('playground.sub');
goog.require('clojure.string');
if((typeof playground !== 'undefined') && (typeof playground.core1 !== 'undefined') && (typeof playground.core1.data !== 'undefined')){
} else {
playground.core1.data = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"data1","data1",1540824181),clojure.string.capitalize(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.interpose.cljs$core$IFn$_invoke$arity$2(" ",cljs.core.take.cljs$core$IFn$_invoke$arity$2((30),cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1((function (){
return cljs.core.rand_nth(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, ["footer","f\u00F8ter","feet"], null));
})))))),new cljs.core.Keyword(null,"data2","data2",1373169671),clojure.string.capitalize(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.interpose.cljs$core$IFn$_invoke$arity$2(" ",cljs.core.take.cljs$core$IFn$_invoke$arity$2((50),cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1((function (){
return cljs.core.rand_nth(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, ["footer","f\u00F8ter","feet"], null));
}))))))], null));
}
var G__93845_94078 = new cljs.core.Keyword("playground.core1","app-db","playground.core1/app-db",-751291619);
var G__93846_94079 = ((function (G__93845_94078){
return (function (db,_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.Keyword(null,"current-route","current-route",2067529448));
});})(G__93845_94078))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__93845_94078,G__93846_94079) : re_frame.core.reg_sub.call(null,G__93845_94078,G__93846_94079));
var G__93856_94080 = new cljs.core.Keyword(null,"field-change","field-change",-972084523);
var G__93857_94081 = ((function (G__93856_94080){
return (function (db,p__93859){
var vec__93860 = p__93859;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93860,(0),null);
var path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93860,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(db,path,"");
});})(G__93856_94080))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__93856_94080,G__93857_94081) : re_frame.core.reg_sub.call(null,G__93856_94080,G__93857_94081));
playground.core1.change_fn = (function playground$core1$change_fn(path,input){
var G__93863 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"field-change","field-change",-972084523),path,input.target.value], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__93863) : re_frame.core.dispatch.call(null,G__93863));
});
playground.core1.toggle_fn = (function playground$core1$toggle_fn(path,_input){
var G__93864 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toggle","toggle",1291842030),path], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__93864) : re_frame.core.dispatch.call(null,G__93864));
});
playground.core1.subs_fn_SINGLEQUOTE_ = (function playground$core1$subs_fn_SINGLEQUOTE_(path){
var G__93869 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"field-change","field-change",-972084523),path], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__93869) : re_frame.core.subscribe.call(null,G__93869));
});
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"field-change","field-change",-972084523),(function (db,p__93871){
var vec__93872 = p__93871;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93872,(0),null);
var path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93872,(1),null);
var value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93872,(2),null);
return cljs.core.assoc_in(db,path,value);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"toggle","toggle",1291842030),(function (db,p__93878){
var vec__93879 = p__93878;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93879,(0),null);
var path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93879,(1),null);
var value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93879,(2),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,path,cljs.core.not);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","stuff","playground.core1/stuff",-1520978682),(function (_,opts){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-5xl","div.text-5xl",87637115),new cljs.core.Keyword(null,"title","title",636505583).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"timestamp","timestamp",579478971).cljs$core$IFn$_invoke$arity$1(opts)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","timestamp","playground.core1/timestamp",40500540),(function (_,p__93882){
var map__93883 = p__93882;
var map__93883__$1 = (((((!((map__93883 == null))))?(((((map__93883.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93883.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93883):map__93883);
var test = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93883__$1,new cljs.core.Keyword(null,"test","test",577538877));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-black.text-sm.bg-gray-200.p-1.inline.mx-1","div.text-black.text-sm.bg-gray-200.p-1.inline.mx-1",2073639496),"TIME ",test], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","input-field","playground.core1/input-field",1860117274),(function (fld,p__93889){
var map__93890 = p__93889;
var map__93890__$1 = (((((!((map__93890 == null))))?(((((map__93890.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93890.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93890):map__93890);
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93890__$1,new cljs.core.Keyword(null,"path","path",-188191168));
var subs_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93890__$1,new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs","p.text-xs",702447668),"Input fields ",fld], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.border.p-1.text-black.bg-gray-400","input.border.p-1.text-black.bg-gray-400",-1172989120),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref((subs_fn.cljs$core$IFn$_invoke$arity$1 ? subs_fn.cljs$core$IFn$_invoke$arity$1(path) : subs_fn.call(null,path))),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"<Enter something>",new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__93890,map__93890__$1,path,subs_fn){
return (function (p1__93887_SHARP_){
return playground.core1.change_fn(path,p1__93887_SHARP_);
});})(map__93890,map__93890__$1,path,subs_fn))
], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","base-input","playground.core1/base-input",-494461515),(function (_,p__93893){
var map__93894 = p__93893;
var map__93894__$1 = (((((!((map__93894 == null))))?(((((map__93894.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93894.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93894):map__93894);
var on_change = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93894__$1,new cljs.core.Keyword(null,"on-change","on-change",-732046149));
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93894__$1,new cljs.core.Keyword(null,"path","path",-188191168));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),path], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.border.p-1.text-black.bg-gray-100","input.border.p-1.text-black.bg-gray-100",2049125782),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"on-change","on-change",-732046149),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(on_change,path)], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","root","playground.core1/root",-2052971950),(function (_,p__93899){
var map__93900 = p__93899;
var map__93900__$1 = (((((!((map__93900 == null))))?(((((map__93900.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93900.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93900):map__93900);
var opts = map__93900__$1;
var contents = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"contents","contents",-1567174023));
var app_db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"app-db","app-db",865606302));
var input3 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"input3","input3",-1384635424));
var sub_component = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"sub-component","sub-component",-1148457535));
var input1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"input1","input1",-987904984));
var toggler = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"toggler","toggler",-533887283));
var header = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"header","header",119441134));
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"title","title",636505583));
var input2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93900__$1,new cljs.core.Keyword(null,"input2","input2",-403997100));
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-red-400.p-4.rounded.border.bg-gray-100.m-10.shadow-5.select-none","div.text-red-400.p-4.rounded.border.bg-gray-100.m-10.shadow-5.select-none",65174270),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex-row","div.flex-row",-1914111882),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.justify-between","div.flex.justify-between",-1943883738),input3,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.clip-x","div.flex.clip-x",-864293586),toggler,new cljs.core.Keyword(null,"toggler2","toggler2",-2080233396).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"toggler3","toggler3",-1854530988).cljs$core$IFn$_invoke$arity$1(opts)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.xflex.xflex-grow.border","div.xflex.xflex-grow.border",623694515),input2], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.xflex.xflex-grow.border","div.xflex.xflex-grow.border",623694515),input2], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.xflex.xflex-grow.border","div.xflex.xflex-grow.border",623694515),input2], null)], null),sub_component,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-xs.text-blue-500","div.text-xs.text-blue-500",290272958),(function (){var or__4131__auto__ = title;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "title-1";
}
})()], null),new cljs.core.Keyword(null,"group","group",582596132).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"special-group","special-group",35554335).cljs$core$IFn$_invoke$arity$2(opts,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-16.bg-gray-400.text-gray-600","div.h-16.bg-gray-400.text-gray-600",672872670),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.center","p.center",1543660383),"NADA"], null)], null)),header,input1,contents,app_db], null);
}));
playground.core1.placeholder_sm = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-32.bg-gray-400.text-gray-200","div.h-32.bg-gray-400.text-gray-200",-2094403881),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.center.font-sans.font-black","p.center.font-sans.font-black",-771341619),"placeholder"], null)], null);
playground.core1.placeholder = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-full.w-full.bg-gray-400.text-gray-200","div.h-full.w-full.bg-gray-400.text-gray-200",80529887),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.center.font-sans.font-black","p.center.font-sans.font-black",-771341619),"placeholder"], null)], null);
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","list","playground.core1/list",-2034341902),(function (_,p__93903){
var map__93904 = p__93903;
var map__93904__$1 = (((((!((map__93904 == null))))?(((((map__93904.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93904.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93904):map__93904);
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93904__$1,new cljs.core.Keyword(null,"title","title",636505583));
var contents = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93904__$1,new cljs.core.Keyword(null,"contents","contents",-1567174023));
var item_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93904__$1,new cljs.core.Keyword(null,"item-fn","item-fn",-291245183));
var footer = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93904__$1,new cljs.core.Keyword(null,"footer","footer",1606445390));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-3.py-3.text-blue-500","div.mt-3.py-3.text-blue-500",494413780),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs.text-blue-500.px-6.pb-1","p.text-xs.text-blue-500.px-6.pb-1",-167782879),(function (){var or__4131__auto__ = title;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "no-title";
}
})()], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.xpx-20.h-full.text-black.bg-white.snap-x","div.xpx-20.h-full.text-black.bg-white.snap-x",-869749383),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"overflow-x","overflow-x",-26547754),new cljs.core.Keyword(null,"scroll","scroll",971553779)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"padding-right","padding-right",-1250249681),new cljs.core.Keyword(null,"2rem","2rem",-1563551450),new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"10px","10px",-859606082),new cljs.core.Keyword(null,"grid-template-rows","grid-template-rows",-372292629),"auto",new cljs.core.Keyword(null,"grid-auto-flow","grid-auto-flow",-1754873684),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"scroll-padding","scroll-padding",-1958014802),new cljs.core.Keyword(null,"50%","50%",2071665143),new cljs.core.Keyword(null,"grid-auto-columns","grid-auto-columns",-1346990274),"minmax(200px,calc(90%))"], null)], null),(function (){var iter__4523__auto__ = ((function (map__93904,map__93904__$1,title,contents,item_fn,footer){
return (function playground$core1$iter__93912(s__93913){
return (new cljs.core.LazySeq(null,((function (map__93904,map__93904__$1,title,contents,item_fn,footer){
return (function (){
var s__93913__$1 = s__93913;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93913__$1);
if(temp__5735__auto__){
var s__93913__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__93913__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93913__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93915 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93914 = (0);
while(true){
if((i__93914 < size__4522__auto__)){
var each = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93914);
cljs.core.chunk_append(b__93915,cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.snap-center.font-serif","div.snap-center.font-serif",-1150202928),(item_fn.cljs$core$IFn$_invoke$arity$1 ? item_fn.cljs$core$IFn$_invoke$arity$1(each) : item_fn.call(null,each))], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),each], null)));

var G__94140 = (i__93914 + (1));
i__93914 = G__94140;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93915),playground$core1$iter__93912(cljs.core.chunk_rest(s__93913__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93915),null);
}
} else {
var each = cljs.core.first(s__93913__$2);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.snap-center.font-serif","div.snap-center.font-serif",-1150202928),(item_fn.cljs$core$IFn$_invoke$arity$1 ? item_fn.cljs$core$IFn$_invoke$arity$1(each) : item_fn.call(null,each))], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),each], null)),playground$core1$iter__93912(cljs.core.rest(s__93913__$2)));
}
} else {
return null;
}
break;
}
});})(map__93904,map__93904__$1,title,contents,item_fn,footer))
,null,null));
});})(map__93904,map__93904__$1,title,contents,item_fn,footer))
;
return iter__4523__auto__(contents);
})()], null)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.pt-5","div.pt-5",302799775),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.pl-6.pr-8..font-bold.italic.text-base","h2.pl-6.pr-8..font-bold.italic.text-base",1249026108),"Header 2"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.pl-6.pr-8..pb-3.text-base.text-blue-800.font-serif","div.pl-6.pr-8..pb-3.text-base.text-blue-800.font-serif",-1257909261),new cljs.core.Keyword(null,"data1","data1",1540824181).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(playground.core1.data))], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.pl-6.pr-8.bg-blue-200.py-4.text-xs.text-gray-900.font-serif","div.pl-6.pr-8.bg-blue-200.py-4.text-xs.text-gray-900.font-serif",-1245690401),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1.font-bold.font-sans.mb-1","h1.font-bold.font-sans.mb-1",-353641398),"Header 1"], null),new cljs.core.Keyword(null,"data2","data2",1373169671).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(playground.core1.data))], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","group","playground.core1/group",-2095476829),(function (_,p__93929){
var map__93930 = p__93929;
var map__93930__$1 = (((((!((map__93930 == null))))?(((((map__93930.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93930.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93930):map__93930);
var opts = map__93930__$1;
var caption = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93930__$1,new cljs.core.Keyword(null,"caption","caption",-855383902));
var content = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93930__$1,new cljs.core.Keyword(null,"content","content",15833224));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),caption,content], null);
}));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.sub","toggler-1","playground.sub/toggler-1",741378078),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.sub","toggler-2","playground.sub/toggler-2",1985412322),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.sub","toggler-3","playground.sub/toggler-3",-550100993),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.core1","toggler-4-grp","playground.core1/toggler-4-grp",-186283126),new cljs.core.Keyword("playground.core1","group","playground.core1/group",-2095476829));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.sub","toggler-4","playground.sub/toggler-4",659453422),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.core1","special-group","playground.core1/special-group",1648266912),new cljs.core.Keyword("playground.core1","group","playground.core1/group",-2095476829));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.sub","toggle-5","playground.sub/toggle-5",-1523192789),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.sub","toggle-6","playground.sub/toggle-6",1572010406),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","header","playground.core1/header",1736346995),(function (_,p__93940){
var map__93941 = p__93940;
var map__93941__$1 = (((((!((map__93941 == null))))?(((((map__93941.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93941.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93941):map__93941);
var menu = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93941__$1,new cljs.core.Keyword(null,"menu","menu",352255198));
var menu_state_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93941__$1,new cljs.core.Keyword(null,"menu-state-fn","menu-state-fn",1760163611));
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93941__$1,new cljs.core.Keyword(null,"path","path",-188191168));
var path_click_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93941__$1,new cljs.core.Keyword(null,"path-click-fn","path-click-fn",-1858368554));
var menu_click_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93941__$1,new cljs.core.Keyword(null,"menu-click-fn","menu-click-fn",-1640760249));
var role_click_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93941__$1,new cljs.core.Keyword(null,"role-click-fn","role-click-fn",1586630001));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.xfixed.xtop-0.w-full.shadow-6","div.xfixed.xtop-0.w-full.shadow-6",-502262989),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-gray-100.text-xs.h-12.bg-gray-500.flex.justify-between.z-0","div.text-gray-100.text-xs.h-12.bg-gray-500.flex.justify-between.z-0",-1335575969),(function (){var bg_colors = new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bg-green-700","bg-green-700",1311202480),new cljs.core.Keyword(null,"bg-green-600","bg-green-600",-10193556),new cljs.core.Keyword(null,"bg-green-500","bg-green-500",1294461417),new cljs.core.Keyword(null,"bg-green-400","bg-green-400",826146911),new cljs.core.Keyword(null,"bg-green-300","bg-green-300",-1746043704),new cljs.core.Keyword(null,"bg-green-200","bg-green-200",1042943832),new cljs.core.Keyword(null,"bg-green-100","bg-green-100",2099308292)], null);
var fg_colors = new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"text-white","text-white",-1261600697),new cljs.core.Keyword(null,"text-white","text-white",-1261600697),new cljs.core.Keyword(null,"text-black","text-black",-1990796629),new cljs.core.Keyword(null,"text-black","text-black",-1990796629),new cljs.core.Keyword(null,"text-black","text-black",-1990796629),new cljs.core.Keyword(null,"text-black","text-black",-1990796629),new cljs.core.Keyword(null,"text-black","text-black",-1990796629)], null);
var iter__4523__auto__ = ((function (bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn){
return (function playground$core1$iter__93945(s__93946){
return (new cljs.core.LazySeq(null,((function (bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn){
return (function (){
var s__93946__$1 = s__93946;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93946__$1);
if(temp__5735__auto__){
var s__93946__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__93946__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93946__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93948 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93947 = (0);
while(true){
if((i__93947 < size__4522__auto__)){
var vec__93956 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93947);
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93956,(0),null);
var vec__93959 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93956,(1),null);
var path_elem = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93959,(0),null);
var vec__93962 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93959,(1),null);
var bg = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93962,(0),null);
var fg = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93962,(1),null);
cljs.core.chunk_append(b__93948,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-red-900","div.bg-red-900",-952333994),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.m-0.px-4.h-full","button.m-0.px-4.h-full",467757603),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__93947,vec__93956,idx,vec__93959,path_elem,vec__93962,bg,fg,c__4521__auto__,size__4522__auto__,b__93948,s__93946__$2,temp__5735__auto__,bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn){
return (function (){
return (path_click_fn.cljs$core$IFn$_invoke$arity$1 ? path_click_fn.cljs$core$IFn$_invoke$arity$1(idx) : path_click_fn.call(null,idx));
});})(i__93947,vec__93956,idx,vec__93959,path_elem,vec__93962,bg,fg,c__4521__auto__,size__4522__auto__,b__93948,s__93946__$2,temp__5735__auto__,bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn))
,new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [fg,bg], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),path_elem], null)], null)], null));

var G__94166 = (i__93947 + (1));
i__93947 = G__94166;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93948),playground$core1$iter__93945(cljs.core.chunk_rest(s__93946__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93948),null);
}
} else {
var vec__93969 = cljs.core.first(s__93946__$2);
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93969,(0),null);
var vec__93972 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93969,(1),null);
var path_elem = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93972,(0),null);
var vec__93975 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93972,(1),null);
var bg = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93975,(0),null);
var fg = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93975,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-red-900","div.bg-red-900",-952333994),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.m-0.px-4.h-full","button.m-0.px-4.h-full",467757603),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (vec__93969,idx,vec__93972,path_elem,vec__93975,bg,fg,s__93946__$2,temp__5735__auto__,bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn){
return (function (){
return (path_click_fn.cljs$core$IFn$_invoke$arity$1 ? path_click_fn.cljs$core$IFn$_invoke$arity$1(idx) : path_click_fn.call(null,idx));
});})(vec__93969,idx,vec__93972,path_elem,vec__93975,bg,fg,s__93946__$2,temp__5735__auto__,bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn))
,new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [fg,bg], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),path_elem], null)], null)], null),playground$core1$iter__93945(cljs.core.rest(s__93946__$2)));
}
} else {
return null;
}
break;
}
});})(bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn))
,null,null));
});})(bg_colors,fg_colors,map__93941,map__93941__$1,menu,menu_state_fn,path,path_click_fn,menu_click_fn,role_click_fn))
;
return iter__4523__auto__(cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,cljs.core.mapv.cljs$core$IFn$_invoke$arity$3(cljs.core.vector,path,cljs.core.mapv.cljs$core$IFn$_invoke$arity$3(cljs.core.vector,bg_colors,fg_colors))));
})(),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.flex-grow","div.flex.flex-grow",1497743171)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.justify-between","div.flex.justify-between",-1943883738),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.m-0.px-4.bg-gray-700.h-full","button.m-0.px-4.bg-gray-700.h-full",1236957291),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),role_click_fn], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"role"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.m-0.px-4.bg-gray-800.h-full.outline-none","button.m-0.px-4.bg-gray-800.h-full.outline-none",-275555829),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),(cljs.core.truth_(cljs.core.deref(menu_state_fn))?new cljs.core.Keyword(null,"bg-gray-900","bg-gray-900",-1916121301):new cljs.core.Keyword(null,"bg-black","bg-black",2110303851)),new cljs.core.Keyword(null,"on-click","on-click",1632826543),menu_click_fn], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"menu"], null)], null)], null)], null),(cljs.core.truth_(cljs.core.deref(menu_state_fn))?menu:null)], null);
}));
playground.core1.separator = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.border-b-2.border-dashed.border-gray-800.my-5","div.border-b-2.border-dashed.border-gray-800.my-5",1382900489)], null);
playground.core1.menus = (function playground$core1$menus(items,items2){
var f = (function (e){
var pred__93986 = cljs.core._EQ_;
var expr__93987 = e;
if(cljs.core.truth_((function (){var G__93990 = new cljs.core.Keyword(null,"sep","sep",1970430530);
var G__93991 = expr__93987;
return (pred__93986.cljs$core$IFn$_invoke$arity$2 ? pred__93986.cljs$core$IFn$_invoke$arity$2(G__93990,G__93991) : pred__93986.call(null,G__93990,G__93991));
})())){
return playground.core1.separator;
} else {
if(cljs.core.truth_((function (){var G__93992 = new cljs.core.Keyword(null,"space","space",348133475);
var G__93993 = expr__93987;
return (pred__93986.cljs$core$IFn$_invoke$arity$2 ? pred__93986.cljs$core$IFn$_invoke$arity$2(G__93992,G__93993) : pred__93986.call(null,G__93992,G__93993));
})())){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-4","div.h-4",397934628),""], null);
} else {
if(cljs.core.vector_QMARK_(e)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),""], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-gray-400","p.text-gray-400",-85453884),cljs.core.first(e)], null)], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),""], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-gray-700","p.text-gray-700",-655873393),cljs.core.str.cljs$core$IFn$_invoke$arity$1(e)], null)], null);
}
}
}
});
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.justify-between","div.flex.justify-between",-1943883738),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex-col.w-full","div.flex-col.w-full",-202267715),cljs.core.map.cljs$core$IFn$_invoke$arity$2(f,items)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.w-20","div.w-20",-2054351842)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex-col.w-full","div.flex-col.w-full",-202267715),cljs.core.map.cljs$core$IFn$_invoke$arity$2(f,items2)], null)], null);
});
playground.core1.textblock = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.block.bg-blue-900.mt-4.rounded.shadow-4","div.block.bg-blue-900.mt-4.rounded.shadow-4",1290066337),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.pl-6.pr-8.py-6.text-blue-300","div.pl-6.pr-8.py-6.text-blue-300",1469675134),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.font-bold.italic.text-base.mb-3","h2.font-bold.italic.text-base.mb-3",1031225940),"Header 2"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-base.text-blue-200.font-serif","div.text-base.text-blue-200.font-serif",-1743899767),new cljs.core.Keyword(null,"data1","data1",1540824181).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(playground.core1.data))], null)], null)], null);
playground.core1.menu = new cljs.core.Keyword(null,"div.block.bg-gray-900.p-1.text-gray-600.px-6.py-6.text-xs","div.block.bg-gray-900.p-1.text-gray-600.px-6.py-6.text-xs",-136323640);
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","menu","playground.core1/menu",-1252059299),(function (_,p__94000){
var map__94001 = p__94000;
var map__94001__$1 = (((((!((map__94001 == null))))?(((((map__94001.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94001.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94001):map__94001);
var opts = map__94001__$1;
var items = new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"This","This",-2096396902,null),new cljs.core.Symbol(null,"That","That",-1117822638,null),new cljs.core.Symbol(null,"Other","Other",-837955174,null),new cljs.core.Symbol(null,"Whatever","Whatever",-873649307,null),new cljs.core.Keyword(null,"sep","sep",1970430530),new cljs.core.Symbol(null,"AtLast","AtLast",1229748625,null),"Til slutt"], null);
var items2 = new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"This","This",-2096396902,null),new cljs.core.Symbol(null,"That","That",-1117822638,null),new cljs.core.Keyword(null,"sep","sep",1970430530),new cljs.core.Symbol(null,"Other","Other",-837955174,null),new cljs.core.Symbol(null,"Whatever","Whatever",-873649307,null),new cljs.core.Symbol(null,"AtLas","AtLas",-1962528978,null),new cljs.core.Keyword(null,"sep","sep",1970430530),"Login","Log ut",new cljs.core.Keyword(null,"space","space",348133475),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, ["Endre passord"], null)], null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.core1.menu,playground.core1.menus(items,items2),playground.core1.textblock], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","root-alt-2","playground.core1/root-alt-2",-1632138714),(function (_,p__94010){
var map__94011 = p__94010;
var map__94011__$1 = (((((!((map__94011 == null))))?(((((map__94011.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94011.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94011):map__94011);
var opts = map__94011__$1;
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94011__$1,new cljs.core.Keyword(null,"title","title",636505583));
var header = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94011__$1,new cljs.core.Keyword(null,"header","header",119441134));
var contents = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94011__$1,new cljs.core.Keyword(null,"contents","contents",-1567174023));
var menu = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94011__$1,new cljs.core.Keyword(null,"menu","menu",352255198));
return new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-blue-500.bg-gray-100.z-0","div.text-blue-500.bg-gray-100.z-0",928861180),header,new cljs.core.Keyword(null,"xyz","xyz",-1605570015).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-6","div.px-6",-1073187177),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-xl.uppercase.text-gray-500.py-4.mt-8","div.text-xl.uppercase.text-gray-500.py-4.mt-8",-135469864),(function (){var or__4131__auto__ = title;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "title-1";
}
})()], null)], null),new cljs.core.Keyword(null,"group","group",582596132).cljs$core$IFn$_invoke$arity$2(opts,playground.core1.placeholder),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.pt-5","div.pt-5",302799775),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.pl-6.pr-8..font-bold.italic.text-base","h2.pl-6.pr-8..font-bold.italic.text-base",1249026108),"Header 2"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.pl-6.pr-8..pb-3.text-base.text-blue-800.font-serif","div.pl-6.pr-8..pb-3.text-base.text-blue-800.font-serif",-1257909261),new cljs.core.Keyword(null,"data2","data2",1373169671).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(playground.core1.data))], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-4","div.px-4",472594622),new cljs.core.Keyword(null,"input-1","input-1",797506559).cljs$core$IFn$_invoke$arity$1(opts)], null),new cljs.core.Keyword(null,"button-row","button-row",631379105).cljs$core$IFn$_invoke$arity$1(opts),contents,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-4","div.px-4",472594622),new cljs.core.Keyword(null,"input-2","input-2",-2145025732).cljs$core$IFn$_invoke$arity$1(opts)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.py-4","div.py-4",1523486045),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-4","div.px-4",472594622),new cljs.core.Keyword(null,"check-1","check-1",-742946059).cljs$core$IFn$_invoke$arity$1(opts)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-4","div.px-4",472594622),new cljs.core.Keyword(null,"check-2","check-2",-199785412).cljs$core$IFn$_invoke$arity$1(opts)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-4","div.px-4",472594622),new cljs.core.Keyword(null,"check-3","check-3",-909610113).cljs$core$IFn$_invoke$arity$1(opts)], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.core1","button-row","playground.core1/button-row",88239394),(function (_,p__94021){
var map__94023 = p__94021;
var map__94023__$1 = (((((!((map__94023 == null))))?(((((map__94023.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94023.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94023):map__94023);
var opts = map__94023__$1;
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94023__$1,new cljs.core.Keyword(null,"title","title",636505583));
var contents = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94023__$1,new cljs.core.Keyword(null,"contents","contents",-1567174023));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.py-2.px-4","div.py-2.px-4",-1922493785),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"justify-content","justify-content",-1990475787),new cljs.core.Keyword(null,"end","end",-268185958),new cljs.core.Keyword(null,"grid-auto-flow","grid-auto-flow",-1754873684),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"grid-auto-columns","grid-auto-columns",-1346990274),"100px",new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"4px","4px",200148326)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.py-1.border-2.border-green-700.rounded.w-24.m-1.font-bold.text-white.bg-green-500","button.py-1.border-2.border-green-700.rounded.w-24.m-1.font-bold.text-white.bg-green-500",1024939077),"Send"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.rounded.w-24.m-1.text-red-500.border-2.border-red-700","button.rounded.w-24.m-1.text-red-500.border-2.border-red-700",-1702589622),"Cancel"], null)], null)], null);
}));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.core1","text1","playground.core1/text1",2004923667),new cljs.core.Keyword("playground.sub","text-field","playground.sub/text-field",1753210994));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.core1","text2","playground.core1/text2",-415897299),new cljs.core.Keyword("playground.sub","text-field","playground.sub/text-field",1753210994));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.core1","some-toggle-1","playground.core1/some-toggle-1",1091477591),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.core1","some-toggle-2","playground.core1/some-toggle-2",1152622195),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.core1","some-toggle-3","playground.core1/some-toggle-3",-1033944016),new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414));
var G__94038_94168 = new cljs.core.Keyword(null,"menu-open","menu-open",1430293295);
var G__94039_94169 = ((function (G__94038_94168){
return (function (db,_){
return new cljs.core.Keyword(null,"menu-open","menu-open",1430293295).cljs$core$IFn$_invoke$arity$2(db,false);
});})(G__94038_94168))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__94038_94168,G__94039_94169) : re_frame.core.reg_sub.call(null,G__94038_94168,G__94039_94169));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"menu-click","menu-click",-307878757),(function (db,_){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"menu-open","menu-open",1430293295),cljs.core.not);
}));
cljs.core.derive.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("system","root-alt-2","system/root-alt-2",-214234730),new cljs.core.Keyword("playground.core1","root-alt-2","playground.core1/root-alt-2",-1632138714));
playground.core1.alt_config = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword("playground.core1","button-row","playground.core1/button-row",88239394),new cljs.core.Keyword("playground.core1","text2","playground.core1/text2",-415897299),new cljs.core.Keyword("playground.sub","app-db","playground.sub/app-db",934386255),new cljs.core.Keyword("playground.core1","some-toggle-3","playground.core1/some-toggle-3",-1033944016),new cljs.core.Keyword("playground.core1","list","playground.core1/list",-2034341902),new cljs.core.Keyword("playground.core1","text1","playground.core1/text1",2004923667),new cljs.core.Keyword("playground.core1","header","playground.core1/header",1736346995),new cljs.core.Keyword("playground.core1","some-toggle-2","playground.core1/some-toggle-2",1152622195),new cljs.core.Keyword("system","root-alt-2","system/root-alt-2",-214234730),new cljs.core.Keyword("playground.core1","some-toggle-1","playground.core1/some-toggle-1",1091477591),new cljs.core.Keyword("playground.core1","menu","playground.core1/menu",-1252059299),new cljs.core.Keyword("playground.sub","field","playground.sub/field",-1366235937)],[cljs.core.PersistentArrayMap.EMPTY,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"input-field-2","input-field-2",450671259)], null),new cljs.core.Keyword(null,"title","title",636505583),"Fill in",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.change_fn], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value-fn","value-fn",544624790),(function (){
var G__94045 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("playground.core1","app-db","playground.core1/app-db",-751291619)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__94045) : re_frame.core.subscribe.call(null,G__94045));
})], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toggler-xyz-2","toggler-xyz-2",2008879934)], null),new cljs.core.Keyword(null,"title","title",636505583),"Super special toggles 2",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"title","title",636505583),"snapping h-list",new cljs.core.Keyword(null,"contents","contents",-1567174023),cljs.core.range.cljs$core$IFn$_invoke$arity$1((10)),new cljs.core.Keyword(null,"footer","footer",1606445390),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),"Sample #footer here. "], null),new cljs.core.Keyword(null,"item-fn","item-fn",-291245183),(function (a){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-blue-100.bg-blue-400.py-4","div.text-blue-100.bg-blue-400.py-4",436267599),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex-col","div.flex-col",121724551),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.center","p.center",1543660383),"test"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.center.text-xs.text-blue-300","p.center.text-xs.text-blue-300",-1914626898),["item ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(a)].join('')], null)], null)], null);
})], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"input-field-3","input-field-3",-29478027)], null),new cljs.core.Keyword(null,"caption","caption",-855383902),"testasd",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.change_fn], null),new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"home","home",1565974218,null),new cljs.core.Symbol(null,"a","a",-482876059,null),new cljs.core.Symbol(null,"b","b",-1172211299,null)], null),new cljs.core.Keyword(null,"menu","menu",352255198),integrant.core.ref(new cljs.core.Keyword("playground.core1","menu","playground.core1/menu",-1252059299)),new cljs.core.Keyword(null,"path-click-fn","path-click-fn",-1858368554),(function (p1__94040_SHARP_){
return cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([p1__94040_SHARP_], 0));
}),new cljs.core.Keyword(null,"menu-click-fn","menu-click-fn",-1640760249),(function (){
var G__94046 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"menu-click","menu-click",-307878757)], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__94046) : re_frame.core.dispatch.call(null,G__94046));
}),new cljs.core.Keyword(null,"menu-state-fn","menu-state-fn",1760163611),(function (){var G__94047 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"menu-open","menu-open",1430293295)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__94047) : re_frame.core.subscribe.call(null,G__94047));
})(),new cljs.core.Keyword(null,"role-click-fn","role-click-fn",1586630001),(function (){
return cljs.core.List.EMPTY;
})], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler-xyz-2","toggler-xyz-2",2008879934)], null),new cljs.core.Keyword(null,"title","title",636505583),"Super special toggles 2",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"button-row","button-row",631379105),new cljs.core.Keyword(null,"xyz","xyz",-1605570015),new cljs.core.Keyword(null,"header","header",119441134),new cljs.core.Keyword(null,"title","title",636505583),new cljs.core.Keyword(null,"check-1","check-1",-742946059),new cljs.core.Keyword(null,"contents","contents",-1567174023),new cljs.core.Keyword(null,"check-2","check-2",-199785412),new cljs.core.Keyword(null,"input-2","input-2",-2145025732),new cljs.core.Keyword(null,"check-3","check-3",-909610113),new cljs.core.Keyword(null,"input-1","input-1",797506559)],[integrant.core.ref(new cljs.core.Keyword("playground.core1","button-row","playground.core1/button-row",88239394)),integrant.core.ref(new cljs.core.Keyword("playground.sub","app-db","playground.sub/app-db",934386255)),integrant.core.ref(new cljs.core.Keyword("playground.core1","header","playground.core1/header",1736346995)),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.font-thin.tracking-wider","p.font-thin.tracking-wider",1414404268),"Proper hierarchies"], null),integrant.core.ref(new cljs.core.Keyword("playground.core1","some-toggle-1","playground.core1/some-toggle-1",1091477591)),integrant.core.ref(new cljs.core.Keyword("playground.core1","list","playground.core1/list",-2034341902)),integrant.core.ref(new cljs.core.Keyword("playground.core1","some-toggle-2","playground.core1/some-toggle-2",1152622195)),integrant.core.ref(new cljs.core.Keyword("playground.core1","text2","playground.core1/text2",-415897299)),integrant.core.ref(new cljs.core.Keyword("playground.core1","some-toggle-3","playground.core1/some-toggle-3",-1033944016)),integrant.core.ref(new cljs.core.Keyword("playground.core1","text1","playground.core1/text1",2004923667))]),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler-xyz","toggler-xyz",-532978886)], null),new cljs.core.Keyword(null,"title","title",636505583),"Super special toggles",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"input-field-1","input-field-1",-1037608564)], null),new cljs.core.Keyword(null,"title","title",636505583),"test",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.change_fn], null)]);
playground.core1.config = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword("playground.core1","special-group","playground.core1/special-group",1648266912),new cljs.core.Keyword("playground.sub","component","playground.sub/component",1490105985),new cljs.core.Keyword("playground.sub","toggler-2","playground.sub/toggler-2",1985412322),new cljs.core.Keyword("playground.core1","stuff","playground.core1/stuff",-1520978682),new cljs.core.Keyword("playground.sub","toggle-6","playground.sub/toggle-6",1572010406),new cljs.core.Keyword("playground.core1","toggler-4-grp","playground.core1/toggler-4-grp",-186283126),new cljs.core.Keyword("playground.sub","toggle-5","playground.sub/toggle-5",-1523192789),new cljs.core.Keyword("playground.sub","contents","playground.sub/contents",-1225764340),new cljs.core.Keyword("playground.sub","toggler-4","playground.sub/toggler-4",659453422),new cljs.core.Keyword("playground.sub","app-db","playground.sub/app-db",934386255),new cljs.core.Keyword("playground.sub","field2","playground.sub/field2",1274030194),new cljs.core.Keyword("playground.core1","root","playground.core1/root",-2052971950),new cljs.core.Keyword("playground.core1","list","playground.core1/list",-2034341902),new cljs.core.Keyword("playground.core1","root-alt-x2","playground.core1/root-alt-x2",1732419827),new cljs.core.Keyword("playground.core1","input-field","playground.core1/input-field",1860117274),new cljs.core.Keyword("playground.core1","timestamp","playground.core1/timestamp",40500540),new cljs.core.Keyword("playground.sub","toggler-1","playground.sub/toggler-1",741378078),new cljs.core.Keyword("playground.sub","field","playground.sub/field",-1366235937),new cljs.core.Keyword("playground.sub","toggler-3","playground.sub/toggler-3",-550100993)],[new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Group caption 2",new cljs.core.Keyword(null,"content","content",15833224),integrant.core.ref(new cljs.core.Keyword("playground.sub","toggle-5","playground.sub/toggle-5",-1523192789))], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"title","title",636505583),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-black.bg-yellow-400.inline.p-2.rounded.shadow-4.m-12","div.text-black.bg-yellow-400.inline.p-2.rounded.shadow-4.m-12",-1049489493),"Test title"], null)], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler2","toggler2",-2080233396)], null),new cljs.core.Keyword(null,"title","title",636505583),"Toggle something2",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"timestamp","timestamp",579478971),integrant.core.ref(new cljs.core.Keyword("playground.core1","timestamp","playground.core1/timestamp",40500540)),new cljs.core.Keyword(null,"title","title",636505583),"Some title"], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler-xyz2","toggler-xyz2",-1785875623)], null),new cljs.core.Keyword(null,"title","title",636505583),"Super special toggle 2",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Group caption 2",new cljs.core.Keyword(null,"content","content",15833224),integrant.core.ref(new cljs.core.Keyword("playground.sub","toggler-4","playground.sub/toggler-4",659453422))], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler-xyz","toggler-xyz",-532978886)], null),new cljs.core.Keyword(null,"title","title",636505583),"Super special toggle",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"title","title",636505583),"CONTENTS",new cljs.core.Keyword(null,"stuff","stuff",-2051563643),(function playground$core1$filler(){
return cljs.core.range.cljs$core$IFn$_invoke$arity$1((20));
}),new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (p1__94053_SHARP_){
return cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__94053_SHARP_)], 0));
})], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler-special","toggler-special",-1753883140)], null),new cljs.core.Keyword(null,"title","title",636505583),"Toggle something",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value-fn","value-fn",544624790),(function (){
var G__94056 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("playground.core1","app-db","playground.core1/app-db",-751291619)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__94056) : re_frame.core.subscribe.call(null,G__94056));
})], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"input-field-3","input-field-3",-29478027)], null),new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.change_fn], null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"input3","input3",-1384635424),new cljs.core.Keyword(null,"sub-component","sub-component",-1148457535),new cljs.core.Keyword(null,"group","group",582596132),new cljs.core.Keyword(null,"input1","input1",-987904984),new cljs.core.Keyword(null,"toggler2","toggler2",-2080233396),new cljs.core.Keyword(null,"toggler","toggler",-533887283),new cljs.core.Keyword(null,"header","header",119441134),new cljs.core.Keyword(null,"toggler3","toggler3",-1854530988),new cljs.core.Keyword(null,"input2","input2",-403997100),new cljs.core.Keyword(null,"contents","contents",-1567174023),new cljs.core.Keyword(null,"app-db","app-db",865606302),new cljs.core.Keyword(null,"special-group","special-group",35554335)],[integrant.core.ref(new cljs.core.Keyword("playground.sub","field2","playground.sub/field2",1274030194)),integrant.core.ref(new cljs.core.Keyword("playground.sub","component","playground.sub/component",1490105985)),integrant.core.ref(new cljs.core.Keyword("playground.core1","toggler-4-grp","playground.core1/toggler-4-grp",-186283126)),integrant.core.ref(new cljs.core.Keyword("playground.core1","input-field","playground.core1/input-field",1860117274)),integrant.core.ref(new cljs.core.Keyword("playground.sub","toggler-2","playground.sub/toggler-2",1985412322)),integrant.core.ref(new cljs.core.Keyword("playground.sub","toggler-1","playground.sub/toggler-1",741378078)),integrant.core.ref(new cljs.core.Keyword("playground.core1","stuff","playground.core1/stuff",-1520978682)),integrant.core.ref(new cljs.core.Keyword("playground.sub","toggler-3","playground.sub/toggler-3",-550100993)),integrant.core.ref(new cljs.core.Keyword("playground.sub","field","playground.sub/field",-1366235937)),integrant.core.ref(new cljs.core.Keyword("playground.sub","contents","playground.sub/contents",-1225764340)),integrant.core.ref(new cljs.core.Keyword("playground.sub","app-db","playground.sub/app-db",934386255)),integrant.core.ref(new cljs.core.Keyword("playground.core1","special-group","playground.core1/special-group",1648266912))]),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"title","title",636505583),"::List",new cljs.core.Keyword(null,"contents","contents",-1567174023),cljs.core.range.cljs$core$IFn$_invoke$arity$1((16)),new cljs.core.Keyword(null,"item-fn","item-fn",-291245183),(function (p1__94052_SHARP_){
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"p","p",151049309),["item ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__94052_SHARP_)].join('')],null));
})], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"standard-form",new cljs.core.Keyword(null,"contents","contents",-1567174023),integrant.core.ref(new cljs.core.Keyword("playground.core1","list","playground.core1/list",-2034341902))], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"input-field-1","input-field-1",-1037608564)], null),new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"test","test",577538877),new cljs.core.Keyword(null,"20:10","20:10",-789172922)], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler","toggler",-533887283)], null),new cljs.core.Keyword(null,"title","title",636505583),"Toggle something",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"input-field-2","input-field-2",450671259)], null),new cljs.core.Keyword(null,"title","title",636505583),"test",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.change_fn], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"toggler3","toggler3",-1854530988)], null),new cljs.core.Keyword(null,"title","title",636505583),"Toggle something",new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466),playground.core1.subs_fn_SINGLEQUOTE_,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825),playground.core1.toggle_fn], null)]);
playground.core1.render = (function playground$core1$render(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.Keyword("playground.core1","root-alt-2","playground.core1/root-alt-2",-1632138714).cljs$core$IFn$_invoke$arity$2(integrant.core.init.cljs$core$IFn$_invoke$arity$1(cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(cljs.core.merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([playground.core1.alt_config,cljs.core.PersistentArrayMap.EMPTY], 0))),playground.core1.placeholder)], null);
});

//# sourceMappingURL=playground.core1.js.map
