goog.provide('reagent.debug');
goog.require('cljs.core');
reagent.debug.has_console = (typeof console !== 'undefined');
reagent.debug.tracking = false;
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.warnings !== 'undefined')){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.track_console !== 'undefined')){
} else {
reagent.debug.track_console = (function (){var o = ({});
o.warn = ((function (o){
return (function() { 
var G__76139__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__76139 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__76140__i = 0, G__76140__a = new Array(arguments.length -  0);
while (G__76140__i < G__76140__a.length) {G__76140__a[G__76140__i] = arguments[G__76140__i + 0]; ++G__76140__i;}
  args = new cljs.core.IndexedSeq(G__76140__a,0,null);
} 
return G__76139__delegate.call(this,args);};
G__76139.cljs$lang$maxFixedArity = 0;
G__76139.cljs$lang$applyTo = (function (arglist__76141){
var args = cljs.core.seq(arglist__76141);
return G__76139__delegate(args);
});
G__76139.cljs$core$IFn$_invoke$arity$variadic = G__76139__delegate;
return G__76139;
})()
;})(o))
;

o.error = ((function (o){
return (function() { 
var G__76146__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__76146 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__76147__i = 0, G__76147__a = new Array(arguments.length -  0);
while (G__76147__i < G__76147__a.length) {G__76147__a[G__76147__i] = arguments[G__76147__i + 0]; ++G__76147__i;}
  args = new cljs.core.IndexedSeq(G__76147__a,0,null);
} 
return G__76146__delegate.call(this,args);};
G__76146.cljs$lang$maxFixedArity = 0;
G__76146.cljs$lang$applyTo = (function (arglist__76148){
var args = cljs.core.seq(arglist__76148);
return G__76146__delegate(args);
});
G__76146.cljs$core$IFn$_invoke$arity$variadic = G__76146__delegate;
return G__76146;
})()
;})(o))
;

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
reagent.debug.tracking = true;

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

reagent.debug.tracking = false;

return warns;
});

//# sourceMappingURL=reagent.debug.js.map
