goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
goog.require('goog.string');
goog.require('goog.string.format');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__85856){
var map__85857 = p__85856;
var map__85857__$1 = (((((!((map__85857 == null))))?(((((map__85857.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__85857.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__85857):map__85857);
var m = map__85857__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85857__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85857__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4131__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return [(function (){var temp__5735__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__85859_86242 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__85860_86243 = null;
var count__85861_86244 = (0);
var i__85862_86245 = (0);
while(true){
if((i__85862_86245 < count__85861_86244)){
var f_86253 = chunk__85860_86243.cljs$core$IIndexed$_nth$arity$2(null,i__85862_86245);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_86253], 0));


var G__86254 = seq__85859_86242;
var G__86255 = chunk__85860_86243;
var G__86256 = count__85861_86244;
var G__86257 = (i__85862_86245 + (1));
seq__85859_86242 = G__86254;
chunk__85860_86243 = G__86255;
count__85861_86244 = G__86256;
i__85862_86245 = G__86257;
continue;
} else {
var temp__5735__auto___86258 = cljs.core.seq(seq__85859_86242);
if(temp__5735__auto___86258){
var seq__85859_86263__$1 = temp__5735__auto___86258;
if(cljs.core.chunked_seq_QMARK_(seq__85859_86263__$1)){
var c__4550__auto___86264 = cljs.core.chunk_first(seq__85859_86263__$1);
var G__86265 = cljs.core.chunk_rest(seq__85859_86263__$1);
var G__86266 = c__4550__auto___86264;
var G__86267 = cljs.core.count(c__4550__auto___86264);
var G__86268 = (0);
seq__85859_86242 = G__86265;
chunk__85860_86243 = G__86266;
count__85861_86244 = G__86267;
i__85862_86245 = G__86268;
continue;
} else {
var f_86272 = cljs.core.first(seq__85859_86263__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_86272], 0));


var G__86273 = cljs.core.next(seq__85859_86263__$1);
var G__86274 = null;
var G__86275 = (0);
var G__86276 = (0);
seq__85859_86242 = G__86273;
chunk__85860_86243 = G__86274;
count__85861_86244 = G__86275;
i__85862_86245 = G__86276;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_86282 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4131__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_86282], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_86282)))?cljs.core.second(arglists_86282):arglists_86282)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__85876_86291 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__85877_86292 = null;
var count__85878_86293 = (0);
var i__85879_86294 = (0);
while(true){
if((i__85879_86294 < count__85878_86293)){
var vec__85903_86295 = chunk__85877_86292.cljs$core$IIndexed$_nth$arity$2(null,i__85879_86294);
var name_86296 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__85903_86295,(0),null);
var map__85906_86297 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__85903_86295,(1),null);
var map__85906_86298__$1 = (((((!((map__85906_86297 == null))))?(((((map__85906_86297.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__85906_86297.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__85906_86297):map__85906_86297);
var doc_86299 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85906_86298__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_86300 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85906_86298__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_86296], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_86300], 0));

if(cljs.core.truth_(doc_86299)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_86299], 0));
} else {
}


var G__86305 = seq__85876_86291;
var G__86306 = chunk__85877_86292;
var G__86307 = count__85878_86293;
var G__86308 = (i__85879_86294 + (1));
seq__85876_86291 = G__86305;
chunk__85877_86292 = G__86306;
count__85878_86293 = G__86307;
i__85879_86294 = G__86308;
continue;
} else {
var temp__5735__auto___86309 = cljs.core.seq(seq__85876_86291);
if(temp__5735__auto___86309){
var seq__85876_86311__$1 = temp__5735__auto___86309;
if(cljs.core.chunked_seq_QMARK_(seq__85876_86311__$1)){
var c__4550__auto___86317 = cljs.core.chunk_first(seq__85876_86311__$1);
var G__86320 = cljs.core.chunk_rest(seq__85876_86311__$1);
var G__86321 = c__4550__auto___86317;
var G__86322 = cljs.core.count(c__4550__auto___86317);
var G__86323 = (0);
seq__85876_86291 = G__86320;
chunk__85877_86292 = G__86321;
count__85878_86293 = G__86322;
i__85879_86294 = G__86323;
continue;
} else {
var vec__85912_86324 = cljs.core.first(seq__85876_86311__$1);
var name_86325 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__85912_86324,(0),null);
var map__85915_86326 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__85912_86324,(1),null);
var map__85915_86327__$1 = (((((!((map__85915_86326 == null))))?(((((map__85915_86326.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__85915_86326.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__85915_86326):map__85915_86326);
var doc_86328 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85915_86327__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_86329 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85915_86327__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_86325], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_86329], 0));

if(cljs.core.truth_(doc_86328)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_86328], 0));
} else {
}


var G__86333 = cljs.core.next(seq__85876_86311__$1);
var G__86334 = null;
var G__86335 = (0);
var G__86336 = (0);
seq__85876_86291 = G__86333;
chunk__85877_86292 = G__86334;
count__85878_86293 = G__86335;
i__85879_86294 = G__86336;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5735__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5735__auto__)){
var fnspec = temp__5735__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__85932 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__85933 = null;
var count__85934 = (0);
var i__85935 = (0);
while(true){
if((i__85935 < count__85934)){
var role = chunk__85933.cljs$core$IIndexed$_nth$arity$2(null,i__85935);
var temp__5735__auto___86343__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___86343__$1)){
var spec_86345 = temp__5735__auto___86343__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_86345)], 0));
} else {
}


var G__86347 = seq__85932;
var G__86348 = chunk__85933;
var G__86349 = count__85934;
var G__86350 = (i__85935 + (1));
seq__85932 = G__86347;
chunk__85933 = G__86348;
count__85934 = G__86349;
i__85935 = G__86350;
continue;
} else {
var temp__5735__auto____$1 = cljs.core.seq(seq__85932);
if(temp__5735__auto____$1){
var seq__85932__$1 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__85932__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__85932__$1);
var G__86355 = cljs.core.chunk_rest(seq__85932__$1);
var G__86356 = c__4550__auto__;
var G__86357 = cljs.core.count(c__4550__auto__);
var G__86358 = (0);
seq__85932 = G__86355;
chunk__85933 = G__86356;
count__85934 = G__86357;
i__85935 = G__86358;
continue;
} else {
var role = cljs.core.first(seq__85932__$1);
var temp__5735__auto___86360__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___86360__$2)){
var spec_86361 = temp__5735__auto___86360__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_86361)], 0));
} else {
}


var G__86362 = cljs.core.next(seq__85932__$1);
var G__86363 = null;
var G__86364 = (0);
var G__86365 = (0);
seq__85932 = G__86362;
chunk__85933 = G__86363;
count__85934 = G__86364;
i__85935 = G__86365;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof EvalError))?new cljs.core.Symbol("js","EvalError","js/EvalError",1793498501,null):(((t instanceof RangeError))?new cljs.core.Symbol("js","RangeError","js/RangeError",1703848089,null):(((t instanceof ReferenceError))?new cljs.core.Symbol("js","ReferenceError","js/ReferenceError",-198403224,null):(((t instanceof SyntaxError))?new cljs.core.Symbol("js","SyntaxError","js/SyntaxError",-1527651665,null):(((t instanceof URIError))?new cljs.core.Symbol("js","URIError","js/URIError",505061350,null):(((t instanceof Error))?new cljs.core.Symbol("js","Error","js/Error",-1692659266,null):null
)))))))], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5735__auto__)){
var msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5735__auto__)){
var ed = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__86389 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__86390 = cljs.core.ex_cause(t);
via = G__86389;
t = G__86390;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5735__auto__)){
var root_msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5735__auto__)){
var data = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5735__auto__)){
var phase = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__85993 = datafied_throwable;
var map__85993__$1 = (((((!((map__85993 == null))))?(((((map__85993.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__85993.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__85993):map__85993);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85993__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85993__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__85993__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__85994 = cljs.core.last(via);
var map__85994__$1 = (((((!((map__85994 == null))))?(((((map__85994.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__85994.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__85994):map__85994);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85994__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85994__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85994__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__85995 = data;
var map__85995__$1 = (((((!((map__85995 == null))))?(((((map__85995.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__85995.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__85995):map__85995);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85995__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85995__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85995__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__85996 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__85996__$1 = (((((!((map__85996 == null))))?(((((map__85996.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__85996.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__85996):map__85996);
var top_data = map__85996__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85996__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__86008 = phase;
var G__86008__$1 = (((G__86008 instanceof cljs.core.Keyword))?G__86008.fqn:null);
switch (G__86008__$1) {
case "read-source":
var map__86011 = data;
var map__86011__$1 = (((((!((map__86011 == null))))?(((((map__86011.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__86011.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__86011):map__86011);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86011__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86011__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__86018 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__86018__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86018,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__86018);
var G__86018__$2 = (cljs.core.truth_((function (){var fexpr__86019 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__86019.cljs$core$IFn$_invoke$arity$1 ? fexpr__86019.cljs$core$IFn$_invoke$arity$1(source) : fexpr__86019.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__86018__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__86018__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86018__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__86018__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__86025 = top_data;
var G__86025__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86025,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__86025);
var G__86025__$2 = (cljs.core.truth_((function (){var fexpr__86030 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__86030.cljs$core$IFn$_invoke$arity$1 ? fexpr__86030.cljs$core$IFn$_invoke$arity$1(source) : fexpr__86030.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__86025__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__86025__$1);
var G__86025__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86025__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__86025__$2);
var G__86025__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86025__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__86025__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86025__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__86025__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__86037 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86037,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86037,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86037,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86037,(3),null);
var G__86040 = top_data;
var G__86040__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86040,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__86040);
var G__86040__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86040__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__86040__$1);
var G__86040__$3 = (cljs.core.truth_((function (){var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86040__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__86040__$2);
var G__86040__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86040__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__86040__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86040__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__86040__$4;
}

break;
case "execution":
var vec__86058 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86058,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86058,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86058,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__86058,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (vec__86058,source__$1,method,file,line,G__86008,G__86008__$1,map__85993,map__85993__$1,via,trace,phase,map__85994,map__85994__$1,type,message,data,map__85995,map__85995__$1,problems,fn,caller,map__85996,map__85996__$1,top_data,source){
return (function (p1__85988_SHARP_){
var or__4131__auto__ = (p1__85988_SHARP_ == null);
if(or__4131__auto__){
return or__4131__auto__;
} else {
var fexpr__86065 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__86065.cljs$core$IFn$_invoke$arity$1 ? fexpr__86065.cljs$core$IFn$_invoke$arity$1(p1__85988_SHARP_) : fexpr__86065.call(null,p1__85988_SHARP_));
}
});})(vec__86058,source__$1,method,file,line,G__86008,G__86008__$1,map__85993,map__85993__$1,via,trace,phase,map__85994,map__85994__$1,type,message,data,map__85995,map__85995__$1,problems,fn,caller,map__85996,map__85996__$1,top_data,source))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4131__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return line;
}
})();
var G__86070 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__86070__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86070,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__86070);
var G__86070__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86070__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__86070__$1);
var G__86070__$3 = (cljs.core.truth_((function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86070__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__86070__$2);
var G__86070__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86070__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__86070__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__86070__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__86070__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__86008__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__86107){
var map__86108 = p__86107;
var map__86108__$1 = (((((!((map__86108 == null))))?(((((map__86108.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__86108.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__86108):map__86108);
var triage_data = map__86108__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86108__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = source;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = line;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4131__auto__ = class$;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__86155 = phase;
var G__86155__$1 = (((G__86155 instanceof cljs.core.Keyword))?G__86155.fqn:null);
switch (G__86155__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__86158 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__86159 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__86160 = loc;
var G__86161 = (cljs.core.truth_(spec)?(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__86162_86497 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__86163_86498 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__86164_86499 = true;
var _STAR_print_fn_STAR__temp_val__86165_86500 = ((function (_STAR_print_newline_STAR__orig_val__86162_86497,_STAR_print_fn_STAR__orig_val__86163_86498,_STAR_print_newline_STAR__temp_val__86164_86499,sb__4661__auto__,G__86158,G__86159,G__86160,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__86162_86497,_STAR_print_fn_STAR__orig_val__86163_86498,_STAR_print_newline_STAR__temp_val__86164_86499,sb__4661__auto__,G__86158,G__86159,G__86160,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__86164_86499;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__86165_86500;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__86162_86497,_STAR_print_fn_STAR__orig_val__86163_86498,_STAR_print_newline_STAR__temp_val__86164_86499,_STAR_print_fn_STAR__temp_val__86165_86500,sb__4661__auto__,G__86158,G__86159,G__86160,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__86162_86497,_STAR_print_fn_STAR__orig_val__86163_86498,_STAR_print_newline_STAR__temp_val__86164_86499,_STAR_print_fn_STAR__temp_val__86165_86500,sb__4661__auto__,G__86158,G__86159,G__86160,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__86085_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__86085_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__86162_86497,_STAR_print_fn_STAR__orig_val__86163_86498,_STAR_print_newline_STAR__temp_val__86164_86499,_STAR_print_fn_STAR__temp_val__86165_86500,sb__4661__auto__,G__86158,G__86159,G__86160,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__86162_86497,_STAR_print_fn_STAR__orig_val__86163_86498,_STAR_print_newline_STAR__temp_val__86164_86499,_STAR_print_fn_STAR__temp_val__86165_86500,sb__4661__auto__,G__86158,G__86159,G__86160,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__86163_86498;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__86162_86497;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__86158,G__86159,G__86160,G__86161) : format.call(null,G__86158,G__86159,G__86160,G__86161));

break;
case "macroexpansion":
var G__86171 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__86172 = cause_type;
var G__86173 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__86174 = loc;
var G__86175 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__86171,G__86172,G__86173,G__86174,G__86175) : format.call(null,G__86171,G__86172,G__86173,G__86174,G__86175));

break;
case "compile-syntax-check":
var G__86176 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__86177 = cause_type;
var G__86178 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__86179 = loc;
var G__86180 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__86176,G__86177,G__86178,G__86179,G__86180) : format.call(null,G__86176,G__86177,G__86178,G__86179,G__86180));

break;
case "compilation":
var G__86188 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__86189 = cause_type;
var G__86190 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__86191 = loc;
var G__86192 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__86188,G__86189,G__86190,G__86191,G__86192) : format.call(null,G__86188,G__86189,G__86190,G__86191,G__86192));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__86193 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__86194 = symbol;
var G__86195 = loc;
var G__86196 = (function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__86197_86520 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__86198_86521 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__86199_86522 = true;
var _STAR_print_fn_STAR__temp_val__86200_86523 = ((function (_STAR_print_newline_STAR__orig_val__86197_86520,_STAR_print_fn_STAR__orig_val__86198_86521,_STAR_print_newline_STAR__temp_val__86199_86522,sb__4661__auto__,G__86193,G__86194,G__86195,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__86197_86520,_STAR_print_fn_STAR__orig_val__86198_86521,_STAR_print_newline_STAR__temp_val__86199_86522,sb__4661__auto__,G__86193,G__86194,G__86195,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__86199_86522;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__86200_86523;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__86197_86520,_STAR_print_fn_STAR__orig_val__86198_86521,_STAR_print_newline_STAR__temp_val__86199_86522,_STAR_print_fn_STAR__temp_val__86200_86523,sb__4661__auto__,G__86193,G__86194,G__86195,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__86197_86520,_STAR_print_fn_STAR__orig_val__86198_86521,_STAR_print_newline_STAR__temp_val__86199_86522,_STAR_print_fn_STAR__temp_val__86200_86523,sb__4661__auto__,G__86193,G__86194,G__86195,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__86092_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__86092_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__86197_86520,_STAR_print_fn_STAR__orig_val__86198_86521,_STAR_print_newline_STAR__temp_val__86199_86522,_STAR_print_fn_STAR__temp_val__86200_86523,sb__4661__auto__,G__86193,G__86194,G__86195,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__86197_86520,_STAR_print_fn_STAR__orig_val__86198_86521,_STAR_print_newline_STAR__temp_val__86199_86522,_STAR_print_fn_STAR__temp_val__86200_86523,sb__4661__auto__,G__86193,G__86194,G__86195,G__86155,G__86155__$1,loc,class_name,simple_class,cause_type,format,map__86108,map__86108__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__86198_86521;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__86197_86520;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__86193,G__86194,G__86195,G__86196) : format.call(null,G__86193,G__86194,G__86195,G__86196));
} else {
var G__86210 = "Execution error%s at %s(%s).\n%s\n";
var G__86211 = cause_type;
var G__86212 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__86213 = loc;
var G__86214 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__86210,G__86211,G__86212,G__86213,G__86214) : format.call(null,G__86210,G__86211,G__86212,G__86213,G__86214));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__86155__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
