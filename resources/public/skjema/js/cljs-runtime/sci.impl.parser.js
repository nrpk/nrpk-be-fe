goog.provide('sci.impl.parser');
goog.require('cljs.core');
goog.require('sci.impl.readers');
goog.require('edamame.core');
sci.impl.parser.opts = edamame.impl.parser.normalize_opts(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"all","all",892129742),true,new cljs.core.Keyword(null,"read-eval","read-eval",1788733932),false,new cljs.core.Keyword(null,"fn","fn",-1175266204),sci.impl.readers.read_fn], null));
sci.impl.parser.parse_next = (function sci$impl$parser$parse_next(var_args){
var G__88617 = arguments.length;
switch (G__88617) {
case 1:
return sci.impl.parser.parse_next.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sci.impl.parser.parse_next.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sci.impl.parser.parse_next.cljs$core$IFn$_invoke$arity$1 = (function (r){
return edamame.impl.parser.parse_next(sci.impl.parser.opts,r);
});

sci.impl.parser.parse_next.cljs$core$IFn$_invoke$arity$2 = (function (r,features){
return edamame.impl.parser.parse_next(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(sci.impl.parser.opts,new cljs.core.Keyword(null,"read-cond","read-cond",1056899244),new cljs.core.Keyword(null,"allow","allow",-1857325745),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"features","features",-1146962336),features], 0)),r);
});

sci.impl.parser.parse_next.cljs$lang$maxFixedArity = 2;

sci.impl.parser.parse_string = (function sci$impl$parser$parse_string(var_args){
var G__88635 = arguments.length;
switch (G__88635) {
case 1:
return sci.impl.parser.parse_string.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sci.impl.parser.parse_string.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sci.impl.parser.parse_string.cljs$core$IFn$_invoke$arity$1 = (function (s){
return edamame.core.parse_string.cljs$core$IFn$_invoke$arity$2(s,sci.impl.parser.opts);
});

sci.impl.parser.parse_string.cljs$core$IFn$_invoke$arity$2 = (function (s,features){
return edamame.core.parse_string.cljs$core$IFn$_invoke$arity$2(s,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(sci.impl.parser.opts,new cljs.core.Keyword(null,"read-cond","read-cond",1056899244),new cljs.core.Keyword(null,"allow","allow",-1857325745),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"features","features",-1146962336),features], 0)));
});

sci.impl.parser.parse_string.cljs$lang$maxFixedArity = 2;

sci.impl.parser.parse_string_all = (function sci$impl$parser$parse_string_all(var_args){
var G__88648 = arguments.length;
switch (G__88648) {
case 1:
return sci.impl.parser.parse_string_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sci.impl.parser.parse_string_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sci.impl.parser.parse_string_all.cljs$core$IFn$_invoke$arity$1 = (function (s){
return edamame.core.parse_string_all.cljs$core$IFn$_invoke$arity$2(s,sci.impl.parser.opts);
});

sci.impl.parser.parse_string_all.cljs$core$IFn$_invoke$arity$2 = (function (s,features){
return edamame.core.parse_string_all.cljs$core$IFn$_invoke$arity$2(s,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(sci.impl.parser.opts,new cljs.core.Keyword(null,"read-cond","read-cond",1056899244),new cljs.core.Keyword(null,"allow","allow",-1857325745),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"features","features",-1146962336),features], 0)));
});

sci.impl.parser.parse_string_all.cljs$lang$maxFixedArity = 2;


//# sourceMappingURL=sci.impl.parser.js.map
