goog.provide('sci.impl.analyzer');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('clojure.walk');
goog.require('sci.impl.destructure');
goog.require('sci.impl.doseq_macro');
goog.require('sci.impl.for_macro');
goog.require('sci.impl.utils');
sci.impl.analyzer.macros = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 27, [new cljs.core.Symbol(null,"try","try",-1273693247,null),"null",new cljs.core.Symbol(null,"doseq","doseq",221164135,null),"null",new cljs.core.Symbol(null,"lazy-seq","lazy-seq",489632906,null),"null",new cljs.core.Symbol(null,"let","let",358118826,null),"null",new cljs.core.Symbol(null,"->","->",-2139605430,null),"null",new cljs.core.Symbol(null,"fn","fn",465265323,null),"null",new cljs.core.Symbol(null,"as->","as->",1430690540,null),"null",new cljs.core.Symbol(null,"do","do",1686842252,null),"null",new cljs.core.Symbol(null,"when","when",1064114221,null),"null",new cljs.core.Symbol(null,"defn","defn",-126010802,null),"null",new cljs.core.Symbol(null,"if","if",1181717262,null),"null",new cljs.core.Symbol(null,"declare","declare",654042991,null),"null",new cljs.core.Symbol(null,"or","or",1876275696,null),"null",new cljs.core.Symbol(null,"require","require",1172530194,null),"null",new cljs.core.Symbol(null,"fn*","fn*",-752876845,null),"null",new cljs.core.Symbol(null,"loop","loop",1244978678,null),"null",new cljs.core.Symbol(null,"cond","cond",1606708055,null),"null",new cljs.core.Symbol(null,"syntax-quote","syntax-quote",407366680,null),"null",new cljs.core.Symbol(null,"for","for",316745208,null),"null",new cljs.core.Symbol(null,"defmacro","defmacro",2054157304,null),"null",new cljs.core.Symbol(null,"quote","quote",1377916282,null),"null",new cljs.core.Symbol(null,"case","case",-1510733573,null),"null",new cljs.core.Symbol(null,"comment","comment",-2122229700,null),"null",new cljs.core.Symbol(null,"and","and",668631710,null),"null",new cljs.core.Symbol(null,"def","def",597100991,null),"null",new cljs.core.Symbol(null,"->>","->>",-1874332161,null),"null",new cljs.core.Symbol(null,"quote*","quote*",1608011487,null),"null"], null), null);
sci.impl.analyzer.check_permission_BANG_ = (function sci$impl$analyzer$check_permission_BANG_(p__88151,check_sym,sym){
var map__88152 = p__88151;
var map__88152__$1 = (((((!((map__88152 == null))))?(((((map__88152.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__88152.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__88152):map__88152);
var allow = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88152__$1,new cljs.core.Keyword(null,"allow","allow",-1857325745));
var deny = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88152__$1,new cljs.core.Keyword(null,"deny","deny",1589338523));
if((cljs.core.truth_(allow)?cljs.core.contains_QMARK_(allow,check_sym):true)){
} else {
sci.impl.utils.throw_error_with_location.cljs$core$IFn$_invoke$arity$2([cljs.core.str.cljs$core$IFn$_invoke$arity$1(sym)," is not allowed!"].join(''),sym);
}

if((cljs.core.truth_(deny)?cljs.core.contains_QMARK_(deny,check_sym):false)){
return sci.impl.utils.throw_error_with_location.cljs$core$IFn$_invoke$arity$2([cljs.core.str.cljs$core$IFn$_invoke$arity$1(sym)," is not allowed!"].join(''),sym);
} else {
return null;
}
});
sci.impl.analyzer.lookup_env = (function sci$impl$analyzer$lookup_env(env,sym,sym_ns,sym_name){
var env__$1 = cljs.core.deref(env);
var or__4131__auto__ = cljs.core.find(env__$1,sym);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
if(cljs.core.truth_(sym_ns)){
var or__4131__auto____$1 = (function (){var G__88156 = env__$1;
var G__88156__$1 = (((G__88156 == null))?null:new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469).cljs$core$IFn$_invoke$arity$1(G__88156));
var G__88156__$2 = (((G__88156__$1 == null))?null:(sym_ns.cljs$core$IFn$_invoke$arity$1 ? sym_ns.cljs$core$IFn$_invoke$arity$1(G__88156__$1) : sym_ns.call(null,G__88156__$1)));
if((G__88156__$2 == null)){
return null;
} else {
return cljs.core.find(G__88156__$2,sym_name);
}
})();
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
var temp__5735__auto__ = (function (){var G__88157 = env__$1;
var G__88157__$1 = (((G__88157 == null))?null:new cljs.core.Keyword(null,"aliases","aliases",1346874714).cljs$core$IFn$_invoke$arity$1(G__88157));
if((G__88157__$1 == null)){
return null;
} else {
return (sym_ns.cljs$core$IFn$_invoke$arity$1 ? sym_ns.cljs$core$IFn$_invoke$arity$1(G__88157__$1) : sym_ns.call(null,G__88157__$1));
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var aliased = temp__5735__auto__;
var temp__5735__auto____$1 = (function (){var G__88158 = env__$1;
var G__88158__$1 = (((G__88158 == null))?null:new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469).cljs$core$IFn$_invoke$arity$1(G__88158));
var G__88158__$2 = (((G__88158__$1 == null))?null:(aliased.cljs$core$IFn$_invoke$arity$1 ? aliased.cljs$core$IFn$_invoke$arity$1(G__88158__$1) : aliased.call(null,G__88158__$1)));
if((G__88158__$2 == null)){
return null;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(G__88158__$2,sym_name);
}
})();
if(cljs.core.truth_(temp__5735__auto____$1)){
var v = temp__5735__auto____$1;
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(aliased),cljs.core.str.cljs$core$IFn$_invoke$arity$1(sym_name)),v], null);
} else {
return null;
}
} else {
return null;
}
}
} else {
var G__88159 = env__$1;
var G__88159__$1 = (((G__88159 == null))?null:new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469).cljs$core$IFn$_invoke$arity$1(G__88159));
var G__88159__$2 = (((G__88159__$1 == null))?null:cljs.core.get.cljs$core$IFn$_invoke$arity$2(G__88159__$1,new cljs.core.Symbol(null,"clojure.core","clojure.core",-189332625,null)));
if((G__88159__$2 == null)){
return null;
} else {
return cljs.core.find(G__88159__$2,sym_name);
}
}
}
});
sci.impl.analyzer.lookup = (function sci$impl$analyzer$lookup(p__88160,sym){
var map__88161 = p__88160;
var map__88161__$1 = (((((!((map__88161 == null))))?(((((map__88161.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__88161.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__88161):map__88161);
var ctx = map__88161__$1;
var env = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88161__$1,new cljs.core.Keyword(null,"env","env",-1815813235));
var bindings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88161__$1,new cljs.core.Keyword(null,"bindings","bindings",1271397192));
var classes = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88161__$1,new cljs.core.Keyword(null,"classes","classes",2037804510));
var sym_ns = (function (){var G__88166 = cljs.core.namespace(sym);
if((G__88166 == null)){
return null;
} else {
return cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(G__88166);
}
})();
var sym_name = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(cljs.core.name(sym));
var vec__88163 = (function (){var or__4131__auto__ = (function (){var temp__5735__auto__ = cljs.core.find(bindings,sym);
if(cljs.core.truth_(temp__5735__auto__)){
var vec__88170 = temp__5735__auto__;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88170,(0),null);
var _v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88170,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,sci.impl.utils.mark_resolve_sym(k)], null);
} else {
return null;
}
})();
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var temp__5735__auto__ = (function (){var or__4131__auto____$1 = sci.impl.analyzer.lookup_env(env,sym,sym_ns,sym_name);
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
var or__4131__auto____$2 = cljs.core.find(classes,sym);
if(cljs.core.truth_(or__4131__auto____$2)){
return or__4131__auto____$2;
} else {
var or__4131__auto____$3 = (function (){var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(sci.impl.analyzer.macros,sym);
if(cljs.core.truth_(temp__5735__auto__)){
var v = temp__5735__auto__;
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [v,v], null);
} else {
return null;
}
})();
if(cljs.core.truth_(or__4131__auto____$3)){
return or__4131__auto____$3;
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"recur","recur",1202958259,null),sym)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [sym,sci.impl.utils.mark_resolve_sym(sym)], null);
} else {
return null;
}
}
}
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var vec__88176 = temp__5735__auto__;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88176,(0),null);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88176,(1),null);
var kv = vec__88176;
sci.impl.analyzer.check_permission_BANG_(ctx,k,sym);

return kv;
} else {
return null;
}
}
})();
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88163,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88163,(1),null);
var kv = vec__88163;
var temp__5733__auto__ = cljs.core.meta(k);
if(cljs.core.truth_(temp__5733__auto__)){
var m = temp__5733__auto__;
if(cljs.core.truth_(new cljs.core.Keyword("sci","deref!","sci/deref!",153175030).cljs$core$IFn$_invoke$arity$1(m))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,cljs.core.deref(v)], null);
} else {
return kv;
}
} else {
return kv;
}
});
sci.impl.analyzer.resolve_symbol = (function sci$impl$analyzer$resolve_symbol(ctx,sym){
var sym__$1 = sci.impl.utils.strip_core_ns(sym);
var res = cljs.core.second((function (){var or__4131__auto__ = sci.impl.analyzer.lookup(ctx,sym__$1);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var n = cljs.core.name(sym__$1);
if(clojure.string.starts_with_QMARK_(n,"'")){
var v = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(cljs.core.subs.cljs$core$IFn$_invoke$arity$2(n,(1)));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [v,v], null);
} else {
return sci.impl.utils.throw_error_with_location.cljs$core$IFn$_invoke$arity$2(["Could not resolve symbol: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(sym__$1)].join(''),sym__$1);
}
}
})());
return res;
});
sci.impl.analyzer.expand_fn_args_PLUS_body = (function sci$impl$analyzer$expand_fn_args_PLUS_body(ctx,fn_name,p__88182,macro_QMARK_){
var vec__88183 = p__88182;
var seq__88184 = cljs.core.seq(vec__88183);
var first__88185 = cljs.core.first(seq__88184);
var seq__88184__$1 = cljs.core.next(seq__88184);
var binding_vector = first__88185;
var body_exprs = seq__88184__$1;
var binding_vector__$1 = (cljs.core.truth_(macro_QMARK_)?cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"&form","&form",1482799337,null),new cljs.core.Symbol(null,"&env","&env",-919163083,null)], null),binding_vector):binding_vector);
var fixed_args = cljs.core.take_while.cljs$core$IFn$_invoke$arity$2(((function (binding_vector__$1,vec__88183,seq__88184,first__88185,seq__88184__$1,binding_vector,body_exprs){
return (function (p1__88179_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"&","&",-2144855648,null),p1__88179_SHARP_);
});})(binding_vector__$1,vec__88183,seq__88184,first__88185,seq__88184__$1,binding_vector,body_exprs))
,binding_vector__$1);
var var_arg = cljs.core.second(cljs.core.drop_while.cljs$core$IFn$_invoke$arity$2(((function (binding_vector__$1,fixed_args,vec__88183,seq__88184,first__88185,seq__88184__$1,binding_vector,body_exprs){
return (function (p1__88180_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"&","&",-2144855648,null),p1__88180_SHARP_);
});})(binding_vector__$1,fixed_args,vec__88183,seq__88184,first__88185,seq__88184__$1,binding_vector,body_exprs))
,binding_vector__$1));
var fixed_arity = cljs.core.count(fixed_args);
var fixed_names = cljs.core.vec(cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$2(fixed_arity,sci.impl.utils.gensym_STAR_));
var destructure_vec = cljs.core.vec(cljs.core.interleave.cljs$core$IFn$_invoke$arity$2(binding_vector__$1,fixed_names));
var var_arg_name = (cljs.core.truth_(var_arg)?sci.impl.utils.gensym_STAR_.cljs$core$IFn$_invoke$arity$0():null);
var destructure_vec__$1 = (cljs.core.truth_(var_arg)?cljs.core.conj.cljs$core$IFn$_invoke$arity$variadic(destructure_vec,var_arg,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([var_arg_name], 0)):destructure_vec);
var destructured_vec = sci.impl.destructure.destructure(destructure_vec__$1);
var ctx__$1 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(ctx,new cljs.core.Keyword(null,"bindings","bindings",1271397192),cljs.core.merge,cljs.core.zipmap(cljs.core.take_nth.cljs$core$IFn$_invoke$arity$2((2),destructured_vec),cljs.core.repeat.cljs$core$IFn$_invoke$arity$1(null)));
var body_form = sci.impl.utils.mark_eval_call(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"let","let",358118826,null),null,(1),null)),(new cljs.core.List(null,destructured_vec,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (binding_vector__$1,fixed_args,var_arg,fixed_arity,fixed_names,destructure_vec,var_arg_name,destructure_vec__$1,destructured_vec,ctx__$1,vec__88183,seq__88184,first__88185,seq__88184__$1,binding_vector,body_exprs){
return (function (p1__88181_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx__$1,p1__88181_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx__$1,p1__88181_SHARP_));
});})(binding_vector__$1,fixed_args,var_arg,fixed_arity,fixed_names,destructure_vec,var_arg_name,destructure_vec__$1,destructured_vec,ctx__$1,vec__88183,seq__88184,first__88185,seq__88184__$1,binding_vector,body_exprs))
,body_exprs))], 0)))));
var arg_list = (cljs.core.truth_(var_arg)?cljs.core.conj.cljs$core$IFn$_invoke$arity$variadic(fixed_names,new cljs.core.Symbol(null,"&","&",-2144855648,null),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([var_arg_name], 0)):fixed_names);
return new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword("sci.impl","arg-list","sci.impl/arg-list",649854284),arg_list,new cljs.core.Keyword("sci.impl","body","sci.impl/body",-1493886648),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [body_form], null),new cljs.core.Keyword("sci.impl","fixed-arity","sci.impl/fixed-arity",-1251617052),fixed_arity,new cljs.core.Keyword("sci.impl","destructure-vec","sci.impl/destructure-vec",-692349508),destructure_vec__$1,new cljs.core.Keyword("sci.impl","fixed-names","sci.impl/fixed-names",-1163180677),fixed_names,new cljs.core.Keyword("sci.impl","fixed-args","sci.impl/fixed-args",-1759118015),fixed_args,new cljs.core.Keyword("sci.impl","var-arg-name","sci.impl/var-arg-name",1800498100),var_arg_name,new cljs.core.Keyword("sci.impl","fn-name","sci.impl/fn-name",-1172300569),fn_name], null);
});
sci.impl.analyzer.expand_fn = (function sci$impl$analyzer$expand_fn(ctx,p__88195,macro_QMARK_){
var vec__88196 = p__88195;
var seq__88197 = cljs.core.seq(vec__88196);
var first__88198 = cljs.core.first(seq__88197);
var seq__88197__$1 = cljs.core.next(seq__88197);
var _fn = first__88198;
var first__88198__$1 = cljs.core.first(seq__88197__$1);
var seq__88197__$2 = cljs.core.next(seq__88197__$1);
var name_QMARK_ = first__88198__$1;
var body = seq__88197__$2;
var fn_name = (((name_QMARK_ instanceof cljs.core.Symbol))?name_QMARK_:null);
var body__$1 = (cljs.core.truth_(fn_name)?body:cljs.core.cons(name_QMARK_,body));
var bodies = ((cljs.core.seq_QMARK_(cljs.core.first(body__$1)))?body__$1:new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [body__$1], null));
var ctx__$1 = (cljs.core.truth_(fn_name)?cljs.core.assoc_in(ctx,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bindings","bindings",1271397192),fn_name], null),null):ctx);
var arities = cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (fn_name,body__$1,bodies,ctx__$1,vec__88196,seq__88197,first__88198,seq__88197__$1,_fn,first__88198__$1,seq__88197__$2,name_QMARK_,body){
return (function (p1__88190_SHARP_){
return sci.impl.analyzer.expand_fn_args_PLUS_body(ctx__$1,fn_name,p1__88190_SHARP_,macro_QMARK_);
});})(fn_name,body__$1,bodies,ctx__$1,vec__88196,seq__88197,first__88198,seq__88197__$1,_fn,first__88198__$1,seq__88197__$2,name_QMARK_,body))
,bodies));
return sci.impl.utils.mark_eval(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("sci.impl","fn-bodies","sci.impl/fn-bodies",134751661),arities,new cljs.core.Keyword("sci.impl","fn-name","sci.impl/fn-name",-1172300569),fn_name,new cljs.core.Keyword("sci.impl","fn","sci.impl/fn",1695180073),true], null));
});
sci.impl.analyzer.expand_fn_literal_body = (function sci$impl$analyzer$expand_fn_literal_body(ctx,expr){
var fn_body = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(expr,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sci.impl","fn-bodies","sci.impl/fn-bodies",134751661),(0)], null));
var fixed_names = new cljs.core.Keyword("sci.impl","fixed-names","sci.impl/fixed-names",-1163180677).cljs$core$IFn$_invoke$arity$1(fn_body);
var var_arg_name = new cljs.core.Keyword("sci.impl","var-arg-name","sci.impl/var-arg-name",1800498100).cljs$core$IFn$_invoke$arity$1(fn_body);
var bindings = (cljs.core.truth_(var_arg_name)?cljs.core.conj.cljs$core$IFn$_invoke$arity$2(fixed_names,var_arg_name):fixed_names);
var bindings__$1 = cljs.core.zipmap(bindings,cljs.core.repeat.cljs$core$IFn$_invoke$arity$1(null));
var ctx__$1 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(ctx,new cljs.core.Keyword(null,"bindings","bindings",1271397192),cljs.core.merge,bindings__$1);
return sci.impl.utils.mark_eval(cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(expr,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sci.impl","fn-bodies","sci.impl/fn-bodies",134751661),(0),new cljs.core.Keyword("sci.impl","body","sci.impl/body",-1493886648),(0)], null),((function (fn_body,fixed_names,var_arg_name,bindings,bindings__$1,ctx__$1){
return (function (expr__$1){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx__$1,expr__$1) : sci.impl.analyzer.macroexpand.call(null,ctx__$1,expr__$1));
});})(fn_body,fixed_names,var_arg_name,bindings,bindings__$1,ctx__$1))
));
});
sci.impl.analyzer.expand_let_STAR_ = (function sci$impl$analyzer$expand_let_STAR_(ctx,destructured_let_bindings,exprs){
var vec__88210 = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p__88214,p__88215){
var vec__88217 = p__88214;
var ctx__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88217,(0),null);
var new_let_bindings = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88217,(1),null);
var vec__88220 = p__88215;
var binding_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88220,(0),null);
var binding_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88220,(1),null);
var v = (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx__$1,binding_value) : sci.impl.analyzer.macroexpand.call(null,ctx__$1,binding_value));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.update.cljs$core$IFn$_invoke$arity$5(ctx__$1,new cljs.core.Keyword(null,"bindings","bindings",1271397192),cljs.core.assoc,binding_name,v),cljs.core.conj.cljs$core$IFn$_invoke$arity$variadic(new_let_bindings,binding_name,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([v], 0))], null);
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [ctx,cljs.core.PersistentVector.EMPTY], null),cljs.core.partition.cljs$core$IFn$_invoke$arity$2((2),destructured_let_bindings));
var ctx__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88210,(0),null);
var new_let_bindings = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88210,(1),null);
return sci.impl.utils.mark_eval_call(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"let","let",358118826,null),null,(1),null)),(new cljs.core.List(null,new_let_bindings,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (vec__88210,ctx__$1,new_let_bindings){
return (function (p1__88206_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx__$1,p1__88206_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx__$1,p1__88206_SHARP_));
});})(vec__88210,ctx__$1,new_let_bindings))
,exprs))], 0)))));
});
/**
 * The let macro from clojure.core
 */
sci.impl.analyzer.expand_let = (function sci$impl$analyzer$expand_let(ctx,p__88226){
var vec__88227 = p__88226;
var seq__88228 = cljs.core.seq(vec__88227);
var first__88229 = cljs.core.first(seq__88228);
var seq__88228__$1 = cljs.core.next(seq__88228);
var _let = first__88229;
var first__88229__$1 = cljs.core.first(seq__88228__$1);
var seq__88228__$2 = cljs.core.next(seq__88228__$1);
var let_bindings = first__88229__$1;
var exprs = seq__88228__$2;
var let_bindings__$1 = sci.impl.destructure.destructure(let_bindings);
return sci.impl.analyzer.expand_let_STAR_(ctx,let_bindings__$1,exprs);
});
/**
 * The -> macro from clojure.core.
 */
sci.impl.analyzer.expand__GT_ = (function sci$impl$analyzer$expand__GT_(ctx,p__88230){
var vec__88231 = p__88230;
var seq__88232 = cljs.core.seq(vec__88231);
var first__88233 = cljs.core.first(seq__88232);
var seq__88232__$1 = cljs.core.next(seq__88232);
var x = first__88233;
var forms = seq__88232__$1;
var expanded = (function (){var x__$1 = x;
var forms__$1 = forms;
while(true){
if(forms__$1){
var form = cljs.core.first(forms__$1);
var threaded = ((cljs.core.seq_QMARK_(form))?cljs.core.with_meta(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,cljs.core.first(form),(new cljs.core.List(null,x__$1,null,(1),null)),(2),null)),cljs.core.next(form)),cljs.core.meta(form)):(new cljs.core.List(null,form,(new cljs.core.List(null,x__$1,null,(1),null)),(2),null)));
var G__88461 = threaded;
var G__88462 = cljs.core.next(forms__$1);
x__$1 = G__88461;
forms__$1 = G__88462;
continue;
} else {
return x__$1;
}
break;
}
})();
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,expanded) : sci.impl.analyzer.macroexpand.call(null,ctx,expanded));
});
/**
 * The ->> macro from clojure.core.
 */
sci.impl.analyzer.expand__GT__GT_ = (function sci$impl$analyzer$expand__GT__GT_(ctx,p__88244){
var vec__88245 = p__88244;
var seq__88246 = cljs.core.seq(vec__88245);
var first__88247 = cljs.core.first(seq__88246);
var seq__88246__$1 = cljs.core.next(seq__88246);
var x = first__88247;
var forms = seq__88246__$1;
var expanded = (function (){var x__$1 = x;
var forms__$1 = forms;
while(true){
if(forms__$1){
var form = cljs.core.first(forms__$1);
var threaded = ((cljs.core.seq_QMARK_(form))?cljs.core.with_meta(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.cons(cljs.core.first(form),cljs.core.next(form)),(new cljs.core.List(null,x__$1,null,(1),null))),cljs.core.meta(form)):(new cljs.core.List(null,form,(new cljs.core.List(null,x__$1,null,(1),null)),(2),null)));
var G__88470 = threaded;
var G__88471 = cljs.core.next(forms__$1);
x__$1 = G__88470;
forms__$1 = G__88471;
continue;
} else {
return x__$1;
}
break;
}
})();
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,expanded) : sci.impl.analyzer.macroexpand.call(null,ctx,expanded));
});
/**
 * The ->> macro from clojure.core.
 */
sci.impl.analyzer.expand_as__GT_ = (function sci$impl$analyzer$expand_as__GT_(ctx,p__88257){
var vec__88258 = p__88257;
var seq__88259 = cljs.core.seq(vec__88258);
var first__88260 = cljs.core.first(seq__88259);
var seq__88259__$1 = cljs.core.next(seq__88259);
var _as = first__88260;
var first__88260__$1 = cljs.core.first(seq__88259__$1);
var seq__88259__$2 = cljs.core.next(seq__88259__$1);
var expr = first__88260__$1;
var first__88260__$2 = cljs.core.first(seq__88259__$2);
var seq__88259__$3 = cljs.core.next(seq__88259__$2);
var name = first__88260__$2;
var forms = seq__88259__$3;
var vec__88261 = cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,name,null,(1),null)),(new cljs.core.List(null,expr,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.interleave.cljs$core$IFn$_invoke$arity$2(cljs.core.repeat.cljs$core$IFn$_invoke$arity$1(name),cljs.core.butlast(forms))], 0))))),null,(1),null)),(new cljs.core.List(null,((cljs.core.empty_QMARK_(forms))?name:cljs.core.last(forms)),null,(1),null)))));
var seq__88262 = cljs.core.seq(vec__88261);
var first__88263 = cljs.core.first(seq__88262);
var seq__88262__$1 = cljs.core.next(seq__88262);
var let_bindings = first__88263;
var body = seq__88262__$1;
return sci.impl.analyzer.expand_let_STAR_(ctx,let_bindings,body);
});
sci.impl.analyzer.expand_def = (function sci$impl$analyzer$expand_def(ctx,p__88267){
var vec__88268 = p__88267;
var _def = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88268,(0),null);
var var_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88268,(1),null);
var _QMARK_docstring = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88268,(2),null);
var _QMARK_init = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88268,(3),null);
var docstring = (cljs.core.truth_(_QMARK_init)?_QMARK_docstring:null);
var init = (cljs.core.truth_(docstring)?_QMARK_init:_QMARK_docstring);
var init__$1 = (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,init) : sci.impl.analyzer.macroexpand.call(null,ctx,init));
var m = (cljs.core.truth_(docstring)?new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("sci","doc","sci/doc",1913179154),docstring], null):cljs.core.PersistentArrayMap.EMPTY);
var var_name__$1 = cljs.core.with_meta(var_name,m);
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx),cljs.core.assoc,var_name__$1,new cljs.core.Keyword("sci","var.unbound","sci/var.unbound",1718616093));

return sci.impl.utils.mark_eval_call((new cljs.core.List(null,new cljs.core.Symbol(null,"def","def",597100991,null),(new cljs.core.List(null,var_name__$1,(new cljs.core.List(null,init__$1,null,(1),null)),(2),null)),(3),null)));
});
sci.impl.analyzer.expand_defn = (function sci$impl$analyzer$expand_defn(ctx,p__88277){
var vec__88281 = p__88277;
var seq__88282 = cljs.core.seq(vec__88281);
var first__88283 = cljs.core.first(seq__88282);
var seq__88282__$1 = cljs.core.next(seq__88282);
var op = first__88283;
var first__88283__$1 = cljs.core.first(seq__88282__$1);
var seq__88282__$2 = cljs.core.next(seq__88282__$1);
var fn_name = first__88283__$1;
var first__88283__$2 = cljs.core.first(seq__88282__$2);
var seq__88282__$3 = cljs.core.next(seq__88282__$2);
var docstring_QMARK_ = first__88283__$2;
var body = seq__88282__$3;
var G__88284_88480 = ctx;
var G__88285_88481 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,fn_name], null);
(sci.impl.analyzer.expand_declare.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.expand_declare.cljs$core$IFn$_invoke$arity$2(G__88284_88480,G__88285_88481) : sci.impl.analyzer.expand_declare.call(null,G__88284_88480,G__88285_88481));

var macro_QMARK_ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"defmacro","defmacro",2054157304,null),op);
var docstring = ((typeof docstring_QMARK_ === 'string')?docstring_QMARK_:null);
var body__$1 = (cljs.core.truth_(docstring)?body:cljs.core.cons(docstring_QMARK_,body));
var fn_body = cljs.core.list_STAR_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"fn","fn",465265323,null),body__$1);
var f = sci.impl.analyzer.expand_fn(ctx,fn_body,macro_QMARK_);
var f__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(f,new cljs.core.Keyword("sci","macro","sci/macro",-868536151),macro_QMARK_,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("sci.impl","fn-name","sci.impl/fn-name",-1172300569),fn_name], 0));
return sci.impl.utils.mark_eval_call((new cljs.core.List(null,new cljs.core.Symbol(null,"def","def",597100991,null),(new cljs.core.List(null,fn_name,(new cljs.core.List(null,f__$1,null,(1),null)),(2),null)),(3),null)));
});
/**
 * The comment macro from clojure.core.
 */
sci.impl.analyzer.expand_comment = (function sci$impl$analyzer$expand_comment(var_args){
var args__4736__auto__ = [];
var len__4730__auto___88485 = arguments.length;
var i__4731__auto___88486 = (0);
while(true){
if((i__4731__auto___88486 < len__4730__auto___88485)){
args__4736__auto__.push((arguments[i__4731__auto___88486]));

var G__88487 = (i__4731__auto___88486 + (1));
i__4731__auto___88486 = G__88487;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return sci.impl.analyzer.expand_comment.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

sci.impl.analyzer.expand_comment.cljs$core$IFn$_invoke$arity$variadic = (function (_ctx,_body){
return null;
});

sci.impl.analyzer.expand_comment.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
sci.impl.analyzer.expand_comment.cljs$lang$applyTo = (function (seq88293){
var G__88294 = cljs.core.first(seq88293);
var seq88293__$1 = cljs.core.next(seq88293);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__88294,seq88293__$1);
});

sci.impl.analyzer.expand_loop = (function sci$impl$analyzer$expand_loop(ctx,expr){
var bv = cljs.core.second(expr);
var arg_names = cljs.core.take_nth.cljs$core$IFn$_invoke$arity$2((2),bv);
var init_vals = cljs.core.take_nth.cljs$core$IFn$_invoke$arity$2((2),cljs.core.rest(bv));
var body = cljs.core.nnext(expr);
var G__88299 = ctx;
var G__88300 = cljs.core.apply.cljs$core$IFn$_invoke$arity$3(cljs.core.list,(new cljs.core.List(null,new cljs.core.Symbol(null,"fn","fn",465265323,null),(new cljs.core.List(null,cljs.core.vec(arg_names),(new cljs.core.List(null,cljs.core.cons(new cljs.core.Symbol(null,"do","do",1686842252,null),body),null,(1),null)),(2),null)),(3),null)),init_vals);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88299,G__88300) : sci.impl.analyzer.macroexpand.call(null,G__88299,G__88300));
});
sci.impl.analyzer.expand_lazy_seq = (function sci$impl$analyzer$expand_lazy_seq(ctx,expr){
var body = cljs.core.rest(expr);
return sci.impl.utils.mark_eval_call((new cljs.core.List(null,new cljs.core.Symbol(null,"lazy-seq","lazy-seq",489632906,null),(new cljs.core.List(null,(function (){var G__88302 = ctx;
var G__88303 = (new cljs.core.List(null,new cljs.core.Symbol(null,"fn","fn",465265323,null),(new cljs.core.List(null,cljs.core.PersistentVector.EMPTY,(new cljs.core.List(null,cljs.core.cons(new cljs.core.Symbol(null,"do","do",1686842252,null),body),null,(1),null)),(2),null)),(3),null));
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88302,G__88303) : sci.impl.analyzer.macroexpand.call(null,G__88302,G__88303));
})(),null,(1),null)),(2),null)));
});
/**
 * The cond macro from clojure.core
 */
sci.impl.analyzer.expand_cond_STAR_ = (function sci$impl$analyzer$expand_cond_STAR_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___88492 = arguments.length;
var i__4731__auto___88493 = (0);
while(true){
if((i__4731__auto___88493 < len__4730__auto___88492)){
args__4736__auto__.push((arguments[i__4731__auto___88493]));

var G__88494 = (i__4731__auto___88493 + (1));
i__4731__auto___88493 = G__88494;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return sci.impl.analyzer.expand_cond_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

sci.impl.analyzer.expand_cond_STAR_.cljs$core$IFn$_invoke$arity$variadic = (function (clauses){
if(cljs.core.truth_(clauses)){
return (new cljs.core.List(null,new cljs.core.Symbol(null,"if","if",1181717262,null),(new cljs.core.List(null,cljs.core.first(clauses),(new cljs.core.List(null,((cljs.core.next(clauses))?cljs.core.second(clauses):(function(){throw (new Error("cond requires an even number of forms"))})()),(new cljs.core.List(null,cljs.core.apply.cljs$core$IFn$_invoke$arity$2(sci.impl.analyzer.expand_cond_STAR_,cljs.core.next(cljs.core.next(clauses))),null,(1),null)),(2),null)),(3),null)),(4),null));
} else {
return null;
}
});

sci.impl.analyzer.expand_cond_STAR_.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
sci.impl.analyzer.expand_cond_STAR_.cljs$lang$applyTo = (function (seq88304){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq88304));
});

sci.impl.analyzer.expand_cond = (function sci$impl$analyzer$expand_cond(ctx,expr){
var clauses = cljs.core.rest(expr);
var G__88311 = ctx;
var G__88312 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(sci.impl.analyzer.expand_cond_STAR_,clauses);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88311,G__88312) : sci.impl.analyzer.macroexpand.call(null,G__88311,G__88312));
});
sci.impl.analyzer.expand_case = (function sci$impl$analyzer$expand_case(ctx,expr){
var v = (function (){var G__88314 = ctx;
var G__88315 = cljs.core.second(expr);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88314,G__88315) : sci.impl.analyzer.macroexpand.call(null,G__88314,G__88315));
})();
var clauses = cljs.core.nnext(expr);
var match_clauses = cljs.core.take_nth.cljs$core$IFn$_invoke$arity$2((2),clauses);
var result_clauses = cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (v,clauses,match_clauses){
return (function (p1__88313_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88313_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88313_SHARP_));
});})(v,clauses,match_clauses))
,cljs.core.take_nth.cljs$core$IFn$_invoke$arity$2((2),cljs.core.rest(clauses)));
var default$ = ((cljs.core.odd_QMARK_(cljs.core.count(clauses)))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"val","val",128701612),(function (){var G__88316 = ctx;
var G__88317 = cljs.core.last(clauses);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88316,G__88317) : sci.impl.analyzer.macroexpand.call(null,G__88316,G__88317));
})()], null):null);
var case_map = cljs.core.zipmap(match_clauses,result_clauses);
var ret = sci.impl.utils.mark_eval_call((new cljs.core.List(null,new cljs.core.Symbol(null,"case","case",-1510733573,null),(new cljs.core.List(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"case-map","case-map",955082964),case_map,new cljs.core.Keyword(null,"case-val","case-val",880926521),v,new cljs.core.Keyword(null,"case-default","case-default",1140470708),default$], null),(new cljs.core.List(null,default$,null,(1),null)),(2),null)),(3),null)));
return sci.impl.utils.mark_eval_call(ret);
});
sci.impl.analyzer.expand_try = (function sci$impl$analyzer$expand_try(ctx,expr){
var catches = cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p1__88318_SHARP_){
return ((cljs.core.seq_QMARK_(p1__88318_SHARP_)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"catch","catch",-1616370245,null),cljs.core.first(p1__88318_SHARP_))));
}),expr);
var catches__$1 = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2(((function (catches){
return (function (c){
var vec__88321 = c;
var seq__88322 = cljs.core.seq(vec__88321);
var first__88323 = cljs.core.first(seq__88322);
var seq__88322__$1 = cljs.core.next(seq__88322);
var _ = first__88323;
var first__88323__$1 = cljs.core.first(seq__88322__$1);
var seq__88322__$2 = cljs.core.next(seq__88322__$1);
var ex = first__88323__$1;
var first__88323__$2 = cljs.core.first(seq__88322__$2);
var seq__88322__$3 = cljs.core.next(seq__88322__$2);
var binding = first__88323__$2;
var body = seq__88322__$3;
var clazz = sci.impl.analyzer.resolve_symbol(ctx,ex);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"class","class",-2030961996),clazz,new cljs.core.Keyword(null,"binding","binding",539932593),binding,new cljs.core.Keyword(null,"body","body",-2049205669),(function (){var G__88324 = cljs.core.assoc_in(ctx,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bindings","bindings",1271397192),binding], null),null);
var G__88325 = cljs.core.cons(new cljs.core.Symbol(null,"do","do",1686842252,null),body);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88324,G__88325) : sci.impl.analyzer.macroexpand.call(null,G__88324,G__88325));
})()], null);
});})(catches))
,catches);
var finally$ = (function (){var l = cljs.core.last(expr);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"finally","finally",-1065347064,null),cljs.core.first(l))){
var G__88326 = ctx;
var G__88327 = cljs.core.cons(new cljs.core.Symbol(null,"do","do",1686842252,null),cljs.core.rest(l));
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88326,G__88327) : sci.impl.analyzer.macroexpand.call(null,G__88326,G__88327));
} else {
return null;
}
})();
return sci.impl.utils.mark_eval(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("sci.impl","try","sci.impl/try",2142624741),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"body","body",-2049205669),(function (){var G__88328 = ctx;
var G__88329 = cljs.core.second(expr);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88328,G__88329) : sci.impl.analyzer.macroexpand.call(null,G__88328,G__88329));
})(),new cljs.core.Keyword(null,"catches","catches",-1478797617),catches__$1,new cljs.core.Keyword(null,"finally","finally",1589088705),finally$], null)], null));
});
sci.impl.analyzer.expand_syntax_quote = (function sci$impl$analyzer$expand_syntax_quote(ctx,expr){
var ret = clojure.walk.prewalk((function (x){
if(cljs.core.seq_QMARK_(x)){
var G__88333 = cljs.core.first(x);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"unquote","unquote",-1004694737,null),G__88333)){
var G__88335 = ctx;
var G__88336 = cljs.core.second(x);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88335,G__88336) : sci.impl.analyzer.macroexpand.call(null,G__88335,G__88336));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"unquote-splicing","unquote-splicing",-1359168213,null),G__88333)){
return cljs.core.vary_meta.cljs$core$IFn$_invoke$arity$2((function (){var G__88337 = ctx;
var G__88338 = cljs.core.second(x);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88337,G__88338) : sci.impl.analyzer.macroexpand.call(null,G__88337,G__88338));
})(),((function (G__88333){
return (function (m){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,new cljs.core.Keyword("sci.impl","unquote-splicing","sci.impl/unquote-splicing",2047521727),true);
});})(G__88333))
);
} else {
return x;

}
}
} else {
return x;
}
}),cljs.core.second(expr));
return sci.impl.utils.mark_eval_call((new cljs.core.List(null,new cljs.core.Symbol(null,"syntax-quote","syntax-quote",407366680,null),(new cljs.core.List(null,ret,null,(1),null)),(2),null)));
});
sci.impl.analyzer.expand_declare = (function sci$impl$analyzer$expand_declare(ctx,p__88346){
var vec__88347 = p__88346;
var seq__88348 = cljs.core.seq(vec__88347);
var first__88349 = cljs.core.first(seq__88348);
var seq__88348__$1 = cljs.core.next(seq__88348);
var _declare = first__88349;
var names = seq__88348__$1;
var _expr = vec__88347;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx),((function (vec__88347,seq__88348,first__88349,seq__88348__$1,_declare,names,_expr){
return (function (env){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.zipmap(names,cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (vec__88347,seq__88348,first__88349,seq__88348__$1,_declare,names,_expr){
return (function (n){
return cljs.core.vary_meta.cljs$core$IFn$_invoke$arity$2(sci.impl.utils.mark_eval(n),((function (vec__88347,seq__88348,first__88349,seq__88348__$1,_declare,names,_expr){
return (function (p1__88341_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__88341_SHARP_,new cljs.core.Keyword("sci.impl","var.declared","sci.impl/var.declared",874674867),true);
});})(vec__88347,seq__88348,first__88349,seq__88348__$1,_declare,names,_expr))
);
});})(vec__88347,seq__88348,first__88349,seq__88348__$1,_declare,names,_expr))
,names)),env], 0));
});})(vec__88347,seq__88348,first__88349,seq__88348__$1,_declare,names,_expr))
);

return null;
});
sci.impl.analyzer.macro_QMARK_ = (function sci$impl$analyzer$macro_QMARK_(f){
var temp__5735__auto__ = cljs.core.meta(f);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return new cljs.core.Keyword("sci","macro","sci/macro",-868536151).cljs$core$IFn$_invoke$arity$1(m);
} else {
return null;
}
});
sci.impl.analyzer.macroexpand_call = (function sci$impl$analyzer$macroexpand_call(ctx,expr){
if(cljs.core.empty_QMARK_(expr)){
return expr;
} else {
var f = cljs.core.first(expr);
if((f instanceof cljs.core.Symbol)){
var f__$1 = (function (){var temp__5733__auto__ = cljs.core.namespace(f);
if(cljs.core.truth_(temp__5733__auto__)){
var ns = temp__5733__auto__;
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("clojure.core",ns)) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("cljs.core",ns)))){
return cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(cljs.core.name(f));
} else {
return f;
}
} else {
return f;
}
})();
if(cljs.core.contains_QMARK_(sci.impl.analyzer.macros,f__$1)){
sci.impl.analyzer.check_permission_BANG_(ctx,f__$1,f__$1);

var G__88374 = f__$1;
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"case","case",-1510733573,null),G__88374)){
return sci.impl.analyzer.expand_case(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"fn*","fn*",-752876845,null),G__88374)){
return sci.impl.analyzer.expand_fn(ctx,expr,false);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"defmacro","defmacro",2054157304,null),G__88374)){
var ret = sci.impl.analyzer.expand_defn(ctx,expr);
return ret;
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"defn","defn",-126010802,null),G__88374)){
var ret = sci.impl.analyzer.expand_defn(ctx,expr);
return ret;
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"comment","comment",-2122229700,null),G__88374)){
return sci.impl.analyzer.expand_comment.cljs$core$IFn$_invoke$arity$variadic(ctx,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([expr], 0));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"do","do",1686842252,null),G__88374)){
return sci.impl.utils.mark_eval_call(expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"loop","loop",1244978678,null),G__88374)){
return sci.impl.analyzer.expand_loop(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"->>","->>",-1874332161,null),G__88374)){
return sci.impl.analyzer.expand__GT__GT_(ctx,cljs.core.rest(expr));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"doseq","doseq",221164135,null),G__88374)){
var G__88379 = ctx;
var G__88380 = sci.impl.doseq_macro.expand_doseq(ctx,expr);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88379,G__88380) : sci.impl.analyzer.macroexpand.call(null,G__88379,G__88380));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"def","def",597100991,null),G__88374)){
return sci.impl.analyzer.expand_def(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"declare","declare",654042991,null),G__88374)){
return sci.impl.analyzer.expand_declare(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),G__88374)){

return cljs.core.second(expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"for","for",316745208,null),G__88374)){
var G__88381 = ctx;
var G__88382 = sci.impl.for_macro.expand_for(ctx,expr);
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(G__88381,G__88382) : sci.impl.analyzer.macroexpand.call(null,G__88381,G__88382));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"cond","cond",1606708055,null),G__88374)){
return sci.impl.analyzer.expand_cond(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"let","let",358118826,null),G__88374)){
return sci.impl.analyzer.expand_let(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"fn","fn",465265323,null),G__88374)){
return sci.impl.analyzer.expand_fn(ctx,expr,false);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"require","require",1172530194,null),G__88374)){
return sci.impl.utils.mark_eval_call(cljs.core.cons(new cljs.core.Symbol(null,"require","require",1172530194,null),cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (G__88374,f__$1,f){
return (function (p1__88369_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88369_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88369_SHARP_));
});})(G__88374,f__$1,f))
,cljs.core.rest(expr))));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"->","->",-2139605430,null),G__88374)){
return sci.impl.analyzer.expand__GT_(ctx,cljs.core.rest(expr));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"as->","as->",1430690540,null),G__88374)){
return sci.impl.analyzer.expand_as__GT_(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"lazy-seq","lazy-seq",489632906,null),G__88374)){
return sci.impl.analyzer.expand_lazy_seq(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"syntax-quote","syntax-quote",407366680,null),G__88374)){
return sci.impl.analyzer.expand_syntax_quote(ctx,expr);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"try","try",-1273693247,null),G__88374)){
return sci.impl.analyzer.expand_try(ctx,expr);
} else {
return sci.impl.utils.mark_eval_call(cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (G__88374,f__$1,f){
return (function (p1__88370_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88370_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88370_SHARP_));
});})(G__88374,f__$1,f))
,expr)));

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
} else {
var temp__5733__auto__ = sci.impl.analyzer.resolve_symbol(ctx,f__$1);
if(cljs.core.truth_(temp__5733__auto__)){
var vf = temp__5733__auto__;
try{if(cljs.core.truth_(sci.impl.analyzer.macro_QMARK_(vf))){
var v = cljs.core.apply.cljs$core$IFn$_invoke$arity$4(vf,expr,new cljs.core.Keyword(null,"bindings","bindings",1271397192).cljs$core$IFn$_invoke$arity$1(ctx),cljs.core.rest(expr));
var expanded = (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,v) : sci.impl.analyzer.macroexpand.call(null,ctx,v));
return expanded;
} else {
return sci.impl.utils.mark_eval_call(cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (vf,temp__5733__auto__,f__$1,f){
return (function (p1__88371_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88371_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88371_SHARP_));
});})(vf,temp__5733__auto__,f__$1,f))
,expr)));
}
}catch (e88389){if((e88389 instanceof Error)){
var e = e88389;
return sci.impl.utils.rethrow_with_location_of_node(ctx,e,expr);
} else {
throw e88389;

}
}} else {
return sci.impl.utils.mark_eval_call(cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (temp__5733__auto__,f__$1,f){
return (function (p1__88372_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88372_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88372_SHARP_));
});})(temp__5733__auto__,f__$1,f))
,expr)));
}
}
} else {
var ret = sci.impl.utils.mark_eval_call(cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (f){
return (function (p1__88373_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88373_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88373_SHARP_));
});})(f))
,expr)));
return ret;
}
}
});
sci.impl.analyzer.macroexpand = (function sci$impl$analyzer$macroexpand(ctx,expr){
var ret = ((sci.impl.utils.constant_QMARK_(expr))?expr:(((expr instanceof cljs.core.Symbol))?(function (){var v = sci.impl.analyzer.resolve_symbol(ctx,expr);
if(sci.impl.utils.kw_identical_QMARK_(new cljs.core.Keyword("sci","var.unbound","sci/var.unbound",1718616093),v)){
return null;
} else {
if(sci.impl.utils.constant_QMARK_(v)){
return v;
} else {
if(cljs.core.fn_QMARK_(v)){
return sci.impl.utils.merge_meta(v,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("sci.impl","eval","sci.impl/eval",-210871022),false], null));
} else {
return sci.impl.utils.merge_meta(v,cljs.core.meta(expr));

}
}
}
})():sci.impl.utils.merge_meta((cljs.core.truth_(new cljs.core.Keyword("sci.impl","fn","sci.impl/fn",1695180073).cljs$core$IFn$_invoke$arity$1(expr))?sci.impl.analyzer.expand_fn_literal_body(ctx,expr):((cljs.core.map_QMARK_(expr))?sci.impl.utils.mark_eval(cljs.core.zipmap(cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__88393_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88393_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88393_SHARP_));
}),cljs.core.keys(expr)),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__88394_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88394_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88394_SHARP_));
}),cljs.core.vals(expr)))):((((cljs.core.vector_QMARK_(expr)) || (cljs.core.set_QMARK_(expr))))?sci.impl.utils.mark_eval(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.empty(expr),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__88403_SHARP_){
return (sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2 ? sci.impl.analyzer.macroexpand.cljs$core$IFn$_invoke$arity$2(ctx,p1__88403_SHARP_) : sci.impl.analyzer.macroexpand.call(null,ctx,p1__88403_SHARP_));
}),expr))):((cljs.core.seq_QMARK_(expr))?sci.impl.analyzer.macroexpand_call(ctx,expr):expr
)))),cljs.core.select_keys(cljs.core.meta(expr),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"row","row",-570139521),new cljs.core.Keyword(null,"col","col",-1959363084)], null)))
));
return ret;
});

//# sourceMappingURL=sci.impl.analyzer.js.map
