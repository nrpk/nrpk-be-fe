goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('goog.array');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__80913 = arguments.length;
switch (G__80913) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async80915 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async80915 = (function (f,blockable,meta80916){
this.f = f;
this.blockable = blockable;
this.meta80916 = meta80916;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async80915.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_80917,meta80916__$1){
var self__ = this;
var _80917__$1 = this;
return (new cljs.core.async.t_cljs$core$async80915(self__.f,self__.blockable,meta80916__$1));
});

cljs.core.async.t_cljs$core$async80915.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_80917){
var self__ = this;
var _80917__$1 = this;
return self__.meta80916;
});

cljs.core.async.t_cljs$core$async80915.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async80915.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async80915.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async80915.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async80915.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta80916","meta80916",-1631882054,null)], null);
});

cljs.core.async.t_cljs$core$async80915.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async80915.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async80915";

cljs.core.async.t_cljs$core$async80915.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async80915");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async80915.
 */
cljs.core.async.__GT_t_cljs$core$async80915 = (function cljs$core$async$__GT_t_cljs$core$async80915(f__$1,blockable__$1,meta80916){
return (new cljs.core.async.t_cljs$core$async80915(f__$1,blockable__$1,meta80916));
});

}

return (new cljs.core.async.t_cljs$core$async80915(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__80937 = arguments.length;
switch (G__80937) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__80945 = arguments.length;
switch (G__80945) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__80961 = arguments.length;
switch (G__80961) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_83002 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_83002) : fn1.call(null,val_83002));
} else {
cljs.core.async.impl.dispatch.run(((function (val_83002,ret){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_83002) : fn1.call(null,val_83002));
});})(val_83002,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__80973 = arguments.length;
switch (G__80973) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run(((function (ret,retb,temp__5733__auto__){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
});})(ret,retb,temp__5733__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4607__auto___83011 = n;
var x_83012 = (0);
while(true){
if((x_83012 < n__4607__auto___83011)){
(a[x_83012] = x_83012);

var G__83013 = (x_83012 + (1));
x_83012 = G__83013;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async80985 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async80985 = (function (flag,meta80986){
this.flag = flag;
this.meta80986 = meta80986;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async80985.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_80987,meta80986__$1){
var self__ = this;
var _80987__$1 = this;
return (new cljs.core.async.t_cljs$core$async80985(self__.flag,meta80986__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async80985.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_80987){
var self__ = this;
var _80987__$1 = this;
return self__.meta80986;
});})(flag))
;

cljs.core.async.t_cljs$core$async80985.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async80985.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async80985.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async80985.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async80985.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta80986","meta80986",77672329,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async80985.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async80985.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async80985";

cljs.core.async.t_cljs$core$async80985.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async80985");
});})(flag))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async80985.
 */
cljs.core.async.__GT_t_cljs$core$async80985 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async80985(flag__$1,meta80986){
return (new cljs.core.async.t_cljs$core$async80985(flag__$1,meta80986));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async80985(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async81000 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async81000 = (function (flag,cb,meta81001){
this.flag = flag;
this.cb = cb;
this.meta81001 = meta81001;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async81000.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_81002,meta81001__$1){
var self__ = this;
var _81002__$1 = this;
return (new cljs.core.async.t_cljs$core$async81000(self__.flag,self__.cb,meta81001__$1));
});

cljs.core.async.t_cljs$core$async81000.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_81002){
var self__ = this;
var _81002__$1 = this;
return self__.meta81001;
});

cljs.core.async.t_cljs$core$async81000.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async81000.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
});

cljs.core.async.t_cljs$core$async81000.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async81000.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async81000.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta81001","meta81001",-1690528517,null)], null);
});

cljs.core.async.t_cljs$core$async81000.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async81000.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async81000";

cljs.core.async.t_cljs$core$async81000.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async81000");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async81000.
 */
cljs.core.async.__GT_t_cljs$core$async81000 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async81000(flag__$1,cb__$1,meta81001){
return (new cljs.core.async.t_cljs$core$async81000(flag__$1,cb__$1,meta81001));
});

}

return (new cljs.core.async.t_cljs$core$async81000(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__81010_SHARP_){
var G__81017 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__81010_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__81017) : fret.call(null,G__81017));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__81011_SHARP_){
var G__81018 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__81011_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__81018) : fret.call(null,G__81018));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4131__auto__ = wport;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return port;
}
})()], null));
} else {
var G__83085 = (i + (1));
i = G__83085;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4131__auto__ = ret;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4120__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4120__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4120__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___83090 = arguments.length;
var i__4731__auto___83091 = (0);
while(true){
if((i__4731__auto___83091 < len__4730__auto___83090)){
args__4736__auto__.push((arguments[i__4731__auto___83091]));

var G__83092 = (i__4731__auto___83091 + (1));
i__4731__auto___83091 = G__83092;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__81028){
var map__81029 = p__81028;
var map__81029__$1 = (((((!((map__81029 == null))))?(((((map__81029.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__81029.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__81029):map__81029);
var opts = map__81029__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq81023){
var G__81024 = cljs.core.first(seq81023);
var seq81023__$1 = cljs.core.next(seq81023);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__81024,seq81023__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__81037 = arguments.length;
switch (G__81037) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__80824__auto___83102 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83102){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83102){
return (function (state_81092){
var state_val_81093 = (state_81092[(1)]);
if((state_val_81093 === (7))){
var inst_81088 = (state_81092[(2)]);
var state_81092__$1 = state_81092;
var statearr_81116_83103 = state_81092__$1;
(statearr_81116_83103[(2)] = inst_81088);

(statearr_81116_83103[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (1))){
var state_81092__$1 = state_81092;
var statearr_81117_83105 = state_81092__$1;
(statearr_81117_83105[(2)] = null);

(statearr_81117_83105[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (4))){
var inst_81064 = (state_81092[(7)]);
var inst_81064__$1 = (state_81092[(2)]);
var inst_81068 = (inst_81064__$1 == null);
var state_81092__$1 = (function (){var statearr_81118 = state_81092;
(statearr_81118[(7)] = inst_81064__$1);

return statearr_81118;
})();
if(cljs.core.truth_(inst_81068)){
var statearr_81119_83106 = state_81092__$1;
(statearr_81119_83106[(1)] = (5));

} else {
var statearr_81120_83107 = state_81092__$1;
(statearr_81120_83107[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (13))){
var state_81092__$1 = state_81092;
var statearr_81123_83108 = state_81092__$1;
(statearr_81123_83108[(2)] = null);

(statearr_81123_83108[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (6))){
var inst_81064 = (state_81092[(7)]);
var state_81092__$1 = state_81092;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81092__$1,(11),to,inst_81064);
} else {
if((state_val_81093 === (3))){
var inst_81090 = (state_81092[(2)]);
var state_81092__$1 = state_81092;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81092__$1,inst_81090);
} else {
if((state_val_81093 === (12))){
var state_81092__$1 = state_81092;
var statearr_81129_83112 = state_81092__$1;
(statearr_81129_83112[(2)] = null);

(statearr_81129_83112[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (2))){
var state_81092__$1 = state_81092;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81092__$1,(4),from);
} else {
if((state_val_81093 === (11))){
var inst_81078 = (state_81092[(2)]);
var state_81092__$1 = state_81092;
if(cljs.core.truth_(inst_81078)){
var statearr_81130_83113 = state_81092__$1;
(statearr_81130_83113[(1)] = (12));

} else {
var statearr_81131_83114 = state_81092__$1;
(statearr_81131_83114[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (9))){
var state_81092__$1 = state_81092;
var statearr_81132_83115 = state_81092__$1;
(statearr_81132_83115[(2)] = null);

(statearr_81132_83115[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (5))){
var state_81092__$1 = state_81092;
if(cljs.core.truth_(close_QMARK_)){
var statearr_81136_83119 = state_81092__$1;
(statearr_81136_83119[(1)] = (8));

} else {
var statearr_81139_83120 = state_81092__$1;
(statearr_81139_83120[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (14))){
var inst_81086 = (state_81092[(2)]);
var state_81092__$1 = state_81092;
var statearr_81143_83121 = state_81092__$1;
(statearr_81143_83121[(2)] = inst_81086);

(statearr_81143_83121[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (10))){
var inst_81075 = (state_81092[(2)]);
var state_81092__$1 = state_81092;
var statearr_81144_83123 = state_81092__$1;
(statearr_81144_83123[(2)] = inst_81075);

(statearr_81144_83123[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81093 === (8))){
var inst_81071 = cljs.core.async.close_BANG_(to);
var state_81092__$1 = state_81092;
var statearr_81145_83124 = state_81092__$1;
(statearr_81145_83124[(2)] = inst_81071);

(statearr_81145_83124[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83102))
;
return ((function (switch__80630__auto__,c__80824__auto___83102){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_81149 = [null,null,null,null,null,null,null,null];
(statearr_81149[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_81149[(1)] = (1));

return statearr_81149;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_81092){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81092);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81150){if((e81150 instanceof Object)){
var ex__80634__auto__ = e81150;
var statearr_81151_83127 = state_81092;
(statearr_81151_83127[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81092);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81150;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83129 = state_81092;
state_81092 = G__83129;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_81092){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_81092);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83102))
})();
var state__80826__auto__ = (function (){var statearr_81152 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81152[(6)] = c__80824__auto___83102);

return statearr_81152;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83102))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = ((function (jobs,results){
return (function (p__81156){
var vec__81157 = p__81156;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__81157,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__81157,(1),null);
var job = vec__81157;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__80824__auto___83135 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83135,res,vec__81157,v,p,job,jobs,results){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83135,res,vec__81157,v,p,job,jobs,results){
return (function (state_81167){
var state_val_81168 = (state_81167[(1)]);
if((state_val_81168 === (1))){
var state_81167__$1 = state_81167;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81167__$1,(2),res,v);
} else {
if((state_val_81168 === (2))){
var inst_81164 = (state_81167[(2)]);
var inst_81165 = cljs.core.async.close_BANG_(res);
var state_81167__$1 = (function (){var statearr_81169 = state_81167;
(statearr_81169[(7)] = inst_81164);

return statearr_81169;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_81167__$1,inst_81165);
} else {
return null;
}
}
});})(c__80824__auto___83135,res,vec__81157,v,p,job,jobs,results))
;
return ((function (switch__80630__auto__,c__80824__auto___83135,res,vec__81157,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0 = (function (){
var statearr_81170 = [null,null,null,null,null,null,null,null];
(statearr_81170[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__);

(statearr_81170[(1)] = (1));

return statearr_81170;
});
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1 = (function (state_81167){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81167);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81174){if((e81174 instanceof Object)){
var ex__80634__auto__ = e81174;
var statearr_81177_83137 = state_81167;
(statearr_81177_83137[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81167);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81174;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83138 = state_81167;
state_81167 = G__83138;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = function(state_81167){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1.call(this,state_81167);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83135,res,vec__81157,v,p,job,jobs,results))
})();
var state__80826__auto__ = (function (){var statearr_81179 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81179[(6)] = c__80824__auto___83135);

return statearr_81179;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83135,res,vec__81157,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__81180){
var vec__81181 = p__81180;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__81181,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__81181,(1),null);
var job = vec__81181;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results,process))
;
var n__4607__auto___83143 = n;
var __83144 = (0);
while(true){
if((__83144 < n__4607__auto___83143)){
var G__81184_83145 = type;
var G__81184_83146__$1 = (((G__81184_83145 instanceof cljs.core.Keyword))?G__81184_83145.fqn:null);
switch (G__81184_83146__$1) {
case "compute":
var c__80824__auto___83148 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__83144,c__80824__auto___83148,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (__83144,c__80824__auto___83148,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async){
return (function (state_81201){
var state_val_81202 = (state_81201[(1)]);
if((state_val_81202 === (1))){
var state_81201__$1 = state_81201;
var statearr_81203_83149 = state_81201__$1;
(statearr_81203_83149[(2)] = null);

(statearr_81203_83149[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81202 === (2))){
var state_81201__$1 = state_81201;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81201__$1,(4),jobs);
} else {
if((state_val_81202 === (3))){
var inst_81199 = (state_81201[(2)]);
var state_81201__$1 = state_81201;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81201__$1,inst_81199);
} else {
if((state_val_81202 === (4))){
var inst_81191 = (state_81201[(2)]);
var inst_81192 = process(inst_81191);
var state_81201__$1 = state_81201;
if(cljs.core.truth_(inst_81192)){
var statearr_81208_83151 = state_81201__$1;
(statearr_81208_83151[(1)] = (5));

} else {
var statearr_81211_83152 = state_81201__$1;
(statearr_81211_83152[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81202 === (5))){
var state_81201__$1 = state_81201;
var statearr_81213_83153 = state_81201__$1;
(statearr_81213_83153[(2)] = null);

(statearr_81213_83153[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81202 === (6))){
var state_81201__$1 = state_81201;
var statearr_81218_83156 = state_81201__$1;
(statearr_81218_83156[(2)] = null);

(statearr_81218_83156[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81202 === (7))){
var inst_81197 = (state_81201[(2)]);
var state_81201__$1 = state_81201;
var statearr_81219_83157 = state_81201__$1;
(statearr_81219_83157[(2)] = inst_81197);

(statearr_81219_83157[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__83144,c__80824__auto___83148,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async))
;
return ((function (__83144,switch__80630__auto__,c__80824__auto___83148,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0 = (function (){
var statearr_81220 = [null,null,null,null,null,null,null];
(statearr_81220[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__);

(statearr_81220[(1)] = (1));

return statearr_81220;
});
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1 = (function (state_81201){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81201);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81221){if((e81221 instanceof Object)){
var ex__80634__auto__ = e81221;
var statearr_81222_83161 = state_81201;
(statearr_81222_83161[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81201);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81221;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83166 = state_81201;
state_81201 = G__83166;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = function(state_81201){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1.call(this,state_81201);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__;
})()
;})(__83144,switch__80630__auto__,c__80824__auto___83148,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async))
})();
var state__80826__auto__ = (function (){var statearr_81227 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81227[(6)] = c__80824__auto___83148);

return statearr_81227;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(__83144,c__80824__auto___83148,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async))
);


break;
case "async":
var c__80824__auto___83178 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__83144,c__80824__auto___83178,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (__83144,c__80824__auto___83178,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async){
return (function (state_81241){
var state_val_81242 = (state_81241[(1)]);
if((state_val_81242 === (1))){
var state_81241__$1 = state_81241;
var statearr_81243_83179 = state_81241__$1;
(statearr_81243_83179[(2)] = null);

(statearr_81243_83179[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81242 === (2))){
var state_81241__$1 = state_81241;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81241__$1,(4),jobs);
} else {
if((state_val_81242 === (3))){
var inst_81239 = (state_81241[(2)]);
var state_81241__$1 = state_81241;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81241__$1,inst_81239);
} else {
if((state_val_81242 === (4))){
var inst_81231 = (state_81241[(2)]);
var inst_81232 = async(inst_81231);
var state_81241__$1 = state_81241;
if(cljs.core.truth_(inst_81232)){
var statearr_81244_83181 = state_81241__$1;
(statearr_81244_83181[(1)] = (5));

} else {
var statearr_81245_83183 = state_81241__$1;
(statearr_81245_83183[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81242 === (5))){
var state_81241__$1 = state_81241;
var statearr_81247_83185 = state_81241__$1;
(statearr_81247_83185[(2)] = null);

(statearr_81247_83185[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81242 === (6))){
var state_81241__$1 = state_81241;
var statearr_81248_83187 = state_81241__$1;
(statearr_81248_83187[(2)] = null);

(statearr_81248_83187[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81242 === (7))){
var inst_81237 = (state_81241[(2)]);
var state_81241__$1 = state_81241;
var statearr_81249_83189 = state_81241__$1;
(statearr_81249_83189[(2)] = inst_81237);

(statearr_81249_83189[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__83144,c__80824__auto___83178,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async))
;
return ((function (__83144,switch__80630__auto__,c__80824__auto___83178,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0 = (function (){
var statearr_81250 = [null,null,null,null,null,null,null];
(statearr_81250[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__);

(statearr_81250[(1)] = (1));

return statearr_81250;
});
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1 = (function (state_81241){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81241);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81251){if((e81251 instanceof Object)){
var ex__80634__auto__ = e81251;
var statearr_81252_83194 = state_81241;
(statearr_81252_83194[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81241);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81251;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83197 = state_81241;
state_81241 = G__83197;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = function(state_81241){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1.call(this,state_81241);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__;
})()
;})(__83144,switch__80630__auto__,c__80824__auto___83178,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async))
})();
var state__80826__auto__ = (function (){var statearr_81253 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81253[(6)] = c__80824__auto___83178);

return statearr_81253;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(__83144,c__80824__auto___83178,G__81184_83145,G__81184_83146__$1,n__4607__auto___83143,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__81184_83146__$1)].join('')));

}

var G__83199 = (__83144 + (1));
__83144 = G__83199;
continue;
} else {
}
break;
}

var c__80824__auto___83200 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83200,jobs,results,process,async){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83200,jobs,results,process,async){
return (function (state_81275){
var state_val_81276 = (state_81275[(1)]);
if((state_val_81276 === (7))){
var inst_81271 = (state_81275[(2)]);
var state_81275__$1 = state_81275;
var statearr_81278_83202 = state_81275__$1;
(statearr_81278_83202[(2)] = inst_81271);

(statearr_81278_83202[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81276 === (1))){
var state_81275__$1 = state_81275;
var statearr_81279_83203 = state_81275__$1;
(statearr_81279_83203[(2)] = null);

(statearr_81279_83203[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81276 === (4))){
var inst_81256 = (state_81275[(7)]);
var inst_81256__$1 = (state_81275[(2)]);
var inst_81257 = (inst_81256__$1 == null);
var state_81275__$1 = (function (){var statearr_81280 = state_81275;
(statearr_81280[(7)] = inst_81256__$1);

return statearr_81280;
})();
if(cljs.core.truth_(inst_81257)){
var statearr_81281_83205 = state_81275__$1;
(statearr_81281_83205[(1)] = (5));

} else {
var statearr_81282_83206 = state_81275__$1;
(statearr_81282_83206[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81276 === (6))){
var inst_81261 = (state_81275[(8)]);
var inst_81256 = (state_81275[(7)]);
var inst_81261__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_81262 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_81263 = [inst_81256,inst_81261__$1];
var inst_81264 = (new cljs.core.PersistentVector(null,2,(5),inst_81262,inst_81263,null));
var state_81275__$1 = (function (){var statearr_81283 = state_81275;
(statearr_81283[(8)] = inst_81261__$1);

return statearr_81283;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81275__$1,(8),jobs,inst_81264);
} else {
if((state_val_81276 === (3))){
var inst_81273 = (state_81275[(2)]);
var state_81275__$1 = state_81275;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81275__$1,inst_81273);
} else {
if((state_val_81276 === (2))){
var state_81275__$1 = state_81275;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81275__$1,(4),from);
} else {
if((state_val_81276 === (9))){
var inst_81268 = (state_81275[(2)]);
var state_81275__$1 = (function (){var statearr_81288 = state_81275;
(statearr_81288[(9)] = inst_81268);

return statearr_81288;
})();
var statearr_81290_83209 = state_81275__$1;
(statearr_81290_83209[(2)] = null);

(statearr_81290_83209[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81276 === (5))){
var inst_81259 = cljs.core.async.close_BANG_(jobs);
var state_81275__$1 = state_81275;
var statearr_81291_83210 = state_81275__$1;
(statearr_81291_83210[(2)] = inst_81259);

(statearr_81291_83210[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81276 === (8))){
var inst_81261 = (state_81275[(8)]);
var inst_81266 = (state_81275[(2)]);
var state_81275__$1 = (function (){var statearr_81292 = state_81275;
(statearr_81292[(10)] = inst_81266);

return statearr_81292;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81275__$1,(9),results,inst_81261);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83200,jobs,results,process,async))
;
return ((function (switch__80630__auto__,c__80824__auto___83200,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0 = (function (){
var statearr_81293 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_81293[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__);

(statearr_81293[(1)] = (1));

return statearr_81293;
});
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1 = (function (state_81275){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81275);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81294){if((e81294 instanceof Object)){
var ex__80634__auto__ = e81294;
var statearr_81295_83214 = state_81275;
(statearr_81295_83214[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81275);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81294;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83215 = state_81275;
state_81275 = G__83215;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = function(state_81275){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1.call(this,state_81275);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83200,jobs,results,process,async))
})();
var state__80826__auto__ = (function (){var statearr_81297 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81297[(6)] = c__80824__auto___83200);

return statearr_81297;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83200,jobs,results,process,async))
);


var c__80824__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto__,jobs,results,process,async){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto__,jobs,results,process,async){
return (function (state_81336){
var state_val_81337 = (state_81336[(1)]);
if((state_val_81337 === (7))){
var inst_81332 = (state_81336[(2)]);
var state_81336__$1 = state_81336;
var statearr_81338_83216 = state_81336__$1;
(statearr_81338_83216[(2)] = inst_81332);

(statearr_81338_83216[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (20))){
var state_81336__$1 = state_81336;
var statearr_81339_83217 = state_81336__$1;
(statearr_81339_83217[(2)] = null);

(statearr_81339_83217[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (1))){
var state_81336__$1 = state_81336;
var statearr_81340_83218 = state_81336__$1;
(statearr_81340_83218[(2)] = null);

(statearr_81340_83218[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (4))){
var inst_81300 = (state_81336[(7)]);
var inst_81300__$1 = (state_81336[(2)]);
var inst_81301 = (inst_81300__$1 == null);
var state_81336__$1 = (function (){var statearr_81341 = state_81336;
(statearr_81341[(7)] = inst_81300__$1);

return statearr_81341;
})();
if(cljs.core.truth_(inst_81301)){
var statearr_81342_83220 = state_81336__$1;
(statearr_81342_83220[(1)] = (5));

} else {
var statearr_81343_83221 = state_81336__$1;
(statearr_81343_83221[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (15))){
var inst_81314 = (state_81336[(8)]);
var state_81336__$1 = state_81336;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81336__$1,(18),to,inst_81314);
} else {
if((state_val_81337 === (21))){
var inst_81327 = (state_81336[(2)]);
var state_81336__$1 = state_81336;
var statearr_81344_83223 = state_81336__$1;
(statearr_81344_83223[(2)] = inst_81327);

(statearr_81344_83223[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (13))){
var inst_81329 = (state_81336[(2)]);
var state_81336__$1 = (function (){var statearr_81345 = state_81336;
(statearr_81345[(9)] = inst_81329);

return statearr_81345;
})();
var statearr_81346_83224 = state_81336__$1;
(statearr_81346_83224[(2)] = null);

(statearr_81346_83224[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (6))){
var inst_81300 = (state_81336[(7)]);
var state_81336__$1 = state_81336;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81336__$1,(11),inst_81300);
} else {
if((state_val_81337 === (17))){
var inst_81322 = (state_81336[(2)]);
var state_81336__$1 = state_81336;
if(cljs.core.truth_(inst_81322)){
var statearr_81347_83225 = state_81336__$1;
(statearr_81347_83225[(1)] = (19));

} else {
var statearr_81348_83226 = state_81336__$1;
(statearr_81348_83226[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (3))){
var inst_81334 = (state_81336[(2)]);
var state_81336__$1 = state_81336;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81336__$1,inst_81334);
} else {
if((state_val_81337 === (12))){
var inst_81311 = (state_81336[(10)]);
var state_81336__$1 = state_81336;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81336__$1,(14),inst_81311);
} else {
if((state_val_81337 === (2))){
var state_81336__$1 = state_81336;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81336__$1,(4),results);
} else {
if((state_val_81337 === (19))){
var state_81336__$1 = state_81336;
var statearr_81349_83230 = state_81336__$1;
(statearr_81349_83230[(2)] = null);

(statearr_81349_83230[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (11))){
var inst_81311 = (state_81336[(2)]);
var state_81336__$1 = (function (){var statearr_81350 = state_81336;
(statearr_81350[(10)] = inst_81311);

return statearr_81350;
})();
var statearr_81351_83232 = state_81336__$1;
(statearr_81351_83232[(2)] = null);

(statearr_81351_83232[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (9))){
var state_81336__$1 = state_81336;
var statearr_81352_83234 = state_81336__$1;
(statearr_81352_83234[(2)] = null);

(statearr_81352_83234[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (5))){
var state_81336__$1 = state_81336;
if(cljs.core.truth_(close_QMARK_)){
var statearr_81353_83235 = state_81336__$1;
(statearr_81353_83235[(1)] = (8));

} else {
var statearr_81354_83236 = state_81336__$1;
(statearr_81354_83236[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (14))){
var inst_81316 = (state_81336[(11)]);
var inst_81314 = (state_81336[(8)]);
var inst_81314__$1 = (state_81336[(2)]);
var inst_81315 = (inst_81314__$1 == null);
var inst_81316__$1 = cljs.core.not(inst_81315);
var state_81336__$1 = (function (){var statearr_81355 = state_81336;
(statearr_81355[(11)] = inst_81316__$1);

(statearr_81355[(8)] = inst_81314__$1);

return statearr_81355;
})();
if(inst_81316__$1){
var statearr_81356_83242 = state_81336__$1;
(statearr_81356_83242[(1)] = (15));

} else {
var statearr_81357_83243 = state_81336__$1;
(statearr_81357_83243[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (16))){
var inst_81316 = (state_81336[(11)]);
var state_81336__$1 = state_81336;
var statearr_81359_83244 = state_81336__$1;
(statearr_81359_83244[(2)] = inst_81316);

(statearr_81359_83244[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (10))){
var inst_81307 = (state_81336[(2)]);
var state_81336__$1 = state_81336;
var statearr_81360_83245 = state_81336__$1;
(statearr_81360_83245[(2)] = inst_81307);

(statearr_81360_83245[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (18))){
var inst_81319 = (state_81336[(2)]);
var state_81336__$1 = state_81336;
var statearr_81361_83246 = state_81336__$1;
(statearr_81361_83246[(2)] = inst_81319);

(statearr_81361_83246[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81337 === (8))){
var inst_81304 = cljs.core.async.close_BANG_(to);
var state_81336__$1 = state_81336;
var statearr_81362_83253 = state_81336__$1;
(statearr_81362_83253[(2)] = inst_81304);

(statearr_81362_83253[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto__,jobs,results,process,async))
;
return ((function (switch__80630__auto__,c__80824__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0 = (function (){
var statearr_81364 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_81364[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__);

(statearr_81364[(1)] = (1));

return statearr_81364;
});
var cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1 = (function (state_81336){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81336);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81366){if((e81366 instanceof Object)){
var ex__80634__auto__ = e81366;
var statearr_81367_83254 = state_81336;
(statearr_81367_83254[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81336);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81366;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83255 = state_81336;
state_81336 = G__83255;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__ = function(state_81336){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1.call(this,state_81336);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__80631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto__,jobs,results,process,async))
})();
var state__80826__auto__ = (function (){var statearr_81368 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81368[(6)] = c__80824__auto__);

return statearr_81368;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto__,jobs,results,process,async))
);

return c__80824__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__81370 = arguments.length;
switch (G__81370) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__81373 = arguments.length;
switch (G__81373) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__81375 = arguments.length;
switch (G__81375) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__80824__auto___83269 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83269,tc,fc){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83269,tc,fc){
return (function (state_81401){
var state_val_81402 = (state_81401[(1)]);
if((state_val_81402 === (7))){
var inst_81397 = (state_81401[(2)]);
var state_81401__$1 = state_81401;
var statearr_81405_83270 = state_81401__$1;
(statearr_81405_83270[(2)] = inst_81397);

(statearr_81405_83270[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (1))){
var state_81401__$1 = state_81401;
var statearr_81406_83273 = state_81401__$1;
(statearr_81406_83273[(2)] = null);

(statearr_81406_83273[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (4))){
var inst_81378 = (state_81401[(7)]);
var inst_81378__$1 = (state_81401[(2)]);
var inst_81379 = (inst_81378__$1 == null);
var state_81401__$1 = (function (){var statearr_81408 = state_81401;
(statearr_81408[(7)] = inst_81378__$1);

return statearr_81408;
})();
if(cljs.core.truth_(inst_81379)){
var statearr_81409_83282 = state_81401__$1;
(statearr_81409_83282[(1)] = (5));

} else {
var statearr_81410_83283 = state_81401__$1;
(statearr_81410_83283[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (13))){
var state_81401__$1 = state_81401;
var statearr_81412_83284 = state_81401__$1;
(statearr_81412_83284[(2)] = null);

(statearr_81412_83284[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (6))){
var inst_81378 = (state_81401[(7)]);
var inst_81384 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_81378) : p.call(null,inst_81378));
var state_81401__$1 = state_81401;
if(cljs.core.truth_(inst_81384)){
var statearr_81414_83285 = state_81401__$1;
(statearr_81414_83285[(1)] = (9));

} else {
var statearr_81415_83286 = state_81401__$1;
(statearr_81415_83286[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (3))){
var inst_81399 = (state_81401[(2)]);
var state_81401__$1 = state_81401;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81401__$1,inst_81399);
} else {
if((state_val_81402 === (12))){
var state_81401__$1 = state_81401;
var statearr_81416_83289 = state_81401__$1;
(statearr_81416_83289[(2)] = null);

(statearr_81416_83289[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (2))){
var state_81401__$1 = state_81401;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81401__$1,(4),ch);
} else {
if((state_val_81402 === (11))){
var inst_81378 = (state_81401[(7)]);
var inst_81388 = (state_81401[(2)]);
var state_81401__$1 = state_81401;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81401__$1,(8),inst_81388,inst_81378);
} else {
if((state_val_81402 === (9))){
var state_81401__$1 = state_81401;
var statearr_81417_83293 = state_81401__$1;
(statearr_81417_83293[(2)] = tc);

(statearr_81417_83293[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (5))){
var inst_81381 = cljs.core.async.close_BANG_(tc);
var inst_81382 = cljs.core.async.close_BANG_(fc);
var state_81401__$1 = (function (){var statearr_81419 = state_81401;
(statearr_81419[(8)] = inst_81381);

return statearr_81419;
})();
var statearr_81420_83297 = state_81401__$1;
(statearr_81420_83297[(2)] = inst_81382);

(statearr_81420_83297[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (14))){
var inst_81395 = (state_81401[(2)]);
var state_81401__$1 = state_81401;
var statearr_81422_83305 = state_81401__$1;
(statearr_81422_83305[(2)] = inst_81395);

(statearr_81422_83305[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (10))){
var state_81401__$1 = state_81401;
var statearr_81423_83308 = state_81401__$1;
(statearr_81423_83308[(2)] = fc);

(statearr_81423_83308[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81402 === (8))){
var inst_81390 = (state_81401[(2)]);
var state_81401__$1 = state_81401;
if(cljs.core.truth_(inst_81390)){
var statearr_81424_83317 = state_81401__$1;
(statearr_81424_83317[(1)] = (12));

} else {
var statearr_81425_83324 = state_81401__$1;
(statearr_81425_83324[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83269,tc,fc))
;
return ((function (switch__80630__auto__,c__80824__auto___83269,tc,fc){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_81426 = [null,null,null,null,null,null,null,null,null];
(statearr_81426[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_81426[(1)] = (1));

return statearr_81426;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_81401){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81401);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81428){if((e81428 instanceof Object)){
var ex__80634__auto__ = e81428;
var statearr_81430_83325 = state_81401;
(statearr_81430_83325[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81401);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81428;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83326 = state_81401;
state_81401 = G__83326;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_81401){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_81401);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83269,tc,fc))
})();
var state__80826__auto__ = (function (){var statearr_81431 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81431[(6)] = c__80824__auto___83269);

return statearr_81431;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83269,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__80824__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto__){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto__){
return (function (state_81453){
var state_val_81454 = (state_81453[(1)]);
if((state_val_81454 === (7))){
var inst_81449 = (state_81453[(2)]);
var state_81453__$1 = state_81453;
var statearr_81455_83339 = state_81453__$1;
(statearr_81455_83339[(2)] = inst_81449);

(statearr_81455_83339[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81454 === (1))){
var inst_81433 = init;
var state_81453__$1 = (function (){var statearr_81456 = state_81453;
(statearr_81456[(7)] = inst_81433);

return statearr_81456;
})();
var statearr_81457_83340 = state_81453__$1;
(statearr_81457_83340[(2)] = null);

(statearr_81457_83340[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81454 === (4))){
var inst_81436 = (state_81453[(8)]);
var inst_81436__$1 = (state_81453[(2)]);
var inst_81437 = (inst_81436__$1 == null);
var state_81453__$1 = (function (){var statearr_81458 = state_81453;
(statearr_81458[(8)] = inst_81436__$1);

return statearr_81458;
})();
if(cljs.core.truth_(inst_81437)){
var statearr_81459_83344 = state_81453__$1;
(statearr_81459_83344[(1)] = (5));

} else {
var statearr_81460_83345 = state_81453__$1;
(statearr_81460_83345[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81454 === (6))){
var inst_81433 = (state_81453[(7)]);
var inst_81436 = (state_81453[(8)]);
var inst_81440 = (state_81453[(9)]);
var inst_81440__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_81433,inst_81436) : f.call(null,inst_81433,inst_81436));
var inst_81441 = cljs.core.reduced_QMARK_(inst_81440__$1);
var state_81453__$1 = (function (){var statearr_81462 = state_81453;
(statearr_81462[(9)] = inst_81440__$1);

return statearr_81462;
})();
if(inst_81441){
var statearr_81463_83346 = state_81453__$1;
(statearr_81463_83346[(1)] = (8));

} else {
var statearr_81464_83347 = state_81453__$1;
(statearr_81464_83347[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81454 === (3))){
var inst_81451 = (state_81453[(2)]);
var state_81453__$1 = state_81453;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81453__$1,inst_81451);
} else {
if((state_val_81454 === (2))){
var state_81453__$1 = state_81453;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81453__$1,(4),ch);
} else {
if((state_val_81454 === (9))){
var inst_81440 = (state_81453[(9)]);
var inst_81433 = inst_81440;
var state_81453__$1 = (function (){var statearr_81465 = state_81453;
(statearr_81465[(7)] = inst_81433);

return statearr_81465;
})();
var statearr_81466_83350 = state_81453__$1;
(statearr_81466_83350[(2)] = null);

(statearr_81466_83350[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81454 === (5))){
var inst_81433 = (state_81453[(7)]);
var state_81453__$1 = state_81453;
var statearr_81467_83351 = state_81453__$1;
(statearr_81467_83351[(2)] = inst_81433);

(statearr_81467_83351[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81454 === (10))){
var inst_81447 = (state_81453[(2)]);
var state_81453__$1 = state_81453;
var statearr_81468_83353 = state_81453__$1;
(statearr_81468_83353[(2)] = inst_81447);

(statearr_81468_83353[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81454 === (8))){
var inst_81440 = (state_81453[(9)]);
var inst_81443 = cljs.core.deref(inst_81440);
var state_81453__$1 = state_81453;
var statearr_81469_83355 = state_81453__$1;
(statearr_81469_83355[(2)] = inst_81443);

(statearr_81469_83355[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto__))
;
return ((function (switch__80630__auto__,c__80824__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__80631__auto__ = null;
var cljs$core$async$reduce_$_state_machine__80631__auto____0 = (function (){
var statearr_81470 = [null,null,null,null,null,null,null,null,null,null];
(statearr_81470[(0)] = cljs$core$async$reduce_$_state_machine__80631__auto__);

(statearr_81470[(1)] = (1));

return statearr_81470;
});
var cljs$core$async$reduce_$_state_machine__80631__auto____1 = (function (state_81453){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81453);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81474){if((e81474 instanceof Object)){
var ex__80634__auto__ = e81474;
var statearr_81475_83357 = state_81453;
(statearr_81475_83357[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81453);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81474;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83358 = state_81453;
state_81453 = G__83358;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__80631__auto__ = function(state_81453){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__80631__auto____1.call(this,state_81453);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__80631__auto____0;
cljs$core$async$reduce_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__80631__auto____1;
return cljs$core$async$reduce_$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto__))
})();
var state__80826__auto__ = (function (){var statearr_81476 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81476[(6)] = c__80824__auto__);

return statearr_81476;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto__))
);

return c__80824__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__80824__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto__,f__$1){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto__,f__$1){
return (function (state_81482){
var state_val_81483 = (state_81482[(1)]);
if((state_val_81483 === (1))){
var inst_81477 = cljs.core.async.reduce(f__$1,init,ch);
var state_81482__$1 = state_81482;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81482__$1,(2),inst_81477);
} else {
if((state_val_81483 === (2))){
var inst_81479 = (state_81482[(2)]);
var inst_81480 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_81479) : f__$1.call(null,inst_81479));
var state_81482__$1 = state_81482;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81482__$1,inst_81480);
} else {
return null;
}
}
});})(c__80824__auto__,f__$1))
;
return ((function (switch__80630__auto__,c__80824__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__80631__auto__ = null;
var cljs$core$async$transduce_$_state_machine__80631__auto____0 = (function (){
var statearr_81486 = [null,null,null,null,null,null,null];
(statearr_81486[(0)] = cljs$core$async$transduce_$_state_machine__80631__auto__);

(statearr_81486[(1)] = (1));

return statearr_81486;
});
var cljs$core$async$transduce_$_state_machine__80631__auto____1 = (function (state_81482){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81482);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81487){if((e81487 instanceof Object)){
var ex__80634__auto__ = e81487;
var statearr_81488_83366 = state_81482;
(statearr_81488_83366[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81482);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81487;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83368 = state_81482;
state_81482 = G__83368;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__80631__auto__ = function(state_81482){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__80631__auto____1.call(this,state_81482);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__80631__auto____0;
cljs$core$async$transduce_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__80631__auto____1;
return cljs$core$async$transduce_$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto__,f__$1))
})();
var state__80826__auto__ = (function (){var statearr_81491 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81491[(6)] = c__80824__auto__);

return statearr_81491;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto__,f__$1))
);

return c__80824__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__81493 = arguments.length;
switch (G__81493) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__80824__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto__){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto__){
return (function (state_81520){
var state_val_81521 = (state_81520[(1)]);
if((state_val_81521 === (7))){
var inst_81502 = (state_81520[(2)]);
var state_81520__$1 = state_81520;
var statearr_81522_83374 = state_81520__$1;
(statearr_81522_83374[(2)] = inst_81502);

(statearr_81522_83374[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (1))){
var inst_81496 = cljs.core.seq(coll);
var inst_81497 = inst_81496;
var state_81520__$1 = (function (){var statearr_81523 = state_81520;
(statearr_81523[(7)] = inst_81497);

return statearr_81523;
})();
var statearr_81524_83377 = state_81520__$1;
(statearr_81524_83377[(2)] = null);

(statearr_81524_83377[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (4))){
var inst_81497 = (state_81520[(7)]);
var inst_81500 = cljs.core.first(inst_81497);
var state_81520__$1 = state_81520;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81520__$1,(7),ch,inst_81500);
} else {
if((state_val_81521 === (13))){
var inst_81514 = (state_81520[(2)]);
var state_81520__$1 = state_81520;
var statearr_81527_83381 = state_81520__$1;
(statearr_81527_83381[(2)] = inst_81514);

(statearr_81527_83381[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (6))){
var inst_81505 = (state_81520[(2)]);
var state_81520__$1 = state_81520;
if(cljs.core.truth_(inst_81505)){
var statearr_81528_83383 = state_81520__$1;
(statearr_81528_83383[(1)] = (8));

} else {
var statearr_81529_83385 = state_81520__$1;
(statearr_81529_83385[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (3))){
var inst_81518 = (state_81520[(2)]);
var state_81520__$1 = state_81520;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81520__$1,inst_81518);
} else {
if((state_val_81521 === (12))){
var state_81520__$1 = state_81520;
var statearr_81530_83386 = state_81520__$1;
(statearr_81530_83386[(2)] = null);

(statearr_81530_83386[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (2))){
var inst_81497 = (state_81520[(7)]);
var state_81520__$1 = state_81520;
if(cljs.core.truth_(inst_81497)){
var statearr_81531_83387 = state_81520__$1;
(statearr_81531_83387[(1)] = (4));

} else {
var statearr_81532_83388 = state_81520__$1;
(statearr_81532_83388[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (11))){
var inst_81511 = cljs.core.async.close_BANG_(ch);
var state_81520__$1 = state_81520;
var statearr_81533_83390 = state_81520__$1;
(statearr_81533_83390[(2)] = inst_81511);

(statearr_81533_83390[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (9))){
var state_81520__$1 = state_81520;
if(cljs.core.truth_(close_QMARK_)){
var statearr_81534_83391 = state_81520__$1;
(statearr_81534_83391[(1)] = (11));

} else {
var statearr_81535_83392 = state_81520__$1;
(statearr_81535_83392[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (5))){
var inst_81497 = (state_81520[(7)]);
var state_81520__$1 = state_81520;
var statearr_81536_83393 = state_81520__$1;
(statearr_81536_83393[(2)] = inst_81497);

(statearr_81536_83393[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (10))){
var inst_81516 = (state_81520[(2)]);
var state_81520__$1 = state_81520;
var statearr_81537_83394 = state_81520__$1;
(statearr_81537_83394[(2)] = inst_81516);

(statearr_81537_83394[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81521 === (8))){
var inst_81497 = (state_81520[(7)]);
var inst_81507 = cljs.core.next(inst_81497);
var inst_81497__$1 = inst_81507;
var state_81520__$1 = (function (){var statearr_81538 = state_81520;
(statearr_81538[(7)] = inst_81497__$1);

return statearr_81538;
})();
var statearr_81539_83395 = state_81520__$1;
(statearr_81539_83395[(2)] = null);

(statearr_81539_83395[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto__))
;
return ((function (switch__80630__auto__,c__80824__auto__){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_81540 = [null,null,null,null,null,null,null,null];
(statearr_81540[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_81540[(1)] = (1));

return statearr_81540;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_81520){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81520);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81541){if((e81541 instanceof Object)){
var ex__80634__auto__ = e81541;
var statearr_81542_83396 = state_81520;
(statearr_81542_83396[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81520);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81541;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83397 = state_81520;
state_81520 = G__83397;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_81520){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_81520);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto__))
})();
var state__80826__auto__ = (function (){var statearr_81543 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81543[(6)] = c__80824__auto__);

return statearr_81543;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto__))
);

return c__80824__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__4433__auto__ = (((_ == null))?null:_);
var m__4434__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4434__auto__.call(null,_));
} else {
var m__4431__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4431__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4434__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4431__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async81551 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async81551 = (function (ch,cs,meta81552){
this.ch = ch;
this.cs = cs;
this.meta81552 = meta81552;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_81553,meta81552__$1){
var self__ = this;
var _81553__$1 = this;
return (new cljs.core.async.t_cljs$core$async81551(self__.ch,self__.cs,meta81552__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_81553){
var self__ = this;
var _81553__$1 = this;
return self__.meta81552;
});})(cs))
;

cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async81551.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async81551.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta81552","meta81552",1595108795,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async81551.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async81551.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async81551";

cljs.core.async.t_cljs$core$async81551.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async81551");
});})(cs))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async81551.
 */
cljs.core.async.__GT_t_cljs$core$async81551 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async81551(ch__$1,cs__$1,meta81552){
return (new cljs.core.async.t_cljs$core$async81551(ch__$1,cs__$1,meta81552));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async81551(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__80824__auto___83400 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83400,cs,m,dchan,dctr,done){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83400,cs,m,dchan,dctr,done){
return (function (state_81710){
var state_val_81711 = (state_81710[(1)]);
if((state_val_81711 === (7))){
var inst_81706 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81712_83401 = state_81710__$1;
(statearr_81712_83401[(2)] = inst_81706);

(statearr_81712_83401[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (20))){
var inst_81589 = (state_81710[(7)]);
var inst_81608 = cljs.core.first(inst_81589);
var inst_81609 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_81608,(0),null);
var inst_81610 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_81608,(1),null);
var state_81710__$1 = (function (){var statearr_81713 = state_81710;
(statearr_81713[(8)] = inst_81609);

return statearr_81713;
})();
if(cljs.core.truth_(inst_81610)){
var statearr_81714_83402 = state_81710__$1;
(statearr_81714_83402[(1)] = (22));

} else {
var statearr_81715_83403 = state_81710__$1;
(statearr_81715_83403[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (27))){
var inst_81652 = (state_81710[(9)]);
var inst_81643 = (state_81710[(10)]);
var inst_81558 = (state_81710[(11)]);
var inst_81641 = (state_81710[(12)]);
var inst_81652__$1 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_81641,inst_81643);
var inst_81653 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_81652__$1,inst_81558,done);
var state_81710__$1 = (function (){var statearr_81716 = state_81710;
(statearr_81716[(9)] = inst_81652__$1);

return statearr_81716;
})();
if(cljs.core.truth_(inst_81653)){
var statearr_81717_83404 = state_81710__$1;
(statearr_81717_83404[(1)] = (30));

} else {
var statearr_81720_83405 = state_81710__$1;
(statearr_81720_83405[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (1))){
var state_81710__$1 = state_81710;
var statearr_81722_83406 = state_81710__$1;
(statearr_81722_83406[(2)] = null);

(statearr_81722_83406[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (24))){
var inst_81589 = (state_81710[(7)]);
var inst_81618 = (state_81710[(2)]);
var inst_81619 = cljs.core.next(inst_81589);
var inst_81567 = inst_81619;
var inst_81568 = null;
var inst_81569 = (0);
var inst_81570 = (0);
var state_81710__$1 = (function (){var statearr_81723 = state_81710;
(statearr_81723[(13)] = inst_81570);

(statearr_81723[(14)] = inst_81618);

(statearr_81723[(15)] = inst_81567);

(statearr_81723[(16)] = inst_81569);

(statearr_81723[(17)] = inst_81568);

return statearr_81723;
})();
var statearr_81724_83407 = state_81710__$1;
(statearr_81724_83407[(2)] = null);

(statearr_81724_83407[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (39))){
var state_81710__$1 = state_81710;
var statearr_81731_83408 = state_81710__$1;
(statearr_81731_83408[(2)] = null);

(statearr_81731_83408[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (4))){
var inst_81558 = (state_81710[(11)]);
var inst_81558__$1 = (state_81710[(2)]);
var inst_81559 = (inst_81558__$1 == null);
var state_81710__$1 = (function (){var statearr_81732 = state_81710;
(statearr_81732[(11)] = inst_81558__$1);

return statearr_81732;
})();
if(cljs.core.truth_(inst_81559)){
var statearr_81733_83409 = state_81710__$1;
(statearr_81733_83409[(1)] = (5));

} else {
var statearr_81736_83410 = state_81710__$1;
(statearr_81736_83410[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (15))){
var inst_81570 = (state_81710[(13)]);
var inst_81567 = (state_81710[(15)]);
var inst_81569 = (state_81710[(16)]);
var inst_81568 = (state_81710[(17)]);
var inst_81585 = (state_81710[(2)]);
var inst_81586 = (inst_81570 + (1));
var tmp81725 = inst_81567;
var tmp81726 = inst_81569;
var tmp81727 = inst_81568;
var inst_81567__$1 = tmp81725;
var inst_81568__$1 = tmp81727;
var inst_81569__$1 = tmp81726;
var inst_81570__$1 = inst_81586;
var state_81710__$1 = (function (){var statearr_81738 = state_81710;
(statearr_81738[(13)] = inst_81570__$1);

(statearr_81738[(18)] = inst_81585);

(statearr_81738[(15)] = inst_81567__$1);

(statearr_81738[(16)] = inst_81569__$1);

(statearr_81738[(17)] = inst_81568__$1);

return statearr_81738;
})();
var statearr_81739_83411 = state_81710__$1;
(statearr_81739_83411[(2)] = null);

(statearr_81739_83411[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (21))){
var inst_81622 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81743_83412 = state_81710__$1;
(statearr_81743_83412[(2)] = inst_81622);

(statearr_81743_83412[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (31))){
var inst_81652 = (state_81710[(9)]);
var inst_81656 = done(null);
var inst_81657 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_81652);
var state_81710__$1 = (function (){var statearr_81745 = state_81710;
(statearr_81745[(19)] = inst_81656);

return statearr_81745;
})();
var statearr_81747_83414 = state_81710__$1;
(statearr_81747_83414[(2)] = inst_81657);

(statearr_81747_83414[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (32))){
var inst_81643 = (state_81710[(10)]);
var inst_81640 = (state_81710[(20)]);
var inst_81642 = (state_81710[(21)]);
var inst_81641 = (state_81710[(12)]);
var inst_81659 = (state_81710[(2)]);
var inst_81660 = (inst_81643 + (1));
var tmp81740 = inst_81640;
var tmp81741 = inst_81642;
var tmp81742 = inst_81641;
var inst_81640__$1 = tmp81740;
var inst_81641__$1 = tmp81742;
var inst_81642__$1 = tmp81741;
var inst_81643__$1 = inst_81660;
var state_81710__$1 = (function (){var statearr_81749 = state_81710;
(statearr_81749[(22)] = inst_81659);

(statearr_81749[(10)] = inst_81643__$1);

(statearr_81749[(20)] = inst_81640__$1);

(statearr_81749[(21)] = inst_81642__$1);

(statearr_81749[(12)] = inst_81641__$1);

return statearr_81749;
})();
var statearr_81750_83418 = state_81710__$1;
(statearr_81750_83418[(2)] = null);

(statearr_81750_83418[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (40))){
var inst_81672 = (state_81710[(23)]);
var inst_81679 = done(null);
var inst_81680 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_81672);
var state_81710__$1 = (function (){var statearr_81752 = state_81710;
(statearr_81752[(24)] = inst_81679);

return statearr_81752;
})();
var statearr_81753_83421 = state_81710__$1;
(statearr_81753_83421[(2)] = inst_81680);

(statearr_81753_83421[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (33))){
var inst_81663 = (state_81710[(25)]);
var inst_81665 = cljs.core.chunked_seq_QMARK_(inst_81663);
var state_81710__$1 = state_81710;
if(inst_81665){
var statearr_81754_83422 = state_81710__$1;
(statearr_81754_83422[(1)] = (36));

} else {
var statearr_81755_83424 = state_81710__$1;
(statearr_81755_83424[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (13))){
var inst_81579 = (state_81710[(26)]);
var inst_81582 = cljs.core.async.close_BANG_(inst_81579);
var state_81710__$1 = state_81710;
var statearr_81756_83425 = state_81710__$1;
(statearr_81756_83425[(2)] = inst_81582);

(statearr_81756_83425[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (22))){
var inst_81609 = (state_81710[(8)]);
var inst_81615 = cljs.core.async.close_BANG_(inst_81609);
var state_81710__$1 = state_81710;
var statearr_81760_83428 = state_81710__$1;
(statearr_81760_83428[(2)] = inst_81615);

(statearr_81760_83428[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (36))){
var inst_81663 = (state_81710[(25)]);
var inst_81667 = cljs.core.chunk_first(inst_81663);
var inst_81668 = cljs.core.chunk_rest(inst_81663);
var inst_81669 = cljs.core.count(inst_81667);
var inst_81640 = inst_81668;
var inst_81641 = inst_81667;
var inst_81642 = inst_81669;
var inst_81643 = (0);
var state_81710__$1 = (function (){var statearr_81761 = state_81710;
(statearr_81761[(10)] = inst_81643);

(statearr_81761[(20)] = inst_81640);

(statearr_81761[(21)] = inst_81642);

(statearr_81761[(12)] = inst_81641);

return statearr_81761;
})();
var statearr_81762_83433 = state_81710__$1;
(statearr_81762_83433[(2)] = null);

(statearr_81762_83433[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (41))){
var inst_81663 = (state_81710[(25)]);
var inst_81682 = (state_81710[(2)]);
var inst_81683 = cljs.core.next(inst_81663);
var inst_81640 = inst_81683;
var inst_81641 = null;
var inst_81642 = (0);
var inst_81643 = (0);
var state_81710__$1 = (function (){var statearr_81764 = state_81710;
(statearr_81764[(10)] = inst_81643);

(statearr_81764[(20)] = inst_81640);

(statearr_81764[(21)] = inst_81642);

(statearr_81764[(27)] = inst_81682);

(statearr_81764[(12)] = inst_81641);

return statearr_81764;
})();
var statearr_81767_83437 = state_81710__$1;
(statearr_81767_83437[(2)] = null);

(statearr_81767_83437[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (43))){
var state_81710__$1 = state_81710;
var statearr_81768_83441 = state_81710__$1;
(statearr_81768_83441[(2)] = null);

(statearr_81768_83441[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (29))){
var inst_81691 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81770_83442 = state_81710__$1;
(statearr_81770_83442[(2)] = inst_81691);

(statearr_81770_83442[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (44))){
var inst_81703 = (state_81710[(2)]);
var state_81710__$1 = (function (){var statearr_81771 = state_81710;
(statearr_81771[(28)] = inst_81703);

return statearr_81771;
})();
var statearr_81772_83443 = state_81710__$1;
(statearr_81772_83443[(2)] = null);

(statearr_81772_83443[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (6))){
var inst_81632 = (state_81710[(29)]);
var inst_81631 = cljs.core.deref(cs);
var inst_81632__$1 = cljs.core.keys(inst_81631);
var inst_81633 = cljs.core.count(inst_81632__$1);
var inst_81634 = cljs.core.reset_BANG_(dctr,inst_81633);
var inst_81639 = cljs.core.seq(inst_81632__$1);
var inst_81640 = inst_81639;
var inst_81641 = null;
var inst_81642 = (0);
var inst_81643 = (0);
var state_81710__$1 = (function (){var statearr_81775 = state_81710;
(statearr_81775[(10)] = inst_81643);

(statearr_81775[(20)] = inst_81640);

(statearr_81775[(29)] = inst_81632__$1);

(statearr_81775[(30)] = inst_81634);

(statearr_81775[(21)] = inst_81642);

(statearr_81775[(12)] = inst_81641);

return statearr_81775;
})();
var statearr_81776_83452 = state_81710__$1;
(statearr_81776_83452[(2)] = null);

(statearr_81776_83452[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (28))){
var inst_81640 = (state_81710[(20)]);
var inst_81663 = (state_81710[(25)]);
var inst_81663__$1 = cljs.core.seq(inst_81640);
var state_81710__$1 = (function (){var statearr_81777 = state_81710;
(statearr_81777[(25)] = inst_81663__$1);

return statearr_81777;
})();
if(inst_81663__$1){
var statearr_81778_83453 = state_81710__$1;
(statearr_81778_83453[(1)] = (33));

} else {
var statearr_81779_83454 = state_81710__$1;
(statearr_81779_83454[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (25))){
var inst_81643 = (state_81710[(10)]);
var inst_81642 = (state_81710[(21)]);
var inst_81645 = (inst_81643 < inst_81642);
var inst_81646 = inst_81645;
var state_81710__$1 = state_81710;
if(cljs.core.truth_(inst_81646)){
var statearr_81780_83458 = state_81710__$1;
(statearr_81780_83458[(1)] = (27));

} else {
var statearr_81781_83459 = state_81710__$1;
(statearr_81781_83459[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (34))){
var state_81710__$1 = state_81710;
var statearr_81782_83460 = state_81710__$1;
(statearr_81782_83460[(2)] = null);

(statearr_81782_83460[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (17))){
var state_81710__$1 = state_81710;
var statearr_81783_83461 = state_81710__$1;
(statearr_81783_83461[(2)] = null);

(statearr_81783_83461[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (3))){
var inst_81708 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81710__$1,inst_81708);
} else {
if((state_val_81711 === (12))){
var inst_81627 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81785_83462 = state_81710__$1;
(statearr_81785_83462[(2)] = inst_81627);

(statearr_81785_83462[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (2))){
var state_81710__$1 = state_81710;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81710__$1,(4),ch);
} else {
if((state_val_81711 === (23))){
var state_81710__$1 = state_81710;
var statearr_81786_83463 = state_81710__$1;
(statearr_81786_83463[(2)] = null);

(statearr_81786_83463[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (35))){
var inst_81689 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81787_83464 = state_81710__$1;
(statearr_81787_83464[(2)] = inst_81689);

(statearr_81787_83464[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (19))){
var inst_81589 = (state_81710[(7)]);
var inst_81597 = cljs.core.chunk_first(inst_81589);
var inst_81598 = cljs.core.chunk_rest(inst_81589);
var inst_81599 = cljs.core.count(inst_81597);
var inst_81567 = inst_81598;
var inst_81568 = inst_81597;
var inst_81569 = inst_81599;
var inst_81570 = (0);
var state_81710__$1 = (function (){var statearr_81788 = state_81710;
(statearr_81788[(13)] = inst_81570);

(statearr_81788[(15)] = inst_81567);

(statearr_81788[(16)] = inst_81569);

(statearr_81788[(17)] = inst_81568);

return statearr_81788;
})();
var statearr_81789_83465 = state_81710__$1;
(statearr_81789_83465[(2)] = null);

(statearr_81789_83465[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (11))){
var inst_81567 = (state_81710[(15)]);
var inst_81589 = (state_81710[(7)]);
var inst_81589__$1 = cljs.core.seq(inst_81567);
var state_81710__$1 = (function (){var statearr_81792 = state_81710;
(statearr_81792[(7)] = inst_81589__$1);

return statearr_81792;
})();
if(inst_81589__$1){
var statearr_81793_83467 = state_81710__$1;
(statearr_81793_83467[(1)] = (16));

} else {
var statearr_81794_83471 = state_81710__$1;
(statearr_81794_83471[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (9))){
var inst_81629 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81795_83481 = state_81710__$1;
(statearr_81795_83481[(2)] = inst_81629);

(statearr_81795_83481[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (5))){
var inst_81565 = cljs.core.deref(cs);
var inst_81566 = cljs.core.seq(inst_81565);
var inst_81567 = inst_81566;
var inst_81568 = null;
var inst_81569 = (0);
var inst_81570 = (0);
var state_81710__$1 = (function (){var statearr_81799 = state_81710;
(statearr_81799[(13)] = inst_81570);

(statearr_81799[(15)] = inst_81567);

(statearr_81799[(16)] = inst_81569);

(statearr_81799[(17)] = inst_81568);

return statearr_81799;
})();
var statearr_81800_83482 = state_81710__$1;
(statearr_81800_83482[(2)] = null);

(statearr_81800_83482[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (14))){
var state_81710__$1 = state_81710;
var statearr_81803_83485 = state_81710__$1;
(statearr_81803_83485[(2)] = null);

(statearr_81803_83485[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (45))){
var inst_81700 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81805_83493 = state_81710__$1;
(statearr_81805_83493[(2)] = inst_81700);

(statearr_81805_83493[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (26))){
var inst_81632 = (state_81710[(29)]);
var inst_81693 = (state_81710[(2)]);
var inst_81694 = cljs.core.seq(inst_81632);
var state_81710__$1 = (function (){var statearr_81807 = state_81710;
(statearr_81807[(31)] = inst_81693);

return statearr_81807;
})();
if(inst_81694){
var statearr_81808_83494 = state_81710__$1;
(statearr_81808_83494[(1)] = (42));

} else {
var statearr_81809_83495 = state_81710__$1;
(statearr_81809_83495[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (16))){
var inst_81589 = (state_81710[(7)]);
var inst_81595 = cljs.core.chunked_seq_QMARK_(inst_81589);
var state_81710__$1 = state_81710;
if(inst_81595){
var statearr_81810_83496 = state_81710__$1;
(statearr_81810_83496[(1)] = (19));

} else {
var statearr_81811_83497 = state_81710__$1;
(statearr_81811_83497[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (38))){
var inst_81686 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81818_83498 = state_81710__$1;
(statearr_81818_83498[(2)] = inst_81686);

(statearr_81818_83498[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (30))){
var state_81710__$1 = state_81710;
var statearr_81820_83499 = state_81710__$1;
(statearr_81820_83499[(2)] = null);

(statearr_81820_83499[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (10))){
var inst_81570 = (state_81710[(13)]);
var inst_81568 = (state_81710[(17)]);
var inst_81578 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_81568,inst_81570);
var inst_81579 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_81578,(0),null);
var inst_81580 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_81578,(1),null);
var state_81710__$1 = (function (){var statearr_81821 = state_81710;
(statearr_81821[(26)] = inst_81579);

return statearr_81821;
})();
if(cljs.core.truth_(inst_81580)){
var statearr_81822_83504 = state_81710__$1;
(statearr_81822_83504[(1)] = (13));

} else {
var statearr_81823_83505 = state_81710__$1;
(statearr_81823_83505[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (18))){
var inst_81625 = (state_81710[(2)]);
var state_81710__$1 = state_81710;
var statearr_81826_83506 = state_81710__$1;
(statearr_81826_83506[(2)] = inst_81625);

(statearr_81826_83506[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (42))){
var state_81710__$1 = state_81710;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_81710__$1,(45),dchan);
} else {
if((state_val_81711 === (37))){
var inst_81672 = (state_81710[(23)]);
var inst_81663 = (state_81710[(25)]);
var inst_81558 = (state_81710[(11)]);
var inst_81672__$1 = cljs.core.first(inst_81663);
var inst_81676 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_81672__$1,inst_81558,done);
var state_81710__$1 = (function (){var statearr_81828 = state_81710;
(statearr_81828[(23)] = inst_81672__$1);

return statearr_81828;
})();
if(cljs.core.truth_(inst_81676)){
var statearr_81830_83507 = state_81710__$1;
(statearr_81830_83507[(1)] = (39));

} else {
var statearr_81831_83508 = state_81710__$1;
(statearr_81831_83508[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81711 === (8))){
var inst_81570 = (state_81710[(13)]);
var inst_81569 = (state_81710[(16)]);
var inst_81572 = (inst_81570 < inst_81569);
var inst_81573 = inst_81572;
var state_81710__$1 = state_81710;
if(cljs.core.truth_(inst_81573)){
var statearr_81832_83509 = state_81710__$1;
(statearr_81832_83509[(1)] = (10));

} else {
var statearr_81833_83510 = state_81710__$1;
(statearr_81833_83510[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83400,cs,m,dchan,dctr,done))
;
return ((function (switch__80630__auto__,c__80824__auto___83400,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__80631__auto__ = null;
var cljs$core$async$mult_$_state_machine__80631__auto____0 = (function (){
var statearr_81836 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_81836[(0)] = cljs$core$async$mult_$_state_machine__80631__auto__);

(statearr_81836[(1)] = (1));

return statearr_81836;
});
var cljs$core$async$mult_$_state_machine__80631__auto____1 = (function (state_81710){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81710);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e81837){if((e81837 instanceof Object)){
var ex__80634__auto__ = e81837;
var statearr_81838_83514 = state_81710;
(statearr_81838_83514[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81710);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e81837;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83515 = state_81710;
state_81710 = G__83515;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__80631__auto__ = function(state_81710){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__80631__auto____1.call(this,state_81710);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__80631__auto____0;
cljs$core$async$mult_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__80631__auto____1;
return cljs$core$async$mult_$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83400,cs,m,dchan,dctr,done))
})();
var state__80826__auto__ = (function (){var statearr_81841 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_81841[(6)] = c__80824__auto___83400);

return statearr_81841;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83400,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__81843 = arguments.length;
switch (G__81843) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4434__auto__.call(null,m,state_map));
} else {
var m__4431__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4431__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4434__auto__.call(null,m,mode));
} else {
var m__4431__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4431__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___83525 = arguments.length;
var i__4731__auto___83526 = (0);
while(true){
if((i__4731__auto___83526 < len__4730__auto___83525)){
args__4736__auto__.push((arguments[i__4731__auto___83526]));

var G__83527 = (i__4731__auto___83526 + (1));
i__4731__auto___83526 = G__83527;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((3) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4737__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__81856){
var map__81857 = p__81856;
var map__81857__$1 = (((((!((map__81857 == null))))?(((((map__81857.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__81857.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__81857):map__81857);
var opts = map__81857__$1;
var statearr_81859_83532 = state;
(statearr_81859_83532[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts(((function (map__81857,map__81857__$1,opts){
return (function (val){
var statearr_81860_83533 = state;
(statearr_81860_83533[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
});})(map__81857,map__81857__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_81861_83534 = state;
(statearr_81861_83534[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

/** @this {Function} */
cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq81852){
var G__81853 = cljs.core.first(seq81852);
var seq81852__$1 = cljs.core.next(seq81852);
var G__81854 = cljs.core.first(seq81852__$1);
var seq81852__$2 = cljs.core.next(seq81852__$1);
var G__81855 = cljs.core.first(seq81852__$2);
var seq81852__$3 = cljs.core.next(seq81852__$2);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__81853,G__81854,G__81855,seq81852__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv(((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async81862 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async81862 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta81863){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta81863 = meta81863;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_81864,meta81863__$1){
var self__ = this;
var _81864__$1 = this;
return (new cljs.core.async.t_cljs$core$async81862(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta81863__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_81864){
var self__ = this;
var _81864__$1 = this;
return self__.meta81863;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta81863","meta81863",1246356456,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async81862.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async81862.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async81862";

cljs.core.async.t_cljs$core$async81862.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async81862");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async81862.
 */
cljs.core.async.__GT_t_cljs$core$async81862 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async81862(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta81863){
return (new cljs.core.async.t_cljs$core$async81862(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta81863));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async81862(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__80824__auto___83582 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83582,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83582,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_81983){
var state_val_81984 = (state_81983[(1)]);
if((state_val_81984 === (7))){
var inst_81886 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
var statearr_81985_83584 = state_81983__$1;
(statearr_81985_83584[(2)] = inst_81886);

(statearr_81985_83584[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (20))){
var inst_81898 = (state_81983[(7)]);
var state_81983__$1 = state_81983;
var statearr_81992_83585 = state_81983__$1;
(statearr_81992_83585[(2)] = inst_81898);

(statearr_81992_83585[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (27))){
var state_81983__$1 = state_81983;
var statearr_81993_83587 = state_81983__$1;
(statearr_81993_83587[(2)] = null);

(statearr_81993_83587[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (1))){
var inst_81873 = (state_81983[(8)]);
var inst_81873__$1 = calc_state();
var inst_81875 = (inst_81873__$1 == null);
var inst_81876 = cljs.core.not(inst_81875);
var state_81983__$1 = (function (){var statearr_81994 = state_81983;
(statearr_81994[(8)] = inst_81873__$1);

return statearr_81994;
})();
if(inst_81876){
var statearr_81995_83590 = state_81983__$1;
(statearr_81995_83590[(1)] = (2));

} else {
var statearr_81996_83591 = state_81983__$1;
(statearr_81996_83591[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (24))){
var inst_81937 = (state_81983[(9)]);
var inst_81922 = (state_81983[(10)]);
var inst_81951 = (state_81983[(11)]);
var inst_81951__$1 = (inst_81922.cljs$core$IFn$_invoke$arity$1 ? inst_81922.cljs$core$IFn$_invoke$arity$1(inst_81937) : inst_81922.call(null,inst_81937));
var state_81983__$1 = (function (){var statearr_81998 = state_81983;
(statearr_81998[(11)] = inst_81951__$1);

return statearr_81998;
})();
if(cljs.core.truth_(inst_81951__$1)){
var statearr_81999_83592 = state_81983__$1;
(statearr_81999_83592[(1)] = (29));

} else {
var statearr_82000_83593 = state_81983__$1;
(statearr_82000_83593[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (4))){
var inst_81889 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
if(cljs.core.truth_(inst_81889)){
var statearr_82001_83595 = state_81983__$1;
(statearr_82001_83595[(1)] = (8));

} else {
var statearr_82003_83596 = state_81983__$1;
(statearr_82003_83596[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (15))){
var inst_81916 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
if(cljs.core.truth_(inst_81916)){
var statearr_82004_83598 = state_81983__$1;
(statearr_82004_83598[(1)] = (19));

} else {
var statearr_82005_83599 = state_81983__$1;
(statearr_82005_83599[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (21))){
var inst_81921 = (state_81983[(12)]);
var inst_81921__$1 = (state_81983[(2)]);
var inst_81922 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_81921__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_81923 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_81921__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_81924 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_81921__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_81983__$1 = (function (){var statearr_82006 = state_81983;
(statearr_82006[(10)] = inst_81922);

(statearr_82006[(12)] = inst_81921__$1);

(statearr_82006[(13)] = inst_81923);

return statearr_82006;
})();
return cljs.core.async.ioc_alts_BANG_(state_81983__$1,(22),inst_81924);
} else {
if((state_val_81984 === (31))){
var inst_81959 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
if(cljs.core.truth_(inst_81959)){
var statearr_82007_83600 = state_81983__$1;
(statearr_82007_83600[(1)] = (32));

} else {
var statearr_82009_83601 = state_81983__$1;
(statearr_82009_83601[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (32))){
var inst_81936 = (state_81983[(14)]);
var state_81983__$1 = state_81983;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_81983__$1,(35),out,inst_81936);
} else {
if((state_val_81984 === (33))){
var inst_81921 = (state_81983[(12)]);
var inst_81898 = inst_81921;
var state_81983__$1 = (function (){var statearr_82010 = state_81983;
(statearr_82010[(7)] = inst_81898);

return statearr_82010;
})();
var statearr_82011_83602 = state_81983__$1;
(statearr_82011_83602[(2)] = null);

(statearr_82011_83602[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (13))){
var inst_81898 = (state_81983[(7)]);
var inst_81905 = inst_81898.cljs$lang$protocol_mask$partition0$;
var inst_81906 = (inst_81905 & (64));
var inst_81907 = inst_81898.cljs$core$ISeq$;
var inst_81908 = (cljs.core.PROTOCOL_SENTINEL === inst_81907);
var inst_81909 = ((inst_81906) || (inst_81908));
var state_81983__$1 = state_81983;
if(cljs.core.truth_(inst_81909)){
var statearr_82012_83603 = state_81983__$1;
(statearr_82012_83603[(1)] = (16));

} else {
var statearr_82013_83604 = state_81983__$1;
(statearr_82013_83604[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (22))){
var inst_81937 = (state_81983[(9)]);
var inst_81936 = (state_81983[(14)]);
var inst_81935 = (state_81983[(2)]);
var inst_81936__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_81935,(0),null);
var inst_81937__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_81935,(1),null);
var inst_81938 = (inst_81936__$1 == null);
var inst_81939 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_81937__$1,change);
var inst_81940 = ((inst_81938) || (inst_81939));
var state_81983__$1 = (function (){var statearr_82014 = state_81983;
(statearr_82014[(9)] = inst_81937__$1);

(statearr_82014[(14)] = inst_81936__$1);

return statearr_82014;
})();
if(cljs.core.truth_(inst_81940)){
var statearr_82015_83605 = state_81983__$1;
(statearr_82015_83605[(1)] = (23));

} else {
var statearr_82017_83606 = state_81983__$1;
(statearr_82017_83606[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (36))){
var inst_81921 = (state_81983[(12)]);
var inst_81898 = inst_81921;
var state_81983__$1 = (function (){var statearr_82019 = state_81983;
(statearr_82019[(7)] = inst_81898);

return statearr_82019;
})();
var statearr_82020_83610 = state_81983__$1;
(statearr_82020_83610[(2)] = null);

(statearr_82020_83610[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (29))){
var inst_81951 = (state_81983[(11)]);
var state_81983__$1 = state_81983;
var statearr_82022_83611 = state_81983__$1;
(statearr_82022_83611[(2)] = inst_81951);

(statearr_82022_83611[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (6))){
var state_81983__$1 = state_81983;
var statearr_82023_83612 = state_81983__$1;
(statearr_82023_83612[(2)] = false);

(statearr_82023_83612[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (28))){
var inst_81947 = (state_81983[(2)]);
var inst_81948 = calc_state();
var inst_81898 = inst_81948;
var state_81983__$1 = (function (){var statearr_82024 = state_81983;
(statearr_82024[(7)] = inst_81898);

(statearr_82024[(15)] = inst_81947);

return statearr_82024;
})();
var statearr_82025_83614 = state_81983__$1;
(statearr_82025_83614[(2)] = null);

(statearr_82025_83614[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (25))){
var inst_81973 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
var statearr_82026_83615 = state_81983__$1;
(statearr_82026_83615[(2)] = inst_81973);

(statearr_82026_83615[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (34))){
var inst_81971 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
var statearr_82030_83616 = state_81983__$1;
(statearr_82030_83616[(2)] = inst_81971);

(statearr_82030_83616[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (17))){
var state_81983__$1 = state_81983;
var statearr_82031_83617 = state_81983__$1;
(statearr_82031_83617[(2)] = false);

(statearr_82031_83617[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (3))){
var state_81983__$1 = state_81983;
var statearr_82032_83618 = state_81983__$1;
(statearr_82032_83618[(2)] = false);

(statearr_82032_83618[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (12))){
var inst_81975 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
return cljs.core.async.impl.ioc_helpers.return_chan(state_81983__$1,inst_81975);
} else {
if((state_val_81984 === (2))){
var inst_81873 = (state_81983[(8)]);
var inst_81878 = inst_81873.cljs$lang$protocol_mask$partition0$;
var inst_81879 = (inst_81878 & (64));
var inst_81880 = inst_81873.cljs$core$ISeq$;
var inst_81881 = (cljs.core.PROTOCOL_SENTINEL === inst_81880);
var inst_81882 = ((inst_81879) || (inst_81881));
var state_81983__$1 = state_81983;
if(cljs.core.truth_(inst_81882)){
var statearr_82033_83619 = state_81983__$1;
(statearr_82033_83619[(1)] = (5));

} else {
var statearr_82037_83620 = state_81983__$1;
(statearr_82037_83620[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (23))){
var inst_81936 = (state_81983[(14)]);
var inst_81942 = (inst_81936 == null);
var state_81983__$1 = state_81983;
if(cljs.core.truth_(inst_81942)){
var statearr_82039_83621 = state_81983__$1;
(statearr_82039_83621[(1)] = (26));

} else {
var statearr_82040_83622 = state_81983__$1;
(statearr_82040_83622[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (35))){
var inst_81962 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
if(cljs.core.truth_(inst_81962)){
var statearr_82041_83624 = state_81983__$1;
(statearr_82041_83624[(1)] = (36));

} else {
var statearr_82042_83625 = state_81983__$1;
(statearr_82042_83625[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (19))){
var inst_81898 = (state_81983[(7)]);
var inst_81918 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_81898);
var state_81983__$1 = state_81983;
var statearr_82043_83626 = state_81983__$1;
(statearr_82043_83626[(2)] = inst_81918);

(statearr_82043_83626[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (11))){
var inst_81898 = (state_81983[(7)]);
var inst_81902 = (inst_81898 == null);
var inst_81903 = cljs.core.not(inst_81902);
var state_81983__$1 = state_81983;
if(inst_81903){
var statearr_82044_83628 = state_81983__$1;
(statearr_82044_83628[(1)] = (13));

} else {
var statearr_82045_83629 = state_81983__$1;
(statearr_82045_83629[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (9))){
var inst_81873 = (state_81983[(8)]);
var state_81983__$1 = state_81983;
var statearr_82046_83630 = state_81983__$1;
(statearr_82046_83630[(2)] = inst_81873);

(statearr_82046_83630[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (5))){
var state_81983__$1 = state_81983;
var statearr_82047_83631 = state_81983__$1;
(statearr_82047_83631[(2)] = true);

(statearr_82047_83631[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (14))){
var state_81983__$1 = state_81983;
var statearr_82048_83632 = state_81983__$1;
(statearr_82048_83632[(2)] = false);

(statearr_82048_83632[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (26))){
var inst_81937 = (state_81983[(9)]);
var inst_81944 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_81937);
var state_81983__$1 = state_81983;
var statearr_82049_83633 = state_81983__$1;
(statearr_82049_83633[(2)] = inst_81944);

(statearr_82049_83633[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (16))){
var state_81983__$1 = state_81983;
var statearr_82050_83639 = state_81983__$1;
(statearr_82050_83639[(2)] = true);

(statearr_82050_83639[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (38))){
var inst_81967 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
var statearr_82051_83640 = state_81983__$1;
(statearr_82051_83640[(2)] = inst_81967);

(statearr_82051_83640[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (30))){
var inst_81937 = (state_81983[(9)]);
var inst_81922 = (state_81983[(10)]);
var inst_81923 = (state_81983[(13)]);
var inst_81954 = cljs.core.empty_QMARK_(inst_81922);
var inst_81955 = (inst_81923.cljs$core$IFn$_invoke$arity$1 ? inst_81923.cljs$core$IFn$_invoke$arity$1(inst_81937) : inst_81923.call(null,inst_81937));
var inst_81956 = cljs.core.not(inst_81955);
var inst_81957 = ((inst_81954) && (inst_81956));
var state_81983__$1 = state_81983;
var statearr_82052_83641 = state_81983__$1;
(statearr_82052_83641[(2)] = inst_81957);

(statearr_82052_83641[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (10))){
var inst_81873 = (state_81983[(8)]);
var inst_81894 = (state_81983[(2)]);
var inst_81895 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_81894,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_81896 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_81894,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_81897 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_81894,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_81898 = inst_81873;
var state_81983__$1 = (function (){var statearr_82053 = state_81983;
(statearr_82053[(16)] = inst_81895);

(statearr_82053[(7)] = inst_81898);

(statearr_82053[(17)] = inst_81896);

(statearr_82053[(18)] = inst_81897);

return statearr_82053;
})();
var statearr_82054_83642 = state_81983__$1;
(statearr_82054_83642[(2)] = null);

(statearr_82054_83642[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (18))){
var inst_81913 = (state_81983[(2)]);
var state_81983__$1 = state_81983;
var statearr_82055_83644 = state_81983__$1;
(statearr_82055_83644[(2)] = inst_81913);

(statearr_82055_83644[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (37))){
var state_81983__$1 = state_81983;
var statearr_82056_83649 = state_81983__$1;
(statearr_82056_83649[(2)] = null);

(statearr_82056_83649[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_81984 === (8))){
var inst_81873 = (state_81983[(8)]);
var inst_81891 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_81873);
var state_81983__$1 = state_81983;
var statearr_82057_83650 = state_81983__$1;
(statearr_82057_83650[(2)] = inst_81891);

(statearr_82057_83650[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83582,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__80630__auto__,c__80824__auto___83582,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__80631__auto__ = null;
var cljs$core$async$mix_$_state_machine__80631__auto____0 = (function (){
var statearr_82058 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_82058[(0)] = cljs$core$async$mix_$_state_machine__80631__auto__);

(statearr_82058[(1)] = (1));

return statearr_82058;
});
var cljs$core$async$mix_$_state_machine__80631__auto____1 = (function (state_81983){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_81983);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82059){if((e82059 instanceof Object)){
var ex__80634__auto__ = e82059;
var statearr_82060_83651 = state_81983;
(statearr_82060_83651[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_81983);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82059;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83652 = state_81983;
state_81983 = G__83652;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__80631__auto__ = function(state_81983){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__80631__auto____1.call(this,state_81983);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__80631__auto____0;
cljs$core$async$mix_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__80631__auto____1;
return cljs$core$async$mix_$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83582,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__80826__auto__ = (function (){var statearr_82064 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82064[(6)] = c__80824__auto___83582);

return statearr_82064;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83582,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4434__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4431__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4434__auto__.call(null,p,v,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4431__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__82080 = arguments.length;
switch (G__82080) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4434__auto__.call(null,p));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4431__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4434__auto__.call(null,p,v));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4431__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__82084 = arguments.length;
switch (G__82084) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__4131__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,((function (or__4131__auto__,mults){
return (function (p1__82082_SHARP_){
if(cljs.core.truth_((p1__82082_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__82082_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__82082_SHARP_.call(null,topic)))){
return p1__82082_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__82082_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
});})(or__4131__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async82085 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async82085 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta82086){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta82086 = meta82086;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_82087,meta82086__$1){
var self__ = this;
var _82087__$1 = this;
return (new cljs.core.async.t_cljs$core$async82085(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta82086__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_82087){
var self__ = this;
var _82087__$1 = this;
return self__.meta82086;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta82086","meta82086",296161417,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async82085.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async82085.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async82085";

cljs.core.async.t_cljs$core$async82085.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async82085");
});})(mults,ensure_mult))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async82085.
 */
cljs.core.async.__GT_t_cljs$core$async82085 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async82085(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta82086){
return (new cljs.core.async.t_cljs$core$async82085(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta82086));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async82085(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__80824__auto___83671 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83671,mults,ensure_mult,p){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83671,mults,ensure_mult,p){
return (function (state_82184){
var state_val_82185 = (state_82184[(1)]);
if((state_val_82185 === (7))){
var inst_82180 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
var statearr_82188_83672 = state_82184__$1;
(statearr_82188_83672[(2)] = inst_82180);

(statearr_82188_83672[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (20))){
var state_82184__$1 = state_82184;
var statearr_82189_83673 = state_82184__$1;
(statearr_82189_83673[(2)] = null);

(statearr_82189_83673[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (1))){
var state_82184__$1 = state_82184;
var statearr_82190_83674 = state_82184__$1;
(statearr_82190_83674[(2)] = null);

(statearr_82190_83674[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (24))){
var inst_82163 = (state_82184[(7)]);
var inst_82172 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_82163);
var state_82184__$1 = state_82184;
var statearr_82191_83676 = state_82184__$1;
(statearr_82191_83676[(2)] = inst_82172);

(statearr_82191_83676[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (4))){
var inst_82111 = (state_82184[(8)]);
var inst_82111__$1 = (state_82184[(2)]);
var inst_82112 = (inst_82111__$1 == null);
var state_82184__$1 = (function (){var statearr_82192 = state_82184;
(statearr_82192[(8)] = inst_82111__$1);

return statearr_82192;
})();
if(cljs.core.truth_(inst_82112)){
var statearr_82193_83677 = state_82184__$1;
(statearr_82193_83677[(1)] = (5));

} else {
var statearr_82196_83678 = state_82184__$1;
(statearr_82196_83678[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (15))){
var inst_82157 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
var statearr_82197_83681 = state_82184__$1;
(statearr_82197_83681[(2)] = inst_82157);

(statearr_82197_83681[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (21))){
var inst_82177 = (state_82184[(2)]);
var state_82184__$1 = (function (){var statearr_82198 = state_82184;
(statearr_82198[(9)] = inst_82177);

return statearr_82198;
})();
var statearr_82199_83682 = state_82184__$1;
(statearr_82199_83682[(2)] = null);

(statearr_82199_83682[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (13))){
var inst_82135 = (state_82184[(10)]);
var inst_82138 = cljs.core.chunked_seq_QMARK_(inst_82135);
var state_82184__$1 = state_82184;
if(inst_82138){
var statearr_82200_83687 = state_82184__$1;
(statearr_82200_83687[(1)] = (16));

} else {
var statearr_82201_83689 = state_82184__$1;
(statearr_82201_83689[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (22))){
var inst_82169 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
if(cljs.core.truth_(inst_82169)){
var statearr_82202_83713 = state_82184__$1;
(statearr_82202_83713[(1)] = (23));

} else {
var statearr_82203_83714 = state_82184__$1;
(statearr_82203_83714[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (6))){
var inst_82165 = (state_82184[(11)]);
var inst_82111 = (state_82184[(8)]);
var inst_82163 = (state_82184[(7)]);
var inst_82163__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_82111) : topic_fn.call(null,inst_82111));
var inst_82164 = cljs.core.deref(mults);
var inst_82165__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_82164,inst_82163__$1);
var state_82184__$1 = (function (){var statearr_82204 = state_82184;
(statearr_82204[(11)] = inst_82165__$1);

(statearr_82204[(7)] = inst_82163__$1);

return statearr_82204;
})();
if(cljs.core.truth_(inst_82165__$1)){
var statearr_82207_83715 = state_82184__$1;
(statearr_82207_83715[(1)] = (19));

} else {
var statearr_82208_83716 = state_82184__$1;
(statearr_82208_83716[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (25))){
var inst_82174 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
var statearr_82215_83717 = state_82184__$1;
(statearr_82215_83717[(2)] = inst_82174);

(statearr_82215_83717[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (17))){
var inst_82135 = (state_82184[(10)]);
var inst_82148 = cljs.core.first(inst_82135);
var inst_82149 = cljs.core.async.muxch_STAR_(inst_82148);
var inst_82150 = cljs.core.async.close_BANG_(inst_82149);
var inst_82151 = cljs.core.next(inst_82135);
var inst_82121 = inst_82151;
var inst_82122 = null;
var inst_82123 = (0);
var inst_82124 = (0);
var state_82184__$1 = (function (){var statearr_82216 = state_82184;
(statearr_82216[(12)] = inst_82123);

(statearr_82216[(13)] = inst_82121);

(statearr_82216[(14)] = inst_82150);

(statearr_82216[(15)] = inst_82124);

(statearr_82216[(16)] = inst_82122);

return statearr_82216;
})();
var statearr_82217_83719 = state_82184__$1;
(statearr_82217_83719[(2)] = null);

(statearr_82217_83719[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (3))){
var inst_82182 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
return cljs.core.async.impl.ioc_helpers.return_chan(state_82184__$1,inst_82182);
} else {
if((state_val_82185 === (12))){
var inst_82159 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
var statearr_82218_83720 = state_82184__$1;
(statearr_82218_83720[(2)] = inst_82159);

(statearr_82218_83720[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (2))){
var state_82184__$1 = state_82184;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82184__$1,(4),ch);
} else {
if((state_val_82185 === (23))){
var state_82184__$1 = state_82184;
var statearr_82219_83722 = state_82184__$1;
(statearr_82219_83722[(2)] = null);

(statearr_82219_83722[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (19))){
var inst_82165 = (state_82184[(11)]);
var inst_82111 = (state_82184[(8)]);
var inst_82167 = cljs.core.async.muxch_STAR_(inst_82165);
var state_82184__$1 = state_82184;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82184__$1,(22),inst_82167,inst_82111);
} else {
if((state_val_82185 === (11))){
var inst_82121 = (state_82184[(13)]);
var inst_82135 = (state_82184[(10)]);
var inst_82135__$1 = cljs.core.seq(inst_82121);
var state_82184__$1 = (function (){var statearr_82220 = state_82184;
(statearr_82220[(10)] = inst_82135__$1);

return statearr_82220;
})();
if(inst_82135__$1){
var statearr_82221_83724 = state_82184__$1;
(statearr_82221_83724[(1)] = (13));

} else {
var statearr_82222_83725 = state_82184__$1;
(statearr_82222_83725[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (9))){
var inst_82161 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
var statearr_82223_83726 = state_82184__$1;
(statearr_82223_83726[(2)] = inst_82161);

(statearr_82223_83726[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (5))){
var inst_82118 = cljs.core.deref(mults);
var inst_82119 = cljs.core.vals(inst_82118);
var inst_82120 = cljs.core.seq(inst_82119);
var inst_82121 = inst_82120;
var inst_82122 = null;
var inst_82123 = (0);
var inst_82124 = (0);
var state_82184__$1 = (function (){var statearr_82224 = state_82184;
(statearr_82224[(12)] = inst_82123);

(statearr_82224[(13)] = inst_82121);

(statearr_82224[(15)] = inst_82124);

(statearr_82224[(16)] = inst_82122);

return statearr_82224;
})();
var statearr_82225_83729 = state_82184__$1;
(statearr_82225_83729[(2)] = null);

(statearr_82225_83729[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (14))){
var state_82184__$1 = state_82184;
var statearr_82229_83731 = state_82184__$1;
(statearr_82229_83731[(2)] = null);

(statearr_82229_83731[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (16))){
var inst_82135 = (state_82184[(10)]);
var inst_82140 = cljs.core.chunk_first(inst_82135);
var inst_82141 = cljs.core.chunk_rest(inst_82135);
var inst_82142 = cljs.core.count(inst_82140);
var inst_82121 = inst_82141;
var inst_82122 = inst_82140;
var inst_82123 = inst_82142;
var inst_82124 = (0);
var state_82184__$1 = (function (){var statearr_82236 = state_82184;
(statearr_82236[(12)] = inst_82123);

(statearr_82236[(13)] = inst_82121);

(statearr_82236[(15)] = inst_82124);

(statearr_82236[(16)] = inst_82122);

return statearr_82236;
})();
var statearr_82237_83732 = state_82184__$1;
(statearr_82237_83732[(2)] = null);

(statearr_82237_83732[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (10))){
var inst_82123 = (state_82184[(12)]);
var inst_82121 = (state_82184[(13)]);
var inst_82124 = (state_82184[(15)]);
var inst_82122 = (state_82184[(16)]);
var inst_82129 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_82122,inst_82124);
var inst_82130 = cljs.core.async.muxch_STAR_(inst_82129);
var inst_82131 = cljs.core.async.close_BANG_(inst_82130);
var inst_82132 = (inst_82124 + (1));
var tmp82226 = inst_82123;
var tmp82227 = inst_82121;
var tmp82228 = inst_82122;
var inst_82121__$1 = tmp82227;
var inst_82122__$1 = tmp82228;
var inst_82123__$1 = tmp82226;
var inst_82124__$1 = inst_82132;
var state_82184__$1 = (function (){var statearr_82243 = state_82184;
(statearr_82243[(12)] = inst_82123__$1);

(statearr_82243[(13)] = inst_82121__$1);

(statearr_82243[(15)] = inst_82124__$1);

(statearr_82243[(16)] = inst_82122__$1);

(statearr_82243[(17)] = inst_82131);

return statearr_82243;
})();
var statearr_82244_83736 = state_82184__$1;
(statearr_82244_83736[(2)] = null);

(statearr_82244_83736[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (18))){
var inst_82154 = (state_82184[(2)]);
var state_82184__$1 = state_82184;
var statearr_82245_83744 = state_82184__$1;
(statearr_82245_83744[(2)] = inst_82154);

(statearr_82245_83744[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82185 === (8))){
var inst_82123 = (state_82184[(12)]);
var inst_82124 = (state_82184[(15)]);
var inst_82126 = (inst_82124 < inst_82123);
var inst_82127 = inst_82126;
var state_82184__$1 = state_82184;
if(cljs.core.truth_(inst_82127)){
var statearr_82246_83745 = state_82184__$1;
(statearr_82246_83745[(1)] = (10));

} else {
var statearr_82247_83746 = state_82184__$1;
(statearr_82247_83746[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83671,mults,ensure_mult,p))
;
return ((function (switch__80630__auto__,c__80824__auto___83671,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82248 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_82248[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82248[(1)] = (1));

return statearr_82248;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82184){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82184);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82249){if((e82249 instanceof Object)){
var ex__80634__auto__ = e82249;
var statearr_82250_83750 = state_82184;
(statearr_82250_83750[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82184);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82249;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83752 = state_82184;
state_82184 = G__83752;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82184){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82184);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83671,mults,ensure_mult,p))
})();
var state__80826__auto__ = (function (){var statearr_82251 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82251[(6)] = c__80824__auto___83671);

return statearr_82251;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83671,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__82253 = arguments.length;
switch (G__82253) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__82255 = arguments.length;
switch (G__82255) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1(p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2(p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__82258 = arguments.length;
switch (G__82258) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2(((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__80824__auto___83761 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83761,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83761,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_82303){
var state_val_82304 = (state_82303[(1)]);
if((state_val_82304 === (7))){
var state_82303__$1 = state_82303;
var statearr_82306_83762 = state_82303__$1;
(statearr_82306_83762[(2)] = null);

(statearr_82306_83762[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (1))){
var state_82303__$1 = state_82303;
var statearr_82307_83763 = state_82303__$1;
(statearr_82307_83763[(2)] = null);

(statearr_82307_83763[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (4))){
var inst_82263 = (state_82303[(7)]);
var inst_82265 = (inst_82263 < cnt);
var state_82303__$1 = state_82303;
if(cljs.core.truth_(inst_82265)){
var statearr_82308_83764 = state_82303__$1;
(statearr_82308_83764[(1)] = (6));

} else {
var statearr_82309_83765 = state_82303__$1;
(statearr_82309_83765[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (15))){
var inst_82297 = (state_82303[(2)]);
var state_82303__$1 = state_82303;
var statearr_82310_83766 = state_82303__$1;
(statearr_82310_83766[(2)] = inst_82297);

(statearr_82310_83766[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (13))){
var inst_82290 = cljs.core.async.close_BANG_(out);
var state_82303__$1 = state_82303;
var statearr_82311_83767 = state_82303__$1;
(statearr_82311_83767[(2)] = inst_82290);

(statearr_82311_83767[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (6))){
var state_82303__$1 = state_82303;
var statearr_82312_83768 = state_82303__$1;
(statearr_82312_83768[(2)] = null);

(statearr_82312_83768[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (3))){
var inst_82299 = (state_82303[(2)]);
var state_82303__$1 = state_82303;
return cljs.core.async.impl.ioc_helpers.return_chan(state_82303__$1,inst_82299);
} else {
if((state_val_82304 === (12))){
var inst_82287 = (state_82303[(8)]);
var inst_82287__$1 = (state_82303[(2)]);
var inst_82288 = cljs.core.some(cljs.core.nil_QMARK_,inst_82287__$1);
var state_82303__$1 = (function (){var statearr_82316 = state_82303;
(statearr_82316[(8)] = inst_82287__$1);

return statearr_82316;
})();
if(cljs.core.truth_(inst_82288)){
var statearr_82318_83770 = state_82303__$1;
(statearr_82318_83770[(1)] = (13));

} else {
var statearr_82319_83771 = state_82303__$1;
(statearr_82319_83771[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (2))){
var inst_82262 = cljs.core.reset_BANG_(dctr,cnt);
var inst_82263 = (0);
var state_82303__$1 = (function (){var statearr_82320 = state_82303;
(statearr_82320[(9)] = inst_82262);

(statearr_82320[(7)] = inst_82263);

return statearr_82320;
})();
var statearr_82321_83772 = state_82303__$1;
(statearr_82321_83772[(2)] = null);

(statearr_82321_83772[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (11))){
var inst_82263 = (state_82303[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame(state_82303,(10),Object,null,(9));
var inst_82272 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_82263) : chs__$1.call(null,inst_82263));
var inst_82273 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_82263) : done.call(null,inst_82263));
var inst_82274 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_82272,inst_82273);
var state_82303__$1 = state_82303;
var statearr_82322_83773 = state_82303__$1;
(statearr_82322_83773[(2)] = inst_82274);


cljs.core.async.impl.ioc_helpers.process_exception(state_82303__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (9))){
var inst_82263 = (state_82303[(7)]);
var inst_82276 = (state_82303[(2)]);
var inst_82277 = (inst_82263 + (1));
var inst_82263__$1 = inst_82277;
var state_82303__$1 = (function (){var statearr_82323 = state_82303;
(statearr_82323[(7)] = inst_82263__$1);

(statearr_82323[(10)] = inst_82276);

return statearr_82323;
})();
var statearr_82325_83774 = state_82303__$1;
(statearr_82325_83774[(2)] = null);

(statearr_82325_83774[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (5))){
var inst_82283 = (state_82303[(2)]);
var state_82303__$1 = (function (){var statearr_82326 = state_82303;
(statearr_82326[(11)] = inst_82283);

return statearr_82326;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82303__$1,(12),dchan);
} else {
if((state_val_82304 === (14))){
var inst_82287 = (state_82303[(8)]);
var inst_82292 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_82287);
var state_82303__$1 = state_82303;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82303__$1,(16),out,inst_82292);
} else {
if((state_val_82304 === (16))){
var inst_82294 = (state_82303[(2)]);
var state_82303__$1 = (function (){var statearr_82327 = state_82303;
(statearr_82327[(12)] = inst_82294);

return statearr_82327;
})();
var statearr_82328_83775 = state_82303__$1;
(statearr_82328_83775[(2)] = null);

(statearr_82328_83775[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (10))){
var inst_82267 = (state_82303[(2)]);
var inst_82268 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_82303__$1 = (function (){var statearr_82330 = state_82303;
(statearr_82330[(13)] = inst_82267);

return statearr_82330;
})();
var statearr_82331_83779 = state_82303__$1;
(statearr_82331_83779[(2)] = inst_82268);


cljs.core.async.impl.ioc_helpers.process_exception(state_82303__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82304 === (8))){
var inst_82281 = (state_82303[(2)]);
var state_82303__$1 = state_82303;
var statearr_82332_83780 = state_82303__$1;
(statearr_82332_83780[(2)] = inst_82281);

(statearr_82332_83780[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83761,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__80630__auto__,c__80824__auto___83761,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82333 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_82333[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82333[(1)] = (1));

return statearr_82333;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82303){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82303);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82334){if((e82334 instanceof Object)){
var ex__80634__auto__ = e82334;
var statearr_82335_83785 = state_82303;
(statearr_82335_83785[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82303);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82334;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83786 = state_82303;
state_82303 = G__83786;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82303){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82303);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83761,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__80826__auto__ = (function (){var statearr_82336 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82336[(6)] = c__80824__auto___83761);

return statearr_82336;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83761,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__82340 = arguments.length;
switch (G__82340) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__80824__auto___83794 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83794,out){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83794,out){
return (function (state_82374){
var state_val_82375 = (state_82374[(1)]);
if((state_val_82375 === (7))){
var inst_82353 = (state_82374[(7)]);
var inst_82354 = (state_82374[(8)]);
var inst_82353__$1 = (state_82374[(2)]);
var inst_82354__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_82353__$1,(0),null);
var inst_82355 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_82353__$1,(1),null);
var inst_82356 = (inst_82354__$1 == null);
var state_82374__$1 = (function (){var statearr_82376 = state_82374;
(statearr_82376[(9)] = inst_82355);

(statearr_82376[(7)] = inst_82353__$1);

(statearr_82376[(8)] = inst_82354__$1);

return statearr_82376;
})();
if(cljs.core.truth_(inst_82356)){
var statearr_82377_83804 = state_82374__$1;
(statearr_82377_83804[(1)] = (8));

} else {
var statearr_82378_83805 = state_82374__$1;
(statearr_82378_83805[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82375 === (1))){
var inst_82343 = cljs.core.vec(chs);
var inst_82344 = inst_82343;
var state_82374__$1 = (function (){var statearr_82379 = state_82374;
(statearr_82379[(10)] = inst_82344);

return statearr_82379;
})();
var statearr_82380_83807 = state_82374__$1;
(statearr_82380_83807[(2)] = null);

(statearr_82380_83807[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82375 === (4))){
var inst_82344 = (state_82374[(10)]);
var state_82374__$1 = state_82374;
return cljs.core.async.ioc_alts_BANG_(state_82374__$1,(7),inst_82344);
} else {
if((state_val_82375 === (6))){
var inst_82370 = (state_82374[(2)]);
var state_82374__$1 = state_82374;
var statearr_82381_83808 = state_82374__$1;
(statearr_82381_83808[(2)] = inst_82370);

(statearr_82381_83808[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82375 === (3))){
var inst_82372 = (state_82374[(2)]);
var state_82374__$1 = state_82374;
return cljs.core.async.impl.ioc_helpers.return_chan(state_82374__$1,inst_82372);
} else {
if((state_val_82375 === (2))){
var inst_82344 = (state_82374[(10)]);
var inst_82346 = cljs.core.count(inst_82344);
var inst_82347 = (inst_82346 > (0));
var state_82374__$1 = state_82374;
if(cljs.core.truth_(inst_82347)){
var statearr_82384_83809 = state_82374__$1;
(statearr_82384_83809[(1)] = (4));

} else {
var statearr_82386_83811 = state_82374__$1;
(statearr_82386_83811[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82375 === (11))){
var inst_82344 = (state_82374[(10)]);
var inst_82363 = (state_82374[(2)]);
var tmp82382 = inst_82344;
var inst_82344__$1 = tmp82382;
var state_82374__$1 = (function (){var statearr_82387 = state_82374;
(statearr_82387[(11)] = inst_82363);

(statearr_82387[(10)] = inst_82344__$1);

return statearr_82387;
})();
var statearr_82388_83813 = state_82374__$1;
(statearr_82388_83813[(2)] = null);

(statearr_82388_83813[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82375 === (9))){
var inst_82354 = (state_82374[(8)]);
var state_82374__$1 = state_82374;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82374__$1,(11),out,inst_82354);
} else {
if((state_val_82375 === (5))){
var inst_82368 = cljs.core.async.close_BANG_(out);
var state_82374__$1 = state_82374;
var statearr_82390_83820 = state_82374__$1;
(statearr_82390_83820[(2)] = inst_82368);

(statearr_82390_83820[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82375 === (10))){
var inst_82366 = (state_82374[(2)]);
var state_82374__$1 = state_82374;
var statearr_82391_83822 = state_82374__$1;
(statearr_82391_83822[(2)] = inst_82366);

(statearr_82391_83822[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82375 === (8))){
var inst_82355 = (state_82374[(9)]);
var inst_82353 = (state_82374[(7)]);
var inst_82344 = (state_82374[(10)]);
var inst_82354 = (state_82374[(8)]);
var inst_82358 = (function (){var cs = inst_82344;
var vec__82349 = inst_82353;
var v = inst_82354;
var c = inst_82355;
return ((function (cs,vec__82349,v,c,inst_82355,inst_82353,inst_82344,inst_82354,state_val_82375,c__80824__auto___83794,out){
return (function (p1__82337_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__82337_SHARP_);
});
;})(cs,vec__82349,v,c,inst_82355,inst_82353,inst_82344,inst_82354,state_val_82375,c__80824__auto___83794,out))
})();
var inst_82359 = cljs.core.filterv(inst_82358,inst_82344);
var inst_82344__$1 = inst_82359;
var state_82374__$1 = (function (){var statearr_82394 = state_82374;
(statearr_82394[(10)] = inst_82344__$1);

return statearr_82394;
})();
var statearr_82395_83839 = state_82374__$1;
(statearr_82395_83839[(2)] = null);

(statearr_82395_83839[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83794,out))
;
return ((function (switch__80630__auto__,c__80824__auto___83794,out){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82399 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_82399[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82399[(1)] = (1));

return statearr_82399;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82374){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82374);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82400){if((e82400 instanceof Object)){
var ex__80634__auto__ = e82400;
var statearr_82401_83841 = state_82374;
(statearr_82401_83841[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82374);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82400;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83842 = state_82374;
state_82374 = G__83842;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82374){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82374);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83794,out))
})();
var state__80826__auto__ = (function (){var statearr_82403 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82403[(6)] = c__80824__auto___83794);

return statearr_82403;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83794,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__82408 = arguments.length;
switch (G__82408) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__80824__auto___83847 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83847,out){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83847,out){
return (function (state_82432){
var state_val_82433 = (state_82432[(1)]);
if((state_val_82433 === (7))){
var inst_82414 = (state_82432[(7)]);
var inst_82414__$1 = (state_82432[(2)]);
var inst_82415 = (inst_82414__$1 == null);
var inst_82416 = cljs.core.not(inst_82415);
var state_82432__$1 = (function (){var statearr_82434 = state_82432;
(statearr_82434[(7)] = inst_82414__$1);

return statearr_82434;
})();
if(inst_82416){
var statearr_82435_83849 = state_82432__$1;
(statearr_82435_83849[(1)] = (8));

} else {
var statearr_82436_83850 = state_82432__$1;
(statearr_82436_83850[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (1))){
var inst_82409 = (0);
var state_82432__$1 = (function (){var statearr_82438 = state_82432;
(statearr_82438[(8)] = inst_82409);

return statearr_82438;
})();
var statearr_82439_83851 = state_82432__$1;
(statearr_82439_83851[(2)] = null);

(statearr_82439_83851[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (4))){
var state_82432__$1 = state_82432;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82432__$1,(7),ch);
} else {
if((state_val_82433 === (6))){
var inst_82427 = (state_82432[(2)]);
var state_82432__$1 = state_82432;
var statearr_82440_83855 = state_82432__$1;
(statearr_82440_83855[(2)] = inst_82427);

(statearr_82440_83855[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (3))){
var inst_82429 = (state_82432[(2)]);
var inst_82430 = cljs.core.async.close_BANG_(out);
var state_82432__$1 = (function (){var statearr_82442 = state_82432;
(statearr_82442[(9)] = inst_82429);

return statearr_82442;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_82432__$1,inst_82430);
} else {
if((state_val_82433 === (2))){
var inst_82409 = (state_82432[(8)]);
var inst_82411 = (inst_82409 < n);
var state_82432__$1 = state_82432;
if(cljs.core.truth_(inst_82411)){
var statearr_82443_83862 = state_82432__$1;
(statearr_82443_83862[(1)] = (4));

} else {
var statearr_82444_83863 = state_82432__$1;
(statearr_82444_83863[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (11))){
var inst_82409 = (state_82432[(8)]);
var inst_82419 = (state_82432[(2)]);
var inst_82420 = (inst_82409 + (1));
var inst_82409__$1 = inst_82420;
var state_82432__$1 = (function (){var statearr_82445 = state_82432;
(statearr_82445[(8)] = inst_82409__$1);

(statearr_82445[(10)] = inst_82419);

return statearr_82445;
})();
var statearr_82446_83864 = state_82432__$1;
(statearr_82446_83864[(2)] = null);

(statearr_82446_83864[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (9))){
var state_82432__$1 = state_82432;
var statearr_82447_83865 = state_82432__$1;
(statearr_82447_83865[(2)] = null);

(statearr_82447_83865[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (5))){
var state_82432__$1 = state_82432;
var statearr_82448_83866 = state_82432__$1;
(statearr_82448_83866[(2)] = null);

(statearr_82448_83866[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (10))){
var inst_82424 = (state_82432[(2)]);
var state_82432__$1 = state_82432;
var statearr_82449_83867 = state_82432__$1;
(statearr_82449_83867[(2)] = inst_82424);

(statearr_82449_83867[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82433 === (8))){
var inst_82414 = (state_82432[(7)]);
var state_82432__$1 = state_82432;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82432__$1,(11),out,inst_82414);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83847,out))
;
return ((function (switch__80630__auto__,c__80824__auto___83847,out){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82454 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_82454[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82454[(1)] = (1));

return statearr_82454;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82432){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82432);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82455){if((e82455 instanceof Object)){
var ex__80634__auto__ = e82455;
var statearr_82456_83869 = state_82432;
(statearr_82456_83869[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82432);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82455;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83874 = state_82432;
state_82432 = G__83874;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82432){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82432);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83847,out))
})();
var state__80826__auto__ = (function (){var statearr_82459 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82459[(6)] = c__80824__auto___83847);

return statearr_82459;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83847,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async82464 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async82464 = (function (f,ch,meta82465){
this.f = f;
this.ch = ch;
this.meta82465 = meta82465;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_82466,meta82465__$1){
var self__ = this;
var _82466__$1 = this;
return (new cljs.core.async.t_cljs$core$async82464(self__.f,self__.ch,meta82465__$1));
});

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_82466){
var self__ = this;
var _82466__$1 = this;
return self__.meta82465;
});

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async82478 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async82478 = (function (f,ch,meta82465,_,fn1,meta82479){
this.f = f;
this.ch = ch;
this.meta82465 = meta82465;
this._ = _;
this.fn1 = fn1;
this.meta82479 = meta82479;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async82478.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_82480,meta82479__$1){
var self__ = this;
var _82480__$1 = this;
return (new cljs.core.async.t_cljs$core$async82478(self__.f,self__.ch,self__.meta82465,self__._,self__.fn1,meta82479__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async82478.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_82480){
var self__ = this;
var _82480__$1 = this;
return self__.meta82479;
});})(___$1))
;

cljs.core.async.t_cljs$core$async82478.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82478.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async82478.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async82478.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__82463_SHARP_){
var G__82485 = (((p1__82463_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__82463_SHARP_) : self__.f.call(null,p1__82463_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__82485) : f1.call(null,G__82485));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async82478.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta82465","meta82465",1650541756,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async82464","cljs.core.async/t_cljs$core$async82464",-1578398823,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta82479","meta82479",969764073,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async82478.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async82478.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async82478";

cljs.core.async.t_cljs$core$async82478.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async82478");
});})(___$1))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async82478.
 */
cljs.core.async.__GT_t_cljs$core$async82478 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async82478(f__$1,ch__$1,meta82465__$1,___$2,fn1__$1,meta82479){
return (new cljs.core.async.t_cljs$core$async82478(f__$1,ch__$1,meta82465__$1,___$2,fn1__$1,meta82479));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async82478(self__.f,self__.ch,self__.meta82465,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4120__auto__ = ret;
if(cljs.core.truth_(and__4120__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4120__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__82493 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__82493) : self__.f.call(null,G__82493));
})());
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82464.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async82464.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta82465","meta82465",1650541756,null)], null);
});

cljs.core.async.t_cljs$core$async82464.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async82464.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async82464";

cljs.core.async.t_cljs$core$async82464.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async82464");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async82464.
 */
cljs.core.async.__GT_t_cljs$core$async82464 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async82464(f__$1,ch__$1,meta82465){
return (new cljs.core.async.t_cljs$core$async82464(f__$1,ch__$1,meta82465));
});

}

return (new cljs.core.async.t_cljs$core$async82464(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async82496 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async82496 = (function (f,ch,meta82497){
this.f = f;
this.ch = ch;
this.meta82497 = meta82497;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_82498,meta82497__$1){
var self__ = this;
var _82498__$1 = this;
return (new cljs.core.async.t_cljs$core$async82496(self__.f,self__.ch,meta82497__$1));
});

cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_82498){
var self__ = this;
var _82498__$1 = this;
return self__.meta82497;
});

cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82496.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
});

cljs.core.async.t_cljs$core$async82496.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta82497","meta82497",1449089115,null)], null);
});

cljs.core.async.t_cljs$core$async82496.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async82496.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async82496";

cljs.core.async.t_cljs$core$async82496.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async82496");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async82496.
 */
cljs.core.async.__GT_t_cljs$core$async82496 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async82496(f__$1,ch__$1,meta82497){
return (new cljs.core.async.t_cljs$core$async82496(f__$1,ch__$1,meta82497));
});

}

return (new cljs.core.async.t_cljs$core$async82496(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async82502 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async82502 = (function (p,ch,meta82503){
this.p = p;
this.ch = ch;
this.meta82503 = meta82503;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_82504,meta82503__$1){
var self__ = this;
var _82504__$1 = this;
return (new cljs.core.async.t_cljs$core$async82502(self__.p,self__.ch,meta82503__$1));
});

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_82504){
var self__ = this;
var _82504__$1 = this;
return self__.meta82503;
});

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async82502.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
});

cljs.core.async.t_cljs$core$async82502.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta82503","meta82503",1608042392,null)], null);
});

cljs.core.async.t_cljs$core$async82502.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async82502.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async82502";

cljs.core.async.t_cljs$core$async82502.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async82502");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async82502.
 */
cljs.core.async.__GT_t_cljs$core$async82502 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async82502(p__$1,ch__$1,meta82503){
return (new cljs.core.async.t_cljs$core$async82502(p__$1,ch__$1,meta82503));
});

}

return (new cljs.core.async.t_cljs$core$async82502(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__82521 = arguments.length;
switch (G__82521) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__80824__auto___83892 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83892,out){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83892,out){
return (function (state_82542){
var state_val_82543 = (state_82542[(1)]);
if((state_val_82543 === (7))){
var inst_82538 = (state_82542[(2)]);
var state_82542__$1 = state_82542;
var statearr_82547_83893 = state_82542__$1;
(statearr_82547_83893[(2)] = inst_82538);

(statearr_82547_83893[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (1))){
var state_82542__$1 = state_82542;
var statearr_82548_83894 = state_82542__$1;
(statearr_82548_83894[(2)] = null);

(statearr_82548_83894[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (4))){
var inst_82524 = (state_82542[(7)]);
var inst_82524__$1 = (state_82542[(2)]);
var inst_82525 = (inst_82524__$1 == null);
var state_82542__$1 = (function (){var statearr_82549 = state_82542;
(statearr_82549[(7)] = inst_82524__$1);

return statearr_82549;
})();
if(cljs.core.truth_(inst_82525)){
var statearr_82550_83895 = state_82542__$1;
(statearr_82550_83895[(1)] = (5));

} else {
var statearr_82552_83897 = state_82542__$1;
(statearr_82552_83897[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (6))){
var inst_82524 = (state_82542[(7)]);
var inst_82529 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_82524) : p.call(null,inst_82524));
var state_82542__$1 = state_82542;
if(cljs.core.truth_(inst_82529)){
var statearr_82553_83899 = state_82542__$1;
(statearr_82553_83899[(1)] = (8));

} else {
var statearr_82554_83900 = state_82542__$1;
(statearr_82554_83900[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (3))){
var inst_82540 = (state_82542[(2)]);
var state_82542__$1 = state_82542;
return cljs.core.async.impl.ioc_helpers.return_chan(state_82542__$1,inst_82540);
} else {
if((state_val_82543 === (2))){
var state_82542__$1 = state_82542;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82542__$1,(4),ch);
} else {
if((state_val_82543 === (11))){
var inst_82532 = (state_82542[(2)]);
var state_82542__$1 = state_82542;
var statearr_82556_83902 = state_82542__$1;
(statearr_82556_83902[(2)] = inst_82532);

(statearr_82556_83902[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (9))){
var state_82542__$1 = state_82542;
var statearr_82557_83903 = state_82542__$1;
(statearr_82557_83903[(2)] = null);

(statearr_82557_83903[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (5))){
var inst_82527 = cljs.core.async.close_BANG_(out);
var state_82542__$1 = state_82542;
var statearr_82559_83904 = state_82542__$1;
(statearr_82559_83904[(2)] = inst_82527);

(statearr_82559_83904[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (10))){
var inst_82535 = (state_82542[(2)]);
var state_82542__$1 = (function (){var statearr_82560 = state_82542;
(statearr_82560[(8)] = inst_82535);

return statearr_82560;
})();
var statearr_82561_83905 = state_82542__$1;
(statearr_82561_83905[(2)] = null);

(statearr_82561_83905[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82543 === (8))){
var inst_82524 = (state_82542[(7)]);
var state_82542__$1 = state_82542;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82542__$1,(11),out,inst_82524);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83892,out))
;
return ((function (switch__80630__auto__,c__80824__auto___83892,out){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82562 = [null,null,null,null,null,null,null,null,null];
(statearr_82562[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82562[(1)] = (1));

return statearr_82562;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82542){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82542);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82564){if((e82564 instanceof Object)){
var ex__80634__auto__ = e82564;
var statearr_82565_83906 = state_82542;
(statearr_82565_83906[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82542);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82564;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83908 = state_82542;
state_82542 = G__83908;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82542){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82542);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83892,out))
})();
var state__80826__auto__ = (function (){var statearr_82567 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82567[(6)] = c__80824__auto___83892);

return statearr_82567;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83892,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__82570 = arguments.length;
switch (G__82570) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__80824__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto__){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto__){
return (function (state_82633){
var state_val_82634 = (state_82633[(1)]);
if((state_val_82634 === (7))){
var inst_82629 = (state_82633[(2)]);
var state_82633__$1 = state_82633;
var statearr_82648_83910 = state_82633__$1;
(statearr_82648_83910[(2)] = inst_82629);

(statearr_82648_83910[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (20))){
var inst_82599 = (state_82633[(7)]);
var inst_82610 = (state_82633[(2)]);
var inst_82611 = cljs.core.next(inst_82599);
var inst_82585 = inst_82611;
var inst_82586 = null;
var inst_82587 = (0);
var inst_82588 = (0);
var state_82633__$1 = (function (){var statearr_82649 = state_82633;
(statearr_82649[(8)] = inst_82586);

(statearr_82649[(9)] = inst_82610);

(statearr_82649[(10)] = inst_82587);

(statearr_82649[(11)] = inst_82588);

(statearr_82649[(12)] = inst_82585);

return statearr_82649;
})();
var statearr_82651_83912 = state_82633__$1;
(statearr_82651_83912[(2)] = null);

(statearr_82651_83912[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (1))){
var state_82633__$1 = state_82633;
var statearr_82652_83913 = state_82633__$1;
(statearr_82652_83913[(2)] = null);

(statearr_82652_83913[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (4))){
var inst_82574 = (state_82633[(13)]);
var inst_82574__$1 = (state_82633[(2)]);
var inst_82575 = (inst_82574__$1 == null);
var state_82633__$1 = (function (){var statearr_82653 = state_82633;
(statearr_82653[(13)] = inst_82574__$1);

return statearr_82653;
})();
if(cljs.core.truth_(inst_82575)){
var statearr_82654_83914 = state_82633__$1;
(statearr_82654_83914[(1)] = (5));

} else {
var statearr_82655_83915 = state_82633__$1;
(statearr_82655_83915[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (15))){
var state_82633__$1 = state_82633;
var statearr_82659_83916 = state_82633__$1;
(statearr_82659_83916[(2)] = null);

(statearr_82659_83916[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (21))){
var state_82633__$1 = state_82633;
var statearr_82660_83917 = state_82633__$1;
(statearr_82660_83917[(2)] = null);

(statearr_82660_83917[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (13))){
var inst_82586 = (state_82633[(8)]);
var inst_82587 = (state_82633[(10)]);
var inst_82588 = (state_82633[(11)]);
var inst_82585 = (state_82633[(12)]);
var inst_82595 = (state_82633[(2)]);
var inst_82596 = (inst_82588 + (1));
var tmp82656 = inst_82586;
var tmp82657 = inst_82587;
var tmp82658 = inst_82585;
var inst_82585__$1 = tmp82658;
var inst_82586__$1 = tmp82656;
var inst_82587__$1 = tmp82657;
var inst_82588__$1 = inst_82596;
var state_82633__$1 = (function (){var statearr_82664 = state_82633;
(statearr_82664[(8)] = inst_82586__$1);

(statearr_82664[(10)] = inst_82587__$1);

(statearr_82664[(11)] = inst_82588__$1);

(statearr_82664[(12)] = inst_82585__$1);

(statearr_82664[(14)] = inst_82595);

return statearr_82664;
})();
var statearr_82665_83919 = state_82633__$1;
(statearr_82665_83919[(2)] = null);

(statearr_82665_83919[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (22))){
var state_82633__$1 = state_82633;
var statearr_82667_83921 = state_82633__$1;
(statearr_82667_83921[(2)] = null);

(statearr_82667_83921[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (6))){
var inst_82574 = (state_82633[(13)]);
var inst_82583 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_82574) : f.call(null,inst_82574));
var inst_82584 = cljs.core.seq(inst_82583);
var inst_82585 = inst_82584;
var inst_82586 = null;
var inst_82587 = (0);
var inst_82588 = (0);
var state_82633__$1 = (function (){var statearr_82668 = state_82633;
(statearr_82668[(8)] = inst_82586);

(statearr_82668[(10)] = inst_82587);

(statearr_82668[(11)] = inst_82588);

(statearr_82668[(12)] = inst_82585);

return statearr_82668;
})();
var statearr_82669_83923 = state_82633__$1;
(statearr_82669_83923[(2)] = null);

(statearr_82669_83923[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (17))){
var inst_82599 = (state_82633[(7)]);
var inst_82603 = cljs.core.chunk_first(inst_82599);
var inst_82604 = cljs.core.chunk_rest(inst_82599);
var inst_82605 = cljs.core.count(inst_82603);
var inst_82585 = inst_82604;
var inst_82586 = inst_82603;
var inst_82587 = inst_82605;
var inst_82588 = (0);
var state_82633__$1 = (function (){var statearr_82671 = state_82633;
(statearr_82671[(8)] = inst_82586);

(statearr_82671[(10)] = inst_82587);

(statearr_82671[(11)] = inst_82588);

(statearr_82671[(12)] = inst_82585);

return statearr_82671;
})();
var statearr_82672_83924 = state_82633__$1;
(statearr_82672_83924[(2)] = null);

(statearr_82672_83924[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (3))){
var inst_82631 = (state_82633[(2)]);
var state_82633__$1 = state_82633;
return cljs.core.async.impl.ioc_helpers.return_chan(state_82633__$1,inst_82631);
} else {
if((state_val_82634 === (12))){
var inst_82619 = (state_82633[(2)]);
var state_82633__$1 = state_82633;
var statearr_82673_83925 = state_82633__$1;
(statearr_82673_83925[(2)] = inst_82619);

(statearr_82673_83925[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (2))){
var state_82633__$1 = state_82633;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82633__$1,(4),in$);
} else {
if((state_val_82634 === (23))){
var inst_82627 = (state_82633[(2)]);
var state_82633__$1 = state_82633;
var statearr_82674_83926 = state_82633__$1;
(statearr_82674_83926[(2)] = inst_82627);

(statearr_82674_83926[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (19))){
var inst_82614 = (state_82633[(2)]);
var state_82633__$1 = state_82633;
var statearr_82675_83927 = state_82633__$1;
(statearr_82675_83927[(2)] = inst_82614);

(statearr_82675_83927[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (11))){
var inst_82599 = (state_82633[(7)]);
var inst_82585 = (state_82633[(12)]);
var inst_82599__$1 = cljs.core.seq(inst_82585);
var state_82633__$1 = (function (){var statearr_82676 = state_82633;
(statearr_82676[(7)] = inst_82599__$1);

return statearr_82676;
})();
if(inst_82599__$1){
var statearr_82677_83928 = state_82633__$1;
(statearr_82677_83928[(1)] = (14));

} else {
var statearr_82678_83929 = state_82633__$1;
(statearr_82678_83929[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (9))){
var inst_82621 = (state_82633[(2)]);
var inst_82622 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_82633__$1 = (function (){var statearr_82679 = state_82633;
(statearr_82679[(15)] = inst_82621);

return statearr_82679;
})();
if(cljs.core.truth_(inst_82622)){
var statearr_82681_83932 = state_82633__$1;
(statearr_82681_83932[(1)] = (21));

} else {
var statearr_82683_83933 = state_82633__$1;
(statearr_82683_83933[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (5))){
var inst_82577 = cljs.core.async.close_BANG_(out);
var state_82633__$1 = state_82633;
var statearr_82684_83934 = state_82633__$1;
(statearr_82684_83934[(2)] = inst_82577);

(statearr_82684_83934[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (14))){
var inst_82599 = (state_82633[(7)]);
var inst_82601 = cljs.core.chunked_seq_QMARK_(inst_82599);
var state_82633__$1 = state_82633;
if(inst_82601){
var statearr_82691_83935 = state_82633__$1;
(statearr_82691_83935[(1)] = (17));

} else {
var statearr_82693_83936 = state_82633__$1;
(statearr_82693_83936[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (16))){
var inst_82617 = (state_82633[(2)]);
var state_82633__$1 = state_82633;
var statearr_82694_83937 = state_82633__$1;
(statearr_82694_83937[(2)] = inst_82617);

(statearr_82694_83937[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82634 === (10))){
var inst_82586 = (state_82633[(8)]);
var inst_82588 = (state_82633[(11)]);
var inst_82593 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_82586,inst_82588);
var state_82633__$1 = state_82633;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82633__$1,(13),out,inst_82593);
} else {
if((state_val_82634 === (18))){
var inst_82599 = (state_82633[(7)]);
var inst_82608 = cljs.core.first(inst_82599);
var state_82633__$1 = state_82633;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82633__$1,(20),out,inst_82608);
} else {
if((state_val_82634 === (8))){
var inst_82587 = (state_82633[(10)]);
var inst_82588 = (state_82633[(11)]);
var inst_82590 = (inst_82588 < inst_82587);
var inst_82591 = inst_82590;
var state_82633__$1 = state_82633;
if(cljs.core.truth_(inst_82591)){
var statearr_82696_83942 = state_82633__$1;
(statearr_82696_83942[(1)] = (10));

} else {
var statearr_82697_83943 = state_82633__$1;
(statearr_82697_83943[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto__))
;
return ((function (switch__80630__auto__,c__80824__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__80631__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__80631__auto____0 = (function (){
var statearr_82698 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_82698[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__80631__auto__);

(statearr_82698[(1)] = (1));

return statearr_82698;
});
var cljs$core$async$mapcat_STAR__$_state_machine__80631__auto____1 = (function (state_82633){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82633);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82699){if((e82699 instanceof Object)){
var ex__80634__auto__ = e82699;
var statearr_82700_83944 = state_82633;
(statearr_82700_83944[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82633);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82699;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83945 = state_82633;
state_82633 = G__83945;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__80631__auto__ = function(state_82633){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__80631__auto____1.call(this,state_82633);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__80631__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__80631__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto__))
})();
var state__80826__auto__ = (function (){var statearr_82703 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82703[(6)] = c__80824__auto__);

return statearr_82703;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto__))
);

return c__80824__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__82705 = arguments.length;
switch (G__82705) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__82708 = arguments.length;
switch (G__82708) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__82714 = arguments.length;
switch (G__82714) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__80824__auto___83967 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83967,out){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83967,out){
return (function (state_82749){
var state_val_82750 = (state_82749[(1)]);
if((state_val_82750 === (7))){
var inst_82744 = (state_82749[(2)]);
var state_82749__$1 = state_82749;
var statearr_82754_83968 = state_82749__$1;
(statearr_82754_83968[(2)] = inst_82744);

(statearr_82754_83968[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82750 === (1))){
var inst_82726 = null;
var state_82749__$1 = (function (){var statearr_82756 = state_82749;
(statearr_82756[(7)] = inst_82726);

return statearr_82756;
})();
var statearr_82757_83969 = state_82749__$1;
(statearr_82757_83969[(2)] = null);

(statearr_82757_83969[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82750 === (4))){
var inst_82729 = (state_82749[(8)]);
var inst_82729__$1 = (state_82749[(2)]);
var inst_82730 = (inst_82729__$1 == null);
var inst_82731 = cljs.core.not(inst_82730);
var state_82749__$1 = (function (){var statearr_82758 = state_82749;
(statearr_82758[(8)] = inst_82729__$1);

return statearr_82758;
})();
if(inst_82731){
var statearr_82759_83974 = state_82749__$1;
(statearr_82759_83974[(1)] = (5));

} else {
var statearr_82761_83975 = state_82749__$1;
(statearr_82761_83975[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82750 === (6))){
var state_82749__$1 = state_82749;
var statearr_82762_83976 = state_82749__$1;
(statearr_82762_83976[(2)] = null);

(statearr_82762_83976[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82750 === (3))){
var inst_82746 = (state_82749[(2)]);
var inst_82747 = cljs.core.async.close_BANG_(out);
var state_82749__$1 = (function (){var statearr_82763 = state_82749;
(statearr_82763[(9)] = inst_82746);

return statearr_82763;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_82749__$1,inst_82747);
} else {
if((state_val_82750 === (2))){
var state_82749__$1 = state_82749;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82749__$1,(4),ch);
} else {
if((state_val_82750 === (11))){
var inst_82729 = (state_82749[(8)]);
var inst_82738 = (state_82749[(2)]);
var inst_82726 = inst_82729;
var state_82749__$1 = (function (){var statearr_82771 = state_82749;
(statearr_82771[(7)] = inst_82726);

(statearr_82771[(10)] = inst_82738);

return statearr_82771;
})();
var statearr_82773_83981 = state_82749__$1;
(statearr_82773_83981[(2)] = null);

(statearr_82773_83981[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82750 === (9))){
var inst_82729 = (state_82749[(8)]);
var state_82749__$1 = state_82749;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82749__$1,(11),out,inst_82729);
} else {
if((state_val_82750 === (5))){
var inst_82726 = (state_82749[(7)]);
var inst_82729 = (state_82749[(8)]);
var inst_82733 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_82729,inst_82726);
var state_82749__$1 = state_82749;
if(inst_82733){
var statearr_82777_83982 = state_82749__$1;
(statearr_82777_83982[(1)] = (8));

} else {
var statearr_82778_83983 = state_82749__$1;
(statearr_82778_83983[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82750 === (10))){
var inst_82741 = (state_82749[(2)]);
var state_82749__$1 = state_82749;
var statearr_82781_83988 = state_82749__$1;
(statearr_82781_83988[(2)] = inst_82741);

(statearr_82781_83988[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82750 === (8))){
var inst_82726 = (state_82749[(7)]);
var tmp82775 = inst_82726;
var inst_82726__$1 = tmp82775;
var state_82749__$1 = (function (){var statearr_82782 = state_82749;
(statearr_82782[(7)] = inst_82726__$1);

return statearr_82782;
})();
var statearr_82783_83989 = state_82749__$1;
(statearr_82783_83989[(2)] = null);

(statearr_82783_83989[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83967,out))
;
return ((function (switch__80630__auto__,c__80824__auto___83967,out){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82786 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_82786[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82786[(1)] = (1));

return statearr_82786;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82749){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82749);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82789){if((e82789 instanceof Object)){
var ex__80634__auto__ = e82789;
var statearr_82790_83991 = state_82749;
(statearr_82790_83991[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82749);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82789;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__83992 = state_82749;
state_82749 = G__83992;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82749){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82749);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83967,out))
})();
var state__80826__auto__ = (function (){var statearr_82793 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82793[(6)] = c__80824__auto___83967);

return statearr_82793;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83967,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__82797 = arguments.length;
switch (G__82797) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__80824__auto___83997 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___83997,out){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___83997,out){
return (function (state_82842){
var state_val_82843 = (state_82842[(1)]);
if((state_val_82843 === (7))){
var inst_82837 = (state_82842[(2)]);
var state_82842__$1 = state_82842;
var statearr_82844_84002 = state_82842__$1;
(statearr_82844_84002[(2)] = inst_82837);

(statearr_82844_84002[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (1))){
var inst_82802 = (new Array(n));
var inst_82803 = inst_82802;
var inst_82804 = (0);
var state_82842__$1 = (function (){var statearr_82845 = state_82842;
(statearr_82845[(7)] = inst_82803);

(statearr_82845[(8)] = inst_82804);

return statearr_82845;
})();
var statearr_82846_84007 = state_82842__$1;
(statearr_82846_84007[(2)] = null);

(statearr_82846_84007[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (4))){
var inst_82807 = (state_82842[(9)]);
var inst_82807__$1 = (state_82842[(2)]);
var inst_82808 = (inst_82807__$1 == null);
var inst_82809 = cljs.core.not(inst_82808);
var state_82842__$1 = (function (){var statearr_82847 = state_82842;
(statearr_82847[(9)] = inst_82807__$1);

return statearr_82847;
})();
if(inst_82809){
var statearr_82848_84009 = state_82842__$1;
(statearr_82848_84009[(1)] = (5));

} else {
var statearr_82852_84010 = state_82842__$1;
(statearr_82852_84010[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (15))){
var inst_82831 = (state_82842[(2)]);
var state_82842__$1 = state_82842;
var statearr_82853_84013 = state_82842__$1;
(statearr_82853_84013[(2)] = inst_82831);

(statearr_82853_84013[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (13))){
var state_82842__$1 = state_82842;
var statearr_82854_84020 = state_82842__$1;
(statearr_82854_84020[(2)] = null);

(statearr_82854_84020[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (6))){
var inst_82804 = (state_82842[(8)]);
var inst_82826 = (inst_82804 > (0));
var state_82842__$1 = state_82842;
if(cljs.core.truth_(inst_82826)){
var statearr_82855_84023 = state_82842__$1;
(statearr_82855_84023[(1)] = (12));

} else {
var statearr_82856_84024 = state_82842__$1;
(statearr_82856_84024[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (3))){
var inst_82839 = (state_82842[(2)]);
var state_82842__$1 = state_82842;
return cljs.core.async.impl.ioc_helpers.return_chan(state_82842__$1,inst_82839);
} else {
if((state_val_82843 === (12))){
var inst_82803 = (state_82842[(7)]);
var inst_82829 = cljs.core.vec(inst_82803);
var state_82842__$1 = state_82842;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82842__$1,(15),out,inst_82829);
} else {
if((state_val_82843 === (2))){
var state_82842__$1 = state_82842;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82842__$1,(4),ch);
} else {
if((state_val_82843 === (11))){
var inst_82819 = (state_82842[(2)]);
var inst_82820 = (new Array(n));
var inst_82803 = inst_82820;
var inst_82804 = (0);
var state_82842__$1 = (function (){var statearr_82859 = state_82842;
(statearr_82859[(10)] = inst_82819);

(statearr_82859[(7)] = inst_82803);

(statearr_82859[(8)] = inst_82804);

return statearr_82859;
})();
var statearr_82862_84032 = state_82842__$1;
(statearr_82862_84032[(2)] = null);

(statearr_82862_84032[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (9))){
var inst_82803 = (state_82842[(7)]);
var inst_82817 = cljs.core.vec(inst_82803);
var state_82842__$1 = state_82842;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82842__$1,(11),out,inst_82817);
} else {
if((state_val_82843 === (5))){
var inst_82812 = (state_82842[(11)]);
var inst_82803 = (state_82842[(7)]);
var inst_82804 = (state_82842[(8)]);
var inst_82807 = (state_82842[(9)]);
var inst_82811 = (inst_82803[inst_82804] = inst_82807);
var inst_82812__$1 = (inst_82804 + (1));
var inst_82813 = (inst_82812__$1 < n);
var state_82842__$1 = (function (){var statearr_82863 = state_82842;
(statearr_82863[(11)] = inst_82812__$1);

(statearr_82863[(12)] = inst_82811);

return statearr_82863;
})();
if(cljs.core.truth_(inst_82813)){
var statearr_82864_84042 = state_82842__$1;
(statearr_82864_84042[(1)] = (8));

} else {
var statearr_82865_84047 = state_82842__$1;
(statearr_82865_84047[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (14))){
var inst_82834 = (state_82842[(2)]);
var inst_82835 = cljs.core.async.close_BANG_(out);
var state_82842__$1 = (function (){var statearr_82870 = state_82842;
(statearr_82870[(13)] = inst_82834);

return statearr_82870;
})();
var statearr_82872_84051 = state_82842__$1;
(statearr_82872_84051[(2)] = inst_82835);

(statearr_82872_84051[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (10))){
var inst_82823 = (state_82842[(2)]);
var state_82842__$1 = state_82842;
var statearr_82873_84052 = state_82842__$1;
(statearr_82873_84052[(2)] = inst_82823);

(statearr_82873_84052[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82843 === (8))){
var inst_82812 = (state_82842[(11)]);
var inst_82803 = (state_82842[(7)]);
var tmp82866 = inst_82803;
var inst_82803__$1 = tmp82866;
var inst_82804 = inst_82812;
var state_82842__$1 = (function (){var statearr_82874 = state_82842;
(statearr_82874[(7)] = inst_82803__$1);

(statearr_82874[(8)] = inst_82804);

return statearr_82874;
})();
var statearr_82875_84058 = state_82842__$1;
(statearr_82875_84058[(2)] = null);

(statearr_82875_84058[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___83997,out))
;
return ((function (switch__80630__auto__,c__80824__auto___83997,out){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82876 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_82876[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82876[(1)] = (1));

return statearr_82876;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82842){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82842);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82877){if((e82877 instanceof Object)){
var ex__80634__auto__ = e82877;
var statearr_82878_84061 = state_82842;
(statearr_82878_84061[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82842);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82877;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__84062 = state_82842;
state_82842 = G__84062;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82842){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82842);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___83997,out))
})();
var state__80826__auto__ = (function (){var statearr_82879 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82879[(6)] = c__80824__auto___83997);

return statearr_82879;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___83997,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__82885 = arguments.length;
switch (G__82885) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__80824__auto___84099 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___84099,out){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___84099,out){
return (function (state_82940){
var state_val_82941 = (state_82940[(1)]);
if((state_val_82941 === (7))){
var inst_82936 = (state_82940[(2)]);
var state_82940__$1 = state_82940;
var statearr_82945_84105 = state_82940__$1;
(statearr_82945_84105[(2)] = inst_82936);

(statearr_82945_84105[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (1))){
var inst_82887 = [];
var inst_82888 = inst_82887;
var inst_82889 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_82940__$1 = (function (){var statearr_82946 = state_82940;
(statearr_82946[(7)] = inst_82888);

(statearr_82946[(8)] = inst_82889);

return statearr_82946;
})();
var statearr_82950_84115 = state_82940__$1;
(statearr_82950_84115[(2)] = null);

(statearr_82950_84115[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (4))){
var inst_82892 = (state_82940[(9)]);
var inst_82892__$1 = (state_82940[(2)]);
var inst_82893 = (inst_82892__$1 == null);
var inst_82894 = cljs.core.not(inst_82893);
var state_82940__$1 = (function (){var statearr_82954 = state_82940;
(statearr_82954[(9)] = inst_82892__$1);

return statearr_82954;
})();
if(inst_82894){
var statearr_82958_84120 = state_82940__$1;
(statearr_82958_84120[(1)] = (5));

} else {
var statearr_82959_84122 = state_82940__$1;
(statearr_82959_84122[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (15))){
var inst_82930 = (state_82940[(2)]);
var state_82940__$1 = state_82940;
var statearr_82960_84125 = state_82940__$1;
(statearr_82960_84125[(2)] = inst_82930);

(statearr_82960_84125[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (13))){
var state_82940__$1 = state_82940;
var statearr_82961_84126 = state_82940__$1;
(statearr_82961_84126[(2)] = null);

(statearr_82961_84126[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (6))){
var inst_82888 = (state_82940[(7)]);
var inst_82922 = inst_82888.length;
var inst_82923 = (inst_82922 > (0));
var state_82940__$1 = state_82940;
if(cljs.core.truth_(inst_82923)){
var statearr_82968_84132 = state_82940__$1;
(statearr_82968_84132[(1)] = (12));

} else {
var statearr_82969_84133 = state_82940__$1;
(statearr_82969_84133[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (3))){
var inst_82938 = (state_82940[(2)]);
var state_82940__$1 = state_82940;
return cljs.core.async.impl.ioc_helpers.return_chan(state_82940__$1,inst_82938);
} else {
if((state_val_82941 === (12))){
var inst_82888 = (state_82940[(7)]);
var inst_82928 = cljs.core.vec(inst_82888);
var state_82940__$1 = state_82940;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82940__$1,(15),out,inst_82928);
} else {
if((state_val_82941 === (2))){
var state_82940__$1 = state_82940;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_82940__$1,(4),ch);
} else {
if((state_val_82941 === (11))){
var inst_82896 = (state_82940[(10)]);
var inst_82892 = (state_82940[(9)]);
var inst_82912 = (state_82940[(2)]);
var inst_82913 = [];
var inst_82914 = inst_82913.push(inst_82892);
var inst_82888 = inst_82913;
var inst_82889 = inst_82896;
var state_82940__$1 = (function (){var statearr_82970 = state_82940;
(statearr_82970[(11)] = inst_82912);

(statearr_82970[(7)] = inst_82888);

(statearr_82970[(8)] = inst_82889);

(statearr_82970[(12)] = inst_82914);

return statearr_82970;
})();
var statearr_82971_84157 = state_82940__$1;
(statearr_82971_84157[(2)] = null);

(statearr_82971_84157[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (9))){
var inst_82888 = (state_82940[(7)]);
var inst_82910 = cljs.core.vec(inst_82888);
var state_82940__$1 = state_82940;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_82940__$1,(11),out,inst_82910);
} else {
if((state_val_82941 === (5))){
var inst_82896 = (state_82940[(10)]);
var inst_82892 = (state_82940[(9)]);
var inst_82889 = (state_82940[(8)]);
var inst_82896__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_82892) : f.call(null,inst_82892));
var inst_82897 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_82896__$1,inst_82889);
var inst_82898 = cljs.core.keyword_identical_QMARK_(inst_82889,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_82899 = ((inst_82897) || (inst_82898));
var state_82940__$1 = (function (){var statearr_82972 = state_82940;
(statearr_82972[(10)] = inst_82896__$1);

return statearr_82972;
})();
if(cljs.core.truth_(inst_82899)){
var statearr_82973_84172 = state_82940__$1;
(statearr_82973_84172[(1)] = (8));

} else {
var statearr_82974_84173 = state_82940__$1;
(statearr_82974_84173[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (14))){
var inst_82933 = (state_82940[(2)]);
var inst_82934 = cljs.core.async.close_BANG_(out);
var state_82940__$1 = (function (){var statearr_82978 = state_82940;
(statearr_82978[(13)] = inst_82933);

return statearr_82978;
})();
var statearr_82979_84176 = state_82940__$1;
(statearr_82979_84176[(2)] = inst_82934);

(statearr_82979_84176[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (10))){
var inst_82920 = (state_82940[(2)]);
var state_82940__$1 = state_82940;
var statearr_82980_84177 = state_82940__$1;
(statearr_82980_84177[(2)] = inst_82920);

(statearr_82980_84177[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_82941 === (8))){
var inst_82896 = (state_82940[(10)]);
var inst_82888 = (state_82940[(7)]);
var inst_82892 = (state_82940[(9)]);
var inst_82903 = inst_82888.push(inst_82892);
var tmp82976 = inst_82888;
var inst_82888__$1 = tmp82976;
var inst_82889 = inst_82896;
var state_82940__$1 = (function (){var statearr_82981 = state_82940;
(statearr_82981[(7)] = inst_82888__$1);

(statearr_82981[(8)] = inst_82889);

(statearr_82981[(14)] = inst_82903);

return statearr_82981;
})();
var statearr_82982_84190 = state_82940__$1;
(statearr_82982_84190[(2)] = null);

(statearr_82982_84190[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___84099,out))
;
return ((function (switch__80630__auto__,c__80824__auto___84099,out){
return (function() {
var cljs$core$async$state_machine__80631__auto__ = null;
var cljs$core$async$state_machine__80631__auto____0 = (function (){
var statearr_82986 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_82986[(0)] = cljs$core$async$state_machine__80631__auto__);

(statearr_82986[(1)] = (1));

return statearr_82986;
});
var cljs$core$async$state_machine__80631__auto____1 = (function (state_82940){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_82940);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e82987){if((e82987 instanceof Object)){
var ex__80634__auto__ = e82987;
var statearr_82988_84205 = state_82940;
(statearr_82988_84205[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_82940);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e82987;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__84211 = state_82940;
state_82940 = G__84211;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
cljs$core$async$state_machine__80631__auto__ = function(state_82940){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__80631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__80631__auto____1.call(this,state_82940);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__80631__auto____0;
cljs$core$async$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__80631__auto____1;
return cljs$core$async$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___84099,out))
})();
var state__80826__auto__ = (function (){var statearr_82989 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_82989[(6)] = c__80824__auto___84099);

return statearr_82989;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___84099,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=cljs.core.async.js.map
