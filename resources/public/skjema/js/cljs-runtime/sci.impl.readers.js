goog.provide('sci.impl.readers');
goog.require('cljs.core');
goog.require('clojure.walk');
goog.require('sci.impl.utils');
sci.impl.readers.read_fn = (function sci$impl$readers$read_fn(expr){
var state = cljs.core.volatile_BANG_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"max-fixed","max-fixed",166770124),(0),new cljs.core.Keyword(null,"var-args?","var-args?",-1630678710),false], null));
var expr__$1 = clojure.walk.postwalk(((function (state){
return (function (elt){
if((elt instanceof cljs.core.Symbol)){
var temp__5733__auto__ = cljs.core.re_matches(/^%(.*)/,cljs.core.name(elt));
if(cljs.core.truth_(temp__5733__auto__)){
var vec__88351 = temp__5733__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88351,(0),null);
var m = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88351,(1),null);
if(cljs.core.empty_QMARK_(m)){
state.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.update.cljs$core$IFn$_invoke$arity$4(state.cljs$core$IDeref$_deref$arity$1(null),new cljs.core.Keyword(null,"max-fixed","max-fixed",166770124),cljs.core.max,(1)));

return new cljs.core.Symbol(null,"%1","%1",1309450150,null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("&",m)){
state.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(state.cljs$core$IDeref$_deref$arity$1(null),new cljs.core.Keyword(null,"var-args?","var-args?",-1630678710),true));

return elt;
} else {
var n_88375 = parseInt(m);
state.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.update.cljs$core$IFn$_invoke$arity$4(state.cljs$core$IDeref$_deref$arity$1(null),new cljs.core.Keyword(null,"max-fixed","max-fixed",166770124),cljs.core.max,n_88375));

return elt;

}
}
} else {
return elt;
}
} else {
return elt;
}
});})(state))
,expr);
var map__88344 = cljs.core.deref(state);
var map__88344__$1 = (((((!((map__88344 == null))))?(((((map__88344.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__88344.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__88344):map__88344);
var max_fixed = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88344__$1,new cljs.core.Keyword(null,"max-fixed","max-fixed",166770124));
var var_args_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__88344__$1,new cljs.core.Keyword(null,"var-args?","var-args?",-1630678710));
var fixed_names = cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (state,expr__$1,map__88344,map__88344__$1,max_fixed,var_args_QMARK_){
return (function (p1__88339_SHARP_){
return cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(["%",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__88339_SHARP_)].join(''));
});})(state,expr__$1,map__88344,map__88344__$1,max_fixed,var_args_QMARK_))
,cljs.core.range.cljs$core$IFn$_invoke$arity$2((1),(max_fixed + (1))));
var fixed_names__$1 = cljs.core.map.cljs$core$IFn$_invoke$arity$2(sci.impl.utils.mark_resolve_sym,fixed_names);
var var_args_sym = sci.impl.utils.mark_resolve_sym(new cljs.core.Symbol(null,"%&","%&",-728707069,null));
var arg_list = cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fixed_names__$1,(cljs.core.truth_(var_args_QMARK_)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"&","&",-2144855648,null),var_args_sym], null):null)));
var destructure_vec = cljs.core.vec(cljs.core.interleave.cljs$core$IFn$_invoke$arity$2(fixed_names__$1,fixed_names__$1));
var destructure_vec__$1 = (cljs.core.truth_(var_args_QMARK_)?cljs.core.conj.cljs$core$IFn$_invoke$arity$variadic(destructure_vec,var_args_sym,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([var_args_sym], 0)):destructure_vec);
var form = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("sci.impl","fn","sci.impl/fn",1695180073),true,new cljs.core.Keyword("sci.impl","fn-bodies","sci.impl/fn-bodies",134751661),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("sci.impl","binding-vector","sci.impl/binding-vector",-181335739),arg_list,new cljs.core.Keyword("sci.impl","body","sci.impl/body",-1493886648),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [expr__$1], null),new cljs.core.Keyword("sci.impl","fixed-arity","sci.impl/fixed-arity",-1251617052),cljs.core.count(fixed_names__$1),new cljs.core.Keyword("sci.impl","destructure-vec","sci.impl/destructure-vec",-692349508),destructure_vec__$1,new cljs.core.Keyword("sci.impl","arg-list","sci.impl/arg-list",649854284),arg_list,new cljs.core.Keyword("sci.impl","fixed-names","sci.impl/fixed-names",-1163180677),fixed_names__$1,new cljs.core.Keyword("sci.impl","var-arg-name","sci.impl/var-arg-name",1800498100),(cljs.core.truth_(var_args_QMARK_)?new cljs.core.Symbol(null,"%&","%&",-728707069,null):null)], null)], null)], null);
return form;
});

//# sourceMappingURL=sci.impl.readers.js.map
