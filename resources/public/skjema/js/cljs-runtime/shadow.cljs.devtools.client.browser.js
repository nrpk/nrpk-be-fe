goog.provide('shadow.cljs.devtools.client.browser');
goog.require('cljs.core');
goog.require('cljs.reader');
goog.require('clojure.string');
goog.require('goog.dom');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');
goog.require('shadow.cljs.devtools.client.env');
goog.require('shadow.cljs.devtools.client.console');
goog.require('shadow.cljs.devtools.client.hud');
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.active_modules_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.active_modules_ref = cljs.core.volatile_BANG_(cljs.core.PersistentHashSet.EMPTY);
}
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.repl_ns_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.repl_ns_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
shadow.cljs.devtools.client.browser.module_loaded = (function shadow$cljs$devtools$client$browser$module_loaded(name){
return shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IDeref$_deref$arity$1(null),cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(name)));
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.socket_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.socket_ref = cljs.core.volatile_BANG_(null);
}
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___87245 = arguments.length;
var i__4731__auto___87246 = (0);
while(true){
if((i__4731__auto___87246 < len__4730__auto___87245)){
args__4736__auto__.push((arguments[i__4731__auto___87246]));

var G__87248 = (i__4731__auto___87246 + (1));
i__4731__auto___87246 = G__87248;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq86964){
var G__86965 = cljs.core.first(seq86964);
var seq86964__$1 = cljs.core.next(seq86964);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__86965,seq86964__$1);
});

shadow.cljs.devtools.client.browser.ws_msg = (function shadow$cljs$devtools$client$browser$ws_msg(msg){
var temp__5733__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5733__auto__)){
var s = temp__5733__auto__;
return s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
} else {
return console.warn("WEBSOCKET NOT CONNECTED",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
}
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.scripts_to_load !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.scripts_to_load = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
shadow.cljs.devtools.client.browser.loaded_QMARK_ = goog.isProvided_;
shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$goog_is_loaded_QMARK_(name){
return $CLJS.SHADOW_ENV.isLoaded(name);
});
shadow.cljs.devtools.client.browser.goog_base_rc = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("shadow.build.classpath","resource","shadow.build.classpath/resource",-879517823),"goog/base.js"], null);
shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$src_is_loaded_QMARK_(p__86975){
var map__86976 = p__86975;
var map__86976__$1 = (((((!((map__86976 == null))))?(((((map__86976.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__86976.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__86976):map__86976);
var src = map__86976__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86976__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86976__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var or__4131__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.goog_base_rc,resource_id);
if(or__4131__auto__){
return or__4131__auto__;
} else {
return shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_(output_name);
}
});
shadow.cljs.devtools.client.browser.module_is_active_QMARK_ = (function shadow$cljs$devtools$client$browser$module_is_active_QMARK_(module){
return cljs.core.contains_QMARK_(cljs.core.deref(shadow.cljs.devtools.client.browser.active_modules_ref),module);
});
shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__86990 = cljs.core.seq(sources);
var chunk__86991 = null;
var count__86992 = (0);
var i__86993 = (0);
while(true){
if((i__86993 < count__86992)){
var map__87004 = chunk__86991.cljs$core$IIndexed$_nth$arity$2(null,i__86993);
var map__87004__$1 = (((((!((map__87004 == null))))?(((((map__87004.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87004.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87004):map__87004);
var src = map__87004__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87004__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87004__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87004__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87004__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));
}catch (e87007){var e_87253 = e87007;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_87253);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_87253.message)].join('')));
}

var G__87254 = seq__86990;
var G__87255 = chunk__86991;
var G__87256 = count__86992;
var G__87257 = (i__86993 + (1));
seq__86990 = G__87254;
chunk__86991 = G__87255;
count__86992 = G__87256;
i__86993 = G__87257;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__86990);
if(temp__5735__auto__){
var seq__86990__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__86990__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__86990__$1);
var G__87258 = cljs.core.chunk_rest(seq__86990__$1);
var G__87259 = c__4550__auto__;
var G__87260 = cljs.core.count(c__4550__auto__);
var G__87261 = (0);
seq__86990 = G__87258;
chunk__86991 = G__87259;
count__86992 = G__87260;
i__86993 = G__87261;
continue;
} else {
var map__87009 = cljs.core.first(seq__86990__$1);
var map__87009__$1 = (((((!((map__87009 == null))))?(((((map__87009.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87009.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87009):map__87009);
var src = map__87009__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87009__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87009__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87009__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87009__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));
}catch (e87011){var e_87265 = e87011;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_87265);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_87265.message)].join('')));
}

var G__87268 = cljs.core.next(seq__86990__$1);
var G__87269 = null;
var G__87270 = (0);
var G__87271 = (0);
seq__86990 = G__87268;
chunk__86991 = G__87269;
count__86992 = G__87270;
i__86993 = G__87271;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["can't find fn ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__87015 = cljs.core.seq(js_requires);
var chunk__87016 = null;
var count__87017 = (0);
var i__87018 = (0);
while(true){
if((i__87018 < count__87017)){
var js_ns = chunk__87016.cljs$core$IIndexed$_nth$arity$2(null,i__87018);
var require_str_87280 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_87280);


var G__87281 = seq__87015;
var G__87282 = chunk__87016;
var G__87283 = count__87017;
var G__87284 = (i__87018 + (1));
seq__87015 = G__87281;
chunk__87016 = G__87282;
count__87017 = G__87283;
i__87018 = G__87284;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__87015);
if(temp__5735__auto__){
var seq__87015__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__87015__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__87015__$1);
var G__87285 = cljs.core.chunk_rest(seq__87015__$1);
var G__87286 = c__4550__auto__;
var G__87287 = cljs.core.count(c__4550__auto__);
var G__87288 = (0);
seq__87015 = G__87285;
chunk__87016 = G__87286;
count__87017 = G__87287;
i__87018 = G__87288;
continue;
} else {
var js_ns = cljs.core.first(seq__87015__$1);
var require_str_87289 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_87289);


var G__87290 = cljs.core.next(seq__87015__$1);
var G__87291 = null;
var G__87292 = (0);
var G__87293 = (0);
seq__87015 = G__87290;
chunk__87016 = G__87291;
count__87017 = G__87292;
i__87018 = G__87293;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.load_sources = (function shadow$cljs$devtools$client$browser$load_sources(sources,callback){
if(cljs.core.empty_QMARK_(sources)){
var G__87021 = cljs.core.PersistentVector.EMPTY;
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(G__87021) : callback.call(null,G__87021));
} else {
var G__87023 = shadow.cljs.devtools.client.env.files_url();
var G__87024 = ((function (G__87023){
return (function (res){
var req = this;
var content = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(content) : callback.call(null,content));
});})(G__87023))
;
var G__87025 = "POST";
var G__87026 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"client","client",-1323448117),new cljs.core.Keyword(null,"browser","browser",828191719),new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources)], null)], 0));
var G__87027 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__87023,G__87024,G__87025,G__87026,G__87027);
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(p__87029){
var map__87030 = p__87029;
var map__87030__$1 = (((((!((map__87030 == null))))?(((((map__87030.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87030.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87030):map__87030);
var msg = map__87030__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87030__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87030__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var map__87032 = info;
var map__87032__$1 = (((((!((map__87032 == null))))?(((((map__87032.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87032.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87032):map__87032);
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87032__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var compiled = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87032__$1,new cljs.core.Keyword(null,"compiled","compiled",850043082));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4523__auto__ = ((function (map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__87034(s__87035){
return (new cljs.core.LazySeq(null,((function (map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info){
return (function (){
var s__87035__$1 = s__87035;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__87035__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__87044 = cljs.core.first(xs__6292__auto__);
var map__87044__$1 = (((((!((map__87044 == null))))?(((((map__87044.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87044.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87044):map__87044);
var src = map__87044__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87044__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87044__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4519__auto__ = ((function (s__87035__$1,map__87044,map__87044__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__87034_$_iter__87036(s__87037){
return (new cljs.core.LazySeq(null,((function (s__87035__$1,map__87044,map__87044__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info){
return (function (){
var s__87037__$1 = s__87037;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__87037__$1);
if(temp__5735__auto____$1){
var s__87037__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__87037__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__87037__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__87039 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__87038 = (0);
while(true){
if((i__87038 < size__4522__auto__)){
var warning = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__87038);
cljs.core.chunk_append(b__87039,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__87295 = (i__87038 + (1));
i__87038 = G__87295;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__87039),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__87034_$_iter__87036(cljs.core.chunk_rest(s__87037__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__87039),null);
}
} else {
var warning = cljs.core.first(s__87037__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__87034_$_iter__87036(cljs.core.rest(s__87037__$2)));
}
} else {
return null;
}
break;
}
});})(s__87035__$1,map__87044,map__87044__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info))
,null,null));
});})(s__87035__$1,map__87044,map__87044__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(warnings));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__87034(cljs.core.rest(s__87035__$1)));
} else {
var G__87296 = cljs.core.rest(s__87035__$1);
s__87035__$1 = G__87296;
continue;
}
} else {
var G__87297 = cljs.core.rest(s__87035__$1);
s__87035__$1 = G__87297;
continue;
}
} else {
return null;
}
break;
}
});})(map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info))
,null,null));
});})(map__87032,map__87032__$1,sources,compiled,map__87030,map__87030__$1,msg,info,reload_info))
;
return iter__4523__auto__(sources);
})()));
var seq__87050_87298 = cljs.core.seq(warnings);
var chunk__87051_87299 = null;
var count__87052_87300 = (0);
var i__87053_87301 = (0);
while(true){
if((i__87053_87301 < count__87052_87300)){
var map__87061_87302 = chunk__87051_87299.cljs$core$IIndexed$_nth$arity$2(null,i__87053_87301);
var map__87061_87303__$1 = (((((!((map__87061_87302 == null))))?(((((map__87061_87302.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87061_87302.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87061_87302):map__87061_87302);
var w_87304 = map__87061_87303__$1;
var msg_87305__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87061_87303__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_87306 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87061_87303__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_87307 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87061_87303__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_87308 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87061_87303__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_87308)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_87306),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_87307),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_87305__$1)].join(''));


var G__87309 = seq__87050_87298;
var G__87310 = chunk__87051_87299;
var G__87311 = count__87052_87300;
var G__87312 = (i__87053_87301 + (1));
seq__87050_87298 = G__87309;
chunk__87051_87299 = G__87310;
count__87052_87300 = G__87311;
i__87053_87301 = G__87312;
continue;
} else {
var temp__5735__auto___87313 = cljs.core.seq(seq__87050_87298);
if(temp__5735__auto___87313){
var seq__87050_87314__$1 = temp__5735__auto___87313;
if(cljs.core.chunked_seq_QMARK_(seq__87050_87314__$1)){
var c__4550__auto___87315 = cljs.core.chunk_first(seq__87050_87314__$1);
var G__87316 = cljs.core.chunk_rest(seq__87050_87314__$1);
var G__87317 = c__4550__auto___87315;
var G__87318 = cljs.core.count(c__4550__auto___87315);
var G__87319 = (0);
seq__87050_87298 = G__87316;
chunk__87051_87299 = G__87317;
count__87052_87300 = G__87318;
i__87053_87301 = G__87319;
continue;
} else {
var map__87063_87320 = cljs.core.first(seq__87050_87314__$1);
var map__87063_87321__$1 = (((((!((map__87063_87320 == null))))?(((((map__87063_87320.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87063_87320.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87063_87320):map__87063_87320);
var w_87322 = map__87063_87321__$1;
var msg_87323__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87063_87321__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_87324 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87063_87321__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_87325 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87063_87321__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_87326 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87063_87321__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_87326)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_87324),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_87325),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_87323__$1)].join(''));


var G__87327 = cljs.core.next(seq__87050_87314__$1);
var G__87328 = null;
var G__87329 = (0);
var G__87330 = (0);
seq__87050_87298 = G__87327;
chunk__87051_87299 = G__87328;
count__87052_87300 = G__87329;
i__87053_87301 = G__87330;
continue;
}
} else {
}
}
break;
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info){
return (function (p__87068){
var map__87069 = p__87068;
var map__87069__$1 = (((((!((map__87069 == null))))?(((((map__87069.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87069.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87069):map__87069);
var src = map__87069__$1;
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87069__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87069__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
return ((cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"always-load","always-load",66405637).cljs$core$IFn$_invoke$arity$1(reload_info),ns)) || (cljs.core.not(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src))) || (((cljs.core.contains_QMARK_(compiled,resource_id)) && (cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))))));
});})(map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info))
,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info){
return (function (p__87077){
var map__87078 = p__87077;
var map__87078__$1 = (((((!((map__87078 == null))))?(((((map__87078.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87078.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87078):map__87078);
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87078__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"never-load","never-load",1300896819).cljs$core$IFn$_invoke$arity$1(reload_info),ns);
});})(map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info))
,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info){
return (function (p__87084){
var map__87085 = p__87084;
var map__87085__$1 = (((((!((map__87085 == null))))?(((((map__87085.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87085.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87085):map__87085);
var rc = map__87085__$1;
var module = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87085__$1,new cljs.core.Keyword(null,"module","module",1424618191));
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("js",shadow.cljs.devtools.client.env.module_format)) || (shadow.cljs.devtools.client.browser.module_is_active_QMARK_(module)));
});})(map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info))
,sources))));
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.browser.load_sources(sources_to_get,((function (sources_to_get,map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info){
return (function (p1__87028_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__87028_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
});})(sources_to_get,map__87032,map__87032__$1,sources,compiled,warnings,map__87030,map__87030__$1,msg,info,reload_info))
);
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(rel_new),"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__4120__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__4120__auto__){
var and__4120__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__4120__auto____$1){
return new$;
} else {
return and__4120__auto____$1;
}
} else {
return and__4120__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_watch = (function shadow$cljs$devtools$client$browser$handle_asset_watch(p__87096){
var map__87097 = p__87096;
var map__87097__$1 = (((((!((map__87097 == null))))?(((((map__87097.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87097.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87097):map__87097);
var msg = map__87097__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87097__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__87102 = cljs.core.seq(updates);
var chunk__87104 = null;
var count__87105 = (0);
var i__87106 = (0);
while(true){
if((i__87106 < count__87105)){
var path = chunk__87104.cljs$core$IIndexed$_nth$arity$2(null,i__87106);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__87141_87331 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__87144_87332 = null;
var count__87145_87333 = (0);
var i__87146_87334 = (0);
while(true){
if((i__87146_87334 < count__87145_87333)){
var node_87335 = chunk__87144_87332.cljs$core$IIndexed$_nth$arity$2(null,i__87146_87334);
var path_match_87336 = shadow.cljs.devtools.client.browser.match_paths(node_87335.getAttribute("href"),path);
if(cljs.core.truth_(path_match_87336)){
var new_link_87337 = (function (){var G__87156 = node_87335.cloneNode(true);
G__87156.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_87336),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__87156;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_87336], 0));

goog.dom.insertSiblingAfter(new_link_87337,node_87335);

goog.dom.removeNode(node_87335);


var G__87338 = seq__87141_87331;
var G__87339 = chunk__87144_87332;
var G__87340 = count__87145_87333;
var G__87341 = (i__87146_87334 + (1));
seq__87141_87331 = G__87338;
chunk__87144_87332 = G__87339;
count__87145_87333 = G__87340;
i__87146_87334 = G__87341;
continue;
} else {
var G__87342 = seq__87141_87331;
var G__87343 = chunk__87144_87332;
var G__87344 = count__87145_87333;
var G__87345 = (i__87146_87334 + (1));
seq__87141_87331 = G__87342;
chunk__87144_87332 = G__87343;
count__87145_87333 = G__87344;
i__87146_87334 = G__87345;
continue;
}
} else {
var temp__5735__auto___87346 = cljs.core.seq(seq__87141_87331);
if(temp__5735__auto___87346){
var seq__87141_87347__$1 = temp__5735__auto___87346;
if(cljs.core.chunked_seq_QMARK_(seq__87141_87347__$1)){
var c__4550__auto___87348 = cljs.core.chunk_first(seq__87141_87347__$1);
var G__87349 = cljs.core.chunk_rest(seq__87141_87347__$1);
var G__87350 = c__4550__auto___87348;
var G__87351 = cljs.core.count(c__4550__auto___87348);
var G__87352 = (0);
seq__87141_87331 = G__87349;
chunk__87144_87332 = G__87350;
count__87145_87333 = G__87351;
i__87146_87334 = G__87352;
continue;
} else {
var node_87353 = cljs.core.first(seq__87141_87347__$1);
var path_match_87354 = shadow.cljs.devtools.client.browser.match_paths(node_87353.getAttribute("href"),path);
if(cljs.core.truth_(path_match_87354)){
var new_link_87355 = (function (){var G__87163 = node_87353.cloneNode(true);
G__87163.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_87354),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__87163;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_87354], 0));

goog.dom.insertSiblingAfter(new_link_87355,node_87353);

goog.dom.removeNode(node_87353);


var G__87356 = cljs.core.next(seq__87141_87347__$1);
var G__87357 = null;
var G__87358 = (0);
var G__87359 = (0);
seq__87141_87331 = G__87356;
chunk__87144_87332 = G__87357;
count__87145_87333 = G__87358;
i__87146_87334 = G__87359;
continue;
} else {
var G__87360 = cljs.core.next(seq__87141_87347__$1);
var G__87361 = null;
var G__87362 = (0);
var G__87363 = (0);
seq__87141_87331 = G__87360;
chunk__87144_87332 = G__87361;
count__87145_87333 = G__87362;
i__87146_87334 = G__87363;
continue;
}
}
} else {
}
}
break;
}


var G__87364 = seq__87102;
var G__87365 = chunk__87104;
var G__87366 = count__87105;
var G__87367 = (i__87106 + (1));
seq__87102 = G__87364;
chunk__87104 = G__87365;
count__87105 = G__87366;
i__87106 = G__87367;
continue;
} else {
var G__87368 = seq__87102;
var G__87369 = chunk__87104;
var G__87370 = count__87105;
var G__87371 = (i__87106 + (1));
seq__87102 = G__87368;
chunk__87104 = G__87369;
count__87105 = G__87370;
i__87106 = G__87371;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__87102);
if(temp__5735__auto__){
var seq__87102__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__87102__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__87102__$1);
var G__87372 = cljs.core.chunk_rest(seq__87102__$1);
var G__87373 = c__4550__auto__;
var G__87374 = cljs.core.count(c__4550__auto__);
var G__87375 = (0);
seq__87102 = G__87372;
chunk__87104 = G__87373;
count__87105 = G__87374;
i__87106 = G__87375;
continue;
} else {
var path = cljs.core.first(seq__87102__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__87165_87376 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__87168_87377 = null;
var count__87169_87378 = (0);
var i__87170_87379 = (0);
while(true){
if((i__87170_87379 < count__87169_87378)){
var node_87380 = chunk__87168_87377.cljs$core$IIndexed$_nth$arity$2(null,i__87170_87379);
var path_match_87381 = shadow.cljs.devtools.client.browser.match_paths(node_87380.getAttribute("href"),path);
if(cljs.core.truth_(path_match_87381)){
var new_link_87382 = (function (){var G__87179 = node_87380.cloneNode(true);
G__87179.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_87381),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__87179;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_87381], 0));

goog.dom.insertSiblingAfter(new_link_87382,node_87380);

goog.dom.removeNode(node_87380);


var G__87383 = seq__87165_87376;
var G__87384 = chunk__87168_87377;
var G__87385 = count__87169_87378;
var G__87386 = (i__87170_87379 + (1));
seq__87165_87376 = G__87383;
chunk__87168_87377 = G__87384;
count__87169_87378 = G__87385;
i__87170_87379 = G__87386;
continue;
} else {
var G__87387 = seq__87165_87376;
var G__87388 = chunk__87168_87377;
var G__87389 = count__87169_87378;
var G__87390 = (i__87170_87379 + (1));
seq__87165_87376 = G__87387;
chunk__87168_87377 = G__87388;
count__87169_87378 = G__87389;
i__87170_87379 = G__87390;
continue;
}
} else {
var temp__5735__auto___87391__$1 = cljs.core.seq(seq__87165_87376);
if(temp__5735__auto___87391__$1){
var seq__87165_87392__$1 = temp__5735__auto___87391__$1;
if(cljs.core.chunked_seq_QMARK_(seq__87165_87392__$1)){
var c__4550__auto___87393 = cljs.core.chunk_first(seq__87165_87392__$1);
var G__87394 = cljs.core.chunk_rest(seq__87165_87392__$1);
var G__87395 = c__4550__auto___87393;
var G__87396 = cljs.core.count(c__4550__auto___87393);
var G__87397 = (0);
seq__87165_87376 = G__87394;
chunk__87168_87377 = G__87395;
count__87169_87378 = G__87396;
i__87170_87379 = G__87397;
continue;
} else {
var node_87398 = cljs.core.first(seq__87165_87392__$1);
var path_match_87399 = shadow.cljs.devtools.client.browser.match_paths(node_87398.getAttribute("href"),path);
if(cljs.core.truth_(path_match_87399)){
var new_link_87400 = (function (){var G__87181 = node_87398.cloneNode(true);
G__87181.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_87399),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__87181;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_87399], 0));

goog.dom.insertSiblingAfter(new_link_87400,node_87398);

goog.dom.removeNode(node_87398);


var G__87401 = cljs.core.next(seq__87165_87392__$1);
var G__87402 = null;
var G__87403 = (0);
var G__87404 = (0);
seq__87165_87376 = G__87401;
chunk__87168_87377 = G__87402;
count__87169_87378 = G__87403;
i__87170_87379 = G__87404;
continue;
} else {
var G__87405 = cljs.core.next(seq__87165_87392__$1);
var G__87406 = null;
var G__87407 = (0);
var G__87408 = (0);
seq__87165_87376 = G__87405;
chunk__87168_87377 = G__87406;
count__87169_87378 = G__87407;
i__87170_87379 = G__87408;
continue;
}
}
} else {
}
}
break;
}


var G__87409 = cljs.core.next(seq__87102__$1);
var G__87410 = null;
var G__87411 = (0);
var G__87412 = (0);
seq__87102 = G__87409;
chunk__87104 = G__87410;
count__87105 = G__87411;
i__87106 = G__87412;
continue;
} else {
var G__87413 = cljs.core.next(seq__87102__$1);
var G__87414 = null;
var G__87415 = (0);
var G__87416 = (0);
seq__87102 = G__87413;
chunk__87104 = G__87414;
count__87105 = G__87415;
i__87106 = G__87416;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.get_ua_product = (function shadow$cljs$devtools$client$browser$get_ua_product(){
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
});
shadow.cljs.devtools.client.browser.get_asset_root = (function shadow$cljs$devtools$client$browser$get_asset_root(){
var loc = (new goog.Uri(document.location.href));
var cbp = (new goog.Uri(CLOSURE_BASE_PATH));
var s = loc.resolve(cbp).toString();
return clojure.string.replace(s,/^file:\//,"file:///");
});
shadow.cljs.devtools.client.browser.repl_error = (function shadow$cljs$devtools$client$browser$repl_error(e){
console.error("repl/invoke error",e);

return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(shadow.cljs.devtools.client.env.repl_error(e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),shadow.cljs.devtools.client.browser.get_ua_product(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"asset-root","asset-root",1771735072),shadow.cljs.devtools.client.browser.get_asset_root()], 0));
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_invoke = (function shadow$cljs$devtools$client$browser$repl_invoke(p__87190){
var map__87191 = p__87190;
var map__87191__$1 = (((((!((map__87191 == null))))?(((((map__87191.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87191.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87191):map__87191);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87191__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87191__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var result = shadow.cljs.devtools.client.env.repl_call(((function (map__87191,map__87191__$1,id,js){
return (function (){
return shadow.cljs.devtools.client.browser.global_eval(js);
});})(map__87191,map__87191__$1,id,js))
,shadow.cljs.devtools.client.browser.repl_error);
return shadow.cljs.devtools.client.browser.ws_msg(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(result,new cljs.core.Keyword(null,"id","id",-1388402092),id));
});
shadow.cljs.devtools.client.browser.repl_require = (function shadow$cljs$devtools$client$browser$repl_require(p__87194,done){
var map__87195 = p__87194;
var map__87195__$1 = (((((!((map__87195 == null))))?(((((map__87195.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87195.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87195):map__87195);
var msg = map__87195__$1;
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87195__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87195__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87195__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87195__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__87195,map__87195__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (p__87202){
var map__87203 = p__87202;
var map__87203__$1 = (((((!((map__87203 == null))))?(((((map__87203.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87203.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87203):map__87203);
var src = map__87203__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87203__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4120__auto__ = shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4120__auto__;
}
});})(map__87195,map__87195__$1,msg,id,sources,reload_namespaces,js_requires))
,sources));
return shadow.cljs.devtools.client.browser.load_sources(sources_to_load,((function (sources_to_load,map__87195,map__87195__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (sources__$1){
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-complete","repl/require-complete",-2140254719),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));
}catch (e87205){var e = e87205;
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-error","repl/require-error",1689310021),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"error","error",-978969032),e.message], null));
}finally {(done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}});})(sources_to_load,map__87195,map__87195__$1,msg,id,sources,reload_namespaces,js_requires))
);
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(p__87207,done){
var map__87208 = p__87207;
var map__87208__$1 = (((((!((map__87208 == null))))?(((((map__87208.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87208.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87208):map__87208);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87208__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87208__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
return shadow.cljs.devtools.client.browser.load_sources(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),((function (map__87208,map__87208__$1,repl_state,id){
return (function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","init-complete","repl/init-complete",-162252879),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));

shadow.cljs.devtools.client.browser.devtools_msg("REPL session start successful");

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
});})(map__87208,map__87208__$1,repl_state,id))
);
});
shadow.cljs.devtools.client.browser.repl_set_ns = (function shadow$cljs$devtools$client$browser$repl_set_ns(p__87214){
var map__87215 = p__87214;
var map__87215__$1 = (((((!((map__87215 == null))))?(((((map__87215.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87215.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87215):map__87215);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87215__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87215__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","set-ns-complete","repl/set-ns-complete",680944662),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"ns","ns",441598760),ns], null));
});
shadow.cljs.devtools.client.browser.close_reason_ref = cljs.core.volatile_BANG_(null);
shadow.cljs.devtools.client.browser.handle_message = (function shadow$cljs$devtools$client$browser$handle_message(p__87220,done){
var map__87221 = p__87220;
var map__87221__$1 = (((((!((map__87221 == null))))?(((((map__87221.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__87221.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__87221):map__87221);
var msg = map__87221__$1;
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__87221__$1,new cljs.core.Keyword(null,"type","type",1174270348));
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

var G__87224_87422 = type;
var G__87224_87423__$1 = (((G__87224_87422 instanceof cljs.core.Keyword))?G__87224_87422.fqn:null);
switch (G__87224_87423__$1) {
case "asset-watch":
shadow.cljs.devtools.client.browser.handle_asset_watch(msg);

break;
case "repl/invoke":
shadow.cljs.devtools.client.browser.repl_invoke(msg);

break;
case "repl/require":
shadow.cljs.devtools.client.browser.repl_require(msg,done);

break;
case "repl/set-ns":
shadow.cljs.devtools.client.browser.repl_set_ns(msg);

break;
case "repl/init":
shadow.cljs.devtools.client.browser.repl_init(msg,done);

break;
case "repl/session-start":
shadow.cljs.devtools.client.browser.repl_init(msg,done);

break;
case "repl/ping":
shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","pong","repl/pong",-166610159),new cljs.core.Keyword(null,"time-server","time-server",786726561),new cljs.core.Keyword(null,"time-server","time-server",786726561).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"time-runtime","time-runtime",-40294923),Date.now()], null));

break;
case "build-complete":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

shadow.cljs.devtools.client.browser.handle_build_complete(msg);

break;
case "build-failure":
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

break;
case "build-init":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

break;
case "build-start":
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

break;
case "pong":

break;
case "client/stale":
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,"Stale Client! You are not using the latest compilation output!");

break;
case "client/no-worker":
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,["watch for build \"",shadow.cljs.devtools.client.env.build_id,"\" not running"].join(''));

break;
case "custom-msg":
shadow.cljs.devtools.client.env.publish_BANG_(new cljs.core.Keyword(null,"payload","payload",-383036092).cljs$core$IFn$_invoke$arity$1(msg));

break;
default:

}

if(cljs.core.contains_QMARK_(shadow.cljs.devtools.client.env.async_ops,type)){
return null;
} else {
return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}
});
shadow.cljs.devtools.client.browser.compile = (function shadow$cljs$devtools$client$browser$compile(text,callback){
var G__87225 = ["http",((shadow.cljs.devtools.client.env.ssl)?"s":null),"://",shadow.cljs.devtools.client.env.server_host,":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.server_port),"/worker/compile/",shadow.cljs.devtools.client.env.build_id,"/",shadow.cljs.devtools.client.env.proc_id,"/browser"].join('');
var G__87226 = ((function (G__87225){
return (function (res){
var req = this;
var actions = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
if(cljs.core.truth_(callback)){
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(actions) : callback.call(null,actions));
} else {
return null;
}
});})(G__87225))
;
var G__87227 = "POST";
var G__87228 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"input","input",556931961),text], null)], 0));
var G__87229 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__87225,G__87226,G__87227,G__87228,G__87229);
});
shadow.cljs.devtools.client.browser.ws_connect = (function shadow$cljs$devtools$client$browser$ws_connect(){
try{var print_fn = cljs.core._STAR_print_fn_STAR_;
var ws_url = shadow.cljs.devtools.client.env.ws_url(new cljs.core.Keyword(null,"browser","browser",828191719));
var socket = (new WebSocket(ws_url));
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,socket);

socket.onmessage = ((function (print_fn,ws_url,socket){
return (function (e){
return shadow.cljs.devtools.client.env.process_ws_msg(e.data,shadow.cljs.devtools.client.browser.handle_message);
});})(print_fn,ws_url,socket))
;

socket.onopen = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,null);

if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("goog",shadow.cljs.devtools.client.env.module_format)){
goog.provide = goog.constructNamespace_;
} else {
}

shadow.cljs.devtools.client.env.set_print_fns_BANG_(shadow.cljs.devtools.client.browser.ws_msg);

return shadow.cljs.devtools.client.browser.devtools_msg("WebSocket connected!");
});})(print_fn,ws_url,socket))
;

socket.onclose = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.browser.devtools_msg("WebSocket disconnected!");

shadow.cljs.devtools.client.hud.connection_error((function (){var or__4131__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.close_reason_ref);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "Connection closed!";
}
})());

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);

return shadow.cljs.devtools.client.env.reset_print_fns_BANG_();
});})(print_fn,ws_url,socket))
;

return socket.onerror = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error("Connection failed!");

return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("websocket error",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
});})(print_fn,ws_url,socket))
;
}catch (e87237){var e = e87237;
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("WebSocket setup failed",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
}});
if(shadow.cljs.devtools.client.env.enabled){
var temp__5735__auto___87428 = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto___87428)){
var s_87429 = temp__5735__auto___87428;
shadow.cljs.devtools.client.browser.devtools_msg("connection reset!");

s_87429.onclose = ((function (s_87429,temp__5735__auto___87428){
return (function (e){
return null;
});})(s_87429,temp__5735__auto___87428))
;

s_87429.close();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);
} else {
}

window.addEventListener("beforeunload",(function (){
var temp__5735__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto__)){
var s = temp__5735__auto__;
return s.close();
} else {
return null;
}
}));

if(cljs.core.truth_((function (){var and__4120__auto__ = document;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("loading",document.readyState);
} else {
return and__4120__auto__;
}
})())){
window.addEventListener("DOMContentLoaded",shadow.cljs.devtools.client.browser.ws_connect);
} else {
setTimeout(shadow.cljs.devtools.client.browser.ws_connect,(10));
}
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
