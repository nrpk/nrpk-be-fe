goog.provide('nv.datum');
goog.require('cljs.core');
goog.require('clojure.set');
goog.require('cljs.pprint');
if((typeof nv !== 'undefined') && (typeof nv.datum !== 'undefined') && (typeof nv.datum.parse !== 'undefined')){
} else {
nv.datum.parse = (function (){var method_table__4613__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__4614__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var method_cache__4615__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__4616__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__4617__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),(function (){var fexpr__94057 = cljs.core.get_global_hierarchy;
return (fexpr__94057.cljs$core$IFn$_invoke$arity$0 ? fexpr__94057.cljs$core$IFn$_invoke$arity$0() : fexpr__94057.call(null));
})());
return (new cljs.core.MultiFn(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("nv.datum","parse"),((function (method_table__4613__auto__,prefer_table__4614__auto__,method_cache__4615__auto__,cached_hierarchy__4616__auto__,hierarchy__4617__auto__){
return (function (st,e){
return new cljs.core.Keyword(null,"evt","evt",-1251566867).cljs$core$IFn$_invoke$arity$1(e);
});})(method_table__4613__auto__,prefer_table__4614__auto__,method_cache__4615__auto__,cached_hierarchy__4616__auto__,hierarchy__4617__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__4617__auto__,method_table__4613__auto__,prefer_table__4614__auto__,method_cache__4615__auto__,cached_hierarchy__4616__auto__));
})();
}
nv.datum.parse.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("slot","add","slot/add",222039737),(function (st,p__94064){
var map__94066 = p__94064;
var map__94066__$1 = (((((!((map__94066 == null))))?(((((map__94066.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94066.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94066):map__94066);
var c = map__94066__$1;
var args = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94066__$1,new cljs.core.Keyword(null,"args","args",1315556576));

return cljs.core.assoc_in(st,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"place","place",-819689466),args], null),cljs.core.PersistentHashSet.EMPTY);
}));
nv.datum.parse.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("slot","set","slot/set",301074364),(function (st,p__94071){
var map__94076 = p__94071;
var map__94076__$1 = (((((!((map__94076 == null))))?(((((map__94076.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94076.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94076):map__94076);
var args = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94076__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var z = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94076__$1,new cljs.core.Keyword(null,"z","z",-789527183));
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(st,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"place","place",-819689466),args], null),cljs.core.conj,z);
}));
nv.datum.parse.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("slot","remove","slot/remove",-134445624),(function (st,p__94082){
var map__94083 = p__94082;
var map__94083__$1 = (((((!((map__94083 == null))))?(((((map__94083.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94083.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94083):map__94083);
var args = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94083__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var z = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94083__$1,new cljs.core.Keyword(null,"z","z",-789527183));
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(st,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"place","place",-819689466),args], null),cljs.core.disj,z);
}));
nv.datum.parse.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"log","log",-1595516004),(function (st,p__94085){
var map__94086 = p__94085;
var map__94086__$1 = (((((!((map__94086 == null))))?(((((map__94086.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94086.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94086):map__94086);
var args = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__94086__$1,new cljs.core.Keyword(null,"args","args",1315556576));
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(st,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"log","log",-1595516004)], null),cljs.core.conj,args);
}));
nv.datum.parse.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"end","end",-268185958),(function (st,p__94089){
var map__94090 = p__94089;
var map__94090__$1 = (((((!((map__94090 == null))))?(((((map__94090.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__94090.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__94090):map__94090);
return st;
}));
nv.datum.event = (function nv$datum$event(var_args){
var args__4736__auto__ = [];
var len__4730__auto___94128 = arguments.length;
var i__4731__auto___94129 = (0);
while(true){
if((i__4731__auto___94129 < len__4730__auto___94128)){
args__4736__auto__.push((arguments[i__4731__auto___94129]));

var G__94130 = (i__4731__auto___94129 + (1));
i__4731__auto___94129 = G__94130;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((2) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((2)),(0),null)):null);
return nv.datum.event.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4737__auto__);
});

nv.datum.event.cljs$core$IFn$_invoke$arity$variadic = (function (evt,args,p__94100){
var vec__94101 = p__94100;
var extra = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__94101,(0),null);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"evt","evt",-1251566867),evt,new cljs.core.Keyword(null,"args","args",1315556576),args,new cljs.core.Keyword(null,"z","z",-789527183),extra], null);
});

nv.datum.event.cljs$lang$maxFixedArity = (2);

/** @this {Function} */
nv.datum.event.cljs$lang$applyTo = (function (seq94092){
var G__94093 = cljs.core.first(seq94092);
var seq94092__$1 = cljs.core.next(seq94092);
var G__94094 = cljs.core.first(seq94092__$1);
var seq94092__$2 = cljs.core.next(seq94092__$1);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__94093,G__94094,seq94092__$2);
});

nv.datum.log = (function nv$datum$log(var_args){
var args__4736__auto__ = [];
var len__4730__auto___94133 = arguments.length;
var i__4731__auto___94134 = (0);
while(true){
if((i__4731__auto___94134 < len__4730__auto___94133)){
args__4736__auto__.push((arguments[i__4731__auto___94134]));

var G__94135 = (i__4731__auto___94134 + (1));
i__4731__auto___94134 = G__94135;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return nv.datum.log.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

nv.datum.log.cljs$core$IFn$_invoke$arity$variadic = (function (e){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"evt","evt",-1251566867),new cljs.core.Keyword(null,"log","log",-1595516004),new cljs.core.Keyword(null,"args","args",1315556576),e], null);
});

nv.datum.log.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
nv.datum.log.cljs$lang$applyTo = (function (seq94108){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq94108));
});

nv.datum.inc_epoch = (function nv$datum$inc_epoch(st){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(st,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"epoch","epoch",1435633666)], null),cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.inc,(0)));
});
nv.datum.get_epoch = (function nv$datum$get_epoch(st){
return new cljs.core.Keyword(null,"epoch","epoch",1435633666).cljs$core$IFn$_invoke$arity$1(st);
});
nv.datum.p = (function nv$datum$p(var_args){
var args__4736__auto__ = [];
var len__4730__auto___94137 = arguments.length;
var i__4731__auto___94138 = (0);
while(true){
if((i__4731__auto___94138 < len__4730__auto___94137)){
args__4736__auto__.push((arguments[i__4731__auto___94138]));

var G__94139 = (i__4731__auto___94138 + (1));
i__4731__auto___94138 = G__94139;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return nv.datum.p.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

nv.datum.p.cljs$core$IFn$_invoke$arity$variadic = (function (st,r){
var st__$1 = nv.datum.inc_epoch(st);
var evt = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(nv.datum.event,r),new cljs.core.Keyword(null,"epoch","epoch",1435633666),nv.datum.get_epoch(st__$1));
var log = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(nv.datum.log,r);
var G__94118 = (nv.datum.parse.cljs$core$IFn$_invoke$arity$2 ? nv.datum.parse.cljs$core$IFn$_invoke$arity$2(st__$1,log) : nv.datum.parse.call(null,st__$1,log));
var G__94119 = evt;
return (nv.datum.parse.cljs$core$IFn$_invoke$arity$2 ? nv.datum.parse.cljs$core$IFn$_invoke$arity$2(G__94118,G__94119) : nv.datum.parse.call(null,G__94118,G__94119));
});

nv.datum.p.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
nv.datum.p.cljs$lang$applyTo = (function (seq94114){
var G__94115 = cljs.core.first(seq94114);
var seq94114__$1 = cljs.core.next(seq94114);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__94115,seq94114__$1);
});

var state_94143 = nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(nv.datum.p.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"log","log",-1595516004),cljs.core.PersistentVector.EMPTY], null),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","add","slot/add",222039737),(1)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","add","slot/add",222039737),(2)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","add","slot/add",222039737),(3)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","add","slot/add",222039737),(4)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","set","slot/set",301074364),(1),new cljs.core.Symbol(null,"a","a",-482876059,null)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","set","slot/set",301074364),(1),new cljs.core.Symbol(null,"b","b",-1172211299,null)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","set","slot/set",301074364),(2),new cljs.core.Symbol(null,"c","c",-122660552,null)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","set","slot/set",301074364),(2),new cljs.core.Symbol(null,"d","d",-682293345,null)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("slot","set","slot/set",301074364),(3),new cljs.core.Symbol(null,"a","a",-482876059,null)], 0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),(123)], 0));
cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1(state_94143);

cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(clojure.set.union,cljs.core.keys(clojure.set.map_invert(new cljs.core.Keyword(null,"place","place",-819689466).cljs$core$IFn$_invoke$arity$1(state_94143)))));

//# sourceMappingURL=nv.datum.js.map
