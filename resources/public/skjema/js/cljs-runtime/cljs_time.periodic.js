goog.provide('cljs_time.periodic');
goog.require('cljs.core');
goog.require('cljs_time.core');
goog.require('cljs_time.internal.core');
/**
 * Returns a sequence of date-time values growing over specific period.
 *   The 2 argument function takes as input the starting value and the growing value,
 *   returning a lazy infinite sequence.
 *   The 3 argument function takes as input the starting value, the upper bound value,
 *   and the growing value, return a lazy sequence.
 */
cljs_time.periodic.periodic_seq = (function cljs_time$periodic$periodic_seq(var_args){
var G__93719 = arguments.length;
switch (G__93719) {
case 2:
return cljs_time.periodic.periodic_seq.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs_time.periodic.periodic_seq.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs_time.periodic.periodic_seq.cljs$core$IFn$_invoke$arity$2 = (function (start,period_like){
var period = cljs_time.core.__GT_period(period_like);
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (period){
return (function (i){
return cljs_time.core.plus.cljs$core$IFn$_invoke$arity$2(start,cljs_time.internal.core.multiplied_by(period,i));
});})(period))
,cljs.core.iterate(cljs.core.inc,(0)));
});

cljs_time.periodic.periodic_seq.cljs$core$IFn$_invoke$arity$3 = (function (start,end,period_like){
var period = cljs_time.core.__GT_period(period_like);
return cljs.core.take_while.cljs$core$IFn$_invoke$arity$2(((function (period){
return (function (next){
return cljs_time.core.before_QMARK_(next,end);
});})(period))
,cljs_time.periodic.periodic_seq.cljs$core$IFn$_invoke$arity$2(start,period_like));
});

cljs_time.periodic.periodic_seq.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=cljs_time.periodic.js.map
