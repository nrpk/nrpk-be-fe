goog.provide('instaparse.failure');
goog.require('cljs.core');
goog.require('instaparse.print');
/**
 * Takes an index into text, and determines the line and column info
 */
instaparse.failure.index__GT_line_column = (function instaparse$failure$index__GT_line_column(index,text){
var line = (1);
var col = (1);
var counter = (0);
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(index,counter)){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"line","line",212345235),line,new cljs.core.Keyword(null,"column","column",2078222095),col], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("\n",cljs.core.get.cljs$core$IFn$_invoke$arity$2(text,counter))){
var G__92420 = (line + (1));
var G__92421 = (1);
var G__92422 = (counter + (1));
line = G__92420;
col = G__92421;
counter = G__92422;
continue;
} else {
var G__92423 = line;
var G__92424 = (col + (1));
var G__92425 = (counter + (1));
line = G__92423;
col = G__92424;
counter = G__92425;
continue;

}
}
break;
}
});
instaparse.failure.get_line = (function instaparse$failure$get_line(n,text){
var chars = cljs.core.seq(clojure.string.replace(text,"\r\n","\n"));
var n__$1 = n;
while(true){
if(cljs.core.empty_QMARK_(chars)){
return "";
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(n__$1,(1))){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.take_while.cljs$core$IFn$_invoke$arity$2(cljs.core.complement(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["\n",null], null), null)),chars));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("\n",cljs.core.first(chars))){
var G__92429 = cljs.core.next(chars);
var G__92430 = (n__$1 - (1));
chars = G__92429;
n__$1 = G__92430;
continue;
} else {
var G__92432 = cljs.core.next(chars);
var G__92433 = n__$1;
chars = G__92432;
n__$1 = G__92433;
continue;

}
}
}
break;
}
});
/**
 * Creates string with caret at nth position, 1-based
 */
instaparse.failure.marker = (function instaparse$failure$marker(n){
if(cljs.core.integer_QMARK_(n)){
if((n <= (1))){
return "^";
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((n - (1))," "),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, ["^"], null)));
}
} else {
return null;
}
});
/**
 * Adds text, line, and column info to failure object.
 */
instaparse.failure.augment_failure = (function instaparse$failure$augment_failure(failure,text){
var lc = instaparse.failure.index__GT_line_column(new cljs.core.Keyword(null,"index","index",-1531685915).cljs$core$IFn$_invoke$arity$1(failure),text);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([failure,lc,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text","text",-1790561697),instaparse.failure.get_line(new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(lc),text)], null)], 0));
});
/**
 * Provides special case for printing negative lookahead reasons
 */
instaparse.failure.print_reason = (function instaparse$failure$print_reason(r){
if(cljs.core.truth_(new cljs.core.Keyword(null,"NOT","NOT",-1689245341).cljs$core$IFn$_invoke$arity$1(r))){
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["NOT "], 0));

return cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"NOT","NOT",-1689245341).cljs$core$IFn$_invoke$arity$1(r)], 0));
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"char-range","char-range",1443391389).cljs$core$IFn$_invoke$arity$1(r))){
return cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([instaparse.print.char_range__GT_str(r)], 0));
} else {
if((r instanceof RegExp)){
return cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([instaparse.print.regexp__GT_str(r)], 0));
} else {
return cljs.core.pr.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([r], 0));

}
}
}
});
/**
 * Takes an augmented failure object and prints the error message
 */
instaparse.failure.pprint_failure = (function instaparse$failure$pprint_failure(p__92401){
var map__92402 = p__92401;
var map__92402__$1 = (((((!((map__92402 == null))))?(((((map__92402.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__92402.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__92402):map__92402);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__92402__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__92402__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var text = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__92402__$1,new cljs.core.Keyword(null,"text","text",-1790561697));
var reason = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__92402__$1,new cljs.core.Keyword(null,"reason","reason",-2070751759));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["Parse error at line ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line),", column ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column),":"].join('')], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([text], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([instaparse.failure.marker(column)], 0));

var full_reasons = cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"expecting","expecting",-57706705),cljs.core.filter.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"full","full",436801220),reason)));
var partial_reasons = cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"expecting","expecting",-57706705),cljs.core.filter.cljs$core$IFn$_invoke$arity$2(cljs.core.complement(new cljs.core.Keyword(null,"full","full",436801220)),reason)));
var total = (cljs.core.count(full_reasons) + cljs.core.count(partial_reasons));
if((total === (0))){
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),total)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Expected:"], 0));
} else {
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Expected one of:"], 0));

}
}

var seq__92404_92436 = cljs.core.seq(full_reasons);
var chunk__92405_92437 = null;
var count__92406_92438 = (0);
var i__92407_92439 = (0);
while(true){
if((i__92407_92439 < count__92406_92438)){
var r_92441 = chunk__92405_92437.cljs$core$IIndexed$_nth$arity$2(null,i__92407_92439);
instaparse.failure.print_reason(r_92441);

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" (followed by end-of-string)"], 0));


var G__92442 = seq__92404_92436;
var G__92443 = chunk__92405_92437;
var G__92444 = count__92406_92438;
var G__92445 = (i__92407_92439 + (1));
seq__92404_92436 = G__92442;
chunk__92405_92437 = G__92443;
count__92406_92438 = G__92444;
i__92407_92439 = G__92445;
continue;
} else {
var temp__5735__auto___92446 = cljs.core.seq(seq__92404_92436);
if(temp__5735__auto___92446){
var seq__92404_92447__$1 = temp__5735__auto___92446;
if(cljs.core.chunked_seq_QMARK_(seq__92404_92447__$1)){
var c__4550__auto___92448 = cljs.core.chunk_first(seq__92404_92447__$1);
var G__92449 = cljs.core.chunk_rest(seq__92404_92447__$1);
var G__92450 = c__4550__auto___92448;
var G__92451 = cljs.core.count(c__4550__auto___92448);
var G__92452 = (0);
seq__92404_92436 = G__92449;
chunk__92405_92437 = G__92450;
count__92406_92438 = G__92451;
i__92407_92439 = G__92452;
continue;
} else {
var r_92453 = cljs.core.first(seq__92404_92447__$1);
instaparse.failure.print_reason(r_92453);

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" (followed by end-of-string)"], 0));


var G__92455 = cljs.core.next(seq__92404_92447__$1);
var G__92456 = null;
var G__92457 = (0);
var G__92458 = (0);
seq__92404_92436 = G__92455;
chunk__92405_92437 = G__92456;
count__92406_92438 = G__92457;
i__92407_92439 = G__92458;
continue;
}
} else {
}
}
break;
}

var seq__92408 = cljs.core.seq(partial_reasons);
var chunk__92409 = null;
var count__92410 = (0);
var i__92411 = (0);
while(true){
if((i__92411 < count__92410)){
var r = chunk__92409.cljs$core$IIndexed$_nth$arity$2(null,i__92411);
instaparse.failure.print_reason(r);

cljs.core.println();


var G__92465 = seq__92408;
var G__92466 = chunk__92409;
var G__92467 = count__92410;
var G__92468 = (i__92411 + (1));
seq__92408 = G__92465;
chunk__92409 = G__92466;
count__92410 = G__92467;
i__92411 = G__92468;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__92408);
if(temp__5735__auto__){
var seq__92408__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__92408__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__92408__$1);
var G__92474 = cljs.core.chunk_rest(seq__92408__$1);
var G__92475 = c__4550__auto__;
var G__92476 = cljs.core.count(c__4550__auto__);
var G__92477 = (0);
seq__92408 = G__92474;
chunk__92409 = G__92475;
count__92410 = G__92476;
i__92411 = G__92477;
continue;
} else {
var r = cljs.core.first(seq__92408__$1);
instaparse.failure.print_reason(r);

cljs.core.println();


var G__92480 = cljs.core.next(seq__92408__$1);
var G__92481 = null;
var G__92482 = (0);
var G__92483 = (0);
seq__92408 = G__92480;
chunk__92409 = G__92481;
count__92410 = G__92482;
i__92411 = G__92483;
continue;
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=instaparse.failure.js.map
