goog.provide('website.editors');
goog.require('cljs.core');
var module$node_modules$codemirror$mode$clojure$clojure=shadow.js.require("module$node_modules$codemirror$mode$clojure$clojure", {});
var module$node_modules$codemirror$mode$gfm$gfm=shadow.js.require("module$node_modules$codemirror$mode$gfm$gfm", {});
var module$node_modules$codemirror$lib$codemirror=shadow.js.require("module$node_modules$codemirror$lib$codemirror", {});
var module$node_modules$codemirror$addon$edit$matchbrackets=shadow.js.require("module$node_modules$codemirror$addon$edit$matchbrackets", {});
var module$node_modules$codemirror$addon$lint$lint=shadow.js.require("module$node_modules$codemirror$addon$lint$lint", {});
var module$node_modules$codemirror$addon$runmode$runmode=shadow.js.require("module$node_modules$codemirror$addon$runmode$runmode", {});
var module$node_modules$parinfer$parinfer=shadow.js.require("module$node_modules$parinfer$parinfer", {});
var module$node_modules$parinfer_codemirror$parinfer_codemirror=shadow.js.require("module$node_modules$parinfer_codemirror$parinfer_codemirror", {});
goog.require('sci.core');
goog.require('website.button');
goog.require('reagent.core');
goog.require('re_frame.core');
goog.require('website.toolbar');
goog.require('markdown.core');
goog.require('website.pure.utils');
goog.require('cljs.core.async');
website.editors.debounce = (function website$editors$debounce(in$,ms){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var c__80824__auto___89619 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___89619,out){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___89619,out){
return (function (state_89506){
var state_val_89507 = (state_89506[(1)]);
if((state_val_89507 === (7))){
var inst_89462 = (state_89506[(2)]);
var state_89506__$1 = state_89506;
var statearr_89508_89620 = state_89506__$1;
(statearr_89508_89620[(2)] = inst_89462);

(statearr_89508_89620[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (20))){
var state_89506__$1 = state_89506;
var statearr_89509_89621 = state_89506__$1;
(statearr_89509_89621[(2)] = null);

(statearr_89509_89621[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (1))){
var inst_89453 = null;
var state_89506__$1 = (function (){var statearr_89510 = state_89506;
(statearr_89510[(7)] = inst_89453);

return statearr_89510;
})();
var statearr_89511_89622 = state_89506__$1;
(statearr_89511_89622[(2)] = null);

(statearr_89511_89622[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (4))){
var state_89506__$1 = state_89506;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_89506__$1,(7),in$);
} else {
if((state_val_89507 === (15))){
var inst_89485 = (state_89506[(2)]);
var inst_89453 = null;
var state_89506__$1 = (function (){var statearr_89514 = state_89506;
(statearr_89514[(8)] = inst_89485);

(statearr_89514[(7)] = inst_89453);

return statearr_89514;
})();
var statearr_89515_89623 = state_89506__$1;
(statearr_89515_89623[(2)] = null);

(statearr_89515_89623[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (21))){
var inst_89494 = (state_89506[(2)]);
var state_89506__$1 = state_89506;
var statearr_89518_89624 = state_89506__$1;
(statearr_89518_89624[(2)] = inst_89494);

(statearr_89518_89624[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (13))){
var state_89506__$1 = state_89506;
var statearr_89519_89625 = state_89506__$1;
(statearr_89519_89625[(2)] = null);

(statearr_89519_89625[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (6))){
var inst_89466 = (state_89506[(9)]);
var inst_89465 = (state_89506[(2)]);
var inst_89466__$1 = cljs.core.async.timeout(ms);
var inst_89467 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_89468 = [in$,inst_89466__$1];
var inst_89469 = (new cljs.core.PersistentVector(null,2,(5),inst_89467,inst_89468,null));
var state_89506__$1 = (function (){var statearr_89520 = state_89506;
(statearr_89520[(9)] = inst_89466__$1);

(statearr_89520[(10)] = inst_89465);

return statearr_89520;
})();
return cljs.core.async.ioc_alts_BANG_(state_89506__$1,(8),inst_89469);
} else {
if((state_val_89507 === (17))){
var inst_89473 = (state_89506[(11)]);
var inst_89496 = ["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_89473)].join('');
var inst_89497 = (new Error(inst_89496));
var inst_89498 = (function(){throw inst_89497})();
var state_89506__$1 = state_89506;
var statearr_89521_89626 = state_89506__$1;
(statearr_89521_89626[(2)] = inst_89498);

(statearr_89521_89626[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (3))){
var inst_89504 = (state_89506[(2)]);
var state_89506__$1 = state_89506;
return cljs.core.async.impl.ioc_helpers.return_chan(state_89506__$1,inst_89504);
} else {
if((state_val_89507 === (12))){
var inst_89480 = (state_89506[(2)]);
var state_89506__$1 = state_89506;
if(cljs.core.truth_(inst_89480)){
var statearr_89522_89627 = state_89506__$1;
(statearr_89522_89627[(1)] = (13));

} else {
var statearr_89523_89628 = state_89506__$1;
(statearr_89523_89628[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (2))){
var inst_89453 = (state_89506[(7)]);
var inst_89459 = (inst_89453 == null);
var state_89506__$1 = state_89506;
if(cljs.core.truth_(inst_89459)){
var statearr_89524_89629 = state_89506__$1;
(statearr_89524_89629[(1)] = (4));

} else {
var statearr_89525_89630 = state_89506__$1;
(statearr_89525_89630[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (19))){
var inst_89472 = (state_89506[(12)]);
var inst_89453 = inst_89472;
var state_89506__$1 = (function (){var statearr_89526 = state_89506;
(statearr_89526[(7)] = inst_89453);

return statearr_89526;
})();
var statearr_89527_89631 = state_89506__$1;
(statearr_89527_89631[(2)] = null);

(statearr_89527_89631[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (11))){
var inst_89502 = (state_89506[(2)]);
var state_89506__$1 = state_89506;
var statearr_89529_89632 = state_89506__$1;
(statearr_89529_89632[(2)] = inst_89502);

(statearr_89529_89632[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (9))){
var inst_89465 = (state_89506[(10)]);
var state_89506__$1 = state_89506;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_89506__$1,(12),out,inst_89465);
} else {
if((state_val_89507 === (5))){
var inst_89453 = (state_89506[(7)]);
var state_89506__$1 = state_89506;
var statearr_89531_89633 = state_89506__$1;
(statearr_89531_89633[(2)] = inst_89453);

(statearr_89531_89633[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (14))){
var inst_89483 = cljs.core.async.close_BANG_(in$);
var state_89506__$1 = state_89506;
var statearr_89532_89634 = state_89506__$1;
(statearr_89532_89634[(2)] = inst_89483);

(statearr_89532_89634[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (16))){
var inst_89472 = (state_89506[(12)]);
var state_89506__$1 = state_89506;
if(cljs.core.truth_(inst_89472)){
var statearr_89534_89635 = state_89506__$1;
(statearr_89534_89635[(1)] = (19));

} else {
var statearr_89535_89636 = state_89506__$1;
(statearr_89535_89636[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (10))){
var inst_89473 = (state_89506[(11)]);
var inst_89488 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(in$,inst_89473);
var state_89506__$1 = state_89506;
if(inst_89488){
var statearr_89536_89637 = state_89506__$1;
(statearr_89536_89637[(1)] = (16));

} else {
var statearr_89537_89638 = state_89506__$1;
(statearr_89537_89638[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (18))){
var inst_89500 = (state_89506[(2)]);
var state_89506__$1 = state_89506;
var statearr_89538_89640 = state_89506__$1;
(statearr_89538_89640[(2)] = inst_89500);

(statearr_89538_89640[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89507 === (8))){
var inst_89466 = (state_89506[(9)]);
var inst_89473 = (state_89506[(11)]);
var inst_89471 = (state_89506[(2)]);
var inst_89472 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_89471,(0),null);
var inst_89473__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_89471,(1),null);
var inst_89477 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_89466,inst_89473__$1);
var state_89506__$1 = (function (){var statearr_89540 = state_89506;
(statearr_89540[(12)] = inst_89472);

(statearr_89540[(11)] = inst_89473__$1);

return statearr_89540;
})();
if(inst_89477){
var statearr_89541_89644 = state_89506__$1;
(statearr_89541_89644[(1)] = (9));

} else {
var statearr_89542_89645 = state_89506__$1;
(statearr_89542_89645[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__80824__auto___89619,out))
;
return ((function (switch__80630__auto__,c__80824__auto___89619,out){
return (function() {
var website$editors$debounce_$_state_machine__80631__auto__ = null;
var website$editors$debounce_$_state_machine__80631__auto____0 = (function (){
var statearr_89543 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_89543[(0)] = website$editors$debounce_$_state_machine__80631__auto__);

(statearr_89543[(1)] = (1));

return statearr_89543;
});
var website$editors$debounce_$_state_machine__80631__auto____1 = (function (state_89506){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_89506);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e89544){if((e89544 instanceof Object)){
var ex__80634__auto__ = e89544;
var statearr_89545_89646 = state_89506;
(statearr_89545_89646[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_89506);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e89544;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__89647 = state_89506;
state_89506 = G__89647;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
website$editors$debounce_$_state_machine__80631__auto__ = function(state_89506){
switch(arguments.length){
case 0:
return website$editors$debounce_$_state_machine__80631__auto____0.call(this);
case 1:
return website$editors$debounce_$_state_machine__80631__auto____1.call(this,state_89506);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
website$editors$debounce_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = website$editors$debounce_$_state_machine__80631__auto____0;
website$editors$debounce_$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = website$editors$debounce_$_state_machine__80631__auto____1;
return website$editors$debounce_$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___89619,out))
})();
var state__80826__auto__ = (function (){var statearr_89548 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_89548[(6)] = c__80824__auto___89619);

return statearr_89548;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___89619,out))
);


return out;
});
/**
 * testings
 */
website.editors.jalla = (function website$editors$jalla(x){
return cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (p1__89549_SHARP_){
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"p.text-white","p.text-white",661910817),p1__89549_SHARP_],null));
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(x));
});
if((typeof website !== 'undefined') && (typeof website.editors !== 'undefined') && (typeof website.editors.lineWidget !== 'undefined')){
} else {
website.editors.lineWidget = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
website.editors.editor_settings = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"gutters","gutters",688671428),new cljs.core.Keyword(null,"matchBrackets","matchBrackets",1256448936),new cljs.core.Keyword(null,"highlightFormatting","highlightFormatting",1641607404),new cljs.core.Keyword(null,"autocorrect","autocorrect",531427757),new cljs.core.Keyword(null,"taskLists","taskLists",-2072488622),new cljs.core.Keyword(null,"spellcheck","spellcheck",-508643726),new cljs.core.Keyword(null,"lint","lint",-1212848237),new cljs.core.Keyword(null,"firstLineNumber","firstLineNumber",-327461640),new cljs.core.Keyword(null,"lineNumbers","lineNumbers",1374890941)],[new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, ["CodeMirror-lint-markers"], null),true,true,true,true,true,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"lintOnChange","lintOnChange",-1260558899),true], null),(1),true]);
website.editors.editor_SINGLEQUOTE_ = (function website$editors$editor_SINGLEQUOTE_(p__89553){
var map__89554 = p__89553;
var map__89554__$1 = (((((!((map__89554 == null))))?(((((map__89554.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__89554.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__89554):map__89554);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89554__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var parsing_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__89554__$1,new cljs.core.Keyword(null,"parsing-fn","parsing-fn",-1063705507),((function (map__89554,map__89554__$1,id){
return (function (p1__89552_SHARP_){
return cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["DEFAULT EVAL ",p1__89552_SHARP_], 0));
});})(map__89554,map__89554__$1,id))
);
var default_value = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89554__$1,new cljs.core.Keyword(null,"default-value","default-value",232220170));
var mode = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89554__$1,new cljs.core.Keyword(null,"mode","mode",654403691));
var theme = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__89554__$1,new cljs.core.Keyword(null,"theme","theme",-1247880880),"default");
var line_wrapping = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__89554__$1,new cljs.core.Keyword(null,"line-wrapping","line-wrapping",919045521),false);
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var out = website.editors.debounce(in$,(1500));
var ln_QMARK_ = (function (){var G__89556 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","ln?","website.toolbar/ln?",-546067471),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__89556) : re_frame.core.subscribe.call(null,G__89556));
})();
var theme__$1 = (function (){var G__89561 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","theme","website.toolbar/theme",1990527592),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__89561) : re_frame.core.subscribe.call(null,G__89561));
})();
var local_editor_ref = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var c__80824__auto___89648 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___89648,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___89648,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function (state_89570){
var state_val_89571 = (state_89570[(1)]);
if((state_val_89571 === (1))){
var state_89570__$1 = state_89570;
var statearr_89576_89649 = state_89570__$1;
(statearr_89576_89649[(2)] = null);

(statearr_89576_89649[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_89571 === (2))){
var state_89570__$1 = state_89570;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_89570__$1,(4),out);
} else {
if((state_val_89571 === (3))){
var inst_89568 = (state_89570[(2)]);
var state_89570__$1 = state_89570;
return cljs.core.async.impl.ioc_helpers.return_chan(state_89570__$1,inst_89568);
} else {
if((state_val_89571 === (4))){
var inst_89564 = (state_89570[(2)]);
var inst_89565 = (parsing_fn.cljs$core$IFn$_invoke$arity$1 ? parsing_fn.cljs$core$IFn$_invoke$arity$1(local_editor_ref) : parsing_fn.call(null,local_editor_ref));
var state_89570__$1 = (function (){var statearr_89580 = state_89570;
(statearr_89580[(7)] = inst_89565);

(statearr_89580[(8)] = inst_89564);

return statearr_89580;
})();
var statearr_89581_89650 = state_89570__$1;
(statearr_89581_89650[(2)] = null);

(statearr_89581_89650[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
});})(c__80824__auto___89648,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
;
return ((function (switch__80630__auto__,c__80824__auto___89648,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function() {
var website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto__ = null;
var website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto____0 = (function (){
var statearr_89585 = [null,null,null,null,null,null,null,null,null];
(statearr_89585[(0)] = website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto__);

(statearr_89585[(1)] = (1));

return statearr_89585;
});
var website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto____1 = (function (state_89570){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_89570);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e89586){if((e89586 instanceof Object)){
var ex__80634__auto__ = e89586;
var statearr_89587_89651 = state_89570;
(statearr_89587_89651[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_89570);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e89586;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__89653 = state_89570;
state_89570 = G__89653;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto__ = function(state_89570){
switch(arguments.length){
case 0:
return website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto____0.call(this);
case 1:
return website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto____1.call(this,state_89570);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto____0;
website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto____1;
return website$editors$editor_SINGLEQUOTE__$_state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___89648,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
})();
var state__80826__auto__ = (function (){var statearr_89591 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_89591[(6)] = c__80824__auto___89648);

return statearr_89591;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___89648,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
);


return reagent.core.create_class(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"display-name","display-name",694513143),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(id),"-editor"].join(''),new cljs.core.Keyword(null,"reagent-render","reagent-render",-985383853),((function (in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function (m){
var temp__5735__auto___89657 = cljs.core.deref(local_editor_ref);
if(cljs.core.truth_(temp__5735__auto___89657)){
var cm_89658 = temp__5735__auto___89657;
cm_89658.setOption("lineNumbers",cljs.core.deref(ln_QMARK_));

cm_89658.setOption("theme",cljs.core.nth.cljs$core$IFn$_invoke$arity$3(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["oceanic-next","neo"], null),cljs.core.deref(theme__$1),"shadowfox"));
} else {
}

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"default-value","default-value",232220170),default_value,new cljs.core.Keyword(null,"auto-complete","auto-complete",244958848),"off"], null)], null);
});})(in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
,new cljs.core.Keyword(null,"component-did-mount","component-did-mount",-1126910518),((function (in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function (this$){
var opts = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([website.editors.editor_settings,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"mode","mode",654403691),mode], null)], 0));
var cm_SINGLEQUOTE_ = module$node_modules$codemirror$lib$codemirror.fromTextArea(reagent.core.dom_node(this$),cljs.core.clj__GT_js(opts));
cm_SINGLEQUOTE_.on("change",((function (opts,cm_SINGLEQUOTE_,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function (instance,changeObj){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(in$,cm_SINGLEQUOTE_);
});})(opts,cm_SINGLEQUOTE_,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
);

module$node_modules$parinfer_codemirror$parinfer_codemirror.init(cm_SINGLEQUOTE_);

cm_SINGLEQUOTE_.removeKeyMap();

cm_SINGLEQUOTE_.setOption("extraKeys",({"Cmd-Enter": ((function (opts,cm_SINGLEQUOTE_,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function (){
return (parsing_fn.cljs$core$IFn$_invoke$arity$1 ? parsing_fn.cljs$core$IFn$_invoke$arity$1(local_editor_ref) : parsing_fn.call(null,local_editor_ref));
});})(opts,cm_SINGLEQUOTE_,in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
}));

cm_SINGLEQUOTE_.setOption("theme","mbo");

cm_SINGLEQUOTE_.setOption("lineWrapping",line_wrapping);

cljs.core.reset_BANG_(local_editor_ref,cm_SINGLEQUOTE_);

return (parsing_fn.cljs$core$IFn$_invoke$arity$1 ? parsing_fn.cljs$core$IFn$_invoke$arity$1(local_editor_ref) : parsing_fn.call(null,local_editor_ref));
});})(in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
,new cljs.core.Keyword(null,"component-will-unmount","component-will-unmount",-2058314698),((function (in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping){
return (function (){
var cm = cljs.core.deref(local_editor_ref);
return cm.toTextArea(cm);
});})(in$,out,ln_QMARK_,theme__$1,local_editor_ref,map__89554,map__89554__$1,id,parsing_fn,default_value,mode,theme,line_wrapping))
], null));
});
website.editors.compile_md_BANG_ = (function website$editors$compile_md_BANG_(editor_ref){
var data = markdown.core.md_to_html_string_STAR_(cljs.core.deref(editor_ref).getValue(),cljs.core.PersistentArrayMap.EMPTY);
var G__89596 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","set-html-parsed","website.toolbar/set-html-parsed",-1629656998),new cljs.core.Keyword(null,"markdown","markdown",1227225089),data], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__89596) : re_frame.core.dispatch.call(null,G__89596));
});
website.editors.compile_sci_BANG_ = (function website$editors$compile_sci_BANG_(editor_ref){
if(cljs.core.truth_(cljs.core.deref(website.editors.lineWidget))){
cljs.core.deref(website.editors.lineWidget).clear();
} else {
}

try{var res = sci.core.eval_string.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(editor_ref).getValue(),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"bindings","bindings",1271397192),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Symbol(null,"jalla","jalla",-77063192,null),website.editors.jalla,new cljs.core.Symbol(null,"prn","prn",1561684909,null),cljs.core.prn,new cljs.core.Symbol(null,"println","println",-733595439,null),cljs.core.println], null),new cljs.core.Keyword(null,"realize-max","realize-max",-1846442543),(10000)], null));
var res_string = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([res], 0));
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["> ",res,res_string], 0));

var G__89607_89668 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.editors","some-result","website.editors/some-result",911079240),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"result","result",1415092211),res], null)], null);
(re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1(G__89607_89668) : re_frame.core.dispatch_sync.call(null,G__89607_89668));

return module$node_modules$codemirror$lib$codemirror.runMode(res_string,"clojure",document.getElementById("result"));
}catch (e89597){if((e89597 instanceof cljs.core.ExceptionInfo)){
var e = e89597;
var map__89599 = cljs.core.ex_data(e);
var map__89599__$1 = (((((!((map__89599 == null))))?(((((map__89599.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__89599.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__89599):map__89599);
var row = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89599__$1,new cljs.core.Keyword(null,"row","row",-570139521));
if(cljs.core.truth_(row)){
var msg = (e["message"]);
var msg_node = document.createElement("div");
var icon_node = msg_node.appendChild(document.createElement("span"));
var _ = icon_node.innerHTML = "!";
var ___$1 = icon_node.className = "lint-error-icon";
var ___$2 = msg_node.appendChild(document.createTextNode(msg));
var ___$3 = msg_node.className = "lint-error";
var lw = cljs.core.deref(editor_ref).addLineWidget((row - (1)),msg_node,({"coverGutter": false, "above": false}));
if(cljs.core.truth_(website.editors.lineWidget)){
cljs.core.deref(website.editors.lineWidget).clear();
} else {
}

if(cljs.core.truth_(lw)){
cljs.core.reset_BANG_(website.editors.lineWidget,lw);
} else {
}

var G__89603 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.editors","some-result","website.editors/some-result",911079240),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),msg], null)], null);
return (re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch_sync.cljs$core$IFn$_invoke$arity$1(G__89603) : re_frame.core.dispatch_sync.call(null,G__89603));
} else {
return module$node_modules$codemirror$lib$codemirror.runMode(["ERROR: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1((e["message"]))].join(''),"clojure",document.getElementById("result"));
}
} else {
throw e89597;

}
}});
website.editors.grid = (function website$editors$grid(var_args){
var args__4736__auto__ = [];
var len__4730__auto___89677 = arguments.length;
var i__4731__auto___89679 = (0);
while(true){
if((i__4731__auto___89679 < len__4730__auto___89677)){
args__4736__auto__.push((arguments[i__4731__auto___89679]));

var G__89680 = (i__4731__auto___89679 + (1));
i__4731__auto___89679 = G__89680;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return website.editors.grid.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

website.editors.grid.cljs$core$IFn$_invoke$arity$variadic = (function (xs){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.p-px","div.p-px",-907764705),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"2px","2px",-1697779893),new cljs.core.Keyword(null,"grid-template-columns","grid-template-columns",-594112133),"minmax(88px,1fr) 1fr 2fr",new cljs.core.Keyword(null,"grid-auto-rows","grid-auto-rows",-113194069),"32px"], null)], null),xs], null);
});

website.editors.grid.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
website.editors.grid.cljs$lang$applyTo = (function (seq89608){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq89608));
});

website.editors.editor_code = (function website$editors$editor_code(p__89609){
var map__89610 = p__89609;
var map__89610__$1 = (((((!((map__89610 == null))))?(((((map__89610.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__89610.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__89610):map__89610);
var m = map__89610__$1;
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89610__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var controls_fn = website.toolbar.controls;
var preview = (function (){var G__89612 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","preview-toggle","website.toolbar/preview-toggle",1847212737),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__89612) : re_frame.core.subscribe.call(null,G__89612));
})();
var parsed_html = (function (){var G__89613 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","html-parsed","website.toolbar/html-parsed",1292747553),new cljs.core.Keyword(null,"markdown","markdown",1227225089)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__89613) : re_frame.core.subscribe.call(null,G__89613));
})();
return ((function (controls_fn,preview,parsed_html,map__89610,map__89610__$1,m,id){
return (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-blue-900","div.bg-blue-900",608296466),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.p-1","div.flex.p-1",-394841003),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Extract",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.Keyword(null,"default","default",-1987822328),new cljs.core.Keyword(null,"_sub-caption","_sub-caption",-1984610424),"cmd-enter",new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),website.editors.compile_sci_BANG_], null)),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-white","div.text-white",1926076215),id], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [controls_fn,id], null),(cljs.core.truth_(cljs.core.deref(preview))?website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$3(new cljs.core.Keyword(null,"section.art1","section.art1",-410262286),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"width","width",-384071477),"100%"], null)], null),new cljs.core.Keyword(null,"html","html",-998796897).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(parsed_html))):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.editors.editor_SINGLEQUOTE_,m], null))], null);
});
;})(controls_fn,preview,parsed_html,map__89610,map__89610__$1,m,id))
});
website.editors.editor_markdown = (function website$editors$editor_markdown(p__89614){
var map__89615 = p__89614;
var map__89615__$1 = (((((!((map__89615 == null))))?(((((map__89615.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__89615.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__89615):map__89615);
var m = map__89615__$1;
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89615__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var controls_fn = website.toolbar.markdown_controls;
var preview = (function (){var G__89617 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","preview-toggle","website.toolbar/preview-toggle",1847212737),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__89617) : re_frame.core.subscribe.call(null,G__89617));
})();
var parsed_html = (function (){var G__89618 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","html-parsed","website.toolbar/html-parsed",1292747553),new cljs.core.Keyword(null,"markdown","markdown",1227225089)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__89618) : re_frame.core.subscribe.call(null,G__89618));
})();
var m__$1 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([m,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"mode","mode",654403691),"gfm",new cljs.core.Keyword(null,"line-wrapping","line-wrapping",919045521),true,new cljs.core.Keyword(null,"parsing-fn","parsing-fn",-1063705507),website.editors.compile_md_BANG_], null)], 0));
return ((function (controls_fn,preview,parsed_html,m__$1,map__89615,map__89615__$1,m,id){
return (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-gray-100.text-xs.sticky.z-40.shadow-4","div.bg-gray-100.text-xs.sticky.z-40.shadow-4",783045116),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"top","top",-1856271961),"3rem"], null)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.editors.grid,website.button.button(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Publiser",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.Keyword(null,"hollow","hollow",-1445647344),new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),((function (controls_fn,preview,parsed_html,m__$1,map__89615,map__89615__$1,m,id){
return (function (){
return cljs.core.List.EMPTY;
});})(controls_fn,preview,parsed_html,m__$1,map__89615,map__89615__$1,m,id))
], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Tid",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.Keyword(null,"hollow","hollow",-1445647344),new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),((function (controls_fn,preview,parsed_html,m__$1,map__89615,map__89615__$1,m,id){
return (function (){
return cljs.core.List.EMPTY;
});})(controls_fn,preview,parsed_html,m__$1,map__89615,map__89615__$1,m,id))
], null)),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-gray-900.px-1","div.text-gray-900.px-1",1729174832),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"justify-self","justify-self",-2135975605),new cljs.core.Keyword(null,"end","end",-268185958),new cljs.core.Keyword(null,"align-self","align-self",1475936794),new cljs.core.Keyword(null,"center","center",-748944368)], null)], null),website.pure.utils.linked("Sample ",id)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [controls_fn,id], null),(cljs.core.truth_(cljs.core.deref(preview))?website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$3(new cljs.core.Keyword(null,"section.art1","section.art1",-410262286),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"width","width",-384071477),"100%"], null)], null),new cljs.core.Keyword(null,"html","html",-998796897).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(parsed_html))):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [website.editors.editor_SINGLEQUOTE_,m__$1], null))], null);
});
;})(controls_fn,preview,parsed_html,m__$1,map__89615,map__89615__$1,m,id))
});

//# sourceMappingURL=website.editors.js.map
