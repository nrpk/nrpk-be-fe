goog.provide('website.pages.kurs');
goog.require('cljs.core');
goog.require('markdown.core');
goog.require('website.pure.utils');
website.pages.kurs.data = (function website$pages$kurs$data(n){
var G__90850 = n;
switch (G__90850) {
case (10):
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"Kurs ved NRPK",new cljs.core.Keyword(null,"content","content",15833224),"# Kurs ved NRPK\nKurstrinnene i Padleforbundets v\u00E5ttkortstige for havpadling er:\n\n- Grunnkurs hav\n- Teknikkurs hav\n- Aktivitetslederkurs hav\n\n\n>  Se [her](www.padling.no) for en detaljert beskrivelse.\n\nNRPK holder hver sommer grunnkurs i havpadling til en gunstig pris for medlemmene. Dette er et to-dagers kurs og annonseres p\u00E5 hjemmesiden.\n\n\nKlubben kan subsidiere teknikkurs for v\u00E5re medlemmer og i enkelte tilfeller aktivitetslederkurs. Se [her](www) for mere detaljer"], null);

break;
case (11):
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"Kano",new cljs.core.Keyword(null,"content","content",15833224),"For kano er trinnene p\u00E5 v\u00E5ttkortstigen:\n\n- Grunnkurs kano\n- Sikkerhetskurs kano\n- Teknikkurs kano\n- Aktivitetslederkurs kano\n\n\n>  Se [her](www.padling.no) for en detaljert beskrivelse.\n\nNRPK setter opp Grunnkurs kano hver sommer p\u00E5 N\u00F8klevann. Dette er et todagers kurs, og annonseres p\u00E5 hjemmesiden. Klubben kan subsidiere sikkerhetskurs, teknikkurs og i enkelte tilfeller aktivitetslederkurs for v\u00E5re medlemmer \u2013 se [her](www) for mere informasjon"], null);

break;
case (2):
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"content","content",15833224),"NRPK har forel\u00F8pig ikke planlagt noe formelt kurs innen roing i \u00E5r, men det vil bli gitt instruksjon i roing, hovedsakelig p\u00E5 enkelte onsdager i l\u00F8pet av sommeren.\n\n## Suvi rob\u00E5ter\nSuvi-b\u00E5tene er solide, stabile rob\u00E5ter med plass til flere. Har man ikke rodd f\u00F8r, er disse gode \u00E5 begynne med. Det kreves ikke noen spesiell instruksjon for \u00E5 bruke disse, men sp\u00F8r gjerne en av n\u00F8kkelvaktene om hjelp til sj\u00F8setting og igjen n\u00E5r b\u00E5ten skal tas opp av vannet og sveives opp p\u00E5 trallen sin p\u00E5 bryggen.\n\n## Scullere\nF\u00F8r du kan l\u00E5ne v\u00E5re scullere m\u00E5 du f\u00E5 informasjon og veiledning, dersom du ikke har erfaring fra tilsvarende b\u00E5ter. Dette b\u00E5de for \u00E5 unng\u00E5 farlige situasjoner (f.eks. velt i kaldt vann) og for \u00E5 hindre at utstyr blir skadet. V\u00E5r Ro-oppmann Jens Christian kan hjelpe deg. Kontaktes p\u00E5 [epost](jcriis@gmail.com)"], null);

break;
case (3):
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"SUP (Stand Up Padling)",new cljs.core.Keyword(null,"content","content",15833224),"# SUP (Stand Up Padling)\n\u00D8nsker du ogs\u00E5 \u00E5 padle SUP p\u00E5 N\u00F8klevann? Meld deg p\u00E5 kurs som etterhvert blir annonserert.\n\nKurset holdes p\u00E5 N\u00F8klevann og varer rundt 3 timer og m\u00E5let er at alle skal blir trygge p\u00E5 brettet, l\u00E6re padleteknikk, av og p\u00E5stigning, sikkerhetsrutiner og at du f\u00E5r anledning til \u00E5 pr\u00F8ve ut de ulike brettene vi har.\n\nDu kan begynne p\u00E5 de mest stabile, brede brettene. Kanskje du f\u00E5r lyst til \u00E5 pr\u00F8ve sprint-brettet allerede p\u00E5 kurset!\n\nTid: Kanskje en onsdag i juni, kl.18.30-21.30, pris: kr. 100,-\n\nP\u00E5melding: Benytt NRPKs hjemmeside n\u00E5r vi har satt opp en dato."], null);

break;
case (0):
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"Livredningskurs",new cljs.core.Keyword(null,"content","content",15833224),"# Livredningskurs\nNRPK arrangerer hvert \u00E5r livredningskurs med instrukt\u00F8r fra Norges Livredningsselskap. Kurset varer rundt 2-3 timer og holdes p\u00E5 Holmlia bad hvor klubben ogs\u00E5 har sine bassengtreninger med kajakk.\n\nKurset gir et kompetansebevis i livredning som er gyldig ut neste kalender\u00E5r. Kursdeltagere forplikter seg til \u00E5 v\u00E6re tilstede som bassengvakt to ganger (dette er en forutsetning for at klubben kan leie basseng av Oslo kommune.)\n\nKurset inkluderer redningsteknikker i basseng og grunnleggende hjerte- lungeredning og krever\n\n- 200 meter sv\u00F8mming,\n- derav 100 meter rygg,\n- og \u00E5 \u00F8ve p\u00E5 \u00E5 dykke etter dukke.\n\n\nAvslutningspr\u00F8ve best\u00E5r i \u00E5 hente opp en dukke p\u00E5 bunnen og sv\u00F8mme med en person en bassenglengde.\n\nKurset er \u00E5pent for alle medlemmer i klubben **men n\u00F8kkelvakter vil bli prioritert** hvis det blir fulltegnet. Vi oppfordrer n\u00F8kkelvaktene til \u00E5 ta kurset. Kurset er gratis."], null);

break;
case (1):
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"Subsidiering",new cljs.core.Keyword(null,"content","content",15833224),"# Subsidiering\n\nNRPK dekker hele kursavgiften n\u00E5r medlemmer tar et eller flere av f\u00F8lgende kurs:\n\n- F\u00F8rstehjelpskurs (hjerte-/lungeredning)\n- Livredningskurs\n- Aktivitetslederkurs\n\nDen som f\u00E5r dekket aktivitetslederkurs vil m\u00E5tte kunne holde grunnkurs som gjenytelse til klubben. Deltagelse p\u00E5 slike kurs skal forh\u00E5ndsgodkjennes av styret.\n\nNRPK arrangerer ogs\u00E5 kurs etter ettersp\u00F8rsel der det gis rabatt for medlemmer:\n\n- Grunnkurs kano\n- Sikkerhetskurs kano\n- Grunnkurs roing\n- Grunnkurs hav\n- Grunnkurs flattvann\n- Teknikk-kurs hav\n- Teknikk-kurs flattvann\n- SUP-kurs\n\nFor medlemmer som tar eksterne v\u00E5ttkortkurs som ikke tilbys av NRPK, for eksempel grunnkurs elv, kan NRPK eventuelt dekke en del av kursavgiften.  Dette m\u00E5 forh\u00E5ndsgodkjennes av styret og kurstilskudd blir i s\u00E5 tilfelle utbetalt etterskuddsvis mot oversendelse av kvittering."], null);

break;
default:
return "";

}
});
website.pages.kurs.render = (function website$pages$kurs$render(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.w-full.scroll-x.snap-x.h-full.center","div.w-full.scroll-x.snap-x.h-full.center",-781312313),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.w-full.xh-full.flex","div.w-full.xh-full.flex",-333218413),(function (){var iter__4523__auto__ = (function website$pages$kurs$render_$_iter__90852(s__90853){
return (new cljs.core.LazySeq(null,(function (){
var s__90853__$1 = s__90853;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__90853__$1);
if(temp__5735__auto__){
var s__90853__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__90853__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__90853__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__90855 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__90854 = (0);
while(true){
if((i__90854 < size__4522__auto__)){
var e = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__90854);
cljs.core.chunk_append(b__90855,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-full.bg-gray-800.snap-center.mx-2.rounded-lg.p-2.flex-col","div.h-full.bg-gray-800.snap-center.mx-2.rounded-lg.p-2.flex-col",-1923545362),website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$3(new cljs.core.Keyword(null,"section.w-128.text-base.text-gray-400.art1.overflow-hidden","section.w-128.text-base.text-gray-400.art1.overflow-hidden",-434130331),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"html","html",-998796897).cljs$core$IFn$_invoke$arity$1(markdown.core.md_to_html_string_STAR_(new cljs.core.Keyword(null,"content","content",15833224).cljs$core$IFn$_invoke$arity$1(website.pages.kurs.data(e)),cljs.core.PersistentArrayMap.EMPTY)))], null));

var G__90869 = (i__90854 + (1));
i__90854 = G__90869;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__90855),website$pages$kurs$render_$_iter__90852(cljs.core.chunk_rest(s__90853__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__90855),null);
}
} else {
var e = cljs.core.first(s__90853__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-full.bg-gray-800.snap-center.mx-2.rounded-lg.p-2.flex-col","div.h-full.bg-gray-800.snap-center.mx-2.rounded-lg.p-2.flex-col",-1923545362),website.pure.utils.dangerous.cljs$core$IFn$_invoke$arity$3(new cljs.core.Keyword(null,"section.w-128.text-base.text-gray-400.art1.overflow-hidden","section.w-128.text-base.text-gray-400.art1.overflow-hidden",-434130331),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"html","html",-998796897).cljs$core$IFn$_invoke$arity$1(markdown.core.md_to_html_string_STAR_(new cljs.core.Keyword(null,"content","content",15833224).cljs$core$IFn$_invoke$arity$1(website.pages.kurs.data(e)),cljs.core.PersistentArrayMap.EMPTY)))], null),website$pages$kurs$render_$_iter__90852(cljs.core.rest(s__90853__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2((0),(10)));
})()], null)], null);
});

//# sourceMappingURL=website.pages.kurs.js.map
