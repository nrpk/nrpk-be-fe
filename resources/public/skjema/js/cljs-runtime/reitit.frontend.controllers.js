goog.provide('reitit.frontend.controllers');
goog.require('cljs.core');
reitit.frontend.controllers.pad_same_length = (function reitit$frontend$controllers$pad_same_length(a,b){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(a,cljs.core.take.cljs$core$IFn$_invoke$arity$2((cljs.core.count(b) - cljs.core.count(a)),cljs.core.repeat.cljs$core$IFn$_invoke$arity$1(null)));
});
reitit.frontend.controllers.params_warning = (new cljs.core.Delay((function (){
return console.warn("Reitit-frontend controller :params is deprecated. Replace with :identity or :parameters option.");
}),null));
/**
 * Get controller identity given controller and match.
 * 
 *   To select interesting properties from Match :parameters option can be set.
 *   Value should be param-type => [param-key]
 *   Resulting value is map of param-type => param-key => value.
 * 
 *   For other uses, :identity option can be used to provide function from
 *   Match to identity.
 * 
 *   Default value is nil, i.e. controller identity doesn't depend on Match.
 */
reitit.frontend.controllers.get_identity = (function reitit$frontend$controllers$get_identity(p__89088,match){
var map__89089 = p__89088;
var map__89089__$1 = (((((!((map__89089 == null))))?(((((map__89089.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__89089.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__89089):map__89089);
var identity = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89089__$1,new cljs.core.Keyword(null,"identity","identity",1647396035));
var parameters = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89089__$1,new cljs.core.Keyword(null,"parameters","parameters",-1229919748));
var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__89089__$1,new cljs.core.Keyword(null,"params","params",710516235));
if(cljs.core.not((function (){var and__4120__auto__ = identity;
if(cljs.core.truth_(and__4120__auto__)){
return parameters;
} else {
return and__4120__auto__;
}
})())){
} else {
throw (new Error(["Assert failed: ","Use either :identity or :parameters for controller, not both.","\n","(not (and identity parameters))"].join('')));
}

if(cljs.core.truth_(params)){
cljs.core.deref(reitit.frontend.controllers.params_warning);
} else {
}

if(cljs.core.truth_(parameters)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4523__auto__ = ((function (map__89089,map__89089__$1,identity,parameters,params){
return (function reitit$frontend$controllers$get_identity_$_iter__89094(s__89095){
return (new cljs.core.LazySeq(null,((function (map__89089,map__89089__$1,identity,parameters,params){
return (function (){
var s__89095__$1 = s__89095;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__89095__$1);
if(temp__5735__auto__){
var s__89095__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__89095__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__89095__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__89097 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__89096 = (0);
while(true){
if((i__89096 < size__4522__auto__)){
var vec__89109 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__89096);
var param_type = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89109,(0),null);
var ks = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89109,(1),null);
cljs.core.chunk_append(b__89097,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [param_type,cljs.core.select_keys(cljs.core.get.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"parameters","parameters",-1229919748).cljs$core$IFn$_invoke$arity$1(match),param_type),ks)], null));

var G__89148 = (i__89096 + (1));
i__89096 = G__89148;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__89097),reitit$frontend$controllers$get_identity_$_iter__89094(cljs.core.chunk_rest(s__89095__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__89097),null);
}
} else {
var vec__89112 = cljs.core.first(s__89095__$2);
var param_type = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89112,(0),null);
var ks = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__89112,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [param_type,cljs.core.select_keys(cljs.core.get.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"parameters","parameters",-1229919748).cljs$core$IFn$_invoke$arity$1(match),param_type),ks)], null),reitit$frontend$controllers$get_identity_$_iter__89094(cljs.core.rest(s__89095__$2)));
}
} else {
return null;
}
break;
}
});})(map__89089,map__89089__$1,identity,parameters,params))
,null,null));
});})(map__89089,map__89089__$1,identity,parameters,params))
;
return iter__4523__auto__(parameters);
})());
} else {
if(cljs.core.truth_(identity)){
return (identity.cljs$core$IFn$_invoke$arity$1 ? identity.cljs$core$IFn$_invoke$arity$1(match) : identity.call(null,match));
} else {
if(cljs.core.truth_(params)){
return (params.cljs$core$IFn$_invoke$arity$1 ? params.cljs$core$IFn$_invoke$arity$1(match) : params.call(null,match));
} else {
return null;

}
}
}
});
/**
 * Run side-effects (:start or :stop) for controller.
 *   The side-effect function is called with controller identity value.
 */
reitit.frontend.controllers.apply_controller = (function reitit$frontend$controllers$apply_controller(controller,method){
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(controller,method);
if(cljs.core.truth_(temp__5735__auto__)){
var f = temp__5735__auto__;
var G__89118 = new cljs.core.Keyword("reitit.frontend.controllers","identity","reitit.frontend.controllers/identity",-806277693).cljs$core$IFn$_invoke$arity$1(controller);
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__89118) : f.call(null,G__89118));
} else {
return null;
}
});
/**
 * Applies changes between current controllers and
 *   those previously enabled. Reinitializes controllers whose
 *   identity has changed.
 */
reitit.frontend.controllers.apply_controllers = (function reitit$frontend$controllers$apply_controllers(old_controllers,new_match){
var new_controllers = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (controller){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(controller,new cljs.core.Keyword("reitit.frontend.controllers","identity","reitit.frontend.controllers/identity",-806277693),reitit.frontend.controllers.get_identity(controller,new_match));
}),new cljs.core.Keyword(null,"controllers","controllers",-1120410624).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(new_match)));
var changed_controllers = cljs.core.vec(cljs.core.keep.cljs$core$IFn$_invoke$arity$2(cljs.core.identity,cljs.core.map.cljs$core$IFn$_invoke$arity$3(((function (new_controllers){
return (function (old,new$){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(old,new$)){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"old","old",-1825222690),old,new cljs.core.Keyword(null,"new","new",-2085437848),new$], null);
} else {
return null;
}
});})(new_controllers))
,reitit.frontend.controllers.pad_same_length(old_controllers,new_controllers),reitit.frontend.controllers.pad_same_length(new_controllers,old_controllers))));
var seq__89120_89168 = cljs.core.seq(cljs.core.reverse(cljs.core.map.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"old","old",-1825222690),changed_controllers)));
var chunk__89121_89169 = null;
var count__89122_89170 = (0);
var i__89123_89171 = (0);
while(true){
if((i__89123_89171 < count__89122_89170)){
var controller_89172 = chunk__89121_89169.cljs$core$IIndexed$_nth$arity$2(null,i__89123_89171);
reitit.frontend.controllers.apply_controller(controller_89172,new cljs.core.Keyword(null,"stop","stop",-2140911342));


var G__89173 = seq__89120_89168;
var G__89174 = chunk__89121_89169;
var G__89175 = count__89122_89170;
var G__89176 = (i__89123_89171 + (1));
seq__89120_89168 = G__89173;
chunk__89121_89169 = G__89174;
count__89122_89170 = G__89175;
i__89123_89171 = G__89176;
continue;
} else {
var temp__5735__auto___89177 = cljs.core.seq(seq__89120_89168);
if(temp__5735__auto___89177){
var seq__89120_89178__$1 = temp__5735__auto___89177;
if(cljs.core.chunked_seq_QMARK_(seq__89120_89178__$1)){
var c__4550__auto___89182 = cljs.core.chunk_first(seq__89120_89178__$1);
var G__89183 = cljs.core.chunk_rest(seq__89120_89178__$1);
var G__89184 = c__4550__auto___89182;
var G__89185 = cljs.core.count(c__4550__auto___89182);
var G__89186 = (0);
seq__89120_89168 = G__89183;
chunk__89121_89169 = G__89184;
count__89122_89170 = G__89185;
i__89123_89171 = G__89186;
continue;
} else {
var controller_89187 = cljs.core.first(seq__89120_89178__$1);
reitit.frontend.controllers.apply_controller(controller_89187,new cljs.core.Keyword(null,"stop","stop",-2140911342));


var G__89188 = cljs.core.next(seq__89120_89178__$1);
var G__89189 = null;
var G__89190 = (0);
var G__89191 = (0);
seq__89120_89168 = G__89188;
chunk__89121_89169 = G__89189;
count__89122_89170 = G__89190;
i__89123_89171 = G__89191;
continue;
}
} else {
}
}
break;
}

var seq__89132_89192 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"new","new",-2085437848),changed_controllers));
var chunk__89133_89193 = null;
var count__89134_89194 = (0);
var i__89135_89195 = (0);
while(true){
if((i__89135_89195 < count__89134_89194)){
var controller_89196 = chunk__89133_89193.cljs$core$IIndexed$_nth$arity$2(null,i__89135_89195);
reitit.frontend.controllers.apply_controller(controller_89196,new cljs.core.Keyword(null,"start","start",-355208981));


var G__89197 = seq__89132_89192;
var G__89198 = chunk__89133_89193;
var G__89199 = count__89134_89194;
var G__89200 = (i__89135_89195 + (1));
seq__89132_89192 = G__89197;
chunk__89133_89193 = G__89198;
count__89134_89194 = G__89199;
i__89135_89195 = G__89200;
continue;
} else {
var temp__5735__auto___89201 = cljs.core.seq(seq__89132_89192);
if(temp__5735__auto___89201){
var seq__89132_89202__$1 = temp__5735__auto___89201;
if(cljs.core.chunked_seq_QMARK_(seq__89132_89202__$1)){
var c__4550__auto___89203 = cljs.core.chunk_first(seq__89132_89202__$1);
var G__89204 = cljs.core.chunk_rest(seq__89132_89202__$1);
var G__89205 = c__4550__auto___89203;
var G__89206 = cljs.core.count(c__4550__auto___89203);
var G__89207 = (0);
seq__89132_89192 = G__89204;
chunk__89133_89193 = G__89205;
count__89134_89194 = G__89206;
i__89135_89195 = G__89207;
continue;
} else {
var controller_89208 = cljs.core.first(seq__89132_89202__$1);
reitit.frontend.controllers.apply_controller(controller_89208,new cljs.core.Keyword(null,"start","start",-355208981));


var G__89209 = cljs.core.next(seq__89132_89202__$1);
var G__89210 = null;
var G__89211 = (0);
var G__89212 = (0);
seq__89132_89192 = G__89209;
chunk__89133_89193 = G__89210;
count__89134_89194 = G__89211;
i__89135_89195 = G__89212;
continue;
}
} else {
}
}
break;
}

return new_controllers;
});

//# sourceMappingURL=reitit.frontend.controllers.js.map
