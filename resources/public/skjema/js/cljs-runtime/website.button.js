goog.provide('website.button');
goog.require('cljs.core');
goog.require('re_frame.core');
website.button.button = (function website$button$button(p__86908){
var map__86909 = p__86908;
var map__86909__$1 = (((((!((map__86909 == null))))?(((((map__86909.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__86909.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__86909):map__86909);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86909__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var base_style = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__86909__$1,new cljs.core.Keyword(null,"base-style","base-style",1985179158),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"rounded-sm","rounded-sm",595100194),new cljs.core.Keyword(null,"font-bold","font-bold",272258030),new cljs.core.Keyword(null,"text-xs","text-xs",1250326458),new cljs.core.Keyword(null,"leading-none","leading-none",-325338682),new cljs.core.Keyword(null,"_px-2","_px-2",-405635966)], null));
var caption = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86909__$1,new cljs.core.Keyword(null,"caption","caption",-855383902));
var click_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__86909__$1,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548));
var style = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__86909__$1,new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.Keyword(null,"hollow","hollow",-1445647344));
var style__$1 = ((cljs.core.fn_QMARK_(style))?(style.cljs$core$IFn$_invoke$arity$0 ? style.cljs$core$IFn$_invoke$arity$0() : style.call(null)):style);
var click_fn__$1 = (((click_fn instanceof cljs.core.Keyword))?((function (style__$1,map__86909,map__86909__$1,id,base_style,caption,click_fn,style){
return (function (){
var G__86915 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [click_fn,id], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__86915) : re_frame.core.dispatch.call(null,G__86915));
});})(style__$1,map__86909,map__86909__$1,id,base_style,caption,click_fn,style))
:click_fn);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),cljs.core.flatten(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [base_style,"focus:outline-none focus:select-none",(function (){var pred__86916 = cljs.core._EQ_;
var expr__86917 = style__$1;
if(cljs.core.truth_((function (){var G__86919 = new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
var G__86920 = expr__86917;
return (pred__86916.cljs$core$IFn$_invoke$arity$2 ? pred__86916.cljs$core$IFn$_invoke$arity$2(G__86919,G__86920) : pred__86916.call(null,G__86919,G__86920));
})())){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bg-clear","bg-clear",-673074101),new cljs.core.Keyword(null,"border-gray-700","border-gray-700",465742906),new cljs.core.Keyword(null,"border-2","border-2",-1138091451),new cljs.core.Keyword(null,"text-black","text-black",-1990796629)], null);
} else {
if(cljs.core.truth_((function (){var G__86925 = new cljs.core.Keyword(null,"hollow-2","hollow-2",1625458912);
var G__86926 = expr__86917;
return (pred__86916.cljs$core$IFn$_invoke$arity$2 ? pred__86916.cljs$core$IFn$_invoke$arity$2(G__86925,G__86926) : pred__86916.call(null,G__86925,G__86926));
})())){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bg-green-800","bg-green-800",934765793),new cljs.core.Keyword(null,"border-green-700","border-green-700",-1308279736),new cljs.core.Keyword(null,"border-2","border-2",-1138091451),new cljs.core.Keyword(null,"text-white","text-white",-1261600697)], null);
} else {
if(cljs.core.truth_((function (){var G__86927 = new cljs.core.Keyword(null,"slight","slight",-1057194628);
var G__86928 = expr__86917;
return (pred__86916.cljs$core$IFn$_invoke$arity$2 ? pred__86916.cljs$core$IFn$_invoke$arity$2(G__86927,G__86928) : pred__86916.call(null,G__86927,G__86928));
})())){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bg-orange-600","bg-orange-600",159956151),new cljs.core.Keyword(null,"text-white","text-white",-1261600697)], null);
} else {
if(cljs.core.truth_((function (){var G__86929 = new cljs.core.Keyword(null,"danger","danger",-624338030);
var G__86930 = expr__86917;
return (pred__86916.cljs$core$IFn$_invoke$arity$2 ? pred__86916.cljs$core$IFn$_invoke$arity$2(G__86929,G__86930) : pred__86916.call(null,G__86929,G__86930));
})())){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bg-red-600","bg-red-600",884143063)], null);
} else {
if(cljs.core.truth_((function (){var G__86931 = new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
var G__86932 = expr__86917;
return (pred__86916.cljs$core$IFn$_invoke$arity$2 ? pred__86916.cljs$core$IFn$_invoke$arity$2(G__86931,G__86932) : pred__86916.call(null,G__86931,G__86932));
})())){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bg-gray-600","bg-gray-600",-1204897057),new cljs.core.Keyword(null,"text-gray-100","text-gray-100",1074950007)], null);
} else {
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"bg-blue-600","bg-blue-600",-827042293)], null);
}
}
}
}
}
})()], null)),new cljs.core.Keyword(null,"on-click","on-click",1632826543),click_fn__$1], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),caption], null)], null);
});

//# sourceMappingURL=website.button.js.map
