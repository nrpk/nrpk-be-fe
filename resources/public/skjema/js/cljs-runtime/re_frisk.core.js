goog.provide('re_frisk.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.ratom');
goog.require('re_frame.core');
goog.require('re_frame.db');
goog.require('re_frame.subs');
goog.require('re_frisk.data');
goog.require('re_frisk.devtool');
goog.require('re_frisk.diff');
re_frisk.core.app_db_diff = (function re_frisk$core$app_db_diff(){
return re_frisk.diff.diff(cljs.core.deref(re_frisk.data.app_db_prev_event),cljs.core.deref(new cljs.core.Keyword(null,"app-db","app-db",865606302).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.re_frame_data))));
});
re_frisk.core.post_event_callback = (function re_frisk$core$post_event_callback(value){
var cntx = cljs.core.get.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"contexts","contexts",4351546).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.deb_data)),cljs.core.first(value));
var indx = cljs.core.count(cljs.core.deref(re_frisk.data.re_frame_events));
var diff = re_frisk.core.app_db_diff();
cljs.core.reset_BANG_(re_frisk.data.app_db_prev_event,cljs.core.deref(new cljs.core.Keyword(null,"app-db","app-db",865606302).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.re_frame_data))));

return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frisk.data.re_frame_events,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic((function (){var or__4131__auto__ = cntx;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
})(),new cljs.core.Keyword(null,"event","event",301435442),value,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"app-db-diff","app-db-diff",709588713),diff,new cljs.core.Keyword(null,"indx","indx",1571035590),indx], 0)));
});
re_frisk.core.re_frame_sub = (function re_frisk$core$re_frame_sub(var_args){
var args__4736__auto__ = [];
var len__4730__auto___83843 = arguments.length;
var i__4731__auto___83844 = (0);
while(true){
if((i__4731__auto___83844 < len__4730__auto___83843)){
args__4736__auto__.push((arguments[i__4731__auto___83844]));

var G__83845 = (i__4731__auto___83844 + (1));
i__4731__auto___83844 = G__83845;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return re_frisk.core.re_frame_sub.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

re_frisk.core.re_frame_sub.cljs$core$IFn$_invoke$arity$variadic = (function (rest){
return cljs.core.reset_BANG_(new cljs.core.Keyword(null,"id-handler","id-handler",1013135509).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.re_frame_data)),cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__83776_SHARP_){
return cljs.core.PersistentHashMap.fromArrays([cljs.core.first(p1__83776_SHARP_)],[cljs.core.deref(cljs.core.second(p1__83776_SHARP_))]);
}),cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__83777_SHARP_){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.first(cljs.core.ffirst(p1__83777_SHARP_)),new cljs.core.Keyword("re-frisk.core","db","re-frisk.core/db",447694709))) || ((cljs.core.second(p1__83777_SHARP_) == null)));
}),cljs.core.deref(re_frame.subs.query__GT_reaction)))));
});

re_frisk.core.re_frame_sub.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
re_frisk.core.re_frame_sub.cljs$lang$applyTo = (function (seq83778){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq83778));
});

re_frisk.core.render_re_frisk = (function re_frisk$core$render_re_frisk(params){
var div = document.createElement("div");
document.body.appendChild(div);

onbeforeunload = ((function (div){
return (function (){
if(cljs.core.truth_(new cljs.core.Keyword(null,"win","win",-1624642689).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.deb_data)))){
return new cljs.core.Keyword(null,"win","win",-1624642689).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.deb_data)).alert("Application has been closed or refreshed. Debugger has been stopped!");
} else {
return null;
}
});})(div))
;

return reagent.core.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_frisk.devtool.re_frisk_shell,params], null),div);
});
re_frisk.core.enable_re_frisk_BANG_ = (function re_frisk$core$enable_re_frisk_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___83852 = arguments.length;
var i__4731__auto___83853 = (0);
while(true){
if((i__4731__auto___83853 < len__4730__auto___83852)){
args__4736__auto__.push((arguments[i__4731__auto___83853]));

var G__83854 = (i__4731__auto___83853 + (1));
i__4731__auto___83853 = G__83854;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return re_frisk.core.enable_re_frisk_BANG_.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

re_frisk.core.enable_re_frisk_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__83797){
var vec__83798 = p__83797;
var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__83798,(0),null);
if(cljs.core.truth_(cljs.core.deref(re_frisk.data.initialized))){
return null;
} else {
cljs.core.reset_BANG_(re_frisk.data.initialized,true);

if(cljs.core.truth_(re_frame.core.reg_sub)){
var G__83801_83856 = new cljs.core.Keyword("re-frisk.core","db","re-frisk.core/db",447694709);
var G__83802_83857 = ((function (G__83801_83856,vec__83798,opts){
return (function (db,_){
return db;
});})(G__83801_83856,vec__83798,opts))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__83801_83856,G__83802_83857) : re_frame.core.reg_sub.call(null,G__83801_83856,G__83802_83857));
} else {
re_frame.core.register_sub.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("re-frisk.core","db","re-frisk.core/db",447694709),((function (vec__83798,opts){
return (function (db,_){
return reagent.ratom.make_reaction(((function (vec__83798,opts){
return (function (){
return cljs.core.deref(db);
});})(vec__83798,opts))
);
});})(vec__83798,opts))
], 0));
}

cljs.core.reset_BANG_(re_frisk.data.re_frame_data,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"app-db","app-db",865606302),(function (){var G__83806 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("re-frisk.core","db","re-frisk.core/db",447694709)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__83806) : re_frame.core.subscribe.call(null,G__83806));
})(),new cljs.core.Keyword(null,"id-handler","id-handler",1013135509),reagent.core.atom.cljs$core$IFn$_invoke$arity$1("not connected")], null));

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.deb_data,cljs.core.assoc,new cljs.core.Keyword(null,"prefs","prefs",-1818938470),opts);

if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"events?","events?",-780512682).cljs$core$IFn$_invoke$arity$1(opts),false)){
} else {
re_frame.core.add_post_event_callback.cljs$core$IFn$_invoke$arity$1(re_frisk.core.post_event_callback);
}

if(cljs.core.truth_(re_frame.subs.query__GT_reaction)){
cljs.core.add_watch(re_frame.db.app_db,new cljs.core.Keyword(null,"re-frisk-watcher","re-frisk-watcher",1126824275),re_frisk.core.re_frame_sub);

re_frisk.core.re_frame_sub();
} else {
}

return setTimeout(re_frisk.core.render_re_frisk,(100),opts);
}
});

re_frisk.core.enable_re_frisk_BANG_.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
re_frisk.core.enable_re_frisk_BANG_.cljs$lang$applyTo = (function (seq83791){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq83791));
});

re_frisk.core.enable_frisk_BANG_ = (function re_frisk$core$enable_frisk_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___83859 = arguments.length;
var i__4731__auto___83860 = (0);
while(true){
if((i__4731__auto___83860 < len__4730__auto___83859)){
args__4736__auto__.push((arguments[i__4731__auto___83860]));

var G__83861 = (i__4731__auto___83860 + (1));
i__4731__auto___83860 = G__83861;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return re_frisk.core.enable_frisk_BANG_.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

re_frisk.core.enable_frisk_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (params){
if(cljs.core.truth_(cljs.core.deref(re_frisk.data.initialized))){
return null;
} else {
cljs.core.reset_BANG_(re_frisk.data.initialized,true);

return setTimeout(re_frisk.core.render_re_frisk,(100),cljs.core.first(params));
}
});

re_frisk.core.enable_frisk_BANG_.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
re_frisk.core.enable_frisk_BANG_.cljs$lang$applyTo = (function (seq83810){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq83810));
});

re_frisk.core.add_data = (function re_frisk$core$add_data(key,data){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.re_frame_data,cljs.core.assoc,key,data);
});
re_frisk.core.add_in_data = (function re_frisk$core$add_in_data(keys,data){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.re_frame_data,cljs.core.assoc_in,keys,data);
});
re_frisk.core.watch_context = (function (){var G__83814 = new cljs.core.Keyword(null,"id","id",-1388402092);
var G__83815 = new cljs.core.Keyword(null,"re-frisk-watch-context","re-frisk-watch-context",1102368726);
var G__83816 = new cljs.core.Keyword(null,"before","before",-1633692388);
var G__83817 = ((function (G__83814,G__83815,G__83816){
return (function (context){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.deb_data,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"contexts","contexts",4351546),cljs.core.first(new cljs.core.Keyword(null,"event","event",301435442).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"coeffects","coeffects",497912985).cljs$core$IFn$_invoke$arity$1(context))),new cljs.core.Keyword(null,"before","before",-1633692388)], null),context);

return context;
});})(G__83814,G__83815,G__83816))
;
return (re_frame.core.__GT_interceptor.cljs$core$IFn$_invoke$arity$4 ? re_frame.core.__GT_interceptor.cljs$core$IFn$_invoke$arity$4(G__83814,G__83815,G__83816,G__83817) : re_frame.core.__GT_interceptor.call(null,G__83814,G__83815,G__83816,G__83817));
})();
re_frisk.core.reg_view = (function re_frisk$core$reg_view(view,subs,events){
if(cljs.core.truth_(new cljs.core.Keyword(null,"app-db","app-db",865606302).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.re_frame_data)))){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.re_frame_data,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"views","views",1450155487),view,new cljs.core.Keyword(null,"events","events",1792552201)], null),cljs.core.set(events));

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.re_frame_data,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"views","views",1450155487),view,new cljs.core.Keyword(null,"subs","subs",-186681991)], null),cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__83818_SHARP_){
return cljs.core.PersistentHashMap.fromArrays([p1__83818_SHARP_],[(function (){var G__83821 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__83818_SHARP_], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__83821) : re_frame.core.subscribe.call(null,G__83821));
})()]);
}),subs)));

var seq__83823 = cljs.core.seq(subs);
var chunk__83824 = null;
var count__83825 = (0);
var i__83826 = (0);
while(true){
if((i__83826 < count__83825)){
var s = chunk__83824.cljs$core$IIndexed$_nth$arity$2(null,i__83826);
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.re_frame_data,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"subs","subs",-186681991),s], null),(function (){var G__83829 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [s], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__83829) : re_frame.core.subscribe.call(null,G__83829));
})());


var G__83870 = seq__83823;
var G__83871 = chunk__83824;
var G__83872 = count__83825;
var G__83873 = (i__83826 + (1));
seq__83823 = G__83870;
chunk__83824 = G__83871;
count__83825 = G__83872;
i__83826 = G__83873;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__83823);
if(temp__5735__auto__){
var seq__83823__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__83823__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__83823__$1);
var G__83875 = cljs.core.chunk_rest(seq__83823__$1);
var G__83876 = c__4550__auto__;
var G__83877 = cljs.core.count(c__4550__auto__);
var G__83878 = (0);
seq__83823 = G__83875;
chunk__83824 = G__83876;
count__83825 = G__83877;
i__83826 = G__83878;
continue;
} else {
var s = cljs.core.first(seq__83823__$1);
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frisk.data.re_frame_data,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"subs","subs",-186681991),s], null),(function (){var G__83831 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [s], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__83831) : re_frame.core.subscribe.call(null,G__83831));
})());


var G__83879 = cljs.core.next(seq__83823__$1);
var G__83880 = null;
var G__83881 = (0);
var G__83882 = (0);
seq__83823 = G__83879;
chunk__83824 = G__83880;
count__83825 = G__83881;
i__83826 = G__83882;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
});
re_frisk.core.unmount_view = (function re_frisk$core$unmount_view(view){
if(cljs.core.truth_(new cljs.core.Keyword(null,"app-db","app-db",865606302).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frisk.data.re_frame_data)))){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(re_frisk.data.re_frame_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"views","views",1450155487)], null),cljs.core.dissoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([view], 0));
} else {
return null;
}
});
var G__83833_83883 = new cljs.core.Keyword("re-frisk","update-db","re-frisk/update-db",-658524246);
var G__83834_83884 = ((function (G__83833_83883){
return (function (_,p__83835){
var vec__83836 = p__83835;
var ___$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__83836,(0),null);
var value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__83836,(1),null);
return value;
});})(G__83833_83883))
;
var fexpr__83832_83885 = (function (){var or__4131__auto__ = re_frame.core.reg_event_db;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return re_frame.core.register_handler;
}
})();
(fexpr__83832_83885.cljs$core$IFn$_invoke$arity$2 ? fexpr__83832_83885.cljs$core$IFn$_invoke$arity$2(G__83833_83883,G__83834_83884) : fexpr__83832_83885.call(null,G__83833_83883,G__83834_83884));

//# sourceMappingURL=re_frisk.core.js.map
