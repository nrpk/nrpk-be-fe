goog.provide('clojure.math.combinatorics');
goog.require('cljs.core');
clojure.math.combinatorics._STAR__SINGLEQUOTE_ = cljs.core._STAR_;
clojure.math.combinatorics._PLUS__SINGLEQUOTE_ = cljs.core._PLUS_;
/**
 * Lazily concatenates a collection of collections into a flat sequence,
 *   because Clojure's `apply concat` is insufficiently lazy.
 */
clojure.math.combinatorics.join = (function clojure$math$combinatorics$join(colls){
return (new cljs.core.LazySeq(null,(function (){
var temp__5735__auto__ = cljs.core.seq(colls);
if(temp__5735__auto__){
var s = temp__5735__auto__;
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.first(s),(function (){var G__90920 = cljs.core.rest(s);
return (clojure.math.combinatorics.join.cljs$core$IFn$_invoke$arity$1 ? clojure.math.combinatorics.join.cljs$core$IFn$_invoke$arity$1(G__90920) : clojure.math.combinatorics.join.call(null,G__90920));
})());
} else {
return null;
}
}),null,null));
});
/**
 * Uses join to achieve lazier version of mapcat (on one collection)
 */
clojure.math.combinatorics.mapjoin = (function clojure$math$combinatorics$mapjoin(f,coll){
return clojure.math.combinatorics.join(cljs.core.map.cljs$core$IFn$_invoke$arity$2(f,coll));
});
/**
 * Annoyingly, the built-in distinct? doesn't handle 0 args, so we need
 *   to write our own version that considers the empty-list to be distinct
 */
clojure.math.combinatorics.all_different_QMARK_ = (function clojure$math$combinatorics$all_different_QMARK_(s){
if(cljs.core.seq(s)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.distinct_QMARK_,s);
} else {
return true;
}
});
clojure.math.combinatorics.index_combinations = (function clojure$math$combinatorics$index_combinations(n,cnt){
return (new cljs.core.LazySeq(null,(function (){
var c = cljs.core.vec(cljs.core.cons(null,(function (){var iter__4523__auto__ = (function clojure$math$combinatorics$index_combinations_$_iter__90926(s__90927){
return (new cljs.core.LazySeq(null,(function (){
var s__90927__$1 = s__90927;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__90927__$1);
if(temp__5735__auto__){
var s__90927__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__90927__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__90927__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__90929 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__90928 = (0);
while(true){
if((i__90928 < size__4522__auto__)){
var j = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__90928);
cljs.core.chunk_append(b__90929,((j + cnt) + (- (n + (1)))));

var G__91837 = (i__90928 + (1));
i__90928 = G__91837;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__90929),clojure$math$combinatorics$index_combinations_$_iter__90926(cljs.core.chunk_rest(s__90927__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__90929),null);
}
} else {
var j = cljs.core.first(s__90927__$2);
return cljs.core.cons(((j + cnt) + (- (n + (1)))),clojure$math$combinatorics$index_combinations_$_iter__90926(cljs.core.rest(s__90927__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2((1),(n + (1))));
})()));
var iter_comb = ((function (c){
return (function clojure$math$combinatorics$index_combinations_$_iter_comb(c__$1,j){
if((j > n)){
return null;
} else {
var c__$2 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(c__$1,j,((c__$1.cljs$core$IFn$_invoke$arity$1 ? c__$1.cljs$core$IFn$_invoke$arity$1(j) : c__$1.call(null,j)) - (1)));
if(((c__$2.cljs$core$IFn$_invoke$arity$1 ? c__$2.cljs$core$IFn$_invoke$arity$1(j) : c__$2.call(null,j)) < j)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [c__$2,(j + (1))], null);
} else {
var c__$3 = c__$2;
var j__$1 = j;
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(j__$1,(1))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [c__$3,j__$1], null);
} else {
var G__91838 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(c__$3,(j__$1 - (1)),((c__$3.cljs$core$IFn$_invoke$arity$1 ? c__$3.cljs$core$IFn$_invoke$arity$1(j__$1) : c__$3.call(null,j__$1)) - (1)));
var G__91839 = (j__$1 - (1));
c__$3 = G__91838;
j__$1 = G__91839;
continue;
}
break;
}
}
}
});})(c))
;
var step = ((function (c,iter_comb){
return (function clojure$math$combinatorics$index_combinations_$_step(c__$1,j){
return cljs.core.cons(cljs.core.rseq(cljs.core.subvec.cljs$core$IFn$_invoke$arity$3(c__$1,(1),(n + (1)))),(new cljs.core.LazySeq(null,((function (c,iter_comb){
return (function (){
var next_step = iter_comb(c__$1,j);
if(cljs.core.truth_(next_step)){
return clojure$math$combinatorics$index_combinations_$_step((next_step.cljs$core$IFn$_invoke$arity$1 ? next_step.cljs$core$IFn$_invoke$arity$1((0)) : next_step.call(null,(0))),(next_step.cljs$core$IFn$_invoke$arity$1 ? next_step.cljs$core$IFn$_invoke$arity$1((1)) : next_step.call(null,(1))));
} else {
return null;
}
});})(c,iter_comb))
,null,null)));
});})(c,iter_comb))
;
return step(c,(1));
}),null,null));
});
clojure.math.combinatorics.distribute = (function clojure$math$combinatorics$distribute(m,index,total,distribution,already_distributed){
var distribution__$1 = distribution;
var index__$1 = index;
var already_distributed__$1 = already_distributed;
while(true){
if((index__$1 >= cljs.core.count(m))){
return null;
} else {
var quantity_to_distribute = (total - already_distributed__$1);
var mi = (m.cljs$core$IFn$_invoke$arity$1 ? m.cljs$core$IFn$_invoke$arity$1(index__$1) : m.call(null,index__$1));
if((quantity_to_distribute <= mi)){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(distribution__$1,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [index__$1,quantity_to_distribute,total], null));
} else {
var G__91840 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(distribution__$1,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [index__$1,mi,(already_distributed__$1 + mi)], null));
var G__91841 = (index__$1 + (1));
var G__91842 = (already_distributed__$1 + mi);
distribution__$1 = G__91840;
index__$1 = G__91841;
already_distributed__$1 = G__91842;
continue;
}
}
break;
}
});
clojure.math.combinatorics.next_distribution = (function clojure$math$combinatorics$next_distribution(m,total,distribution){
var vec__90934 = cljs.core.peek(distribution);
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90934,(0),null);
var this_bucket = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90934,(1),null);
var this_and_to_the_left = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90934,(2),null);
if((index < (cljs.core.count(m) - (1)))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this_bucket,(1))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(cljs.core.pop(distribution),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(index + (1)),(1),this_and_to_the_left], null));
} else {
return cljs.core.conj.cljs$core$IFn$_invoke$arity$variadic(cljs.core.pop(distribution),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [index,(this_bucket - (1)),(this_and_to_the_left - (1))], null),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(index + (1)),(1),this_and_to_the_left], null)], 0));
}
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this_bucket,total)){
return null;
} else {
var distribution__$1 = cljs.core.pop(distribution);
while(true){
var vec__90944 = cljs.core.peek(distribution__$1);
var index__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90944,(0),null);
var this_bucket__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90944,(1),null);
var this_and_to_the_left__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90944,(2),null);
var distribution__$2 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this_bucket__$1,(1)))?cljs.core.pop(distribution__$1):cljs.core.conj.cljs$core$IFn$_invoke$arity$2(cljs.core.pop(distribution__$1),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [index__$1,(this_bucket__$1 - (1)),(this_and_to_the_left__$1 - (1))], null)));
if(((total - (this_and_to_the_left__$1 - (1))) <= cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,cljs.core.subvec.cljs$core$IFn$_invoke$arity$2(m,(index__$1 + (1)))))){
return clojure.math.combinatorics.distribute(m,(index__$1 + (1)),total,distribution__$2,(this_and_to_the_left__$1 - (1)));
} else {
if(cljs.core.seq(distribution__$2)){
var G__91846 = distribution__$2;
distribution__$1 = G__91846;
continue;
} else {
return null;

}
}
break;
}

}
}
});
clojure.math.combinatorics.bounded_distributions = (function clojure$math$combinatorics$bounded_distributions(m,t){
var step = (function clojure$math$combinatorics$bounded_distributions_$_step(distribution){
return cljs.core.cons(distribution,(new cljs.core.LazySeq(null,(function (){
var temp__5735__auto__ = clojure.math.combinatorics.next_distribution(m,t,distribution);
if(cljs.core.truth_(temp__5735__auto__)){
var next_step = temp__5735__auto__;
return clojure$math$combinatorics$bounded_distributions_$_step(next_step);
} else {
return null;
}
}),null,null)));
});
return step(clojure.math.combinatorics.distribute(m,(0),t,cljs.core.PersistentVector.EMPTY,(0)));
});
/**
 * Handles the case when you want the combinations of a list with duplicate items.
 */
clojure.math.combinatorics.multi_comb = (function clojure$math$combinatorics$multi_comb(l,t){
var f = cljs.core.frequencies(l);
var v = cljs.core.vec(cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(l));
var domain = cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(v));
var m = cljs.core.vec((function (){var iter__4523__auto__ = ((function (f,v,domain){
return (function clojure$math$combinatorics$multi_comb_$_iter__90955(s__90956){
return (new cljs.core.LazySeq(null,((function (f,v,domain){
return (function (){
var s__90956__$1 = s__90956;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__90956__$1);
if(temp__5735__auto__){
var s__90956__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__90956__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__90956__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__90958 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__90957 = (0);
while(true){
if((i__90957 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__90957);
cljs.core.chunk_append(b__90958,(function (){var G__90959 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__90959) : f.call(null,G__90959));
})());

var G__91849 = (i__90957 + (1));
i__90957 = G__91849;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__90958),clojure$math$combinatorics$multi_comb_$_iter__90955(cljs.core.chunk_rest(s__90956__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__90958),null);
}
} else {
var i = cljs.core.first(s__90956__$2);
return cljs.core.cons((function (){var G__90960 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__90960) : f.call(null,G__90960));
})(),clojure$math$combinatorics$multi_comb_$_iter__90955(cljs.core.rest(s__90956__$2)));
}
} else {
return null;
}
break;
}
});})(f,v,domain))
,null,null));
});})(f,v,domain))
;
return iter__4523__auto__(domain);
})());
var qs = clojure.math.combinatorics.bounded_distributions(m,t);
var iter__4523__auto__ = ((function (f,v,domain,m,qs){
return (function clojure$math$combinatorics$multi_comb_$_iter__90961(s__90962){
return (new cljs.core.LazySeq(null,((function (f,v,domain,m,qs){
return (function (){
var s__90962__$1 = s__90962;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__90962__$1);
if(temp__5735__auto__){
var s__90962__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__90962__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__90962__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__90964 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__90963 = (0);
while(true){
if((i__90963 < size__4522__auto__)){
var q = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__90963);
cljs.core.chunk_append(b__90964,clojure.math.combinatorics.join((function (){var iter__4523__auto__ = ((function (i__90963,q,c__4521__auto__,size__4522__auto__,b__90964,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs){
return (function clojure$math$combinatorics$multi_comb_$_iter__90961_$_iter__90965(s__90966){
return (new cljs.core.LazySeq(null,((function (i__90963,q,c__4521__auto__,size__4522__auto__,b__90964,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs){
return (function (){
var s__90966__$1 = s__90966;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__90966__$1);
if(temp__5735__auto____$1){
var s__90966__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__90966__$2)){
var c__4521__auto____$1 = cljs.core.chunk_first(s__90966__$2);
var size__4522__auto____$1 = cljs.core.count(c__4521__auto____$1);
var b__90968 = cljs.core.chunk_buffer(size__4522__auto____$1);
if((function (){var i__90967 = (0);
while(true){
if((i__90967 < size__4522__auto____$1)){
var vec__90969 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto____$1,i__90967);
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90969,(0),null);
var this_bucket = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90969,(1),null);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90969,(2),null);
cljs.core.chunk_append(b__90968,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(this_bucket,(v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(index) : v.call(null,index))));

var G__91851 = (i__90967 + (1));
i__90967 = G__91851;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__90968),clojure$math$combinatorics$multi_comb_$_iter__90961_$_iter__90965(cljs.core.chunk_rest(s__90966__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__90968),null);
}
} else {
var vec__90973 = cljs.core.first(s__90966__$2);
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90973,(0),null);
var this_bucket = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90973,(1),null);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90973,(2),null);
return cljs.core.cons(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(this_bucket,(v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(index) : v.call(null,index))),clojure$math$combinatorics$multi_comb_$_iter__90961_$_iter__90965(cljs.core.rest(s__90966__$2)));
}
} else {
return null;
}
break;
}
});})(i__90963,q,c__4521__auto__,size__4522__auto__,b__90964,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs))
,null,null));
});})(i__90963,q,c__4521__auto__,size__4522__auto__,b__90964,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs))
;
return iter__4523__auto__(q);
})()));

var G__91852 = (i__90963 + (1));
i__90963 = G__91852;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__90964),clojure$math$combinatorics$multi_comb_$_iter__90961(cljs.core.chunk_rest(s__90962__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__90964),null);
}
} else {
var q = cljs.core.first(s__90962__$2);
return cljs.core.cons(clojure.math.combinatorics.join((function (){var iter__4523__auto__ = ((function (q,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs){
return (function clojure$math$combinatorics$multi_comb_$_iter__90961_$_iter__90976(s__90977){
return (new cljs.core.LazySeq(null,((function (q,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs){
return (function (){
var s__90977__$1 = s__90977;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__90977__$1);
if(temp__5735__auto____$1){
var s__90977__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__90977__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__90977__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__90979 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__90978 = (0);
while(true){
if((i__90978 < size__4522__auto__)){
var vec__90981 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__90978);
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90981,(0),null);
var this_bucket = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90981,(1),null);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90981,(2),null);
cljs.core.chunk_append(b__90979,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(this_bucket,(v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(index) : v.call(null,index))));

var G__91857 = (i__90978 + (1));
i__90978 = G__91857;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__90979),clojure$math$combinatorics$multi_comb_$_iter__90961_$_iter__90976(cljs.core.chunk_rest(s__90977__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__90979),null);
}
} else {
var vec__90984 = cljs.core.first(s__90977__$2);
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90984,(0),null);
var this_bucket = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90984,(1),null);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__90984,(2),null);
return cljs.core.cons(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(this_bucket,(v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(index) : v.call(null,index))),clojure$math$combinatorics$multi_comb_$_iter__90961_$_iter__90976(cljs.core.rest(s__90977__$2)));
}
} else {
return null;
}
break;
}
});})(q,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs))
,null,null));
});})(q,s__90962__$2,temp__5735__auto__,f,v,domain,m,qs))
;
return iter__4523__auto__(q);
})()),clojure$math$combinatorics$multi_comb_$_iter__90961(cljs.core.rest(s__90962__$2)));
}
} else {
return null;
}
break;
}
});})(f,v,domain,m,qs))
,null,null));
});})(f,v,domain,m,qs))
;
return iter__4523__auto__(qs);
});
/**
 * All the unique ways of taking t different elements from items
 */
clojure.math.combinatorics.combinations = (function clojure$math$combinatorics$combinations(items,t){
var v_items = cljs.core.vec(cljs.core.reverse(items));
if((t === (0))){
return (new cljs.core.List(null,cljs.core.List.EMPTY,null,(1),null));
} else {
var cnt = cljs.core.count(items);
if((t > cnt)){
return null;
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,(1))){
var iter__4523__auto__ = ((function (cnt,v_items){
return (function clojure$math$combinatorics$combinations_$_iter__90991(s__90992){
return (new cljs.core.LazySeq(null,((function (cnt,v_items){
return (function (){
var s__90992__$1 = s__90992;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__90992__$1);
if(temp__5735__auto__){
var s__90992__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__90992__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__90992__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__90994 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__90993 = (0);
while(true){
if((i__90993 < size__4522__auto__)){
var item = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__90993);
cljs.core.chunk_append(b__90994,(new cljs.core.List(null,item,null,(1),null)));

var G__91859 = (i__90993 + (1));
i__90993 = G__91859;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__90994),clojure$math$combinatorics$combinations_$_iter__90991(cljs.core.chunk_rest(s__90992__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__90994),null);
}
} else {
var item = cljs.core.first(s__90992__$2);
return cljs.core.cons((new cljs.core.List(null,item,null,(1),null)),clojure$math$combinatorics$combinations_$_iter__90991(cljs.core.rest(s__90992__$2)));
}
} else {
return null;
}
break;
}
});})(cnt,v_items))
,null,null));
});})(cnt,v_items))
;
return iter__4523__auto__(cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(items));
} else {
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,cnt)){
return (new cljs.core.List(null,cljs.core.seq(items),null,(1),null));
} else {
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (cnt,v_items){
return (function (p1__90987_SHARP_){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(v_items,p1__90987_SHARP_);
});})(cnt,v_items))
,clojure.math.combinatorics.index_combinations(t,cnt));
}
} else {
return clojure.math.combinatorics.multi_comb(items,t);

}
}
}
}
});
/**
 * Given a sequence that may have chunks, return a sequence that is 1-at-a-time
 * lazy with no chunks. Chunks are good for efficiency when the data items are
 * small, but when being processed via map, for example, a reference is kept to
 * every function result in the chunk until the entire chunk has been processed,
 * which increases the amount of memory in use that cannot be garbage
 * collected.
 */
clojure.math.combinatorics.unchunk = (function clojure$math$combinatorics$unchunk(s){
return (new cljs.core.LazySeq(null,(function (){
if(cljs.core.seq(s)){
return cljs.core.cons(cljs.core.first(s),(function (){var G__90998 = cljs.core.rest(s);
return (clojure.math.combinatorics.unchunk.cljs$core$IFn$_invoke$arity$1 ? clojure.math.combinatorics.unchunk.cljs$core$IFn$_invoke$arity$1(G__90998) : clojure.math.combinatorics.unchunk.call(null,G__90998));
})());
} else {
return null;
}
}),null,null));
});
/**
 * All the subsets of items
 */
clojure.math.combinatorics.subsets = (function clojure$math$combinatorics$subsets(items){
return clojure.math.combinatorics.mapjoin((function (n){
return clojure.math.combinatorics.combinations(items,n);
}),clojure.math.combinatorics.unchunk(cljs.core.range.cljs$core$IFn$_invoke$arity$1((cljs.core.count(items) + (1)))));
});
/**
 * All the ways to take one item from each sequence
 */
clojure.math.combinatorics.cartesian_product = (function clojure$math$combinatorics$cartesian_product(var_args){
var args__4736__auto__ = [];
var len__4730__auto___91864 = arguments.length;
var i__4731__auto___91865 = (0);
while(true){
if((i__4731__auto___91865 < len__4730__auto___91864)){
args__4736__auto__.push((arguments[i__4731__auto___91865]));

var G__91869 = (i__4731__auto___91865 + (1));
i__4731__auto___91865 = G__91869;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return clojure.math.combinatorics.cartesian_product.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

clojure.math.combinatorics.cartesian_product.cljs$core$IFn$_invoke$arity$variadic = (function (seqs){
var v_original_seqs = cljs.core.vec(seqs);
var step = ((function (v_original_seqs){
return (function clojure$math$combinatorics$step(v_seqs){
var increment = ((function (v_original_seqs){
return (function (v_seqs__$1){
var i = (cljs.core.count(v_seqs__$1) - (1));
var v_seqs__$2 = v_seqs__$1;
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(i,(-1))){
return null;
} else {
var temp__5733__auto__ = cljs.core.next((v_seqs__$2.cljs$core$IFn$_invoke$arity$1 ? v_seqs__$2.cljs$core$IFn$_invoke$arity$1(i) : v_seqs__$2.call(null,i)));
if(temp__5733__auto__){
var rst = temp__5733__auto__;
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v_seqs__$2,i,rst);
} else {
var G__91871 = (i - (1));
var G__91872 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v_seqs__$2,i,(v_original_seqs.cljs$core$IFn$_invoke$arity$1 ? v_original_seqs.cljs$core$IFn$_invoke$arity$1(i) : v_original_seqs.call(null,i)));
i = G__91871;
v_seqs__$2 = G__91872;
continue;
}
}
break;
}
});})(v_original_seqs))
;
if(cljs.core.truth_(v_seqs)){
return cljs.core.cons(cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.first,v_seqs),(new cljs.core.LazySeq(null,((function (increment,v_original_seqs){
return (function (){
return clojure$math$combinatorics$step(increment(v_seqs));
});})(increment,v_original_seqs))
,null,null)));
} else {
return null;
}
});})(v_original_seqs))
;
if(cljs.core.every_QMARK_(cljs.core.seq,seqs)){
return (new cljs.core.LazySeq(null,((function (v_original_seqs,step){
return (function (){
return step(v_original_seqs);
});})(v_original_seqs,step))
,null,null));
} else {
return null;
}
});

clojure.math.combinatorics.cartesian_product.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
clojure.math.combinatorics.cartesian_product.cljs$lang$applyTo = (function (seq91002){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq91002));
});

/**
 * All the ways of taking n (possibly the same) elements from the sequence of items
 */
clojure.math.combinatorics.selections = (function clojure$math$combinatorics$selections(items,n){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(clojure.math.combinatorics.cartesian_product,cljs.core.take.cljs$core$IFn$_invoke$arity$2(n,cljs.core.repeat.cljs$core$IFn$_invoke$arity$1(items)));
});
clojure.math.combinatorics.iter_perm = (function clojure$math$combinatorics$iter_perm(v){
var len = cljs.core.count(v);
var j = (function (){var i = (len - (2));
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(i,(-1))){
return null;
} else {
if(((v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i)) < (function (){var G__91009 = (i + (1));
return (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(G__91009) : v.call(null,G__91009));
})())){
return i;
} else {
var G__91875 = (i - (1));
i = G__91875;
continue;

}
}
break;
}
})();
if(cljs.core.truth_(j)){
var vj = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(j) : v.call(null,j));
var l = (function (){var i = (len - (1));
while(true){
if((vj < (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i)))){
return i;
} else {
var G__91876 = (i - (1));
i = G__91876;
continue;
}
break;
}
})();
var v__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(v,j,(v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(l) : v.call(null,l)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([l,vj], 0));
var k = (j + (1));
var l__$1 = (len - (1));
while(true){
if((k < l__$1)){
var G__91877 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(v__$1,k,(v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(l__$1) : v__$1.call(null,l__$1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([l__$1,(v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(k) : v__$1.call(null,k))], 0));
var G__91878 = (k + (1));
var G__91879 = (l__$1 - (1));
v__$1 = G__91877;
k = G__91878;
l__$1 = G__91879;
continue;
} else {
return v__$1;
}
break;
}
} else {
return null;
}
});
clojure.math.combinatorics.vec_lex_permutations = (function clojure$math$combinatorics$vec_lex_permutations(v){
if(cljs.core.truth_(v)){
return cljs.core.cons(v,(new cljs.core.LazySeq(null,(function (){
var G__91012 = clojure.math.combinatorics.iter_perm(v);
return (clojure.math.combinatorics.vec_lex_permutations.cljs$core$IFn$_invoke$arity$1 ? clojure.math.combinatorics.vec_lex_permutations.cljs$core$IFn$_invoke$arity$1(G__91012) : clojure.math.combinatorics.vec_lex_permutations.call(null,G__91012));
}),null,null)));
} else {
return null;
}
});
/**
 * DEPRECATED as a public function.
 * 
 * In prior versions of the combinatorics library, there were two similar functions: permutations and lex-permutations.  It was a source of confusion to know which to call.  Now, you can always call permutations.  When appropriate (i.e., when you pass in a sorted sequence of numbers), permutations will automatically call lex-permutations as a speed optimization.
 */
clojure.math.combinatorics.lex_permutations = (function clojure$math$combinatorics$lex_permutations(c){
return (new cljs.core.LazySeq(null,(function (){
var vec_sorted = cljs.core.vec(cljs.core.sort.cljs$core$IFn$_invoke$arity$1(c));
if((cljs.core.count(vec_sorted) === (0))){
return (new cljs.core.List(null,cljs.core.PersistentVector.EMPTY,null,(1),null));
} else {
return clojure.math.combinatorics.vec_lex_permutations(vec_sorted);
}
}),null,null));
});
/**
 * Returns true iff s is a sequence of numbers in non-decreasing order
 */
clojure.math.combinatorics.sorted_numbers_QMARK_ = (function clojure$math$combinatorics$sorted_numbers_QMARK_(s){
var and__4120__auto__ = cljs.core.every_QMARK_(cljs.core.number_QMARK_,s);
if(and__4120__auto__){
var or__4131__auto__ = cljs.core.empty_QMARK_(s);
if(or__4131__auto__){
return or__4131__auto__;
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._LT__EQ_,s);
}
} else {
return and__4120__auto__;
}
});
/**
 * Handles the case when you want the permutations of a list with duplicate items.
 */
clojure.math.combinatorics.multi_perm = (function clojure$math$combinatorics$multi_perm(l){
var f = cljs.core.frequencies(l);
var v = cljs.core.vec(cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(l));
var indices = clojure.math.combinatorics.join((function (){var iter__4523__auto__ = ((function (f,v){
return (function clojure$math$combinatorics$multi_perm_$_iter__91018(s__91019){
return (new cljs.core.LazySeq(null,((function (f,v){
return (function (){
var s__91019__$1 = s__91019;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91019__$1);
if(temp__5735__auto__){
var s__91019__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91019__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91019__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91021 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91020 = (0);
while(true){
if((i__91020 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91020);
cljs.core.chunk_append(b__91021,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91023 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91023) : f.call(null,G__91023));
})(),i));

var G__91884 = (i__91020 + (1));
i__91020 = G__91884;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91021),clojure$math$combinatorics$multi_perm_$_iter__91018(cljs.core.chunk_rest(s__91019__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91021),null);
}
} else {
var i = cljs.core.first(s__91019__$2);
return cljs.core.cons(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91025 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91025) : f.call(null,G__91025));
})(),i),clojure$math$combinatorics$multi_perm_$_iter__91018(cljs.core.rest(s__91019__$2)));
}
} else {
return null;
}
break;
}
});})(f,v))
,null,null));
});})(f,v))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(v)));
})());
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.map,v),clojure.math.combinatorics.lex_permutations(indices));
});
/**
 * All the distinct permutations of items, lexicographic by index 
 *   (special handling for duplicate items).
 */
clojure.math.combinatorics.permutations = (function clojure$math$combinatorics$permutations(items){
if(cljs.core.truth_(clojure.math.combinatorics.sorted_numbers_QMARK_(items))){
return clojure.math.combinatorics.lex_permutations(items);
} else {
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
var v = cljs.core.vec(items);
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (v){
return (function (p1__91027_SHARP_){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(v,p1__91027_SHARP_);
});})(v))
,clojure.math.combinatorics.lex_permutations(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(v))));
} else {
return clojure.math.combinatorics.multi_perm(items);

}
}
});
/**
 * Every permutation of every combination of t elements from items
 */
clojure.math.combinatorics.permuted_combinations = (function clojure$math$combinatorics$permuted_combinations(items,t){
return clojure.math.combinatorics.join(cljs.core.map.cljs$core$IFn$_invoke$arity$2(clojure.math.combinatorics.permutations,clojure.math.combinatorics.combinations(items,t)));
});
clojure.math.combinatorics.factorial = (function clojure$math$combinatorics$factorial(n){
if(cljs.core.integer_QMARK_(n)){
} else {
throw (new Error("Assert failed: (integer? n)"));
}

if((!((n < (0))))){
} else {
throw (new Error("Assert failed: (not (neg? n))"));
}

var acc = (1);
var n__$1 = n;
while(true){
if((n__$1 === (0))){
return acc;
} else {
var G__91885 = (clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(acc,n__$1) : clojure.math.combinatorics._STAR__SINGLEQUOTE_.call(null,acc,n__$1));
var G__91886 = (n__$1 - (1));
acc = G__91885;
n__$1 = G__91886;
continue;
}
break;
}
});
/**
 * Input is a non-negative base 10 integer, output is the number in the
 * factorial number system (http://en.wikipedia.org/wiki/Factorial_number_system)
 * expressed as a list of 'digits'
 */
clojure.math.combinatorics.factorial_numbers = (function clojure$math$combinatorics$factorial_numbers(n){
if(cljs.core.integer_QMARK_(n)){
} else {
throw (new Error("Assert failed: (integer? n)"));
}

if((!((n < (0))))){
} else {
throw (new Error("Assert failed: (not (neg? n))"));
}

var n__$1 = n;
var digits = cljs.core.List.EMPTY;
var divisor = (1);
while(true){
if((n__$1 === (0))){
return digits;
} else {
var q = cljs.core.quot(n__$1,divisor);
var r = cljs.core.rem(n__$1,divisor);
var G__91888 = q;
var G__91889 = cljs.core.cons(r,digits);
var G__91890 = (divisor + (1));
n__$1 = G__91888;
digits = G__91889;
divisor = G__91890;
continue;
}
break;
}
});
clojure.math.combinatorics.remove_nth = (function clojure$math$combinatorics$remove_nth(l,n){
var acc = cljs.core.PersistentVector.EMPTY;
var l__$1 = l;
var n__$1 = n;
while(true){
if((n__$1 === (0))){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(acc,cljs.core.rest(l__$1));
} else {
var G__91891 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(acc,cljs.core.first(l__$1));
var G__91892 = cljs.core.rest(l__$1);
var G__91893 = (n__$1 - (1));
acc = G__91891;
l__$1 = G__91892;
n__$1 = G__91893;
continue;
}
break;
}
});
/**
 * Input should be a sorted sequential collection l of distinct items, 
 * output is nth-permutation (0-based)
 */
clojure.math.combinatorics.nth_permutation_distinct = (function clojure$math$combinatorics$nth_permutation_distinct(l,n){
if((n < clojure.math.combinatorics.factorial(cljs.core.count(l)))){
} else {
throw (new Error(["Assert failed: ",cljs.core.print_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([n,"is too large. Input has only",clojure.math.combinatorics.factorial(cljs.core.count(l)),"permutations."], 0)),"\n","(< n (factorial (count l)))"].join('')));
}

var length = cljs.core.count(l);
var fact_nums = clojure.math.combinatorics.factorial_numbers(n);
var indices = cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((length - cljs.core.count(fact_nums)),(0)),fact_nums);
var l__$1 = l;
var perm = cljs.core.PersistentVector.EMPTY;
while(true){
if(cljs.core.empty_QMARK_(indices)){
return perm;
} else {
var i = cljs.core.first(indices);
var item = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(l__$1,i);
var G__91896 = cljs.core.rest(indices);
var G__91897 = clojure.math.combinatorics.remove_nth(l__$1,i);
var G__91898 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(perm,item);
indices = G__91896;
l__$1 = G__91897;
perm = G__91898;
continue;
}
break;
}
});
clojure.math.combinatorics.count_permutations_from_frequencies = (function clojure$math$combinatorics$count_permutations_from_frequencies(freqs){
var counts = cljs.core.vals(freqs);
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._SLASH_,clojure.math.combinatorics.factorial(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,counts)),cljs.core.map.cljs$core$IFn$_invoke$arity$2(clojure.math.combinatorics.factorial,counts));
});
/**
 * Counts the number of distinct permutations of l
 */
clojure.math.combinatorics.count_permutations = (function clojure$math$combinatorics$count_permutations(l){
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(l))){
return clojure.math.combinatorics.factorial(cljs.core.count(l));
} else {
return clojure.math.combinatorics.count_permutations_from_frequencies(cljs.core.frequencies(l));
}
});
/**
 * Takes a sorted frequency map and returns how far into the sequence of
 * lexicographic permutations you get by varying the first item
 */
clojure.math.combinatorics.initial_perm_numbers = (function clojure$math$combinatorics$initial_perm_numbers(freqs){
return cljs.core.reductions.cljs$core$IFn$_invoke$arity$3(clojure.math.combinatorics._PLUS__SINGLEQUOTE_,(0),(function (){var iter__4523__auto__ = (function clojure$math$combinatorics$initial_perm_numbers_$_iter__91098(s__91099){
return (new cljs.core.LazySeq(null,(function (){
var s__91099__$1 = s__91099;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91099__$1);
if(temp__5735__auto__){
var s__91099__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91099__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91099__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91101 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91100 = (0);
while(true){
if((i__91100 < size__4522__auto__)){
var vec__91110 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91100);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91110,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91110,(1),null);
cljs.core.chunk_append(b__91101,clojure.math.combinatorics.count_permutations_from_frequencies(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(freqs,k,(v - (1)))));

var G__91901 = (i__91100 + (1));
i__91100 = G__91901;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91101),clojure$math$combinatorics$initial_perm_numbers_$_iter__91098(cljs.core.chunk_rest(s__91099__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91101),null);
}
} else {
var vec__91120 = cljs.core.first(s__91099__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91120,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91120,(1),null);
return cljs.core.cons(clojure.math.combinatorics.count_permutations_from_frequencies(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(freqs,k,(v - (1)))),clojure$math$combinatorics$initial_perm_numbers_$_iter__91098(cljs.core.rest(s__91099__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(freqs);
})());
});
/**
 * Finds the index and remainder from the initial-perm-numbers.
 */
clojure.math.combinatorics.index_remainder = (function clojure$math$combinatorics$index_remainder(perm_numbers,n){
var perm_numbers__$1 = perm_numbers;
var index = (0);
while(true){
if((((cljs.core.first(perm_numbers__$1) <= n)) && ((n < cljs.core.second(perm_numbers__$1))))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [index,(n - cljs.core.first(perm_numbers__$1))], null);
} else {
var G__91902 = cljs.core.rest(perm_numbers__$1);
var G__91903 = (index + (1));
perm_numbers__$1 = G__91902;
index = G__91903;
continue;
}
break;
}
});
clojure.math.combinatorics.dec_key = (function clojure$math$combinatorics$dec_key(m,k){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),(m.cljs$core$IFn$_invoke$arity$1 ? m.cljs$core$IFn$_invoke$arity$1(k) : m.call(null,k)))){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(m,k);
} else {
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(m,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [k], null),cljs.core.dec);
}
});
/**
 * Input is a non-negative base 10 integer n, and a sorted frequency map freqs.
 * Output is a list of 'digits' in this wacky duplicate factorial number system
 */
clojure.math.combinatorics.factorial_numbers_with_duplicates = (function clojure$math$combinatorics$factorial_numbers_with_duplicates(n,freqs){
var n__$1 = n;
var digits = cljs.core.PersistentVector.EMPTY;
var freqs__$1 = freqs;
while(true){
if((n__$1 === (0))){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(digits,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,cljs.core.vals(freqs__$1)),(0)));
} else {
var vec__91164 = clojure.math.combinatorics.index_remainder(clojure.math.combinatorics.initial_perm_numbers(freqs__$1),n__$1);
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91164,(0),null);
var remainder = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91164,(1),null);
var G__91905 = remainder;
var G__91906 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(digits,index);
var G__91907 = (function (){var nth_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(cljs.core.keys(freqs__$1),index);
return clojure.math.combinatorics.dec_key(freqs__$1,nth_key);
})();
n__$1 = G__91905;
digits = G__91906;
freqs__$1 = G__91907;
continue;
}
break;
}
});
/**
 * Input should be a sorted sequential collection l of distinct items, 
 * output is nth-permutation (0-based)
 */
clojure.math.combinatorics.nth_permutation_duplicates = (function clojure$math$combinatorics$nth_permutation_duplicates(l,n){
if((n < clojure.math.combinatorics.count_permutations(l))){
} else {
throw (new Error(["Assert failed: ",cljs.core.print_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([n,"is too large. Input has only",clojure.math.combinatorics.count_permutations(l),"permutations."], 0)),"\n","(< n (count-permutations l))"].join('')));
}

var freqs = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_map(),cljs.core.frequencies(l));
var indices = clojure.math.combinatorics.factorial_numbers_with_duplicates(n,freqs);
var perm = cljs.core.PersistentVector.EMPTY;
while(true){
if(cljs.core.empty_QMARK_(indices)){
return perm;
} else {
var i = cljs.core.first(indices);
var item = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(cljs.core.keys(freqs),i);
var G__91908 = clojure.math.combinatorics.dec_key(freqs,item);
var G__91909 = cljs.core.rest(indices);
var G__91910 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(perm,item);
freqs = G__91908;
indices = G__91909;
perm = G__91910;
continue;
}
break;
}
});
/**
 * (nth (permutations items)) but calculated more directly.
 */
clojure.math.combinatorics.nth_permutation = (function clojure$math$combinatorics$nth_permutation(items,n){
if(cljs.core.truth_(clojure.math.combinatorics.sorted_numbers_QMARK_(items))){
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
return clojure.math.combinatorics.nth_permutation_distinct(items,n);
} else {
return clojure.math.combinatorics.nth_permutation_duplicates(items,n);
}
} else {
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
var v = cljs.core.vec(items);
var perm_indices = clojure.math.combinatorics.nth_permutation_distinct(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(items)),n);
return cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(v,perm_indices));
} else {
var v = cljs.core.vec(cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(items));
var f = cljs.core.frequencies(items);
var indices = clojure.math.combinatorics.join((function (){var iter__4523__auto__ = ((function (v,f){
return (function clojure$math$combinatorics$nth_permutation_$_iter__91206(s__91207){
return (new cljs.core.LazySeq(null,((function (v,f){
return (function (){
var s__91207__$1 = s__91207;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91207__$1);
if(temp__5735__auto__){
var s__91207__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91207__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91207__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91209 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91208 = (0);
while(true){
if((i__91208 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91208);
cljs.core.chunk_append(b__91209,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91224 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91224) : f.call(null,G__91224));
})(),i));

var G__91921 = (i__91208 + (1));
i__91208 = G__91921;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91209),clojure$math$combinatorics$nth_permutation_$_iter__91206(cljs.core.chunk_rest(s__91207__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91209),null);
}
} else {
var i = cljs.core.first(s__91207__$2);
return cljs.core.cons(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91229 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91229) : f.call(null,G__91229));
})(),i),clojure$math$combinatorics$nth_permutation_$_iter__91206(cljs.core.rest(s__91207__$2)));
}
} else {
return null;
}
break;
}
});})(v,f))
,null,null));
});})(v,f))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(v)));
})());
return cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(v,clojure.math.combinatorics.nth_permutation_duplicates(indices,n)));
}
}
});
/**
 * (drop n (permutations items)) but calculated more directly.
 */
clojure.math.combinatorics.drop_permutations = (function clojure$math$combinatorics$drop_permutations(items,n){
if((n === (0))){
return clojure.math.combinatorics.permutations(items);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(n,clojure.math.combinatorics.count_permutations(items))){
return cljs.core.List.EMPTY;
} else {
if(cljs.core.truth_(clojure.math.combinatorics.sorted_numbers_QMARK_(items))){
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
return clojure.math.combinatorics.vec_lex_permutations(clojure.math.combinatorics.nth_permutation_distinct(items,n));
} else {
return clojure.math.combinatorics.vec_lex_permutations(clojure.math.combinatorics.nth_permutation_duplicates(items,n));
}
} else {
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
var v = cljs.core.vec(items);
var perm_indices = clojure.math.combinatorics.nth_permutation_distinct(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(items)),n);
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.map,v),clojure.math.combinatorics.vec_lex_permutations(perm_indices));
} else {
var v = cljs.core.vec(cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(items));
var f = cljs.core.frequencies(items);
var indices = clojure.math.combinatorics.join((function (){var iter__4523__auto__ = ((function (v,f){
return (function clojure$math$combinatorics$drop_permutations_$_iter__91247(s__91248){
return (new cljs.core.LazySeq(null,((function (v,f){
return (function (){
var s__91248__$1 = s__91248;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91248__$1);
if(temp__5735__auto__){
var s__91248__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91248__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91248__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91250 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91249 = (0);
while(true){
if((i__91249 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91249);
cljs.core.chunk_append(b__91250,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91259 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91259) : f.call(null,G__91259));
})(),i));

var G__91926 = (i__91249 + (1));
i__91249 = G__91926;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91250),clojure$math$combinatorics$drop_permutations_$_iter__91247(cljs.core.chunk_rest(s__91248__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91250),null);
}
} else {
var i = cljs.core.first(s__91248__$2);
return cljs.core.cons(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91260 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91260) : f.call(null,G__91260));
})(),i),clojure$math$combinatorics$drop_permutations_$_iter__91247(cljs.core.rest(s__91248__$2)));
}
} else {
return null;
}
break;
}
});})(v,f))
,null,null));
});})(v,f))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(v)));
})());
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.map,v),clojure.math.combinatorics.vec_lex_permutations(clojure.math.combinatorics.nth_permutation_duplicates(indices,n)));
}
}

}
}
});
clojure.math.combinatorics.n_take_k = (function clojure$math$combinatorics$n_take_k(n,k){
while(true){
if((k < (0))){
return (0);
} else {
if((k > n)){
return (0);
} else {
if((k === (0))){
return (1);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(k,(1))){
return n;
} else {
if((k > cljs.core.quot(n,(2)))){
var G__91928 = n;
var G__91929 = (n - k);
n = G__91928;
k = G__91929;
continue;
} else {
return (cljs.core.apply.cljs$core$IFn$_invoke$arity$2(clojure.math.combinatorics._STAR__SINGLEQUOTE_,cljs.core.range.cljs$core$IFn$_invoke$arity$2(((n - k) + (1)),(n + (1)))) / cljs.core.apply.cljs$core$IFn$_invoke$arity$2(clojure.math.combinatorics._STAR__SINGLEQUOTE_,cljs.core.range.cljs$core$IFn$_invoke$arity$2((1),(k + (1)))));

}
}
}
}
}
break;
}
});
clojure.math.combinatorics.count_combinations_from_frequencies = (function clojure$math$combinatorics$count_combinations_from_frequencies(freqs,t){
var counts = cljs.core.vals(freqs);
var sum = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,counts);
if((t === (0))){
return (1);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,(1))){
return cljs.core.count(freqs);
} else {
if(cljs.core.every_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [(1),null], null), null),counts)){
return clojure.math.combinatorics.n_take_k(cljs.core.count(freqs),t);
} else {
if((t > sum)){
return (0);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,sum)){
return (1);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.count(freqs),(1))){
return (1);
} else {
var new_freqs = clojure.math.combinatorics.dec_key(freqs,cljs.core.first(cljs.core.keys(freqs)));
var G__91268 = (function (){var G__91270 = new_freqs;
var G__91271 = (t - (1));
return (clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2(G__91270,G__91271) : clojure.math.combinatorics.count_combinations_from_frequencies.call(null,G__91270,G__91271));
})();
var G__91269 = (function (){var G__91272 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(freqs,cljs.core.first(cljs.core.keys(freqs)));
var G__91273 = t;
return (clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2(G__91272,G__91273) : clojure.math.combinatorics.count_combinations_from_frequencies.call(null,G__91272,G__91273));
})();
return (clojure.math.combinatorics._PLUS__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._PLUS__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(G__91268,G__91269) : clojure.math.combinatorics._PLUS__SINGLEQUOTE_.call(null,G__91268,G__91269));

}
}
}
}
}
}
});
/**
 * We need an internal version that doesn't memoize each call to count-combinations-from-frequencies
 * so that we can memoize over a series of calls.
 */
clojure.math.combinatorics.count_combinations_unmemoized = (function clojure$math$combinatorics$count_combinations_unmemoized(items,t){
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
return clojure.math.combinatorics.n_take_k(cljs.core.count(items),t);
} else {
return (clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2(cljs.core.frequencies(items),t) : clojure.math.combinatorics.count_combinations_from_frequencies.call(null,cljs.core.frequencies(items),t));
}
});
/**
 * (count (combinations items t)) but computed more directly
 */
clojure.math.combinatorics.count_combinations = (function clojure$math$combinatorics$count_combinations(items,t){
var count_combinations_from_frequencies_orig_val__91275 = clojure.math.combinatorics.count_combinations_from_frequencies;
var count_combinations_from_frequencies_temp_val__91276 = cljs.core.memoize(clojure.math.combinatorics.count_combinations_from_frequencies);
clojure.math.combinatorics.count_combinations_from_frequencies = count_combinations_from_frequencies_temp_val__91276;

try{return clojure.math.combinatorics.count_combinations_unmemoized(items,t);
}finally {clojure.math.combinatorics.count_combinations_from_frequencies = count_combinations_from_frequencies_orig_val__91275;
}});
clojure.math.combinatorics.expt_int = (function clojure$math$combinatorics$expt_int(base,pow){
var n = pow;
var y = (1);
var z = base;
while(true){
var t = cljs.core.even_QMARK_(n);
var n__$1 = cljs.core.quot(n,(2));
if(t){
var G__91935 = n__$1;
var G__91936 = y;
var G__91937 = (clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(z,z) : clojure.math.combinatorics._STAR__SINGLEQUOTE_.call(null,z,z));
n = G__91935;
y = G__91936;
z = G__91937;
continue;
} else {
if((n__$1 === (0))){
return (clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(z,y) : clojure.math.combinatorics._STAR__SINGLEQUOTE_.call(null,z,y));
} else {
var G__91938 = n__$1;
var G__91939 = (clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(z,y) : clojure.math.combinatorics._STAR__SINGLEQUOTE_.call(null,z,y));
var G__91940 = (clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(z,z) : clojure.math.combinatorics._STAR__SINGLEQUOTE_.call(null,z,z));
n = G__91938;
y = G__91939;
z = G__91940;
continue;

}
}
break;
}
});
clojure.math.combinatorics.count_subsets_unmemoized = (function clojure$math$combinatorics$count_subsets_unmemoized(items){
if(cljs.core.empty_QMARK_(items)){
return (1);
} else {
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
return clojure.math.combinatorics.expt_int((2),cljs.core.count(items));
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(clojure.math.combinatorics._PLUS__SINGLEQUOTE_,(function (){var iter__4523__auto__ = (function clojure$math$combinatorics$count_subsets_unmemoized_$_iter__91292(s__91293){
return (new cljs.core.LazySeq(null,(function (){
var s__91293__$1 = s__91293;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91293__$1);
if(temp__5735__auto__){
var s__91293__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91293__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91293__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91295 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91294 = (0);
while(true){
if((i__91294 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91294);
cljs.core.chunk_append(b__91295,clojure.math.combinatorics.count_combinations_unmemoized(items,i));

var G__91941 = (i__91294 + (1));
i__91294 = G__91941;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91295),clojure$math$combinatorics$count_subsets_unmemoized_$_iter__91292(cljs.core.chunk_rest(s__91293__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91295),null);
}
} else {
var i = cljs.core.first(s__91293__$2);
return cljs.core.cons(clojure.math.combinatorics.count_combinations_unmemoized(items,i),clojure$math$combinatorics$count_subsets_unmemoized_$_iter__91292(cljs.core.rest(s__91293__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2((0),(cljs.core.count(items) + (1))));
})());

}
}
});
/**
 * (count (subsets items)) but computed more directly
 */
clojure.math.combinatorics.count_subsets = (function clojure$math$combinatorics$count_subsets(items){
var count_combinations_from_frequencies_orig_val__91300 = clojure.math.combinatorics.count_combinations_from_frequencies;
var count_combinations_from_frequencies_temp_val__91301 = cljs.core.memoize(clojure.math.combinatorics.count_combinations_from_frequencies);
clojure.math.combinatorics.count_combinations_from_frequencies = count_combinations_from_frequencies_temp_val__91301;

try{return clojure.math.combinatorics.count_subsets_unmemoized(items);
}finally {clojure.math.combinatorics.count_combinations_from_frequencies = count_combinations_from_frequencies_orig_val__91300;
}});
/**
 * The nth element of the sequence of t-combinations of items,
 * where items is a collection of distinct elements
 */
clojure.math.combinatorics.nth_combination_distinct = (function clojure$math$combinatorics$nth_combination_distinct(items,t,n){
var comb = cljs.core.PersistentVector.EMPTY;
var items__$1 = items;
var t__$1 = t;
var n__$1 = n;
while(true){
if((((n__$1 === (0))) || (cljs.core.empty_QMARK_(items__$1)))){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(comb,cljs.core.take.cljs$core$IFn$_invoke$arity$2(t__$1,items__$1));
} else {
var dc_dt = clojure.math.combinatorics.n_take_k((cljs.core.count(items__$1) - (1)),(t__$1 - (1)));
if((n__$1 < dc_dt)){
var G__91943 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(comb,cljs.core.first(items__$1));
var G__91944 = cljs.core.rest(items__$1);
var G__91945 = (t__$1 - (1));
var G__91946 = n__$1;
comb = G__91943;
items__$1 = G__91944;
t__$1 = G__91945;
n__$1 = G__91946;
continue;
} else {
var G__91949 = comb;
var G__91950 = cljs.core.rest(items__$1);
var G__91951 = t__$1;
var G__91952 = (n__$1 - dc_dt);
comb = G__91949;
items__$1 = G__91950;
t__$1 = G__91951;
n__$1 = G__91952;
continue;
}
}
break;
}
});
/**
 * The nth element of the sequence of t-combinations of the multiset
 * represented by freqs
 */
clojure.math.combinatorics.nth_combination_freqs = (function clojure$math$combinatorics$nth_combination_freqs(freqs,t,n){
var comb = cljs.core.PersistentVector.EMPTY;
var freqs__$1 = freqs;
var t__$1 = t;
var n__$1 = n;
while(true){
if((((n__$1 === (0))) || (cljs.core.empty_QMARK_(freqs__$1)))){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(comb,cljs.core.take.cljs$core$IFn$_invoke$arity$2(t__$1,clojure.math.combinatorics.join((function (){var iter__4523__auto__ = ((function (comb,freqs__$1,t__$1,n__$1){
return (function clojure$math$combinatorics$nth_combination_freqs_$_iter__91329(s__91330){
return (new cljs.core.LazySeq(null,((function (comb,freqs__$1,t__$1,n__$1){
return (function (){
var s__91330__$1 = s__91330;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91330__$1);
if(temp__5735__auto__){
var s__91330__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91330__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91330__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91332 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91331 = (0);
while(true){
if((i__91331 < size__4522__auto__)){
var vec__91333 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91331);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91333,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91333,(1),null);
cljs.core.chunk_append(b__91332,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(v,k));

var G__91955 = (i__91331 + (1));
i__91331 = G__91955;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91332),clojure$math$combinatorics$nth_combination_freqs_$_iter__91329(cljs.core.chunk_rest(s__91330__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91332),null);
}
} else {
var vec__91336 = cljs.core.first(s__91330__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91336,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91336,(1),null);
return cljs.core.cons(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(v,k),clojure$math$combinatorics$nth_combination_freqs_$_iter__91329(cljs.core.rest(s__91330__$2)));
}
} else {
return null;
}
break;
}
});})(comb,freqs__$1,t__$1,n__$1))
,null,null));
});})(comb,freqs__$1,t__$1,n__$1))
;
return iter__4523__auto__(freqs__$1);
})())));
} else {
var first_key = cljs.core.first(cljs.core.keys(freqs__$1));
var remove_one_key = clojure.math.combinatorics.dec_key(freqs__$1,first_key);
var dc_dt = (clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics.count_combinations_from_frequencies.cljs$core$IFn$_invoke$arity$2(remove_one_key,(t__$1 - (1))) : clojure.math.combinatorics.count_combinations_from_frequencies.call(null,remove_one_key,(t__$1 - (1))));
if((n__$1 < dc_dt)){
var G__91956 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(comb,first_key);
var G__91957 = remove_one_key;
var G__91958 = (t__$1 - (1));
var G__91959 = n__$1;
comb = G__91956;
freqs__$1 = G__91957;
t__$1 = G__91958;
n__$1 = G__91959;
continue;
} else {
var G__91960 = comb;
var G__91961 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(freqs__$1,first_key);
var G__91962 = t__$1;
var G__91963 = (n__$1 - dc_dt);
comb = G__91960;
freqs__$1 = G__91961;
t__$1 = G__91962;
n__$1 = G__91963;
continue;
}
}
break;
}
});
/**
 * The nth element of the sequence of t-combinations of items
 */
clojure.math.combinatorics.nth_combination = (function clojure$math$combinatorics$nth_combination(items,t,n){
if((n < clojure.math.combinatorics.count_combinations(items,t))){
} else {
throw (new Error(["Assert failed: ",cljs.core.print_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([n,"is too large. Input has only",clojure.math.combinatorics.count_combinations_unmemoized(items,t),"combinations."], 0)),"\n","(< n (count-combinations items t))"].join('')));
}

if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
return clojure.math.combinatorics.nth_combination_distinct(items,t,n);
} else {
var count_combinations_from_frequencies_orig_val__91345 = clojure.math.combinatorics.count_combinations_from_frequencies;
var count_combinations_from_frequencies_temp_val__91346 = cljs.core.memoize(clojure.math.combinatorics.count_combinations_from_frequencies);
clojure.math.combinatorics.count_combinations_from_frequencies = count_combinations_from_frequencies_temp_val__91346;

try{var v = cljs.core.vec(cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(items));
var f = cljs.core.frequencies(items);
var indices = clojure.math.combinatorics.join((function (){var iter__4523__auto__ = ((function (v,f,count_combinations_from_frequencies_orig_val__91345,count_combinations_from_frequencies_temp_val__91346){
return (function clojure$math$combinatorics$nth_combination_$_iter__91347(s__91348){
return (new cljs.core.LazySeq(null,((function (v,f,count_combinations_from_frequencies_orig_val__91345,count_combinations_from_frequencies_temp_val__91346){
return (function (){
var s__91348__$1 = s__91348;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91348__$1);
if(temp__5735__auto__){
var s__91348__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91348__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91348__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91350 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91349 = (0);
while(true){
if((i__91349 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91349);
cljs.core.chunk_append(b__91350,cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91351 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91351) : f.call(null,G__91351));
})(),i));

var G__91965 = (i__91349 + (1));
i__91349 = G__91965;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91350),clojure$math$combinatorics$nth_combination_$_iter__91347(cljs.core.chunk_rest(s__91348__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91350),null);
}
} else {
var i = cljs.core.first(s__91348__$2);
return cljs.core.cons(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2((function (){var G__91352 = (v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91352) : f.call(null,G__91352));
})(),i),clojure$math$combinatorics$nth_combination_$_iter__91347(cljs.core.rest(s__91348__$2)));
}
} else {
return null;
}
break;
}
});})(v,f,count_combinations_from_frequencies_orig_val__91345,count_combinations_from_frequencies_temp_val__91346))
,null,null));
});})(v,f,count_combinations_from_frequencies_orig_val__91345,count_combinations_from_frequencies_temp_val__91346))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(v)));
})());
var indices_freqs = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_map(),cljs.core.frequencies(indices));
return cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(v,clojure.math.combinatorics.nth_combination_freqs(indices_freqs,t,n)));
}finally {clojure.math.combinatorics.count_combinations_from_frequencies = count_combinations_from_frequencies_orig_val__91345;
}}
});
clojure.math.combinatorics.nth_subset = (function clojure$math$combinatorics$nth_subset(items,n){
if((n < clojure.math.combinatorics.count_subsets(items))){
} else {
throw (new Error(["Assert failed: ",cljs.core.print_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([n,"is too large. Input has only",clojure.math.combinatorics.count_subsets(items),"subsets."], 0)),"\n","(< n (count-subsets items))"].join('')));
}

var size = (0);
var n__$1 = n;
while(true){
var num_combinations = clojure.math.combinatorics.count_combinations(items,size);
if((n__$1 < num_combinations)){
return clojure.math.combinatorics.nth_combination(items,size,n__$1);
} else {
var G__91971 = (size + (1));
var G__91972 = (n__$1 - num_combinations);
size = G__91971;
n__$1 = G__91972;
continue;
}
break;
}
});
/**
 * The opposite of nth, i.e., from an item in a list, find the n
 */
clojure.math.combinatorics.list_index = (function clojure$math$combinatorics$list_index(l,item){
var l__$1 = l;
var n = (0);
while(true){
if(cljs.core.seq(l__$1)){
} else {
throw (new Error("Assert failed: (seq l)"));
}

if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(item,cljs.core.first(l__$1))){
return n;
} else {
var G__91973 = cljs.core.rest(l__$1);
var G__91974 = (n + (1));
l__$1 = G__91973;
n = G__91974;
continue;
}
break;
}
});
clojure.math.combinatorics.permutation_index_distinct = (function clojure$math$combinatorics$permutation_index_distinct(l){
var l__$1 = l;
var index = (0);
var n = (cljs.core.count(l__$1) - (1));
while(true){
if(cljs.core.empty_QMARK_(l__$1)){
return index;
} else {
var G__91976 = cljs.core.rest(l__$1);
var G__91977 = (function (){var G__91372 = index;
var G__91373 = (function (){var G__91374 = clojure.math.combinatorics.factorial(n);
var G__91375 = clojure.math.combinatorics.list_index(cljs.core.sort.cljs$core$IFn$_invoke$arity$1(l__$1),cljs.core.first(l__$1));
return (clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._STAR__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(G__91374,G__91375) : clojure.math.combinatorics._STAR__SINGLEQUOTE_.call(null,G__91374,G__91375));
})();
return (clojure.math.combinatorics._PLUS__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2 ? clojure.math.combinatorics._PLUS__SINGLEQUOTE_.cljs$core$IFn$_invoke$arity$2(G__91372,G__91373) : clojure.math.combinatorics._PLUS__SINGLEQUOTE_.call(null,G__91372,G__91373));
})();
var G__91978 = (n - (1));
l__$1 = G__91976;
index = G__91977;
n = G__91978;
continue;
}
break;
}
});
clojure.math.combinatorics.permutation_index_duplicates = (function clojure$math$combinatorics$permutation_index_duplicates(l){
var l__$1 = l;
var index = (0);
var freqs = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_map(),cljs.core.frequencies(l__$1));
while(true){
if(cljs.core.empty_QMARK_(l__$1)){
return index;
} else {
var G__91979 = cljs.core.rest(l__$1);
var G__91980 = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(clojure.math.combinatorics._PLUS__SINGLEQUOTE_,index,(function (){var iter__4523__auto__ = ((function (l__$1,index,freqs){
return (function clojure$math$combinatorics$permutation_index_duplicates_$_iter__91417(s__91418){
return (new cljs.core.LazySeq(null,((function (l__$1,index,freqs){
return (function (){
var s__91418__$1 = s__91418;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91418__$1);
if(temp__5735__auto__){
var s__91418__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91418__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91418__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91420 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91419 = (0);
while(true){
if((i__91419 < size__4522__auto__)){
var k = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91419);
cljs.core.chunk_append(b__91420,clojure.math.combinatorics.count_permutations_from_frequencies(clojure.math.combinatorics.dec_key(freqs,k)));

var G__91984 = (i__91419 + (1));
i__91419 = G__91984;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91420),clojure$math$combinatorics$permutation_index_duplicates_$_iter__91417(cljs.core.chunk_rest(s__91418__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91420),null);
}
} else {
var k = cljs.core.first(s__91418__$2);
return cljs.core.cons(clojure.math.combinatorics.count_permutations_from_frequencies(clojure.math.combinatorics.dec_key(freqs,k)),clojure$math$combinatorics$permutation_index_duplicates_$_iter__91417(cljs.core.rest(s__91418__$2)));
}
} else {
return null;
}
break;
}
});})(l__$1,index,freqs))
,null,null));
});})(l__$1,index,freqs))
;
return iter__4523__auto__(cljs.core.take_while.cljs$core$IFn$_invoke$arity$2(((function (l__$1,index,freqs,iter__4523__auto__){
return (function (p1__91380_SHARP_){
return (cljs.core.compare(p1__91380_SHARP_,cljs.core.first(l__$1)) < (0));
});})(l__$1,index,freqs,iter__4523__auto__))
,cljs.core.keys(freqs)));
})());
var G__91981 = clojure.math.combinatorics.dec_key(freqs,cljs.core.first(l__$1));
l__$1 = G__91979;
index = G__91980;
freqs = G__91981;
continue;
}
break;
}
});
/**
 * Input must be a sortable collection of items.  Returns the n such that
 *  (nth-permutation (sort items) n) is items.
 */
clojure.math.combinatorics.permutation_index = (function clojure$math$combinatorics$permutation_index(items){
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
return clojure.math.combinatorics.permutation_index_distinct(items);
} else {
return clojure.math.combinatorics.permutation_index_duplicates(items);
}
});
clojure.math.combinatorics.update = (function clojure$math$combinatorics$update(vec,index,f){
var item = (vec.cljs$core$IFn$_invoke$arity$1 ? vec.cljs$core$IFn$_invoke$arity$1(index) : vec.call(null,index));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(vec,index,(f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(item) : f.call(null,item)));
});
clojure.math.combinatorics.reify_bool = (function clojure$math$combinatorics$reify_bool(statement){
if(cljs.core.truth_(statement)){
return (1);
} else {
return (0);
}
});
clojure.math.combinatorics.init = (function clojure$math$combinatorics$init(n,s){
if(cljs.core.truth_(s)){
return cljs.core.vec((function (){var iter__4523__auto__ = (function clojure$math$combinatorics$init_$_iter__91449(s__91450){
return (new cljs.core.LazySeq(null,(function (){
var s__91450__$1 = s__91450;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91450__$1);
if(temp__5735__auto__){
var s__91450__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91450__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91450__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91452 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91451 = (0);
while(true){
if((i__91451 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91451);
cljs.core.chunk_append(b__91452,(function (){var x__4219__auto__ = (0);
var y__4220__auto__ = (i - ((n - s) - (-1)));
return ((x__4219__auto__ > y__4220__auto__) ? x__4219__auto__ : y__4220__auto__);
})());

var G__91988 = (i__91451 + (1));
i__91451 = G__91988;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91452),clojure$math$combinatorics$init_$_iter__91449(cljs.core.chunk_rest(s__91450__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91452),null);
}
} else {
var i = cljs.core.first(s__91450__$2);
return cljs.core.cons((function (){var x__4219__auto__ = (0);
var y__4220__auto__ = (i - ((n - s) - (-1)));
return ((x__4219__auto__ > y__4220__auto__) ? x__4219__auto__ : y__4220__auto__);
})(),clojure$math$combinatorics$init_$_iter__91449(cljs.core.rest(s__91450__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2((1),(n + (1))));
})());
} else {
return cljs.core.vec(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(n,(0)));
}
});
clojure.math.combinatorics.growth_strings_H = (function clojure$math$combinatorics$growth_strings_H(var_args){
var G__91465 = arguments.length;
switch (G__91465) {
case 3:
return clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 5:
return clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$3 = (function (n,r,s){
return clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$5(n,clojure.math.combinatorics.init(n,s),cljs.core.vec(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(n,(1))),r,s);
});

clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$5 = (function (n,a,b,r,s){
return cljs.core.cons(a,(new cljs.core.LazySeq(null,(function (){
if((function (){var and__4120__auto__ = (cljs.core.peek(b) > cljs.core.peek(a));
if(and__4120__auto__){
if(cljs.core.truth_(r)){
return (cljs.core.peek(a) < (r - (1)));
} else {
return true;
}
} else {
return and__4120__auto__;
}
})()){
return clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$5(n,clojure.math.combinatorics.update(a,(n - (1)),cljs.core.inc),b,r,s);
} else {
var j = (function (){var j = (n - (2));
while(true){
if((function (){var and__4120__auto__ = ((a.cljs$core$IFn$_invoke$arity$1 ? a.cljs$core$IFn$_invoke$arity$1(j) : a.call(null,j)) < (b.cljs$core$IFn$_invoke$arity$1 ? b.cljs$core$IFn$_invoke$arity$1(j) : b.call(null,j)));
if(and__4120__auto__){
var and__4120__auto____$1 = (cljs.core.truth_(r)?((a.cljs$core$IFn$_invoke$arity$1 ? a.cljs$core$IFn$_invoke$arity$1(j) : a.call(null,j)) < (r - (1))):true);
if(and__4120__auto____$1){
if(cljs.core.truth_(s)){
return (((s - (b.cljs$core$IFn$_invoke$arity$1 ? b.cljs$core$IFn$_invoke$arity$1(j) : b.call(null,j))) - clojure.math.combinatorics.reify_bool((((a.cljs$core$IFn$_invoke$arity$1 ? a.cljs$core$IFn$_invoke$arity$1(j) : a.call(null,j)) + (1)) === (b.cljs$core$IFn$_invoke$arity$1 ? b.cljs$core$IFn$_invoke$arity$1(j) : b.call(null,j))))) <= (n - j));
} else {
return true;
}
} else {
return and__4120__auto____$1;
}
} else {
return and__4120__auto__;
}
})()){
return j;
} else {
var G__91995 = (j - (1));
j = G__91995;
continue;
}
break;
}
})();
if((j === (0))){
return cljs.core.List.EMPTY;
} else {
var a__$1 = clojure.math.combinatorics.update(a,j,cljs.core.inc);
var x = (cljs.core.truth_(s)?(s - ((b.cljs$core$IFn$_invoke$arity$1 ? b.cljs$core$IFn$_invoke$arity$1(j) : b.call(null,j)) + clojure.math.combinatorics.reify_bool(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((a__$1.cljs$core$IFn$_invoke$arity$1 ? a__$1.cljs$core$IFn$_invoke$arity$1(j) : a__$1.call(null,j)),(b.cljs$core$IFn$_invoke$arity$1 ? b.cljs$core$IFn$_invoke$arity$1(j) : b.call(null,j)))))):null);
var vec__91482 = (function (){var a__$2 = a__$1;
var b__$1 = b;
var i = (j + (1));
var current_max = ((b__$1.cljs$core$IFn$_invoke$arity$1 ? b__$1.cljs$core$IFn$_invoke$arity$1(j) : b__$1.call(null,j)) + clojure.math.combinatorics.reify_bool(((b__$1.cljs$core$IFn$_invoke$arity$1 ? b__$1.cljs$core$IFn$_invoke$arity$1(j) : b__$1.call(null,j)) === (a__$2.cljs$core$IFn$_invoke$arity$1 ? a__$2.cljs$core$IFn$_invoke$arity$1(j) : a__$2.call(null,j)))));
while(true){
if((i === n)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [a__$2,b__$1], null);
} else {
if(cljs.core.truth_((function (){var and__4120__auto__ = s;
if(cljs.core.truth_(and__4120__auto__)){
return (i > ((n - x) - (1)));
} else {
return and__4120__auto__;
}
})())){
var new_a_i = ((i - n) + s);
var G__92001 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(a__$2,i,new_a_i);
var G__92002 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(b__$1,i,current_max);
var G__92003 = (i + (1));
var G__92004 = (function (){var x__4219__auto__ = current_max;
var y__4220__auto__ = (new_a_i + (1));
return ((x__4219__auto__ > y__4220__auto__) ? x__4219__auto__ : y__4220__auto__);
})();
a__$2 = G__92001;
b__$1 = G__92002;
i = G__92003;
current_max = G__92004;
continue;
} else {
var G__92005 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(a__$2,i,(0));
var G__92006 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(b__$1,i,current_max);
var G__92007 = (i + (1));
var G__92008 = current_max;
a__$2 = G__92005;
b__$1 = G__92006;
i = G__92007;
current_max = G__92008;
continue;

}
}
break;
}
})();
var a__$2 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91482,(0),null);
var b__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91482,(1),null);
return clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$5(n,a__$2,b__$1,r,s);
}
}
}),null,null)));
});

clojure.math.combinatorics.growth_strings_H.cljs$lang$maxFixedArity = 5;

clojure.math.combinatorics.lex_partitions_H = (function clojure$math$combinatorics$lex_partitions_H(var_args){
var args__4736__auto__ = [];
var len__4730__auto___92009 = arguments.length;
var i__4731__auto___92010 = (0);
while(true){
if((i__4731__auto___92010 < len__4730__auto___92009)){
args__4736__auto__.push((arguments[i__4731__auto___92010]));

var G__92011 = (i__4731__auto___92010 + (1));
i__4731__auto___92010 = G__92011;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return clojure.math.combinatorics.lex_partitions_H.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

clojure.math.combinatorics.lex_partitions_H.cljs$core$IFn$_invoke$arity$variadic = (function (N,p__91513){
var map__91517 = p__91513;
var map__91517__$1 = (((((!((map__91517 == null))))?(((((map__91517.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__91517.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__91517):map__91517);
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__91517__$1,new cljs.core.Keyword(null,"min","min",444991522));
var to = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__91517__$1,new cljs.core.Keyword(null,"max","max",61366548));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(N,(0))){
if(((((function (){var or__4131__auto__ = from;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (0);
}
})() <= (0))) && (((0) <= (function (){var or__4131__auto__ = to;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (0);
}
})())))){
return cljs.core.list(cljs.core.List.EMPTY);
} else {
return cljs.core.List.EMPTY;
}
} else {
var from__$1 = (cljs.core.truth_((function (){var and__4120__auto__ = from;
if(cljs.core.truth_(and__4120__auto__)){
return (from <= (1));
} else {
return and__4120__auto__;
}
})())?null:from);
var to__$1 = (cljs.core.truth_((function (){var and__4120__auto__ = to;
if(cljs.core.truth_(and__4120__auto__)){
return (to >= N);
} else {
return and__4120__auto__;
}
})())?null:to);
if((!(((((1) <= (function (){var or__4131__auto__ = from__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})())) && (((((function (){var or__4131__auto__ = from__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})() <= (function (){var or__4131__auto__ = to__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return N;
}
})())) && (((function (){var or__4131__auto__ = to__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return N;
}
})() <= N)))))))){
return cljs.core.List.EMPTY;
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(N,(0))){
return cljs.core.list(cljs.core.List.EMPTY);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(N,(1))){
return cljs.core.list(cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0)], null)));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(to__$1,(1))){
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$1((new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$1((new cljs.core.List(null,cljs.core.range.cljs$core$IFn$_invoke$arity$1(N),null,(1),null))))),null,(1),null)))));
} else {
var growth_strings = clojure.math.combinatorics.growth_strings_H.cljs$core$IFn$_invoke$arity$3(N,to__$1,from__$1);
var iter__4523__auto__ = ((function (growth_strings,from__$1,to__$1,map__91517,map__91517__$1,from,to){
return (function clojure$math$combinatorics$iter__91540(s__91541){
return (new cljs.core.LazySeq(null,((function (growth_strings,from__$1,to__$1,map__91517,map__91517__$1,from,to){
return (function (){
var s__91541__$1 = s__91541;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91541__$1);
if(temp__5735__auto__){
var s__91541__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91541__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91541__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91543 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91542 = (0);
while(true){
if((i__91542 < size__4522__auto__)){
var growth_string = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91542);
var groups = cljs.core.group_by(growth_string,cljs.core.range.cljs$core$IFn$_invoke$arity$1(N));
cljs.core.chunk_append(b__91543,cljs.core.map.cljs$core$IFn$_invoke$arity$2(groups,cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(groups))));

var G__92017 = (i__91542 + (1));
i__91542 = G__92017;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91543),clojure$math$combinatorics$iter__91540(cljs.core.chunk_rest(s__91541__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91543),null);
}
} else {
var growth_string = cljs.core.first(s__91541__$2);
var groups = cljs.core.group_by(growth_string,cljs.core.range.cljs$core$IFn$_invoke$arity$1(N));
return cljs.core.cons(cljs.core.map.cljs$core$IFn$_invoke$arity$2(groups,cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.count(groups))),clojure$math$combinatorics$iter__91540(cljs.core.rest(s__91541__$2)));
}
} else {
return null;
}
break;
}
});})(growth_strings,from__$1,to__$1,map__91517,map__91517__$1,from,to))
,null,null));
});})(growth_strings,from__$1,to__$1,map__91517,map__91517__$1,from,to))
;
return iter__4523__auto__(growth_strings);

}
}
}
}
}
});

clojure.math.combinatorics.lex_partitions_H.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
clojure.math.combinatorics.lex_partitions_H.cljs$lang$applyTo = (function (seq91510){
var G__91511 = cljs.core.first(seq91510);
var seq91510__$1 = cljs.core.next(seq91510);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__91511,seq91510__$1);
});

clojure.math.combinatorics.partitions_H = (function clojure$math$combinatorics$partitions_H(var_args){
var args__4736__auto__ = [];
var len__4730__auto___92024 = arguments.length;
var i__4731__auto___92025 = (0);
while(true){
if((i__4731__auto___92025 < len__4730__auto___92024)){
args__4736__auto__.push((arguments[i__4731__auto___92025]));

var G__92026 = (i__4731__auto___92025 + (1));
i__4731__auto___92025 = G__92026;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return clojure.math.combinatorics.partitions_H.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

clojure.math.combinatorics.partitions_H.cljs$core$IFn$_invoke$arity$variadic = (function (items,args){
var items__$1 = cljs.core.vec(items);
var N = cljs.core.count(items__$1);
var lex = cljs.core.apply.cljs$core$IFn$_invoke$arity$3(clojure.math.combinatorics.lex_partitions_H,N,args);
var iter__4523__auto__ = ((function (items__$1,N,lex){
return (function clojure$math$combinatorics$iter__91568(s__91569){
return (new cljs.core.LazySeq(null,((function (items__$1,N,lex){
return (function (){
var s__91569__$1 = s__91569;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91569__$1);
if(temp__5735__auto__){
var s__91569__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91569__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91569__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91571 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91570 = (0);
while(true){
if((i__91570 < size__4522__auto__)){
var parts = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91570);
cljs.core.chunk_append(b__91571,(function (){var iter__4523__auto__ = ((function (i__91570,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function clojure$math$combinatorics$iter__91568_$_iter__91610(s__91611){
return (new cljs.core.LazySeq(null,((function (i__91570,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function (){
var s__91611__$1 = s__91611;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__91611__$1);
if(temp__5735__auto____$1){
var s__91611__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__91611__$2)){
var c__4521__auto____$1 = cljs.core.chunk_first(s__91611__$2);
var size__4522__auto____$1 = cljs.core.count(c__4521__auto____$1);
var b__91613 = cljs.core.chunk_buffer(size__4522__auto____$1);
if((function (){var i__91612 = (0);
while(true){
if((i__91612 < size__4522__auto____$1)){
var part = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto____$1,i__91612);
cljs.core.chunk_append(b__91613,cljs.core.persistent_BANG_(cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (i__91612,i__91570,part,c__4521__auto____$1,size__4522__auto____$1,b__91613,s__91611__$2,temp__5735__auto____$1,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function (v,o){
return cljs.core.conj_BANG_.cljs$core$IFn$_invoke$arity$2(v,(items__$1.cljs$core$IFn$_invoke$arity$1 ? items__$1.cljs$core$IFn$_invoke$arity$1(o) : items__$1.call(null,o)));
});})(i__91612,i__91570,part,c__4521__auto____$1,size__4522__auto____$1,b__91613,s__91611__$2,temp__5735__auto____$1,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
,cljs.core.transient$(cljs.core.PersistentVector.EMPTY),part)));

var G__92039 = (i__91612 + (1));
i__91612 = G__92039;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91613),clojure$math$combinatorics$iter__91568_$_iter__91610(cljs.core.chunk_rest(s__91611__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91613),null);
}
} else {
var part = cljs.core.first(s__91611__$2);
return cljs.core.cons(cljs.core.persistent_BANG_(cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (i__91570,part,s__91611__$2,temp__5735__auto____$1,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function (v,o){
return cljs.core.conj_BANG_.cljs$core$IFn$_invoke$arity$2(v,(items__$1.cljs$core$IFn$_invoke$arity$1 ? items__$1.cljs$core$IFn$_invoke$arity$1(o) : items__$1.call(null,o)));
});})(i__91570,part,s__91611__$2,temp__5735__auto____$1,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
,cljs.core.transient$(cljs.core.PersistentVector.EMPTY),part)),clojure$math$combinatorics$iter__91568_$_iter__91610(cljs.core.rest(s__91611__$2)));
}
} else {
return null;
}
break;
}
});})(i__91570,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
,null,null));
});})(i__91570,parts,c__4521__auto__,size__4522__auto__,b__91571,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
;
return iter__4523__auto__(parts);
})());

var G__92043 = (i__91570 + (1));
i__91570 = G__92043;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91571),clojure$math$combinatorics$iter__91568(cljs.core.chunk_rest(s__91569__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91571),null);
}
} else {
var parts = cljs.core.first(s__91569__$2);
return cljs.core.cons((function (){var iter__4523__auto__ = ((function (parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function clojure$math$combinatorics$iter__91568_$_iter__91626(s__91627){
return (new cljs.core.LazySeq(null,((function (parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function (){
var s__91627__$1 = s__91627;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__91627__$1);
if(temp__5735__auto____$1){
var s__91627__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__91627__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91627__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91629 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91628 = (0);
while(true){
if((i__91628 < size__4522__auto__)){
var part = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91628);
cljs.core.chunk_append(b__91629,cljs.core.persistent_BANG_(cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (i__91628,part,c__4521__auto__,size__4522__auto__,b__91629,s__91627__$2,temp__5735__auto____$1,parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function (v,o){
return cljs.core.conj_BANG_.cljs$core$IFn$_invoke$arity$2(v,(items__$1.cljs$core$IFn$_invoke$arity$1 ? items__$1.cljs$core$IFn$_invoke$arity$1(o) : items__$1.call(null,o)));
});})(i__91628,part,c__4521__auto__,size__4522__auto__,b__91629,s__91627__$2,temp__5735__auto____$1,parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
,cljs.core.transient$(cljs.core.PersistentVector.EMPTY),part)));

var G__92044 = (i__91628 + (1));
i__91628 = G__92044;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91629),clojure$math$combinatorics$iter__91568_$_iter__91626(cljs.core.chunk_rest(s__91627__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91629),null);
}
} else {
var part = cljs.core.first(s__91627__$2);
return cljs.core.cons(cljs.core.persistent_BANG_(cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (part,s__91627__$2,temp__5735__auto____$1,parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex){
return (function (v,o){
return cljs.core.conj_BANG_.cljs$core$IFn$_invoke$arity$2(v,(items__$1.cljs$core$IFn$_invoke$arity$1 ? items__$1.cljs$core$IFn$_invoke$arity$1(o) : items__$1.call(null,o)));
});})(part,s__91627__$2,temp__5735__auto____$1,parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
,cljs.core.transient$(cljs.core.PersistentVector.EMPTY),part)),clojure$math$combinatorics$iter__91568_$_iter__91626(cljs.core.rest(s__91627__$2)));
}
} else {
return null;
}
break;
}
});})(parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
,null,null));
});})(parts,s__91569__$2,temp__5735__auto__,items__$1,N,lex))
;
return iter__4523__auto__(parts);
})(),clojure$math$combinatorics$iter__91568(cljs.core.rest(s__91569__$2)));
}
} else {
return null;
}
break;
}
});})(items__$1,N,lex))
,null,null));
});})(items__$1,N,lex))
;
return iter__4523__auto__(lex);
});

clojure.math.combinatorics.partitions_H.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
clojure.math.combinatorics.partitions_H.cljs$lang$applyTo = (function (seq91555){
var G__91556 = cljs.core.first(seq91555);
var seq91555__$1 = cljs.core.next(seq91555);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__91556,seq91555__$1);
});


clojure.math.combinatorics.multiset_partitions_M = (function clojure$math$combinatorics$multiset_partitions_M(var_args){
var G__91645 = arguments.length;
switch (G__91645) {
case 3:
return clojure.math.combinatorics.multiset_partitions_M.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 11:
return clojure.math.combinatorics.multiset_partitions_M.cljs$core$IFn$_invoke$arity$11((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]),(arguments[(6)]),(arguments[(7)]),(arguments[(8)]),(arguments[(9)]),(arguments[(10)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

clojure.math.combinatorics.multiset_partitions_M.cljs$core$IFn$_invoke$arity$3 = (function (multiset,r,s){
var n = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,cljs.core.vals(multiset));
var m = cljs.core.count(multiset);
var f = cljs.core.PersistentVector.EMPTY;
var c = cljs.core.PersistentVector.EMPTY;
var u = cljs.core.PersistentVector.EMPTY;
var v = cljs.core.PersistentVector.EMPTY;
var vec__91646 = (function (){var j = (0);
var c__$1 = c;
var u__$1 = u;
var v__$1 = v;
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(j,m)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [c__$1,u__$1,v__$1], null);
} else {
var G__92048 = (j + (1));
var G__92049 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(c__$1,j,(j + (1)));
var G__92050 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(u__$1,j,(function (){var G__91651 = (j + (1));
return (multiset.cljs$core$IFn$_invoke$arity$1 ? multiset.cljs$core$IFn$_invoke$arity$1(G__91651) : multiset.call(null,G__91651));
})());
var G__92051 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v__$1,j,(function (){var G__91652 = (j + (1));
return (multiset.cljs$core$IFn$_invoke$arity$1 ? multiset.cljs$core$IFn$_invoke$arity$1(G__91652) : multiset.call(null,G__91652));
})());
j = G__92048;
c__$1 = G__92049;
u__$1 = G__92050;
v__$1 = G__92051;
continue;
}
break;
}
})();
var c__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91646,(0),null);
var u__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91646,(1),null);
var v__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91646,(2),null);
var a = (0);
var b = m;
var l = (0);
var f__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(f,(0),(0),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(1),m], 0));
var stack = cljs.core.List.EMPTY;
return clojure.math.combinatorics.multiset_partitions_M.cljs$core$IFn$_invoke$arity$11(n,m,f__$1,c__$1,u__$1,v__$1,a,b,l,r,s);
});

clojure.math.combinatorics.multiset_partitions_M.cljs$core$IFn$_invoke$arity$11 = (function (n,m,f,c,u,v,a,b,l,r,s){
while(true){
var vec__91653 = (function (){var j = a;
var k = b;
var x = false;
var u__$1 = u;
var v__$1 = v;
var c__$1 = c;
while(true){
if((j >= b)){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [u__$1,v__$1,c__$1,j,k], null);
} else {
var u__$2 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(u__$1,k,((u__$1.cljs$core$IFn$_invoke$arity$1 ? u__$1.cljs$core$IFn$_invoke$arity$1(j) : u__$1.call(null,j)) - (v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(j) : v__$1.call(null,j))));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((u__$2.cljs$core$IFn$_invoke$arity$1 ? u__$2.cljs$core$IFn$_invoke$arity$1(k) : u__$2.call(null,k)),(0))){
var G__92052 = (j + (1));
var G__92053 = k;
var G__92054 = true;
var G__92055 = u__$2;
var G__92056 = v__$1;
var G__92057 = c__$1;
j = G__92052;
k = G__92053;
x = G__92054;
u__$1 = G__92055;
v__$1 = G__92056;
c__$1 = G__92057;
continue;
} else {
if((!(x))){
var c__$2 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(c__$1,k,(c__$1.cljs$core$IFn$_invoke$arity$1 ? c__$1.cljs$core$IFn$_invoke$arity$1(j) : c__$1.call(null,j)));
var v__$2 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v__$1,k,(function (){var x__4222__auto__ = (v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(j) : v__$1.call(null,j));
var y__4223__auto__ = (u__$2.cljs$core$IFn$_invoke$arity$1 ? u__$2.cljs$core$IFn$_invoke$arity$1(k) : u__$2.call(null,k));
return ((x__4222__auto__ < y__4223__auto__) ? x__4222__auto__ : y__4223__auto__);
})());
var x__$1 = ((u__$2.cljs$core$IFn$_invoke$arity$1 ? u__$2.cljs$core$IFn$_invoke$arity$1(k) : u__$2.call(null,k)) < (v__$2.cljs$core$IFn$_invoke$arity$1 ? v__$2.cljs$core$IFn$_invoke$arity$1(j) : v__$2.call(null,j)));
var k__$1 = (k + (1));
var j__$1 = (j + (1));
var G__92062 = j__$1;
var G__92063 = k__$1;
var G__92064 = x__$1;
var G__92065 = u__$2;
var G__92066 = v__$2;
var G__92067 = c__$2;
j = G__92062;
k = G__92063;
x = G__92064;
u__$1 = G__92065;
v__$1 = G__92066;
c__$1 = G__92067;
continue;
} else {
var c__$2 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(c__$1,k,(c__$1.cljs$core$IFn$_invoke$arity$1 ? c__$1.cljs$core$IFn$_invoke$arity$1(j) : c__$1.call(null,j)));
var v__$2 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v__$1,k,(u__$2.cljs$core$IFn$_invoke$arity$1 ? u__$2.cljs$core$IFn$_invoke$arity$1(k) : u__$2.call(null,k)));
var k__$1 = (k + (1));
var j__$1 = (j + (1));
var G__92070 = j__$1;
var G__92071 = k__$1;
var G__92072 = x;
var G__92073 = u__$2;
var G__92074 = v__$2;
var G__92075 = c__$2;
j = G__92070;
k = G__92071;
x = G__92072;
u__$1 = G__92073;
v__$1 = G__92074;
c__$1 = G__92075;
continue;
}
}
}
break;
}
})();
var u__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91653,(0),null);
var v__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91653,(1),null);
var c__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91653,(2),null);
var j = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91653,(3),null);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91653,(4),null);
if(cljs.core.truth_((function (){var and__4120__auto__ = r;
if(cljs.core.truth_(and__4120__auto__)){
return (((k > b)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(l,(r - (1)))));
} else {
return and__4120__auto__;
}
})())){
return (clojure.math.combinatorics.m5.cljs$core$IFn$_invoke$arity$11 ? clojure.math.combinatorics.m5.cljs$core$IFn$_invoke$arity$11(n,m,f,c__$1,u__$1,v__$1,a,b,l,r,s) : clojure.math.combinatorics.m5.call(null,n,m,f,c__$1,u__$1,v__$1,a,b,l,r,s));
} else {
if(cljs.core.truth_((function (){var and__4120__auto__ = s;
if(cljs.core.truth_(and__4120__auto__)){
return (((k <= b)) && (((l + (1)) < s)));
} else {
return and__4120__auto__;
}
})())){
return (clojure.math.combinatorics.m5.cljs$core$IFn$_invoke$arity$11 ? clojure.math.combinatorics.m5.cljs$core$IFn$_invoke$arity$11(n,m,f,c__$1,u__$1,v__$1,a,b,l,r,s) : clojure.math.combinatorics.m5.call(null,n,m,f,c__$1,u__$1,v__$1,a,b,l,r,s));
} else {
if((k > b)){
var a__$1 = b;
var b__$1 = k;
var l__$1 = (l + (1));
var f__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(f,(l__$1 + (1)),b__$1);
var G__92078 = n;
var G__92079 = m;
var G__92080 = f__$1;
var G__92081 = c__$1;
var G__92082 = u__$1;
var G__92083 = v__$1;
var G__92084 = a__$1;
var G__92085 = b__$1;
var G__92086 = l__$1;
var G__92087 = r;
var G__92088 = s;
n = G__92078;
m = G__92079;
f = G__92080;
c = G__92081;
u = G__92082;
v = G__92083;
a = G__92084;
b = G__92085;
l = G__92086;
r = G__92087;
s = G__92088;
continue;
} else {
var part = (function (){var iter__4523__auto__ = ((function (n,m,f,c,u,v,a,b,l,r,s,vec__91653,u__$1,v__$1,c__$1,j,k){
return (function clojure$math$combinatorics$iter__91678(s__91679){
return (new cljs.core.LazySeq(null,((function (n,m,f,c,u,v,a,b,l,r,s,vec__91653,u__$1,v__$1,c__$1,j,k){
return (function (){
var s__91679__$1 = s__91679;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91679__$1);
if(temp__5735__auto__){
var s__91679__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91679__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91679__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91681 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91680 = (0);
while(true){
if((i__91680 < size__4522__auto__)){
var y = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91680);
cljs.core.chunk_append(b__91681,(function (){var first_col = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(y) : f.call(null,y));
var last_col = ((function (){var G__91682 = (y + (1));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91682) : f.call(null,G__91682));
})() - (1));
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4523__auto__ = ((function (i__91680,n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,c__4521__auto__,size__4522__auto__,b__91681,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k){
return (function clojure$math$combinatorics$iter__91678_$_iter__91683(s__91684){
return (new cljs.core.LazySeq(null,((function (i__91680,n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,c__4521__auto__,size__4522__auto__,b__91681,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k){
return (function (){
var s__91684__$1 = s__91684;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__91684__$1);
if(temp__5735__auto____$1){
var s__91684__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__91684__$2)){
var c__4521__auto____$1 = cljs.core.chunk_first(s__91684__$2);
var size__4522__auto____$1 = cljs.core.count(c__4521__auto____$1);
var b__91686 = cljs.core.chunk_buffer(size__4522__auto____$1);
if((function (){var i__91685 = (0);
while(true){
if((i__91685 < size__4522__auto____$1)){
var z = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto____$1,i__91685);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2((v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z)),(0))){
cljs.core.chunk_append(b__91686,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(c__$1.cljs$core$IFn$_invoke$arity$1 ? c__$1.cljs$core$IFn$_invoke$arity$1(z) : c__$1.call(null,z)),(v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z))], null));

var G__92093 = (i__91685 + (1));
i__91685 = G__92093;
continue;
} else {
var G__92094 = (i__91685 + (1));
i__91685 = G__92094;
continue;
}
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91686),clojure$math$combinatorics$iter__91678_$_iter__91683(cljs.core.chunk_rest(s__91684__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91686),null);
}
} else {
var z = cljs.core.first(s__91684__$2);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2((v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z)),(0))){
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(c__$1.cljs$core$IFn$_invoke$arity$1 ? c__$1.cljs$core$IFn$_invoke$arity$1(z) : c__$1.call(null,z)),(v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z))], null),clojure$math$combinatorics$iter__91678_$_iter__91683(cljs.core.rest(s__91684__$2)));
} else {
var G__92095 = cljs.core.rest(s__91684__$2);
s__91684__$1 = G__92095;
continue;
}
}
} else {
return null;
}
break;
}
});})(i__91680,n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,c__4521__auto__,size__4522__auto__,b__91681,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k))
,null,null));
});})(i__91680,n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,c__4521__auto__,size__4522__auto__,b__91681,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2(first_col,(last_col + (1))));
})());
})());

var G__92098 = (i__91680 + (1));
i__91680 = G__92098;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91681),clojure$math$combinatorics$iter__91678(cljs.core.chunk_rest(s__91679__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91681),null);
}
} else {
var y = cljs.core.first(s__91679__$2);
return cljs.core.cons((function (){var first_col = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(y) : f.call(null,y));
var last_col = ((function (){var G__91695 = (y + (1));
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__91695) : f.call(null,G__91695));
})() - (1));
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4523__auto__ = ((function (n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k){
return (function clojure$math$combinatorics$iter__91678_$_iter__91696(s__91697){
return (new cljs.core.LazySeq(null,((function (n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k){
return (function (){
var s__91697__$1 = s__91697;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__91697__$1);
if(temp__5735__auto____$1){
var s__91697__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__91697__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91697__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91699 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91698 = (0);
while(true){
if((i__91698 < size__4522__auto__)){
var z = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91698);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2((v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z)),(0))){
cljs.core.chunk_append(b__91699,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(c__$1.cljs$core$IFn$_invoke$arity$1 ? c__$1.cljs$core$IFn$_invoke$arity$1(z) : c__$1.call(null,z)),(v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z))], null));

var G__92107 = (i__91698 + (1));
i__91698 = G__92107;
continue;
} else {
var G__92108 = (i__91698 + (1));
i__91698 = G__92108;
continue;
}
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91699),clojure$math$combinatorics$iter__91678_$_iter__91696(cljs.core.chunk_rest(s__91697__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91699),null);
}
} else {
var z = cljs.core.first(s__91697__$2);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2((v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z)),(0))){
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(c__$1.cljs$core$IFn$_invoke$arity$1 ? c__$1.cljs$core$IFn$_invoke$arity$1(z) : c__$1.call(null,z)),(v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(z) : v__$1.call(null,z))], null),clojure$math$combinatorics$iter__91678_$_iter__91696(cljs.core.rest(s__91697__$2)));
} else {
var G__92117 = cljs.core.rest(s__91697__$2);
s__91697__$1 = G__92117;
continue;
}
}
} else {
return null;
}
break;
}
});})(n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k))
,null,null));
});})(n,m,f,c,u,v,a,b,l,r,s,first_col,last_col,y,s__91679__$2,temp__5735__auto__,vec__91653,u__$1,v__$1,c__$1,j,k))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2(first_col,(last_col + (1))));
})());
})(),clojure$math$combinatorics$iter__91678(cljs.core.rest(s__91679__$2)));
}
} else {
return null;
}
break;
}
});})(n,m,f,c,u,v,a,b,l,r,s,vec__91653,u__$1,v__$1,c__$1,j,k))
,null,null));
});})(n,m,f,c,u,v,a,b,l,r,s,vec__91653,u__$1,v__$1,c__$1,j,k))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1((l + (1))));
})();
return cljs.core.cons(part,(new cljs.core.LazySeq(null,((function (n,m,f,c,u,v,a,b,l,r,s,part,vec__91653,u__$1,v__$1,c__$1,j,k){
return (function (){
return (clojure.math.combinatorics.m5.cljs$core$IFn$_invoke$arity$11 ? clojure.math.combinatorics.m5.cljs$core$IFn$_invoke$arity$11(n,m,f,c__$1,u__$1,v__$1,a,b,l,r,s) : clojure.math.combinatorics.m5.call(null,n,m,f,c__$1,u__$1,v__$1,a,b,l,r,s));
});})(n,m,f,c,u,v,a,b,l,r,s,part,vec__91653,u__$1,v__$1,c__$1,j,k))
,null,null)));

}
}
}
break;
}
});

clojure.math.combinatorics.multiset_partitions_M.cljs$lang$maxFixedArity = 11;

clojure.math.combinatorics.m5 = (function clojure$math$combinatorics$m5(n,m,f,c,u,v,a,b,l,r,s){
var j = (function (){var j = (b - (1));
while(true){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2((v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(j) : v.call(null,j)),(0))){
return j;
} else {
var G__92123 = (j - (1));
j = G__92123;
continue;
}
break;
}
})();
if(cljs.core.truth_((function (){var and__4120__auto__ = r;
if(cljs.core.truth_(and__4120__auto__)){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(j,a)) && (((((v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(j) : v.call(null,j)) - (1)) * (r - l)) < (u.cljs$core$IFn$_invoke$arity$1 ? u.cljs$core$IFn$_invoke$arity$1(j) : u.call(null,j)))));
} else {
return and__4120__auto__;
}
})())){
return (clojure.math.combinatorics.m6.cljs$core$IFn$_invoke$arity$11 ? clojure.math.combinatorics.m6.cljs$core$IFn$_invoke$arity$11(n,m,f,c,u,v,a,b,l,r,s) : clojure.math.combinatorics.m6.call(null,n,m,f,c,u,v,a,b,l,r,s));
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(j,a)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(j) : v.call(null,j)),(1))))){
return (clojure.math.combinatorics.m6.cljs$core$IFn$_invoke$arity$11 ? clojure.math.combinatorics.m6.cljs$core$IFn$_invoke$arity$11(n,m,f,c,u,v,a,b,l,r,s) : clojure.math.combinatorics.m6.call(null,n,m,f,c,u,v,a,b,l,r,s));
} else {
var v__$1 = clojure.math.combinatorics.update(v,j,cljs.core.dec);
var diff_uv = (cljs.core.truth_(s)?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,(function (){var iter__4523__auto__ = ((function (v__$1,j){
return (function clojure$math$combinatorics$m5_$_iter__91714(s__91715){
return (new cljs.core.LazySeq(null,((function (v__$1,j){
return (function (){
var s__91715__$1 = s__91715;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91715__$1);
if(temp__5735__auto__){
var s__91715__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91715__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91715__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91717 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91716 = (0);
while(true){
if((i__91716 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91716);
cljs.core.chunk_append(b__91717,((u.cljs$core$IFn$_invoke$arity$1 ? u.cljs$core$IFn$_invoke$arity$1(i) : u.call(null,i)) - (v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(i) : v__$1.call(null,i))));

var G__92134 = (i__91716 + (1));
i__91716 = G__92134;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91717),clojure$math$combinatorics$m5_$_iter__91714(cljs.core.chunk_rest(s__91715__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91717),null);
}
} else {
var i = cljs.core.first(s__91715__$2);
return cljs.core.cons(((u.cljs$core$IFn$_invoke$arity$1 ? u.cljs$core$IFn$_invoke$arity$1(i) : u.call(null,i)) - (v__$1.cljs$core$IFn$_invoke$arity$1 ? v__$1.cljs$core$IFn$_invoke$arity$1(i) : v__$1.call(null,i))),clojure$math$combinatorics$m5_$_iter__91714(cljs.core.rest(s__91715__$2)));
}
} else {
return null;
}
break;
}
});})(v__$1,j))
,null,null));
});})(v__$1,j))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$2(a,(j + (1))));
})()):null);
var v__$2 = (function (){var ks = cljs.core.range.cljs$core$IFn$_invoke$arity$2((j + (1)),b);
var v__$2 = v__$1;
while(true){
if(cljs.core.empty_QMARK_(ks)){
return v__$2;
} else {
var k = cljs.core.first(ks);
var G__92141 = cljs.core.rest(ks);
var G__92142 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v__$2,k,(u.cljs$core$IFn$_invoke$arity$1 ? u.cljs$core$IFn$_invoke$arity$1(k) : u.call(null,k)));
ks = G__92141;
v__$2 = G__92142;
continue;
}
break;
}
})();
var min_partitions_after_this = (cljs.core.truth_(s)?(s - (l + (1))):(0));
var amount_to_dec = (cljs.core.truth_(s)?(function (){var x__4219__auto__ = (0);
var y__4220__auto__ = (min_partitions_after_this - diff_uv);
return ((x__4219__auto__ > y__4220__auto__) ? x__4219__auto__ : y__4220__auto__);
})():(0));
var v__$3 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(amount_to_dec,(0)))?v__$2:(function (){var k_1 = (b - (1));
var v__$3 = v__$2;
var amount = amount_to_dec;
while(true){
var vk = (v__$3.cljs$core$IFn$_invoke$arity$1 ? v__$3.cljs$core$IFn$_invoke$arity$1(k_1) : v__$3.call(null,k_1));
if((amount > vk)){
var G__92147 = (k_1 - (1));
var G__92148 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v__$3,k_1,(0));
var G__92149 = (amount - vk);
k_1 = G__92147;
v__$3 = G__92148;
amount = G__92149;
continue;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v__$3,k_1,(vk - amount));
}
break;
}
})());
return clojure.math.combinatorics.multiset_partitions_M.cljs$core$IFn$_invoke$arity$11(n,m,f,c,u,v__$3,a,b,l,r,s);

}
}
});
clojure.math.combinatorics.m6 = (function clojure$math$combinatorics$m6(n,m,f,c,u,v,a,b,l,r,s){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(l,(0))){
return cljs.core.List.EMPTY;
} else {
var l__$1 = (l - (1));
var b__$1 = a;
var a__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(l__$1) : f.call(null,l__$1));
return clojure.math.combinatorics.m5(n,m,f,c,u,v,a__$1,b__$1,l__$1,r,s);
}
});
clojure.math.combinatorics.partitions_M = (function clojure$math$combinatorics$partitions_M(var_args){
var args__4736__auto__ = [];
var len__4730__auto___92152 = arguments.length;
var i__4731__auto___92153 = (0);
while(true){
if((i__4731__auto___92153 < len__4730__auto___92152)){
args__4736__auto__.push((arguments[i__4731__auto___92153]));

var G__92169 = (i__4731__auto___92153 + (1));
i__4731__auto___92153 = G__92169;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return clojure.math.combinatorics.partitions_M.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

clojure.math.combinatorics.partitions_M.cljs$core$IFn$_invoke$arity$variadic = (function (items,p__91750){
var map__91751 = p__91750;
var map__91751__$1 = (((((!((map__91751 == null))))?(((((map__91751.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__91751.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__91751):map__91751);
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__91751__$1,new cljs.core.Keyword(null,"min","min",444991522));
var to = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__91751__$1,new cljs.core.Keyword(null,"max","max",61366548));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.count(items),(0))){
if(((((function (){var or__4131__auto__ = from;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (0);
}
})() <= (0))) && (((0) <= (function (){var or__4131__auto__ = to;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (0);
}
})())))){
return cljs.core.list(cljs.core.List.EMPTY);
} else {
return cljs.core.List.EMPTY;
}
} else {
var items__$1 = cljs.core.vec(items);
var ditems = cljs.core.vec(cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(items__$1));
var freqs = cljs.core.frequencies(items__$1);
var N = cljs.core.count(items__$1);
var M = cljs.core.count(ditems);
var from__$1 = (cljs.core.truth_((function (){var and__4120__auto__ = from;
if(cljs.core.truth_(and__4120__auto__)){
return (from <= (1));
} else {
return and__4120__auto__;
}
})())?null:from);
var to__$1 = (cljs.core.truth_((function (){var and__4120__auto__ = to;
if(cljs.core.truth_(and__4120__auto__)){
return (to >= N);
} else {
return and__4120__auto__;
}
})())?null:to);
if((!(((((1) <= (function (){var or__4131__auto__ = from__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})())) && (((((function (){var or__4131__auto__ = from__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})() <= (function (){var or__4131__auto__ = to__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return N;
}
})())) && (((function (){var or__4131__auto__ = to__$1;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return N;
}
})() <= N)))))))){
return cljs.core.List.EMPTY;
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(N,(1))){
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$1((new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$1((new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$1((new cljs.core.List(null,cljs.core.first(items__$1),null,(1),null)))))),null,(1),null))))),null,(1),null)))));
} else {
var start_multiset = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4523__auto__ = ((function (items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function clojure$math$combinatorics$iter__91764(s__91765){
return (new cljs.core.LazySeq(null,((function (items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (){
var s__91765__$1 = s__91765;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91765__$1);
if(temp__5735__auto__){
var s__91765__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91765__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91765__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91767 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91766 = (0);
while(true){
if((i__91766 < size__4522__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91766);
var j = (i + (1));
cljs.core.chunk_append(b__91767,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [j,(function (){var G__91774 = (ditems.cljs$core$IFn$_invoke$arity$1 ? ditems.cljs$core$IFn$_invoke$arity$1(i) : ditems.call(null,i));
return (freqs.cljs$core$IFn$_invoke$arity$1 ? freqs.cljs$core$IFn$_invoke$arity$1(G__91774) : freqs.call(null,G__91774));
})()], null));

var G__92210 = (i__91766 + (1));
i__91766 = G__92210;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91767),clojure$math$combinatorics$iter__91764(cljs.core.chunk_rest(s__91765__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91767),null);
}
} else {
var i = cljs.core.first(s__91765__$2);
var j = (i + (1));
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [j,(function (){var G__91782 = (ditems.cljs$core$IFn$_invoke$arity$1 ? ditems.cljs$core$IFn$_invoke$arity$1(i) : ditems.call(null,i));
return (freqs.cljs$core$IFn$_invoke$arity$1 ? freqs.cljs$core$IFn$_invoke$arity$1(G__91782) : freqs.call(null,G__91782));
})()], null),clojure$math$combinatorics$iter__91764(cljs.core.rest(s__91765__$2)));
}
} else {
return null;
}
break;
}
});})(items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,null,null));
});})(items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
;
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1(M));
})());
var parts = clojure.math.combinatorics.multiset_partitions_M.cljs$core$IFn$_invoke$arity$3(start_multiset,to__$1,from__$1);
var iter__4523__auto__ = ((function (start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function clojure$math$combinatorics$iter__91784(s__91785){
return (new cljs.core.LazySeq(null,((function (start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (){
var s__91785__$1 = s__91785;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__91785__$1);
if(temp__5735__auto__){
var s__91785__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__91785__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91785__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91787 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91786 = (0);
while(true){
if((i__91786 < size__4522__auto__)){
var part = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91786);
cljs.core.chunk_append(b__91787,(function (){var iter__4523__auto__ = ((function (i__91786,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function clojure$math$combinatorics$iter__91784_$_iter__91790(s__91791){
return (new cljs.core.LazySeq(null,((function (i__91786,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (){
var s__91791__$1 = s__91791;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__91791__$1);
if(temp__5735__auto____$1){
var s__91791__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__91791__$2)){
var c__4521__auto____$1 = cljs.core.chunk_first(s__91791__$2);
var size__4522__auto____$1 = cljs.core.count(c__4521__auto____$1);
var b__91793 = cljs.core.chunk_buffer(size__4522__auto____$1);
if((function (){var i__91792 = (0);
while(true){
if((i__91792 < size__4522__auto____$1)){
var multiset = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto____$1,i__91792);
cljs.core.chunk_append(b__91793,cljs.core.vec(clojure.math.combinatorics.mapjoin(((function (i__91792,i__91786,multiset,c__4521__auto____$1,size__4522__auto____$1,b__91793,s__91791__$2,temp__5735__auto____$1,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (p__91799){
var vec__91800 = p__91799;
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91800,(0),null);
var numtimes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91800,(1),null);
return cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(numtimes,(function (){var G__91804 = (index - (1));
return (ditems.cljs$core$IFn$_invoke$arity$1 ? ditems.cljs$core$IFn$_invoke$arity$1(G__91804) : ditems.call(null,G__91804));
})());
});})(i__91792,i__91786,multiset,c__4521__auto____$1,size__4522__auto____$1,b__91793,s__91791__$2,temp__5735__auto____$1,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,multiset)));

var G__92243 = (i__91792 + (1));
i__91792 = G__92243;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91793),clojure$math$combinatorics$iter__91784_$_iter__91790(cljs.core.chunk_rest(s__91791__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91793),null);
}
} else {
var multiset = cljs.core.first(s__91791__$2);
return cljs.core.cons(cljs.core.vec(clojure.math.combinatorics.mapjoin(((function (i__91786,multiset,s__91791__$2,temp__5735__auto____$1,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (p__91809){
var vec__91810 = p__91809;
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91810,(0),null);
var numtimes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91810,(1),null);
return cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(numtimes,(function (){var G__91813 = (index - (1));
return (ditems.cljs$core$IFn$_invoke$arity$1 ? ditems.cljs$core$IFn$_invoke$arity$1(G__91813) : ditems.call(null,G__91813));
})());
});})(i__91786,multiset,s__91791__$2,temp__5735__auto____$1,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,multiset)),clojure$math$combinatorics$iter__91784_$_iter__91790(cljs.core.rest(s__91791__$2)));
}
} else {
return null;
}
break;
}
});})(i__91786,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,null,null));
});})(i__91786,part,c__4521__auto__,size__4522__auto__,b__91787,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
;
return iter__4523__auto__(part);
})());

var G__92257 = (i__91786 + (1));
i__91786 = G__92257;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91787),clojure$math$combinatorics$iter__91784(cljs.core.chunk_rest(s__91785__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91787),null);
}
} else {
var part = cljs.core.first(s__91785__$2);
return cljs.core.cons((function (){var iter__4523__auto__ = ((function (part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function clojure$math$combinatorics$iter__91784_$_iter__91814(s__91815){
return (new cljs.core.LazySeq(null,((function (part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (){
var s__91815__$1 = s__91815;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__91815__$1);
if(temp__5735__auto____$1){
var s__91815__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__91815__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__91815__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__91817 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__91816 = (0);
while(true){
if((i__91816 < size__4522__auto__)){
var multiset = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__91816);
cljs.core.chunk_append(b__91817,cljs.core.vec(clojure.math.combinatorics.mapjoin(((function (i__91816,multiset,c__4521__auto__,size__4522__auto__,b__91817,s__91815__$2,temp__5735__auto____$1,part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (p__91818){
var vec__91819 = p__91818;
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91819,(0),null);
var numtimes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91819,(1),null);
return cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(numtimes,(function (){var G__91822 = (index - (1));
return (ditems.cljs$core$IFn$_invoke$arity$1 ? ditems.cljs$core$IFn$_invoke$arity$1(G__91822) : ditems.call(null,G__91822));
})());
});})(i__91816,multiset,c__4521__auto__,size__4522__auto__,b__91817,s__91815__$2,temp__5735__auto____$1,part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,multiset)));

var G__92265 = (i__91816 + (1));
i__91816 = G__92265;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__91817),clojure$math$combinatorics$iter__91784_$_iter__91814(cljs.core.chunk_rest(s__91815__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__91817),null);
}
} else {
var multiset = cljs.core.first(s__91815__$2);
return cljs.core.cons(cljs.core.vec(clojure.math.combinatorics.mapjoin(((function (multiset,s__91815__$2,temp__5735__auto____$1,part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to){
return (function (p__91823){
var vec__91824 = p__91823;
var index = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91824,(0),null);
var numtimes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__91824,(1),null);
return cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(numtimes,(function (){var G__91827 = (index - (1));
return (ditems.cljs$core$IFn$_invoke$arity$1 ? ditems.cljs$core$IFn$_invoke$arity$1(G__91827) : ditems.call(null,G__91827));
})());
});})(multiset,s__91815__$2,temp__5735__auto____$1,part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,multiset)),clojure$math$combinatorics$iter__91784_$_iter__91814(cljs.core.rest(s__91815__$2)));
}
} else {
return null;
}
break;
}
});})(part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,null,null));
});})(part,s__91785__$2,temp__5735__auto__,start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
;
return iter__4523__auto__(part);
})(),clojure$math$combinatorics$iter__91784(cljs.core.rest(s__91785__$2)));
}
} else {
return null;
}
break;
}
});})(start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
,null,null));
});})(start_multiset,parts,items__$1,ditems,freqs,N,M,from__$1,to__$1,map__91751,map__91751__$1,from,to))
;
return iter__4523__auto__(parts);

}
}
}
});

clojure.math.combinatorics.partitions_M.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
clojure.math.combinatorics.partitions_M.cljs$lang$applyTo = (function (seq91732){
var G__91734 = cljs.core.first(seq91732);
var seq91732__$1 = cljs.core.next(seq91732);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__91734,seq91732__$1);
});

/**
 * All the lexicographic distinct partitions of items.
 *  Optionally pass in :min and/or :max to specify inclusive bounds on the number of parts the items can be split into.
 */
clojure.math.combinatorics.partitions = (function clojure$math$combinatorics$partitions(var_args){
var args__4736__auto__ = [];
var len__4730__auto___92277 = arguments.length;
var i__4731__auto___92278 = (0);
while(true){
if((i__4731__auto___92278 < len__4730__auto___92277)){
args__4736__auto__.push((arguments[i__4731__auto___92278]));

var G__92279 = (i__4731__auto___92278 + (1));
i__4731__auto___92278 = G__92279;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return clojure.math.combinatorics.partitions.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

clojure.math.combinatorics.partitions.cljs$core$IFn$_invoke$arity$variadic = (function (items,args){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.count(items),(0))){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(clojure.math.combinatorics.partitions_H,items,args);
} else {
if(cljs.core.truth_(clojure.math.combinatorics.all_different_QMARK_(items))){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(clojure.math.combinatorics.partitions_H,items,args);
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(clojure.math.combinatorics.partitions_M,items,args);

}
}
});

clojure.math.combinatorics.partitions.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
clojure.math.combinatorics.partitions.cljs$lang$applyTo = (function (seq91831){
var G__91832 = cljs.core.first(seq91831);
var seq91831__$1 = cljs.core.next(seq91831);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__91832,seq91831__$1);
});


//# sourceMappingURL=clojure.math.combinatorics.js.map
