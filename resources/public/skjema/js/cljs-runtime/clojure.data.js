goog.provide('clojure.data');
goog.require('cljs.core');
goog.require('clojure.set');
/**
 * Internal helper for diff.
 */
clojure.data.atom_diff = (function clojure$data$atom_diff(a,b){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(a,b)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,null,a], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [a,b,null], null);
}
});
/**
 * Convert an associative-by-numeric-index collection into
 * an equivalent vector, with nil for any missing keys
 */
clojure.data.vectorize = (function clojure$data$vectorize(m){
if(cljs.core.seq(m)){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (result,p__72251){
var vec__72252 = p__72251;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__72252,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__72252,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(result,k,v);
}),cljs.core.vec(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.max,cljs.core.keys(m)),null)),m);
} else {
return null;
}
});
/**
 * Diff associative things a and b, comparing only the key k.
 */
clojure.data.diff_associative_key = (function clojure$data$diff_associative_key(a,b,k){
var va = cljs.core.get.cljs$core$IFn$_invoke$arity$2(a,k);
var vb = cljs.core.get.cljs$core$IFn$_invoke$arity$2(b,k);
var vec__72255 = clojure.data.diff(va,vb);
var a_STAR_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__72255,(0),null);
var b_STAR_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__72255,(1),null);
var ab = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__72255,(2),null);
var in_a = cljs.core.contains_QMARK_(a,k);
var in_b = cljs.core.contains_QMARK_(b,k);
var same = ((in_a) && (in_b) && ((((!((ab == null)))) || ((((va == null)) && ((vb == null)))))));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [((((in_a) && ((((!((a_STAR_ == null)))) || ((!(same)))))))?cljs.core.PersistentArrayMap.createAsIfByAssoc([k,a_STAR_]):null),((((in_b) && ((((!((b_STAR_ == null)))) || ((!(same)))))))?cljs.core.PersistentArrayMap.createAsIfByAssoc([k,b_STAR_]):null),((same)?cljs.core.PersistentArrayMap.createAsIfByAssoc([k,ab]):null)], null);
});
/**
 * Diff associative things a and b, comparing only keys in ks (if supplied).
 */
clojure.data.diff_associative = (function clojure$data$diff_associative(var_args){
var G__72279 = arguments.length;
switch (G__72279) {
case 2:
return clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$2 = (function (a,b){
return clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3(a,b,clojure.set.union.cljs$core$IFn$_invoke$arity$2(cljs.core.keys(a),cljs.core.keys(b)));
});

clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3 = (function (a,b,ks){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (diff1,diff2){
return cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.merge,diff1,diff2));
}),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,null,null], null),cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.partial.cljs$core$IFn$_invoke$arity$3(clojure.data.diff_associative_key,a,b),ks));
});

clojure.data.diff_associative.cljs$lang$maxFixedArity = 3;

clojure.data.diff_sequential = (function clojure$data$diff_sequential(a,b){
return cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(clojure.data.vectorize,clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3(((cljs.core.vector_QMARK_(a))?a:cljs.core.vec(a)),((cljs.core.vector_QMARK_(b))?b:cljs.core.vec(b)),cljs.core.range.cljs$core$IFn$_invoke$arity$1((function (){var x__4219__auto__ = cljs.core.count(a);
var y__4220__auto__ = cljs.core.count(b);
return ((x__4219__auto__ > y__4220__auto__) ? x__4219__auto__ : y__4220__auto__);
})()))));
});
clojure.data.diff_set = (function clojure$data$diff_set(a,b){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.not_empty(clojure.set.difference.cljs$core$IFn$_invoke$arity$2(a,b)),cljs.core.not_empty(clojure.set.difference.cljs$core$IFn$_invoke$arity$2(b,a)),cljs.core.not_empty(clojure.set.intersection.cljs$core$IFn$_invoke$arity$2(a,b))], null);
});

/**
 * Implementation detail. Subject to change.
 * @interface
 */
clojure.data.EqualityPartition = function(){};

/**
 * Implementation detail. Subject to change.
 */
clojure.data.equality_partition = (function clojure$data$equality_partition(x){
if((((!((x == null)))) && ((!((x.clojure$data$EqualityPartition$equality_partition$arity$1 == null)))))){
return x.clojure$data$EqualityPartition$equality_partition$arity$1(x);
} else {
var x__4433__auto__ = (((x == null))?null:x);
var m__4434__auto__ = (clojure.data.equality_partition[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(x) : m__4434__auto__.call(null,x));
} else {
var m__4431__auto__ = (clojure.data.equality_partition["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(x) : m__4431__auto__.call(null,x));
} else {
throw cljs.core.missing_protocol("EqualityPartition.equality-partition",x);
}
}
}
});


/**
 * Implementation detail. Subject to change.
 * @interface
 */
clojure.data.Diff = function(){};

/**
 * Implementation detail. Subject to change.
 */
clojure.data.diff_similar = (function clojure$data$diff_similar(a,b){
if((((!((a == null)))) && ((!((a.clojure$data$Diff$diff_similar$arity$2 == null)))))){
return a.clojure$data$Diff$diff_similar$arity$2(a,b);
} else {
var x__4433__auto__ = (((a == null))?null:a);
var m__4434__auto__ = (clojure.data.diff_similar[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(a,b) : m__4434__auto__.call(null,a,b));
} else {
var m__4431__auto__ = (clojure.data.diff_similar["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(a,b) : m__4431__auto__.call(null,a,b));
} else {
throw cljs.core.missing_protocol("Diff.diff-similar",a);
}
}
}
});

goog.object.set(clojure.data.EqualityPartition,"null",true);

var G__72299_72449 = clojure.data.equality_partition;
var G__72300_72450 = "null";
var G__72301_72451 = ((function (G__72299_72449,G__72300_72450){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__72299_72449,G__72300_72450))
;
goog.object.set(G__72299_72449,G__72300_72450,G__72301_72451);

goog.object.set(clojure.data.EqualityPartition,"string",true);

var G__72310_72461 = clojure.data.equality_partition;
var G__72311_72462 = "string";
var G__72312_72463 = ((function (G__72310_72461,G__72311_72462){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__72310_72461,G__72311_72462))
;
goog.object.set(G__72310_72461,G__72311_72462,G__72312_72463);

goog.object.set(clojure.data.EqualityPartition,"number",true);

var G__72315_72468 = clojure.data.equality_partition;
var G__72316_72469 = "number";
var G__72317_72470 = ((function (G__72315_72468,G__72316_72469){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__72315_72468,G__72316_72469))
;
goog.object.set(G__72315_72468,G__72316_72469,G__72317_72470);

goog.object.set(clojure.data.EqualityPartition,"array",true);

var G__72319_72471 = clojure.data.equality_partition;
var G__72320_72472 = "array";
var G__72321_72473 = ((function (G__72319_72471,G__72320_72472){
return (function (x){
return new cljs.core.Keyword(null,"sequential","sequential",-1082983960);
});})(G__72319_72471,G__72320_72472))
;
goog.object.set(G__72319_72471,G__72320_72472,G__72321_72473);

goog.object.set(clojure.data.EqualityPartition,"function",true);

var G__72325_72486 = clojure.data.equality_partition;
var G__72326_72487 = "function";
var G__72327_72488 = ((function (G__72325_72486,G__72326_72487){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__72325_72486,G__72326_72487))
;
goog.object.set(G__72325_72486,G__72326_72487,G__72327_72488);

goog.object.set(clojure.data.EqualityPartition,"boolean",true);

var G__72328_72489 = clojure.data.equality_partition;
var G__72329_72490 = "boolean";
var G__72330_72491 = ((function (G__72328_72489,G__72329_72490){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__72328_72489,G__72329_72490))
;
goog.object.set(G__72328_72489,G__72329_72490,G__72330_72491);

goog.object.set(clojure.data.EqualityPartition,"_",true);

var G__72332_72492 = clojure.data.equality_partition;
var G__72333_72493 = "_";
var G__72334_72494 = ((function (G__72332_72492,G__72333_72493){
return (function (x){
if((((!((x == null))))?(((((x.cljs$lang$protocol_mask$partition0$ & (1024))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$IMap$))))?true:(((!x.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.IMap,x):false)):cljs.core.native_satisfies_QMARK_(cljs.core.IMap,x))){
return new cljs.core.Keyword(null,"map","map",1371690461);
} else {
if((((!((x == null))))?(((((x.cljs$lang$protocol_mask$partition0$ & (4096))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$ISet$))))?true:(((!x.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.ISet,x):false)):cljs.core.native_satisfies_QMARK_(cljs.core.ISet,x))){
return new cljs.core.Keyword(null,"set","set",304602554);
} else {
if((((!((x == null))))?(((((x.cljs$lang$protocol_mask$partition0$ & (16777216))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$ISequential$))))?true:(((!x.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.ISequential,x):false)):cljs.core.native_satisfies_QMARK_(cljs.core.ISequential,x))){
return new cljs.core.Keyword(null,"sequential","sequential",-1082983960);
} else {
return new cljs.core.Keyword(null,"atom","atom",-397043653);

}
}
}
});})(G__72332_72492,G__72333_72493))
;
goog.object.set(G__72332_72492,G__72333_72493,G__72334_72494);
goog.object.set(clojure.data.Diff,"null",true);

var G__72347_72499 = clojure.data.diff_similar;
var G__72348_72500 = "null";
var G__72349_72501 = ((function (G__72347_72499,G__72348_72500){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__72347_72499,G__72348_72500))
;
goog.object.set(G__72347_72499,G__72348_72500,G__72349_72501);

goog.object.set(clojure.data.Diff,"string",true);

var G__72351_72504 = clojure.data.diff_similar;
var G__72352_72505 = "string";
var G__72353_72506 = ((function (G__72351_72504,G__72352_72505){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__72351_72504,G__72352_72505))
;
goog.object.set(G__72351_72504,G__72352_72505,G__72353_72506);

goog.object.set(clojure.data.Diff,"number",true);

var G__72357_72509 = clojure.data.diff_similar;
var G__72358_72510 = "number";
var G__72359_72511 = ((function (G__72357_72509,G__72358_72510){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__72357_72509,G__72358_72510))
;
goog.object.set(G__72357_72509,G__72358_72510,G__72359_72511);

goog.object.set(clojure.data.Diff,"array",true);

var G__72362_72512 = clojure.data.diff_similar;
var G__72363_72513 = "array";
var G__72364_72514 = ((function (G__72362_72512,G__72363_72513){
return (function (a,b){
return clojure.data.diff_sequential(a,b);
});})(G__72362_72512,G__72363_72513))
;
goog.object.set(G__72362_72512,G__72363_72513,G__72364_72514);

goog.object.set(clojure.data.Diff,"function",true);

var G__72367_72518 = clojure.data.diff_similar;
var G__72368_72519 = "function";
var G__72369_72520 = ((function (G__72367_72518,G__72368_72519){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__72367_72518,G__72368_72519))
;
goog.object.set(G__72367_72518,G__72368_72519,G__72369_72520);

goog.object.set(clojure.data.Diff,"boolean",true);

var G__72373_72521 = clojure.data.diff_similar;
var G__72374_72522 = "boolean";
var G__72375_72523 = ((function (G__72373_72521,G__72374_72522){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__72373_72521,G__72374_72522))
;
goog.object.set(G__72373_72521,G__72374_72522,G__72375_72523);

goog.object.set(clojure.data.Diff,"_",true);

var G__72378_72529 = clojure.data.diff_similar;
var G__72379_72530 = "_";
var G__72380_72531 = ((function (G__72378_72529,G__72379_72530){
return (function (a,b){
var fexpr__72388 = (function (){var G__72389 = clojure.data.equality_partition(a);
var G__72389__$1 = (((G__72389 instanceof cljs.core.Keyword))?G__72389.fqn:null);
switch (G__72389__$1) {
case "atom":
return clojure.data.atom_diff;

break;
case "set":
return clojure.data.diff_set;

break;
case "sequential":
return clojure.data.diff_sequential;

break;
case "map":
return clojure.data.diff_associative;

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__72389__$1)].join('')));

}
})();
return (fexpr__72388.cljs$core$IFn$_invoke$arity$2 ? fexpr__72388.cljs$core$IFn$_invoke$arity$2(a,b) : fexpr__72388.call(null,a,b));
});})(G__72378_72529,G__72379_72530))
;
goog.object.set(G__72378_72529,G__72379_72530,G__72380_72531);
/**
 * Recursively compares a and b, returning a tuple of
 *   [things-only-in-a things-only-in-b things-in-both].
 *   Comparison rules:
 * 
 *   * For equal a and b, return [nil nil a].
 *   * Maps are subdiffed where keys match and values differ.
 *   * Sets are never subdiffed.
 *   * All sequential things are treated as associative collections
 *  by their indexes, with results returned as vectors.
 *   * Everything else (including strings!) is treated as
 *  an atom and compared for equality.
 */
clojure.data.diff = (function clojure$data$diff(a,b){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(a,b)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,null,a], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(clojure.data.equality_partition(a),clojure.data.equality_partition(b))){
return clojure.data.diff_similar(a,b);
} else {
return clojure.data.atom_diff(a,b);
}
}
});

//# sourceMappingURL=clojure.data.js.map
