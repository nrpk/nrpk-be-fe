goog.provide('playground.sub');
goog.require('cljs.core');
goog.require('integrant.core');
goog.require('cljs.pprint');
goog.require('re_frame.core');
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.sub","app-db","playground.sub/app-db",934386255),(function (_,p__93775){
var map__93776 = p__93775;
var map__93776__$1 = (((((!((map__93776 == null))))?(((((map__93776.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93776.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93776):map__93776);
var value_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93776__$1,new cljs.core.Keyword(null,"value-fn","value-fn",544624790));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-3.text-gray-500","div.mt-3.text-gray-500",-460742126),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-6.flex.justify-between","div.px-6.flex.justify-between",-966335909),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs","p.text-xs",702447668),"App-db"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs","p.text-xs",702447668),"00/00/00"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pre.bg-gray-800.text-gray-500.text-xxs.px-6.py-4","pre.bg-gray-800.text-gray-500.text-xxs.px-6.py-4",-515457647),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"overflow-y","overflow-y",-1436589285),new cljs.core.Keyword(null,"scroll","scroll",971553779)], null)], null),(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__93780_93823 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__93781_93824 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__93782_93825 = true;
var _STAR_print_fn_STAR__temp_val__93783_93826 = ((function (_STAR_print_newline_STAR__orig_val__93780_93823,_STAR_print_fn_STAR__orig_val__93781_93824,_STAR_print_newline_STAR__temp_val__93782_93825,sb__4661__auto__,map__93776,map__93776__$1,value_fn){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__93780_93823,_STAR_print_fn_STAR__orig_val__93781_93824,_STAR_print_newline_STAR__temp_val__93782_93825,sb__4661__auto__,map__93776,map__93776__$1,value_fn))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__93782_93825;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__93783_93826;

try{cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1(cljs.core.deref((value_fn.cljs$core$IFn$_invoke$arity$0 ? value_fn.cljs$core$IFn$_invoke$arity$0() : value_fn.call(null))));
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__93781_93824;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__93780_93823;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})()], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.sub","toggler","playground.sub/toggler",-467466414),(function (_,p__93785){
var map__93786 = p__93785;
var map__93786__$1 = (((((!((map__93786 == null))))?(((((map__93786.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93786.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93786):map__93786);
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93786__$1,new cljs.core.Keyword(null,"path","path",-188191168));
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93786__$1,new cljs.core.Keyword(null,"title","title",636505583));
var subs_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93786__$1,new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466));
var change_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93786__$1,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.w-full","div.w-full",-1626749092),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.select-none.flex.items-baseline.px-2","label.select-none.flex.items-baseline.px-2",609904428),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.border.text-black.bg-gray-400","input.border.text-black.bg-gray-400",1427555361),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"name","name",1843675177),"some name",new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"checked","checked",-50955819),cljs.core.deref((subs_fn.cljs$core$IFn$_invoke$arity$1 ? subs_fn.cljs$core$IFn$_invoke$arity$1(path) : subs_fn.call(null,path))),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"",new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__93786,map__93786__$1,path,title,subs_fn,change_fn){
return (function (p1__93784_SHARP_){
return (change_fn.cljs$core$IFn$_invoke$arity$2 ? change_fn.cljs$core$IFn$_invoke$arity$2(path,p1__93784_SHARP_) : change_fn.call(null,path,p1__93784_SHARP_));
});})(map__93786,map__93786__$1,path,title,subs_fn,change_fn))
], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.px-1","p.px-1",1720770580),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),(cljs.core.truth_(cljs.core.deref((subs_fn.cljs$core$IFn$_invoke$arity$1 ? subs_fn.cljs$core$IFn$_invoke$arity$1(path) : subs_fn.call(null,path))))?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"text-gray-900","text-gray-900",-190813918)], null):new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"text-gray-500","text-gray-500",1201404545)], null))], null),title], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.sub","field","playground.sub/field",-1366235937),(function (_,p__93792){
var map__93794 = p__93792;
var map__93794__$1 = (((((!((map__93794 == null))))?(((((map__93794.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93794.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93794):map__93794);
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93794__$1,new cljs.core.Keyword(null,"path","path",-188191168));
var subs_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93794__$1,new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466));
var change_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93794__$1,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-3.px-0","div.mt-3.px-0",-1431025738),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs","p.text-xs",702447668),path], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.border.p-1.text-black.bg-gray-400.w-full","input.border.p-1.text-black.bg-gray-400.w-full",1864043662),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"password",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref((subs_fn.cljs$core$IFn$_invoke$arity$1 ? subs_fn.cljs$core$IFn$_invoke$arity$1(path) : subs_fn.call(null,path))),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"",new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__93794,map__93794__$1,path,subs_fn,change_fn){
return (function (p1__93788_SHARP_){
return (change_fn.cljs$core$IFn$_invoke$arity$2 ? change_fn.cljs$core$IFn$_invoke$arity$2(path,p1__93788_SHARP_) : change_fn.call(null,path,p1__93788_SHARP_));
});})(map__93794,map__93794__$1,path,subs_fn,change_fn))
], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.sub","text-field","playground.sub/text-field",1753210994),(function (_,p__93802){
var map__93803 = p__93802;
var map__93803__$1 = (((((!((map__93803 == null))))?(((((map__93803.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93803.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93803):map__93803);
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93803__$1,new cljs.core.Keyword(null,"title","title",636505583));
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93803__$1,new cljs.core.Keyword(null,"path","path",-188191168));
var subs_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93803__$1,new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466));
var change_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93803__$1,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-3.px-0","div.mt-3.px-0",-1431025738),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs.px-2.text-blue-500","p.text-xs.px-2.text-blue-500",-764427228),(function (){var or__4131__auto__ = title;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "Tiele";
}
})()], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.rounded.px-2.border.py-1.text-black.bg-white.w-full","input.rounded.px-2.border.py-1.text-black.bg-white.w-full",1562937904),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref((subs_fn.cljs$core$IFn$_invoke$arity$1 ? subs_fn.cljs$core$IFn$_invoke$arity$1(path) : subs_fn.call(null,path))),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"enter some text",new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__93803,map__93803__$1,title,path,subs_fn,change_fn){
return (function (p1__93801_SHARP_){
return (change_fn.cljs$core$IFn$_invoke$arity$2 ? change_fn.cljs$core$IFn$_invoke$arity$2(path,p1__93801_SHARP_) : change_fn.call(null,path,p1__93801_SHARP_));
});})(map__93803,map__93803__$1,title,path,subs_fn,change_fn))
], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.sub","field2","playground.sub/field2",1274030194),(function (_,p__93807){
var map__93808 = p__93807;
var map__93808__$1 = (((((!((map__93808 == null))))?(((((map__93808.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93808.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93808):map__93808);
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93808__$1,new cljs.core.Keyword(null,"path","path",-188191168));
var subs_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93808__$1,new cljs.core.Keyword(null,"subs-fn","subs-fn",1888574466));
var change_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93808__$1,new cljs.core.Keyword(null,"change-fn","change-fn",1566440825));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-3","div.mt-3",-681976597),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs","p.text-xs",702447668),path], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.border.p-1.text-black.bg-gray-400","input.border.p-1.text-black.bg-gray-400",-1172989120),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"date",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref((subs_fn.cljs$core$IFn$_invoke$arity$1 ? subs_fn.cljs$core$IFn$_invoke$arity$1(path) : subs_fn.call(null,path))),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"",new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__93808,map__93808__$1,path,subs_fn,change_fn){
return (function (p1__93805_SHARP_){
return (change_fn.cljs$core$IFn$_invoke$arity$2 ? change_fn.cljs$core$IFn$_invoke$arity$2(path,p1__93805_SHARP_) : change_fn.call(null,path,p1__93805_SHARP_));
});})(map__93808,map__93808__$1,path,subs_fn,change_fn))
], null)], null)], null);
}));
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.sub","component","playground.sub/component",1490105985),(function (_,p__93810){
var map__93811 = p__93810;
var map__93811__$1 = (((((!((map__93811 == null))))?(((((map__93811.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93811.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93811):map__93811);
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93811__$1,new cljs.core.Keyword(null,"title","title",636505583));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-3","div.mt-3",-681976597),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xl","p.text-xl",-9059021),(function (){var or__4131__auto__ = title;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "notitle";
}
})()], null)], null);
}));
playground.sub.item = (function playground$sub$item(p__93813){
var map__93814 = p__93813;
var map__93814__$1 = (((((!((map__93814 == null))))?(((((map__93814.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93814.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93814):map__93814);
var record = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93814__$1,new cljs.core.Keyword(null,"record","record",-779106859));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bg-gray-400.text-xl.p-1.rounded-sm.flex","div.bg-gray-400.text-xl.p-1.rounded-sm.flex",-660579462),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.w-full.h-full.center","p.w-full.h-full.center",-41518751),record], null)], null);
});
integrant.core.init_key.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword("playground.sub","contents","playground.sub/contents",-1225764340),(function (_,p__93816){
var map__93817 = p__93816;
var map__93817__$1 = (((((!((map__93817 == null))))?(((((map__93817.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__93817.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__93817):map__93817);
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93817__$1,new cljs.core.Keyword(null,"title","title",636505583));
var stuff = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93817__$1,new cljs.core.Keyword(null,"stuff","stuff",-2051563643));
var click_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__93817__$1,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mt-3","div.mt-3",-681976597),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xl","p.text-xl",-9059021),(function (){var or__4131__auto__ = title;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "no-title";
}
})()], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.h-32.border.bg-white.text-black","div.h-32.border.bg-white.text-black",-1494591998),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"overflow-y","overflow-y",-1436589285),new cljs.core.Keyword(null,"scroll","scroll",971553779)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"2px","2px",-1697779893),new cljs.core.Keyword(null,"grid-template-columns","grid-template-columns",-594112133),"repeat(auto-fit,minmax(100px, 1fr))"], null)], null),(function (){var iter__4523__auto__ = ((function (map__93817,map__93817__$1,title,stuff,click_fn){
return (function playground$sub$iter__93819(s__93820){
return (new cljs.core.LazySeq(null,((function (map__93817,map__93817__$1,title,stuff,click_fn){
return (function (){
var s__93820__$1 = s__93820;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93820__$1);
if(temp__5735__auto__){
var s__93820__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__93820__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93820__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93822 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93821 = (0);
while(true){
if((i__93821 < size__4522__auto__)){
var each = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93821);
cljs.core.chunk_append(b__93822,cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__93821,each,c__4521__auto__,size__4522__auto__,b__93822,s__93820__$2,temp__5735__auto__,map__93817,map__93817__$1,title,stuff,click_fn){
return (function (){
return (click_fn.cljs$core$IFn$_invoke$arity$1 ? click_fn.cljs$core$IFn$_invoke$arity$1(each) : click_fn.call(null,each));
});})(i__93821,each,c__4521__auto__,size__4522__auto__,b__93822,s__93820__$2,temp__5735__auto__,map__93817,map__93817__$1,title,stuff,click_fn))
], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.sub.item,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"record","record",-779106859),each], null)], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),each], null)));

var G__93892 = (i__93821 + (1));
i__93821 = G__93892;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93822),playground$sub$iter__93819(cljs.core.chunk_rest(s__93820__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93822),null);
}
} else {
var each = cljs.core.first(s__93820__$2);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (each,s__93820__$2,temp__5735__auto__,map__93817,map__93817__$1,title,stuff,click_fn){
return (function (){
return (click_fn.cljs$core$IFn$_invoke$arity$1 ? click_fn.cljs$core$IFn$_invoke$arity$1(each) : click_fn.call(null,each));
});})(each,s__93820__$2,temp__5735__auto__,map__93817,map__93817__$1,title,stuff,click_fn))
], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.sub.item,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"record","record",-779106859),each], null)], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),each], null)),playground$sub$iter__93819(cljs.core.rest(s__93820__$2)));
}
} else {
return null;
}
break;
}
});})(map__93817,map__93817__$1,title,stuff,click_fn))
,null,null));
});})(map__93817,map__93817__$1,title,stuff,click_fn))
;
return iter__4523__auto__((stuff.cljs$core$IFn$_invoke$arity$0 ? stuff.cljs$core$IFn$_invoke$arity$0() : stuff.call(null)));
})()], null)], null)], null);
}));

//# sourceMappingURL=playground.sub.js.map
