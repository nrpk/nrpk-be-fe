goog.provide('website.toolbar');
goog.require('cljs.core');
goog.require('re_frame.core');
goog.require('website.button');
var G__88551_88723 = new cljs.core.Keyword("website.toolbar","auto-eval?","website.toolbar/auto-eval?",809468798);
var G__88552_88724 = ((function (G__88551_88723){
return (function (db,p__88553){
var vec__88554 = p__88553;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88554,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88554,(1),null);
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["ID ",id], 0));

return cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"auto-eval?","auto-eval?",1599001830)], null),false);
});})(G__88551_88723))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__88551_88723,G__88552_88724) : re_frame.core.reg_sub.call(null,G__88551_88723,G__88552_88724));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","set-auto-eval","website.toolbar/set-auto-eval",-249323270),(function (db,p__88557){
var vec__88559 = p__88557;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88559,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88559,(1),null);
var state = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88559,(2),null);
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["ID2 ",id,state], 0));

return cljs.core.assoc_in(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"auto-eval?","auto-eval?",1599001830)], null),state);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","toggle-auto-eval","website.toolbar/toggle-auto-eval",-1764017317),(function (db,p__88564){
var vec__88565 = p__88564;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88565,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88565,(1),null);
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["ID3 ",db], 0));

return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"auto-eval?","auto-eval?",1599001830)], null),cljs.core.not);
}));
var G__88568_88729 = new cljs.core.Keyword("website.toolbar","auto-eval2?","website.toolbar/auto-eval2?",-375585031);
var G__88569_88730 = ((function (G__88568_88729){
return (function (db,p__88570){
var vec__88572 = p__88570;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88572,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88572,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"auto-eval2?","auto-eval2?",915076561)], null),false);
});})(G__88568_88729))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__88568_88729,G__88569_88730) : re_frame.core.reg_sub.call(null,G__88568_88729,G__88569_88730));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","toggle-auto-eval2","website.toolbar/toggle-auto-eval2",495375836),(function (db,p__88576){
var vec__88577 = p__88576;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88577,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88577,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"auto-eval2?","auto-eval2?",915076561)], null),cljs.core.not);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","clear-code-buffer","website.toolbar/clear-code-buffer",-1555662476),(function (db,_){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"code-buffer","code-buffer",-855556304)], null),null);
}));
var G__88589_88736 = new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594);
var G__88590_88737 = ((function (G__88589_88736){
return (function (db,p__88593){
var vec__88594 = p__88593;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88594,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88594,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594)], null),false);
});})(G__88589_88736))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__88589_88736,G__88590_88737) : re_frame.core.reg_sub.call(null,G__88589_88736,G__88590_88737));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594),(function (db,p__88607){
var vec__88608 = p__88607;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88608,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88608,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594)], null),cljs.core.not);
}));
var G__88615_88742 = new cljs.core.Keyword("website.toolbar","ln?","website.toolbar/ln?",-546067471);
var G__88616_88743 = ((function (G__88615_88742){
return (function (db,p__88618){
var vec__88619 = p__88618;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88619,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88619,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"ln?","ln?",-1828632903)], null),false);
});})(G__88615_88742))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__88615_88742,G__88616_88743) : re_frame.core.reg_sub.call(null,G__88615_88742,G__88616_88743));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","toggle-ln","website.toolbar/toggle-ln",-1097809628),(function (db,p__88622){
var vec__88623 = p__88622;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88623,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88623,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"ln?","ln?",-1828632903)], null),cljs.core.not);
}));
var G__88627_88744 = new cljs.core.Keyword("website.toolbar","theme","website.toolbar/theme",1990527592);
var G__88628_88745 = ((function (G__88627_88744){
return (function (db,p__88631){
var vec__88632 = p__88631;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88632,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88632,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"themex","themex",1794608983)], null),(0));
});})(G__88627_88744))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__88627_88744,G__88628_88745) : re_frame.core.reg_sub.call(null,G__88627_88744,G__88628_88745));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","next-theme","website.toolbar/next-theme",493867364),(function (db,p__88637){
var vec__88638 = p__88637;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88638,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88638,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"themex","themex",1794608983)], null),((function (vec__88638,_,id){
return (function (p1__88636_SHARP_){
if((p1__88636_SHARP_ < (1))){
return (p1__88636_SHARP_ + (1));
} else {
return (0);
}
});})(vec__88638,_,id))
);
}));
var G__88641_88765 = new cljs.core.Keyword("website.toolbar","html-parsed","website.toolbar/html-parsed",1292747553);
var G__88642_88766 = ((function (G__88641_88765){
return (function (db,p__88643){
var vec__88645 = p__88643;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88645,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88645,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"html-parsed","html-parsed",1913917929)], null));
});})(G__88641_88765))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__88641_88765,G__88642_88766) : re_frame.core.reg_sub.call(null,G__88641_88765,G__88642_88766));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","set-html-parsed","website.toolbar/set-html-parsed",-1629656998),(function (db,p__88651){
var vec__88652 = p__88651;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88652,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88652,(1),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88652,(2),null);
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("website.toolbar","set-html-parsed","website.toolbar/set-html-parsed",-1629656998),id], 0));

return cljs.core.assoc_in(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"html-parsed","html-parsed",1913917929)], null),v);
}));
var G__88656_88771 = new cljs.core.Keyword("website.toolbar","preview-toggle","website.toolbar/preview-toggle",1847212737);
var G__88657_88772 = ((function (G__88656_88771){
return (function (db,p__88658){
var vec__88659 = p__88658;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88659,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88659,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"preview-toggle","preview-toggle",554609113)], null),false);
});})(G__88656_88771))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__88656_88771,G__88657_88772) : re_frame.core.reg_sub.call(null,G__88656_88771,G__88657_88772));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("website.toolbar","toggle-preview","website.toolbar/toggle-preview",521770163),(function (db,p__88663){
var vec__88664 = p__88663;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88664,(0),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__88664,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"preview-toggle","preview-toggle",554609113)], null),cljs.core.not);
}));
website.toolbar.markdown_controls = (function website$toolbar$markdown_controls(id){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.p-px.bg-white","div.p-px.bg-white",1502422650),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"2px","2px",-1697779893),new cljs.core.Keyword(null,"grid-template-columns","grid-template-columns",-594112133),"repeat(auto-fill, minmax(88px,1fr))",new cljs.core.Keyword(null,"grid-auto-rows","grid-auto-rows",-113194069),"32px"], null)], null),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"M\u00F8rkt/Lyst",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (){
var G__88668 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","next-theme","website.toolbar/next-theme",493867364),id], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__88668) : re_frame.core.dispatch.call(null,G__88668));
}),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
return new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
})], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Linje-nr",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (){
var G__88672 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","toggle-ln","website.toolbar/toggle-ln",-1097809628),id], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__88672) : re_frame.core.dispatch.call(null,G__88672));
}),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
if(cljs.core.truth_(cljs.core.deref((function (){var G__88674 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","ln?","website.toolbar/ln?",-546067471),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88674) : re_frame.core.subscribe.call(null,G__88674));
})()))){
return new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
} else {
return new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
}
})], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Varighet",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
if(cljs.core.truth_(cljs.core.deref((function (){var G__88675 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88675) : re_frame.core.subscribe.call(null,G__88675));
})()))){
return new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
} else {
return new cljs.core.Keyword(null,"hollow-2","hollow-2",1625458912);
}
})], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Preview",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (){
var G__88679 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","toggle-preview","website.toolbar/toggle-preview",521770163),id], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__88679) : re_frame.core.dispatch.call(null,G__88679));
}),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
if(cljs.core.truth_(cljs.core.deref((function (){var G__88680 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","preview-toggle","website.toolbar/preview-toggle",1847212737),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88680) : re_frame.core.subscribe.call(null,G__88680));
})()))){
return new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
} else {
return new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
}
})], null))], null);
});
website.toolbar.controls = (function website$toolbar$controls(id,eval_BANG_){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.p-px.bg-white","div.p-px.bg-white",1502422650),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"display","display",242065432),new cljs.core.Keyword(null,"grid","grid",402978600),new cljs.core.Keyword(null,"grid-gap","grid-gap",1083581064),new cljs.core.Keyword(null,"2px","2px",-1697779893),new cljs.core.Keyword(null,"grid-template-columns","grid-template-columns",-594112133),"repeat(auto-fill, minmax(88px,1fr))",new cljs.core.Keyword(null,"grid-auto-rows","grid-auto-rows",-113194069),"32px"], null)], null),(function (){var t = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["M\u00F8rkt","Lyst"], null),cljs.core.deref((function (){var G__88702 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","theme","website.toolbar/theme",1990527592),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88702) : re_frame.core.subscribe.call(null,G__88702));
})()));
return website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),t,new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),((function (t){
return (function (){
var G__88703 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","next-theme","website.toolbar/next-theme",493867364),id], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__88703) : re_frame.core.dispatch.call(null,G__88703));
});})(t))
,new cljs.core.Keyword(null,"style","style",-496642736),((function (t){
return (function (){
var pred__88704 = cljs.core._EQ_;
var expr__88705 = t;
if(cljs.core.truth_((pred__88704.cljs$core$IFn$_invoke$arity$2 ? pred__88704.cljs$core$IFn$_invoke$arity$2((0),expr__88705) : pred__88704.call(null,(0),expr__88705)))){
return new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
} else {
return new cljs.core.Keyword(null,"hollow-2","hollow-2",1625458912);
}
});})(t))
], null));
})(),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Linje-nr",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (){
var G__88709 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","toggle-ln","website.toolbar/toggle-ln",-1097809628),id], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__88709) : re_frame.core.dispatch.call(null,G__88709));
}),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
if(cljs.core.truth_(cljs.core.deref((function (){var G__88710 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","ln?","website.toolbar/ln?",-546067471),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88710) : re_frame.core.subscribe.call(null,G__88710));
})()))){
return new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
} else {
return new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
}
})], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Evaluate",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.Keyword(null,"default","default",-1987822328),new cljs.core.Keyword(null,"_sub-caption","_sub-caption",-1984610424),"cmd-enter",new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),eval_BANG_], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Clear",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (){
var G__88711 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","clear-code-buffer","website.toolbar/clear-code-buffer",-1555662476)], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__88711) : re_frame.core.dispatch.call(null,G__88711));
}),new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.Keyword(null,"slight","slight",-1057194628)], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Reset",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (p1__88699_SHARP_){
return cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([p1__88699_SHARP_], 0));
}),new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.Keyword(null,"danger","danger",-624338030)], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Auto-eval",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),(function (){
var G__88712 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","toggle-auto-eval","website.toolbar/toggle-auto-eval",-1764017317),id], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__88712) : re_frame.core.dispatch.call(null,G__88712));
}),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
if(cljs.core.truth_(cljs.core.deref((function (){var G__88716 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","auto-eval?","website.toolbar/auto-eval?",809468798),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88716) : re_frame.core.subscribe.call(null,G__88716));
})()))){
return new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
} else {
return new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
}
})], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Publisert",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),new cljs.core.Keyword("website.toolbar","toggle-auto-eval2","website.toolbar/toggle-auto-eval2",495375836),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
if(cljs.core.truth_(cljs.core.deref((function (){var G__88718 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","auto-eval2?","website.toolbar/auto-eval2?",-375585031),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88718) : re_frame.core.subscribe.call(null,G__88718));
})()))){
return new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
} else {
return new cljs.core.Keyword(null,"hollow","hollow",-1445647344);
}
})], null)),website.button.button(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"caption","caption",-855383902),"Varighet",new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"click-fn","click-fn",2099562548),new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594),new cljs.core.Keyword(null,"style","style",-496642736),(function (){
if(cljs.core.truth_(cljs.core.deref((function (){var G__88719 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("website.toolbar","varighet","website.toolbar/varighet",573229594),id], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__88719) : re_frame.core.subscribe.call(null,G__88719));
})()))){
return new cljs.core.Keyword(null,"toggle-on","toggle-on",-265263793);
} else {
return new cljs.core.Keyword(null,"hollow-2","hollow-2",1625458912);
}
})], null))], null);
});

//# sourceMappingURL=website.toolbar.js.map
