goog.provide('nv.yd');
goog.require('cljs.core');
goog.require('cljs_time.format');
goog.require('cljs_time.core');
goog.require('cljs_time.coerce');
goog.require('cljs_time.periodic');
goog.require('cljs_time.instant');
nv.yd.offset = cljs_time.core.date_time.cljs$core$IFn$_invoke$arity$3((2019),(1),(1));
nv.yd.month_names = new cljs.core.PersistentVector(null, 12, 5, cljs.core.PersistentVector.EMPTY_NODE, ["Januar","Februar","Mars","April","Mai","Juni","Juli","August","September","Oktober","November","Desember"], null);
nv.yd.day_names = cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.str,new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"Mandag","Mandag",655016159,null),new cljs.core.Symbol(null,"Tirsdag","Tirsdag",40927917,null),new cljs.core.Symbol(null,"Onsdag","Onsdag",-1863172404,null),new cljs.core.Symbol(null,"Torsdag","Torsdag",-1987290177,null),new cljs.core.Symbol(null,"Fredag","Fredag",-1355819365,null),new cljs.core.Symbol(null,"L\u00F8rdag","L\u00F8rdag",1785077433,null),new cljs.core.Symbol(null,"S\u00F8ndag","S\u00F8ndag",-1149224853,null)], null));
nv.yd.yd__GT_date = cljs.core.memoize((function (offset,yd){
return cljs_time.core.plus.cljs$core$IFn$_invoke$arity$2(offset,cljs_time.core.days.cljs$core$IFn$_invoke$arity$1(yd));
}));
nv.yd.date_time__GT_yd = cljs.core.memoize((function (dt){

var month = cljs_time.core.month(dt);
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (month){
return (function (days,month__$1){
return (days + cljs_time.core.number_of_days_in_the_month.cljs$core$IFn$_invoke$arity$2((2019),month__$1));
});})(month))
,(cljs_time.core.day(dt) - (1)),cljs.core.range.cljs$core$IFn$_invoke$arity$1((month - (1))));
}));
nv.yd.y = (function nv$yd$y(n){

return (nv.yd.yd__GT_date.cljs$core$IFn$_invoke$arity$2 ? nv.yd.yd__GT_date.cljs$core$IFn$_invoke$arity$2(nv.yd.offset,n) : nv.yd.yd__GT_date.call(null,nv.yd.offset,n));
});
nv.yd.year = (function nv$yd$year(yd){
return cljs_time.core.year(yd);
});
nv.yd.month_name = (function nv$yd$month_name(yd){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(nv.yd.month_names,(cljs_time.core.month(yd) - (1)));
});
nv.yd.short_number_date = (function nv$yd$short_number_date(yd){
return cljs_time.format.unparse(cljs_time.format.formatter.cljs$core$IFn$_invoke$arity$1("dd/MM"),nv.yd.y(yd));
});
nv.yd.short_month_name = (function nv$yd$short_month_name(yd){
return cljs.core.subs.cljs$core$IFn$_invoke$arity$3(nv.yd.month_name(yd),(0),(3));
});
nv.yd.yd__GT_short_month_name = (function nv$yd$yd__GT_short_month_name(yd){
return cljs.core.subs.cljs$core$IFn$_invoke$arity$3(nv.yd.month_name(nv.yd.y(yd)),(0),(3));
});
nv.yd.month__GT_total_month_days = (function nv$yd$month__GT_total_month_days(n){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([n,cljs_time.core.number_of_days_in_the_month.cljs$core$IFn$_invoke$arity$2(cljs_time.core.year(nv.yd.offset),n)], 0));

if((n === (0))){
return (0);
} else {
return (cljs_time.core.number_of_days_in_the_month.cljs$core$IFn$_invoke$arity$2(cljs_time.core.year(nv.yd.offset),n) + (1));
}
});
nv.yd.day_name = (function nv$yd$day_name(n){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(nv.yd.day_names,(cljs_time.core.day_of_week(n) - (1)));
});
nv.yd.week = (function nv$yd$week(yd){
return cljs_time.core.week_number_of_year(yd);
});
nv.yd.first_week_day_QMARK__yd = (function nv$yd$first_week_day_QMARK__yd(n){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),cljs_time.core.day_of_week(nv.yd.y(n)));
});
nv.yd.first_weekday_in_month = (function nv$yd$first_weekday_in_month(year,month){
return cljs_time.core.day_of_week(cljs_time.core.date_time.cljs$core$IFn$_invoke$arity$2(year,month));
});
nv.yd.first_month_day_QMARK__yd = (function nv$yd$first_month_day_QMARK__yd(n){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),cljs_time.core.day(nv.yd.y(n)));
});
nv.yd.convert__GT_yd = (function nv$yd$convert__GT_yd(year,month,day){
return cljs_time.format.unparse(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"format-str","format-str",695206156),"D"], null),cljs_time.core.date_time.cljs$core$IFn$_invoke$arity$3(year,month,day));
});
nv.yd.dt__GT_day_number_of_week = (function nv$yd$dt__GT_day_number_of_week(dt){
return cljs_time.core.day_of_week(dt);
});
nv.yd.yd__GT_day_number_of_week = (function nv$yd$yd__GT_day_number_of_week(yd){
return cljs_time.core.day_of_week(nv.yd.y(yd));
});
nv.yd.yd__GT_short_day_name = (function nv$yd$yd__GT_short_day_name(yd){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(nv.yd.day_names,(nv.yd.yd__GT_day_number_of_week(yd) - (1)));
});

//# sourceMappingURL=nv.yd.js.map
