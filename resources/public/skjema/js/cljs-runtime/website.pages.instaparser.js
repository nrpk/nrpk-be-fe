goog.provide('website.pages.instaparser');
goog.require('cljs.core');
goog.require('cljs.pprint');
goog.require('instaparse.core');
website.pages.instaparser.sections = instaparse.core.parser("sentence = token (<whitespace> token)*\n     <token> = word | number | topic\n     whitespace = #'\\s+'\n     word = #'[a-zA-Z]+'\n     <words> = #'[a-zA-Z\\-]+'\n     topic = <'@'> words\n     number = #'[0-9]+'");
website.pages.instaparser.render = (function website$pages$instaparser$render(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"<>","<>",1280186386),(website.pages.instaparser.to_html.cljs$core$IFn$_invoke$arity$1 ? website.pages.instaparser.to_html.cljs$core$IFn$_invoke$arity$1("this is a test 123 @topic-this 123 THis is a ttest") : website.pages.instaparser.to_html.call(null,"this is a test 123 @topic-this 123 THis is a ttest")),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pre.text-xs.bg-yellow-500","pre.text-xs.bg-yellow-500",-1202731892),(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__93922_94072 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__93923_94073 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__93924_94074 = true;
var _STAR_print_fn_STAR__temp_val__93925_94075 = ((function (_STAR_print_newline_STAR__orig_val__93922_94072,_STAR_print_fn_STAR__orig_val__93923_94073,_STAR_print_newline_STAR__temp_val__93924_94074,sb__4661__auto__){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__93922_94072,_STAR_print_fn_STAR__orig_val__93923_94073,_STAR_print_newline_STAR__temp_val__93924_94074,sb__4661__auto__))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__93924_94074;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__93925_94075;

try{cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1(website.pages.instaparser.sections);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__93923_94073;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__93922_94072;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})()], null)], null);
});
website.pages.instaparser.parse_md = instaparse.core.parser("Blocks = (Paragraph | Header | List | Ordered | Code | Rule)+\nHeader = Line Headerline Blankline+\n<Headerline> = h1 | h2\nh1 = '='+\nh2 = '-'+\nList = Listline+ Blankline+\nListline = Listmarker Whitespace+ Word (Whitespace Word)* EOL\n<Listmarker> = <'+' | '*' | '-'>\nOrdered = Orderedline+ Blankline+\nOrderedline = Orderedmarker Whitespace* Word (Whitespace Word)* EOL\n<Orderedmarker> = <#'[0-9]+\\.'>\nCode = Codeline+ Blankline+\nCodeline = <Space Space Space Space> (Whitespace | Word)* EOL\nRule = Ruleline Blankline+\n<Ruleline> = <'+'+ | '*'+ | '-'+>\nParagraph = Line+ Blankline+\n<Blankline> = Whitespace* EOL\n<Line> = Linepre Word (Whitespace Word)* Linepost EOL\n<Linepre> = (Space (Space (Space)? )? )?\n<Linepost> = Space?\n<Whitespace> = #'(\\ | \\t)+'\n<Space> = ' '\n<Word> = #'\\S+'\n<EOL> = <'\\n'>");
website.pages.instaparser.span_elems = new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [/!\[(\S+)\]\((\S+)\)/,(function (p__93932){
var vec__93933 = p__93932;
var n = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93933,(0),null);
var href = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93933,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",-1651076051),href,new cljs.core.Keyword(null,"alt","alt",-3214426),n], null)], null);
})], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [/\[(\S+)\]\((\S+)\)/,(function (p__93936){
var vec__93937 = p__93936;
var n = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93937,(0),null);
var href = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93937,(1),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),href], null),n], null);
})], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [/`(\S+)`/,(function (s){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"code","code",1586293142),s], null);
})], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [/\*\*(\S+)\*\*/,(function (s){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),s], null);
})], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [/__(\S+)__/,(function (s){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),s], null);
})], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [/\*(\S+)\*/,(function (s){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"em","em",707813035),s], null);
})], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [/_(\S+)_/,(function (s){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"em","em",707813035),s], null);
})], null)], null);
website.pages.instaparser.parse_span = (function website$pages$instaparser$parse_span(s){
var res = cljs.core.first(cljs.core.filter.cljs$core$IFn$_invoke$arity$2(cljs.core.complement(cljs.core.nil_QMARK_),(function (){var iter__4523__auto__ = (function website$pages$instaparser$parse_span_$_iter__93950(s__93951){
return (new cljs.core.LazySeq(null,(function (){
var s__93951__$1 = s__93951;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93951__$1);
if(temp__5735__auto__){
var s__93951__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__93951__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93951__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93953 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93952 = (0);
while(true){
if((i__93952 < size__4522__auto__)){
var vec__93965 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93952);
var regex = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93965,(0),null);
var func = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93965,(1),null);
cljs.core.chunk_append(b__93953,(function (){var groups = cljs.core.re_matches(regex,s);
if(cljs.core.truth_(groups)){
var G__93968 = cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),groups);
return (func.cljs$core$IFn$_invoke$arity$1 ? func.cljs$core$IFn$_invoke$arity$1(G__93968) : func.call(null,G__93968));
} else {
return null;
}
})());

var G__94099 = (i__93952 + (1));
i__93952 = G__94099;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93953),website$pages$instaparser$parse_span_$_iter__93950(cljs.core.chunk_rest(s__93951__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93953),null);
}
} else {
var vec__93978 = cljs.core.first(s__93951__$2);
var regex = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93978,(0),null);
var func = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__93978,(1),null);
return cljs.core.cons((function (){var groups = cljs.core.re_matches(regex,s);
if(cljs.core.truth_(groups)){
var G__93981 = cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),groups);
return (func.cljs$core$IFn$_invoke$arity$1 ? func.cljs$core$IFn$_invoke$arity$1(G__93981) : func.call(null,G__93981));
} else {
return null;
}
})(),website$pages$instaparser$parse_span_$_iter__93950(cljs.core.rest(s__93951__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(website.pages.instaparser.span_elems);
})()));
if((res == null)){
return s;
} else {
return res;
}
});
website.pages.instaparser.output_html = (function website$pages$instaparser$output_html(blocks){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$2(cljs.core.str,(function (){var iter__4523__auto__ = (function website$pages$instaparser$output_html_$_iter__93982(s__93983){
return (new cljs.core.LazySeq(null,(function (){
var s__93983__$1 = s__93983;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__93983__$1);
if(temp__5735__auto__){
var s__93983__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__93983__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__93983__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__93985 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__93984 = (0);
while(true){
if((i__93984 < size__4522__auto__)){
var b = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__93984);
cljs.core.chunk_append(b__93985,(function (){var G__93994 = cljs.core.first(b);
var G__93994__$1 = (((G__93994 instanceof cljs.core.Keyword))?G__93994.fqn:null);
switch (G__93994__$1) {
case "List":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),(function (){var iter__4523__auto__ = ((function (i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__){
return (function website$pages$instaparser$output_html_$_iter__93982_$_iter__93995(s__93996){
return (new cljs.core.LazySeq(null,((function (i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__){
return (function (){
var s__93996__$1 = s__93996;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__93996__$1);
if(temp__5735__auto____$1){
var s__93996__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__93996__$2)){
var c__4521__auto____$1 = cljs.core.chunk_first(s__93996__$2);
var size__4522__auto____$1 = cljs.core.count(c__4521__auto____$1);
var b__93998 = cljs.core.chunk_buffer(size__4522__auto____$1);
if((function (){var i__93997 = (0);
while(true){
if((i__93997 < size__4522__auto____$1)){
var li = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto____$1,i__93997);
cljs.core.chunk_append(b__93998,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null));

var G__94113 = (i__93997 + (1));
i__93997 = G__94113;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93998),website$pages$instaparser$output_html_$_iter__93982_$_iter__93995(cljs.core.chunk_rest(s__93996__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93998),null);
}
} else {
var li = cljs.core.first(s__93996__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null),website$pages$instaparser$output_html_$_iter__93982_$_iter__93995(cljs.core.rest(s__93996__$2)));
}
} else {
return null;
}
break;
}
});})(i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__))
,null,null));
});})(i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__))
;
return iter__4523__auto__(cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b));
})()], null);

break;
case "Ordered":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),(function (){var iter__4523__auto__ = ((function (i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__){
return (function website$pages$instaparser$output_html_$_iter__93982_$_iter__94003(s__94004){
return (new cljs.core.LazySeq(null,((function (i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__){
return (function (){
var s__94004__$1 = s__94004;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__94004__$1);
if(temp__5735__auto____$1){
var s__94004__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__94004__$2)){
var c__4521__auto____$1 = cljs.core.chunk_first(s__94004__$2);
var size__4522__auto____$1 = cljs.core.count(c__4521__auto____$1);
var b__94006 = cljs.core.chunk_buffer(size__4522__auto____$1);
if((function (){var i__94005 = (0);
while(true){
if((i__94005 < size__4522__auto____$1)){
var li = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto____$1,i__94005);
cljs.core.chunk_append(b__94006,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null));

var G__94116 = (i__94005 + (1));
i__94005 = G__94116;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__94006),website$pages$instaparser$output_html_$_iter__93982_$_iter__94003(cljs.core.chunk_rest(s__94004__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__94006),null);
}
} else {
var li = cljs.core.first(s__94004__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null),website$pages$instaparser$output_html_$_iter__93982_$_iter__94003(cljs.core.rest(s__94004__$2)));
}
} else {
return null;
}
break;
}
});})(i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__))
,null,null));
});})(i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__))
;
return iter__4523__auto__(cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b));
})()], null);

break;
case "Header":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.first(cljs.core.last(b)),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.take.cljs$core$IFn$_invoke$arity$2((cljs.core.count(b) - (2)),cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b))))], null);

break;
case "Code":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pre","pre",2118456869),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"code","code",1586293142),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.interpose.cljs$core$IFn$_invoke$arity$2("<br />",(function (){var iter__4523__auto__ = ((function (i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__){
return (function website$pages$instaparser$output_html_$_iter__93982_$_iter__94013(s__94014){
return (new cljs.core.LazySeq(null,((function (i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__){
return (function (){
var s__94014__$1 = s__94014;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__94014__$1);
if(temp__5735__auto____$1){
var s__94014__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__94014__$2)){
var c__4521__auto____$1 = cljs.core.chunk_first(s__94014__$2);
var size__4522__auto____$1 = cljs.core.count(c__4521__auto____$1);
var b__94016 = cljs.core.chunk_buffer(size__4522__auto____$1);
if((function (){var i__94015 = (0);
while(true){
if((i__94015 < size__4522__auto____$1)){
var line = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto____$1,i__94015);
cljs.core.chunk_append(b__94016,cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),line)));

var G__94124 = (i__94015 + (1));
i__94015 = G__94124;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__94016),website$pages$instaparser$output_html_$_iter__93982_$_iter__94013(cljs.core.chunk_rest(s__94014__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__94016),null);
}
} else {
var line = cljs.core.first(s__94014__$2);
return cljs.core.cons(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),line)),website$pages$instaparser$output_html_$_iter__93982_$_iter__94013(cljs.core.rest(s__94014__$2)));
}
} else {
return null;
}
break;
}
});})(i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__))
,null,null));
});})(i__93984,G__93994,G__93994__$1,b,c__4521__auto__,size__4522__auto__,b__93985,s__93983__$2,temp__5735__auto__))
;
return iter__4523__auto__(cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b));
})()))], null)], null);

break;
case "Rule":
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"hr","hr",1377740067)], null);

break;
case "Paragraph":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b)))], null);

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__93994__$1)].join('')));

}
})());

var G__94125 = (i__93984 + (1));
i__93984 = G__94125;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__93985),website$pages$instaparser$output_html_$_iter__93982(cljs.core.chunk_rest(s__93983__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__93985),null);
}
} else {
var b = cljs.core.first(s__93983__$2);
return cljs.core.cons((function (){var G__94028 = cljs.core.first(b);
var G__94028__$1 = (((G__94028 instanceof cljs.core.Keyword))?G__94028.fqn:null);
switch (G__94028__$1) {
case "List":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),(function (){var iter__4523__auto__ = ((function (G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__){
return (function website$pages$instaparser$output_html_$_iter__93982_$_iter__94030(s__94031){
return (new cljs.core.LazySeq(null,((function (G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__){
return (function (){
var s__94031__$1 = s__94031;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__94031__$1);
if(temp__5735__auto____$1){
var s__94031__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__94031__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__94031__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__94033 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__94032 = (0);
while(true){
if((i__94032 < size__4522__auto__)){
var li = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__94032);
cljs.core.chunk_append(b__94033,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null));

var G__94127 = (i__94032 + (1));
i__94032 = G__94127;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__94033),website$pages$instaparser$output_html_$_iter__93982_$_iter__94030(cljs.core.chunk_rest(s__94031__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__94033),null);
}
} else {
var li = cljs.core.first(s__94031__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null),website$pages$instaparser$output_html_$_iter__93982_$_iter__94030(cljs.core.rest(s__94031__$2)));
}
} else {
return null;
}
break;
}
});})(G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__))
,null,null));
});})(G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__))
;
return iter__4523__auto__(cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b));
})()], null);

break;
case "Ordered":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),(function (){var iter__4523__auto__ = ((function (G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__){
return (function website$pages$instaparser$output_html_$_iter__93982_$_iter__94041(s__94042){
return (new cljs.core.LazySeq(null,((function (G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__){
return (function (){
var s__94042__$1 = s__94042;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__94042__$1);
if(temp__5735__auto____$1){
var s__94042__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__94042__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__94042__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__94044 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__94043 = (0);
while(true){
if((i__94043 < size__4522__auto__)){
var li = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__94043);
cljs.core.chunk_append(b__94044,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null));

var G__94132 = (i__94043 + (1));
i__94043 = G__94132;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__94044),website$pages$instaparser$output_html_$_iter__93982_$_iter__94041(cljs.core.chunk_rest(s__94042__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__94044),null);
}
} else {
var li = cljs.core.first(s__94042__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),li)))], null),website$pages$instaparser$output_html_$_iter__93982_$_iter__94041(cljs.core.rest(s__94042__$2)));
}
} else {
return null;
}
break;
}
});})(G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__))
,null,null));
});})(G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__))
;
return iter__4523__auto__(cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b));
})()], null);

break;
case "Header":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.first(cljs.core.last(b)),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.take.cljs$core$IFn$_invoke$arity$2((cljs.core.count(b) - (2)),cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b))))], null);

break;
case "Code":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pre","pre",2118456869),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"code","code",1586293142),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.interpose.cljs$core$IFn$_invoke$arity$2("<br />",(function (){var iter__4523__auto__ = ((function (G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__){
return (function website$pages$instaparser$output_html_$_iter__93982_$_iter__94048(s__94049){
return (new cljs.core.LazySeq(null,((function (G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__){
return (function (){
var s__94049__$1 = s__94049;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__94049__$1);
if(temp__5735__auto____$1){
var s__94049__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__94049__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__94049__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__94051 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__94050 = (0);
while(true){
if((i__94050 < size__4522__auto__)){
var line = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__94050);
cljs.core.chunk_append(b__94051,cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),line)));

var G__94142 = (i__94050 + (1));
i__94050 = G__94142;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__94051),website$pages$instaparser$output_html_$_iter__93982_$_iter__94048(cljs.core.chunk_rest(s__94049__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__94051),null);
}
} else {
var line = cljs.core.first(s__94049__$2);
return cljs.core.cons(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),line)),website$pages$instaparser$output_html_$_iter__93982_$_iter__94048(cljs.core.rest(s__94049__$2)));
}
} else {
return null;
}
break;
}
});})(G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__))
,null,null));
});})(G__94028,G__94028__$1,b,s__93983__$2,temp__5735__auto__))
;
return iter__4523__auto__(cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b));
})()))], null)], null);

break;
case "Rule":
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"hr","hr",1377740067)], null);

break;
case "Paragraph":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.parse_span,cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),b)))], null);

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__94028__$1)].join('')));

}
})(),website$pages$instaparser$output_html_$_iter__93982(cljs.core.rest(s__93983__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(blocks);
})());
});
website.pages.instaparser.markdown_to_html = cljs.core.comp.cljs$core$IFn$_invoke$arity$2(website.pages.instaparser.output_html,website.pages.instaparser.parse_md);
/**
 * Parses markup into HTML.
 */
website.pages.instaparser.to_html = (function website$pages$instaparser$to_html(markup){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pre.bg-black.text-white.py-2","pre.bg-black.text-white.py-2",-229649690),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"code.text-xl","code.text-xl",-1678989920),(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__94058_94144 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__94059_94145 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__94060_94146 = true;
var _STAR_print_fn_STAR__temp_val__94061_94147 = ((function (_STAR_print_newline_STAR__orig_val__94058_94144,_STAR_print_fn_STAR__orig_val__94059_94145,_STAR_print_newline_STAR__temp_val__94060_94146,sb__4661__auto__){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__94058_94144,_STAR_print_fn_STAR__orig_val__94059_94145,_STAR_print_newline_STAR__temp_val__94060_94146,sb__4661__auto__))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__94060_94146;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__94061_94147;

try{cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1((website.pages.instaparser.sections.cljs$core$IFn$_invoke$arity$1 ? website.pages.instaparser.sections.cljs$core$IFn$_invoke$arity$1(markup) : website.pages.instaparser.sections.call(null,markup)));
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__94059_94145;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__94058_94144;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})()], null)], null);
});

//# sourceMappingURL=website.pages.instaparser.js.map
