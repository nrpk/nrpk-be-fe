goog.provide('shadow.dom');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('goog.dom.forms');
goog.require('goog.dom.classlist');
goog.require('goog.style');
goog.require('goog.style.transition');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('cljs.core.async');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__84029 = coll;
var G__84030 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__84029,G__84030) : shadow.dom.lazy_native_coll_seq.call(null,G__84029,G__84030));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4131__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return not_found;
}
});

shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
});

shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
});

shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
});

shadow.dom.NativeColl.cljs$lang$type = true;

shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl";

shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"shadow.dom/NativeColl");
});

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__84075 = arguments.length;
switch (G__84075) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
});

shadow.dom.query_one.cljs$lang$maxFixedArity = 2;

shadow.dom.query = (function shadow$dom$query(var_args){
var G__84089 = arguments.length;
switch (G__84089) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
});

shadow.dom.query.cljs$lang$maxFixedArity = 2;

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__84094 = arguments.length;
switch (G__84094) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
});

shadow.dom.by_id.cljs$lang$maxFixedArity = 2;

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__84117 = arguments.length;
switch (G__84117) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
e.cancelBubble = true;

e.returnValue = false;
}

return e;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4;

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__84158 = arguments.length;
switch (G__84158) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
var G__84166 = document;
var G__84167 = shadow.dom.dom_node(el);
return goog.dom.contains(G__84166,G__84167);
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
var G__84174 = shadow.dom.dom_node(parent);
var G__84175 = shadow.dom.dom_node(el);
return goog.dom.contains(G__84174,G__84175);
});

shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2;

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
var G__84178 = shadow.dom.dom_node(el);
var G__84179 = cls;
return goog.dom.classlist.add(G__84178,G__84179);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
var G__84181 = shadow.dom.dom_node(el);
var G__84182 = cls;
return goog.dom.classlist.remove(G__84181,G__84182);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__84201 = arguments.length;
switch (G__84201) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
var G__84217 = shadow.dom.dom_node(el);
var G__84218 = cls;
return goog.dom.classlist.toggle(G__84217,G__84218);
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
});

shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3;

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e84224){if((e84224 instanceof Object)){
var e = e84224;
return console.log("didnt support attachEvent",el,e);
} else {
throw e84224;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__84237 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__84238 = null;
var count__84239 = (0);
var i__84240 = (0);
while(true){
if((i__84240 < count__84239)){
var el = chunk__84238.cljs$core$IIndexed$_nth$arity$2(null,i__84240);
var handler_85095__$1 = ((function (seq__84237,chunk__84238,count__84239,i__84240,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__84237,chunk__84238,count__84239,i__84240,el))
;
var G__84250_85096 = el;
var G__84251_85097 = cljs.core.name(ev);
var G__84252_85098 = handler_85095__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__84250_85096,G__84251_85097,G__84252_85098) : shadow.dom.dom_listen.call(null,G__84250_85096,G__84251_85097,G__84252_85098));


var G__85099 = seq__84237;
var G__85100 = chunk__84238;
var G__85101 = count__84239;
var G__85102 = (i__84240 + (1));
seq__84237 = G__85099;
chunk__84238 = G__85100;
count__84239 = G__85101;
i__84240 = G__85102;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__84237);
if(temp__5735__auto__){
var seq__84237__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__84237__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__84237__$1);
var G__85114 = cljs.core.chunk_rest(seq__84237__$1);
var G__85115 = c__4550__auto__;
var G__85116 = cljs.core.count(c__4550__auto__);
var G__85117 = (0);
seq__84237 = G__85114;
chunk__84238 = G__85115;
count__84239 = G__85116;
i__84240 = G__85117;
continue;
} else {
var el = cljs.core.first(seq__84237__$1);
var handler_85118__$1 = ((function (seq__84237,chunk__84238,count__84239,i__84240,el,seq__84237__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__84237,chunk__84238,count__84239,i__84240,el,seq__84237__$1,temp__5735__auto__))
;
var G__84257_85119 = el;
var G__84258_85120 = cljs.core.name(ev);
var G__84259_85121 = handler_85118__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__84257_85119,G__84258_85120,G__84259_85121) : shadow.dom.dom_listen.call(null,G__84257_85119,G__84258_85120,G__84259_85121));


var G__85122 = cljs.core.next(seq__84237__$1);
var G__85123 = null;
var G__85124 = (0);
var G__85125 = (0);
seq__84237 = G__85122;
chunk__84238 = G__85123;
count__84239 = G__85124;
i__84240 = G__85125;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__84262 = arguments.length;
switch (G__84262) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
var G__84271 = shadow.dom.dom_node(el);
var G__84272 = cljs.core.name(ev);
var G__84273 = handler__$1;
return (shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__84271,G__84272,G__84273) : shadow.dom.dom_listen.call(null,G__84271,G__84272,G__84273));
}
});

shadow.dom.on.cljs$lang$maxFixedArity = 4;

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
var G__84280 = shadow.dom.dom_node(el);
var G__84281 = cljs.core.name(ev);
var G__84282 = handler;
return (shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3(G__84280,G__84281,G__84282) : shadow.dom.dom_listen_remove.call(null,G__84280,G__84281,G__84282));
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__84285 = cljs.core.seq(events);
var chunk__84286 = null;
var count__84287 = (0);
var i__84288 = (0);
while(true){
if((i__84288 < count__84287)){
var vec__84305 = chunk__84286.cljs$core$IIndexed$_nth$arity$2(null,i__84288);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84305,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84305,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__85133 = seq__84285;
var G__85134 = chunk__84286;
var G__85135 = count__84287;
var G__85136 = (i__84288 + (1));
seq__84285 = G__85133;
chunk__84286 = G__85134;
count__84287 = G__85135;
i__84288 = G__85136;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__84285);
if(temp__5735__auto__){
var seq__84285__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__84285__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__84285__$1);
var G__85137 = cljs.core.chunk_rest(seq__84285__$1);
var G__85138 = c__4550__auto__;
var G__85139 = cljs.core.count(c__4550__auto__);
var G__85140 = (0);
seq__84285 = G__85137;
chunk__84286 = G__85138;
count__84287 = G__85139;
i__84288 = G__85140;
continue;
} else {
var vec__84312 = cljs.core.first(seq__84285__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84312,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84312,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__85141 = cljs.core.next(seq__84285__$1);
var G__85142 = null;
var G__85143 = (0);
var G__85144 = (0);
seq__84285 = G__85141;
chunk__84286 = G__85142;
count__84287 = G__85143;
i__84288 = G__85144;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__84321 = cljs.core.seq(styles);
var chunk__84322 = null;
var count__84323 = (0);
var i__84324 = (0);
while(true){
if((i__84324 < count__84323)){
var vec__84348 = chunk__84322.cljs$core$IIndexed$_nth$arity$2(null,i__84324);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84348,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84348,(1),null);
var G__84352_85146 = dom;
var G__84353_85147 = cljs.core.name(k);
var G__84354_85148 = (((v == null))?"":v);
goog.style.setStyle(G__84352_85146,G__84353_85147,G__84354_85148);


var G__85149 = seq__84321;
var G__85150 = chunk__84322;
var G__85151 = count__84323;
var G__85152 = (i__84324 + (1));
seq__84321 = G__85149;
chunk__84322 = G__85150;
count__84323 = G__85151;
i__84324 = G__85152;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__84321);
if(temp__5735__auto__){
var seq__84321__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__84321__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__84321__$1);
var G__85155 = cljs.core.chunk_rest(seq__84321__$1);
var G__85156 = c__4550__auto__;
var G__85157 = cljs.core.count(c__4550__auto__);
var G__85158 = (0);
seq__84321 = G__85155;
chunk__84322 = G__85156;
count__84323 = G__85157;
i__84324 = G__85158;
continue;
} else {
var vec__84378 = cljs.core.first(seq__84321__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84378,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84378,(1),null);
var G__84381_85159 = dom;
var G__84382_85160 = cljs.core.name(k);
var G__84383_85161 = (((v == null))?"":v);
goog.style.setStyle(G__84381_85159,G__84382_85160,G__84383_85161);


var G__85162 = cljs.core.next(seq__84321__$1);
var G__85163 = null;
var G__85164 = (0);
var G__85165 = (0);
seq__84321 = G__85162;
chunk__84322 = G__85163;
count__84323 = G__85164;
i__84324 = G__85165;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__84393_85166 = key;
var G__84393_85167__$1 = (((G__84393_85166 instanceof cljs.core.Keyword))?G__84393_85166.fqn:null);
switch (G__84393_85167__$1) {
case "id":
el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "class":
el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "for":
el.htmlFor = value;

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_85176 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4131__auto__ = goog.string.startsWith(ks_85176,"data-");
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return goog.string.startsWith(ks_85176,"aria-");
}
})())){
el.setAttribute(ks_85176,value);
} else {
(el[ks_85176] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
var G__84422 = shadow.dom.dom_node(el);
var G__84423 = cls;
return goog.dom.classlist.contains(G__84422,G__84423);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__84448){
var map__84449 = p__84448;
var map__84449__$1 = (((((!((map__84449 == null))))?(((((map__84449.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__84449.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__84449):map__84449);
var props = map__84449__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__84449__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__84454 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84454,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84454,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84454,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__84459 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__84459,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__84459;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__84462 = arguments.length;
switch (G__84462) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$lang$maxFixedArity = 2;

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__84484){
var vec__84487 = p__84484;
var seq__84488 = cljs.core.seq(vec__84487);
var first__84489 = cljs.core.first(seq__84488);
var seq__84488__$1 = cljs.core.next(seq__84488);
var nn = first__84489;
var first__84489__$1 = cljs.core.first(seq__84488__$1);
var seq__84488__$2 = cljs.core.next(seq__84488__$1);
var np = first__84489__$1;
var nc = seq__84488__$2;
var node = vec__84487;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__84491 = nn;
var G__84492 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__84491,G__84492) : create_fn.call(null,G__84491,G__84492));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__84496 = nn;
var G__84497 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__84496,G__84497) : create_fn.call(null,G__84496,G__84497));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__84502 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84502,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84502,(1),null);
var seq__84505_85198 = cljs.core.seq(node_children);
var chunk__84506_85199 = null;
var count__84507_85200 = (0);
var i__84508_85201 = (0);
while(true){
if((i__84508_85201 < count__84507_85200)){
var child_struct_85202 = chunk__84506_85199.cljs$core$IIndexed$_nth$arity$2(null,i__84508_85201);
var children_85203 = shadow.dom.dom_node(child_struct_85202);
if(cljs.core.seq_QMARK_(children_85203)){
var seq__84551_85204 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_85203));
var chunk__84553_85205 = null;
var count__84554_85206 = (0);
var i__84555_85207 = (0);
while(true){
if((i__84555_85207 < count__84554_85206)){
var child_85208 = chunk__84553_85205.cljs$core$IIndexed$_nth$arity$2(null,i__84555_85207);
if(cljs.core.truth_(child_85208)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_85208);


var G__85209 = seq__84551_85204;
var G__85210 = chunk__84553_85205;
var G__85211 = count__84554_85206;
var G__85212 = (i__84555_85207 + (1));
seq__84551_85204 = G__85209;
chunk__84553_85205 = G__85210;
count__84554_85206 = G__85211;
i__84555_85207 = G__85212;
continue;
} else {
var G__85213 = seq__84551_85204;
var G__85214 = chunk__84553_85205;
var G__85215 = count__84554_85206;
var G__85216 = (i__84555_85207 + (1));
seq__84551_85204 = G__85213;
chunk__84553_85205 = G__85214;
count__84554_85206 = G__85215;
i__84555_85207 = G__85216;
continue;
}
} else {
var temp__5735__auto___85217 = cljs.core.seq(seq__84551_85204);
if(temp__5735__auto___85217){
var seq__84551_85218__$1 = temp__5735__auto___85217;
if(cljs.core.chunked_seq_QMARK_(seq__84551_85218__$1)){
var c__4550__auto___85219 = cljs.core.chunk_first(seq__84551_85218__$1);
var G__85220 = cljs.core.chunk_rest(seq__84551_85218__$1);
var G__85221 = c__4550__auto___85219;
var G__85222 = cljs.core.count(c__4550__auto___85219);
var G__85223 = (0);
seq__84551_85204 = G__85220;
chunk__84553_85205 = G__85221;
count__84554_85206 = G__85222;
i__84555_85207 = G__85223;
continue;
} else {
var child_85224 = cljs.core.first(seq__84551_85218__$1);
if(cljs.core.truth_(child_85224)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_85224);


var G__85225 = cljs.core.next(seq__84551_85218__$1);
var G__85226 = null;
var G__85227 = (0);
var G__85228 = (0);
seq__84551_85204 = G__85225;
chunk__84553_85205 = G__85226;
count__84554_85206 = G__85227;
i__84555_85207 = G__85228;
continue;
} else {
var G__85231 = cljs.core.next(seq__84551_85218__$1);
var G__85232 = null;
var G__85233 = (0);
var G__85234 = (0);
seq__84551_85204 = G__85231;
chunk__84553_85205 = G__85232;
count__84554_85206 = G__85233;
i__84555_85207 = G__85234;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_85203);
}


var G__85241 = seq__84505_85198;
var G__85242 = chunk__84506_85199;
var G__85243 = count__84507_85200;
var G__85244 = (i__84508_85201 + (1));
seq__84505_85198 = G__85241;
chunk__84506_85199 = G__85242;
count__84507_85200 = G__85243;
i__84508_85201 = G__85244;
continue;
} else {
var temp__5735__auto___85245 = cljs.core.seq(seq__84505_85198);
if(temp__5735__auto___85245){
var seq__84505_85247__$1 = temp__5735__auto___85245;
if(cljs.core.chunked_seq_QMARK_(seq__84505_85247__$1)){
var c__4550__auto___85249 = cljs.core.chunk_first(seq__84505_85247__$1);
var G__85250 = cljs.core.chunk_rest(seq__84505_85247__$1);
var G__85251 = c__4550__auto___85249;
var G__85252 = cljs.core.count(c__4550__auto___85249);
var G__85253 = (0);
seq__84505_85198 = G__85250;
chunk__84506_85199 = G__85251;
count__84507_85200 = G__85252;
i__84508_85201 = G__85253;
continue;
} else {
var child_struct_85256 = cljs.core.first(seq__84505_85247__$1);
var children_85257 = shadow.dom.dom_node(child_struct_85256);
if(cljs.core.seq_QMARK_(children_85257)){
var seq__84565_85258 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_85257));
var chunk__84567_85259 = null;
var count__84568_85260 = (0);
var i__84569_85261 = (0);
while(true){
if((i__84569_85261 < count__84568_85260)){
var child_85266 = chunk__84567_85259.cljs$core$IIndexed$_nth$arity$2(null,i__84569_85261);
if(cljs.core.truth_(child_85266)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_85266);


var G__85267 = seq__84565_85258;
var G__85268 = chunk__84567_85259;
var G__85269 = count__84568_85260;
var G__85270 = (i__84569_85261 + (1));
seq__84565_85258 = G__85267;
chunk__84567_85259 = G__85268;
count__84568_85260 = G__85269;
i__84569_85261 = G__85270;
continue;
} else {
var G__85271 = seq__84565_85258;
var G__85272 = chunk__84567_85259;
var G__85273 = count__84568_85260;
var G__85274 = (i__84569_85261 + (1));
seq__84565_85258 = G__85271;
chunk__84567_85259 = G__85272;
count__84568_85260 = G__85273;
i__84569_85261 = G__85274;
continue;
}
} else {
var temp__5735__auto___85275__$1 = cljs.core.seq(seq__84565_85258);
if(temp__5735__auto___85275__$1){
var seq__84565_85276__$1 = temp__5735__auto___85275__$1;
if(cljs.core.chunked_seq_QMARK_(seq__84565_85276__$1)){
var c__4550__auto___85280 = cljs.core.chunk_first(seq__84565_85276__$1);
var G__85281 = cljs.core.chunk_rest(seq__84565_85276__$1);
var G__85282 = c__4550__auto___85280;
var G__85283 = cljs.core.count(c__4550__auto___85280);
var G__85284 = (0);
seq__84565_85258 = G__85281;
chunk__84567_85259 = G__85282;
count__84568_85260 = G__85283;
i__84569_85261 = G__85284;
continue;
} else {
var child_85285 = cljs.core.first(seq__84565_85276__$1);
if(cljs.core.truth_(child_85285)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_85285);


var G__85286 = cljs.core.next(seq__84565_85276__$1);
var G__85287 = null;
var G__85288 = (0);
var G__85289 = (0);
seq__84565_85258 = G__85286;
chunk__84567_85259 = G__85287;
count__84568_85260 = G__85288;
i__84569_85261 = G__85289;
continue;
} else {
var G__85291 = cljs.core.next(seq__84565_85276__$1);
var G__85292 = null;
var G__85293 = (0);
var G__85294 = (0);
seq__84565_85258 = G__85291;
chunk__84567_85259 = G__85292;
count__84568_85260 = G__85293;
i__84569_85261 = G__85294;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_85257);
}


var G__85296 = cljs.core.next(seq__84505_85247__$1);
var G__85297 = null;
var G__85298 = (0);
var G__85299 = (0);
seq__84505_85198 = G__85296;
chunk__84506_85199 = G__85297;
count__84507_85200 = G__85298;
i__84508_85201 = G__85299;
continue;
}
} else {
}
}
break;
}

return node;
});
cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
});

cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
});
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
var G__84607 = shadow.dom.dom_node(node);
return goog.dom.removeChildren(G__84607);
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__84613 = cljs.core.seq(node);
var chunk__84614 = null;
var count__84615 = (0);
var i__84616 = (0);
while(true){
if((i__84616 < count__84615)){
var n = chunk__84614.cljs$core$IIndexed$_nth$arity$2(null,i__84616);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__85304 = seq__84613;
var G__85305 = chunk__84614;
var G__85306 = count__84615;
var G__85307 = (i__84616 + (1));
seq__84613 = G__85304;
chunk__84614 = G__85305;
count__84615 = G__85306;
i__84616 = G__85307;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__84613);
if(temp__5735__auto__){
var seq__84613__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__84613__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__84613__$1);
var G__85316 = cljs.core.chunk_rest(seq__84613__$1);
var G__85317 = c__4550__auto__;
var G__85318 = cljs.core.count(c__4550__auto__);
var G__85319 = (0);
seq__84613 = G__85316;
chunk__84614 = G__85317;
count__84615 = G__85318;
i__84616 = G__85319;
continue;
} else {
var n = cljs.core.first(seq__84613__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__85320 = cljs.core.next(seq__84613__$1);
var G__85321 = null;
var G__85322 = (0);
var G__85323 = (0);
seq__84613 = G__85320;
chunk__84614 = G__85321;
count__84615 = G__85322;
i__84616 = G__85323;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
var G__84619 = shadow.dom.dom_node(new$);
var G__84620 = shadow.dom.dom_node(old);
return goog.dom.replaceNode(G__84619,G__84620);
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__84623 = arguments.length;
switch (G__84623) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return shadow.dom.dom_node(el).innerText = new_text;
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
});

shadow.dom.text.cljs$lang$maxFixedArity = 2;

shadow.dom.check = (function shadow$dom$check(var_args){
var G__84636 = arguments.length;
switch (G__84636) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return shadow.dom.dom_node(el).checked = checked;
});

shadow.dom.check.cljs$lang$maxFixedArity = 2;

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__84659 = arguments.length;
switch (G__84659) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4131__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return default$;
}
});

shadow.dom.attr.cljs$lang$maxFixedArity = 3;

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return shadow.dom.dom_node(node).innerHTML = text;
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4736__auto__ = [];
var len__4730__auto___85357 = arguments.length;
var i__4731__auto___85358 = (0);
while(true){
if((i__4731__auto___85358 < len__4730__auto___85357)){
args__4736__auto__.push((arguments[i__4731__auto___85358]));

var G__85359 = (i__4731__auto___85358 + (1));
i__4731__auto___85358 = G__85359;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__84708_85362 = cljs.core.seq(nodes);
var chunk__84709_85363 = null;
var count__84710_85364 = (0);
var i__84711_85366 = (0);
while(true){
if((i__84711_85366 < count__84710_85364)){
var node_85371 = chunk__84709_85363.cljs$core$IIndexed$_nth$arity$2(null,i__84711_85366);
fragment.appendChild(shadow.dom._to_dom(node_85371));


var G__85372 = seq__84708_85362;
var G__85373 = chunk__84709_85363;
var G__85374 = count__84710_85364;
var G__85375 = (i__84711_85366 + (1));
seq__84708_85362 = G__85372;
chunk__84709_85363 = G__85373;
count__84710_85364 = G__85374;
i__84711_85366 = G__85375;
continue;
} else {
var temp__5735__auto___85376 = cljs.core.seq(seq__84708_85362);
if(temp__5735__auto___85376){
var seq__84708_85377__$1 = temp__5735__auto___85376;
if(cljs.core.chunked_seq_QMARK_(seq__84708_85377__$1)){
var c__4550__auto___85378 = cljs.core.chunk_first(seq__84708_85377__$1);
var G__85379 = cljs.core.chunk_rest(seq__84708_85377__$1);
var G__85380 = c__4550__auto___85378;
var G__85381 = cljs.core.count(c__4550__auto___85378);
var G__85382 = (0);
seq__84708_85362 = G__85379;
chunk__84709_85363 = G__85380;
count__84710_85364 = G__85381;
i__84711_85366 = G__85382;
continue;
} else {
var node_85384 = cljs.core.first(seq__84708_85377__$1);
fragment.appendChild(shadow.dom._to_dom(node_85384));


var G__85388 = cljs.core.next(seq__84708_85377__$1);
var G__85389 = null;
var G__85390 = (0);
var G__85391 = (0);
seq__84708_85362 = G__85388;
chunk__84709_85363 = G__85389;
count__84710_85364 = G__85390;
i__84711_85366 = G__85391;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
});

shadow.dom.fragment.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
shadow.dom.fragment.cljs$lang$applyTo = (function (seq84702){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq84702));
});

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__84718_85393 = cljs.core.seq(scripts);
var chunk__84719_85394 = null;
var count__84720_85395 = (0);
var i__84721_85396 = (0);
while(true){
if((i__84721_85396 < count__84720_85395)){
var vec__84733_85398 = chunk__84719_85394.cljs$core$IIndexed$_nth$arity$2(null,i__84721_85396);
var script_tag_85399 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84733_85398,(0),null);
var script_body_85400 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84733_85398,(1),null);
eval(script_body_85400);


var G__85401 = seq__84718_85393;
var G__85402 = chunk__84719_85394;
var G__85403 = count__84720_85395;
var G__85404 = (i__84721_85396 + (1));
seq__84718_85393 = G__85401;
chunk__84719_85394 = G__85402;
count__84720_85395 = G__85403;
i__84721_85396 = G__85404;
continue;
} else {
var temp__5735__auto___85408 = cljs.core.seq(seq__84718_85393);
if(temp__5735__auto___85408){
var seq__84718_85409__$1 = temp__5735__auto___85408;
if(cljs.core.chunked_seq_QMARK_(seq__84718_85409__$1)){
var c__4550__auto___85411 = cljs.core.chunk_first(seq__84718_85409__$1);
var G__85412 = cljs.core.chunk_rest(seq__84718_85409__$1);
var G__85413 = c__4550__auto___85411;
var G__85414 = cljs.core.count(c__4550__auto___85411);
var G__85415 = (0);
seq__84718_85393 = G__85412;
chunk__84719_85394 = G__85413;
count__84720_85395 = G__85414;
i__84721_85396 = G__85415;
continue;
} else {
var vec__84743_85416 = cljs.core.first(seq__84718_85409__$1);
var script_tag_85417 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84743_85416,(0),null);
var script_body_85418 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84743_85416,(1),null);
eval(script_body_85418);


var G__85422 = cljs.core.next(seq__84718_85409__$1);
var G__85423 = null;
var G__85424 = (0);
var G__85425 = (0);
seq__84718_85393 = G__85422;
chunk__84719_85394 = G__85423;
count__84720_85395 = G__85424;
i__84721_85396 = G__85425;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (scripts){
return (function (s__$1,p__84748){
var vec__84750 = p__84748;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84750,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84750,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
});})(scripts))
,s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
el.innerHTML = s;

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
var G__84755 = shadow.dom.dom_node(el);
var G__84756 = cls;
return goog.dom.getAncestorByClass(G__84755,G__84756);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__84759 = arguments.length;
switch (G__84759) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
var G__84760 = shadow.dom.dom_node(el);
var G__84761 = cljs.core.name(tag);
return goog.dom.getAncestorByTagNameAndClass(G__84760,G__84761);
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
var G__84762 = shadow.dom.dom_node(el);
var G__84763 = cljs.core.name(tag);
var G__84764 = cljs.core.name(cls);
return goog.dom.getAncestorByTagNameAndClass(G__84762,G__84763,G__84764);
});

shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3;

shadow.dom.get_value = (function shadow$dom$get_value(dom){
var G__84767 = shadow.dom.dom_node(dom);
return goog.dom.forms.getValue(G__84767);
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
var G__84776 = shadow.dom.dom_node(dom);
var G__84777 = value;
return goog.dom.forms.setValue(G__84776,G__84777);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__84784 = cljs.core.seq(style_keys);
var chunk__84785 = null;
var count__84786 = (0);
var i__84787 = (0);
while(true){
if((i__84787 < count__84786)){
var it = chunk__84785.cljs$core$IIndexed$_nth$arity$2(null,i__84787);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__85450 = seq__84784;
var G__85451 = chunk__84785;
var G__85452 = count__84786;
var G__85453 = (i__84787 + (1));
seq__84784 = G__85450;
chunk__84785 = G__85451;
count__84786 = G__85452;
i__84787 = G__85453;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__84784);
if(temp__5735__auto__){
var seq__84784__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__84784__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__84784__$1);
var G__85454 = cljs.core.chunk_rest(seq__84784__$1);
var G__85455 = c__4550__auto__;
var G__85456 = cljs.core.count(c__4550__auto__);
var G__85457 = (0);
seq__84784 = G__85454;
chunk__84785 = G__85455;
count__84786 = G__85456;
i__84787 = G__85457;
continue;
} else {
var it = cljs.core.first(seq__84784__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__85458 = cljs.core.next(seq__84784__$1);
var G__85459 = null;
var G__85460 = (0);
var G__85461 = (0);
seq__84784 = G__85458;
chunk__84785 = G__85459;
count__84786 = G__85460;
i__84787 = G__85461;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k84789,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__84804 = k84789;
var G__84804__$1 = (((G__84804 instanceof cljs.core.Keyword))?G__84804.fqn:null);
switch (G__84804__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k84789,else__4388__auto__);

}
});

shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__84808){
var vec__84809 = p__84808;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84809,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84809,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__84788){
var self__ = this;
var G__84788__$1 = this;
return (new cljs.core.RecordIter((0),G__84788__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__84825 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__84825(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this84790,other84791){
var self__ = this;
var this84790__$1 = this;
return (((!((other84791 == null)))) && ((this84790__$1.constructor === other84791.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this84790__$1.x,other84791.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this84790__$1.y,other84791.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this84790__$1.__extmap,other84791.__extmap)));
});

shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__84788){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__84830 = cljs.core.keyword_identical_QMARK_;
var expr__84831 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__84833 = new cljs.core.Keyword(null,"x","x",2099068185);
var G__84834 = expr__84831;
return (pred__84830.cljs$core$IFn$_invoke$arity$2 ? pred__84830.cljs$core$IFn$_invoke$arity$2(G__84833,G__84834) : pred__84830.call(null,G__84833,G__84834));
})())){
return (new shadow.dom.Coordinate(G__84788,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__84836 = new cljs.core.Keyword(null,"y","y",-1757859776);
var G__84837 = expr__84831;
return (pred__84830.cljs$core$IFn$_invoke$arity$2 ? pred__84830.cljs$core$IFn$_invoke$arity$2(G__84836,G__84837) : pred__84830.call(null,G__84836,G__84837));
})())){
return (new shadow.dom.Coordinate(self__.x,G__84788,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__84788),null));
}
}
});

shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__84788){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__84788,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
});

shadow.dom.Coordinate.cljs$lang$type = true;

shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
});

shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Coordinate");
});

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__84796){
var extmap__4424__auto__ = (function (){var G__84841 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__84796,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__84796)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__84841);
} else {
return G__84841;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__84796),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__84796),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = (function (){var G__84843 = shadow.dom.dom_node(el);
return goog.style.getPosition(G__84843);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = (function (){var G__84845 = shadow.dom.dom_node(el);
return goog.style.getClientPosition(G__84845);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = (function (){var G__84849 = shadow.dom.dom_node(el);
return goog.style.getPageOffset(G__84849);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k84851,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__84855 = k84851;
var G__84855__$1 = (((G__84855 instanceof cljs.core.Keyword))?G__84855.fqn:null);
switch (G__84855__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k84851,else__4388__auto__);

}
});

shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__84856){
var vec__84857 = p__84856;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84857,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84857,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Size{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__84850){
var self__ = this;
var G__84850__$1 = this;
return (new cljs.core.RecordIter((0),G__84850__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__84860 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__84860(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this84852,other84853){
var self__ = this;
var this84852__$1 = this;
return (((!((other84853 == null)))) && ((this84852__$1.constructor === other84853.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this84852__$1.w,other84853.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this84852__$1.h,other84853.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this84852__$1.__extmap,other84853.__extmap)));
});

shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__84850){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__84861 = cljs.core.keyword_identical_QMARK_;
var expr__84862 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__84864 = new cljs.core.Keyword(null,"w","w",354169001);
var G__84865 = expr__84862;
return (pred__84861.cljs$core$IFn$_invoke$arity$2 ? pred__84861.cljs$core$IFn$_invoke$arity$2(G__84864,G__84865) : pred__84861.call(null,G__84864,G__84865));
})())){
return (new shadow.dom.Size(G__84850,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__84866 = new cljs.core.Keyword(null,"h","h",1109658740);
var G__84867 = expr__84862;
return (pred__84861.cljs$core$IFn$_invoke$arity$2 ? pred__84861.cljs$core$IFn$_invoke$arity$2(G__84866,G__84867) : pred__84861.call(null,G__84866,G__84867));
})())){
return (new shadow.dom.Size(self__.w,G__84850,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__84850),null));
}
}
});

shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__84850){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__84850,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
});

shadow.dom.Size.cljs$lang$type = true;

shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
});

shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Size");
});

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__84854){
var extmap__4424__auto__ = (function (){var G__84869 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__84854,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__84854)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__84869);
} else {
return G__84869;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__84854),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__84854),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj((function (){var G__84870 = shadow.dom.dom_node(el);
return goog.style.getSize(G__84870);
})());
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(shadow.dom.get_size(el));
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4604__auto__ = opts;
var l__4605__auto__ = a__4604__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4605__auto__)){
var G__85551 = (i + (1));
var G__85552 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__85551;
ret = G__85552;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",cljs.core.str.cljs$core$IFn$_invoke$arity$1(clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__84882){
var vec__84883 = p__84882;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84883,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84883,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params)))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__84887 = arguments.length;
switch (G__84887) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
});

shadow.dom.redirect.cljs$lang$maxFixedArity = 2;

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return document.location.href = document.location.href;
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__84902_85566 = new_node;
var G__84903_85567 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingAfter(G__84902_85566,G__84903_85567);

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__84910_85568 = new_node;
var G__84911_85569 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingBefore(G__84910_85568,G__84911_85569);

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__85570 = ps;
var G__85571 = (i + (1));
el__$1 = G__85570;
i = G__85571;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
var G__84932 = shadow.dom.dom_node(el);
return goog.dom.getParentElement(G__84932);
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,((function (parent){
return (function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
});})(parent))
,null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
var G__84936 = shadow.dom.dom_node(el);
return goog.dom.getNextElementSibling(G__84936);
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
var G__84940 = shadow.dom.dom_node(el);
return goog.dom.getPreviousElementSibling(G__84940);
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__84941 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84941,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84941,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84941,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__84944_85575 = cljs.core.seq(props);
var chunk__84945_85576 = null;
var count__84946_85577 = (0);
var i__84947_85578 = (0);
while(true){
if((i__84947_85578 < count__84946_85577)){
var vec__84954_85579 = chunk__84945_85576.cljs$core$IIndexed$_nth$arity$2(null,i__84947_85578);
var k_85580 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84954_85579,(0),null);
var v_85581 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84954_85579,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_85580);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_85580),v_85581);


var G__85582 = seq__84944_85575;
var G__85583 = chunk__84945_85576;
var G__85584 = count__84946_85577;
var G__85585 = (i__84947_85578 + (1));
seq__84944_85575 = G__85582;
chunk__84945_85576 = G__85583;
count__84946_85577 = G__85584;
i__84947_85578 = G__85585;
continue;
} else {
var temp__5735__auto___85586 = cljs.core.seq(seq__84944_85575);
if(temp__5735__auto___85586){
var seq__84944_85587__$1 = temp__5735__auto___85586;
if(cljs.core.chunked_seq_QMARK_(seq__84944_85587__$1)){
var c__4550__auto___85588 = cljs.core.chunk_first(seq__84944_85587__$1);
var G__85589 = cljs.core.chunk_rest(seq__84944_85587__$1);
var G__85590 = c__4550__auto___85588;
var G__85591 = cljs.core.count(c__4550__auto___85588);
var G__85592 = (0);
seq__84944_85575 = G__85589;
chunk__84945_85576 = G__85590;
count__84946_85577 = G__85591;
i__84947_85578 = G__85592;
continue;
} else {
var vec__84957_85593 = cljs.core.first(seq__84944_85587__$1);
var k_85594 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84957_85593,(0),null);
var v_85595 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84957_85593,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_85594);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_85594),v_85595);


var G__85596 = cljs.core.next(seq__84944_85587__$1);
var G__85597 = null;
var G__85598 = (0);
var G__85599 = (0);
seq__84944_85575 = G__85596;
chunk__84945_85576 = G__85597;
count__84946_85577 = G__85598;
i__84947_85578 = G__85599;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__84961 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84961,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__84961,(1),null);
var seq__84964_85606 = cljs.core.seq(node_children);
var chunk__84966_85607 = null;
var count__84967_85608 = (0);
var i__84968_85609 = (0);
while(true){
if((i__84968_85609 < count__84967_85608)){
var child_struct_85610 = chunk__84966_85607.cljs$core$IIndexed$_nth$arity$2(null,i__84968_85609);
if((!((child_struct_85610 == null)))){
if(typeof child_struct_85610 === 'string'){
var text_85611 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_85611),child_struct_85610].join(''));
} else {
var children_85612 = shadow.dom.svg_node(child_struct_85610);
if(cljs.core.seq_QMARK_(children_85612)){
var seq__84993_85613 = cljs.core.seq(children_85612);
var chunk__84995_85614 = null;
var count__84996_85615 = (0);
var i__84997_85616 = (0);
while(true){
if((i__84997_85616 < count__84996_85615)){
var child_85617 = chunk__84995_85614.cljs$core$IIndexed$_nth$arity$2(null,i__84997_85616);
if(cljs.core.truth_(child_85617)){
node.appendChild(child_85617);


var G__85618 = seq__84993_85613;
var G__85619 = chunk__84995_85614;
var G__85620 = count__84996_85615;
var G__85621 = (i__84997_85616 + (1));
seq__84993_85613 = G__85618;
chunk__84995_85614 = G__85619;
count__84996_85615 = G__85620;
i__84997_85616 = G__85621;
continue;
} else {
var G__85622 = seq__84993_85613;
var G__85623 = chunk__84995_85614;
var G__85624 = count__84996_85615;
var G__85625 = (i__84997_85616 + (1));
seq__84993_85613 = G__85622;
chunk__84995_85614 = G__85623;
count__84996_85615 = G__85624;
i__84997_85616 = G__85625;
continue;
}
} else {
var temp__5735__auto___85628 = cljs.core.seq(seq__84993_85613);
if(temp__5735__auto___85628){
var seq__84993_85629__$1 = temp__5735__auto___85628;
if(cljs.core.chunked_seq_QMARK_(seq__84993_85629__$1)){
var c__4550__auto___85630 = cljs.core.chunk_first(seq__84993_85629__$1);
var G__85631 = cljs.core.chunk_rest(seq__84993_85629__$1);
var G__85632 = c__4550__auto___85630;
var G__85633 = cljs.core.count(c__4550__auto___85630);
var G__85634 = (0);
seq__84993_85613 = G__85631;
chunk__84995_85614 = G__85632;
count__84996_85615 = G__85633;
i__84997_85616 = G__85634;
continue;
} else {
var child_85635 = cljs.core.first(seq__84993_85629__$1);
if(cljs.core.truth_(child_85635)){
node.appendChild(child_85635);


var G__85636 = cljs.core.next(seq__84993_85629__$1);
var G__85637 = null;
var G__85638 = (0);
var G__85639 = (0);
seq__84993_85613 = G__85636;
chunk__84995_85614 = G__85637;
count__84996_85615 = G__85638;
i__84997_85616 = G__85639;
continue;
} else {
var G__85640 = cljs.core.next(seq__84993_85629__$1);
var G__85641 = null;
var G__85642 = (0);
var G__85643 = (0);
seq__84993_85613 = G__85640;
chunk__84995_85614 = G__85641;
count__84996_85615 = G__85642;
i__84997_85616 = G__85643;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_85612);
}
}


var G__85646 = seq__84964_85606;
var G__85647 = chunk__84966_85607;
var G__85648 = count__84967_85608;
var G__85649 = (i__84968_85609 + (1));
seq__84964_85606 = G__85646;
chunk__84966_85607 = G__85647;
count__84967_85608 = G__85648;
i__84968_85609 = G__85649;
continue;
} else {
var G__85650 = seq__84964_85606;
var G__85651 = chunk__84966_85607;
var G__85652 = count__84967_85608;
var G__85653 = (i__84968_85609 + (1));
seq__84964_85606 = G__85650;
chunk__84966_85607 = G__85651;
count__84967_85608 = G__85652;
i__84968_85609 = G__85653;
continue;
}
} else {
var temp__5735__auto___85654 = cljs.core.seq(seq__84964_85606);
if(temp__5735__auto___85654){
var seq__84964_85655__$1 = temp__5735__auto___85654;
if(cljs.core.chunked_seq_QMARK_(seq__84964_85655__$1)){
var c__4550__auto___85656 = cljs.core.chunk_first(seq__84964_85655__$1);
var G__85657 = cljs.core.chunk_rest(seq__84964_85655__$1);
var G__85658 = c__4550__auto___85656;
var G__85659 = cljs.core.count(c__4550__auto___85656);
var G__85660 = (0);
seq__84964_85606 = G__85657;
chunk__84966_85607 = G__85658;
count__84967_85608 = G__85659;
i__84968_85609 = G__85660;
continue;
} else {
var child_struct_85661 = cljs.core.first(seq__84964_85655__$1);
if((!((child_struct_85661 == null)))){
if(typeof child_struct_85661 === 'string'){
var text_85662 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_85662),child_struct_85661].join(''));
} else {
var children_85665 = shadow.dom.svg_node(child_struct_85661);
if(cljs.core.seq_QMARK_(children_85665)){
var seq__85001_85666 = cljs.core.seq(children_85665);
var chunk__85003_85667 = null;
var count__85004_85668 = (0);
var i__85005_85669 = (0);
while(true){
if((i__85005_85669 < count__85004_85668)){
var child_85671 = chunk__85003_85667.cljs$core$IIndexed$_nth$arity$2(null,i__85005_85669);
if(cljs.core.truth_(child_85671)){
node.appendChild(child_85671);


var G__85672 = seq__85001_85666;
var G__85673 = chunk__85003_85667;
var G__85674 = count__85004_85668;
var G__85675 = (i__85005_85669 + (1));
seq__85001_85666 = G__85672;
chunk__85003_85667 = G__85673;
count__85004_85668 = G__85674;
i__85005_85669 = G__85675;
continue;
} else {
var G__85676 = seq__85001_85666;
var G__85677 = chunk__85003_85667;
var G__85678 = count__85004_85668;
var G__85679 = (i__85005_85669 + (1));
seq__85001_85666 = G__85676;
chunk__85003_85667 = G__85677;
count__85004_85668 = G__85678;
i__85005_85669 = G__85679;
continue;
}
} else {
var temp__5735__auto___85680__$1 = cljs.core.seq(seq__85001_85666);
if(temp__5735__auto___85680__$1){
var seq__85001_85685__$1 = temp__5735__auto___85680__$1;
if(cljs.core.chunked_seq_QMARK_(seq__85001_85685__$1)){
var c__4550__auto___85686 = cljs.core.chunk_first(seq__85001_85685__$1);
var G__85687 = cljs.core.chunk_rest(seq__85001_85685__$1);
var G__85688 = c__4550__auto___85686;
var G__85689 = cljs.core.count(c__4550__auto___85686);
var G__85690 = (0);
seq__85001_85666 = G__85687;
chunk__85003_85667 = G__85688;
count__85004_85668 = G__85689;
i__85005_85669 = G__85690;
continue;
} else {
var child_85691 = cljs.core.first(seq__85001_85685__$1);
if(cljs.core.truth_(child_85691)){
node.appendChild(child_85691);


var G__85695 = cljs.core.next(seq__85001_85685__$1);
var G__85696 = null;
var G__85697 = (0);
var G__85698 = (0);
seq__85001_85666 = G__85695;
chunk__85003_85667 = G__85696;
count__85004_85668 = G__85697;
i__85005_85669 = G__85698;
continue;
} else {
var G__85699 = cljs.core.next(seq__85001_85685__$1);
var G__85700 = null;
var G__85701 = (0);
var G__85702 = (0);
seq__85001_85666 = G__85699;
chunk__85003_85667 = G__85700;
count__85004_85668 = G__85701;
i__85005_85669 = G__85702;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_85665);
}
}


var G__85703 = cljs.core.next(seq__84964_85655__$1);
var G__85704 = null;
var G__85705 = (0);
var G__85706 = (0);
seq__84964_85606 = G__85703;
chunk__84966_85607 = G__85704;
count__84967_85608 = G__85705;
i__84968_85609 = G__85706;
continue;
} else {
var G__85707 = cljs.core.next(seq__84964_85655__$1);
var G__85708 = null;
var G__85709 = (0);
var G__85710 = (0);
seq__84964_85606 = G__85707;
chunk__84966_85607 = G__85708;
count__84967_85608 = G__85709;
i__84968_85609 = G__85710;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

var G__85014_85711 = shadow.dom._to_svg;
var G__85015_85712 = "string";
var G__85016_85713 = ((function (G__85014_85711,G__85015_85712){
return (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
});})(G__85014_85711,G__85015_85712))
;
goog.object.set(G__85014_85711,G__85015_85712,G__85016_85713);

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
});

goog.object.set(shadow.dom.SVGElement,"null",true);

var G__85017_85717 = shadow.dom._to_svg;
var G__85018_85718 = "null";
var G__85019_85719 = ((function (G__85017_85717,G__85018_85718){
return (function (_){
return null;
});})(G__85017_85717,G__85018_85718))
;
goog.object.set(G__85017_85717,G__85018_85718,G__85019_85719);
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___85720 = arguments.length;
var i__4731__auto___85721 = (0);
while(true){
if((i__4731__auto___85721 < len__4730__auto___85720)){
args__4736__auto__.push((arguments[i__4731__auto___85721]));

var G__85722 = (i__4731__auto___85721 + (1));
i__4731__auto___85721 = G__85722;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
});

shadow.dom.svg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.dom.svg.cljs$lang$applyTo = (function (seq85020){
var G__85021 = cljs.core.first(seq85020);
var seq85020__$1 = cljs.core.next(seq85020);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__85021,seq85020__$1);
});

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__85026 = arguments.length;
switch (G__85026) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = ((function (buf,chan){
return (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});})(buf,chan))
;
var G__85027_85727 = shadow.dom.dom_node(el);
var G__85028_85728 = cljs.core.name(event);
var G__85029_85729 = event_fn;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__85027_85727,G__85028_85728,G__85029_85729) : shadow.dom.dom_listen.call(null,G__85027_85727,G__85028_85728,G__85029_85729));

if(cljs.core.truth_((function (){var and__4120__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4120__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4120__auto__;
}
})())){
var c__80824__auto___85732 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__80824__auto___85732,buf,chan,event_fn){
return (function (){
var f__80825__auto__ = (function (){var switch__80630__auto__ = ((function (c__80824__auto___85732,buf,chan,event_fn){
return (function (state_85047){
var state_val_85048 = (state_85047[(1)]);
if((state_val_85048 === (1))){
var state_85047__$1 = state_85047;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_85047__$1,(2),once_or_cleanup);
} else {
if((state_val_85048 === (2))){
var inst_85044 = (state_85047[(2)]);
var inst_85045 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_85047__$1 = (function (){var statearr_85059 = state_85047;
(statearr_85059[(7)] = inst_85044);

return statearr_85059;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_85047__$1,inst_85045);
} else {
return null;
}
}
});})(c__80824__auto___85732,buf,chan,event_fn))
;
return ((function (switch__80630__auto__,c__80824__auto___85732,buf,chan,event_fn){
return (function() {
var shadow$dom$state_machine__80631__auto__ = null;
var shadow$dom$state_machine__80631__auto____0 = (function (){
var statearr_85063 = [null,null,null,null,null,null,null,null];
(statearr_85063[(0)] = shadow$dom$state_machine__80631__auto__);

(statearr_85063[(1)] = (1));

return statearr_85063;
});
var shadow$dom$state_machine__80631__auto____1 = (function (state_85047){
while(true){
var ret_value__80632__auto__ = (function (){try{while(true){
var result__80633__auto__ = switch__80630__auto__(state_85047);
if(cljs.core.keyword_identical_QMARK_(result__80633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__80633__auto__;
}
break;
}
}catch (e85071){if((e85071 instanceof Object)){
var ex__80634__auto__ = e85071;
var statearr_85072_85738 = state_85047;
(statearr_85072_85738[(5)] = ex__80634__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_85047);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e85071;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__80632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__85739 = state_85047;
state_85047 = G__85739;
continue;
} else {
return ret_value__80632__auto__;
}
break;
}
});
shadow$dom$state_machine__80631__auto__ = function(state_85047){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__80631__auto____0.call(this);
case 1:
return shadow$dom$state_machine__80631__auto____1.call(this,state_85047);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__80631__auto____0;
shadow$dom$state_machine__80631__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__80631__auto____1;
return shadow$dom$state_machine__80631__auto__;
})()
;})(switch__80630__auto__,c__80824__auto___85732,buf,chan,event_fn))
})();
var state__80826__auto__ = (function (){var statearr_85080 = (f__80825__auto__.cljs$core$IFn$_invoke$arity$0 ? f__80825__auto__.cljs$core$IFn$_invoke$arity$0() : f__80825__auto__.call(null));
(statearr_85080[(6)] = c__80824__auto___85732);

return statearr_85080;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__80826__auto__);
});})(c__80824__auto___85732,buf,chan,event_fn))
);

} else {
}

return chan;
});

shadow.dom.event_chan.cljs$lang$maxFixedArity = 4;


//# sourceMappingURL=shadow.dom.js.map
