 Footnotes will appear automatically numbered with a link to the footnote at bottom of the page [^footnote1].

```clojure
(ns app.initial-db
  (:require [clojure.pprint :refer [pprint]]
            [clojure.string :refer [capitalize]]
            #?(:clj [clojure.core :refer [subs defprotocol]]
               :cljs [cljs.core :refer [subs defprotocol]])))
```

# Nøklevann på sjøen

![bilde](http://nrpk.no/wp-content/uploads/2017/05/Mosse-oversikt-1000x420.jpg)

**Nøklevann ro- og padleklubb** har kajakker og utstyr for padling på egne turer, samt for kurs og gruppeturer!

<img src="http://nrpk.no/wp-content/uploads/2017/05/Mosse-oversikt-1000x420.jpg" alt="bilde">

## NRPK sin sjøbase ved Mosseveien 169B

[Cryogens site](https://github.com/cryogen-project/cryogen "Cryogen Github")

> Alle klubbens medlemmer som har «Våttkort grunnkurs hav», kan bruke kajakkene som er plassert der. Kravet om Våttkort-kurs er absolutt og av **sikkerhetsmessige** grunner (se HMS).
>
> Klubben har satt opp 2 kajakkstativ med 14 havkajakker type Boréal Baffin P1, P2 og P3 og to SUP-brett. Der er det også padlevester, årer, spruttrekk, lensepumper, svamper og annet.

Nedenfor er det lagt ut regler og informasjon om bruk av klubben på Mosseveien, samt et bookingsystem for bestilling av båter.

Båtene og annet utstyr skal kun brukes med sjøbasen som utgangspunkt og kan maksimalt bookes for to påfølgende dager, slik at det er mulig å dra på overnattingstur med en overnatting.

Kajakkene skal være tilgjengelige på sjøbasen når klubben har kurs, «Rull og Tull» og fellesturer. Bruk utover dette tillates normalt ikke, og skal avklares på forhånd med kontaktpersonene på sjøbasen eller styret@nrpk.no.

[^footnote1]: The footnote will contain a back link to to the referring text.
