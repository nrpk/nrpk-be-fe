# Prelimenary

## Never write an expression you cannot locally evaluate

## Guide to graphQL with Clojure for the front– and backend

The project is in a state of limbo right now, uncertain of which direction I
should steer it.

### Prerequisites

- # Lacinia

  This library is a full implementation of Facebook's [GraphQL
  specification](https://facebook.github.io/graphql).

  Lacinia should be viewed as roughly analogous to the[official reference
  JavaScript implementation](https://github.com/graphql/graphql-js/). In other
  words, it is a backend-agnostic GraphQL query execution engine. Lacinia is not
  an Object Relational Mapper ... it's simply the implementation of a contract
  sitting between the GraphQL client and your data.

  Lacinia features:

  * An [EDN](https://github.com/edn-format/edn)-based schema language. High
  * performance parser for GraphQL queries, built on
  * [Antlr4](http://www.antlr.org/). Efficient and asynchronous query execution.
  * Full support for GraphQL types, interfaces, unions, enums, input objects,
  * and custom scalars. Full support for GraphQL subscriptions. Full support of
  * inline and named query fragments. Full support for GraphQL Schema
  * Introspection.

  Lacinia has been developed with a set of core philosophies:

  * Prefer data over macros and other tricks. Compose your schema in whatever
  * mix of data and code works for you. Embrace Clojure: Use EDN data, keywords,
  * functions, and persistent data structures. Keep it simple: You provide the
  * schema and a handful of functions to resolve data, and Lacinia does the
  * rest. Do the right thing: apply reasonable defaults without a lot of
  * "magic".

  This library can be plugged into any Clojure HTTP pipeline. The companion
  library
  [lacinia-pedestal](https://github.com/walmartlabs/lacinia-pedestal)provides
  full HTTP support, including GraphQL subscriptions, for
  [Pedestal](https://github.com/pedestal/pedestal).

  An externally developed library,
  [duct-lacinia](https://github.com/kakao/duct-lacinia), provides similar
  capability for [Duct](https://github.com/duct-framework/duct).

![image-20190818143707143](PROGRESSNOTES.assets/image-20190818143707143.png)

# Debugging

```clojure {:ns 'back-httpkit.middleware}

```

`.core`

`.middleware`

when

``` {"Content-Type" "application/transit+json"} ```

35:20.186 [worker-3] WARN  back-httpkit.routes.services - 2 :body-params req=
"{\"query\" \"query { hero { name id } }\", \"variables\" {}}"

``` {"Content-Type" "application/transit+edn"} ```

36:26.319 [worker-1] WARN  back-httpkit.routes.services - 2 :body-params req= ""

###### This seems correct since we get a string from the request on server-side

``` {"Content-Type" "application/json"} ```

37:07.323 [worker-4] WARN  back-httpkit.routes.services - 2 :body-params req=
"{:variables {}, :query \"query { hero { name id } }\"}"

I need to convert this into

```json {'query', 'mutation', 'subscription', '...', NameId}"} ```

###### Solution

There was code like this:

```clojure (defn execute-request [query] (let [vars nil context nil] (log/info
"query=" query) (-> (lacinia/execute compiled-schema (:query query) (:variables
vars) context) (json/write-str)))) ```

which added json

it should look like this instead:

```clojure (defn execute-request [{:keys [query variables] :as payload}] (let
[vars nil context nil] (log/warn (with-out-str (prn "\nquery=" (str query) ",
variables=" variables))) (-> (lacinia/execute compiled-schema (str query)
variables context) ; not need for json/write-str since we want our data
uncluttered with json #_(json/write-str)))) ```

# Library, tolitius/cprop

`tolitius/cprop` (the same guy behind `mount`)

> likes properties, environments, configs, profiles..

```clojure (require '[cprop.core :refer [load-config]])

(load-config) ```

[see load-config in
action](https://github.com/tolitius/cprop/blob/master/test/cprop/test/core.cljc)

Reads all your dev-config.edn files etc. But how does load-config know what to
load?

###### Default

By default `cprop` would look in two places for configuration files:

* classpath: for the `config.edn` resource file system: for a path identified by
* the `conf` system property

If both are there, they will be merged. A `file system` source would override
matching properties from a `classpath`source, and the result will be [merged
with System
properties](https://github.com/tolitius/cprop/blob/master/README.md#merging-with-system-properties)
and then [merged with ENV
variables](https://github.com/tolitius/cprop/blob/master/README.md#merging-with-env-variables)
for all the *matching*properties.

check out [cprop
test](https://github.com/tolitius/cprop/blob/master/test/cprop/test/core.cljc)
to see `(load-config)` in action.

# Library, ring/middleware

# Library, metosin/reitit

reitit (Finish) = router

###### quick-start

```clojure (require '[reitit.core :as r])

(def router (r/router [["/api/ping" ::ping] ["/api/orders/:id" ::order]]))

(r/match-by-path router "/api/ping") ; #Match{:template "/api/ping" ;
:data {:name ::ping} ;        :result nil ;        :path-params {} ;
:path "/api/ping"}

(r/match-by-name router ::order {:id 2}) ; #Match{:template "/api/orders/:id", ;
:data {:name ::order}, ;        :result nil, ;        :path-params {:id 2}, ;
:path "/api/orders/2"} ```

# Library, metosin/muuntaja.middleware

muuntaja (Finish) = transformator

Clojure library for fast http format negotiation, encoding and decoding.
Standalone library, but ships with adapters for ring (async) middleware &
Pedestal-style interceptors. Explicit & extendable, supporting out-of-the-box
[JSON](http://www.json.org/), [EDN](https://github.com/edn-format/edn) and
[Transit](https://github.com/cognitect/transit-format) (both JSON & Msgpack).
Ships with optional adapters for [MessagePack](http://msgpack.org/) and
[YAML](http://yaml.org/).

Based on
[ring-middleware-format](https://github.com/ngrunwald/ring-middleware-format),
but a complete rewrite ([and up to 30x
faster](https://github.com/metosin/muuntaja/blob/master/doc/Performance.md)).

###### Modules

* `metosin/muuntaja` - the core abstractions + [Jsonista
* JSON](https://github.com/metosin/jsonista), EDN and Transit formats
* `metosin/muuntaja-cheshire` - optional [Cheshire
* JSON](https://github.com/dakrone/cheshire) format `metosin/muuntaja-msgpack` -
* Messagepack format `metosin/muuntaja-yaml` - YAML format

###### Quickstart

```clojure (require '[muuntaja.core :as m])

(->> {:kikka 42} (m/encode "application/json")) ; =>
#object[java.io.ByteArrayInputStream]

(->> {:kikka 42} (m/encode "application/json") slurp) ; => "{\"kikka\":42}"

(->> {:kikka 42} (m/encode "application/json") (m/decode "application/json")) ;
=> {:kikka 42} ```

# Clojure

| Function  | Args<br />Description                                        | |
:-------- | ------------------------------------------------------------ | |
cond      | (cond & clauses)                                             | |
| Takes a set of test/expr pairs. It evaluates each test one at a time.  If a
test returns logical true, cond evaluates and returns the value of the
corresponding expr and doesn't evaluate any of the other tests or exprs. (cond)
returns nil. | | meta      | (meta obj)
| |           | Returns the metadata of obj, returns nil if there is no
metadata. | | empty     | (empty coll)
| |           | Returns an empty collection of the same category as coll, or nil
| | with-meta | (with-meta obj m)                                            | |
| Returns an object of the same type and value as obj, with   map m as its
metadata. | | union     | (union) (union s1) (union s1 s2) (union s1 s2 & sets)
| |           | Return a set that is the union of the input sets             | |
into      | (into) (into to) (into to from) (into to xform from)         | |
| Returns a new coll consisting of to-coll with all of the items of from-coll
conjoined. A transducer may be supplied. |

```clojure (defn meta-merge "Recursively merge values based on the information
in their metadata." ([] {}) ([left] left) ([left right] (cond
(different-priority? left right) (pick-prioritized left right)

         (and (map? left) (map? right)) (merge-with meta-merge left right)

         (and (set? left) (set? right)) (set/union right left)

         (and (coll? left) (coll? right)) (if (or (-> left meta :prepend) (->
         right meta :prepend)) (-> (into (empty left) (concat right left))
         (with-meta (merge (meta left) (select-keys (meta right) [:displace]))))
         (into (empty left) (concat left right)))

         :else right)) ([left right & more] (reduce meta-merge left (cons right
         more))))

```
