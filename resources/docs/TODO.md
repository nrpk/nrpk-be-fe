# test 2

## Never write an expression you cannot locally evaluate.

I try to avoid writing expressions that stop me from evaluating them locally without bringing in the rest of the world.

### Prewriting

Right, what do I want to.

I don't want to sell anything. I want to show how can use different tools to create stuff. How to make the machine work for you? This is something I often have struggled with, finding a way to leverage the machine. The machine wants me to work in a specific way - it lures me into becoming another machine. Machines can be beautiful and fascinating. I can like machines. They are dead objects that appear to have life and I think it is this appearance of life, this idea one can have that they really are living entities – perhaps the promise of this that is so fascinating to me.

Enter the toolbox: It's a machine, no surprise. It's dead, it brings complexity to the table, necessary some say, I tend to agree. We are not there yet, we cannot just sit down in front of our computers and think loud and get assistance to our endeavours. We have to adapt and we need to understand how the parts are supposed to work together, where they struggle and how you must adapt to leverage the dance.

Clojure is such a thing. I'm not pretending to be an expert. I've failed too many times to even keep a straight face pretending. Things are complex. Things are complected. That this stuff even works in our day to day lives is astonishing, let's not go there, yet.

So what do we do? I want to play around with some ideas that I've had for some time; combining Clojure Spec with the creation of a circular diagram, resembling the Apple watch activity widget, but with more features and added complexity. I wanted to make the machine create variations of these diagrams for me. I wanted to see if I could create a spec, feed some machinery with test-data generated from this spec, and see if anything interesting could come out of a couple of thousand diagrams this could result in.

I was thinking that the circle was an interesting shape, and explore how I could represent data in various way using the circle with arcs, lines and colours in various combinations. Or I could spend some days inside Pinterest, but that's not my definition of fun, I want to create not consume. Being inspired, absolutely, but not staying in there feeding on the dopamine new structures and visuals give you.

So I have these elements; HTML/CSS/SVG, Clojurescript, Clojure/Spec and its offsprings, and the idea of leveraging the machine by feeding a program with data coming in via Spec.
