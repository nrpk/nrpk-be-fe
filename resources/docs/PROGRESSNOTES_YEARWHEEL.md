Never write an expression you cannot locally evaluate. #lisp

I try to avoid writing expressions that stop me from evaluating them locally without bringing in the rest of the world. #lisp

### Prewriting

Right, what do I want to.

I don't want to sell anything. I want to show how can use different tools to create stuff. How to make the machine work for you? This is something I often have struggled with, finding a way to leverage the machine. The machine wants me to work in a specific way - it lures me into becoming another machine. Machines can be beautiful and fascinating. I can like machines. They are dead objects that appear to have life and I think it is this appearance of life, this idea one can have that they really are living entities – perhaps the promise of this that is so fascinating to me.

Enter the toolbox: It's a machine, no surprise. It's dead, it brings complexity to the table, necessary some say, I tend to agree. We are not there yet, we cannot just sit down in front of our computers and think loud and get assistance to our endeavours. We have to adapt and we need to understand how the parts are supposed to work together, where they struggle and how you must adapt to leverage the dance. 

Clojure is such a thing. I'm not pretending to be an expert. I've failed too many times to even keep a straight face pretending. Things are complex. Things are complected. That this stuff even works in our day to day lives is astonishing, let's not go there, yet.

So what do we do? I want to play around with some ideas that I've had for some time; combining Clojure Spec with the creation of a circular diagram, resembling the Apple watch activity widget, but with more features and added complexity. I wanted to make the machine create variations of these diagrams for me. I wanted to see if I could create a spec, feed some machinery with test-data generated from this spec, and see if anything interesting could come out of a couple of thousand diagrams this could result in. 

I was thinking that the circle was an interesting shape, and explore how I could represent data in various way using the circle with arcs, lines and colours in various combinations. Or I could spend some days inside Pinterest, but that's not my definition of fun, I want to create not consume. Being inspired, absolutely, but not staying in there feeding on the dopamine new structures and visuals give you.

So I have these elements; HTML/CSS/SVG, Clojurescript, Clojure/Spec and its offsprings, and the idea of leveraging the machine by feeding a program with data coming in via Spec. 

### prolog

With clojure/spec I can generate these diagrams randomly, say I don't want to code each of the designs by hand, I just declare and define the shape of valid data, then make the machine create thousands of diagrams I can examine and evaluate while in the hammock to see if something interesting comes up.

My plan is to post this as a series of tweets and see if I can generate some response and capture some interest. Perhaps you are new to Clojure and maybe I could piquet your interest. However this is not the goal. The goal is to show that despite all the tribalism that flourishes in our industry, these are just different moving parts of a system. Anything can be combined. Some combinations can be quite demanding in the way that there is a high impedance between the systems. It could be structural differences, operating conditions that don't match. Political differences, you know, the forever argument about what is the best and most efficient. I could go trying to accomplish my goals using arbitrary tools, but who am I kidding here? Obviously some tools gives themselves. Lisp. Transforming data with data. From one shape to the other. From specification to a product. And I want to work with clay, not chisel and stone. Clay is not better than stone. Its just a different material. I want the process to be fluid and quick. I need results for the attention-span of twitter or the rest of the internet. I need a dynamic environment where I can shape the environment and form data as we go. Perhaps the end-results require the speed of a chetah, thats fine, then I know, then chetah it will be, but until then, I need to know. 

And how do you know if you cannot move swift and fluid?

This decision lays the premise of a rather complex environment. Working in a lisp is a complex process. It is complex because you are combining the statefulness of the environment with the a functional language. So, it's Clojure with its wonderful immutable data-structures that gives us the safety and ability to run with sharp tools. I just love sharp tools, sure you can cut yourself to pieces, well, not really, because we are working in a safe language-environment that doesn't change under our feet, unless the working-environment breaks, which it does, all the time. So we need to discern the difference and accept that idealism and the real world can work together as long as we take care and understand the tooling. 

Long and demanding requirements are not fun and often impose struggle and complexity – true – but it's a necessity, so we will have to deal with it. Nothing comes from nothing, invest, see if triggers your interest. 

First, let us do some investment to our playground. Let's see if we can handle the complexity. Working with Clojure means starting a bunch of processes which communicate through a network connection. There are several options available for us, and that is basically where things get complicated for newcomers to the environment. It doesn't have to be so complicated even if you have to make a choice between Lein, Boot or Tools-deps. 

But you didn't come here to make lots of complicated choices with consequences reaching far beyond whatever horizon you might have at the moment. You came here for me to make the decision, and we will use shadow-cljs which supports hot-reloading and other goodies that the lisp environment is known to provide. 

# The playground



# Introduction

So I have a bunch of these code-snippets, a couple of hundred lines of code that makes me generate a wheel in SVG, something nice and round, like a ferris-wheel, representing dates during a year for instance, or something cyclic. I'll write up something later to show the origin of this. 

This project is written in Clojurescript,  using reagent and re-frame as the primary buildingblocks, svg is the designated format, so this is something that constructs a circular diagram with svg generated from clojure-code, which is again generated from the output of clojure/spec/gen [come back to the names later]().

The idea here is to show how you can leverage some of the cool tools that clojure brings to the table, like clojure/spec and its surrounding conglomerate of tools.

# First hurdle

1. Define the api and make it spec-able without ruining your emotional life.
2. Define the spec to match.
3. Generate a diagram on the fly, each every second, see how if it could work out.
4. Find a way to represent 1000 on web-page, since we are already in web-land this should be straight forward.
5. Profit



