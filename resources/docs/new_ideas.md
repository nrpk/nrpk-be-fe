# New ideas

##### Introducing

Introduce a concept called Semantic headers

###### use-case

while copying text, you are copying semantics in the formatting. This is striking when working in typora markdown, but I believe it applies other places as well. What you end up doing is reformatting the text, changing header levels to fit your existing system.

My idea is to use headers as the beginning of a new concept, not as a divisor of text. This idea coincides with the idea of a header as a beginning of a chapter or section.

> Often this is not the case, and you are struggling with keeping the semantic structure of your headers in sync. This is an attempt to ignore the structural idea of a specific level header.

###### observed strategies

Using h1 and h6 on the first division. The highest combined with the lowest to indicate hierarchy and that each of them are at the opposite spectrum of the semantic context. Introducing the reader (and the writer in the process of writing the text) that the division is intentional

###### outcome

There is no need to decrease the distance between these two positions, even when we add content and outcomes, like I have done here. Keeping the text in lowercase is just a stylistic choice.

###### further

It seems that the shortest distance between the headers themselves is a comment on their familiarity or how strong the belong together as a concept. Above, there is a H5 and H6, show that the _introducing_ section belongs to _use-case_ because of their close distance in levels. Even if there is a H1, the H5 is closer to H6 and almost defines a continuous block of information.

## Conclusion

H6 has no other meaning than being the farthest away from H1 we can come in the html standard. If you start the division with an H6 instead of the usual H2, something is promised in this text which remains to be seen fulfilled.
