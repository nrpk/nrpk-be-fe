Preparations

# To start

lein new luminus back_httpkit +swagger +graphql +http-kit +re-frame +reagent +shadow-cljs +oauth +auth-jwe +postgres +sqlite

```
.
├── env
│   ├── dev
│   │   ├── clj
│   │   ├── cljs
│   │   └── resources
│   ├── prod
│   │   ├── clj
│   │   ├── cljs
│   │   └── resources
│   └── test
│       └── resources
├── log
├── resources
│   ├── docs
│   ├── graphql
│   ├── html
│   ├── migrations
│   ├── public
│   │   ├── css
│   │   ├── img
│   │   └── js
│   └── sql
├── src
│   ├── clj
│   │   └── back_httpkit
│   ├── cljc
│   │   └── back_httpkit
│   └── cljs
│       └── back_httpkit
├── target
│   ├── cljsbuild
│   └── default
│       ├── classes
│       └── stale
└── test
    ├── clj
    │   └── back_httpkit
    └── cljs
        └── back_httpkit
```

Resulting in
```
├── backend
├── cont
│   ├── database
│   ├── repl
│   └── web
├── datadir
│   ├── base
│   │   ├── 1
│   │   ├── 13116
│   │   ├── 13117
│   │   ├── 16384
│   │   └── 16385
│   ├── global
│   ├── pg_commit_ts
│   ├── pg_dynshmem
│   ├── pg_logical
│   │   ├── mappings
│   │   └── snapshots
│   ├── pg_multixact
│   │   ├── members
│   │   └── offsets
│   ├── pg_notify
│   ├── pg_replslot
│   ├── pg_serial
│   ├── pg_snapshots
│   ├── pg_stat
│   ├── pg_stat_tmp
│   ├── pg_subtrans
│   ├── pg_tblspc
│   ├── pg_twophase
│   ├── pg_wal
│   │   └── archive_status
│   └── pg_xact
├── env
│   ├── dev
│   │   ├── clj
│   │   ├── cljs
│   │   └── resources
│   ├── prod
│   │   ├── clj
│   │   ├── cljs
│   │   └── resources
│   └── test
│       └── resources
├── log
├── resources
│   ├── docs
│   │   └── PROGRESSNOTES.assets
│   ├── graphql
│   ├── html
│   ├── migrations
│   ├── nrpk-tekst
│   │   ├── 1-Aktiviteter
│   │   ├── 2-Nøkkelvakt
│   │   ├── 3-Sikkerhet
│   │   ├── 4-Om\ oss
│   │   ├── 5-epost
│   │   └── 6-Plakater
│   ├── public
│   │   ├── css
│   │   ├── img
│   │   └── js
│   └── sql
├── src
│   ├── clj
│   │   └── back_httpkit
│   ├── cljc
│   │   ├── app
│   │   ├── arcs
│   │   ├── back_httpkit
│   │   ├── core
│   │   └── frontend
│   └── cljs
│       ├── app
│       ├── arcs
│       ├── back_httpkit
│       ├── core
│       ├── frontend
│       ├── items
│       ├── panel
│       └── website
├── styles
├── target
│   ├── cljsbuild
│   │   └── public
│   ├── default
│   │   ├── classes
│   │   └── stale
│   └── dev
│       ├── classes
│       └── stale
└── test
    ├── clj
    ├── cljc
    └── cljs
        └── back_httpkit
```
