(ns back-httpkit.core-test
  (:require [cljs.test :refer-macros [is are deftest testing use-fixtures]]
            [cljs.pprint :refer [pprint]]
            [pjstadig.humane-test-output]
            [reagent.core :as re :refer [atom]]
            [cljs-time.core :as t]
            [frontend.time2 :as t2 :refer [next-time set-next-time-now now set-next-time]]
            [app.packet.builder :as b]
            [app.aggregation :as agg :refer [boat-status-filter
                                              computation
                                              initial-model]]))

;;; helpers ;;;

(defn- assert-errors [r]
  (is (not (empty? (:errors r)))))

(defn- assert-no-errors [r]
  (is (empty? (:errors r))
      (with-out-str (pprint {:msg   "no errors was expected, errors: "
                             :error (map (comp agg/error-messages :error/reason-id)
                                         (:errors r))}))))

(defn- reducer-mc
  "reducer (originally with mapcat)"
  [& xs]
  (reduce agg/computation agg/initial-model xs))

;;; tests ;;;

(deftest basics-1
  (testing "a"
    (let [r (-> agg/initial-model
                (agg/computation (b/add-boat 1 1)))]
      (assert-no-errors r)))

  (testing "b"
    (let [r (reducer-mc
             (b/add-boat 1 1)
             (b/assign-boat-name 1 "100")
             (b/rent-start 1 1))]
      (assert-no-errors r)))

  (testing "c"
    (let [r (-> agg/initial-model
                (agg/computation (b/add-boat 1 1))
                (agg/computation (b/rent-start 2 (next-time))))]
      (assert-errors r))))

(deftest testing-rental
  (testing "get 1 error in :errors when trying to start rent on a non-existing boat-id"
    (let [r (reducer-mc (b/rent-start 1 (next-time)))
          errors r]
      (is (not-empty errors))))
  (testing "complete rental-cycle"
    (let [r (reducer-mc
             (b/add-boat 1)
             (b/assign-boat-name 1 "100")
             (b/rent-start 1 (next-time))
             (b/rent-end 1 (next-time)))
          e (:errors r)]
      (is (empty e))))

  (testing "assigning names to non-existent boats should fail"
    (let [r (reducer-mc
             (b/assign-boat-name 1 "100")
             (b/assign-boat-name 2 "200"))
          c (count (:errors r))]
      (is (= 2 c))))

  (testing "add a boat but don't make it available and attempt to rent it should fail"
    (let [r (reducer-mc
             (b/add-boat 1)
             (b/rent-start 1 (next-time)))
          e (:errors r)]
      (is (not-empty e))
      (is (= 2 (:event/epoch r)))
      (is (= nil (:boat/status (get-in r [:boats 1]))))))

  (testing "add a boat, make it available and rent it"
    (let [r (reducer-mc
             (b/add-boat 1)
             (b/assign-boat-name 1 "100")
             (b/rent-start 1 (next-time)))]
      (is (empty? (:errors r)))
      (is (= 3 (:event/epoch r)))
      (is (= :boat/busy (:boat/status (get-in r [:boats 1]))))))

  (testing "add a boat, make it available and attempt to rent it, check the time"
    (let [t (next-time)
          r (reducer-mc
             (b/add-boat 1)
             (b/assign-boat-name 1 "100")
             (b/rent-start 1 t))]
      (assert-no-errors r)
      ;(is (not-empty (:errors r)))
      (is (= 3 (:event/epoch r)))
      (is (= t (:event/time (get-in r [:boats 1]))))))

  (testing "add two boats, make them available and attempt to rent them, check the time"
    (let [t1 (next-time)
          t2 (next-time)
          r (reducer-mc
             (b/add-boat 1)
             (b/add-boat 2)
             (b/assign-boat-name 1 "100")
             (b/assign-boat-name 2 "100")
             (b/rent-start 1 t1)
             (b/rent-start 2 t2))]
      (assert-no-errors r)
      (is (= 6 (:event/epoch r)))
      (is (= t1 (:event/time (get-in r [:boats 1]))))
      (is (= t2 (:event/time (get-in r [:boats 2]))))))

  (testing "add 10 boats, make them available and attempt to rent them, check the time"
    (let [a (range 10)
          t (map next-time a)
          i (concat
             (map b/add-boat a)
             (map (partial apply b/assign-boat-name)
                  (map (juxt identity (comp str (partial + 100))) a))
             (map (fn [e] (b/rent-start e (nth t e))) a))

          r (apply reducer-mc i)]
      (assert-no-errors r)
      (is (= (* 3 (count a)) (:event/epoch r)))
      (doseq [e a]
        (is (= (nth t e) (:event/time (get-in r [:boats e]))))))))

(deftest testing-multiple
  (testing "add 10 boats and make them all available, use one odd id"
    (let [a (range 9)
          i (concat [(b/add-boat 199)]
                    (map b/add-boat a)
                    (map (partial apply b/assign-boat-name)
                         ; b/assign-boat-name takes two args: [id:int, name:string]
                         (map (juxt identity str)
                              (concat [199] a))))
          r (apply reducer-mc i)]
      (assert-no-errors r)
      (is (= 20 (:event/epoch r)) ":event/epoch is not 20")
      (is (= 10 (count (:boats r))))
      (is (= (set (concat [199] (range 9)))
             (set (keys (:boats r)))))))

  (testing "add 10 boats and make them all available and rent them out"
    (let [r (apply reducer-mc
                   (concat (map b/add-boat (range 10))
                           (map #(b/assign-boat-name % (str %)) (range 10))
                           (map b/rent-start (range 10))))]
      (assert-no-errors r)
      (is (= 30 (:event/epoch r)) ":event/epoch is not 30")
      (is (= 10 (count (:boats r))))
      (is (= (range 10) (keys (:boats r))))))

  (testing "add 10 boats and make them all in-repair and rent them out"
    (let [r (apply reducer-mc (concat (map (fn [e] (b/add-boat e)) (range 10))
                                      (map (fn [e] (b/set-boat-in-repair e)) (range 10))
                                      (map (fn [e] (b/rent-start e (next-time))) (range 10))))]
      (assert-errors r)
      (is (= 10 (count (:errors r))) (str "Expected 10 but got " (count (:errors r)) " errors"))
      (is (= 30 (:event/epoch r)) ":event/epoch is not 30")
      (is (= 10 (count (:boats r))))
      (is (= (range 10) (keys (:boats r))))))

  (testing "complex, as the previous"
    (let [r (reduce computation initial-model (concat (map b/add-boat (range 10))
                                                      [(b/add-boat 100)
                                                       (b/assign-boat-name 100 "100")
                                                       (b/rent-start 100 (next-time))]
                                                      (map b/set-boat-in-repair (range 10))
                                                      (map b/rent-start (range 10))))]
      (assert-errors r)
      (is (= 11 (count (:boats r))))
      (is (= (set (concat [100] (range 10)))
             (set (keys (:boats r))))))))

(deftest testing-repair

  (testing "single repair"
    (let [r (reducer-mc
             (b/add-boat 100)
             (b/assign-boat-name 100 "100")
             (b/rent-start 100 (next-time))
             (b/set-boat-in-repair 100))]
      (is (= :boat/in-repair (:boat/status (get-in r [:boats 100]))))))

  (testing "two boats in repair"
    (let [r (reducer-mc
             (b/add-boat 1)
             (b/assign-boat-name 1 "100")
             (b/add-boat 2)
             (b/assign-boat-name 2 "200")
             (b/rent-start 1 (next-time))
             (b/rent-start 2 (next-time))
             (b/set-boat-in-repair 1)
             (b/set-boat-in-repair 2))]
      (is (= :boat/in-repair (:boat/status (get-in r [:boats 1]))))))

  (testing "4 added boats, available, 3 starts rent, 2 in repair, 1 rent completed"
    (let [r (reducer-mc
             (b/add-boat 1)
             (b/add-boat 2)
             (b/add-boat 3)
             (b/add-boat 4)
             (b/assign-boat-name 1 "100")
             (b/assign-boat-name 2 "200")
             (b/assign-boat-name 3 "300")
             (b/assign-boat-name 4 "400")
             (b/rent-start 1 (next-time))
             (b/rent-start 2 (next-time))
             (b/rent-start 3 (next-time))
             (b/set-boat-in-repair 2)
             (b/set-boat-in-repair 3)
             (b/rent-start 4 (next-time))
             (b/rent-end 4 (next-time)))]
      (assert-no-errors r)
      (is (= :boat/busy (:boat/status (get-in r [:boats 1]))))
      (is (= :boat/in-repair (:boat/status (get-in r [:boats 2]))))
      (is (= :boat/in-repair (:boat/status (get-in r [:boats 3]))))
      (is (= :boat/available (:boat/status (get-in r [:boats 4]))))))

  (testing "serial repair of 20 boats"
    (let [r (apply reducer-mc
                   (concat (map b/add-boat (range 20))
                           (map b/set-boat-in-repair (range 20))
                           (map b/set-boat-out-of-repair (range 20))))]
      (assert-no-errors r)
      (is (= :boat/available (:boat/status (get-in r [:boats 0]))))
      (is (= :boat/available (:boat/status (get-in r [:boats 19]))))))

  (testing "10 in repair, 2 out of repair"
    (let [r (apply reducer-mc
                   (concat (map b/add-boat (range 10))
                           (map b/set-boat-in-repair (range 10))
                           (map b/set-boat-out-of-repair (range 1 9))))]
      (assert-no-errors r)
      (is (= :boat/in-repair (:boat/status (get-in r [:boats 0]))))
      (is (= :boat/available (:boat/status (get-in r [:boats 1]))))
      (is (= :boat/in-repair (:boat/status (get-in r [:boats 9]))))
      (is (= 2 (count (boat-status-filter r :boat/in-repair))))
      (is (= 8 (count (boat-status-filter r :boat/available)))))))

(deftest testing-add-boats
  (testing "unsure about this one"
    (let [r (apply reducer-mc
                   (concat
                    (map b/add-boat (range 10))
                    (map b/assign-boat-name (range 10) (map (partial str "X") (range 100 110)))
                    (map b/add-boat (range 10))
                    (map b/add-boat (range 1000 1012))))]
      (assert-errors r)
      (are [x y] (= x y)
        false (empty? (boat-status-filter r :boat/available))
        10 (count (boat-status-filter r :boat/available))
        12 (count (boat-status-filter r nil)))))

  (testing "make inactive active"
    (let [r (reducer-mc
             (b/add-boat 1)
             (b/add-boat 2)
             [:hack-system/intrude {}]
             (b/add-boat 2)
             (b/add-boat 2)
             (b/add-boat 2)
             (b/add-boat 2)
             (b/add-boat 2)
             (b/assign-boat-name 11 "400")
             (b/rent-start 1 (next-time))
             [:hack-system {}]
             (b/remove-boat-name 1))]
      (assert-errors r)
      (is (= 12 (r :event/epoch)))
      (is (= 9 (count (r :errors)))))))

(deftest testing-epoch

  (testing "no events, epoch is nil"
    (let [r (reducer-mc [])]
      (is (nil? (-> r :boats (get 1) :event/epoch)))))

  (testing "single event, epoch is 1"
    (let [r (reducer-mc (b/add-boat 1))]
      (is (= 0 (-> r :boats (get 1) :event/epoch)))))

  (testing "single illegal event, epoch is 2"
    (let [r (reducer-mc (b/assign-boat-name 1 "herkules"))]
      (assert-errors r)
      (is (= 1 (-> r :event/epoch)))
      (is (= nil (-> r :boats (get 1) :event/epoch)))))

  (testing "make active, check epoch"
    (let [r (reducer-mc (b/add-boat 1)
                        (b/assign-boat-name 1 "herkules")
                        (b/rent-start 1 (next-time)))]
      (is (= 2 (-> r :boats (get 1)
                   :event/epoch))))))
