(ns back-httpkit.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[back_httpkit started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[back_httpkit has shut down successfully]=-"))
   :middleware identity})
