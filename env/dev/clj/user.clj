(ns user
  "Userspace functions you can run by default in your local REPL."
  (:require
   [back-httpkit.config :refer [env]]
   [clojure.spec.alpha :as s]
   [expound.alpha :as expound]
   [clojure.tools.logging :as log]
   [mount.core :as mount]
   [back-httpkit.core :refer [start-app]]
   [back-httpkit.db.core]
   [conman.core :as conman]
   [luminus-migrations.core :as migrations]
   #_[shadow.cljs.devtools.api :as shadow]
   ))

;(shadow/repl :app)

(log/info "log/info from user-namespace")

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(add-tap (bound-fn* clojure.pprint/pprint))

(defn start
  "Starts application.
  You'll usually want to run this on startup."
  []
  (mount/start-without #'back-httpkit.core/repl-server))

(defn stop
  "Stops application."
  []
  (mount/stop-except #'back-httpkit.core/repl-server))

(defn restart
  "Restarts application."
  []
  (stop)
  (start))

(defn restart-db
  "Restarts database."
  []
  (mount/stop #'back-httpkit.db.core/*db*)
  (mount/start #'back-httpkit.db.core/*db*)
  (binding [*ns* 'back-httpkit.db.core]
    (conman/bind-connection back-httpkit.db.core/*db* "sql/queries.sql")))

(defn reset-db
  "Resets database."
  []
  (migrations/migrate ["reset"] (select-keys env [:database-url])))

(defn migrate
  "Migrates database up for all outstanding migrations."
  []
  (migrations/migrate ["migrate"] (select-keys env [:database-url])))

(defn rollback
  "Rollback latest database migration."
  []
  (migrations/migrate ["rollback"] (select-keys env [:database-url])))

(defn create-migration
  "Create a new up and down migration file with a generated timestamp and `name`."
  [name]
  (migrations/create name (select-keys env [:database-url])))

(tap> "user-namespace was evaluated")
