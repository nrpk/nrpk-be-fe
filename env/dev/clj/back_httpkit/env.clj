(ns back-httpkit.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [back-httpkit.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[back_httpkit started successfully using the development profile]=-\n\n"))
   :stop
   (fn []
     (log/info "\n-=[back_httpkit has shut down successfully]=-"))
   :middleware wrap-dev})
