(ns ^:dev/once back-httpkit.app
  (:require
   [back-httpkit.core :as core]
   [cljs.spec.alpha :as s]
   [expound.alpha :as expound]
   [devtools.core :as devtools]
     ;[shadow.cljs.devtools.api :as shadow]
   [re-graph.logging :as log]))

(extend-protocol IPrintWithWriter
  js/Symbol
  (-pr-writer [sym writer _]
    (-write writer (str "\"" (.toString sym) "\""))))

(set! s/*explain-out* expound/printer)

(enable-console-print!)

(devtools/install!)
(devtools.core/set-pref! :dont-detect-custom-formatters true)

(core/init!)

(log/info "APP loaded")
;(shadow/repl :app)
