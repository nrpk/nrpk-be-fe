(ns nv.ui
  (:require [clojure.string]
            [yd.core :as y]
            [re-frame.core :as rf]))

(defn pseudo [pseudo-class class]
  (apply str (interpose " " (map (partial str pseudo-class ":")
                                 (clojure.string/split (name class) ".")))))

(defn toggles [f r]
  (interpose [:div.p-px] (map f r)))

(defn button
  ([e]
   (button {:class "text-gray-100 bg-gray-400 border-none w-20"} e))
  ([opts e]
   [:a.border.rounded-sm.px-2.py-1 (merge-with #(conj [%2] %1)
                                               opts
                                               {:class "items-center flex justify-center text-xs text-center font-bold "})
    (clojure.string/capitalize (str e))]))

(defn question-button [])

(defn side-button
  ([e]
   (side-button {:class "border-none h-20 w-20 text-3xl"} e))
  ([opts e]
   [:a (merge-with #(conj [%2] %1)
                   {:class "items-center flex justify-center text-xs text-center font-bold border-none text-2xl text-gray-400 bg-gray-600"}
                   opts)
    (clojure.string/capitalize (str e))]))

(comment
  (do
    (side-button {:p 'a} 1)))

(defn input
  ([]
   (input {}))
  ([opts]
   [:input.text-base.border.border-gray-500.rounded-sm.px-2.py-1.w-full (merge opts {:class [(pseudo "focus" :bg-yellow-200)]})]))

(defn date
  ([]
   (date {}))
  ([opts]
   [:input.text-base.border.border-gray-500.rounded-sm.px-2.w-40 (merge opts {:class (pseudo "focus" :bg-yellow-200)
                                                                              :type "date"})]))

(defn header [& xs]
  [:h1.text-lg.font-black.opacity-50.bg-gray-800.px-2.py-1 xs])

(defn header-2 [s]
  [:h2.text-base.font-thin.px-2.py-1 s])

(defn card [s n]
  [:div.w-fill.h-24.bg-gray-10s0.rounded
   [:div.center.flex-col.bg-gray-600.text-gray-300
    [:p.text-center.text-xs.text-gray-300 s]
    [:p.text-center.text-xl.font-black n]]])

(def space [:div.p-1])

(def error-style {:class [:bg-gray-900 :text-yellow-500 :text-xl]})

(defn circle
  ([s]
   [:svg.inline  {:viewBox "0 0 10 10" :preserveAspectRatio "xMidYMid slice"
                  :width   "100%"
                  :height  :auto}
    (when (:debug s) [:rect {:x 0 :y 0 :width :100% :height :100% :fill 'none :stroke 'gray}])
    [:circle {:cx :50% :cy :50% :r (if (:burst s) :30% (if (:mini s) :7% :16%)) :fill (if (:fill s) :currentColor 'none) :stroke :currentColor}]])
  ([]
   [:svg.inline {:class  [#_(rand-nth [:text-orange-400 :text-transparent])] :viewBox "0 0 10 10" :preserveAspectRatio "xMidYMin meet"
                 :width  20
                 :height :auto}
    [:circle {:cx :50% :cy :38% :r :25% :xfill :currentColor :stroke :currentColor}]]))

(defn grid' [c & xs]
  [:div
   {:style (merge {:display :grid :grid-gap 4} c)}
   xs])

(defn grid'' [c & xs]
  [:div
   (merge-with merge  c
          {:style {:display  :grid
                   :grid-gap 4}})

   xs])

(defn grid-scroll-x [c & xs]
  [:div.scroll-x.snap-x
   {:style (merge {:display :grid :grid-gap 4} c)}
   xs])

(defn grid [c & xs]
  [:div.bg-white.clip.m-2.rounded-sm.border-2.border-gray-900.border-dashed
   {:style {:display :grid
            :grid-gap 2
            :grid-auto-rows "auto"
            :grid-template-columns c}} xs])

(def help-panel :div.h-auto.text-green-500.bg-green-900)

(def help-panel' :div.bg-gray-400.text-gray-800.p-2)

(def token :p.text-xxs.inline.bg-black.py-px.px-2.rounded-sm.m-px)

(defn g-area [s & xs]
  [:div.scroll-y.h-auto {:style {:grid-area s}} xs])

(defn g-area-noscroll [s & xs]
  [:div.h-full {:style {:grid-area s}} xs])

(defn g-area' [s]
  {:style {:grid-area s}})

(def mini-panel' :div.bg-gray-400.text-gray-800.px-2)

(def debug :div.py-4.px-6.w-full.bg-gray-100.debug)
(def title :p.font-thin.tracking-wider.bg-red-700.text-white.py-4.px-6)
(def item :div.text-green-500)
(def training :p.bg-yellow-300.center)
(def vakt-item :div.text-black.mb-2.bg-gray-100.clip.p-1.shadow-4.select-none.cursor-pointer)

(def std-hover {:class (pseudo "hover" "text-white.opacity-100")})

(defn list-item-sm [e]
  [:div.bg-gray-x700.opacity-25.cursor-default std-hover
   [:div.center.h-4.text-base.font-black e]])

(defn list-item-sm' [e]
  [:button.truncate.text-xs.h-6.border-gray-500.border.text-gray-500.rounded-sm.px-2.py-px.opacity-50 {:class (pseudo "hover" "border-white.text-white.opacity-100")} e])

(defn button-sm-toggle
  ;todo: fix color-styles
  ([e]
   [:button.truncate.text-xs.h-6.border-gray-500.rounded-sm.px-2.py-px.opacity-50
    {:class (pseudo "hover" "border-white.text-white.opacity-100")} e])
  ([e k]
   (let [s @(rf/subscribe [k])]
     [:button.truncate.text-xs.h-6.border-gray-500.rounded-sm.px-2.py-px.opacity-50
      {:on-click #(rf/dispatch [k])
       :class    [(if s "border-2 text-yellow" "border text-white bg-gray-800") (pseudo "hover" "border-white.text-white.opacity-100")]} e])))

(defn togglebar-item-sm [e n]
  [:div.rounded-xs.bg-gray-300 {:class (pseudo "hover" :bg-yellow-200.text-black)}
   [:div.center.h-4.text-xxs e]])

(defn cell
  ([a]
   (cell {:class [:bg-gray-400]} a ""))
  ([a b]
   (cell {:class [:bg-gray-900]} a b))
  ([opts a b]
   [:div.text-center.xborder.flex-grow.xp-4.xbg-gray-300.rounded.h-auto.py-4.shadow-4.opacity-50 (merge {:style {:min-width :30%}} opts)
    [:p.text-xxs a]
    [:p.text-xl.font-base.font-bold.text-yellow-500 b]]))

(defn dim [& xs] [:span.text-gray-600 xs " "])

(defn placeholder-sm'' [opts msg] [:div.bg-gray-400.text-gray-200.w-full.h-full opts [:p.center.font-sans.font-black msg]])
(defn placeholder-sm' [opts msg] [:div.h-32.bg-gray-400.text-gray-200 opts [:p.center.font-sans.font-black msg]])
(def placeholder-sm (placeholder-sm' {:class [:text-white]} "placeholder"))
(def placeholder [:div.h-full.w-full.bg-gray-400.text-gray-200 [:p.center.font-sans.font-black

                                                                "placeholder"]])


