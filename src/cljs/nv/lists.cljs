(ns nv.lists
  (:require
   [nv.datum :refer [all-data!]]
   [nv.ui :as ui]))

(defn number [i]
  ;todo: I'm too lazy
  (cljs.pprint/cl-format nil "~4,'0d" i))

(defn cell [xs]
  [:div.flex.border.p-1.w-64.bg-red-300
   [:div.flex.text-gray-600.w-16.clip.items-center.border [:p.inv (first xs)]]
   [:div.flex.text-red-900 (rest xs)]])

(defn render-log-event
  [i {:event/keys [msg]}]
  (let [c [:div.flex.items-center.px-2.justify-between
           [:div.text-gray-600.w-12.clip.flex (number i)]
           [:div.text-red-900.flex-grow "/" msg]
           [:a.border.rounded.px-2.my-1.text-xxs.xbg-blue-500.text-black.border-black.shadow-4.select-none {:class (ui/pseudo "hover" :bg-black.text-white)} "Info"]]
        k (keyword (namespace msg))]
    (case k
      :msg.skift [:div.bg-orange-300.snap-start c]
      :msg.vakt [:div.bg-red-300.snap-start c]
      [:div.border.snap-start c])))

(defn render-log [system-log-data]
  (for [[e] (map vector system-log-data)]
    (render-log-event (:system/epoch e) e)))

(comment
  (do
    (filter #(= (:event/msg %) :msg.skift/set) (:system/log all-data!))))
