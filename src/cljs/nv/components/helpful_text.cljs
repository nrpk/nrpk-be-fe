(ns nv.components.helpful-text
  (:require [clojure.string :as string]
            [reagent.core :as re]
            [re-frame.core :as rf]
            [nv.datum]
            [clojure.pprint :refer [pprint]]
            [nv.ui :as ui]))

(def h1 :div.text-red-500.mb-4)
(def ing :div.text-red-200.mb-4)
(def p :div.text-red-100.mb-2)
(def mini-button :p.text-xxs.text-black.bg-yellow-500.font-bold.p-2.select-none.cursor-pointer)

(comment
  ;; todo: re-visit this later
  #_(defn action-for
      "returns a subsriber and the dispatcher"
      [{:keys [depends-on]} k]

      (rf/reg-sub-raw
       k
       (fn [db event]
         (reagent.ratom/reaction
          (let [d (rf/subscribe [depends-on])]
            (prn ">>> " d)
            (if @d
              (get db k false))))))

      (rf/reg-event-db k (fn [db _] (update db k not)))

      [(rf/subscribe [k])
       (fn [] (rf/dispatch [k]))])

  #_(rf/reg-sub
     :debug/questions
     (fn [db _]
       (prn "SKKSD")
       true)))

(defn help-text [{:keys [click-fn scene role]}]
  [:div.fixed.top-0.bottom-0.left-0.right-0.z-40.bg-black.shadow-4.opacity-75.border-4.border-yellow-500.mt-4.mb-16.pb-2.ml-16.mr-4
   {:style {:max-width :260px}}
   [:div.absolute.right-0.top-0
    [mini-button {:on-click click-fn} "mindre"]]
   [:div.h-full.scroll-y
    [:div.m-10.text-white.antialiased
     [:p (with-out-str (pprint 123))]
     [h1 "Header " scene]
     [ing (string/capitalize (apply str (repeat 20 "SOME HELPFUL TEXT ")))]]]])

(defn module-name [{:keys [click-fn scene role]}]
  [:div.fixed.top-0.right-0.z-10.bg-gray-900.shadow-4.cursor-pointer.select-none.opacity-90.border-white.border.shadow-6
   {:class (ui/pseudo "hover" "bg-gray-800")
    :on-click click-fn}
   [:div
    [:div.text-xl.text-white.center.font-black.text-yellow-500
     [:p.px-4.py-1 [:span "Del "
                    [:span.text-white (str scene)]
                    ", "
                    [:span.text-white (string/capitalize (name (get nv.datum/user-states role "?")))]]]]]])

(defn render [{:as opts :keys [scene]}]
  (let [help-overlay-type @(rf/subscribe [:debug/toggle-size])
        opts (assoc opts :click-fn #(rf/dispatch [:debug/toggle-size]))]
    (case help-overlay-type
      true (help-text opts)
      false (module-name opts))))
