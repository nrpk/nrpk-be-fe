(ns nv.components.archive)

(defn adder []
  [:div.border.border-dashed.border-black.mx-1.w-24.h-10.justify-center.flex.rounded-sm.bg-white.opacity-25.cursor-pointer
   [:a.text-4xl.align-end.self-center.items-end.flex "+"]])
(defn selected
  [y]
  [:div.border.border-ashed.border-black.mx-1.w-32.h-10.a-0.bg-red-400.rounded-sm {:href       "#home"
                                                                                   :aria-label "tips"}
   [:a.center.text-sm.text-white.font-thin y]])
(defn past
  [y]
  [:div.border.border-black.mx-1.w-32.h-10.rounded-sm.cursor-pointer.bg-gray-600
   [:a.center.text-sm.text-white.font-thin y]])
(defn future
  [y]
  [:div.border.border-black.mx-1.w-32.h-10.a-0.bg-gray-600.rounded-sm.cursor-pointer
   [:a.center.text-sm.text-white.font-thin y]])

(defn render []
  [:div.scroll-x
   [:div.flex.bg-gray-200.py-2.select-none
    (past 2016)
    (past 2017)
    (past 2018)
    (selected 2019)
    (future 2020)
    (adder)]])
