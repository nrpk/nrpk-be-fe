(ns nv.components.calendar
  (:require [cljs-time.core :as t]
            [yd.core :as yd]
            [nv.ui :refer [circle]]
            [nv.datum]
            [nv.ui :as ui]))

(defn slot-state-fn [yd offset]
  (let [start 9
        end 85]
    (let [open (<= start (+ offset yd) end)]
      {:open  open
       :day   (yd/yd->day-number-of-week yd)
       :weekend (and open (get #{4 5} (yd/yd->day-number-of-week yd)))
       :avail (and open (get #{1 2 7 4 5} (yd/yd->day-number-of-week yd)))})))

(defn random-name-abbr []
  (apply str (take (+ 2 (rand-int 3)) (shuffle (map (comp char (partial + 65)) (range 26))))))

(defn item' [opts datum]
  [:div.flex.justify-between.px-px.my-px.flex
   {:class (case (:type opts)
             :error [:border :border-red-900 :text-red-200 :bg-red-500]
             :reserve [:border :border-orange-500 :text-orange-200 :bg-purple-600]
             [:xborder :border-green-900 :text-green-900 :bg-green-200])}
   #_(rand-nth [[:div] (circle)])
   [:p datum]])

(defn name-initials [xs]
  (apply str (map first xs)))

(defn day-slot
  [yd]
  [:div.flex-col.flex.rounded-sm.justify-between.w-full.font-mono
   (let [vakt-liste (get nv.datum/all-data! :vakt/liste)]
     (when-let [skift (get-in nv.datum/all-data! [:skift yd])]
       (for [vakt-id skift]
         (if-let [navn (get-in vakt-liste [vakt-id :vakt/navn])]
           [item' {} (-> navn
                         (clojure.string/split " ")
                         name-initials)]
           [item' {:type (condp #(%1 %2) vakt-id
                           number? :error
                           keyword? :reserve
                           :other)}
            vakt-id]))))])

(defn pr-month
  [n]
  [:div.break-column [:div.inv-2.w-10.h-12 [:p.center n]]])

(defn header [day i s]
  (let [prev-month? (:prev-month s)
        weekend? (:weekend s)
        slot-type (fn [i] (if (:avail s)
                            (if weekend?
                              [:bg-gray-700 :text-white :border-green-500 :xborder :mb-px]
                              [:bg-gray-500 :text-black :border-green-500 :xborder :mb-px])
                            (if prev-month?
                              [:bg-gray-400 :text-gray-300 :border-gray-200 :mb-px]
                              [:bg-gray-500 :text-gray-600 :border-gray-200 :mb-px])))]
    [:div.border-none
     [:p.text-center.text-black {:class (if weekend? "text-red-600")} (subs (yd/yd->short-day-name day) 0 1)]
     [:div.text-center
      [:div.xfont-bold {:class (slot-type i)} i]]]))

(defn pr-day
  [day i s]
  (if (:prev-month s)
    [:div.h-full.border.bg-gray-300
     #_(header day i s)]
    [:div.h-full.border.bg-white
     (header day i s)
     [:div {:style {:xmin-width :1.8rem}}
      (if (:open s)
        (if (:avail s)
          (day-slot i))
        [:div.text-center.bg-gray-100
         [:p.text-blue-300 "s"]
         [:p.text-blue-300 i]])]]))

(defn subgrid [month]
  (let [first-weekday-in-month (yd/first-weekday-in-month 2019 month)]
    [:div.xw-full.px-2.xborder-r-4.border-orange-500 {:style {:min-width 200
                                                              :display               :grid
                                                              :grid-gap              4
                                                              :grid-template-columns "repeat(7, 1fr)"
                                                              ;:grid-template-rows "4rem"
                                                              :grid-auto-rows    "90px"}}

     [:div.flex.flex-col.items-end {:style {:grid-column "5/span 3"}}
      [:div.text-xl.font-black.a-1.mt-2 (str (nth yd/month-names month))]
      [:div.a-1.font-black.text-green-500 (str 1992)]]

     ;month prelude
     (for [e (range (- 7 first-weekday-in-month))]
       (pr-day 0 0 {:prev-month true}))

     ;main content
     (for [day (range 1 (yd/month->total-month-days (inc month)))]
       (let [i (yd/date-time->yd (t/date-time 2019 (inc month) day))
             s (slot-state-fn i 0)]
         (pr-day day i s)))]))

(def magic 8)

(defn user-slots [s]
  [:p.bg-green-200.mb-px.p-px {:style {:font-size 6}} (str s)])

(defn item
  ([] [:div.p-px.bg-gray-400])
  ([e] (if true
         [:div.p-px.bg-gray-300.cursor-pointer.select-none
          {:class (ui/pseudo "hover" "border-gray-600 border")}
          (yd/yd->day-number-of-week e) ; fix: this is off by one for every accumulated week because of the 8th column, see #1
          [:p.text-gray-500 (first (yd/yd->short-day-name e))]
          [:div.h-6.flex.items-center {:class (rand-nth '[text-red-500 text-green-500 text-blue-500])} (circle #{(rand-nth [:burst :mini :mini nil nil])})]
          [:p e]
          [:div.text-left (map user-slots ["CPST" "PTR"])]

          ;[:p.bg-yellow-500.text-black.mb-px {:style {:font-size 6}} "CPSW"]
          ;(y/yd->day-number-of-week e)
          [:p.bg-gray-400.border.text-gray-500.mb-px {:style {:font-size 6}} "—"]
          [:p.bg-gray-400.border.text-gray-500.mb-px {:style {:font-size 6}} "—"]
          [:p.bg-yellow-600.border.text-gray-100.mb-px {:style {:font-size 6}} "—"]]
         [:p.text-gray-500 (first (yd/yd->short-day-name e))])))

(defn placeholder-cal [month]
  [:div.text-xxs.text-black.text-center.bg-gray-200
   #_{:style {:max-width "820px"}}
   [:div.sticky.bg-gray-200.my-1.top-0
    [:div.text-right.text-xl.a-1.font-bold (nth yd/month-names month)]
    [:div.text-right.a-1.font-black.mb-1 2019]]
   [:div (ui/grid' {:grid-template-columns (str "repeat(" magic ",1fr)")
                    :grid-gap              1
                    :grid-auto-flow        :row
                    :grid-auto-rows        "auto"}
                   ; #1 fix: sequence is totally wrong here, fix with recur or a reducer
                   (map #(if (= 0 (mod % magic))
                           [:p.center.bg-gray-300.text-gray-400.font-black.text-xl (yd/yd->week %)]
                           [item %]) (range 0 31)))]])

(defn render "calendar" []
  [:div.text-xxs.bg-gray-300.pt-2
   {:style {:display               :grid
            :grid-gap :4px
            :grid-auto-rows "auto"
            :grid-template-columns "repeat(auto-fill,minmax(300px,1fr))"}}
   (for [month (range 4)]
     [:div.bg-gray-200 (subgrid month)])])

(def ig-config {})
