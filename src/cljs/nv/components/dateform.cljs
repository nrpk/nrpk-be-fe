(ns nv.components.dateform
  (:require [cljs-time.core :as t]
            [yd.core :as yd]
            [nv.ui :as ui]
            [re-frame.core :as rf]))

(rf/reg-event-db :add-date (fn [db [_ args]]
                             (update-in db [:whatever] conj [(get-in db [:whatever :new-text-slot] :Default)])))

(rf/reg-event-db :change (fn [db [_ slot args]]
                           (assoc-in db [:whatever slot] args)))

(rf/reg-sub :new-text-slot-text (fn [db [_ slot]]
                                  (get-in db [:whatever slot])))

(rf/reg-event-db :lock (fn [db [_ args]]
                         (update-in db [:locked] (fnil not false))))

(rf/reg-sub :is-locked? (fn [db _]
                          (:locked db)))

(defn render [{:as args :keys [start-values current-date]}]
  (letfn [(input-list [start-values current-date]
            (for [[k v] start-values]
              (if (t/before? (yd/str-date (:date v)) (yd/str-date current-date))
                [:<>
                 [:div {:style {:grid-column "1/1"}} (ui/date {:disabled 1 :value (:date v)})]
                 [:div {:style {:grid-column "2/2"}} (ui/input {:disabled 1 :value (:text v)})]]
                [:<>
                 [:div {:style {:grid-column "1/1" :justify-self :end}} (ui/button {:class "border-red-500 border-2 text-red-500"} "Fjern")]
                 [:div {:style {:grid-column "2/2"}} (ui/input {:placeholder "Beskrivelse" :value (:text v)})]
                 [:div {:style {:grid-column "3/3"}} (ui/date {:value (:date v)})]])))]
    (let [locked @(rf/subscribe [:is-locked?])]
      (ui/grid' {:grid-template-columns "2fr 1fr"}
             [:div.p-2.pb-10.bg-gray-300 {:style {:xwidth         :80%
                                                  ;:place-content :start
                                                  :aligsn-content :center}}
              [ui/grid' {:grid-gap              4
                         ;:grid-auto-rows "auto"
                         ;:grid-auto-flow :column
                         :justify-content       :start
                         ;:grid-template-rows "auto"
                         :grid-template-columns "repeat(3,minmax(auto-fit,300px)"}
               [:div {:style {:grid-column "1/4"}}
                (ui/header [:p.text-gray-500.a-1.mb-4 "Sesong 2019" #_[:span.text-xxs.font-thin (str "Now: " current-date)]])]
               (input-list start-values current-date)
               [:<>
                [:div {:style {:grid-column "1/span 1"}} (ui/date)]
                [:div {:style {:grid-column "2/span 1"}} (ui/input {:placeholder "Beskrivelse"
                                                                    :on-change   #(rf/dispatch [:change :new-text-slot (-> % .-target .-value)])})]
                [:div {:style {:grid-column "3/span 1"}} (ui/button {:class    "bg-green-500 text-white w-fill h-fill border-none"
                                                                     :on-click #(rf/dispatch [:add-date])}
                                                                    "Ny")]]]]
             [:div.bg-gray-300.h-full.w-full.center.text-center.select-none
              (if locked
                [:a.text-xl.border-2.px-4.py-2.border-gray-600.rounded.cursor-pointer.bg-gray-300.w-32.text-gray-600.shadow-5
                 {:on-click #(rf/dispatch [:lock])}
                 "Endre"]
                [:a.text-xl.border-2.px-4.py-2.border-black.rounded.cursor-pointer.bg-yellow-400.w-32.shadow-4
                 {:aria-label "Lås"
                  :on-click   #(rf/dispatch [:lock])}
                 "Endre"])]))))
