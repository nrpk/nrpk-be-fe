(ns nv.components.vaktliste
  (:require [nv.ui :as ui]
            [clojure.string :as string]
            [nv.datum]
            [re-frame.core :as rf]))

(defn list-item
  [e]
  ~[:div.z-50.py-1.px-2.border.m-0 {:class (ui/pseudo "hover" :bg-yellow-200.text-black)} e])

(defn list-header [_ [e time]]
  [:div.z-0.sticky {:style {:top :4rem}}
   [:div.font-bold.flex.justify-between.px-2.items-center.select-none.border-t.border-gray-500
    {:class [(if (< 12 time) :bg-gray-200 :bg-gray-100) (ui/pseudo "hover" "bg-yellow-200.text-black")]
     :style {:top :2.2rem}}

    [:div.flex-grow.text-gray-500
     (apply str (map first (string/split e " ")))]

    [:div.flex.text-blue-500
     [:div.h-6.w-6.xbg-green-300 (ui/circle #{(rand-nth [:fill :mini :burst nil nil])})]
     [:div.h-6.w-6.xbg-green-300 (ui/circle #{:burst})]
     [:div.h-6.w-6.xbg-green-300 (ui/circle #{:burst})]]

    ui/space
    [:p.w-12.border.px-2.text-black.text-right (str time "t")]]
   [:div.bg-white.text-black {:class (rand-nth [:h-12 :h-8 :h-16])} "Stiff"]])


(defn footer []
  [:div.bg-gray-800.py-1
   (ui/grid' {:grid-gap              4
              :grid-auto-rows        "auto"
              ;:grid-template-rows    "2.4rem"
              :grid-template-columns "repeat(auto-fit,minmax(44px,1fr))"}
             [ui/button-sm-toggle "Sorter" :vaktliste/toggle-help]
             [ui/button-sm-toggle "Opplæring" :vaktliste/toggle-help]
             [ui/button-sm-toggle "Medlemmer"]
             [ui/button-sm-toggle "Andre ting"])])

(defn item-x [i data-source]
  (case i
    0 (ui/cell [:div.flex-col.h-8 [:p "Antall"] [:p "Nøkkelvakter"]] (count data-source))
    (ui/cell [:div.flex-col.h-8 [:p "Antall"] [:p "Nøkkelvakters"]] (count data-source))))

(def help-panel
  [ui/help-panel
   [:div.p-2
    [:h1.text-xl "Beskrivelse"]
    [:p "some more text"]
    [:p "some more text"]
    [:p "some more text"]]])

(defn grid-columns [limit]
  {:style {:justify-content :start
           :display :grid
           :grid-gap 4
           :grid-template-columns (str "repeat(" limit  ", minmax(50px,1fr))")
           :grid-auto-flow :columns}})

(rf/reg-event-db :vaktliste/toggle-help (fn [db _] (update db :vaktliste/toggle-help not)))
(rf/reg-sub :vaktliste/toggle-help (fn [db] (:vaktliste/toggle-help db)))

(defn render "vaktliste"
  [{:keys [grid-area]}]
  (let [s (rf/subscribe [:vaktliste/toggle-help])
        data-source (map (comp :vakt/navn val) (:vakt/liste nv.datum/all-data!))
        g (fn [find-vakt [yd vakts]] {:yd yd
                                      :vakt/id (get vakts find-vakt)})
        f (fn [user-id]
            (map (fn [e] (g user-id e)) (:skift nv.datum/all-data!)))]
    [:div {:style {:height :100%}}
     [:div.flex.flex-col.h-full
      [:div.flex-grow.scroll-y

       [:div.sticky.top-0.z-0.bg-indigo-700.flex-col
        (ui/header [:div.flex.justify-between.items-center
                    [:p "Vaktliste"]
                    ui/space
                    [:div (grid-columns 2)
                     (ui/button-sm-toggle "A" :vaktliste/toggle-help)
                     (ui/button-sm-toggle "B")]])
        (ui/header-2 [:div.flex-col.flex.justify-between.items-center.z-0
                      (ui/button-sm-toggle "utvid" :vaktliste/toggle-help)
                      [:p.text-center "Interessante tall"]])

        [:div.pb-4
         (let [sub-item (fn [i] [:div.h-auto.shadow.snap-start.shadow (item-x i data-source)])]
           [:div.scroll-x
            [:div.pb-4.snap-x {:style {:display           :grid
                                       :padding           "0px 30% 0px 8px"
                                       :grid-auto-flow    :column
                                       :grid-gap          10
                                       :overflow          :auto
                                       :grid-auto-columns "minmax(70px,70px)"}}
             (map sub-item (range 2))]])]]

       (concat
         [[:div.z-0.sticky.bg-red-500.h-12 {:style {:top :2rem}}"Foran"]]
         (map-indexed list-header (map vector data-source (repeatedly #(rand-int 25)))))]
      [:div
       (footer)
       (when @s help-panel)]]]))

(comment
  #_(map (fn [[vakt-id v]]
           (let [new-v (assoc v
                              :vakt/id vakt-id
                              :skift (f vakt-id))]
             [vakt-item {:class (ui/pseudo "hover" :bg-gray-300)}
              [:p (str (keys new-v))]
              [:div vakt-id]
              (ui/grid' {:grid-gap              4
                         :grid-auto-rows        "2rem"
                         :grid-template-columns "repeat(auto-fill,minmax(90px,1fr))"}
                        (map list-item
                             (some->> vakt-id
                                      f
                                      (filter (fn [kv] (:vakt/id kv)))
                                      (map :yd)
                                      (map yd/yd->date-str))))]))

         (:vakt/liste nv.datum/all-data!)))
