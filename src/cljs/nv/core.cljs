(ns nv.core
  (:require [integrant.core :as ig]
            [playground.core1 :refer [alt-config subs-fn' toggle-fn change-fn]]
            [playground.sub :as sub]
            [re-frame.core :as rf]
            [yd.core :as yd :refer [y]]
            ;[cljs-time.core :as t]
            ;[cljs-time.format :as f]
            [cljs.pprint :refer [pprint]]
            [nv.datum]
            [nv.lists]
            [nv.ui :as ui]
            [playground.color]
            [nv.components.calendar]
            [nv.components.dateform]
            [nv.components.archive]
            [nv.components.vaktliste]
            [items.pomodore]
            [items.yearwheel :as yearwheel]
            [clojure.string :as string]

            [app.utils]))

(defn reg-toggle [d f]
  (rf/reg-event-db d f)
  (rf/reg-sub d d))

(defmethod ig/init-key :calendar/header [_ {:keys [calendar start length state-fn] :as args}]
  [ui/grid "5fr 50px"
   [:div.p-2.flex.justify-between.flex-row.items-baseline
    [:p.flex-grow.da-0.text-lg "Vaktlisten 2019"]

    [:div.flex.justify-around.flex-wrap.font-thin
     [:label.flex.flex-col.text-xxs.w-40 "Første dag"
      [:input.text-base.border.border-gray-700.rounded-sm._shadow-5.px-2.h-8 {:type "date"}]]
     [:label.flex.flex-col.text-xxs.w-40 "Begrenset"
      [:input.text-base.border.border-gray-700.rounded-sm._shadow-5.px-2.h-8 {:type "date"}]]
     [:label.flex.flex-col.text-xxs.w-40 "Siste dag"
      [:input.text-base.border.border-gray-700.rounded-sm._shadow-5.px-2.h-8 {:type "date"}]]
     #_[:p.a-1.px-10.border.border-gray-600.p-2.rounded-sm "Meny"]]]

   [:div.flex.justify-end.inv.center.text-black (ui/circle)]])

(defmethod ig/init-key :calendar/flow [_ {:keys [calendar start length state-fn] :as args}]
  [ui/grid "5fr 50px"
   [:div.p-2.flex.justify-between.flex-row.items-baseline
    [:p.flex-grow.da-0.text-lg "Vaktlisten 2019"]
    [:label.flex.flex-col.text-xxs.w-40 "Første dag"
     [:input.text-base.border.border-gray-700.rounded-sm._shadow-5.px-2.h-8 {:type "date"}]]]

   [:div.flex.justify-end.inv.center.text-black (ui/circle)]])

(defmethod ig/init-key :a/history
  [_ args]
  [:div.scroll-y {:style {:grid-area (:grid-area args)}} (nv.components.archive/render)])

(defmethod ig/init-key :calendar
  [_ {:as args :keys [grid-area]}]
  [:div.scroll-y {:style {:grid-area grid-area}} (nv.components.calendar/render)])

(defmethod ig/init-key :ui.season/form
  [_ {:as args :keys [grid-area start-values current-date]}]
  [:div.scroll-y {:style {:grid-area grid-area}} (nv.components.dateform/render args)])

(defmethod ig/init-key :ui.resource/vaktliste [_ {:as args :keys [grid-area]}]
  (if grid-area
    (ui/g-area grid-area (nv.components.vaktliste/render args))
    (nv.components.vaktliste/render args)))

(defmethod ig/init-key :system/root [_ {:keys [calendar calendar-flow  calendar-header] :as args}]
  [:div.bg-gray-300.xh-screen
   ;[:div calendar-header]
   ;[:div calendar-flow]
   [:div.text-xxs.text-white.bg-gray-900
    [:code (nv.datum/render)]]
   #_[:div.z-0 calendar]])

(defmethod ig/init-key :input [_ {:keys [calendar calendar-flow  calendar-header] :as args}]
  [:div.bg-green-200.flex-grow.p-0.xborder.m-2.flex.justify-between.flex-wrap.items-center.h-12
   [:div.flex-grow "Input filter ->"]
   [:div.flex.flex-grow.text-base
    [:input.h-full.w-40.px-3.py-1.bg-gray-200 {:type "" :placeholder "start"}]
    [:div.w-1]
    [:input.h-full.w-40.px-3.py-1.bg-gray-200 {:type "" :placeholder "sakte"}]
    [:div.w-1]
    [:input.h-full.w-40.px-3.py-1.bg-gray-200 {:type "" :placeholder "slutt"}]
    [:div.w-1]]
   (interpose [:div.w-1]
              (map (fn [e] [:div.h-8.mb-px.rounded-sm.px-2.text-gray-500.bg-gray-300 {:class (ui/pseudo "hover" :bg-black.text-white)} e])
                   '[Bekreft Låst]))
   #_(for [[k v] (dissoc args :state-fn)] v)])

(defmethod ig/init-key :ui.status/card [_]
  [:div.py-4.bg-gray-400.px-2.text-gray-100
   (ui/grid' {:grid-template-rows "30px"}
             (ui/header "Nøkkeltall")
             (ui/grid' {:grid-gap :10%
                        :justify-content :center
                        :grid-auto-flow :column

                        :grid-template-columns "repeat(auto-fit,minmax(20%,20%))"}
                       (ui/card "Antall skift" 423)
                       (ui/card "Antall dager" 183)
                       (ui/card "Antall skift" 29)))])

(defmethod ig/init-key :system/root2 [_ {:as args :keys [layout-fn]}] (layout-fn args))

(defmethod ig/init-key :system/root3 [_ main] main)

(defmethod ig/init-key :system/root4 [_ main]
  [:div.flex.w-full.h-full
   [:div.scroll-y.xflex-grow (:a main)]])

(defmethod ig/init-key :system/pomodore [_ main]
  (items.pomodore/render))

(defmethod ig/init-key :system/yearwheel [_]
  yearwheel/rendered)

#_(ui/grid' {:grid-template-columns "repeat(auto-fit,minmax(260px,1fr))"
             :grid-template-rows    "auto"
             :grid-auto-flow        "row dense"}
            (let [item (fn [e]
                         [:div.shadow.p-2
                          [:div [:p.font-black.inv.text-right.shadow (y/yd->date-sans-str e)]]
                          [:div.flex.mt-1
                           [:div.flex-grow.p-2 (arcs.dial-definitions/sample-simple-timeline arcs.dial/draw-arc {})]
                           [:p.w-32.flex-grow.text-justify.bg-gray-900.opacity-25.p-1.text-xxs "Kanskje vise ekstra forklaringer i en tabell her?"]]])]
              (map item (range 4))))

(defmethod ig/init-key :default [x default]
  (if default
    [:div default]
    (ui/placeholder-sm'' ui/error-style
                         [:p.text-4xl "default-method " [:span.font-mono.text-white.underline (str x)]])))

(defmethod ig/init-key :system/log [_ {:as args :keys [grid-area]}]
  [:div.scroll-y.snap-y {:style {:grid-area grid-area}}
   (ui/grid' {:grid-template-columns "1fr"        ;"repeat(auto-fill,minmax(240px,1fr))"
              :grid-auto-rows        "24px"
              :grid-auto-flow        :row
              :grid-gap              1}
             (nv.lists/render-log (reverse (:system/log nv.datum/all-data!))))])

(def config
  {:playground/color      (playground.color/render)

   :system/root4          {:a (ig/ref [:ui.resource/vaktliste])
                           :b (ig/ref [:system/root])}

   :system/root3          (ig/ref [:ui.resource/vaktliste])

   :system/root2          {:layout-fn  (fn [{:as args :keys [components decorator]}]
                                         [:div.flex.flex-col
                                          [:div.debug.scroll-y.flex-grow
                                           [ui/grid' {:grid-template-areas   "'vaktliste z z'
                                                                              'vaktliste c a'
                                                                              'vaktliste b a'
                                                                              '.         b a'"
                                                      :grid-template-columns " 1fr 2fr 100px"
                                                      :grid-template-rows    "44px auto 1fr 100px"
                                                      :grid-gap              4}
                                            (decorator "x")
                                            (map identity components)]]])
                           :decorator  (fn [c] c)
                           :components (mapv ig/ref [:system/log
                                                     :ui.season/form
                                                     :a/history
                                                     :calendar
                                                     :ui.resource/vaktliste])}

   :ui.resource/vaktliste {}

   :system/log            {:grid-area "a"}
   :system/root           {:state-fn        nv.components.calendar/slot-state-fn
                           :calendar        (ig/ref :calendar)
                           :calendar-flow   (ig/ref :calendar/flow)
                           :calendar-header (ig/ref :calendar/header)
                           :user-list       (ig/ref :ui.season/form)
                           :resource        (ig/ref :ui.resource/vaktliste)
                           :status          (ig/ref :ui.status/card)}
   :input                 {}
   :ui.season/form        {:grid-area    "c"
                           :current-date "2019-12-11"
                           :start-values {1 {:date "2019-12-10" :text "Åpning"}
                                          2 {:date "2019-12-15" :text "Stenging"}
                                          3 {:date "2019-12-25" :text "Dugnad Vår"}
                                          4 {:date "2019-12-28" :text "Dugnad Høst"}
                                          5 {:date "2019-12-31" :text "Pinse"}}}
   :ui.status/card        {}
   :calendar/flow         {}
   :calendar              {:offset 0 :length 60 :grid-area "b"}
   :calendar/header       {}
   :a/history             {:grid-area "z"}
   :system/pomodore       {}
   :system/yearwheel      {}})

#_(def config2
    {:a/root                {}
     :a/history             {}
     :ui.resource/vaktliste {}})

;;
;;
;; helpers

#_(defn render-history []
    (:a/history (ig/init config2) (ui/placeholder-sm' ui/error-style "No such component")))

(defn render
  ([]
   (:system/root (ig/init config) ui/placeholder))
  ([k]
   (get (ig/init config) k ui/placeholder-sm)))

#_(defn render-calendar []
    (:calendar (ig/init config) ui/placeholder))

(defn render-header []
  (:calendar/header (ig/init config) ui/placeholder))

(defn render-xyz []
  (:system/root2 (ig/init config)))

(defn render-y []
  (:system/root3 (ig/init config)))
