(ns nv.datum
  (:require
   [clojure.set :refer [map-invert]]
   [cljs.pprint :refer [pprint]]
   [clojure.set :as set]
   [clojure.string :as string]
   [yd.core :as yd]
   [nv.ui :as ui]))

;(ns-unmap 'nv.datum 'parse)

(def user-states (into {} (map vector (map inc (range)) [:opplæring :nøkkelvakt :admin :æresmedlem :sluttet])))

(def initial-model {:system/epoch 0
                    :system/log    []
                    :vakt/neste-id 1})

(defn compute-next-id [st]
  ; todo: make this synchronous
  (update-in st [:vakt/neste-id] inc))

(defmulti parse (fn [st e] (:event/msg e)))
(defmethod parse :msg.skift/set [st {:keys [args extra]}] (update-in st [:skift (:yd args)] #(conj (set %) (:vakt/id args))))
(defmethod parse
  :msg.skift/remove [st {:keys [args z]}] (update-in st [:skift args] disj z))
(defmethod parse
  :system/log [st {:keys [args]}] (update-in st [:system/log] conj (into {} args)))
(defmethod parse
  :end [st {:keys []}] st)
(defmethod parse
  :other [st {:keys []}] st)
(defmethod parse :msg.vakt/add [st {:keys [args]}]
  (comment
    {:vakt/liste
     {1
      {:vakt/navn "Peter Soot",
       :vakt/epost "home@net",
       :vakt/telefon "12345678"},
      2
      {:vakt/navn "Arne Petter Johannessen",
       :vakt/epost "at@net",
       :vakt/telefon "10000000"}}})
  (let [next-id (get-in st [:vakt/neste-id])
        path [:vakt/liste next-id]]
    (-> st
        (assoc-in path args)
        compute-next-id)))
(defmethod parse :msg.vakt/update [st {:keys [args]}]
  (comment
    {:next-id 3,
     :data    {1
               {:vakt/navn    "Peter Soot",
                :vakt/epost   "home@net",
                :vakt/telefon "12345678"},
               2
               {:vakt/navn "Arne Petter Johannessen",
                :vakt/epost "at @net",
                :vakt/telefon "10000000 "}}})
  (let [[id record] [(:vakt/id args) (:data args)]]
    (-> st
        (update-in [:vakt/liste id] merge record))))
(defmethod parse :default [st & r]
  (prn "no op for " r)
  st)

(defn make-msg [evt args #_#_& [extra]]
  {:event/msg evt :args args #_#_:extra extra})

(defn make-log [& e]
  {:event/msg :system/log :args e :z 'jalla})
(defn inc-epoch [st]
  (update-in st [:system/epoch] (fnil inc 0)))
(defn get-epoch [st]
  (:system/epoch st))

(defn random-name!
  []
  (->> (repeatedly #(rand-int 26))
       (map (comp char (partial + 65)))
       (take (+ 2 (rand-int 10)))
       (apply str)
       (string/capitalize)))

(def random-full-name!
  (fn [] (apply str (interpose " " (take (+ 2 (rand-int 3)) (repeatedly random-name!))))))

(def vakt-status-flags #{:vakt.status/under-opplæring :normal :admin :æresmedlem})

(def some-random-names!
  (into [] (concat (reduce (fn [a e]
                             (let [s (random-full-name!)]
                               (conj a
                                     [:msg.vakt/add {:vakt/navn    s
                                                     :vakt/epost   (str "s" "@net")
                                                     :vakt/telefon "12345678"
                                                     :vakt/status  (rand-nth (seq vakt-status-flags))}]
                                     [:msg.skift/set {:yd 1 :vakt/id 123}])))
                           []
                           (range 133)))))

; core
(def just-data
  (concat
   [[:msg.vakt/add {:vakt/navn    "Peter"
                    :vakt/epost   "home@net"
                    :vakt/telefon "12345678"}]
    [:msg.vakt/add {:vakt/navn    "Arne"
                    :vakt/epost   "at@net"
                    :vakt/telefon "10000000"}]
    [:msg.vakt/add {:vakt/navn    "Jeppe Hjelper"
                    :vakt/epost   "jp@net"
                    :vakt/telefon "20000000"
                    :vakt/status  :vakt.status/under-opplæring}]
    [:msg.vakt/update {:vakt/id 2
                       :data    {:vakt/navn "Arne Petter Johannessen"}}]

    [:msg.vakt/update {:vakt/id 1
                       :data    {:vakt/navn "Peter Soot"}}]

    [:msg.skift/set {:yd 80 :vakt/id 1}]
    [:msg.skift/set {:yd 80 :vakt/id 2}]
    [:msg.skift/set {:yd 82 :vakt/id 1}]
    [:msg.skift/set {:yd 83 :vakt/id 1}]
    [:msg.skift/set {:yd 84 :vakt/id 1}]

    [:othesr 123]
    [:msg.skift/remove 1 'b]
    [:msg.skift/remove 1 'a]
    [:end 123]]
   (map #(vector :msg.skift/set {:yd % :vakt/id 1}) (range 100))
   some-random-names!
   (comment
     (map #(vector :msg.skift/set {:yd (rand-int 200) :vakt/id 3}) (range 200))
     (map #(vector :msg.skift/set {:yd % :vakt/id 2}) (range 20))
     (map #(vector :msg.skift/set {:yd % :vakt/id (rand-int 10)}) (range 20)))))

(defn p "Takes the state and applies a cmd to it.
         Increases [:system/epoch], constructs a msg and writes the epoch to the msg,
         constructs a copy for the log, and threads both through parse." [st & [cmd]]
  (let [st (inc-epoch st)
        msg (assoc (apply make-msg cmd) :system/epoch (get-epoch st))
        log (apply make-log msg)]
    (-> (parse st msg)
        (parse log))))

(def all-data! (reduce p initial-model just-data))

(defn item-skift [[k v]]
  [:div.text-white.border.xw-24.xh-16.text-center.p-1
   [:div.center
    [:div
     [:p.text-xs.font-bold (yd/yd->date-sans-str k)]
     [:p v]]]])

(defn item-value
  ([v]
   (item-value "" v))
  ([s v]
   [:div.text-white.border.text-center.p-1.bg-orange-400
    [:div.center
     [:p.text-base s " " v]]]))

(defn item-log [{:event/keys [msg]}]
  [:div.text-white.border.text-center.p-1.bg-green-400
   [:div
    [:p.text-base msg]]])

(defn name-initials [xs]
  (map first (clojure.string/split xs " ")))

(defn item-vakt [[k v]]
  [:div.text-white.border.text-center.p-1.bg-red-400
   [:div
    [:a.text-base {:href (str "#/person" k)} (name-initials (:vakt/navn v))]]])

(defn filter-bar []
  [:div.bg-gray-400.text-gray-800.px-2.py-1.text-xxs.cursor-default
   (ui/grid' {:grid-gap              1
              :grid-auto-rows        "auto"
              :grid-template-rows    "1rem"
              :grid-template-columns "repeat(auto-fill,minmax(20%,1fr))"}
             #_[:div.rounded-xs.bg-gray-300 {:class (ui/pseudo "hover" :bg-yellow-200.text-black)}
                [:div.center.h-4.text-xxs "Vakter"]]

             [ui/togglebar-item-sm "Vakter"]
             [ui/togglebar-item-sm "Skift"]
             [ui/togglebar-item-sm "Feil"]
             [ui/togglebar-item-sm "Bevegelser"])])

(defn render []
  (let [state (assoc all-data! :system/log (count (:system/log all-data!)))]
    [:div.font-sans
     [:div.bg-gray-400.xpx-2.pt-1.h-auto
      [:input.h-8.px-2.text-base {:placeholder "søk/search"}]]
     [:div.bg-gray-400.h-auto
      (filter-bar)]
     [:div.bg-pink-900.text-pink-700.px-2.pt-1
      (ui/header "informatsione")
      [:div.flex.justify-around.p-4.text-green-600
       {:style {:grid-gap 1}}
       (ui/cell {:class [:bg-pink-800 :text-pink-500 :border-none]} "antall byttinger" [:p.text-pink-300 321])
       (ui/cell {:class [:bg-pink-800 :text-pink-500 :border-none]} "vakter totalt" [:p.text-pink-300 321])
       (ui/cell {:class [:bg-pink-800 :text-pink-500 :border-none]} "mangler dekning" [:p.text-pink-300 321])]]
     [:div.w-128.flex.flex-wrap.font-sans.cursor-pointer



      (item-value "epoch" (:system/epoch all-data!))
      (item-value ":vakt/neste-id" (:vakt/neste-id all-data!))

      (for [e (:skift all-data!)]
        (item-skift e))


      (for [e (:system/log all-data!)]
        (item-log e))

      (for [e (:vakt/liste all-data!)]
        (item-vakt e))

      [:div.column-break.break-column]

      #_[:pre.font-mono (with-out-str (pprint (dissoc all-data! :skift :system/epoch :system/log :vakt/liste :vakt/neste-id)))]]]))

