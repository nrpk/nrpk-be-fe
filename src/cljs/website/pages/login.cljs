(ns website.pages.login)

(defn render []
  [:div.mt-20.bg-gray-800.text-white.p-1.line "LOGIN"
   [:div
    [:input#huey {:type "radio" :name "drone" :value "Medlem" :checked "true"}]
    [:label {:for "huey"} "Medlem"]]
   [:div
    [:input#dewey {:type "radio" :name "drone" :value "Nøkkelvakt"}]
    [:label {:for "dewey"} "Nøkkelvakt"]]
   [:div
    [:input#louie {:type "radio" :name "drone" :value "Styre"}]
    [:label {:for "louie"} "Styre"]]])
