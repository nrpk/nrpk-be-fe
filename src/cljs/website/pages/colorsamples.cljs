(ns website.pages.colorsamples
  (:require [clojure.math.combinatorics :as combo]
            [thi.ng.color.core :as col]
            [website.toolbar :as  toolbar]))

(def niceBlue :#55C6F1)
(def primBlue :#61839E)
(def primRed :#CD7F8A)
(def primBrown :#7E6868)

;:#333 :#aaa :#a00 :#0a0 :#00a
(def palette (flatten
              [(for [c [niceBlue primBlue primBlue primRed primBrown :#121012 :#e1e5e1]
                     e (range 1 10)]
                 @(-> (col/css (str (name c)))
                      (col/adjust-luminance (/ (- 5 e) 8))
                      col/as-int24 col/as-css))]))

(defn page [])

(def colors (combo/permutations palette))

(def colors' (map list palette (repeatedly (constantly :#fff)))
  #_(distinct (flatten1 (combo/permutations (partition 2 1 palette)))))

(defn render []
  [:div.mt-12

   [:div.px-1
    [:h1.text-6xl.text-white.font-black "Tigerstad"]
    (for [e (range 20)]
      [:p.text-white "Some text " e])]

   [:div.bg-gray-300.text-black.h-10.sticky.shadow-6.w-full {:style {:top "3rem"}}
    [:div.text-black [toolbar/controls]

     ]]
   [:div.p-1._mt-10.text-white
    [:div {:style {:display               :grid
                   :grid-gap              :4px
                   :grid-template-columns "repeat(auto-fit,minmax(88px,1fr))"
                   :grid-auto-rows        "minmax(1fr,auto-fill)"}}
     (for [x palette
           y palette
           :when (not= x y)]
       (let [a x
             b y]
         [:div {:style {:place-self :stretch
                        :background a :color b}}
          [:svg._p-2 {:width "100%" :height "auto" :preserveAspectRatio "xMaxYMax slice" :viewBox "0 0 10 10"}
           [:circle {:cx 10 :cy 10 :r 10 :fill b}]]
          #_(for [e palette
                  :when (not= e x)]
              [:p.text-xs.center {:style {:color e}} "Some text"])
          #_[:div.text-xs.center.flex-col.h-full
             [:div.p-1 a]
             [:div.p-1 b]]]))]]])
