(ns website.pages.kurs
  (:require [markdown.core :refer [md-to-html-string*]]
            [website.pure.utils :refer [dangerous]]))

(defn data [n]
  (case n
    10 {:title "Kurs ved NRPK"
       :content

"# Kurs ved NRPK
Kurstrinnene i Padleforbundets våttkortstige for havpadling er:

- Grunnkurs hav
- Teknikkurs hav
- Aktivitetslederkurs hav


>  Se [her](www.padling.no) for en detaljert beskrivelse.

NRPK holder hver sommer grunnkurs i havpadling til en gunstig pris for medlemmene. Dette er et to-dagers kurs og annonseres på hjemmesiden.


Klubben kan subsidiere teknikkurs for våre medlemmer og i enkelte tilfeller aktivitetslederkurs. Se [her](www) for mere detaljer"}

    11 {:title "Kano"
       :content
"For kano er trinnene på våttkortstigen:

- Grunnkurs kano
- Sikkerhetskurs kano
- Teknikkurs kano
- Aktivitetslederkurs kano


>  Se [her](www.padling.no) for en detaljert beskrivelse.

NRPK setter opp Grunnkurs kano hver sommer på Nøklevann. Dette er et todagers kurs, og annonseres på hjemmesiden. Klubben kan subsidiere sikkerhetskurs, teknikkurs og i enkelte tilfeller aktivitetslederkurs for våre medlemmer – se [her](www) for mere informasjon"}

    2 {:content
"NRPK har foreløpig ikke planlagt noe formelt kurs innen roing i år, men det vil bli gitt instruksjon i roing, hovedsakelig på enkelte onsdager i løpet av sommeren.

## Suvi robåter
Suvi-båtene er solide, stabile robåter med plass til flere. Har man ikke rodd før, er disse gode å begynne med. Det kreves ikke noen spesiell instruksjon for å bruke disse, men spør gjerne en av nøkkelvaktene om hjelp til sjøsetting og igjen når båten skal tas opp av vannet og sveives opp på trallen sin på bryggen.

## Scullere
Før du kan låne våre scullere må du få informasjon og veiledning, dersom du ikke har erfaring fra tilsvarende båter. Dette både for å unngå farlige situasjoner (f.eks. velt i kaldt vann) og for å hindre at utstyr blir skadet. Vår Ro-oppmann Jens Christian kan hjelpe deg. Kontaktes på [epost](jcriis@gmail.com)"}

    3 {:title "SUP (Stand Up Padling)"
       :content
"# SUP (Stand Up Padling)
Ønsker du også å padle SUP på Nøklevann? Meld deg på kurs som etterhvert blir annonserert.

Kurset holdes på Nøklevann og varer rundt 3 timer og målet er at alle skal blir trygge på brettet, lære padleteknikk, av og påstigning, sikkerhetsrutiner og at du får anledning til å prøve ut de ulike brettene vi har.

Du kan begynne på de mest stabile, brede brettene. Kanskje du får lyst til å prøve sprint-brettet allerede på kurset!

Tid: Kanskje en onsdag i juni, kl.18.30-21.30, pris: kr. 100,-

Påmelding: Benytt NRPKs hjemmeside når vi har satt opp en dato."}

    0 {:title "Livredningskurs"
       :content
"# Livredningskurs
NRPK arrangerer hvert år livredningskurs med instruktør fra Norges Livredningsselskap. Kurset varer rundt 2-3 timer og holdes på Holmlia bad hvor klubben også har sine bassengtreninger med kajakk.

Kurset gir et kompetansebevis i livredning som er gyldig ut neste kalenderår. Kursdeltagere forplikter seg til å være tilstede som bassengvakt to ganger (dette er en forutsetning for at klubben kan leie basseng av Oslo kommune.)

Kurset inkluderer redningsteknikker i basseng og grunnleggende hjerte- lungeredning og krever

- 200 meter svømming,
- derav 100 meter rygg,
- og å øve på å dykke etter dukke.


Avslutningsprøve består i å hente opp en dukke på bunnen og svømme med en person en bassenglengde.

Kurset er åpent for alle medlemmer i klubben **men nøkkelvakter vil bli prioritert** hvis det blir fulltegnet. Vi oppfordrer nøkkelvaktene til å ta kurset. Kurset er gratis."}

    1 {:title "Subsidiering"
       :content
"# Subsidiering

NRPK dekker hele kursavgiften når medlemmer tar et eller flere av følgende kurs:

- Førstehjelpskurs (hjerte-/lungeredning)
- Livredningskurs
- Aktivitetslederkurs

Den som får dekket aktivitetslederkurs vil måtte kunne holde grunnkurs som gjenytelse til klubben. Deltagelse på slike kurs skal forhåndsgodkjennes av styret.

NRPK arrangerer også kurs etter etterspørsel der det gis rabatt for medlemmer:

- Grunnkurs kano
- Sikkerhetskurs kano
- Grunnkurs roing
- Grunnkurs hav
- Grunnkurs flattvann
- Teknikk-kurs hav
- Teknikk-kurs flattvann
- SUP-kurs

For medlemmer som tar eksterne våttkortkurs som ikke tilbys av NRPK, for eksempel grunnkurs elv, kan NRPK eventuelt dekke en del av kursavgiften.  Dette må forhåndsgodkjennes av styret og kurstilskudd blir i så tilfelle utbetalt etterskuddsvis mot oversendelse av kvittering."}

    ""))

(defn render []
  [:div.w-full.scroll-x.snap-x.h-full.center
   [:div.w-full.xh-full.flex
    (for [e (range 0 10)]
      [:div.h-full.bg-gray-800.snap-center.mx-2.rounded-lg.p-2.flex-col
       ;[:p.text-xl.pb-4.text-yellow-500 (:title (data e))]
       (dangerous :section.w-128.text-base.text-gray-400.art1.overflow-hidden
                  {}
                  (:html (md-to-html-string* (:content (data e)) {})))])]])
