(ns website.content
  (:require [reagent.core]))

(defn- content []
  (fn []
    [:div.text-blue-800.font-bold.w-32.h-32.center {:style {:min-width :64px
                                                            :min-height :64px
                                                            :max-width :88px
                                                            :max-height :88px

                                                            :font-size :10.5vmin}} "3"]))

(defn render'                                       []
  [:div.bg-blue-800.h-full.center.z-50
   [:div.shadow-6.bg-blue-900 {:style {:transform "rotate(-5deg)"}} [content]]])

(defn render []
  (fn []
    [:div.h-full.px-16.py-12.bg-blue-900 {:style {:border-width :16px
                                                  :min-width :256px
                                                  :_border-color :#158670
                                                  :_background :#259680
                                                  :overflow-y :scroll}}
     [:div.flex.flex-col.align-center.h-full

      [:div.flex.justify-between.text-black.h-64.mt-10.uppercase
       {:style {:font-size "calc(8px + 1.5vw)"}}
       [:div.text-blue-800.flex.flex-col.justify-end  {:style {:font-family "Inter-light-BETA"}} "chris schreiner"]
       [:div.inline-block.text-blue-800.font-black.flex.flex-col.justify-end "torv.be"]]

      [:div.shadow-6.h-full.w-full.bg-blue-800.rounded-br {:style {:min-height :44px
                                                                   :min-width :160px}} #_[render']]
      #_[:div.pt-16.px-8.justify-end.flex-col.flex {:style               {:color :#559680
                                                                          :transform "rotate(0deg)"}}
         [:svg.shadow-4 {:preserveAspectRatio "xMidYMax meet"
                         :width               :auto
                         :height              :95%
                         :viewBox             "0 0 100 100"}
          [:rect {:x 0 :y 0 :width 100 :height 100 :fill :#666 :stroke :currentColor :stroke-width :4px}]
          [:g {:style {:transformOrigin "30% 0%"
                       :transform "rotate(22deg)"}}
           [:rect {:x 40 :y 40 :width 67 :height 120 :fill :#333 :stroke :currentColor :stroke-width :2px}]]]]]]))

