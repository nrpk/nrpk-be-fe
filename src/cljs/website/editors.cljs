(ns website.editors
  (:require
   ["codemirror/mode/clojure/clojure"]
   ["codemirror/mode/gfm/gfm"]
   ["codemirror" :as cm]
   ["codemirror/addon/edit/matchbrackets"]
   ["codemirror/addon/lint/lint"]
   ["codemirror/addon/runmode/runmode"]
   ["parinfer" :as pi2]
   ["parinfer-codemirror" :as pi :refer (init version)]
   [sci.core :refer [eval-string]]
   [website.button :as but :refer [button]]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [website.toolbar :as toolbar]
   [markdown.core :as md]
   [website.pure.utils :refer [sprint linked dangerous]]
   [clojure.core.async :as a :refer [<! >! put! go go-loop alts! timeout close! chan]]))

(defn debounce [in ms]
  (let [out (chan)]
    (go-loop [last-val nil]
      (let [val (if (nil? last-val) (<! in) last-val)
            timer (timeout ms)
            [new-val ch] (alts! [in timer])]
        (condp = ch
          timer (do
                  (when-not (>! out val)
                    (close! in))
                  (recur nil))
          in (if new-val (recur new-val)))))
    out))

;;;

(defn jalla
  "testings"
  [x]
  (mapv #(vector :p.text-white %) (range x)))

(defonce lineWidget (atom nil))

(def editor-settings
  {:highlightFormatting true
   :matchBrackets       true
   :lineNumbers         true
   :autocorrect         true
   :taskLists           true
   :spellcheck          true
   :firstLineNumber     1
   :lint                 {:lintOnChange true}
   :gutters              ["CodeMirror-lint-markers"]})

(defn editor' [{:keys [id parsing-fn default-value mode theme  line-wrapping]
                :or {theme "default"
                     line-wrapping false
                     parsing-fn #(prn "DEFAULT EVAL " %)}}]
  (let [in (chan)
        out (debounce in 1500)
        ln? (rf/subscribe [::toolbar/ln? id])
        theme (rf/subscribe [::toolbar/theme id])

        local-editor-ref (r/atom nil)]

    ;; on debounce:
    (go-loop []
      (let [_ (<! out)] ;: parked here
        (parsing-fn local-editor-ref))
      (recur))

    (r/create-class
     {:display-name   (str id "-editor")

      :reagent-render (fn [m]
                        (when-let [cm @local-editor-ref]
                          (.setOption cm "lineNumbers" @ln?)
                          (.setOption cm "theme" (nth ["oceanic-next" "neo"] @theme "shadowfox")))
                        [:textarea
                         {:type          "text"
                          :id            id
                          :default-value default-value
                          :auto-complete "off"}])

      :component-did-mount
      (fn [this]
        (let [opts (merge editor-settings {:mode mode})
              cm' (.fromTextArea cm
                                 (r/dom-node this)
                                 (clj->js opts))]
          (.on cm' "change" (fn [instance changeObj]
                                               ;; we are not interested in the changeObj
                              (put! in cm')))
          (pi/init cm')
          (.removeKeyMap cm')
          (.setOption cm' "extraKeys" #js {:Cmd-Enter (fn [] (parsing-fn local-editor-ref))})
          (.setOption cm' "theme" "mbo")
          (.setOption cm' "lineWrapping" line-wrapping)

                           ;(reset! editor-ref cm')
          (reset! local-editor-ref cm')
                           ;(prn "M2> " @local-editor-ref)
          (parsing-fn local-editor-ref)))
      :component-will-unmount
      (fn []
        (let [cm @local-editor-ref]
                           ; toTextArea will destroy and clean up cm
          (.toTextArea cm cm)))})))

(defn compile-md!
  [editor-ref]
  (let [data (md/md-to-html-string* (.getValue @editor-ref) {})]
    (rf/dispatch [::toolbar/set-html-parsed :markdown data])))

(defn compile-sci! [editor-ref]
  (when @lineWidget (.clear @lineWidget))
  (try (let [res (eval-string (.getValue @editor-ref)
                              {:bindings    {'jalla   jalla
                                             'prn     prn
                                             'println println}

                               :realize-max 10000})
             res-string (pr-str res)]
         (prn "> " res res-string)
         (rf/dispatch-sync [::some-result {:result res}])
         (.runMode cm res-string "clojure" (js/document.getElementById "result")))
       (catch ExceptionInfo e
         (let [{:keys [:row]} (ex-data e)]
           (if row
             (let [msg (aget e "message")
                   ;todo: remove refs
                   msg-node (js/document.createElement "div")
                   icon-node (.appendChild msg-node (js/document.createElement "span"))
                   _ (set! (.-innerHTML icon-node) "!")
                   _ (set! (.-className icon-node) "lint-error-icon")
                   _ (.appendChild msg-node (js/document.createTextNode msg))
                   _ (set! (.-className msg-node) "lint-error")
                   lw (.addLineWidget @editor-ref (dec row) msg-node #js {:coverGutter false
                                                                          :above       false})]
               (when lineWidget (.clear @lineWidget))
               (when lw (reset! lineWidget lw))
               (rf/dispatch-sync [::some-result {:error msg}])
               #_(.clear lineWidget)
               #_(prn lw))

             (.runMode cm (str "ERROR: " (aget e "message"))
                       "clojure"
                       (js/document.getElementById "result")))))))

(defn grid [& xs]
  [:div.p-px {:style {:display :grid
                      :grid-gap :2px
                      :grid-template-columns "minmax(88px,1fr) 1fr 2fr"
                      :grid-auto-rows    "32px"}}
   xs])

(defn editor-code [{:keys [id]
                    :as m}]
  (let [controls-fn toolbar/controls
        preview (rf/subscribe [::toolbar/preview-toggle id])
        parsed-html (rf/subscribe [::toolbar/html-parsed :markdown])]
    (fn []
      [:div.bg-blue-900
       [:div.flex.p-1
        (button {:caption "Extract" :style :default :_sub-caption "cmd-enter" :click-fn compile-sci!})
        [:div.text-white id]]
       [controls-fn id]
       (if @preview
         (dangerous :section.art1 {:style {:width "100%"}} (:html @parsed-html))
         [editor' m])])))

(defn editor-markdown [{:keys [id]
                        :as m}]
  (let [controls-fn toolbar/markdown-controls
        preview (rf/subscribe [::toolbar/preview-toggle id])
        parsed-html (rf/subscribe [::toolbar/html-parsed :markdown])
        m (merge m {:mode          "gfm"
                    :line-wrapping true
                    :parsing-fn    compile-md!})]
    (fn []
      [:div
       [:div.bg-gray-100.text-xs.sticky.z-40.shadow-4 {:style {:top "3rem"}}
        [grid
         (button {:caption  "Publiser"
                  :style    :hollow
                  :click-fn #()})
         (button {:caption  "Tid"
                  :style    :hollow
                  :click-fn #()})
         [:div.text-gray-900.px-1 {:style {:justify-self :end
                                           :align-self   :center}} (linked "Sample " id)]]]
       [controls-fn id]
       (if @preview
         (dangerous :section.art1 {:style {:width "100%"}} (:html @parsed-html))
         [editor' m])])))
