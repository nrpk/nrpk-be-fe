(ns website.keyboard
  (:require [re-frame.core :refer [dispatch dispatch-sync]]
            [re-pressed.core :as rp]
            [website.keyboard-defs :as k]))

(defn shortcut->re-frame-event
  ([shortcut event]
   [[(event) [shortcut]]])
  ([shortcut-fn event-fn key-fn]
   (fn [e] [(event-fn e) (shortcut-fn (key-fn e))])))

(defn keydata []
  (concat
   (shortcut->re-frame-event {:keyCode k/X :ctrlKey true} #(vector :debug/toggle-size))
   (shortcut->re-frame-event  {:keyCode k/Z :ctrlKey true} #(vector :debug/questions))
   (shortcut->re-frame-event  {:keyCode 63 :shiftKey true :ctrlKey false} #(vector :debug/questions)) ;; todo: fix
   (->> (range 1 10)
        (map (shortcut->re-frame-event #(vector {:keyCode % :shiftKey true})
                                       #(vector :debug.screenset/set :debug/screenset-scene %)
                                       (partial + k/Number0))))
   (->> (range 1 10)
        (map (shortcut->re-frame-event #(vector {:keyCode % :ctrlKey false})
                                       #(vector :debug.screenset/set :debug/screenset-role %)
                                       (partial + k/Number0))))))

(defn default-shortcuts []
  (dispatch-sync [::rp/add-keyboard-event-listener "keydown"])
  (dispatch [::rp/set-keydown-rules
             {:event-keys           (keydata)
              :clear-keys           [[{:keyCode k/ESC}]
                                     [{:keyCode k/G
                                       :metaKey true}]]
              :prevent-default-keys [{:keyCode k/RETURN
                                      :ctrlKey true}
                                     {:keyCode k/G
                                      :ctrlKey true}]
              :always-listen-keys   []}]))
