(ns website.toolbar
  (:require [re-frame.core :as rf]
            [website.button :as but :refer [button]]))

(rf/reg-sub ::auto-eval? (fn [db [_ id]] (prn "ID " id) (get-in db [id :auto-eval?] false)))
(rf/reg-event-db ::set-auto-eval (fn [db [_ id state]] (prn "ID2 " id state) (assoc-in db [id :auto-eval?] state)))
(rf/reg-event-db ::toggle-auto-eval (fn [db [_ id]] (prn "ID3 " db) (update-in db [id :auto-eval?] not)))

(rf/reg-sub ::auto-eval2? (fn [db [_ id]] (get-in db [id :auto-eval2?] false)))
(rf/reg-event-db ::toggle-auto-eval2 (fn [db [_ id]] (update-in db [id :auto-eval2?] not)))

(rf/reg-event-db ::clear-code-buffer (fn [db _] (update-in db [:code-buffer] nil)))

(rf/reg-sub ::varighet
            (fn [db [_ id]] (get-in db [id ::varighet] false)))
(rf/reg-event-db ::varighet (fn [db [_ id]] (update-in db [id ::varighet] not)))

(rf/reg-sub ::ln? (fn [db [_ id]] (get-in db [id :ln?] false)))
(rf/reg-event-db ::toggle-ln (fn [db [_ id]] (update-in db [id :ln?] not)))

(rf/reg-sub ::theme (fn [db [_ id]] (get-in db [id :themex] 0)))
(rf/reg-event-db ::next-theme (fn [db [_ id]]
                                (update-in db [id :themex] #(if (< % 1) (inc %) 0))))

;;;

(rf/reg-sub ::html-parsed (fn [db [_ id]] (get-in db [id :html-parsed])))
(rf/reg-event-db ::set-html-parsed
                 (fn [db [_ id v]]
                   (prn ::set-html-parsed id )
                   (assoc-in db [id :html-parsed] v)))

;;;

(rf/reg-sub ::preview-toggle (fn [db [_ id]] (get-in db [id :preview-toggle] false)))
(rf/reg-event-db ::toggle-preview
                 (fn [db [_ id ]]
                   (update-in db [id :preview-toggle] not)))


;;;


(defn markdown-controls [id]
  [:div.p-px.bg-white {:style {:display               :grid
                               :grid-gap              :2px
                               :grid-template-columns "repeat(auto-fill, minmax(88px,1fr))"
                               :grid-auto-rows    "32px"}}

   (button {:caption  "Mørkt/Lyst"
            :id       id
            :click-fn (fn [] (rf/dispatch [::next-theme id]))
            :style (fn [] :hollow)})

   (button {:caption  "Linje-nr"
            :id       id
            :click-fn (fn [] (rf/dispatch [::toggle-ln id]))
            :style    (fn [] (if @(rf/subscribe [::ln? id]) :toggle-on :hollow))})

   (button {:caption  "Varighet"
            :id       id
            :click-fn ::varighet
            :style    (fn [] (if @(rf/subscribe [::varighet id]) :toggle-on :hollow-2))})

   (button {:caption  "Preview"
            :id       id
            :click-fn (fn [] (rf/dispatch [::toggle-preview id]))
            :style    (fn [] (if @(rf/subscribe [::preview-toggle id]) :toggle-on :hollow))})])

(defn controls [id eval!]
  [:div.p-px.bg-white {:style {:display               :grid
                               :grid-gap              :2px
                               :grid-template-columns "repeat(auto-fill, minmax(88px,1fr))"
                               :grid-auto-rows    "32px"}}

   (let [t (nth ["Mørkt" "Lyst"] @(rf/subscribe [::theme id]))]
     (button {:caption  t
              :id       id
              :click-fn (fn [] (rf/dispatch [::next-theme id]))
              :style    (fn [] (condp = t 0 :hollow  :hollow-2))}))

   (button {:caption  "Linje-nr"
            :id       id
            :click-fn (fn [] (rf/dispatch [::toggle-ln id]))
            :style    (fn [] (if @(rf/subscribe [::ln? id]) :toggle-on :hollow))})

   (button {:caption "Evaluate" :id id :style :default :_sub-caption "cmd-enter" :click-fn eval!})
   (button {:caption "Clear" :id id :click-fn (fn [] (rf/dispatch [::clear-code-buffer])) :style :slight})
   (button {:caption "Reset" :id id
            ; todo: insert real values
            :click-fn #(prn %) ;#(.setValue @editor-ref initial-code)
            :style :danger})

   (button {:caption  "Auto-eval"
            :id       id
            :click-fn (fn [] (rf/dispatch [::toggle-auto-eval id]))
            :style    (fn [] (if @(rf/subscribe [::auto-eval? id]) :toggle-on :hollow))
            ;:sub-caption (fn [] (if @(rf/subscribe [::auto-eval? id]) "on" "off"))
            })

   (button {:caption  "Publisert"
            :id       id
            :click-fn ::toggle-auto-eval2
            :style    (fn [] (if @(rf/subscribe [::auto-eval2? id]) :toggle-on :hollow))
            ;:sub-caption (fn [] (if @(rf/subscribe [::auto-eval2? id]) "on" "off"))
            })
   (button {:caption  "Varighet"
            :id       id
            :click-fn ::varighet
            :style    (fn [] (if @(rf/subscribe [::varighet id]) :toggle-on :hollow-2))
            ;:sub-caption (fn [] (if @(rf/subscribe [::auto-eval2? id]) "on" "off"))
            })])
