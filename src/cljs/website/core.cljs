(ns website.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [re-frame.db]
            [clojure.pprint]
            [website.keyboard :refer [default-shortcuts]]
            [website.page-header]
            [website.editors]
            [website.pure.utils :refer [inset-x sprint default-sample-value log-fn href]]
            [reitit.frontend]
            [reitit.frontend.easy :as rfe]
            [reitit.frontend.controllers :as rfc]
            [reitit.coercion :as rc]
            [reitit.coercion.spec :as rss]
            [website.pages.login :as login]
            [website.pages.kurs :as kurs]
            [website.pages.colorsamples :as colorsamples]
            [website.pages.instaparser :as instaparser]
            [playground.demo]
            [playground.core1 :as playground]
            [nv.core :as nv]
            [nv.lists]
            [nv.ui :as ui]
            [yd.core :as y]
            [integrant.core :as ig]
            [nv.components.helpful-text]
            [frontend.mainpage]
            [cljs-time.core :as t]
            [playground.arcs]))

(rf/reg-event-db
  ::initialize-db
  (fn [_ _]
    {:db.pom/tasks          [{:description "1" :start-time (t/now) :length 60}
                             {:description "2" :start-time (t/minus (t/now) (t/minutes 2)) :length 30}
                             #_{:description "3" :start-time (t/minus (t/now) (t/minutes 2)) :length 2}
                             #_{:description "4" :start-time (t/minus (t/now) (t/minutes 4)) :length 5}]
     :vaktliste/toggle-help false
     :debug/screenset-scene 1
     :debug/screenset-role  1
     :debug/questions       false
     :debug/toggle-size     false
     :root                  {:input-field-1 "input-field-value"
                             :input-field-2 "1234"
                             :toggler       true}
     :current-route         nil}))

(rf/reg-event-db ::some-result (fn [db [_ arg]] (assoc db :some-result arg)))

(rf/reg-sub ::some-result (fn [db] (:some-result db :empty)))

(defn render-result []
  (let [data @(rf/subscribe [::some-result])]
    (fn []
      (inset-x [:div.my-1.debug.border
                (cond
                  (map? data) [render-result data]
                  (vector? data) [:div "SOME VECTOR " (count data) (doseq [e data] [:div.bg-blue-500 e])]
                  :else [:div "ELSE " (str data)])
                [:p.text-xl.bg-red-500.text-yellow.p-2.inline "s= " (str data)]]
               [:div#result.h-16.text-white.cm-s-default.mono.inline "result here"]))))

;;;

(defn home-page []
  [:div.pt-64.bg-gray-200
   [:div.p-1.text-gray-600.text-xs.font-thin "Oversikt, forskjellige tall-data her, +arc"]
   #_[nv/render]])

(defn about [match]
  [:div.text-white.mt-24 "ABOUT" (str match)])

(defn items-page2 [match]
  [:div.text-white "HERE ARE SOME ITEMS"])

(defn current-page [])

(defonce match (r/atom nil))

(def routes
  ["/"
   [""
    {:name      ::home
     :view      home-page
     :link-text "Home"
     :controllers
     [{;; Do whatever initialization needed for home page
                  ;; I.e (re-frame/dispatch [::events/load-something-with-ajax])
       :start (fn [& params] (js/console.log "Entering home page"))
                  ;; Teardown can be done here.
       :stop  (fn [& params] (js/console.log "Leaving home page"))}]}]

   ["instaparser"
    {:name      ::instaparser
     :view      (fn [] [:div.mt-20 (instaparser/render)])
     :link-text "instaparser"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering instaparser"))
       :stop  (fn [& params] (js/console.log "Leaving instaparser"))}]}]

   ["Darkside"
    {:name ::darkside
     :view home-page}]

   ["sub-page1"
    {:name      ::sub-page1
     :view      items-page2
     :link-text "Sub page 1"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering sub-page 1"))
       :stop  (fn [& params] (js/console.log "Leaving sub-page 1"))}]}]
   ["about"
    {:name      ::about
     :view      about
     :link-text "about"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering about"))
       :stop  (fn [& params] (js/console.log "Leaving about"))}]}]
   ["login"
    {:name      ::login
     :view      login/render
     :link-text "login"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering login"))
       :stop  (fn [& params] (js/console.log "Leaving login"))}]}]
   ["colorsamples"
    {:name      ::colorsamples
     :view      colorsamples/render
     :link-text "colorsamples"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering colorsamples"))
       :stop  (fn [& params] (js/console.log "Leaving colorsamples"))}]}]
   ["kurs"
    {:name      ::kurs
     :view      kurs/render
     :link-text "kurs"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering kurs"))
       :stop  (fn [& params] (js/console.log "Leaving kurs"))}]}]])

;;; Effects ;;;

;; Triggering navigation from events.

(rf/reg-fx
 ::navigate!
 (fn [route]
   (apply rfe/push-state route)))

(rf/reg-event-fx
 ::navigate
 (fn [db [_ & route]]
    ;; See `navigate` effect in routes.cljs
   {::navigate! route}))

(rf/reg-event-db
 ::navigated
 (fn [db [_ new-match]]
   (let [old-match   (:current-route db)
         controllers (rfc/apply-controllers (:controllers old-match) new-match)]
     (assoc db :current-route (assoc new-match :controllers controllers)))))

;;; Subscriptions ;;;

(rf/reg-sub
 ::current-route
 (fn [db]
   (:current-route db)))

(defn on-navigate [new-match]
  (when new-match
    (rf/dispatch [::navigated new-match])))

(def router
  (reitit.frontend/router
   routes
   {:data {:coercion rss/coercion}}))

(defn init-routes! []
  (js/console.log "initializing routes")
  (rfe/start!
   router
   on-navigate
   {:use-fragment true}))

;;;

(defn root-component [{:keys [router]}]
  (let [current-route @(rf/subscribe [::current-route])]
    (prn "current-route>" (:path current-route))
    [:div.h-screen.w-screen
     [website.page-header/render]
     (when current-route
       [(-> current-route :data :view)])]))

(defn footer []
  [:div.bg-gray-400.text-gray-800.px-2.py-1
   (ui/grid' {:grid-gap              1
              :grid-auto-rows        "auto"
              :grid-template-rows    "1rem"
              :grid-template-columns "repeat(auto-fill,minmax(20%,1fr))"}
             [ui/list-item-sm "< 12" 123]
             [ui/list-item-sm "Alle" 321]
             [ui/list-item-sm "Noen" 123]
             [ui/list-item-sm "Erfarne" 123])])

(rf/reg-sub :debug/screenset-scene (fn [db _] (:debug/screenset-scene db 0)))
(rf/reg-sub :debug/screenset-role (fn [db _] (:debug/screenset-role db 0)))
(rf/reg-event-db :debug.screenset/set (fn [db [_ path value]] (assoc db path value)))

(defn ^:dev/after-load mount-root []
  (prn "mount-root")
  (rf/clear-subscription-cache!)
  (init-routes!)
  (when-let [el (js/document.getElementById "app")]
    (r/render-component [#(playground.demo/render (fn [path value] (rf/dispatch [:debug.screenset/set path value])))
                         {:router router}] el #(prn "FRONTEND MOUNTED"))))

(defn init! []
  (prn "init!")
  (rf/dispatch-sync [::initialize-db])
  (default-shortcuts)
  (mount-root))

;;;

(defn render-home []
  (fn []
    [:div.w-full.bg-red-900
     [:div.text-white.bg-gray-500>div.py-0.my-0
      {:style {:display               :grid
               :grid-template-columns "2fr repeat(auto-fit,minmax(320px,1fr))"
               :grid-template-rows    "auto"
               :grid-auto-rows        "auto"
               :grid-gap              :4px}}
      [:div.flex
       [:div.text-base.h-full.w-full
        [website.editors/editor-markdown {:id            :markdown
                                          :default-value (default-sample-value 1)}]]]
      #_[render-result]]]))

(comment
  (apply str (interpose " " (map #(str "hover" ":" %) (clojure.string/split (name :bg-green-200.text-black) ".")))))
