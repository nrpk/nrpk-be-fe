(ns website.button
  (:require [re-frame.core :as rf]))

(defn button [{:keys [id base-style caption click-fn style]
               :or   {style      :hollow
                      base-style [:rounded-sm :font-bold :text-xs :leading-none :_px-2 ]}}]
  (let [style (if (fn? style) (style) style)
        click-fn (if (keyword? click-fn) (fn [] (rf/dispatch [click-fn id])) click-fn)]
    [:button
     {:class    (flatten [base-style "focus:outline-none focus:select-none"
                          (condp = style
                            :hollow [:bg-clear :border-gray-700 :border-2 :text-black]
                            :hollow-2 [:bg-green-800 :border-green-700 :border-2 :text-white]
                            :slight [:bg-orange-600 :text-white]
                            :danger [:bg-red-600]
                            :toggle-on [:bg-gray-600 :text-gray-100]
                            [:bg-blue-600])])
      :on-click click-fn}
     [:p caption]
     ]))

