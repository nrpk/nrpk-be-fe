(ns website.pure.utils
  (:require [reitit.frontend.easy :as rfe]))

(defn href
  "Return relative url for given route. Url can be used in HTML links."
  ([k]
   (href k nil nil))
  ([k params]
   (href k params nil))
  ([k params query]
   (rfe/href k params query)))

(defn inset-x [& xs]
  [:div.px-2 xs])

(defn sprint [& xs]
  (with-out-str (apply print xs)))

(defn default-sample-value [n]
  (case n
    2 "<iframe width=\"300\" height=\"350\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://www.openstreetmap.org/export/embed.html?bbox=10.845168828964235%2C59.874600424194256%2C10.869405269622803%2C59.881357587063434&amp;layer=mapnik\" style=\"border: 1px solid black\"></iframe><br/><small><a href=\"https://www.openstreetmap.org/#map=17/59.87798/10.85729\">View Larger Map</a></small>"
    1 "# This is some stuffn\n\n![link](http://nrpk.no/wp-content/uploads/2019/10/Dugnad-1000x420.jpg)\n> Stuff **bold**\n\n- [ ] gjør ditt\n- [ ] gjør datt\n\n## Tags\n@topic @leaving-nothing
    \n\n\n\nEnd\n- 1\n- 2\n- 3\n- 4\n- 5\n- 5\n- 6\n- 7
    Ting
    du ma
    huske
    pa

    "
    (sprint "default-sample-value" n "not found")))

(defn linked [s l]
  [:div s [:a.border-blue-400.border-b-2.text-blue-700.font-bold {:href (href :website.core/about)} l]])

(defn log-fn [& params]
  (fn [_]
    (apply js/console.log params)))


(defn dangerous
  ([comp content]
   (dangerous comp nil content))
  ([comp props content]
   [comp (assoc props :dangerouslySetInnerHTML {:__html content})]))
