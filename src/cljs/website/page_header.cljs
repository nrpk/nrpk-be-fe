(ns website.page-header
  (:require [website.button :as but]
            [reitit.frontend.easy :as rfe]))

(defn render []
  [:div.h-12.bg-gray-100.shadow-4.top-0.fixed.w-full.z-50.p-1 ;.opacity-50
   {:style {:display               :grid
            :grid-template-columns "100px 1fr 3fr"
            :grid-auto-rows        "auto"
            :grid-gap              :4px
            :align-content         :center}}
   [:p.ml-2.text-black.font-black "TORV.BE"]
   [:div.m-auto]
   [:div.flex-wrap {:style {:display :flex
                  :grid-template-colums "repeat(3,minmax(100px,120px)"
                  :grid-template-rows "auto"

                  ;:grid-auto-rows        "auto"
                  :grid-gap              :4px
                  }}
    [but/button {:caption "Forside" :style :hollow-2 :click-fn #(rfe/push-state :website.core/home)}]
    [but/button {:caption "Kurs" :style :hollow :click-fn #(rfe/push-state :website.core/kurs)}]
    [but/button {:caption "Colorsample" :style :hollow :click-fn #(rfe/push-state :website.core/colorsamples)}]
    [but/button {:caption "Nøkkelside" :style :slight :click-fn #(rfe/push-state :website.core/darkside)}]
    [but/button {:caption "Login" :style :hollow :click-fn #(rfe/push-state :website.core/login)}]]])
