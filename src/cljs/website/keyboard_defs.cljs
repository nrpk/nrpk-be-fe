(ns website.keyboard-defs)

(def timer-speed 1250)
(def buffer-size 10)

(def UP 38) ;; goog.events.KeyCodes.UP
(def DOWN 40) ;; goog.events.KeyCodes.DOWN
(def LEFT 37) ;; goog.events.KeyCodes.LEFT
(def RIGHT 39) ;; goog.events.KeyCodes.RIGHT

(def RETURN 13)
(def BACKSPACE 8) ;goog.events.KeyCodes.BACKSPACE
(def TAB 9) ;goog.events.KeyCodes.TAB

(def Number0 48)
(def Number1 49)
(def Number2 50)
(def Number3 51)

(def A 65)
(def D 68)
(def S 83)

(def B 66)
(def C 67)
(def E 69)
(def G 71)

(def ESC 27)

(def K 75)
(def L 76)
(def M 77)
(def N 78)
(def O 79)
(def P 80)
(def Q 81)
(def R 82)

(def T 84)
(def U 85)
(def V 86)
(def W 87)
(def X 88)
(def Y 89)
(def Z 90)
(def Questionmark 63)


(def F1 112)
(def F2 113)
(def F3 114)
(def F4 115)
