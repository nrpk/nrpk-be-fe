(ns arcs.dialxsampledata)

(defn value->value-bar [n, max-n]
  {:start 0 :end (- 1 (/ n max-n))})

(defn value-bar->skew [m n]
  (-> m
      (update :start #(+ % n))
      (update :end #(- % n))))

(def arc1 {:zero  0
           :r     [10 50]
           :theta [0 90]
           :class [:text-green-800]
           :ticks [{:drop-first 1
                    :stepsize   (/ 180 2 2 2 2)
                    :style      {:stroke-width 1}
                    :class      [:text-gray-900]}]})

(def arc2 {:zero      0
           :r         [50 78]
           :theta     [0 180]
           :class     [:text-green-600]
           :value-bar [{:value       (value->value-bar 8, 12)
                        :kind        :stroke
                        :outer-inset 0.1
                        :class       [:text-red-700]
                        :style       {:stroke-width 3}}]
           :ticks     [{:drop-first  1
                        :stepsize    4
                        :inner-inset 0.9
                        :outer-inset 0
                        :class       [:text-white]}
                       {:drop-first  1
                        :stepsize    20
                        :inner-inset 0
                        :outer-inset 0.5
                        :class       [:text-white]}
                       {:drop-first  1
                        :stepsize    10
                        :inner-inset 0.3
                        :outer-inset 0.3
                        :style       {:stroke-width 3}
                        :class       [:text-green-500]}
                       {:drop-first  2
                        :stepsize    1
                        :inner-inset 0.8
                        :outer-inset 0.1
                        :style       {:stroke-width 1}
                        :class       [:text-green-900]}]})

(def arc3
  {:adjust-zero      90
   :r         [80 140]
   :theta     [0 190]
   :style {:fill :#222 :stroke :#999}
   ;:class     [:text-red-500]
   :value-bar (vec (concat
                    []
                    (map (fn [e]
                           {:value       (value-bar->skew (value->value-bar (+ 2 (rand-int 6)), 12)
                                                          (/ (rand-int 5) 10))
                            :kind        :stroke
                            :outer-inset (/ e 10)
                            :class       (rand-nth [:text-blue-600
                                                    :text-red-600
                                                    :text-green-600
                                                    :text-orange-400
                                                    :text-yellow-500
                                                    :text-yellow-900])
                            :style       {:stroke-width 3 :stroke-linecap 'butt}})
                         (range 0 10))))

   :ticks     [#_{:stepsize    (* 3 2 2 2)
                  :inner-inset 0.9
                  :class       [:text-gray-500]}

               #_{:stepsize    (* 3 2 2)
                  :inner-inset 0.8
                  :outer-inset 0.1
                  :class       [:text-gray-500]}

               #_{:stepsize    (* 3 2)
                  :inner-inset 0.7
                  :outer-inset 0.2
                  :class       [:text-gray-500]}

               {:stepsize    3
                :outer-inset 0.9
                :style {:stroke-width 0.1}
                :class       [:text-gray-500]}]})

(def arc4
  {:r         [140 160]
   :theta     [0 180]
   :class     [:text-gray-900]
   :value-bar {:value       10
               :kind        :stroke
               :outer-inset 0

               :class       [:text-gray-800]
               :style       {:stroke-width 1}}
   :ticks     [{:pos      :outer
                :stepsize (/ 90 2 2 2 2)
                :class    [:text-gray-800]}]})

(def arc5
  "translucent dial for use in a yearwheel"
  (let [data (map (fn [_] (rand-int 20)) (range (Math/abs (apply - [70 230]))))
        data-fn (fn [idx] idx)]
    {:r            [110 150]
     :theta        [0 300]
     :class        [:text-gray-500 :opacity-25]
     :ticks        [{:inner-inset 0.7
                     :outer-inset 0.2
                     :stepsize    (/ 90 3)
                     :class       [:text-gray-800]}
                    {:inner-inset 0.8
                     :stepsize    (/ 90 2 2)
                     :class       [:text-gray-800]}
                    {:inner-inset 0.9
                     :stepsize    (/ 90 2 2 2)
                     :class       [:text-gray-800]}]
     :value-bar    [{:kind        :stroke
                     :outer-inset 0.1
                     :class       [:text-gray-800]
                     :style       {:stroke-width   1
                                   :stroke-linecap :butt}}
                    {:kind        :stroke
                     :outer-inset 0.2
                     :class       [:text-blue-600]
                     :style       {:stroke-width   1
                                   :stroke-linecap :butt}}
                    {:kind        :stroke
                     :outer-inset 0.9
                     :class       [:text-gray-800]
                     :style       {:stroke-width     1
                                   :stroke-linecap   :butt
                                   :stroke-dasharray "1,4"}}]
     :value-series {:data        data
                    :data-fn     data-fn
                    :inner-inset 0.3
                    :outer-inset 0.1}}))

(random-sample 0.2 (range 100))

(def arc6
  "translucent dial for use in a yearwheel"
  (let [magnitute 14
        data (map (fn [e] (rand-int 10))
                  (range 100))
        data-fn (fn [data] (/ data 10))]
    {:r            [110 150]
     :theta        [0 300]
     :class        [:text-gray-500 :opacity-25]
     :ticks        [{:inner-inset 0.7
                     :outer-inset 0.2
                     :stepsize    (/ 90 3)
                     :class       [:text-gray-800]}
                    {:inner-inset 0.8
                     :stepsize    (/ 90 2 2)
                     :class       [:text-gray-800]}
                    {:inner-inset 0.9
                     :stepsize    (/ 90 2 2 2)
                     :class       [:text-gray-800]}]
     :value-bar    nil #_[{:kind        :stroke
                           :outer-inset 0.1
                           :class       [:text-gray-800]
                           :style       {:stroke-width   1
                                         :stroke-linecap :butt}}
                          {:kind        :stroke
                           :outer-inset 0.2
                           :class       [:text-blue-600]
                           :style       {:stroke-width   1
                                         :stroke-linecap :butt}}
                          {:kind        :stroke
                           :outer-inset 0.9
                           :class       [:text-gray-800]
                           :style       {:stroke-width     1
                                         :stroke-linecap   :butt
                                         :stroke-dasharray "1,4"}}]
     :value-series {:data        data
                    :data-fn     data-fn
                    :inner-inset 0.1
                    :outer-inset 0.1}}))
