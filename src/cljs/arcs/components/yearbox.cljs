(ns arcs.components.yearbox
  (:require [re-frame.core :as rf]
            [arcs.time :as time]
            [re-frame.core :as rf]
            [arcs.dial :as arc]
            [arcs.elements :as elm]))

(defn- rf
  ([] 0)
  ([e x] [(+ (* x e) 2) (+ (* x e) 50)]))

(def month-colors (take 12 (cycle [:text-blue-300
                                   :text-blue-500
                                   :text-indigo-400
                                   :text-green-300
                                   :text-green-500
                                   :text-green-800
                                   :text-orange-600
                                   :text-yellow-700
                                   :text-orange-300
                                   :text-blue-100
                                   :text-blue-600
                                   :text-blue-900])))

(comment
	; the original

  (defn year-box
    ([]
     (year-box {}))
    ([{:keys []}]
     (let [scale 0.8
           e 0]                                                  ;@(rf/subscribe [::timer-3])

       [:div
        [:svg.center {:stroke-linecap 'round
                      :width          "100%"
                      :height         "auto"
                      :view-box       "0 0 300 300"}
					;[:rect {:x 0 :y 0 :width 300 :height 300 :fill 'none :stroke 'blue :stroke-width 4}]
         [:g
          {:transform (str "rotate(" e " 150 150) translate(150,150) scale(" scale ") translate(-150,-150) ")}
          [:circle {:cx 150 :cy 150 :r 150 :fill :#111}]
          (let [datums {:1 []
                        :2 []
                        :3 []
                        :4 []}]

            (for [i (range 12)]
              (let [r1 (* 30 i) r2 (+ r1 30)]
                (arc/fill-arc-range
									 ;(if (even? i) :text-gray-200 :text-gray-600)
                 (nth month-colors i)
                 (+ 110) (+ (* 1) 150) r1 r2))))
					 ; now try to define a time range using week-numbers.
          (let [[from to] (time/day-range [2019 29 4] [2019 30 9])]
            (for [i (range from to 10)]
              (arc/fill-arc-range :text-yellow-500 70 100 i (+ 2 i))))
          (let [sector (partial arc/fill-arc-range :text-gray-200)
                [from to] [40 190]]
            (for [i (range from to 10)]
              [:g
               (sector 60 70 i (+ 2 i))
               (sector 50 60 i (+ 5 i))]))]
         [:text.font-black.text-6xl {:x    "50%" :y "75%"
                                     :fill 'white} (time/week-number (js/Date.))]]]))))

(defn offset [n s]
  (map #(+ % n) s))

(defn adorn [yb]
  (let [state @(rf/subscribe [::toggle-local])]
    [:div.relative.h-full.debug-1
     (condp = state
       false
       [:div
        [:div.absolute.w-full.h-full yb]
        [:div.absolute.bottom-0.right-0 [:button {:on-click #(rf/dispatch [::toggle-local])} "Back"]]]

       true
       [:div.relative.h-full
        [:div.absolute.top-0.right-0 [:h1 "Alnavann"]]
        [:div.absolute.top-0.left-0 [:h2.text-green-700 "1230"]]
        [:div.absolute.top-0.bottom-0.center.flex.w-full yb]

        [:div.absolute.bottom-0.right-0 [:button {:on-click #(rf/dispatch [::toggle-local])} "Zoom"]]

        [:div.absolute.bottom-0.left-0
         [:div.row
          (elm/spaced
           [elm/dummy [:div.p-1.sw-20.h-20.center.flex.font-black.font-sans.text-gray-900 "3992"]]
            ;(elm/dummy [:div.p-1.sw-20.h-20.center.flex.text-xs.font-sans "123"])
           [:div.inline [:h2.m-0.p-0 "1970-2019"]]
           (elm/dummy [:div.p-1.sw-20.h-20.center.flex.text-xs.font-sans "123"]))]]])]))

;;
;;
;;

(rf/reg-sub
 ::timer-3
 (fn [db _]
   (* 1.51 (Math/cos (:timer-value db 0))) #_(rem (:timer-value db 0) 113)))

(rf/reg-event-db
 ::toggle-local
 (fn [db _]
   (update db :local-toggle not)))

(rf/reg-sub
 ::toggle-local
 (fn [db]
   (db :local-toggle false)))
