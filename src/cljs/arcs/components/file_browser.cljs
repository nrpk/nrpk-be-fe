(ns arcs.components.file-browser)

(defn random-filename [e]
  (rand-nth
   ["Sample-filename"
    "Introduction"

    "untitled-13"
    "untitled-23"
    "untitled-33"
    "untitled-43"
    "untitled-53"

    "A very long filename"
    "Some very clever title"
    "No need to use extendsions here"]))

(defn file-content-area-menu []
  [:div (interpose [:span.mx-1] (map #(vector :button.btn.m-0.p-0 %)
                                     '[new files edit]))])

(defn footer-content-area-menu []
  [:div (interpose [:span.mx-1] (map #(vector :button.btn.text-base.m-0.p-0 %)
                                     '[sort filter history]))])

(defn data-item [e]
  (condp = 3
    1 [:div.row.items-center.bg-blue-400.px-2.rounded-sm
       [:div.text-white.font-bold "1" [:span.px-4 "Sample filename here.doc"]]
       [:button.btn-1.border-blue-900.bg-blue-200.font-bold "P"]]
    2 [:div.bg-gray-900.opacity-25.text-white.py-1 e]
    3 [:div.text-blue-900.py-1.truncate.clickable-text.cursor-pointer (random-filename e)]))

(letfn [(a-func [{:keys [title show-all data]}]
          [:div
           [:h2 title [:a.font-normal.mx-2.clickable-text.color-black show-all]]
           (if (empty? data)
             [:div.h-12
              [:div.flex.center "No data"]]
             [:div.w-full.square-grid
              (for [e data]
                (data-item e))])])

        (no-data-possible [])]
  (defn file-content-area' []
    [:div.my-4.text-xs
     [:div.mx-2.mb-4 (file-content-area-menu)]
     [:div.mt-1.text-sm.text-blue-900.text-bold.bg-blue-500.p-1.rounded.mr-2.xmb-2.p-2
      [:div

       (a-func {:title "I dag" :show-all "vis alle" :data nil})

       [:div
        [:h2 "Sist sett på"]
        [:div.w-full.square-grid
         (let [data (range 12)]
           (if (empty? data)
             [:div.row.bg-red-100.center.w-10
              [:div.h-12.items-center.flex "No data"]]
             (for [e data]
               (data-item e))))]]

       [:div
        [:h2 "Nylige operasjoner" [:a.font-thin.mx-2.clickable-text "sorter"]]
        [:div.w-full.square-grid
         (let [data (range 12)]
           (if (empty? data)
             [:div.row.bg-red-100.center.w-10
              [:div.h-12.items-center.flex "No data"]]
             (for [e data]
               (data-item e))))]]]]]))

(defn file-content-area []
  (file-content-area'))
