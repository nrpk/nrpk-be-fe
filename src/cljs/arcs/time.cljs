(ns arcs.time
  (:require [cljs-time.core :as t]))

(defn dummy [])

(defn- week-number
  "Week number according to the ISO-8601 standard, weeks starting on
	Monday. The first week of the year is the week that contains that
	year's first Thursday (='First 4-day week'). The highest week number
	in a year is either 52 or 53."
  [ts]
  (let [year (.getFullYear ts)
        month (.getMonth ts)
        date (.getDate ts)
        day (.getDay ts)
        thursday (js/Date. year month (- (+ date 4) (if (= 0 day) 7 day)))
        year-start (js/Date. year 0 1)]
    (Math/ceil (/ (+ (/ (- (.getTime thursday)
                           (.getTime year-start))
                        (* 1000 60 60 24))
                     1)
                  7))))

(defn day-range [from to]
  [131 250])
