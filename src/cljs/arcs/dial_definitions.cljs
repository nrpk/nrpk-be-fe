(ns arcs.dial-definitions
  (:require [re-frame.core :as rf]
            [app.boat-selector.dial-data :as dial-data :refer [value-start-end]]))

(defn svg
  "sets up viewbox scaling and all the boring centering stuff"
  ([key-id [w h] & content]
   (let [[w2 h2] [(/ w 2) (/ h 2)]
         scale 1.2]
     [:svg {:key                 key-id
            :stroke-linecap      'round
            :width               "100%"
            :height              "auto"
            :view-box            (str "0 0 " w " " h)
            :preserveAspectRatio "xMidYMid meet"}
      #_[:g
         {:transform (str "rotate(" 120 "," w2 "," h2 ") translate(" w2 "," h2 ") scale(" scale ") translate(" (- w2) "," (- h2) ") ")}]
      content])))

(defn sample-simple-timeline [draw-fn _]
  (let [timer2 0.3
        timer1 1
        [cx cy] [500 500]]
    [:div.text-sm.font-mono;.w-full
     (when draw-fn
       (svg "sample-simple-timeline" [cx cy]
            (->> [{:class       [:text-orange-400]
                   :outer-inset 0.2
                   :inner-inset 0.2
                   :key-id      "sample-simple-timeline-2"
                   :r           [50 100]
                   :theta       [100 290]
                   :ticks       [dial-data/every-15-minute]
                   :value-bars  [{:style       {; :stroke         'red
                                                :stroke-width   :2%
                                                :stroke-linecap 'butt}
                                  :class       [:text-green-800]
                                  :kind        :stroke
                                  :value       {:start 0 :end (- 1 0.2)}}]}]

                 (map #(assoc % :r (mapv (partial * 1.5) (% :r))))
                 (map (partial draw-fn {:cx (/ cx 2) :cy (/ cy 2) :zero 0})))))]))

(let [pomodore-current-runner
      {:class       [:text-gray-750]
       :insets [0 0]
       :key-id      "current-runner"
       :r           [80 100]
       :theta       [90 100]
       :ticks       [{:key-id      "t1"
                      :stepsize    (/ 360 12)
                      :class       [:text-yellow-500]
                      :insets [0.5 0]
                      :style       {:stroke-width 2}}]
       :widgets [{:key-id "w1"
                  :kind :triangle
                  :style {:stroke-width   1
                          :stroke-linecap 'butt
                          :fill 'lightblue
                          :stroke         'white}
                  :position 0.5}]
       :value-bars  [{:key-id      "vb1"
                      :style {:stroke-width   10
                              :stroke-linecap 'butt
                              :stroke 'currentColor
                              :fill 'none}
                      :class [:text-green-500]
                      :kind  :stroke}
                     #_{:style {:stroke-width   :1%
                                :stroke-linecap 'butt
                                :stroke 'red
                                :fill 'none}
                        :class [:text-pink-300]
                        :kind  :stroke
                        :value {:start 1.1 :end -1}}]}]
  (defn construct-currentrunner [{:keys [start length remaining]}]
    (prn "remaining " remaining)
    (-> pomodore-current-runner
        (assoc :theta [start (+ start length)])
        (assoc-in [:value-bars 0 :value] {:start 0 :end (- 1 remaining) :value 0.2}))))

(defn construct-inner-hourly-cfg [{:keys [all-tasks start length']}]
  (let [pomodore-inner-hourly {:class      [:text-gray-750]
                               :insets     [0.1 0.1]
                               :key-id     "inner-hourly"
                               :r          [20 80]
                               :theta      [90 100]
                               :ticks      []
                               :value-bars []}]
    (-> pomodore-inner-hourly
        (assoc :ticks [{:key-id   "inner-hourly-12"
                        :stepsize (/ length' 12)
                        :class    [:text-red-500]
                        :insets   [0.3 0]
                        :style    {:stroke-width 0.25}}
                       {:key-id   "inner-hourly-60"
                        :stepsize (/ length' 60)
                        :class    [:text-gray-700]
                        :insets   [0 0.5]
                        :style    {:stroke-width 0.25}}])
        (assoc :all-tasks all-tasks)
        (assoc :theta [start (+ start length')])
        (assoc :value-bars (mapv (fn [[i [start length]]]
                                   (let [start' (/ start length')
                                         end' (/ (+ start length) length')]
                                     {:key-id (str "vb-" i)
                                      :style  {:stroke-width   10
                                               :stroke-linecap 'butt}
                                      :class  (nth (cycle [:text-green-500 :text-pink-500 :text-red-500 :text-orange-500]) i)
                                      :kind   :stroke
                                      :value  {:start start'
                                               :end   (- 1 end')}}))
                                 (map-indexed vector all-tasks)))
        #_(assoc-in [:value-bars 2 :value] {:start 0 :end remaining}))))

(def pomodore-second
  {:class      [:text-gray-750]
   :insets     [0.1 0.1]
   :key-id     "inner-second"
   :r          [35 61]
   :theta      [100 300]
   :ticks      [{:key-id "ps_1"
                 :stepsize (/ 360 12)
                 :class    [:text-gray-500]
                 :insets   [0.8 0.1]
                 :style    {:stroke-width 1}}]
   :value-bars [{:key-id "vb_1"
                 :style {:stroke-width   1
                         :stroke-linecap 'butt}
                 :class [:text-red-700]
                 :kind  :stroke
                 :value {:start 0.01 :end 0}}

                {:key-id "vb_2"
                 :style {:stroke-width   1
                         :stroke-linecap 'butt}
                 :class [:text-red-300]
                 :kind  :stroke
                 :value {:start 0.7 :end 0.1}}

                {:key-id "vb_3"
                 :style {:stroke-width   1
                         :stroke-linecap 'butt}
                 :class [:text-green-500]
                 :kind  :stroke
                 :value {:start 0 :end (- 1 0.2)}}]})

(defn construct-inner-second-cfg [{:keys [all-tasks start length']}]
  (-> pomodore-second
      (assoc :ticks [{:stepsize 360;(/ length' 60)
                      :class    [:text-orange-700]
                      :insets   [0 0]
                      :style    {:stroke-width 2}}])

      #_(assoc :all-tasks all-tasks)
      (assoc :theta [start (+ start length')])
      #_(assoc :value-bars (mapv (fn [[i [start length]]]
                                   (let [start' (/ start length')
                                         end' (/ (+ start length) length')]
                                     {:style {:stroke-width   10
                                              :stroke-linecap 'butt}
                                      :class (nth (cycle [:text-green-500 :text-pink-500 :text-red-500 :text-orange-500
                                                          #_(if (odd? i) :text-green-500 :text-orange-500)]) i)
                                      :kind  :stroke
                                      :value {:start start'
                                              :end   (- 1 end')}}))
                                 (map-indexed vector all-tasks)))

      #_(assoc-in [:value-bars 2 :value] {:start 0 :end remaining})))

(comment
  (do
    (let [a [[1 1] [3 2] [5 2] [7 2]]
          m 14
          theta [0 200]
          each (apply max theta)
          d (/ each m)]

      (map (fn [[start end]] [(/ start m) (/ end m)])
           a))))

;(defn simple-timer-2 [draw-fn v]
;  (sample-simple-timeline draw-fn v))

;(def simple-timer simple-timer-2)
