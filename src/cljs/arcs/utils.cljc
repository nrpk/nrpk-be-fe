(ns ^{:author "Chris Schreiner"
      :doc "Utils"} arcs.utils)

(defn source-fn
  "Dummy function to return an article.
	It could instead return a callback article generation"
  [{:keys [ingress byline words]}]
  {:ingress    ["In the shouter project, add the Clojure JDBC and PostgreSQL driver dependencies. Note that the version numbers here are current at the time of this writing. Newer version numbers may be available later, but compatibility is not guaranteed."]
   :byline     ["Andrej Varga"]
   :paragraphs ["Rather than running PostgreSQL as a system-level background daemon as some package managers do by default, it’s recommended for development work that you launch postgres yourself to avoid permissions issues and improve visibility."
                "If these executables aren’t found, try adding /usr/lib/postgresql/*/bin to your $PATH.\n\nThen create a local PostgreSQL database for development work:"
                [:h2 "Web bindings with Compojure"]
                [:p "Compojure is a popular library for web development in Clojure, and it is at the core of Shouter.
Compojure is built on top of Ring, a general-purpose web application library similar to Ruby’s Rack. Ring will implement much of the app’s low-level glue, while Compojure will provide a concise syntax with which to define application logic.

Add Compojure to the project.clj file along with a Jetty HTTP server adapter:"]]})

(defn *wide-background
	"returns a list of tailwind-background-class-colors"
	{:test #(do
						(assert (= 2 2))
						(assert (= 1 1)))}
	[r color]
	(map #(str "bg-" color "-" %)  r))

(def wide-background (partial *wide-background (range 100 1000 100)))

(def wide-bg (partial *wide-background (range 100 1000 100)))

(def narrow-bg (partial *wide-background '(300 400 900)))

(defn flip
	"Like partial except you supply everything but the first argument."
	([f b] (fn [a] (f a b)))
	([f b c] (fn [a] (f a b c)))
	([f b c d & more]
	 (fn [a] (apply f a b c d more))))
