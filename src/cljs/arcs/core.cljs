(comment
  (ns arcs.core
    (:require
     [clojure.string :as string]
     [cljs.pprint :refer [pprint]]
     [goog.events :as events]
     [goog.history.EventType :as HistoryEventType]
     [reagent.core :as r :refer [with-let]]
     [re-frame.core :as rf]
     [re-graph.core :as re-graph]
     [reitit.core :as reitit]
     [markdown.core :refer [md->html]]
     ;;
     [arcs.ajax :as ajax]
     [arcs.events :as evt]
     [arcs.components :as components]
     [arcs.comps :as stuff :refer [some-container]])
    (:import goog.History))

 ;(rfr/enable-re-frisk! nil)


  (defn nav-link [uri title page]
    [:a.navbar-item
     {:href  uri
      :class (when (= page @(rf/subscribe [:page])) :is-active)}
     title])

  (def navbar
    (fn []
      (let [expanded? (atom true)]
        [:nav.navbar.is-info>div.container
         [:div.navbar-brand
          [:a.navbar-item {:href "/" :style {:font-weight :bold}} "back_httpkit"]
          [:span.navbar-burger.burger
           {:data-target :nav-menu
            :on-click    #(swap! expanded? not)
            :class       (when @expanded? :is-active)}
           [:span] [:span] [:span]]]
         [:div#nav-menu.navbar-menu
          {:class (when @expanded? :is-active)}
          [:div.navbar-start
           [nav-link "#/" "Home" :home]
           [nav-link "#/about" "About" :about]]]])))

  (defn about-page []
    [:section.section>div.container>div.content
     [:img {:src "/img/warning_clojure.png"}]])

 ;;
 ;;
 ;;
 ;; Views
  (set! *warn-on-infer* false)

  (defn- part-1 [])

  (def home-page-styles
    {:style {:color      'black
             :height     :4rem
             :background 'white
             :display    'block
             :margin     "1rem"
             :border     "1px dotted black"}})

  (def codemirror-opts
    {;:mode          "clojure"
    ;:matchBrackets true
     :lineNumbers  true
     :lineWrapping false
     :theme        "default"
     :mode         ""})

  (defn change-fn [state]
    (fn [cm change] (swap! state assoc :code (.getValue cm))))

  (defn home-pagex []
    (let [state (r/atom {:code "This is asdasda testpp2"})]
      (fn [] [:<>
              [:div home-page-styles
               [components/main {:value           "test"
                                 :on-change       (change-fn state)
                                 :codemirror-opts codemirror-opts}]]

              [:section.section>div.container>div.content
               [:p "Stuff"]
               [:div {:dangerouslySetInnerHTML {:__html (md->html @(rf/subscribe [::evt/docs]))}}]
               (when-let [docs @(rf/subscribe [::evt/docs])]
                 [:<>
                  [:pre {:style {:color "red" :margin "1rem 0"}} (with-out-str (pprint @(rf/subscribe [::on-thing-1])))]
                  [:pre {:style {:color "green"}} (with-out-str (pprint @(rf/subscribe [::on-thing-2])))]
                  [:div {:dangerouslySetInnerHTML {:__html (md->html docs)}}]])]])))

  (defn load-page []
    (when-let [docs @(rf/subscribe [::evt/docs])]
      [:<>
       [:pre {:style {:color "red" :margin "1rem 0"}} (with-out-str (pprint @(rf/subscribe [::on-thing-1])))]
       [:pre {:style {:color "green"}} (with-out-str (pprint @(rf/subscribe [::on-thing-2])))]
       [:div {:dangerouslySetInnerHTML {:__html (md->html docs)}}]]))

  (defn home-page [] [:p.text-white "SOM"])

  (def pages
    {:home  #'home-page
     :about #'about-page})

  (defn page []
    [:div
     [navbar]
     [:p.text-white @(rf/subscribe [:page])]
     [(pages @(rf/subscribe [:page]))]])

 ;; -------------------------
 ;; Routes

  (def router
    (reitit/router
     [["/" :home]
      ["/about" :about]]))

 ;; -------------------------
 ;; History
 ;; must be called after routes have been defined
  (defn hook-browser-navigation! []
    (doto (History.)
      (events/listen
       HistoryEventType/NAVIGATE
       (fn [event]
         (let [uri (or (not-empty (string/replace (. event -token) #"^.*#" "")) "/")]
           (rf/dispatch [:navigate (reitit/match-by-path router uri)]))))
      (.setEnabled true)))

 ;;
 ;;
 ;;

  (defn dispatch-it
    []
    "perform the query, with the response sent to the callback event provided"
    (rf/dispatch [::re-graph/query :tempo "{message}" {:a 123} [:on-thing-1]])
    (rf/dispatch [::re-graph/query :fish "{ hero { name id appears_in home_planet } }" {:ugs 123} [::on-thing-2]]))

  (rf/reg-event-db
   :on-thing-1
   (fn [db [_ {:keys [data errors] :as payload}]]
     (assoc db :gql-result-1 payload)))

  (rf/reg-event-db
   ::on-thing-2
   ; the data that comes into payload is wrong,
   ; correction: now it is corrected, returns native clojure maps
   ; It was `{"data":{"hero":{"name":"Luke"}}}` which was clearly json.
   ; todo(done:chris): fix the server response and return ... ? --Look at ajax, the transit-stuff--
   (fn [db [_ {:keys [data errors] :as payload}]]
     (assoc db :gql-result-2 payload)))

  (rf/reg-sub
   ::on-thing-1
   (fn [db _]
     (let [{:keys [errors data]} (:gql-result-1 db)]
       (if errors
         (-> errors first :message)
         data))))

  (rf/reg-sub
   ::on-thing-2
   (fn [db _]
     (let [{:keys [errors data]} (:gql-result-2 db)]
       (if errors
         (-> errors first :message)
         data))))

 ;; Initialize app
  (defn ^:dev/after-load mount-components []
    (rf/clear-subscription-cache!)
    (r/render [:div.bg-gray-900
              ;;[:p.text-white.p-10 "HEAD"]
              ;;[page]
               [load-page]
               [stuff/main]] (.getElementById js/document "app")))

  (defn init! []
    (rf/dispatch-sync [:navigate (reitit/match-by-name router :home)])

    (ajax/load-interceptors!)
    (rf/dispatch [::evt/fetch-docs "TODO.md"])
    (rf/dispatch-sync [::re-graph/init
                       :tempo
                       {:ws-url   nil
                       ;; todo hardcoded port, but change this
                        :http-url "http://localhost:4000/api/graphql"}])
    (rf/dispatch-sync [::re-graph/init
                       :fish
                       {:ws-url          nil                            ;"ws://localhost:9000/api/graphql-ws"
                        :http-url        "http:///api/graphql"
                        :http-parameters {:headers {"Content-Type" "application/json"}}}])
    (hook-browser-navigation!)
    (mount-components)
    (dispatch-it)))
