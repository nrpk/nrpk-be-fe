(ns ^{:author "Chris Schreiner"
      :doc "Usage and patterns:

	(path {:section \"arne\" :article-id 300})
	(btn-1 xs)
	(mini-svg [:path {:d \"...\"])
	(divider)
	(well [])
"} arcs.elements
  (:require [clojure.test :refer [deftest testing is report run-tests]]
            [re-frame.core :as rf]))

(def ^:private button-default
  [:p-0
   :my-0
   :whitespace-no-wrap
   :border-2])

(defn- btn [class content]
  [:button {:class (flatten (conj button-default class))} content])

(def btn-1 (partial btn [:px-4
                         :text-lg
                         :border-2
                         :font-bold
                         :border-white
                         :text-gray-900
                         :bg-gray-300]))

(def btn-2 (partial btn [:text-xs
                         ;:bg-blue-900
                         ;:border-gray-900
                         :border-none
                         :text-gray-900
                         :font-thin]))

(defn mini-svg [& xs]
  [:svg {:width "50" :height "auto" :viewBox "0 0 10 10" :preserve-aspect-ratio "xMidYMin meet"}
   xs])

(def ^:private divider (mini-svg [:path {:d "M 0 5 L 1 15"
                                         :stroke 'gray
                                         :stroke-width 0.2
                                         :fill 'none}]))

(let [default {:badge-color :bg-indigo-600}]
  (defn path
    ([{:keys [badge-color section article-id] :or {article-id 230}}]
     [:div.flex.items-center

			;"circle"
      [:div.text-green-900
			 ;[:div.inline-block.mt-1.w-10.h-10.rounded-full {:class badge-color}]
       [:svg.shadow {:width "2rem" :height "auto" :viewBox "-1 -1 22 22" :preserve-aspect-ratio "none"}
        [:g {:fill 'red}
         [:rect {:x -1 :y -1 :width 22 :height 22 :fill 'yellow}]
         [:path {:d "M 0 0, L 20 20 M 0 20 L 20 0 V 10 l -3 3 L 4 0" :stroke 'currentColor :stroke-width 2 :fill 'none}]]]]

			;arrow
      [:span.mx-2.whitespace-no-wrap "->"]

			;path
      [:div.flex.items-center.font-thin.text-xs
       (interpose [:span.w-4.h-20 divider] #_[:span.mx-2.text-gray-300.text-4xl "|"]
                  (map #(vector :a {:href ""} %) ["hjem" section [:span.font-bold "artikkel-" article-id]]))]])))

(defn expand-h []
  [:div.w-full.flex-grow])

(defn well [& content]
  [:div.flex.justify-start.bg-blue-300.p-3.rounded.shadow-inset content])

(defn dummy [xs]
  [:div.font-serif.text-xl.text-thin.center.flex [:div.bg-blue-900.w-full.h-full.center.flex xs]])

(defn spaced [& xs]
  (interpose [:div.w-1] (if (seq? xs)
                          (first xs)
                          (if (vector? xs)
                            xs
                            xs))))

(defn button [caption-fn action-id action-fn]
  "Shortcut that creates the event and subscriber from the action-id"
  (rf/reg-event-db action-id (fn [db [e _]] (action-fn db e)))
  (rf/reg-sub action-id (fn [db _] (get db action-id true)))

  (let [v (rf/subscribe [action-id])]
    [:button.btn-2
     {:style {:width :100% :min-width :100px}
      :on-click #(rf/dispatch [action-id])} (caption-fn @v)]))

(letfn [(defevt [id f]
          (rf/reg-event-db id (fn [db [e _]] (f db e)))
          (rf/reg-sub id (fn [db _] (get db id true))))]
  (defn button-tiny
    ([class content]
     (button-tiny (fn [_] (str content))
                  :action/default-error
                  (fn [db evt] db)))
    ([caption-fn action-id action-fn]
     "Shortcut that creates the event and subscriber from the (action-)id"
     (defevt action-id action-fn)
     (let [v (rf/subscribe [action-id])]
       [:button.btn-2
        {:style    {:_width :100% :min-width :50px}
         :on-click #(rf/dispatch [action-id])} (caption-fn @v)]))))

(def btn-0 (button-tiny :text-xs "Tick"))
