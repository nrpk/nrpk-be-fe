(ns back-httpkit.sample
  (:require [reagent.core :as r]))

(defn ^:dev/after-load mount-components []
  (r/render [:div.bg-black.text-white.text-xl.p-2.mt-10 "yoss"]
            (.getElementById js/document "app")))

(defn init! []
  (prn "INIT")
  (mount-components))
