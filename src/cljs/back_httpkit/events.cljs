(ns
  back-httpkit.events
  (:require
   [re-frame.core :as rf]
   [ajax.core :as ajax]))

;;dispatchers

(rf/reg-event-db
 :navigate
 (fn [db [_ route]]
   (assoc db :route route)))

(rf/reg-event-db
 :set-docs
 (fn [db [_ docs]]
   (assoc db :docs docs)))

(rf/reg-event-fx
 ::fetch-docs
 (fn [_ [_ filename]]
   {:http-xhrio {:method          :get
                 :uri             (str "/docs/" filename)
                 :response-format (ajax/raw-response-format)
                 :on-success      [:set-docs]}}))

(rf/reg-event-db
 :common/set-error
 (fn [db [_ error]]
   (assoc db :common/error error)))

;;subscriptions

(rf/reg-sub
 :route
 (fn [db _]
   (-> db :route)))

(rf/reg-sub
 :page
 :<- [:route]
 (fn [route _]
   (-> route :data :name)))

(rf/reg-sub
 ::docs
 (fn [db _]
   (:docs db)))

(rf/reg-sub
 :common/error
 (fn [db _]
   (:common/error db)))

;;todo: find a declarative way to describe events

