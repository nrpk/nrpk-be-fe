(ns back-httpkit.components.fiddle)

(comment
  (comment
    ;(reduce + (range 10))
    ;
    (map #(vector "this " (+ % 1)) (range 15))

    (defn qwe [a] "12")

    (comment
      (do
        (def data-item {:Even 'more-data :data 1232 :misc [:first :last]})
        (merge data-item {:auto 'didact
                          :setting (range 4)}))))

  (comment
    (do

      (require 'proto-repl-charts.charts)
      (require 'proto-repl-charts.graph)

      (let [input-values (range 0.0 6.0 0.6)]

        (proto-repl-charts.charts/line-chart
         "Trigonometry"
         {"sin" (map #(Math/sin %) input-values)
          "cos" (map #(Math/cos %) input-values)}
         {:labels input-values}))

      (proto-repl-charts.charts/bar-chart
       "GDP_By_Year"
       {"2013" [16768 9469 4919 3731]
        "2014" [17418 10380 4616 3859]}
       {:labels ["US" "China" "Japan" "Germany"]})

      (proto-repl-charts.graph/graph
       "A Graph from a map"
       {:nodes [:a :b :c :d :e :f :g]
        :edges [[:d :b] [:b :c] [:d :c] [:e :f] [:f :g]
                [:d :a] [:f :c]]}
       (let [nodes [{:id 0 :label "0" :group 0}
                    {:id 1 :label "1" :group 0}
                    {:id 2 :label "2" :group 0}
                    {:id 3 :label "3" :group 1}
                    {:id 4 :label "4" :group 1}
                    {:id 5 :label "5" :group 1}
                    {:id 6 :label "6" :group 2}
                    {:id 7 :label "7" :group 2}
                    {:id 8 :label "8" :group 2}
                    {:id 9 :label "9" :group 3}
                    {:id 10 :label "10" :group 3}
                    {:id 11 :label "11" :group 3}
                    {:id 12 :label "12" :group 4}
                    {:id 13 :label "13" :group 4}
                    {:id 14 :label "14" :group 4}
                    {:id 15 :label "15" :group 5}
                    {:id 16 :label "16" :group 5}
                    {:id 17 :label "17" :group 5}
                    {:id 18 :label "18" :group 6}
                    {:id 19 :label "19" :group 6}
                    {:id 20 :label "20" :group 6}
                    {:id 21 :label "21" :group 7}
                    {:id 22 :label "22" :group 7}
                    {:id 23 :label "23" :group 7}
                    {:id 24 :label "24" :group 8}
                    {:id 25 :label "25" :group 8}
                    {:id 26 :label "26" :group 8}
                    {:id 27 :label "27" :group 9}
                    {:id 110 :label "Storyline\nhere" :group 2}
                    {:id 28 :label "28" :group 9}
                    {:id 29 :label "29" :group 9}]
             edges [[1 0] [2 0] [4 3] [5 4] [4 0] [7 6] [8 7] [7 0] [10 9] [11 10]
                    [110 4] [13 12] [14 13] [13 0] [16 15] [17 15] [15 10] [19 18]
                    [20 19] [19 4] [22 21] [23 22] [22 13] [25 24] [26 25] [25 7]
                    [28 27] [29 28] [28 0]]])

       (proto-repl-charts.graph/graph
        "Custom Graph"
        {:nodes nodes :edges edges}
             ;; Options
        {:nodes {:shape "dot" :size 30 :font {:size 32 :color "#111111"}
                 :borderWidth 2}
         :edges {:width 2}}))))

  (comment
    (require '[proto-repl-charts.table :as table])
    (proto-repl-charts.table/table
     "Users"
     [{:name "Jane" :age 24 :favorite-color :blue}
      {:name "Matt" :age 28 :favorite-color :rwer}
      {:name "Austin" :age 56 :favorite-color :green}
      {:name "Lisa" :age 32 :favorite-color :green}
      {:name "Peter" :age 32 :favorite-color :green}])))
