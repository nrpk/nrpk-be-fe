(ns back-httpkit.components.dial.spec
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as st]
            [clojure.test.check.generators :as gen]))

(s/def ::stroked (s/keys :req-un [::cx ::cy ::zero ::r-inner ::r-outer ::start ::end ::style ::class]))

(s/def ::arc (s/keys :req-un [::r ::theta]
                     :opt-un [::zero ::ticks ::value-bar]))

(s/def ::value-bar (s/keys :req-un [::kind]))

(s/def ::filled (s/keys :req-un [
																 ::r
																 ::theta
																 ::key-id
																 ]
												:opt-un [::style
																 ::class
																 ]))

(s/def ::lookup (s/map-of keyword? string? :min-count 1))

(s/exercise ::lookup)
