(ns back-httpkit.components.dial.helpers
	(:require [goog.string :as gstring]
						[goog.string.format]
						[re-frame.core :as rf]))

(defn polar-to-cartesian
	([{:keys [zero cx cy]} radius angle]
	 (let [angle-in-rad (* (- (- angle zero) 90) (/ Math/PI 180))]
		 [(+ cx (* radius (Math/cos angle-in-rad)))
			(+ cy (* radius (Math/sin angle-in-rad)))]))
	([zero cx cy radius angle]
	 (let [angle-in-rad (* (- (- angle zero) 90) (/ Math/PI 180))]
		 [(+ cx (* radius (Math/cos angle-in-rad)))
			(+ cy (* radius (Math/sin angle-in-rad)))])))

(defn point-with-numbers [x y & xs]
	(let [l 30
				]
		[:g
		 [:rect {:x x :y y :width l :height 5 :fill 'black :stroke 'red :stroke-width 0.5}]
		 [:text {:x         x
						 :y         (+ y 4)
						 ;;:textLength l
						 ;;:textAdjust :spacingAndGlyphs
						 :fill      'yellow
						 :font-size 4}
			(apply (partial gstring/format (apply str (interpose ", " (repeat (count xs) "%.1f")))) xs)]

		 [:circle {:cx x :cy y :r 1 :fill 'red}]
		 [:line {:style {:visibility :visible} :x1 x :y1 y :x2 -10 :y2 -10 :stroke 'white :stroke-width 0.1 :stroke-dasharray "10,2"}]]))
