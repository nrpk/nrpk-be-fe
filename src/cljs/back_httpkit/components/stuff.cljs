(ns back-httpkit.components.stuff
  (:require
   [clojure.pprint :as p :refer [pprint]]
   [clojure.string :as string]
   [re-frame.core :as rf]
   [back-httpkit.components.time :as time]
   [back-httpkit.components.yearbox :as yb]
   [back-httpkit.components.file-browser :as br]
   [back-httpkit.components.elements :as elm]
   [back-httpkit.components.utils :as utils]
   [back-httpkit.components.dial :as dialer]
   [back-httpkit.components.elements :as elements]))

(defn style [id]
  (cond
    (= id 1) :div.back.text-red-700.font-thin.text-xl.p-3.shadow.leading-none.tracking-widest.border-b-4.border-t-4.border-black.pb-10
    (= id 2) :div.bg-red-100.text-gray-700.p-2.pb-3
    (= id 3) :div.flex-grow.bg-blue-600
    (= id 4) :div.h-screen.w-screen.border-box.bg-white.overflow-scroll.main-grid.p-4
    (= id 5) :p.text-white.bg-gray-900.center.flex.fat.text-xl.sticky.top-0
    (= id 13) :h1.bg-white.block.font-bold.text-xl.sticky.top-10.h-10.shadow
    :else :div))

(defn header [a] [:p.text-red-100.bg-red-400  a])

(defn some-container
  [x]
  [:div.grid.h-screen.flex-col.flex.bg-blue-300.justify-between
   {:style {:color  :white
            :background-color 'black
            :grid-gap "1rem"}}

   ;[:div.flex.h-auto.flex-grow "stuff"]
   [:div.h-64.bg-red-500.flex-grow "one"]
   [:div.h-32 "two"]
   (for [e (range 2)]
     [:div.border-red.border-2.border-solid.h-32.w-full.bg-blue-500.mb-1 "Other " e])

   [(style 1)
    [:p "red"]
    [(style 2)
     [:p (str "Something is going on here " x)]]]])

(defn toolbar-simpler [[position state]]
  "Displays a toolbar in any of the four corners in any direction."

  (let [color :none
        length "64"
        content [:<>
                 (map (fn [e]
                        [:div.gaps.flex.opacity-50
                         [:div.w-16.h-16.center.flex.rounded-full.bg-p.text-white.text-shadow.text-xs e]])
                      [1, 2, 3])]]
    (condp = [position state]
      [0, false] [:div.absolute.top-0.m-2.left-0.h-16.flex
                  {:class (concat [(str "w-" length) color])} content]
      [0, true] [:div.absolute.top-0.m-1.left-0.w-16.flex
                 {:class [(str "h-" length) color :flex-col]} content]

      [1, false] [:div.absolute.top-0.right-0.w-16.flex
                  {:class [(str "h-" length) color :flex-col]} content]
      [1, true] [:div.absolute.top-0.right-0.h-16.flex {:class [(str "w-" length) color :flex-row-reverse]} content]

      [2, false] [:div.absolute.bottom-0.right-0.h-16.flex {:class [(str "w-" length) color :flex-row-reverse]} content]
      [2, true] [:div.absolute.bottom-0.right-0.w-16.flex {:class [(str "h-" length) color :flex-col-reverse]} content]

      [3, true] [:div.absolute.bottom-0.left-0.h-16.flex {:class [(str "w-" length) color]} content]
      [3, false] [:div.absolute.bottom-0.left-0.w-16.flex {:class [(str "h-" length) color :flex-col-reverse]} content]
      nil)))

(defn bulk [data f]
  (map-indexed f data))

;;
;;
;;
;;

(defn mini-article-view [position content]
  [:div.bg-gray-100.overflow-auto.shadow
   {:style position}
   [:p "1"]
   [:p "2"]
   [:p "3"]
   [:p.sticky.top-0.bg-blue-300.text-white.h-8.p-1.opacity-90.z-10  "Stuff"]
   [:p.w-32.bg-black.text-white "Something more under the line and under the header"]
   [:p.center.flex.fat.text-3xl.fg-t content]])

(rf/reg-sub
 ::toolbar-position-value
 (fn [_]
   {:a 'thist}))

(defn std-fn [i e]
  (vector :div {:class e} (str i)))

(defn main-hover-toolbar [{:keys [title]}]
  [:div.bg-red-400.shadow-inner.sticky.top-0.z-50.overflow-hidden.opacity-90.mb-1_._contents-box._border-box
   {:style {:grid-area " 1 / 1 / span 1 / span 3"}}
   [:div.bg-blue-400.w-100.flex._m-3._border-box._content-box
    [:div.row.w-100
     [:div.border-black.border-4.flex.flex-grow.center title]
     [:div.debug-2.flex.center.px-2.bg-green-300.text-xl.font-serif title]
     [:div._w-32.bg-black.text-white.m-3.flex.center.font-black.text-xl "-"]]]])

;; A couple of things I started to notice while working with the base-layout scheme:

;; 1. styles needs to be defined no matter how clever you name them, if you won't, watch out for feature-creep
;; 2. styles need good names, style 23 wont cut it.

(defn base-content [yx & content]
  [:div.border-4.overflow-hidden.bg-gray-200.py-2
   {:style {:grid-column yx}}
   content])

(defn base-untitled [{:keys [header] :or {header "sample"}}]
  [:div.xp-4.sticky.sub-grid {:style {:grid-gap "0px"}}
   ;toolbar
   [:div.bg-teal-400.sticky.top-0.z-50.overflow-hidden.opacity-90
    {:style {:grid-column "1/3"
             :grid-gap "12px 12px"}}
    [:div.row.p-1.items-end.items-stretch.text-xs.h-6
     {:style {:grid-gap "12px"}}
     [:div.flex.flex-grow.font-thin.bg-red-700.text-red-600.px-1.rounded.items-center.mr-1 header]
     [:div.flex.center.self-stretch.px-2.bg-green-300.font-serif.rounded "127"]
     [:div.flex.center.rounded.px-2.font-black.text-xl.bg-gray-900.text-white.opacity-50.mx-1 "q143"]]]

   ;content
   (base-content "1/2" [:p.text-xs "this is a small test on som e longer formulaes whatever cna come first of all matters"])
   (base-content "2/2" [:p.text-xs "Base stuff 2"])
   (base-content "2/4" [:p.text-xs "Base stuff 3"])
   (base-content "1/2" [:p.text-xs "Base stuff 2"])])

(defn paragraph [& content]
  [:div.text-xs content])

(defn logo []
  [:div.debug-2.w-16.h-16.bg-gray-500.float-right.ml-4.border-white.border-4
   [:p.flex.center.capitalize "logo"]])

(defn black-article [{:keys [ingress byline paragraphs]}]
  [:div.text-white.bg-gray-800.p-5.py-10
   [:div.cps-columns-2
    [:h1.font-bold "Black-article header"]
    (if ingress
      [:div.ingress.text-purple-200
       (for [e ingress]
         (if (vector? e)
           e
           [:p e]))])
    (when byline
      (for [e byline]
        (if (vector? e)
          e
          [:p.byline e])))

    (for [e paragraphs]
      (if (vector? e)
        e
        [:p e]))]])

(defn white-article [{:keys [ingress byline paragraphs]}]
  [:div.text-black.bg-gray-100.p-5.py-10
   [:div
    [:h1 "Black-article header"]
    (if ingress
      [:div.cps-columns-2.ingress.text-gray-700
       (for [e ingress]
         (if (vector? e)
           e
           [:p e]))])
    (when byline
      (for [e byline]
        (if (vector? e)
          e
          [:p.byline.text-black e])))

    (for [e paragraphs]
      (if (vector? e)
        e
        [:p e]))]])

(rf/reg-sub
 ::timer
 (fn [db _]
   (* 1.51 (Math/sin (:timer-value db 0)))))

(rf/reg-sub
 ::timer-2
 (fn [db _]
   (rem (:timer-value db 0) 25)))

(rf/reg-sub
 ::timer-4
 (fn [db _]
   (rem (:timer-value db 0) 360)))

(def style-solid-yellow {:stroke 'yellow :stroke-width 8 :_stroke-dasharray "3,10" :fill 'none})

(def style-solid-red {:stroke 'red :stroke-width 4 :_stroke-dasharray "3,10" :fill 'none})

(defn radius [n] (+ 1 (* 9 n)))

(defn section-color [{:keys [section]}]
  (prn section)
  (condp = section
    "mat" :bg-gray-100
    "2" :bg-red-400
    :bg-green-400))

; todo: new name
(defn sticky-header'
  "This should be different to sticky-header"
  [{:keys [class color title article-id section badge-color show-extra debug]
    :or {article-id 123
         section "mat"
         color :bg-teal-300
         show-extra false
         debug false
         badge-color :bg-indigo-600}
    :as all}]

  [:div.antialiased.h-auto.sticky.top-0.w-100.items-center.opacity-90.shadow
   {:class (section-color all)}
   [:div.px-2

    (when debug
      [:pre.mb-1.bg-green-500.text-green-900.text-xs.px-1.py-2.shadow-inset
       (with-out-str (pprint all))])

    [:div.flex.items-center
     (elm/path all)
     (elm/expand-h)
     (elm/spaced
      [elm/btn-2 "1"]
      [elm/btn-2 "2"]
      [elm/btn-2 "A"])]

    (when show-extra [:div.py-10
                      [:div.row.items-center.text-xs
                       [:div.col.w-full
                        [:div.font-thin.text-2xl.-whitespace-no-wrap title " " 3]
                        [:div.-whitespace-no-wrap.text-xs "Skrevet for 2 dager siden"]]
								 ;(elm/expand-h)
                       (elm/spaced
                        [:div.text-blue-900 (elm/mini-svg [:rect {:width 10 :height 5 :fill 'currentColor}])]
                        [:div.text-red-400 (elm/mini-svg [:rect {:width 10 :height 10 :fill 'currentColor}])]
                        [:div.text-orange-400 (elm/mini-svg [:rect {:width 10 :height 10 :fill 'currentColor}])])]])

    (when show-extra (elm/well
                      (elm/spaced
                       [elm/btn-2 "Historikk"]
                       [elm/btn-2 "Vis lignende"]
                       [elm/btn-2 "Gjem bort"])))

    (when false (elm/well (br/file-content-area)))]
   #_[:div.absolute.right-0.bottom-0.m-3 (symbol-circle-hollow false)]])

(defn scrolling-header []
  [:div.flex.snap-x.scroll-x

   [:div.snap-start.bg-black.text-white
    [:div.w-screen.h-full
		 ;(yb/adorn (yb/year-box'))
     [:p "YEARBOX"]]]

   ;page 1


   [:div.snap-start.bg-black.text-white
    [:div.w-screen.h-full
     (elm/dummy "saving scrollstate - how? ->")]]

	 ;page 2
   [:div.snap-center.bg-green-700.w-screen
    [:div.w-64 {:style {:min-width :20%}} (:p "yb/year-box")]
    [:div.w-full ""]
    [:div.text.whitespace-no-wrap.shadow.flex-grow "cmd"]
    [:div.p-2.text-6xl.font-black.shadow.text-blue-500.bg-blue-500.text-shadow.whitespace-no-wrap.items-end "5"]]

   ;page 3
   [:div.snap-start.w-100.bg-black.text-white
    [:div.w-screen.h-full
     (elm/dummy "saving scrollstate - how? ->")]]])

(defn base-layout []
  (let [source (repeatedly #(rand-nth '[this is a small test]))]
    [:div.bg-black
     (scrolling-header)

		 ;(white-article (utils/source-fn {:ingress false, :byline true, :words 100}))
     (sticky-header' {:debug @(rf/subscribe [::toggle-advanced-mode])
                      :section "mat"
                      :badge-color :bg-pink-500
                      :route 'data
                      :title "Some asdasdasd White-article"})

		 ;(sticky-header' {:route 'data})
		 ;(white-article (source-fn {:ingress true, :byline true, :words 200}))

     [:div
			;(sticky-header' nil)
      (black-article (utils/source-fn {:ingress true, :byline true, :words 200}))]

     (base-untitled {:header "base-untitled"})]))

(defn- symbol-circle-hollow [mode]
  [:svg {:width "25px" :height 'auto :viewBox "0 0 1 1"}
   [:circle {:cx 0.5 :cy 0.5 :r 0.4 :stroke 'currentColor :stroke-width 0.1
             :fill (if mode 'currentColor 'none)}]])

(defn toggle-advanced-mode "dfn dispatch function"
  [mode dfn]
  [:button.px-2.bg-gray-400.text-gray-800.rounded-t-lg
   {:onClick dfn}
   (symbol-circle-hollow mode)])

(rf/reg-event-db
 ::toggle-advanced-mode
 (fn [db [_ x]]
    ;(print ">>>" db x)
   (update db :advanced-mode not)))

(rf/reg-sub
 ::toggle-advanced-mode
 (fn [db _]
   (:advanced-mode db false)))

(defn floating-footer' [advanced-mode]
  [:div.flex.justify-start.fixed.z-50.text-gray-500.bottom-0.cps-shadow.z-50.bg-gray-900.rounded-t-lg.bg-green-500
   (toggle-advanced-mode advanced-mode #(rf/dispatch [::toggle-advanced-mode]))
   (when advanced-mode
     [:<>
      [:button.flex.p-2.xw-100.rounded-none.bg-blue-500.text-white "Bli medlem"]
      [:button.flex.p-2.xw-100.rounded-none.bg-orange-300.text-black.font-bold "Utstyr"]
      [:button.flex.bg-green-500.rounded-t-lg.text-white.p-2.xw-100 "Log in"]])])

(defn bottom-footer []
  [:div.text-center.h-screen
   [:h2 "Contact"]
   [:p "email"]
   [:p "adr"]
   [:p "map"]
   #_[:div.h-64]])

(defn floating-footer []
  [:div.ml-10 (floating-footer' @(rf/subscribe [::toggle-advanced-mode]))])

(defn- present [content]
  [:div.square-grid_flex {:style {:flex-wrap :no-wrap}}
   (for [e (range 2)]
     [:div {:style {:width :100% :min-width :50px}} content])])

(defn- present' [content]
  [:div {:style {:width :100%}} [content]])

(defn- scroller []
  [:div.h-128.select-none
   [:div.w-128
    [:p.text-base.p-2.mb-px.text-white "List of things, now back again"]
    [:div.bg-green-300.row
     elements/divider
     (elements/button-tiny (fn [v] (if v "off" "on"))
                           :action/toggle-lables1
                           (fn [db event-id]
                             (update db event-id (fnil not true))))
     (elements/button-tiny (fn [v] (if v "off" "on"))
                           :action/toggle-lables2
                           (fn [db event-id]
                             (update db event-id (fnil not true))))]]
   [:div.scroll-y.h-full (for [e (range 11)]
                           [:div.row.mb-px
                            [:p.text-base.w-16.mr-px (elements/button-tiny :text-sm e)]
                            [:input.input-1.w-full {:placeholder (str "data for " e)}]])]])

(defn main []
  [:div
	 ;question: is this how you realise the definition?
   [:div.row.flex.h-full.overflow-hiddenx
    [scroller]
    [:div.w-full (present' dialer/main)]]
   [:div.flex

    (elements/button (fn [v] (if v "oasff" "on"))
                     :action/toggle-lables
                     (fn [db event-id]
                       (update db event-id (fnil not true))))

    [:div.w-full]
    (elements/button (fn [v] (str "Makasdasde another " v))
                     :action/make-another
                     (fn [db _] (do (prn "Just got " db) db)))
    [:div.w-full]

    (elements/button (fn [v] (str "Shift" v))
                     :a
                     (fn [_] nil))]])

;; debugging tools
(comment
	; (js/clearInterval xxx)

  (defonce timers (atom nil))

	;;(defonce xxx (js/setInterval #(rf/dispatch [::timer-tick]) 300))

  (rf/reg-event-db
   ::timer-tick
   (fn [{:keys [timer-value] :as db} _]
     (update db :timer-value inc)))

  (when @timers (do
                  (js/clearInterval @timers)
                  (reset! timers nil)))

  (reset! timers (do
                   (js/setInterval #(rf/dispatch [::timer-tick]) 300))))

;(prn "timerss " @timers)
