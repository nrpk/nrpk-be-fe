(ns back-httpkit.components.dial
  (:require
   [back-httpkit.components.dial.sampledata :as sampledata]
   [back-httpkit.components.dial.helpers :refer [point-with-numbers polar-to-cartesian]]
   [back-httpkit.components.dial.spec :as dial-spec]
   [re-frame.core :as rf]
   [cljs.spec.alpha :as s]))

(def default-tick-style {:stroke 'currentColor
                         :stroke-linecap 'butt
                         :stroke-width 1
                         :fill 'none})

(defn- filled
  [context
   {:keys [r-inner r-outer start end style class] :as all}]
  	;{:pre [(s/valid? ::dial-spec/filled all)]}
  (let [f (partial polar-to-cartesian context)
        large-arc-flag (if (<= (- end start) 180) 0 1)

        [x-start-angle y-start-angle] (f r-outer end)
        [x-end-angle y-end-angle] (f r-outer start)

        [x-start-angle' y-start-angle'] (f r-inner end)
        [x-end-angle' y-end-angle'] (f r-inner start)

        s (str "M" x-start-angle "," y-start-angle ","
               "A" r-outer "," r-outer ","
               "0," large-arc-flag ",0,"
               x-end-angle "," y-end-angle

               "L" x-end-angle' "," y-end-angle' ","

               "A" r-inner "," r-inner ","
               "0," large-arc-flag ",1,"
               x-start-angle' "," y-start-angle' " Z")]
    [:g {:class class}
     [:path (merge {:d    s
                    :fill 'currentColor}
                   style)]]))

(defn- stroked
  [context
   {:keys [r-inner r-outer start end style class] :or {style {} class []} :as all}]
  (let [f (partial polar-to-cartesian context)
        large-arc-flag (if (<= (- end start) 180) 0 1)
        [x-start-angle y-start-angle] (f r-outer end)
        [x-end-angle y-end-angle] (f r-outer start)
        s (str "M" x-start-angle "," y-start-angle ","
               "A" r-outer "," r-outer ","
               "0," large-arc-flag ",0,"
               x-end-angle "," y-end-angle)]
    [:g {:class class}
     [:path (merge style {:d s})]]))

#_(defn- draw-tick
  [context
   {:keys [drop-first r-inner r-outer start end  style class inner-inset outer-inset stepsize]
    :or {inner-inset 0 outer-inset 0 stepsize 7 drop-first 0}}]
  (let [polar-func (partial polar-to-cartesian context)]
    (let [f (fn [e]
              (let [[x y] (polar-func r-inner (+ e start))
                    [x-outer y-outer] (polar-func r-outer (+ e start))]
                (str "M" x "," y ","
                     "L" x-outer "," y-outer)))
          r (drop drop-first (range 0 (- end start) stepsize))
          s (apply str (map f r))]
      [:g {:class class}
       [:path (merge style {:d s :stroke 'currentColor})]])))

#_(defn- draw-ticks [context
                   {:keys [ticks r-inner r-outer  sector-start sector-end]}]
  (let [tick-style (merge default-tick-style (get ticks :style {}))
        r-diff (- r-outer r-inner)]
    (when ticks
      [:g
       (for [{:keys [style class stepsize inner-inset outer-inset] :as all} ticks]
         (draw-tick context
                    (merge
                     all
                     {:start    sector-start
                      :end      sector-end
                      :class    class
                      :r-inner  (+ r-inner (* inner-inset r-diff))
                      :r-outer  (- r-outer (* outer-inset r-diff))
                      :stepsize stepsize
                      :style    (merge tick-style style)})))])))

(defn- draw-values [context
                    {:keys [value-bar sector-start sector-end r] :as all}]
  (let [; note: This check makes no sense in its current form, make sure (< 1 (+ start end)), add a spec to fail if not
        kind (value-bar :kind :filled)
        default-value-bar-style {:fill 'currentColor :stroke 'currentColor}
        outer-inset (value-bar :outer-inset 0)
        inner-inset (value-bar :inner-inset 0)
        value (:value value-bar)
        style (:style value-bar)
        [end-value start-value] (if (<= 1 (+ (:start value) (:end value)))
                                  [(:start value) (:end value)]
                                  [(:end value) (:start value)])]
    (condp = kind
      :stroke (stroked context {:class   (:class value-bar)
                                :start   (+ sector-start (* (- sector-end sector-start) start-value))
                                :end     (- sector-end (* (- sector-end sector-start) end-value))
                                :r-inner (+ (apply min r) (* inner-inset (- (apply max r) (apply min r))))
                                :r-outer (- (apply max r) (* outer-inset (- (apply max r) (apply min r))))
																;; note: clean
                                :style   (merge (dissoc default-value-bar-style :fill)
                                                (dissoc style :fill)
                                                {:fill 'none})})
      :fill (filled context {:start   (+ sector-start (* (- sector-end sector-start) start-value))
                             :end     (- sector-end (* (- sector-end sector-start) end-value))
                             :r-inner (+ (apply min r) (* inner-inset (- (apply max r) (apply min r))))
                             :r-outer (- (apply max r) (* outer-inset (- (apply max r) (apply min r))))
                             :style   (merge default-value-bar-style style)})
      nil)))

(defn- draw-series [context
                    {:keys [data r-inner r-outer start end inner-inset outer-inset]
                     :or   {inner-inset 0.1
                            outer-inset 0.1}
                     :as   all}]
  (let [f (partial polar-to-cartesian context)]
    (map-indexed
     (fn [idx e] (let [idx (+ start idx)
                       [x y] (f (+ r-inner (* r-inner inner-inset)) idx)
                       [x-outer y-outer] (f (- r-outer   (* r-outer outer-inset) e) idx)
                       style {}
                       s (str "M" x "," y ","
                              "L" x-outer "," y-outer)]
                   [:g {:class [:text-white]}
                    [:path {:d s :stroke (if (> e 10) 'green 'orange)}]]))
     data)))

(defn draw-arc

  [context
   {:keys [adjust-zero r theta style ticks value-bar  class value-series]
    :or {adjust-zero 0 ticks {} value-bar {} value-series {}}
    :as all}]

  (let [v (rf/subscribe [:action/toggle-lables])
				ctx (update context :zero #(+ % adjust-zero))
        [r-inner r-outer] r
        [sector-start sector-end] (if-not theta [0 359.9] theta)
        f (partial polar-to-cartesian ctx)]
    [:g (filled ctx (merge
                     {:class   class
                      :start   sector-start
                      :end     sector-end
                      :r-inner r-inner
                      :r-outer r-outer
                      :style   style}))

     (when-not (empty? value-bar)
       (let [data {:r            r
                   :sector-start sector-start
                   :sector-end   sector-end}]
         (if (vector? value-bar)
           (do
             (prn (vector? value-bar))
             (for [e value-bar]
               (draw-values ctx (merge data {:value-bar e}))))
           (draw-values ctx (merge data {:value-bar value-bar})))))

     (when-not (empty? ticks)
       (draw-ticks ctx {:ticks        ticks
                        :r-inner 			r-inner
                        :r-outer 			r-outer
                        :sector-start sector-start
                        :sector-end   sector-end}))

     (when-not (empty? value-series)
       (draw-series ctx {:data (:data value-series)
                         :data-fn (:data-fn value-series)
                         :r-inner r-inner
                         :r-outer r-outer
                         :start   sector-start
                         :end     sector-end}))

     (let [[x y] (f r-inner sector-start)
           [x2 y2] (f r-outer sector-start)
           [x3 y3] (f r-outer sector-end)
           [x4 y4] (f r-inner sector-end)]
			 (when @v
				 [:g
        (point-with-numbers x y x y)
        (point-with-numbers x2 y2 x2 y2)
        (point-with-numbers x3 y3 x3 y3)
        (point-with-numbers x4 y4 x4 y4)]))]))

(defn svg
  "sets up viewbox scaling and all the boring centering stuff"
  [[w h] & content]
  (let [[w2 h2] [(/ w 2) (/ h 2)]
        scale 0.9]
    [:svg {:stroke-linecap 'round
           :width          "100%"
           :height         "auto"
           :view-box       (str "0 0 " w " " h)}
     [:g
      {:transform (str "rotate(" 0 "," w2 "," h2 ") translate(" w2 "," h2 ") scale(" scale ") translate(" (- w2) "," (- h2) ") ")}
      content]]))

(def default-svg
  (partial svg [600 600]))

(defn update-vals [m ks & args]
  (reduce #(apply update % %2 args) m ks))

(defn main []
	(let [v @(rf/subscribe [:action/toggle-lables])]
		(default-svg
			(let [context {:cx 300 :cy 300 :zero 0}]
				(->> [sampledata/arc3
							sampledata/arc1
							sampledata/arc2
							(assoc sampledata/arc4 :adjust-zero 30)
							sampledata/arc5]
						 (map (partial draw-arc context)))))))
