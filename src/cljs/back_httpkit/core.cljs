(ns back-httpkit.core
  (:require
   [reagent.core :as r :refer [with-let]]
   [re-frame.core :as rf]
   [goog.events :as events]
   [goog.history.EventType :as HistoryEventType]
   [markdown.core :refer [md->html]]
   [back-httpkit.ajax :as ajax]
   [back-httpkit.events :as evt]
   [reitit.core :as reitit]
   [clojure.string :as string]
   [re-graph.core :as re-graph]
   [cljs.pprint :refer [pprint]]
   [back-httpkit.components.stuff :as stuff :refer [some-container]]
   [app.keyboard :refer [default-shortcuts]]
   [app.db]
   [re-frisk.core :as re]
   [app.boat-selector-fiddle :as bsf])
  (:import goog.History))


(declare mount-components)
(re/enable-re-frisk! {:width 10 :y 100 :x 0})

(defn change-fn [state]
  (fn [cm change] (swap! state assoc :code (.getValue cm))))

(defn load-page []
  (when-let [docs @(rf/subscribe [::evt/docs])]
    [:<>
     [:pre {:style {:color "red" :margin "1rem 0"}} (with-out-str (pprint @(rf/subscribe [::on-thing-1])))]
     [:pre {:style {:color "green"}} (with-out-str (pprint @(rf/subscribe [::on-thing-2])))]
     [:div {:dangerouslySetInnerHTML {:__html (md->html docs)}}]]))

(def router
  (reitit/router
   [["/" :home]
    ["/about" :about]]))

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     HistoryEventType/NAVIGATE
     (fn [event]
       (let [uri (or (not-empty (string/replace ^String (. event -token) #"^.*#" "")) "/")]
         (rf/dispatch [:navigate (reitit/match-by-path router uri)]))))
    (.setEnabled true)))

(defn dispatch-it
  []
  "perform the query, with the response sent to the callback event provided"
	;; two graphql test-calls
  (rf/dispatch [::re-graph/query :tempo "{message}" {:a 123} [::response-to-query-1]])
  (rf/dispatch [::re-graph/query
                :fish
                "{ hero { name id appears_in home_planet } }"
                {:id 123}
                [::response-to-query-2]]))

(rf/reg-event-db
 ::response-to-query-1
 (fn [db [_ {:keys [data errors] :as payload}]]
   (assoc db :gql-result-1 payload)))

(rf/reg-event-db
 ::response-to-query-2
  ; the data that comes into payload is wrong,
  ; correction: now it is corrected, returns native clojure maps
  ; It was `{"data":{"hero":{"name":"Luke"}}}` which was clearly json.
  ; todo(done:chris): fix the server response and return ... ? --Look at ajax, the transit-stuff--
 (fn [db [_ {:keys [data errors] :as payload}]]
   (assoc db :gql-result-2 payload)))

(rf/reg-sub
 ::on-thing-1
 (fn [db _]
   (let [{:keys [errors data]} (:gql-result-1 db)]
     (if errors
       (-> errors first :message)
       data))))

(rf/reg-sub
 ::on-thing-2
 (fn [db _]
   (let [{:keys [errors data]} (:gql-result-2 db)]
     (if errors
       (-> errors first :message)
       data))))

(defn init! []
  (rf/dispatch-sync [:navigate (reitit/match-by-name router :home)])
  (ajax/load-interceptors!)
  (rf/dispatch [::evt/fetch-docs "TODO.md"])
	;; setup two 'channels'
  (rf/dispatch-sync [::re-graph/init
                     :tempo
                     {:ws-url   nil
                      ;; todo hardcoded port, but change this
                      :http-url "http:///api/graphql"}])
  (rf/dispatch-sync [::re-graph/init
                     :fish
                     {:ws-url   nil ;"ws://localhost:9000/api/graphql-ws"
                      :http-url "http:///api/graphql"
                      :http-parameters {:headers {"Content-Type" "application/json"}}}])
  (hook-browser-navigation!)
  (default-shortcuts)
  (mount-components)
  (dispatch-it))

;; Initialize app
(defn ^:dev/after-load mount-components []
  (rf/clear-subscription-cache!)
  (rf/dispatch-sync [:app.core/initialize-db])
  #_(doseq [e bsf/program2]
    (rf/dispatch-sync e))
  (r/render [app.boat-selector.renderer/render']
            (.getElementById js/document "app")))

(prn "OLD CLIENT")
