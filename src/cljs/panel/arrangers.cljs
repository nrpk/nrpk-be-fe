(ns panel.arrangers)

(def palette
  {:std-button
   {:normal {:base "bg-blue-light text-black m-2 rounded p-2 h-24  border-2 border-grey-darkest select-none" ;sflex justify-center items-center
             :top "text-grey"
             :bottom "text-red font-bold"}
    :select {:base "bg-blue text-white"}}


   ;^{:meta "not really invested in this"}


   :man-button
   {:normal {:base "bg-blue-light text-blue-dark m-2 rounded p-2 h-24  xborder-2 xborder-grey-darkest select-none" ;sflex justify-center items-center
             :top "text-grey"
             :bottom "text-red font-bold"}
    :select {:base "bg-blue text-white"}}

   :størrelse

   {:normal {:base "bg-blue-lightest text-green-darker"
             :top "text-grey"
             :bottom "text-red font-bold"}
    :select {:base "bg-blue text-white"}}

   :båttype
   {:normal {:base "bg-grey-darker text-yellow font-bold"}
    :select {:base "bg-yellow text-grey-darker"}}

   :alder
   {:normal
    {:base "bg-orange-lightest text-green-darker"
     :top "text-grey"
     :bottom "text-red font-bold"}
    :select
    {:base "bg-orange text-white"}}

   :kjønn
   {:normal
    {:base "bg-red-lightest text-green-darker"
     :top "text-grey"
     :bottom "text-red font-bold"}
    :select
    {:base "bg-red text-white"}}

   :kode
   {:normal
    {:base "bg-purple-lightest text-green-darker"
     :top "text-grey"
     :bottom "text-red font-bold"}
    :select
    {:base "bg-purple text-white"}}

   :nøkler
   {:normal
    {:base "bg-teal-lightest text-green-darker"
     :top "text-grey"
     :bottom "text-red font-bold"}
    :select
    {:base "bg-teal text-white"}}
   :stabilitiet
   {:normal
    {:base "bg-grey-lightest text-green-darker"
     :top "text-grey"
     :bottom "text-red font-bold"}
    :select
    {:base "bg-grey text-black"}}
   :card
   {:normal
    {:base "bg-red-lightest text-green-darker"
     :top "text-grey"
     :bottom "text-blue font-bold"}
    :select
    {:base "bg-grey text-black"}}
   :exp.palette/card-2
   {:normal {:base ""}
    :select {:base ""}}})

;test
(defn f-- [i s k top bottom]
  (let [selected (if s :select :normal)
        class (fn [x] {:class (get-in palette [k selected x])})]
    ^{:key i}
    [:div.flex.h-16.w-32.justify-center.items-center.text-xl.p-0.rounded.shadow
     (class :base)
     [:div.flex.flex-col.justify-center.h-full
      [:div.text-xl.text-center (class :top) top]
      (when bottom
        [:<>]
        [:div.h-1]
        [:div.text-xs.text-center.font-bold (class :bottom) bottom])]]))

(defn f- [sel? palette-key [top bottom] & {:keys [height palette] :or {height :narrow palette palette}}]
  ""
  ;todo factor out palette to an option
  (let [class-for #({:class (get-in palette [palette-key (if sel? :select :normal) %])})]
    [:div.flex.w-32.justify-center.items-center.text-xl.p-0.rounded.shadow
     (merge (class-for :base) {:class (case height
                                        :narrow :h-12
                                        :h-16)})
     [:div.flex.flex-col.justify-center.h-full
      [:div.text-xl.text-center (class-for :top) top]
      (when bottom)]]))
>       [:<>
         ;todo: Find out if (class-for :bottom) should be the parent?
         [:div.h-1]
         [:div.text-xs.text-center.font-bold (class-for :bottom) bottom]]

(defn f1-narrow [sel? k [top bottom]]
  (let [class-for (fn [x] {:class (get-in palette [k (if sel? :select :normal) x])})]
    [:div.flex.h-12.w-32.justify-center.items-center.shadow.border-r-2.border-grey-darkest.border-b
     (class-for :base)
     [:div.flex.flex-col.justify-center.h-full
      [:div.text-xl.text-center (class-for :top) top]
      (when bottom
        [:<>
         [:div.h-1]
         [:div.text-xs.text-center.font-bold (class-for :bottom) bottom]])]]))

(defn age-view [s [top bottom]]
  "alder"
  (f- s :alder [top bottom]))

(defn f1 [selected? top bottom]
  "båttype"
  (f- selected? :båttype [top]))

(defn gender-view [selected? [top bottom]]
  "kjønn"
  (f- selected? :kjønn [top]))

(defn f4 [selected? top bottom]
  "kode"
  (f- selected? :kode [top]))

(defn f5 [selected? top]
  "nøkler"
  (f- selected? :nøkler [top]))

;;----------------

(comment

  (defrecord Boat
             [base-category ;; [:sup :kayakk :kano :surfski]
              material      ;; [:plast :glassfiber]
              level2        ;; [0 1]
              name          ;; string
              brand         ;; string
              level         ;;
              type
              dimensions
              weight-class]
    Object
    (toString [s] (str (capitalize (get base-category-map base-category :nada)) ", " name)))

  (def boat-map
    {0 (Boat. 1 0 0 "Aquanaut HV" "Valley" 0 2 :57|536 :?)
     1 (Boat. 1 0 0 "Aquanaut LV" "Valley" 0 2 :55|521 :60-70)
     2 (Boat. 1 1 1 "Arrow Nuka" "Zegul" 1 2 :32|505 :50-80)
     3 (Boat. 1 0 0 "Arrow Play" "Zegul" 0 2 :53.5|517 :?)
     4 (Boat. 1 0 0 "Avocet" "Valley" 0 6 :56|488 :?)
     5 (Boat. 1 1 1 "Baffin P1" "Boréal Design" 1 3 :63.5|430 :-75)
     6 (Boat. 1 1 1 "Baffin P2" "Boréal Design" 1 4 :56.5|518 :65-100)
     7 (Boat. 1 1 1 "Baffin P3" "Boréal Design" 1 5 :58.5|536 :90-)
     8 (Boat. 1 0 0 "Islander Bolero" "Islander Kayaks" 0 9 :62|445 :?)
     9 (Boat. 1 0 0 "Brittany Riot" "Riot Kayaks" 0 4 :55|483 :?)
     10 (Boat. 1 0 0 "Cape Horn" "Wilderness Systems" 0 6 :56|442 :?)
     11 (Boat. 1 1 5 "Citius 48" "Citius" 7 15 :48|520 :?)
     12 (Boat. 1 1 2 "Citius 51" "Citius" 4 15 :51|520 :40-110)
     13 (Boat. 1 1 1 "Citius 60" "Citius" 1 14 :60|520 :70-120)
     14 (Boat. 1 1 2 "Citius JR" "Citius" 3 15 :44|480 :?)
     15 (Boat. 1 1 1 "Coastline" "Olsen Boats" 1 14 :60|? :?)
     16 (Boat. 1 1 2 "Coastrunner" "Olsen Boats" 2 15 :55|? :?)
     17 (Boat. 1 0 0 "Delta" "Dagger" 0 9 :?|34' :?)
     18 (Boat. 1 0 0 "Dirigo" "Old Town" 0 10 :?|? :?)
     19 (Boat. 1 1 1 "Duett K2" "Struer" 1 0 :?|? :?)
     20 (Boat. 1 1 2 "Elio Bull" "Elio" 4 15 :51|? :?)
     21 (Boat. 3 1 2 "Epic V10" "Epic" 2 13 :48|610 :?)
     22 (Boat. 3 1 1 "Epic V8" "Epic" 1 13 :56|548 :?)
     23 (Boat. 1 0 0 "Epsilon" "Boréal Design" 0 4 :59|518 :?)
     24 (Boat. 1 0 0 "Esperanto K2" "Boréal Design" 0 0 :?|? :90-)
     25 (Boat. 1 0 0 "Etain 17.5" "Valley" 0 4 :54|531 :65-100)
     26 (Boat. 1 0 0 "Hasle Excursion" "Hasle" 0 5 :57|320 :?)
     27 (Boat. 1 0 0 "PRIJON Kodiak PE" "Prijon Kodiak" 0 5 :59|520 :?)
     28 (Boat. 1 1 1 "Lightning" "Pyranha" 2 7 :53|421 :30-55)
     29 (Boat. 1 0 0 "Loon 160T" "Old Town" 0 9 :?|? :?)
     30 (Boat. 1 0 0 "Nantucket" "Old Town" 0 18 :66|450 :?)
     31 (Boat. 1 0 0 "Necky Zoar" "Necky Kayak" 0 5 :63.5|430 :?)
     32 (Boat. 1 1 2 "Nelo 55" "Nelo" 2 15 :55|520 :-100)
     33 (Boat. 3 1 3 "Nelo 560" "Nelo" 5 13 :45|560 :-120)
     34 (Boat. 1 1 1 "Nelo 60" "Nelo" 1 14 :60|520 :?)
     35 (Boat. 1 0 1 "Nordkapp" "Valley" 1 2 :53|548 :?)      ;; sjekk materialet
     36 (Boat. 1 0 0 "Prijon HV" "Prijon GmbH" 0 2 :58|520 :?)
     37 (Boat. 1 1 1 "Reynard" "Reynard" 2 12 :? :?)
     38 (Boat. 1 0 0 "Scorpio LV" "P&H Sea Kayaks" 0 3 :56|516 :?)
     39 (Boat. 1 0 0 "Solo" "DAG RTM" 0 9 :70|330 :?)
     40 (Boat. 1 0 0 "Tsunami" "Wilderness Systems" 0 6 :54|355 :?)
     41 (Boat. 2 0 0 "Hasle 530PE" "Hasle" 0 17 :?|? :?)
     42 (Boat. 2 0 0 "Mad River Explorer 16" "Mad River" :? 16 :?|16' :?)
     43 (Boat. 2 1 0 "Adirondack" "We-No-Nah" 0 16 :?|16' :?)
     44 (Boat. 2 0 0 "Wilderness" "We-No-Nah" 0 12 :'27|15'4 :?)
     45 (Boat. 2 1 0 "Ally" "West System" :? 16 :?|17' :?)
     46 (Boat. 2 1 0 "Ally" "West System" :? 16 :?|16.5' :?)
     47 (Boat. 2 0 0 "Mad River Reflection 17 RX" "Mad River" 0 16 :?|17' :?) ;; sjekk materialet
     48 (Boat. 0 1 0 "Atlas AST White" "Starboard" 1 8 :12'0|33 :?)
     49 (Boat. 0 1 0 "Technora" "Starboard" 1 8 :10'5|30 :?)
     50 (Boat. 0 1 0 "Drive-Wood" "Starboard" 1 8 :10'5|30 :?)
     51 (Boat. 0 1 0 "Atlas AST Wood" "Starboard" 1 8 :12'6|? :?)
     52 (Boat. 0 0 0 "Oppblåsbar SUP Starboard" "Starboard" 0 8 :12'6|30 :?)
     53 (Boat. 0 0 0 "Oppblåsbar SUP Starboard" "Starboard" 0 8 :9'|30' :?)
     54 (Boat. 0 1 1 "Starboard Sprint" "Starboard" 4 8 :9'4|30'5 :-120) ;; customized
     55 (Boat. 0 1 0 "Starboard Touring" "Starboard" :? 8 :14'30|? :?)
     56 (Boat. 0 0 0 "Astro Junior" "Starboard" 0 8 :10'6|25 :?)
     57 (Boat. 0 0 0 "Astro Tiki" "Starboard" 0 8 :8'|'28 :-50)
     58 (Boat. 0 0 0 "Astro Whopper" "Starboard" 0 8 :10'|0'35 :-120)
     59 (Boat. 2 1 0 "Sandpiper RX" "We-No-Nah" 1 12 :?|? :?)
     60 (Boat. 1 1 1 "Zegul Greenland T" "Zegul" 1 1 :54|540 :?)
     61 (Boat. 1 1 1 "Seabird Qanik" "Seabird Design" 2 1 :52|546 :?)
     62 (Boat. 1 0 0 "Pungo 120" "Wilderness Systems" 0 9 :12'|0'29 :-147)
     63 (Boat. 1 0 0 "Valley Sirona" "Valley" 0 2 :54.5|483 :40-70)
     64 (Boat. 2 0 1 "Covert 10.5 Solo" "Silverbirch" 3 12 :10.5'|? :?)
     65 (Boat. 1 0 0 "Freedom-Sit on Top" "Perception" 0 11 :?|? :?)
     66 (Boat. 2 0 2 "Firefly DL tandem" "Silverbirch" 0 16 :?|? :?)})

  (def root-map
    {100 {:type 3 :acquired :?}
     116 {:type 15 :acquired :?}
     121 {:type 29 :acquired 2002}
     123 {:type 65 :acquired :?}
     140 {:type 29 :acquired 2004}
     141 {:type 29 :acquired 2004}
     142 {:type 30 :acquired 2004}
     143 {:type 30 :acquired 2005}
     144 {:type 30 :acquired 2005}
     145 {:type 29 :acquired 2005}
     146 {:type 29 :acquired 2005}
     147 {:type 15 :acquired 2005}
     148 {:type 15 :acquired 2005}
     154 {:type 28 :acquired 2005}
     155 {:type 28 :acquired 2005}
     156 {:type 29 :acquired 2005}
     157 {:type 29 :acquired 2005}
     158 {:type 29 :acquired 2005}
     159 {:type 29 :acquired 2005}
     164 {:type 15 :acquired 2006}
     165 {:type 15 :acquired 2006}
     166 {:type 15 :acquired 2006}
     169 {:type 17 :acquired 2006}
     170 {:type 17 :acquired 2006}
     171 {:type 10 :acquired 2006}
     176 {:type 39 :acquired 2006}
     178 {:type 17 :acquired 2007}
     179 {:type 17 :acquired 2007}
     180 {:type 17 :acquired 2007}
     181 {:type 17 :acquired 2007}
     182 {:type 39 :acquired 2007}
     184 {:type 26 :acquired 2007}
     185 {:type 16 :acquired 2007}
     186 {:type 16 :acquired 2007}
     189 {:type 62 :acquired 2007}
     190 {:type 40 :acquired 2007}
     191 {:type 40 :acquired 2007}
     192 {:type 40 :acquired 2007}
     193 {:type 40 :acquired 2007}
     194 {:type 40 :acquired 2007}
     195 {:type 40 :acquired 2008}
     196 {:type 40 :acquired 2008}
     197 {:type 40 :acquired 2008}
     198 {:type 40 :acquired 2008}
     208 {:type 41 :acquired :?}
     222 {:type 37 :acquired 1999}
     224 {:type 41 :acquired 2001}
     233 {:type 45 :acquired 2007}
     234 {:type 45 :acquired 2007}
     235 {:type 43 :acquired 2007}
     236 {:type 43 :acquired 2007}
     237 {:type 44 :acquired 2007}
     240 {:type 47 :acquired 2009}
     241 {:type 47 :acquired 2009}
     243 {:type 47 :acquired 2010}
     244 {:type 47 :acquired 2010}
     411 {:type 0 :acquired 2008}
     412 {:type 4 :acquired 2008}
     413 {:type 4 :acquired 2008}
     414 {:type 18 :acquired 2008}
     415 {:type 18 :acquired 2008}
     418 {:type 39 :acquired 2008}
     419 {:type 16 :acquired 2008}
     420 {:type 16 :acquired 2008}
     421 {:type 20 :acquired 2008}
     424 {:type 1 :acquired 2009}
     426 {:type 35 :acquired 2009}
     427 {:type 12 :acquired 2009}
     428 {:type 12 :acquired 2009}
     433 {:type 6 :acquired 2010}
     434 {:type 1 :acquired 2010}
     435 {:type 35 :acquired 2010}
     438 {:type 24 :acquired 2010}
     439 {:type 24 :acquired 2010}
     440 {:type 19 :acquired 2011}
     443 {:type 32 :acquired 2011}
     444 {:type 32 :acquired 2011}
     445 {:type 34 :acquired 2011}
     446 {:type 34 :acquired 2011}
     447 {:type 6 :acquired 2012}
     448 {:type 35 :acquired 2012}
     449 {:type 38 :acquired 2012}
     451 {:type 27 :acquired 2012}
     452 {:type 13 :acquired 2012}
     453 {:type 16 :acquired 2012}
     454 {:type 61 :acquired 2013}
     455 {:type 9 :acquired 2013}
     456 {:type 11 :acquired 2013}
     457 {:type 31 :acquired 2013}
     458 {:type 36 :acquired 2013}
     460 {:type 5 :acquired 2014}
     461 {:type 7 :acquired 2014}
     462 {:type 22 :acquired 2014}
     463 {:type 33 :acquired 2014}
     464 {:type 13 :acquired 2014}
     465 {:type 14 :acquired 2014}
     466 {:type 8 :acquired 2015}
     467 {:type 8 :acquired 2015}
     468 {:type 8 :acquired 2015}
     469 {:type 8 :acquired 2015}
     470 {:type 3 :acquired 2015}
     471 {:type 60 :acquired 2015}
     486 {:type 23 :acquired :?}
     487 {:type 6 :acquired :?}
     488 {:type 25 :acquired :?}
     489 {:type 2 :acquired :?}
     490 {:type 21 :acquired :?}
     494 {:type 63 :acquired 2018}
     600 {:type 48 :acquired 2011}
     601 {:type 48 :acquired 2011}
     602 {:type 48 :acquired 2011}
     603 {:type 49 :acquired 2011}
     604 {:type 50 :acquired 2012}
     605 {:type 51 :acquired :?}
     606 {:type 51 :acquired :?}
     607 {:type 52 :acquired 2013}
     608 {:type 52 :acquired 2013}
     609 {:type 53 :acquired 2013}
     610 {:type 53 :acquired 2013}
     611 {:type 54 :acquired 2013}
     612 {:type 54 :acquired 2014}
     613 {:type 55 :acquired 2014}
     614 {:type 55 :acquired 2014}
     615 {:type 56 :acquired 2014}
     616 {:type 57 :acquired 2015}
     617 {:type 58 :acquired 2015}
     621 {:type 55 :acquired :?}
     622 {:type 55 :acquired :?}
     245 {:type 64 :acquired 2018}
     246 {:type 66 :acquired 2018}}))

(defn brand-name [id]
  (get  ["Seabird Design" "Valley" "Long brand name"] id "Noname brand name"))

(defn product-name [id]
  (get  ["Aquanaut HV" "Valley Sirona" "Aquanaut LV" "Astro Junior" "Pungo 120"] id "Silverbirch"))

(defn type-name [id]
  (get ["Kano" "Sup" "Kajakk" "Robåt" "Surfski"] id "Unknown"))

(defn color-from-type [id]
  (let [color (nth ["red" "blue" "yellow" "green"] id "grey")]
    (str "bg-" color  "-lightest")))

(defn border-from-type [id]
  (let [color (nth ["red" "blue" "yellow" "green"] id "grey")]
    (str "border-" color " border-l-8")))

(defn stability-name [state stability material]
  (let [material-color (nth ["green" "blue"] material "black")]
    [:p.flex.text-xs.rounded-full.justify-center.items-center.w-6.h-6.border-2.font-bold
     {:class (condp #(contains? %2 %1) state
               :repair (str "bg-transparent text-black border-" material-color)
               :busy (str "bg-transparent text-grey border-" material-color)
               (str "bg-" material-color " text-white border-green-light"))} (inc stability)]))

(defn color-from-state [state v]
  (condp #(contains? %2 %1) state
    :selected "bg-black text-white"
    :repair "bg-red-lightest text-black"
    :busy "bg-blue-lightest text-blue-light"
    "bg-grey-lighter text-black"))

(defn p1-old-selectable [state k v]
  ^{:key k}
  [:div.w-full.grid-item
   {:class [(color-from-state state v)]}
   [:div.flex.flex-col.justify-between.h-20.text-xl.p-2.rounded-lg.w-full
    [:div.flex.justify-between
       ;(str "state=" state)
     [:div.flex.font-bold.text-2xl (:id v)]
     [:div.flex [:div (type-name (:type v))]]
     [:div.flex (stability-name state (:stability v) (:material v))]]
    [:div.flex.justify-between
     [:div.flex.text-xs [:div (brand-name (:brand v))]]
       ;[:div.flex.h-full [:div "x"]]
     [:div.flex.text-xs [:div "54/483"]]]
    [:div.flex.justify-between
     [:div.flex.text-xs [:div (product-name (:product v))]]
     [:div.flex.text-xs.text-blue (str (:debug v))]
     [:div.flex.text-xs [:div "50-120kg"]]]]])

(defn p1-old-selectable-larger [state k v]
  (let [style 0]
    [:div.w-full.grid-item; flex.border.border-black.rounded.flex.no-shrink.flex-grow.card-min-width ;.w-full
     {:class [(color-from-state state v)]}
     #_(case style
         0 {:class [(color-from-state state v) #_(color-from-type (:type v))]}
         {:class [(color-from-state state v) (border-from-type (:type v))]})
     [:div.flex.flex-col.justify-between.h-20.text-xl.p-2.rounded-lg.w-full
      [:div.flex.justify-between
       ;(str "state=" state)
       [:div.flex.font-bold.text-2xl (str k)]
       [:div.flex [:div (type-name (:type v))]]
       [:div.flex (stability-name state (:stability v) (:material v))]]
      [:div.flex.justify-between
       [:div.flex.text-xs [:div (brand-name (:brand v))]]
       ;[:div.flex.h-full [:div "x"]]
       [:div.flex.text-xs [:div "54/483"]]]
      [:div.flex.justify-between
       [:div.flex.text-xs [:div (product-name (:product v))]]
       ;[:div.flex.h-full [:div (:stability v)]]
       [:div.flex.text-xs [:div "50-120kg"]]]]]))

(defn p1 [state k v]
  (let [style 1]
    [:p k])); flex.border.border-black.rounded.flex.no-shrink.flex-grow.card-min-width ;.w-full

