(ns panel.tst-renderer
  (:require [panel.subs :as subs]
            [re-frame.core :as rf :refer [subscribe]]
            [clojure.pprint :refer [pprint]]
            [panel.events :as events]
            [panel.boat-card :as boat-card]
            [panel.styling :as styling]
            [panel.logging :as log]
            [panel.pure :as pure]
            [panel.aggregator]))

(rf/reg-event-db
 ::toggle-visibility
 [events/insert-ui-path]
 (fn [db _]
   (update-in db [:visible-token] not)))

(rf/reg-sub
  ::visible-token?
  (fn [db _]
    (subs/ui-path db :visible-token)))

;; show only one item, nth 0
(defn render [r]
  "render the edn-formatted aggregated state"
    ;(let [data-input (subscribe [::subs/record-ds])]
  (fn []
    [:div.flex.flex-col]
    [:pre.font-mono.text-xs.wrap                         ;.break-words.whitespace-normal
     (with-out-str (pprint (:agg/boats r)))]))
;(pprint (get-in r' [ 0 :status] :none)))]]))))

(defn render-errors-0 [r']
  "render the edn-formatted aggregated state"
  [:div.flex.flex-col.overflow-scroll.h-64.flex-no-shrink
   [:pre.font-mono.text-xs
    (map (fn [e] [:p (-> e :error str) (if-let [t (:tail e)] (str " -- " t))])
         (drop 0 r'))]])

(defn render-boats [xs]
  [:div
   [:p "x" xs]
   [:div.flex.flex-wrap (map boat-card/render xs)]])

(defn- event-card [m]
  [:div.w-32.h-10.text-xs.bg-grey.m-1.p-1.rounded.bg-red.text-white.shadow
   [:p (:type m)]
   [:p (:syn m)]])

(defn render-events [xs]
  [:div.text-xs.bg-black.text-white.mb-1
   [:pre.font-mono (panel.pure/pp "Events" xs)]
   [:br]
   [:div.flex.flex-wrap (->> xs
                             :hms
                             (map event-card))]])

(defn- sale-card [m]
  [:div.w-32.h-10.text-xs.m-1.p-1.rounded.bg-teal.shadow
   [:p (:id m)]
   [:p (:value m)]])

(defn render-sales [xs]
  [:div.text-xs.bg-black.text-white.mb-1
   [:pre.font-mono (panel.pure/pp "Sales" (:sale xs))]
   [:br]
   [:div.flex (->> xs
                   :sale
                   (map sale-card))]])

(defn- error-card [m]
  [:div.w-32.h-12.text-xs.m-1.p-1.rounded.bg-purple.shadow.text-white.overflow-hidden
   [:p (first m) (:cmd/unknown m)]
   [:p (second  m)]])

(defn render-errors [xs]
  [:div.text-xs.bg-black.text-white.mb-1
   [:pre.font-mono.bg-black.text-white (panel.pure/pp "feil" xs)]
   [:br]
   [:div.flex.flex-wrap (map error-card xs)]])

(defn- counter-runner [counter]
  [:div.fixed.pin-l.pin-t.mt-10.ml-10.border-black.border-2.items-center.justify-center.flex
   {:class [:text-2xl
            :font-bold
            :font-mono
            :px-4
            :py-1
            :rounded
            :bg-yellow-dark
            :text-blue
            :opacity-50
            :bg-white
            :text-black]}
   counter])

(defn summary-header []
  (let [r (subscribe [::subs/aggregated])]
    (fn []
      [:div.flex.justify-around.text-xs.my-4
       [:div
        [:div "errors " (count (:agg/errors @r))]
        #_[:div.text-xs
           (into [:div]
                 (map (fn [e]
                        [:p.xw-64.p-1.border.border-red.text-red.mb-1.h-10
                         (let [k (vals e)]
                           (case (first (keys e))
                             :unrecognized [:p "Wat? " (:unrecognized e) (-> e :tail :event-id)]
                             [:p.bg-red.text-white "Non " e]))])
                      (take-last 5 (:agg/errors r))))]]

       [:div "count " (count (map vals (:agg/errors @r)))]
       [:div "hms " (count (-> @r :agg/reports :hms))]
       [:div "sales " (count (-> @r :agg/reports :sale))]
       [:p "in use " (count (->> @r
                                 :agg/boats
                                 (map (fn [[k v]] {k (:status v)}))
                                 (filter (fn [e] (= (first (vals e)) :in-use)))
                                 (into {})))]
       [:div "repairs "
        (count (->> @r
                    :agg/boats
                    (map (fn [[k v]] {k (:status v)}))
                    (filter (fn [e] (= (first (vals e)) :in-repair)))
                    (into {})))]])))

(defn test-demo-header []
  (let [counter (subscribe [::subs/agg-counter])
        r (subscribe [::subs/aggregated])]
    (fn []
      [:p "Something here " @counter])))

(declare edn-report just-reports just-errors)

(rf/reg-sub ::db (fn [db _] db))          

(defn main-renderer []
  #_(let [r (subscribe [::subs/aggregated])
        xx (subscribe [::db])
        counter (subscribe [::subs/agg-counter])
        visi (subscribe [::visible-token?])]
    (fn []
      [:div.h-screen.flex.flex-col.bg-black-light.justify-start.text-black
       [:p.text-blue @counter]
       (if @visi [counter-runner (if @counter @counter 0)])
       ;(log/i ">>>" @r)
       [:div.flex
        [oppdrift.cmp/box-center (str (map? @r))]
        [oppdrift.cmp/box-center (str "s " @counter)]]
       [:div {:class styling/some-style-a} [summary-header]]
       [test-demo-header]
       [render-boats (:agg/boats r)]
       [render-events (:agg/report r)]
       [render-sales (:agg/boats r)]
       [render-errors (panel.aggregator/report-errors @r)]
       ;[just-errors r]
       ;[just-reports r]
       ;(dump (map (fn [e] [:p (keys e)]) (-> @xx :data :db/boats)))
       (let [boat-ids (-> @xx :data :db/boats (into {}))]
         [:<>                                          
          [:pre.text-xs.text-blue (pure/pp boat-ids)] 
          [:div (map boat-card/render boat-ids)]])
       [:code.text-xs.font-mono.bg-grey.p-1.m-1
        @xx
        #_(doall (map-indexed
                  (fn [i e] ^{:key i} [:p (-> e key str)])
                  @(subscribe [::subs/db])))]])))

(defn just-errors [r]
  ;; only show the errors
  [:div.m-1.p-0
   [:div.h-32.shadow-lg.rounded.border-black.border-4.flex.flex-no-grow.overflow-auto.bg-green-darker.text-white
    [render-errors-0 (take 20 (:agg/errors @r))]]])

(defn just-reports [r]
  [:div.flex.m-1
   [:div.p-1.mr-1.flex.flex-col.justify-around.bg-red-lighter
    [:div.w-12.h-12.bg-green.flex.justify-center.items-center "R"]
    [:div.w-10.h-10.bg-blue "2"]
    [:div.w-10.h-10.border "3"]
    [:div.w-10.h-10.border "4"]]
   [:div.m-0.p-1.h-64.overflow-auto.flex-no-shrink.rounded.border-black.border-4.flex-grow.bg-green-darker.
    [:div.flex.flex-grow.overflow-auto.text-white
     [:div [:pre (with-out-str (pprint (:agg/reports @r)))]]]]])

(defn edn-report [r]
  [:div.m-1.p-0.flex.flex-col
   [:div.shadow-lg.rounded.border-black.border-4
    [:div.flex.justify-between
     [:p.text-xl.p-2.font-bold.bg-green.text-white.inline-block.mb-1 (count (get-in r [:agg/reports :hms]))]
     [:p.text-xl.p-2.font-bold.bg-green.text-white.inline-block.mb-1 "Pause"]]
    [render r]]])
