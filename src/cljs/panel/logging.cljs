(ns panel.logging
  (:require [cljs.pprint :refer [pprint]]))

(defn gc [s]
    (js/console.groupCollapsed s))

(defn g [s]
    (js/console.group s))

(defn ge []
    ( js/console.groupEnd))

(defn d [& s] (js/console.log s))
(defn i [& s] (js/console.info s))
(defn w [& s] (js/console.warn s))
(defn e [& s] (js/console.error s))

(defn pp [x] (with-out-str (cljs.pprint/pprint x)))
