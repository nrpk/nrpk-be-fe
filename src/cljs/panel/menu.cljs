(ns panel.menu
  (:require [re-frame.core :as rf :refer [reg-event-db reg-sub subscribe dispatch]]
            [reagent.core]
            [clojure.pprint :refer [pprint write]]
            [panel.events]
            [panel.icons :refer [rounded-icon-under-list icons  icon-with-toggle icon]]
            [panel.arrangers :as arr]
            [panel.provide-lending-stat]
            [panel.components :as comps]
            [panel.renderer :as render]
            [panel.db]
            [panel.subs :as subs]
            [panel.data :as data]
            [panel.pure :refer [pp]]
            [oppdrift.core :as odc]))
            ;[exp.macrostuff :refer [funs] :refer-macros [macro-play]]))

(defn flex-row [& xs]
  [:div.flex xs])

(defn panel-header [x]
  [:h1.bg-blue-darker.text-blue-darkest.m-0.px-2.pt-10.leading-loose x])

(let [value :div.h-full.bg-blue.rounded-l-lg.border-4.border-blue-dark.shadow-inner.w-32]
  (defn toolbar-side [tools]
    [value
     [:div [:div.text-white.h-full.px-2.py-1]
      (->> tools)
      (map (partial comps/box (arr/palette :man-button)))]]))

(defn panel
  "a variety of panels"
  ([s]
   (panel s [1 2] nil))
  ([s xs]
   (panel s ["Søjk" 1 2 3] xs))
  ([s cs xs]
   [:div.flex.border-2;.h-full
    [:div.toolbar2 [toolbar-side cs]]
    [:div.flex.flex-col.w-full.border-2
     [:div (panel-header s)]
     [:div.text-white xs]]]))

(defn main-content [& xs]
  [:div.content.bg-purple xs])

; todo: better name or a move to a different/new namespace
(def old->new
  {:circle-plus 'a
   :magnifier 'b
   :in-to-box 'c
   :out-of-box 'd
   :clock 'e
   :note-taker 'f
   :chat-bubble 'g})

;(def render-fns
;  ^{:deprecated "evolved"}
;  (let [boat-datasource @(rf/subscribe [::subs/boats-datasource])
;        record-ds @(rf/subscribe [:record-ds])]
;    {:circle-plus (panel "Heading here"
;                         ["Robåt" "Kajakk"]
;                         [render/card-list render/filter-fn boat-datasource])
;     :magnifier (panel "asd?"
;                       ["this" "that"]
;                       (let [items [render/list-list record-ds]]
;                         (map render/f items)))
;     :in-to-box (panel "list of boats in repair" (comps/header-in-somewhere "asdsada"))
;     :out-of-box (panel "list of boats out" out-of-box-render)
;     :clock (panel "all events"
;                   [:div (map (fn [kv] [:p.m-2.text-white (str kv)])
;                              (map (comp first vals) record-ds))])}))

;;
;;
;;
;;
               
(defn render ^{:deprecated "evolved"} []
  (let [sel-boatcards @(rf/subscribe [::subs/ui-boatfilter-selected])
        sel-panel @(rf/subscribe [::subs/tab-selected])
        is-debugging? @(rf/subscribe [::subs/ui-debugging])]
    [:div
     ;[:div.toolbar [comps/toolbar]]
     [:div.bg-red-darker.p-1
      {:style {:height "100%"}}
      [:div.main-grid.h-full
       [:p.m-4.text-white 1]
       (when is-debugging? (render/debugger))
       (render/render-help)
       (-> sel-panel
           (render/header sel-boatcards))
       (render/footer)
       [main-content
        [:div.h-full
         [:div.flex.border.w-32.h-32 "Content"]]]]]]))

;;
;;
;;
;; higher order components

(defn scroller [] [:div "SCROLLER"])

(defn widget [data dat2]
  (let [amount 3]
    [:div.flex.m-3.w-full
     [:pre.bg-blue.text-white.m-0.font-mono "item"]]))

(defn login-screen [row]
  [:div.h-full.flex-col.bg-blue-lighter.w-64.fixed.pin-t.pin-r.flex.px-4.flex-grow
   [:p.p-1.pb-10.bg-red
    [:h1 "Login screen"]
    [:p "Do you want to register?"]
    [row {:class [:w-full :justify-around :bg-white :px-4 :_bg-red-dark :my-4]}
     (interpose [:span.w-full.bg-white]
                [[:button.bg-red-dark.p-2 "Yes"]
                 [:button.bg-red-dark.p-2 "No"]])]]
   [:div.h-full.overflow-auto
    [:div.p-1.bg-red-lighter (apply str (repeat 20 "this is a long sentence"))]]])


;;
;;
;;
;; todo: here is where things start do diverge, refactor




