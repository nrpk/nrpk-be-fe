(ns panel.menu-alt
  (:require [panel.components :as comps]
            [re-frame.core :as rf]
            [panel.subs :as subs]
            [panel.pure :refer [pp]]
            [panel.slide :as slide]))

;;
;;                     
;;
;; helpers
(defn print-tag [tag id]
  [:div.p-1.bg-red.text-yellow {:key id} "tag=" tag ", id=" id])
(def -next-id
  (atom 1))

;;
;;
;;
;; dsl-attempt
(defn- next-id [] (swap! -next-id inc))
(defn- div [{:keys [class]
             :or {class "bg-red"}
             :as attrs} & xs]
  (let [id (next-id)]
    [:div.my-4.flex.items-center.flex.justify-center.w-full
     {:key id} xs [print-tag 'div id]]))
(defn row [id {:keys [class]
               :or {class "bg-red"}
               :as attrs} & xs]
  [:div.flex.h-auto (merge attrs {:key (next-id)})
   xs
   [:p.text-xs.text-yellow.bg-black.inline.m-1 {:key (next-id)} "id=" id]])
(defn col [{:keys [class]
            :or {class "bg-blue"}
            :as attrs} & xs]
  (let [id (next-id)]
    [:div.flex.flex-col.h-full.overflow-auto (merge attrs {:key id}) xs [print-tag 'col id]]))
(defn pre [id {:keys [class]
               :or {class "bg-blue"}
               :as attrs} & xs]
  ^{:key id} [:pre.w-full.text-xs.font-base.h-full attrs (pp xs)])
(defn center [id {:keys [class]
                  :or {class "bg-purple-light"}
                  :as attrs} & xs]
  [:div.flex.items-center (merge attrs {:key id}) xs])
(defn screen [id xs]
  [:div.w-screen.h-screen
   {:key id :class "antialiased bg-black text-white"}
   xs
   #_[:p.text-xl.text-black.bg-white.inline.m-1.absolute.pin-t.pin-r.opacity-50 "id=" id " count=" (count xs)]])

;;
;;
;;
;;
(defn render []
  (let [text {:class "border-4 border-red w-full pr-10 overflow-auto"}
        focus {:class "flex justify-between flex-grow border-4
                       border-black w-full overflow-auto"}
        clean {:class [:bg-black :text-yellow :m-4_]}
        plain {:class [:bg-grey-light :text-grey :font-extrabold :text-3xl]}
        border-style (fn [^int s] (if (< 3 (mod s 10))
                                    [:border-8 :border-black]
                                    [:border-8 :border-transparent]))
        ;data @(rf/subscribe/ [::subs/boats-datasource])
        ;dat2 @(rf/subscribe [::subs/boats-datasource-with-key])
        ;selected? (rf/subscribe [::subs/on-tick])
        slide-number (rf/subscribe [::slide/slide-number])]
        ;component (rf/subscribe [::subs/component])]
    (fn [] #_[:div "here"]
      ;[screen (reset! -next-id 1)
      [:<>
       [:p.text-black "pop"]
       (when @slide-number [:div
                            [:p "slide=" @slide-number]
                            (get ["A" "B" "C"] @slide-number (str "?> " (str @slide-number)))])
       [:div.text-white.bg-blue.w-32.h-64 (str ">>>" 'component)]]
       ;[comps/debug-in_to_box-render]]
      ;[:p.text-black [col  {} [div {} "here"]]]
      #_[col (next-id) {:class " bg-blue-darker w-1/2_"}
         [div (next-id) [:p "SOME DATA"]]
         [row (next-id) {:class [:h-full]}                ;plain
          [col (next-id) {:class [:flex-no-grow :flex :m-1 :p-1 "w-1/6"]}
           #_[:p.text-red {:key (next-id)} (str "s " @selected?)]
           [:div.w-48.bg-red {:class (border-style @selected?)} [:p "Fronther"]]
           (map (fn [[k v]] [:p {:key k} (str v " " k)]) (zipmap (iterate next-id 0) (range 4)))]
          #_[center (next-id) (border-style (int @selected?))
             [:div
              [:p.mx-10.whitespace-no-wrap.w-64.flex.justify-center {:key (next-id)} (str "INNHOLD " @selected?)]]]
          [row (next-id) [:div.w-48.bg-orange "Tail"]]]
         [row (next-id) {:class [:h-full]} [:p (str "bottom here " (next-id))]]])))