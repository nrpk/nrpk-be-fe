(ns panel.card
  (:require [clojure.pprint]
            [panel.pure :refer [rand-clock pp pp2]]
            [re-frame.core :as rf]
            [panel.subs :as subs]
            [panel.events :as events]
            [panel.menu]
            [exp.smallsteps :as es]
            [panel.renderer :as render]
            [panel.data :as data]
            [panel.event :as pe]
            [panel.slide :refer [gr-1 gr-2]]))

; mark Just for testing purposes, can be safely removed

(comment
    ;; note: protocols are the standard way to decouple dependencies
  (defprotocol ICard
    (ping [t])
    (view [t]))

  (defrecord Card [state]
    ICard
    (ping [this]
      (println "IFN Just updated")
      (update this :state inc))
    (view [this]
      [:p "Pretty view of state " state]))

  (defn new-card [host port]
        ;(println "new-card")
    (map->Card {:host host :port port :extra 12132})))

;;
;;
;;
;; note: start from this event-queue and feed it into the QRSP below

(defn random-event [id]
  (rand-nth [(let [io (rand-nth [:in :out])]
               {:type :repair
                :dir io
                :id id
                :syn (if (= io :in) (rand-nth ["Skade på lakk" "Skade på kjøl"]))
                :time (rand-clock)})
             {:type :reg
              :dir (rand-nth [:in :out])
              :time (rand-clock)
              :id :hms
              :syn (rand-nth ["skade på brygga"
                              "velt i kano"
                              "velt med kajakk"
                              "drukning"
                              "episode med gjester"
                              "nøkkelvakt episode"])}
             {:type :rent
              :dir (rand-nth [:in :out])
              :id id
              :time (rand-clock)}]))

(defn random-events [n r]
  (into [] (map random-event (take n (repeatedly #(rand-int r))))))

(def random-events* (random-events 152 5))

(def event-queue
  (let [f (fn [xs k m]
            (conj xs {k m  #_(into [] rest)}))]
    (->> random-events*
         (reduce-kv f {}))))

(defn is-repair? [x]
  (-> x :type (partial = :repair)))

(defn is-rent? [x]
  (-> x :type (partial = :rent)))

(defn random-queue []
  (let [f (fn [xs k m] (conj xs {k m}))]
    (-> (reduce-kv f {} random-events*))))

(defn filtered-queue [p]
  (filter (fn [[k v]] (= p (:type v))) event-queue))

; the order of the entities are important here
(defn user-view [xs]
  (map (fn [[k {:keys [type dir time id syn]}]] [time type dir id syn]) xs))

;;
;;
;;
;;

(defn debug [c x]
  (println c x)
  x)

(comment
  (do
    (defprotocol QRSP
        ;note: just for the example
      (consume-all [i evs])
      (consume [i ev] "consume an event and change the state, the timestamp is set for the receiving event"))

    (defn- rent-data-format [{:keys [type time event status dir id  syn] :as all}]
      {:dir dir
       :id id
       :time time})

    (defn- reg-data-format [{:keys [type time event status dir id  syn] :as all}]
      {:id id})

    (defn- repair-data-format [{:keys [type time event status dir id  syn] :as all}]
      {:id id
       :time time})

    (defn- aggregate-rent [st {:keys [type time event status dir id  syn] :as all}]
      (case dir
        :in (-> st
                (update :out dissoc id)
                (update :in assoc [id time] id))
        :out (update st :out assoc [id time] id)))

    (defn- aggregate-repair [st {:keys [type time event status dir id  syn] :as all}]
      (update st :repair []))

      ;;
      ;; Sole purpose is to produce a state after processing events

    (comment
      (defrecord Aggregator [st]
        QRSP
        (consume-all [i evs]
          (map (partial consume i) evs))
        (consume [i ev]
          (letfn [(reduce-state-change [st {:keys [type time event status dir id syn] :as all}]
                    (case type
                      :repair (aggregate-repair st all)
                      :reg (update st :reg conj [time syn])
                      :rent (aggregate-rent st all)
                      (assoc st :err :unknown-consumed :all all :type type :st st)))]
            (update i :st #(reduce-state-change st ev)))))

      (defn new-event-aggregator []
        (map->Aggregator {}))

          ;;
          ;;

      (def ea (new-event-aggregator))

          ;(def data (random-queue))

      (comment
        (println ": focus\n")
        (let [source (map val (random-queue))
              ea (new-event-aggregator)]
                  ;(pp "sasd" (consume-all ea source))
                  ;(pp "test" (take 10 source))))
                  ;(reduce (partial consume ea) [] source)))

                  ;(pp (reduce consume ea (first source)))
          (pp "THIS?" (reduce consume ea data)))
              ;(pp (-> (reduce consume ea (map val data)) :st :repair))
              ;(pp (reduce (partial consume ea) (map val data))))


        (let [source (for [{:keys [type dir] :as all} random-events*]
                       all)]
          (count source))))))

(comment (do
           (def boats-out [1 2 4 5])

           (def x
             (loop [st random-events* p true]
               (pp ">" (first st))
               (if p
                 (recur st (if (:type st) false false))
                 st)))

           (print (pp "Cas" x))))

; name breaks conventions, so it's private
(defn- renderAllValues [i e]
  "this is a prototype"
  [:div.flex.border-2.border-black.w-32.h-12.p-1.m-1.h-auto
   {:class (flatten [(condp = (:type e)
                       :rent :bg-blue-light
                       :repair :bg-red-light
                       :reg :bg-yellow-light
                       :bg-white)
                     (condp = (:dir e)
                       :in [:border-t-4 :border-black]
                       :out [:border-b-8 :border-black]
                       [])])}
   [:div.flex.items-start.justify-center.m-1.text-xs.font-mono.flex-col
    ^{:key i}
    [:div i " " (interpose ", " (vals (dissoc e :syn)))
     (when (:syn e) [:span.bg-white.px-1 (:syn e)])]]])

(defn event-card
  ([[k v]]
   (event-card k v))
  ([i e]
   ^{:key (str i e)}
   (renderAllValues i e)))

(defn group [caption color evs]
  [:div.flex-wrap.flex.px-2 {:class [color]}
   [:div.flex.flex-col
    [:p.pt-2.font-mono.text-xs caption]
    [:div.flex.flex-wrap [:<> (map event-card (range) evs)]]]])

(defn group2 [caption color f2 evs]
  [:div.flex-wrap.flex.px-2 {:class [color]}
   [:div.flex.flex-col
    [:p.pt-2.font-mono.text-xs caption]
    [:div.flex.flex-wrap [:<> (map f2 (range) evs)]]]])

(defn f2 [i e]
  ^{:key i} [:p.font-mono.text-xs.pr-1 (:id e)])

#_(defn timers []
    (let [id (js/setInterval
              #(rf/dispatch [:panel.events/make-tick])
              3400)]
      (rf/dispatch [::events/set-main-timer-id id])))

(defn out-of-box-render []
  (let [data (case :exp
               :select (rf/subscribe [::subs/record-ds])
               :exp (rf/subscribe [::subs/boats-datasource-with-key]))]

      ;(map (fn [e] [render/f true [e (boat-base e)]]))
    (fn []
      [gr-1 "large" (map (fn [e] [gr-2 "out-ofs" (interpose "," (keys (val e)))]) @data)])))

(defn some-display-button [s position tag]
  (let [ui-display @(rf/subscribe tag)]
    [:div.fixed.w-32.h-12.rounded.p-2.bg-grey.m-1.text-black.select-none.z-50
     {:on-click #(rf/dispatch [::events/ui-toggle-display])
      :class [position (if ui-display "opacity-1 shadow border-2 border-black bg-red-lighter" :opacity-50)]}
     [:div.flex.justify-center.items-center.h-full
      [:div.text-black.font-extrabold.uppercase.tracking-wide.text-xl
       s]]]))

(defn some-display-button-2 [s position tag]
  [:div.fixed.w-32.h-12.rounded.p-2.bg-grey.m-1.text-black.select-none.z-50

   {:on-click #(rf/dispatch tag)
    :class    ["hover:text-white hover:bg-black" position (if true "opacity-1 shadow border-2 border-black bg-red-lighter" :opacity-50)]}
   [:div.flex.justify-center.items-center.h-full
    [:div.font-extrabold.uppercase.tracking-wide.text-sm

     s]]])

(defn render []
  (let [boats @(rf/subscribe [::subs/boats-datasource-with-key])
        tick @(rf/subscribe [::subs/on-tick])
        record-ds @(rf/subscribe [::subs/record-ds])
        ui-toggle-groovebar @(rf/subscribe [::subs/ui-toggle-groovebar])
        ui-debugging @(rf/subscribe [::subs/ui-debugging])]

    [:div.w-screen.h-screen.flex.flex-col.bg-red-lightest
     (if @(rf/subscribe [:modal/visible]) {:class "bg"})

     ^{:key 0} (some-display-button "Display" "pin-t pin-r" [::subs/ui-toggle-display])
     ^{:key 11} (some-display-button-2 "Open Dialog" "pin-b pin-r" [:modal/visible true])
     ^{:key 10} (some-display-button-2 (if @(rf/subscribe [::subs/simulation-running?]) "Pause" "Resume") "pin-b pin-l" [::events/pause-simulation @(rf/subscribe [::subs/simulation-running?])])
     ^{:key 1} [:p.m-1.font-mono.text-xs (str tick " -- " (:st (es/run' es/poke :history)))]
     ^{:key 2} [:div tick]
     ^{:key 3} [:div (str ui-toggle-groovebar)]
     ^{:key 4} [:div (str ui-debugging)]
     ^{:key 5} [:div "Count " (count record-ds)]
     ^{:key 6} [:div {:class "xw-1/2"} [:div.p-3.bg-black.font-mono
                                        [:div.flex.h-32.bg-green-dark
                                         (map-indexed (fn [i e] (pe/present i e))
                                                      (take 10 record-ds))]]]

     (if @(rf/subscribe [:modal/visible])
       [panel.empty/dialog
        [:div.flex.flex-col.justify-between          ;.h-32
         [:h1.text-center.mb-4 "Heading"]
         [:p.text-center "Heading"]
         [:p.leading-loose.py-10 (map (fn [e] [:span.text-center_ "Long text following " e " "]) (range 20))]
         [:h5 "Heading"]
         [:div.pt-10.flex.justify-between
          [:button (merge panel.empty/button-class {:on-click #(rf/dispatch [:modal/click :ok-pressed])}) "Ok"]
          [:button (merge panel.empty/button-class {:on-click #(rf/dispatch [:modal/click :cancel-pressed])}) "Cancel"]]]])

     ;[out-of-box-render]

     [:div.flex.h-auto
      [:div.flex.flex-col {:class ["w-1/2"]}
       (group "(take 4 random-events*)" :bg-yellow-light (map (fn [{e :a}] {:type (:type e)
                                                                            :time (:time e)
                                                                            :dir (:dir e)}) (take 4 record-ds)))
       (group "filtered-queue :rent" :bg-blue-lighter (take 4 (vals (filtered-queue :rent))))
       (group "available" :bg-green-lighter [{:id 300 :type :test :dir :in} {:id 301 :type :test :dir :in}])
       (group ":repair" :bg-red-lighter (take 14 (vals (filtered-queue :repair))))
       (group ":reg" :bg-yellow-lighter (take 14 (vals (filtered-queue :reg))))]

      [:div.flex.flex-col {:class ["w-1/2"]}
       (group2 "total boats" :bg-green-light
               (fn [i e] ^{:key i}
                 [:p.text-xs.font-mono (str e)])
               [(count boats)])
       (group2 "boat-id's" :bg-purple-light f2 (map #(merge (val %) {:id (key %) :type :rent}) boats))
       (group2 "rest" :bg-blue-dark
               (fn [a e] ^{:key a} [:div.font-mono.text-xs [:p.text-white [:span.text-yellow (:type e)] (str (keys e))]])
               (take 1 random-events*))

       ^{:key 1} [:p "deach"]
       [:p.text-black "render panel.card"]
       [:pre.bg-black.text-white.text-xxs.font-mono.p-1
        (pp2 "Fancy" (pe/present 1 (first record-ds)))]
       [:pre.text-xs.overflow-y-scroll (pp random-events*)]]]]))
