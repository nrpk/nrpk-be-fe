(ns panel.icons)

(defn icons [name]
  (case name
    :cross-out
    [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:line {:x1 "18" :y1 "6" :x2 "6" :y2 "18"}] [:line {:x1 "6" :y1 "6" :x2 "18" :y2 "18"}]]

    :open-checkmark
    [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:polyline {:points "20 6 9 17 4 12"}]]

    :chevron-left
    [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:path {:d "M15 18l-6-6 6-6"}]]

    :delete
    [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:path {:d "M21 4H8l-7 8 7 8h13a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"}] [:line {:x1 "18" :y1 "9" :x2 "12" :y2 "15"}] [:line {:x1 "12" :y1 "9" :x2 "18" :y2 "15"}]]

    :minus
    [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:line {:x1 "5" :y1 "12" :x2 "19" :y2 "12"}]]

    :plus
    [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:line {:x1 "12" :y1 "5" :x2 "12" :y2 "19"}] [:line {:x1 "5" :y1 "12" :x2 "19" :y2 "12"}]]

    :double-chevron-up
    [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:path {:d "M17 11l-5-5-5 5M17 18l-5-5-5 5"}]]
    :question
    [:svg {:xmlns "http://www.w3.org/2000/svg"
           :width "100%"
           :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:circle {:cx "12" :cy "12" :r "10"}] [:path {:d "M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"}] [:line {:x1 "12" :y1 "17" :x2 "12" :y2 "17"}]]

    :sun
    [:svg {:xmlns "http://www.w3.org/2000/svg"
           :width "100%"
           :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:line {:x1 "12" :y1 "2" :x2 "12" :y2 "6"}] [:line {:x1 "12" :y1 "18" :x2 "12" :y2 "22"}] [:line {:x1 "4.93" :y1 "4.93" :x2 "7.76" :y2 "7.76"}] [:line {:x1 "16.24" :y1 "16.24" :x2 "19.07" :y2 "19.07"}] [:line {:x1 "2" :y1 "12" :x2 "6" :y2 "12"}] [:line {:x1 "18" :y1 "12" :x2 "22" :y2 "12"}] [:line {:x1 "4.93" :y1 "19.07" :x2 "7.76" :y2 "16.24"}] [:line {:x1 "16.24" :y1 "7.76" :x2 "19.07" :y2 "4.93"}]]
    :rewind-to-start
    [:svg {:xmlns "http://www.w3.org/2000/svg"
           :width "100%"
           :height "100%"
           :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "2" :stroke-linecap "round" :stroke-linejoin "round"} [:polygon {:points "19 20 9 12 19 4 19 20"}] [:line {:x1 "5" :y1 "19" :x2 "5" :y2 "5"}]]
    :double-chevron-down
    [:svg
     {:xmlns "http://www.w3.org/2000/svg"
      :width "100%"
      :height "100%"
      :viewBox "0 0 24 24"
      :fill "none"
      :stroke "currentColor"
      :stroke-width "2"
      :stroke-linecap "round"
      :stroke-linejoin "round"}
     [:path {:d "M7 13l5 5 5-5M7 6l5 5 5-5"}]]
    :in-to-box
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24",
      :height "100%",
      :width "100%"}
     [:path
      {:d
       "M3 15v4c0 1.1.9 2 2 2h14a2 2 0 0 0 2-2v-4M17 9l-5 5-5-5M12 12.8V2.5"}]]

    :out-of-box
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24",
      :height "100%",
      :width "100%"}
     [:path
      {:d
       "M3 15v4c0 1.1.9 2 2 2h14a2 2 0 0 0 2-2v-4M17 8l-5-5-5 5M12 4.2v10.3"}]]
    :television
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24",
      :height "100%",
      :width "100%"}
     [:rect {:ry "2", :rx "2", :height "15", :width "20", :y "7", :x "2"}]
     [:polyline {:points "17 2 12 7 7 2"}]]

    :chevron-up
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24",
      :height "100%",
      :width "100%"}
     [:path {:d "M17 11l-5-5-5 5M17 18l-5-5-5 5"}]]

    :return-up
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24",
      :height "100%",
      :width "100%"}
     [:path {:d "M9 10l6-6 6 6"}]
     [:path {:d "M4 20h7a4 4 0 0 0 4-4V5"}]]

    :note-taker
    [:svg {:stroke-linejoin "bevel"
           :stroke-linecap "round"
           :stroke-width "2"
           :stroke "currentColor"
           :fill "none"
           :viewBox "0 0 24 24"
           :height "100%",
           :width "100%"}
     [:path {:d "M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"}]
     [:polygon {:points "18 2 22 6 12 16 8 16 8 12 18 2"}]]

    :circle-minus
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24",
      :height "100%",
      :width "100%"}
     [:circle {:r "10", :cy "12", :cx "12"}]
     [:line {:y2 "12", :x2 "16", :y1 "12", :x1 "8"}]]

    :circle-plus
    [:svg {:stroke-linejoin "bevel",
           :stroke-linecap "round",
           :stroke-width "2",
           :stroke "currentColor",
           :fill "none",
           :viewBox "0 0 24 24"
           :height "100%"
           :width "100%"}
     [:circle {:r "10", :cy "12", :cx "12"}]
     [:line {:y2 "16", :x2 "12", :y1 "8", :x1 "12"}]
     [:line {:y2 "12", :x2 "16", :y1 "12", :x1 "8"}]]

    :chevron-r
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24"
      :height "100%",
      :width "100%"}
     [:path {:d "M13 17l5-5-5-5M6 17l5-5-5-5"}]]

    :chevron-l
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :viewBox "0 0 24 24"
      :height "100%",
      :width "100%"}
     [:path {:d "M11 17l-5-5 5-5M18 17l-5-5 5-5"}]]

    :hamburger
    [:svg
     {:stroke-linejoin "arcs",
      :stroke-linecap "square",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :height "100%",
      :width "100%"
      :viewBox "0 0 24 24"}
     [:line {:y2 "12", :x2 "21", :y1 "12", :x1 "3"}]
     [:line {:y2 "6", :x2 "21", :y1 "6", :x1 "3"}]
     [:line {:y2 "18", :x2 "21", :y1 "18", :x1 "3"}]]

    :chat-bubble
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :height "100%",
      :width "100%"
      :viewBox "0 0 24 24"}
     [:path
      {:d
       "M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"}]]

    :magnifier
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :height "100%",
      :width "100%"
      :viewBox "0 0 24 24"}
     [:circle {:r "8", :cy "11", :cx "11"}]
     [:line {:y2 "16.65", :x2 "16.65", :y1 "21", :x1 "21"}]]

    :clock
    [:svg
     {:stroke-linejoin "bevel",
      :stroke-linecap "round",
      :stroke-width "2",
      :stroke "currentColor",
      :fill "none",
      :height "100%"
      :width "100%"
      :viewBox "0 0 24 24"}
     [:circle {:r "10", :cy "12", :cx "12"}]
     [:polyline {:points "12 6 12 12 16 14"}]]

    [:svg {:stroke-linejoin "bevel",
           :stroke-linecap "round",
           :stroke-width "2",
           :stroke "currentColor",
           :fill "none",
           :height "100%",
           :width "100%"
           :viewBox "0 0 24 24"}
     [:circle {:r "10", :cy "12", :cx "12"}]
     [:path {:d "M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"}]
     [:line {:y2 "17", :x2 "12", :y1 "17", :x1 "12"}]]))


(defn icon' [name]
  (let [i (icons name)
        s {:style {:border-color "black"}
           :class ""}]
    [:div.h-full i]))

; fixme deprecated
(defn icon [name]
  (let [i (icons name)
        s {:style {:border-color "black"}
           :class "hover:text-black hover:border-r-2 hover:border-black"}]
    [:div s
     [:div.flex.items-start.h-20.px-4.border
      {:class (if (= name :note-taker)
                "bg-grey-light text-grey-darker border-l-8 border-blue bg-blue-lighter"
                "border-l-8 border-transparent")}
      i]]))

(defn icon-with-toggle [state name]
  (let [i (icons name)]
    [:div.bg-reds
     {:style {:border-color "black"}
      :class "hover:text-black hover:border-r-2 hover:border-black"}
     [:div.flex.items-start.h-20.px-4.border-l-8
      {:class (if state
                "text-black border-blue border-l-8"
                "border-l-8 border-transparent text-grey")}
      i]]))

(defn rounded-icon [name]
  [:div.bg-grey-light.text-black; .rounded-full
   {:class "hover:bg-black hover:text-white hover:opacity-100"}
   [:div.flex.items-end.h-12.py-3 (icons name)]])

(defn rounded-icon-under-list [name]
  (let [s {:style {:border-color "black"}
           :class "hover:text-red"}]
    [:div ;.rounded-full
     s ;{:class "hover:bg-grey hover:opacity-100"}
     [:div.flex.items-end.h-16.py-3 (icons name)]]))
