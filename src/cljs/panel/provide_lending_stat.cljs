(ns panel.provide-lending-stat
  (:require [panel.arrangers :as arr]
            [panel.panel-content :as panel-content]
            [re-frame.core :as rf]
            [panel.components :refer [toolbar system-button]]))

;(comment
;  [:div.flex.flex-wrap
;   (doall (map-indexed
;           (partial r5 arr/age-view #(= % @selected-age) ::panel.panel-content/age-selection)
;           {0 ["Junior" "0-12"]
;            1 ["Ungdom" "13-29"]
;            2 ["Voksen" "30-70"]
;            3 ["Senior" "71-110"]}))
;   (doall (map-indexed
;           (partial r5 arr/gender-view #(= % @selected-gender) ::panel.panel-content/gender-selection)
;           {0 (if child? "Gutt" "Mann")
;            1 (if child? "Jente" "Kvinne")}))
;   (doall (map-indexed
;           (partial r5 arr/f4 #(= % @selected-code) ::panel.panel-content/code-selection)
;           {0 "Trening"
;            1 "Fritid"
;            2 "Tvang"}))
;   (doall (map-indexed
;           (partial r5 arr/f5 #(= % @selected-keys) ::panel.panel-content/keys-selection)
;           {0 "Har med nøkler"}))])

(rf/reg-event-db
 :stat/age-selection
 (fn [db [_ boat-id age-value]]
   (assoc-in db [:stat boat-id :age] age-value)))

(rf/reg-sub
 :stat/age-selection
 (fn [db [_ boat-id]]
   (get-in db [:stat boat-id :age])))

(rf/reg-event-db
 :stat/gender-selection
 (fn [db [_ boat-id gender-value]]
   (println ">>>" boat-id gender-value)
   (assoc-in db [:stat boat-id :gender] gender-value)))

(rf/reg-sub
 :stat/gender-selection
 (fn [db [_ boat-id]]
   (get-in db [:stat boat-id :gender])))

(rf/reg-event-db
 :stat/havekey-selection
 (fn [db [_ boat-id havekey-value]]
    ;(println ">>>" boat-id gender-value)
   (assoc-in db [:stat boat-id :havekey] (not havekey-value))))

(rf/reg-sub
 :stat/havekey-selection
 (fn [db [_ boat-id]]
   (get-in db [:stat boat-id :havekey])))

(defn age-group-selector [boat-id]
  (letfn [(render' [i f sel [text sub-text]]
            [:div.pl-1.pb-1.flex-wrap
             {:on-click (f i)}
             (arr/age-view (= sel i) [text sub-text])])]
    "Set the users belonging age-group."
    (let [selected-age (rf/subscribe [:stat/age-selection boat-id])
          click-dispatch-fn #(rf/dispatch [:stat/age-selection boat-id %])]
      (doall (map-indexed
               (fn [i [[text sub-text]]]
                 ^{:key i}
                 (render' i click-dispatch-fn @selected-age [text sub-text]))
               [["Junior" "0-12"]
                ["Ungdom" "13-29"]
                ["Voksen" "30-70"]
                ["Senior" "71-110"]])))))

(letfn [(render' [i sel? boat-id text]
          [:div.pl-1.pb-1
           {:on-click #(rf/dispatch [:stat/gender-selection boat-id i])}
           (arr/f- sel? :kjønn [text])])]
  (defn gender-group-selector [boat-id]
    (let [gender-sel (rf/subscribe [:stat/gender-selection boat-id])
          age-group (rf/subscribe [:stat/age-selection boat-id])
          child? (< @age-group 2)]
      (doall (map-indexed
              (fn [i text]
                ^{:key i}
                (render' i #(= % gender-sel) boat-id  text)
                [(if child? "Gutt" "Mann")
                 (if child? "Jente" "Kvinne")]))))))

(defn havekey-selector [boat-id]
  (let [selected-havekey (rf/subscribe [:stat/havekey-selection boat-id])]
    (doall (map-indexed
            (fn [i [v1]]
              ^{:key i}
              [:div.pl-1.pb-1.flex-wrap
               {:on-click #(rf/dispatch [:stat/havekey-selection boat-id @selected-havekey])}
               (arr/f5 @selected-havekey v1)])
            ["Har med nøkkel"]))))

;note: fn-names beginning with + are pure delegates

(defn +cancel-card-selection []
  #(rf/dispatch [::panel-content/card-select-cancel]))

(defn +next-step []
  #(rf/dispatch [::panel-content/card-select-final-step]))

;note: experimental api

(defn pure-render [{:keys [boat-datasource right-toolbar left-toolbar bottom]}]
  "This is a pure renderer, no fx please"
  [:div.main-grid
   [:div.header.bg-blue.text-xl.font-bold "Sjekke ut båter"]
   [:div.content.bg-grey-lighter.shadow-lg
    [:div.grids boat-datasource]]
   [:div.toolbar.bg-red left-toolbar]
   [:div.footer.bg-orange
    bottom]
   [:div.toolbar2.bg-grey.flex.flex-col.items-center
    [:div.mt-5 right-toolbar]]])

(defn render []
  "contains the fx's"
  (let [list-of-selected (rf/subscribe [::panel-content/card-select])
        right-toolbar (map-indexed (fn [i e]
                                     ^{:key i}
                                     [:div.flex.w-20.h-20.justify-center.items-center.rounded-full.mb-1.bg-green.text-black.border-4.border-green-dark
                                      [:p.font-bold.text-5xl.mt-2 (inc e)]]) (range 4))]
    ;todo: extract
    (letfn [(boat-grid []
              (map-indexed
               (fn [idx [each-boat-id each-value]]
                 ^{:key idx}
                 [:div.grid-item.flex.flex-wrap
                  [:div.flex {:style {:width "20rem"}}
                   (arr/p1-old-selectable-larger
                    #{(when (contains? @(rf/subscribe [::panel-content/card-select]) each-boat-id) :selected)
                      (when (:repair each-value) :repair)
                      (when (:busy each-value) :busy)} each-boat-id each-value)]
                  (age-group-selector each-boat-id)
                  (gender-group-selector each-boat-id)
                  (havekey-selector each-boat-id)])
               (select-keys @(rf/subscribe [::panel-content/data]) @list-of-selected)))]
      (pure-render {:grid (boat-grid)
                    :right right-toolbar
                    :left (toolbar)
                    :bottom [:div.flex.justify-between.h-full
                             ;todo externalize the dispatchers to gain better names
                             [system-button +cancel-card-selection "Annuller"]
                             [system-button +next-step "Bekreft" :bg-green.m-1]]}))))

