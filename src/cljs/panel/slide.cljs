(ns panel.slide
  (:require [re-frame.core :as rf :refer [reg-event-db reg-sub]]
            [panel.events :as events]
            [panel.subs :as subs :refer [ui-path]]
            [panel.event :as event]))

;; Demo of a self-contained thing
;;
;;
;; observers

(def max-slides 5)

(reg-sub ::slide-number
         (fn [db _]
                ;(println (str (dissoc db :re-pressed.core/keyup)))
              ;(println (ui-path db :ui/panel))
           (-> db :ui/panel :slide-number)))

;;
;;
;;
;; actions

(reg-event-db ::next
              [events/insert-ui-path]
              (fn [db _]
                (let [current (:slide-number db)]
                     ;(println "next=>" current)
                  (update db :slide-number inc))))

(reg-event-db ::prev
              [events/insert-ui-path]
              (fn [db _]
                (let [current (:slide-number db)]
                     ;(println "next=>" current)
                  (update db :slide-number dec))))

;;
;;
;;
;;

(defn color-scheme [n]
  (nth [[:bg-teal :text-black]
        [:bg-red :text-white]
        [:bg-white :text-black]] n))

(defn group [color-scheme caption x]
  [:div.inline-flex.flex-col.border-white.border.m-1
   [:p.text-left.bg-black.text-green.p-1.text-xs.font-sans caption]
   [:p.inline.border-2.border-black.p-1 {:class (map name color-scheme)} x]])

(def gr-1 (partial group (color-scheme 0)))
(def gr-2 (partial group (color-scheme 1)))
(def gr-3 (partial group (color-scheme 2)))

(defn footer [v]
  [:div.xw-16.py-1.px-32.overflow-hidden.text-xs.flex.flex-row.justify-around
   [:div.flex-grow.p-1.mx-1.w-16.rounded.border-2.border-white {:style {:background "#aa881030"}} "Somethign to test' for the longer hours"]
   [:div.flex-grow.p-1.mx-1.w-16.rounded.border-2.border-black {:style {:background "#aa881030"}} "Somethign to test' for the longer hours"]
   [:div.mx-1.text-5xl.bg-black.text-yellow.p-1.rounded.text-center.w-20 v]])

(defn render []
  (let [v (rf/subscribe [::slide-number])
        r (rf/subscribe [::subs/record-ds])]
    (fn []
      [:div.bg-grey
       [:p.text-xs.bg-black.text-white.py-1.px-2 "slide"]
       [:div.flex.flex-col.justify-between.h-full
        [:div
         [:div
          {:style {:background "#1129"}}
          [:<>
           ^{:key 0} [:p (count @r)]
           ^{:key 1} [:p (str (map? @r))]
           ^{:key 2} [:p (str (first @r))]]
          [:br]
          ;^{:key 3} [:p ">>" (interpose ", " (map (comp str map?)  @r))]
          [:br]
          ^{:key 4} [:div.text-xs.font-mono.leading-loose
                     (map-indexed
                      (fn [i e]
                        ^{:key i}
                        [:div.flex.bg-red-lighter.mb-1.py-1.h-64 ;container
                         [:p.text-3xl.w-20.font-sans.font-bold.text-center.items-center.flex.justify-center.text-red-light i]
                         (condp = true
                           (map? e) [:div.flex.flex-col
                                     [:div.flex.flex-grow.w-full [gr-1 "original event" (str e)]]
                                     [:div
                                      (interpose ", " [gr-1 "caption1" (interpose ", " (keys e))])
                                      (interpose " " [gr-3 i (interpose ", " [i (:event e) (:time e)])
                                                      (gr-2 "caption" (interpose ", " (keys (:content e))))])]]

                           (vector? e) [:p.mb-1
                                        [gr-1 "x" [:span.bg-white ">> " i " " (interpose ", " (map str e))]]
                                        [gr-2 "y" [:span.bg-black.text-white "Xx"]]]

                           [:pre "Unknown " e])]) (take (if @v @v 10) @r))]
          [:br]
          ^{:key 6} [:p (str (count (rest @r))) " objects in record-ds' pool"]]
         [footer @v]]]])))

