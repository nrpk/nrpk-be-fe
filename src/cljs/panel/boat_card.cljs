;; view
(ns panel.boat-card
  (:require [re-frame.core :as rf]
            [panel.uidecorator :as uidecorator]
            [panel.logging :as log]
            [panel.pure :as pure]))

(defn render-large [[k {:keys [log history status statistics] :as v}]]
  ^{:key k}
  [:div.w-32.h-48.m-1.text-xs.p-1.flex-no-grow.flex-no-shrink.overflow-auto
   {:class ["rounded shadow border-black border-2" (if (= status :in-repair) "opacity-25 bg-grey")]}

   [:div.flex.flex-col.justify-between.h-full
    [:div.flex.justify-between.mb-1
     [:p k]
     [:p status]
     [:div.w-3.h-3.rounded {:class (case status
                                     :available "bg-green"
                                     :in-use "bg-blue"
                                     :in-repair "bg-red"
                                     :sold "bg-grey-lighter border-none"
                                     "bg-green-lighter")}]]
    [:div.flex.justify-between
     [:p.bg-yellow.font-bold (-> statistics :stat/total-rentals)]
     [:p.bg-white.text-blue.font-bold (-> statistics :stat/total-rentals)]]
    [:p (:name v)]
    [:div.flex.items-center.justify-center.w-full.h-full
     [:div.w-16.h-16.flex.justify-center.items-center
      {:class ["text-5xl border"]}
      [:p (count history)]]]]])

(defn render-compact [[k {:keys [log history status statistics] :as v}]]
  ^{:key k}
  [:div.w-32.h-auto.m-1.text-xs.p-1.flex-no-grow.flex-no-shrink.overflow-auto
   {:class ["rounded shadow border-black border-2"]}
   [:div.flex.flex-col.justify-between.h-full.mb-1
    [:div.h-auto.bg-grey-light.p-1
     [:div.flex.justify-between.mb-1.rounded-t
      [:p k]
      [:p status]
      [:div.w-3.h-3.rounded {:class (case status
                                      :available "bg-green"
                                      :in-use "bg-blue"
                                      :in-repair "bg-red"
                                      :sold "bg-grey-lighter border-none"
                                      "bg-green-lighter")}]]
     [:div.flex.justify-between
      [:p.bg-yellow.font-bold.p-1 (get statistics :stat/total-rentals "?")]
      [:p.bg-white.text-blue.font-bold.p-1 (-> statistics :stat/total-rentals)]]]
    [:div.bg-grey
     [:p (:name v)]
     [:div.flex.items-center.justify-center.w-full.h-full
      [:div.w-16.h-16.flex.justify-center.items-center
       {:class ["text-xl border p-2"]}
       [:p (count history)]]]]]])

(defn render [kv]
  [:p (pure/dump kv)])
