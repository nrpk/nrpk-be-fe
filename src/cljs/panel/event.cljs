(ns panel.event
  (:require [clojure.spec.alpha :as s]
            [clojure.test.check.generators :as gen]
            [panel.event.helpers :as helpers]
            [panel.pure :as pure :refer [rand-clock pp]]
            [re-frame.core :as rf]
            [panel.events :as events :refer [insert-ui-path]]
            [cljs-time.core :as t]
            [cljs-time.format :as f]
            [re-frame.core :as rf]
            [panel.subs :as subs]
            [panel.aggregator]
            [panel.logging :as log]))

;;
;;
;;
;; stores the currently active timer for later cancellation

(rf/reg-event-db
 ::set-main-timer-id
 []
 (fn [db [_ id]]
   (assoc db :debug/timer-id id)))

(rf/reg-event-db
  ::clear-previous-main-timer
  []
  (fn [db _]
    ;fx
    (log/i "clear")
    (js/clearInterval (:debug/timer-id db))))

(rf/reg-sub ::last-timer
            (fn [db _]
              (subs/ui-path db :debug/timer-id)))

;; note: experimental

(def stuff
  {:dispatch #(rf/dispatch [::add-random-event])
   :subs (rf/subscribe [::last-timer])})

(defn make-timer [interval]
  (let [timer (js/setInterval (:dispatch stuff) interval)]
    (rf/dispatch [::clear-previous-main-timer])
    (rf/dispatch [::set-main-timer-id timer])))

;;
;;
;;
;;

(defn kill-timers []
  (js/console.log "KILLING TIMERS")
  (if-let [i (:subs stuff)]
    (js/clearInterval @i)
    (println "Nothing to kill")))

;(kill-timers)
(make-timer 500)

;;
;;
;;
;;

(defn present [serial {e :b}]
  "returns a presentable form of e"
  (let [bg (case (:type e)
             :repair :black
             :reg :orange
             :rent :blue
             :green)]
    ^{:key serial}
    [:div.m-1.p-1.border.text-xxs.overflow-hidden.rounded.shadow.w-full
     {:class [(str "bg-" (name bg))]}
     [:p (:time e)]
     [:p (:event e)]
     [:p.font-bold (str "id" (:id e))]
     [:p.h-2]
     [:p.text-xxs.overflow-hidden {:class [(str "text-" (name bg) "-darker")]}
      (interpose ", " (keys e))]]))

(defn- str-date-format [dt]
  (f/unparse (f/formatter "mm:ss.S") dt))

(defn ts-now [] (str-date-format (t/now)))

(defn- random-event [n]
  "returns a random event (in need of a fitting function?)"
  (let [r 8
        date (str-date-format (t/now))]
    (rand-nth  [;[:illegal-command1 {:event-id id}]
                [:rent-start {:# n :id (rand-int r) :time date}]
                [:rent-end {:# n :id (rand-int r) :time date}]

                ;[:illegal-command2 {:event-id n}]
                ;[:new {:id (rand-int r) :name (str "buggy " r)}]
                ;[:reg {:type :sale :id (rand-int r) :value 1234 :syn (str "synopsis sold " n)}]
                  ;[:repair-in {:id (rand-int r) :syn "repair in"}]

                [:repair-out {:# n :id (rand-int r) :time date :syn (str "reparasjon ferdig " n)}
                 [:repair-in {:# n :id (rand-int r) :time date :syn (str "fant noe som var galt " n)}]]])

                ;[:reg {:type :hms :syn (str "synopsis " n)}]])

    #_(nth  [[:illegal-command {:id id}]
             [:repair-out {:id (rand-int r) :syn "rant"}]
                   ;[:repair-in {:id (rand-int r) :syn "fixed it" :serie id}]
             [:repair-in {:id (rand-int r) :time "00:00" :syn "fucked up"}]

             [:new {:id (rand-int r) :name (str "buggy " r)}]
             [:reg {:type :hms :syn (str "synopsis " id)}]
             [:reg {:type :sale :id (rand-int r) :syn (str "synopsis " id)}]] 0)))
                   ;[:repair-in {:extra 123 :id (rand-int r) :x id}]
                   ;[:repair-out {:id (rand-int r) :time (str "00:" id) :syn (str "Sample synopse " (rand-int r))}]])))

(rf/reg-event-db
 ::add-random-event
 [events/insert-db-path]
 (fn [db _]
   (let [counter (get db :counter 1)
         event (random-event counter)
         r-result (reduce panel.aggregator/reducer (get db :aggregated {}) [event])]
     (do
       (log/i counter (keys db) (-> db :selection :list))
       (-> db
           (assoc :aggregated r-result)
           (update :counter inc))))))

;(defn color-scheme [n]
;  (nth [[:bg-teal :text-black]
;        [:bg-red :text-white]
;        [:bg-white :text-black]] n))
;
;(defn group [color-scheme caption x]
;  [:div.inline-flex.flex-col.border-white.border.m-1.w-48
;   [:p.text-left.bg-black.text-green.p-1.text-xs.font-sans caption]
;   [:p.inline.border-2.border-black.p-1 {:class (map name color-scheme)} x]])
;
;(def gr #(group (partial color-scheme) % "x"))
;(def gr-2 (partial group (color-scheme 1)))
;(def gr-3 (partial group (color-scheme 2)))
;
;(defn render [e]
;  [:div.bg-blue.px-2.py-1.h-64 ; "RENDER EVENT"
;   [:div.m-1 (present 221 {:b e})]
;   (case (:type e)
;     :rent [gr 1 :rent [:div [:p "Utleie " (:time e)] [:p.text-xs (:dir e)]]]
;     :reg [gr 2  :regs [:div [:p "Registrering "] [:p.text-xs (:syn e)]]]
;     :repair [gr 3 :repair [:div [:p "Reparering " (:id e)] [:p.text-xs (:syn e)]]]
;     [:p "–––––– " e])
;   (str (:id e))])
