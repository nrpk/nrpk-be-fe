(ns panel.core
  (:require [re-frame.core :as rf :refer [reg-event-db reg-sub subscribe]]
            [reagent.core :as r]
            [re-frisk-remote.core :refer [enable-re-frisk-remote!]]
            [re-frisk.core :refer [enable-re-frisk!]]
            [panel.keyboard-mappings :refer [default-shortcuts]]
		;[panel.tst-renderer :as tst :refer [main-renderer]]
            [panel.uidecorator :as controller]
            [panel.logging :as log]))

(enable-re-frisk-remote! {:host                 "127.0.0.1:8095"
                          :enable-re-frame-10x? false
                          :events?              false})

(defonce x (default-shortcuts))

(def el (js/document.getElementById "app"))

(rf/reg-event-db
 :app/ready
 (fn [db _]
   (log/g "ready")
    ; I hadn't touched db/boats in a long time, that's why I was confused
    ;(log/i "data 1" (some-> db (get-in [:db/data])))
    ;(log/i "data 2" (some-> db (get-in [:db/data])))
    ;(log/i "data aggregated" (-> (get-in db [:db/data :aggregated])))
    ;(log/i "panel" (-> (get-in db [:ui/panel])))
    ;(log/i "panel :aggregated" (-> (get-in db [:ui/panel :aggregated])))
    ;(log/i (-> (map :status (-> db :ui/panel :aggregated :agg/boats))))
   (log/ge)
   db))

#_(defn ^:dev/after-load start []
  (rf/clear-subscription-cache!)
  (rf/dispatch-sync [:app/initialize-db])
  ;;(default-shortcuts)
  (r/render [controller/decorate [tst/main-renderer]]
            el
            #(rf/dispatch [:app/ready])))

(defn init []
  (start))

