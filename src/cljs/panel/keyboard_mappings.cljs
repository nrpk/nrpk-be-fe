;;(ns panel.keyboard-mappings
;;  (:require [re-frame.core :as rf]
;;            [re-pressed.core :as rp]
;;            [app.keyboard-definitions :as k]))
;;
;;(def keydata
;;	(concat
;;		(map (fn [e] [[:app.boat-selector/append %]
;;									[{:keyCode (+ k/Number0 e)}]]) (range 10))
;;		[[[:app.boat-selector/backspace]
;;			[{:keyCode k/BACKSPACE}]]]
;;
;;		[[[:app.boat-selector/clear]
;;			[{:keyCode k/C}]]]
;;
;;		[[[:next-input]
;;			[{:keyCode k/TAB}]]]
;;
;;		[[[:toggle-havekey]
;;			[{:keyCode k/K}]]]
;;
;;		[[[:app.boat-selector/set-focus :phonenumber]
;;			[{:keyCode k/T}]]]
;;		[[[:app.boat-selector/set-focus :boatnumber]
;;			[{:keyCode k/B}]]]
;;
;;		[[[:system/pause]
;;			[{:keyCode k/P :ctrlKey true}]]]
;;		[[[:system/restart]
;;			[{:keyCode k/R :ctrlKey true}]]]))
;;
;;(defn default-shortcuts []
;;  (js/console.log "default-shortcuts")
;;  (rf/dispatch-sync [::rp/add-keyboard-event-listener "keydown"])
;;  (rf/dispatch
;;   [::rp/set-keydown-rules
;;		{:event-keys           (into [] keydata)
;;		 :clear-keys           [[{:keyCode k/ESC}]
;;														[{:keyCode k/G
;;															:ctrlKey true}]]
;;		 :prevent-default-keys [{:keyCode k/G
;;														 :ctrlKey true}]
;;		 :always-listen-keys
;;
;;													 []}]))
;;
;;
;;
