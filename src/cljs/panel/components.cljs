(ns panel.components
  (:require [re-frame.core :as rf]
            [panel.events :as ev]
            [panel.icons :refer [icons]]
            [panel.arrangers :as arr]
            [panel.panel-content :as pc]
            [panel.data :as data]
            [cljs.pprint :refer [write]]
            [panel.subs :as subs]))

(defn f' [selected? k v]
  ^{:key k}
  [:div.p-2.rounded
   {:class (if selected? "bg-blue text-white" "bg-white") :on-click #(rf/dispatch [:ui-card-selected k])}
   {:id k :content v}
   [:p k]])

(defn debug-in_to_box-render []
  (let [action-record @(rf/subscribe [::subs/record-ds])
        k-ds @(rf/subscribe [::subs/boats-datasource-with-key])
        boats-in-repair (data/agg-set-of-boats-in-repair action-record)
        real_ ^{:with-meta "testing this api"} (->> boats-in-repair (map (fn [kv] (f' true kv (k-ds kv)))))]
    [:pre.mt-10.text-md.font-mono.text-white.bg-grey-dark.p-1.text-xs
     [:p.text-yellow (count action-record)]
     (with-out-str (write {::count (count action-record)
                           ::data action-record}
                          {:length 2 :lines* 3 :miser-width 10}))]))

(defn in-to-box-render []
  (let [action-record @(rf/subscribe [::subs/record-ds])
        k-ds @(rf/subscribe [::subs/boats-datasource-with-key])
        boats-in-repair (data/agg-set-of-boats-in-repair action-record)
        real_ ^{:with-meta "testing this api"} (->> boats-in-repair (map (fn [kv] (f' true kv (k-ds kv)))))

        normal-view [:div (str real_)]]
    normal-view))

(defn header-in-somewhere [c]
  (if @(rf/subscribe [::subs/ui-debugging])
    (debug-in_to_box-render)
    (in-to-box-render)))

(defn- icon-with-toggle2 [toggled? name]
  (let [i (icons name)]
    [:div.flex.items-start.h-20.px-2.border-r-8.bg-grey-lighter.rounded-r.mb-1.mr-1
     {:class (if toggled?
               "border-green border-r-8 bg-white text-black"
               "border-r-8 border-grey-light text-grey-light")}
     i]))

(defn box [palette s]
  [:div
   [:button.w-20
    {:class [(get-in palette [:normal :base] "m-1 p-1 text-xl bg-red text-yellow")]}
    s]])

(defn toolbar []
  (let [which-one (rf/subscribe [::subs/tab-selected])]
    (fn []
      (let [helper (fn [which-one tag]
                     ^{:key tag}
                     [:a.i.w-full
                      {:onClick #(rf/dispatch [::subs/tab-selected tag])}
                      (icon-with-toggle2 (= tag which-one) tag)])]
        [:div.flex.flex-col.overflow-y-scroll.h-full.bg-grey-light.shadow.rounded-r-lg.border-4.border-grey-dark
         (map (partial helper @which-one) [:circle-plus
                                           :magnifier
                                           :in-to-box
                                           :out-of-box
                                           :clock
                                           :note-taker
                                           :chat-bubble])]))))

(defn filter-båt-2 [{:keys [sel-boat sel-size sel-stability]}]
  "renders a compound of selectable items"
  (letfn [(r5 [pf pref d i [k v]]
            ^{:key i} [:div ;.pr-1.pb-1
                       {:on-click #(rf/dispatch [d k])}
                       (pf (pref k) v k)])]
    [:div.flex.flex-wrap
     (doall (map-indexed
              ;question what color is this displayed in?
             (partial r5 arr/f- #(= % sel-size) ::panel.panel-content/size-filter {:height :narrow})
             {0 'Liten
              1 'Normal
              2 'Stor}))
     (doall (map-indexed
             (partial r5 arr/f- #(= % sel-stability) ::panel.panel-content/stability-filter {:height :narrow})
             {0 'Stabil
              1 'Ustabil
              2 'Krevende}))

     (doall (map-indexed
             (partial r5 arr/f- #(= % sel-boat) ::panel.panel-content/båt-filter {:height :narrow})
             {0 'Kano
              1 'Sup
              2 'Kajakk
              3 'Robåt
              4 'Surfski}))]))

(defn system-button
  ([f s]
   [:button.w-48.rounded.bg-red.m-1
    {:on-click f} s])
  ([f s a]
   [(keyword (str (symbol :button.w-48.rounded.bg-red.m-1) "." "a"))
    {:on-click f} s]))

(defn tag [xs] [:p.text-xs.bg-teal-darker.text-white.inline.p-1.rounded xs])

(defn boat-filter []
  [:div [tag "filter"]
   [:div (panel.components/filter-båt-2 {:selected-boat @(rf/subscribe [::pc/båt-filter])})]])
