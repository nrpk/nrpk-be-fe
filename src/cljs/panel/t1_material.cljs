(ns panel.t1-material
  (:require [panel.components :as comps]
            [panel.arrangers :as arr]
            [panel.panel-content :as content :refer [card-list]]
            [panel.events :as ev]
            [re-frame.core :as rf]))

(rf/reg-sub
 ::data-source
 (fn [db [_]]
   (get-in db [:data-source])))

(rf/reg-sub
 ::boat-in-data-source
 (fn [db [_ id]]
   (get-in db [:data-source id])))

(defn +toggle-select-boat [id]
  #(rf/dispatch [::ev/card-select id]))

; todo: new rule, no let in pure fn
(defn pure-render [{:keys [boat-datasource right-toolbar material-filter toolbar footer helparea]}]
  [:div.main-grid.bg-black
   (when helparea
     [:div.helparea
      [:div.my-10.pl-8.pr-20.bg-grey-darkest.text-white.p-1.leading-loose.tracking-wide.antialiased.font-sans
       [:h1.font-hairline.uppercase.text-white-dark.font-serif "Helparea"]
       [:p.text-xs (repeat 4 "There are a lot of things that can go wrong when designing software for humans. ")]]])
   #_(when helparea
       [:div.helparea.bg-green-dark helparea])
   (when material-filter
     [:div.header
      [:div.shadow-lg.bg-blue.p-2.border-8.border-yellow.z-20
       [:p.text-3xl.font-bold
        "Oversikt over tilgjengelige båter."]
       [:p.antialiased.text-xs.text-white.tracking-wide.uppercase.my-4
        "Velg en eller flere og trykk fortsett"]
       [:div;.h-64
        (when material-filter material-filter)]]])
   [:div.content
    [:div.z-20.bg-grey-light.shadow-lg
     [:div.grids boat-datasource]]]
   (when toolbar [:div.toolbar.bg-red (toolbar)])
   (when right-toolbar [:div.toolbar2
                        [:div.bg-grey.flex.flex-col.xitems-center (when material-filter material-filter)]])
   (when footer [:div.footer footer])])

(defn compute-state [card-selected boat-id]
  #{(if (contains? card-selected boat-id) :selected)})

(def footer  :div.bg-black.p-1.h-full.font-thin.text-grey-dark.antialiased)

(defn f [idx [boat-id boat-data]]
  (let [card-selected @(rf/subscribe [::ev/card-select])]
    ^{:key idx}
    [:div {:on-click (+toggle-select-boat boat-id)}
     (arr/p1-old-selectable (compute-state card-selected boat-id)  boat-id boat-data)]))

(defn render [{:keys [top-help]}]
  (let [boat-grid (content/card-list {:busy false})
        selected-boat (rf/subscribe [:ui-boatfilter-selected])
        selected-size (rf/subscribe [:panel.panel-content/size-filter])
        selected-stability (rf/subscribe [:panel.panel-content/stability-filter])]
      (fn []
        (pure-render {:helparea (when top-help [:div.helparea top-help])
                      ;[:div.bg-green-darkest.text-yellow.p-2.font-bold.tracking-wide.antialiased.h-32 top-help]
                      :toolbar (comps/toolbar)
                      :material-filter (comps/filter-båt-2 {:selected-boat @selected-boat
                                                            :selected-size @selected-size
                                                            :selected-stability @selected-stability})
                      :right-toolbar [:div.bg-red2 "foots"]
                      :footer [:div.bg-red.h-full.p-5.text-xl.antialiased "foot a r"]
                      :boat-datasource boat-grid}))))

(comment
    (let [selected-boat (rf/subscribe [:panel.panel-content/båt-filter])
          selected-size (rf/subscribe [:panel.panel-content/size-filter])
          selected-stability (rf/subscribe [:panel.panel-content/stability-filter])
        boat-grid (content/card-list {:busy false})]))
