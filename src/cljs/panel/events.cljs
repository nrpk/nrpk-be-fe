(ns panel.events
  (:require [panel.pure :as pure :refer [rand-clock pp]]
            [re-frame.core :as rf]
            [panel.db :as db :refer [ls-key ->storage]]
            [panel.logging :as log]))


;;
;;
;;
;; remind me of why these two are separate entities?


(def insert-db-path
  "Prefixes `:db/data` to the path and then sends it to local-storage
  usage: as an interceptor"
  [(do (log/i "state->storage")
       (rf/path :data) db/->storage)])

(def insert-ui-path
  ""
  [(do (println "state->storage")
       (rf/path :ui) db/->storage)])

;; end

;note: this is not needed since the path is in the interceptor
#_(defn user-interface-path [p]
    [:user-interface p])

(rf/reg-event-db
 :ui-card-selected
 [insert-ui-path]
 (fn [db [_ arg]]
   (if (contains? (:db/ui-card-selected db) arg)
     (update db :db/ui-card-selected disj arg)
     (update db :db/ui-card-selected conj arg))))
(rf/reg-event-db
 :card-select-step-2
 (fn [db [_ arg]]
   (assoc db :tab-selected :magnifier)))
(rf/reg-event-db
 :card-select-final-step
 (fn [db [_ arg]]
   (assoc db :tab-selected :in-to-box)))
(rf/reg-event-db
 :card-select-cancel
 (fn [db [_]]
   (assoc db :ui-card-selected #{}
          :tab-selected :circle-plus)))
(rf/reg-event-db
 :ui-boatfilter-selected
 [insert-ui-path]
 (fn [db [_ arg]]
   (if (= arg (:db/ui-boatfilter-selected db))
     (assoc-in db [:db/ui-boatfilter-selected] nil)
     (assoc-in db [:db/ui-boatfilter-selected] arg))))
(rf/reg-event-db
 :ui-boatfilter-stability
 [insert-ui-path]
 (fn [db [_ arg]]
   (if (= arg (:db/ui-boatfilter-stability db))
     (assoc-in db [:db/ui-boatfilter-stability] nil)
     (assoc-in db [:db/ui-boatfilter-stability] arg))))
(rf/reg-event-db
 :example-tick
 (fn [db _]
   (update-in db [:tab-selected] first)
   (update db :xxx #(panel.pure/rotate 1 (:xxx db)))
   db))
(rf/reg-event-db
 :tab-selected
 [insert-ui-path]
 (fn [db [_ v]]
   (assoc-in db [:db/sel-tab] v)))
(rf/reg-event-db
 ::ui-toggle-debugging
 [insert-ui-path]
 (fn [db _]
   (update-in db [:db/debugging?] not)))
(rf/reg-event-db
 ::ui-toggle-help
 [insert-ui-path]
 (fn [db _]
   (update-in db [:db/ui-toggle-help] not)))
(rf/reg-event-db
 ::ui-toggle-groovebar
 [insert-ui-path]
 (fn [db _]
   (update-in db [:db/ui-toggle-groovebar] not)))
(rf/reg-event-db
 ::ui-toggle-display
 [insert-ui-path]
 (fn [db _]
   (update-in db [:db/ui-toggle-display] not)))
(rf/reg-cofx
 :component->here
 (fn [{:keys [db event] :as cofx}]
   (assoc cofx :stuff {:some 'data})))
#_(rf/reg-event-fx
   ::make-tick
   [(rf/inject-cofx :component->here)]
   (fn [{:keys [db]} args]
     ;(println (str args))
     ;(js/console.debug "a")
     {:db (update db :system/tick inc)
      :dispatch-later  [{:ms (rand-int 10) :dispatch [::add-random-event]}
                        {:ms 800 :dispatch [::ui-toggle-groovebar]}]}))

;;
;;
;;
;;

(rf/reg-event-db
 ::pause-simulation
 (fn [db [_ arg]]
   (assoc db :debug/run-simulation (not arg))))




;;
;;
;;
;; Component


(rf/reg-event-db
 :component
 [insert-ui-path]
 (fn [db [_ id]]
   (assoc db :component id)))

(rf/reg-event-fx
 ::pause
 [insert-ui-path]
 (fn [cofx _]
     ;(::subs/get-main-timer-id)
   ;(update db :pause not)))
   {:db (update (:db cofx) :pause not)
    :whatever (-> cofx :db)}))

#_(rf/reg-fx
   :whatever)
