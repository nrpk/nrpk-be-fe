(ns panel.renderer
  (:require [panel.components :as comps]
            [panel.history :as history]
            [panel.panel-content :as content]
            [panel.arrangers :as arr]
            [panel.subs :as subs]
            [panel.events :as events]
            [panel.pure :refer [pp]]
            [re-frame.core :as rf]
            [panel.data :as data]))

(defn format-help-header [header a b]
  [:div.px-4. {:class "leading-loose"}
   [:h1.py-1.text-yellow-darker.text-3xl header]
   [:p.py-1 a]
   [:p.py-1.mb-10 b]])
(defn render-help []
  (let [is-helping? @(rf/subscribe [::subs/ui-toggle-help])
        sel-panel @(rf/subscribe [::subs/tab-selected])]
    (if is-helping?
      [:div.helparea.bg-black.flex
       [:div.text-yellow.h-48
        (case sel-panel
          :circle-plus (format-help-header "Tilgjengelige båter" "bla bla" "blablabla")
          :magnifier (format-help-header "Usikker" "bla bla" "blablabla")
          :in-to-box (format-help-header "Liste over båter i reparasjon" "bla bla" "blablabla")
          :note-taker (format-help-header "Liste over båter i reparasjon" "bla bla" "blablabla")
          :out-of-box (format-help-header "reparasjon" "Dette er en liste over båter som er i reparasjon."
                                          "Så snart du er ferdig med reparasjoner")
          :clock (format-help-header "All events stored" "eventlist" "")

          "helps")]]
      [:div.helparea.hidden])))
(defn header [selected selected-boat]
  [:div.header
   (case selected
     :circle-plus (comps/filter-båt-2 {:selected-boat selected-boat})
     :magnifier (comps/filter-båt-2 {:selected-boat selected-boat})
     :clock (comps/filter-båt-2 {:selected-boat selected-boat})
     [:div.h-12.bg-blue-dark.text-yellow.xh-full.px-2.py-1.mb-0 "magnifier"])])
(defn debugger []
  [:div.debugger [:div.text-white.antialiased.text-xl.px-2.py-1.font-bold.h-32.mt-1 "debug"]])
(def transformer
  (fn [e] {(first (keys e)) (first (vals e))}))

;note: card-function
(defn f [selected? [k v]]
  ^{:key k}
  ;(println "X782" k v)
  [:div.p-2.rounded
   {:class (if selected? "bg-blue text-white" "bg-white") :on-click #(rf/dispatch [:ui-card-selected k])}
   {:id k :content v}
   [:p (:type v)]
   [:p (:brand v)]])
(defn card-list [filter-fn data-source]
  (let [card-sel (rf/subscribe [:ui-card-selected])
        filtered-xs (filter filter-fn data-source)
        xs (into {} (map transformer filtered-xs))]
    (fn []
      [:div.text-xs "card-list"
       [:div.grids.select-none
        (doall
         (map (fn [[k _ :as kv]]
                ^{:key k}
                (let [selected? (contains? @card-sel k)]
                  (f selected? kv)))
              xs))]])))
(defn list-list [data-source]
  (let [xs [{:id 1 :content {:a 1}}]]
    (fn []
      [:div.text-xl.text-white ;"list-list"
       [:div.grids.select-none
        (->> data-source
             (map (fn [[k _ :as kv]]
                    ^{:key k}
                    (let [fld (val kv)]
                      [:div.mb-1.bg-green.text-white.p-2.text-xs.leading-normal
                       [:<>
                        [:p.text-yellow k]
                        [:p (str fld)]]])))
             (into []))]])))

;;
;;
;;
;;

(defn gr [g xs] [:div [:pre._uppercase.antialiased.tracking-wide.text-xs.w-full.bg-blue-darker.opacity-75.text-white.my-2.px-2.py-1.font-bold g]
                 xs])
(defn gr-2
  ([g xs] (gr g [:pre.text-xs.text-blue-darker.leading-normal.bg-grey-lighter.py-1.px-2 (pp xs)]))
  ([xs] (gr (str `xs) [:pre.text-xs.text-blue-darker.leading-normal.bg-grey-lighter.py-1.px-2 (pp xs)])))
(defn gr-3
  ([g xs c] (gr-2 g xs))
  ([c xs] (gr (str `xs) [:pre.text-xs.leading-normal.py-1.px-2
                         {:class c} (pp xs)]))
  ([c xs caption x] (gr caption [:pre.text-xs.leading-normal.py-1.px-2
                                 {:class c} (pp xs)])))
(defn gr-4
  ([c caption xs] (gr caption [:pre.text-xs.leading-normal.py-1.px-2
                               {:class c} (pp xs)])))

;;
;;
;;
;;

(defn list-of-boats-in-repair [ds]
  (let [ks (map key ds)
        all (map (juxt :event :time
                       (comp :event :report)
                       (comp :syn :report)
                       (comp :status :content)
                       (comp :havekey :content)
                       (comp :id :content)
                       (comp :syn :content))
                 (map ds ks))
        only-repair-elements (filter #(= (first %) :repair) all)
        workcoll (map (fn [[ev time _evt _ a _havekey id syn :as all]]
                        [time id ev a syn]) only-repair-elements)]
    workcoll))
(defn list-of-boats-in-repair* [ds]
  "this is an exploration template"
  (let [test '(map (juxt :event :time :content) (map ds (map key ds)))]
    [:div
     [:div.py-1.px-2;.content ; <-- todo: remove or decide what this fn returns
      "list-of-boats-in-repair"]
     [:div.h-1
      (reverse
           ;(reverse
       (interpose
        [:br]
        (let [ks (map key ds)
              all (map (juxt :event :time
                             (comp :event :report)
                             (comp :syn :report)
                             (comp :status :content)
                             (comp :havekey :content)
                             (comp :id :content)
                             (comp :syn :content))
                       (map ds ks))
              only-rent-elements (filter #(= (first %) :rent) all)
              only-repair-elements (filter #(= (first %) :repair) all)]
          [;(gr-2 "" ks)
                 ;(gr-2 "" (map (juxt :event :time (comp :status :content)) (map ds ks)))

           (gr-2 "these could work but I am missing the :id"
                 (into {} (map (fn [[_ time action id]] {id [action time]})
                               only-rent-elements)))
           (gr-2 "but this makes me feel I should use the id as a key"
                 (into {} (map (fn [[_ time action id]] {id {action time}})
                               only-rent-elements)))
           (gr-2 "now merge the duplicates, I want to deep merge the result"
                 (apply (partial merge-with conj)
                        (map (fn [[_ time action id]] {id {action time :extra [1 2 3]}})
                             only-rent-elements)))
           (gr-2 "result?"
                 (apply (partial merge-with conj)
                        (map (fn [[_ time action id]] {id {action time}})
                             only-repair-elements)))
           (gr-2 "result? works for rentals too"
                 (apply (partial merge-with conj)
                        (map (fn [[_ time action id]] {id {action time}})
                             only-rent-elements)))
                 ;note: other
           (gr-3 "bg-red-lighter"
                 all)
           (gr-3 "bg-yellow-lighter"
                     ;(apply (partial merge-with conj)
                     ;(apply (partial merge-with conj)
                 (map (fn [[ev time _evt _ a _havekey id  syn :as all]]
                        [time id ev a syn]) only-repair-elements))
           (gr-3 "bg-yellow-light border-black border-2"
                 (map second (map (fn [[ev time _evt _ a _havekey id  syn :as all]]
                                    [time id ev a syn]) only-repair-elements)))
           (gr-4 "bg-red text-white" "this works for all things related to things with an id"
                 (->> (reduce
                       (fn rf [xs [id {:keys [event time content]}]]
                         (let [k (:id content)
                               s (:status content)]
                                                  ;(println event time content)
                           (update xs k assoc s time :type event :id k :f-id id))) {} ds)))])))]]))
(defn button-b [sel? on-click-event caption]
  [:button.py-1.px-2.text-sm.rounded.shadow.shadow-inner.border-2
   {:on-click #(rf/dispatch [on-click-event])
    :class    (if @sel? "bg-grey-dark text-white" "bg-white border-grey")}
   [:p.font-bold caption]])
(defn button [caption]
  [:button.bg-yellow.py-1.px-2.text-md.rounded.shadow.shadow-inner.border-green.border-2
   [:p.font-bold caption]])
(defn footer []
  [:div.footer.antialiased
   [:div.bg-grey-lighter
    [:div.text-base.h-20
     [:div.flex.w-full.justify-between.items-start.h-full.mt-1.py-1.px-2
      [:p.text-grey "Footer here"]
      [:div.flex.flex-row.justify-end
       (interpose [:div.ml-1]
                  [[button-b (rf/subscribe [::subs/ui-debugging]) ::events/ui-toggle-debugging "Åpne feilsøker"]
                   [button-b (rf/subscribe [::subs/ui-toggle-help]) ::events/ui-toggle-help "Vær hjelpsom"]
                   [button-b (rf/subscribe [::subs/ui-toggle-groovebar]) ::events/ui-toggle-groovebar "Verktøy"]
                   [:div.mx-1]
                   [button "undo"]])]]]]])
(defn set-of-something [_]
  (let [ds (rf/subscribe [::subs/record-ds])]
    (fn []
      [:div (str @ds)])))

(comment
  ;#_(defn combined-view [boat-grid ds]
  ;    [:div.ah-64.aoverflow-auto
  ;     ;[:div.flex.xh-full [:div.grids boat-grid]]
  ;     (list-of-boats-in-repair* ds)])
  ;
  ;#_(defn simple-grid-view [boat-grid]
  ;    [:div.flex.overflow-autos;.h-full
  ;     [:div.flex.h-full [:div.grids boat-grid]]])
  ;
  ;(defn boats-in-repair [ds]
  ;  (let [is-debugging? @(rf/subscribe [::subs/ui-debugging])
  ;        boat-datasource @(rf/subscribe [:boats-datasource-with-key])
  ;        boat-grid [:div.flex.grids (map #(f false [% (boat-datasource %)])
  ;                                        (list-of-boats-in-repair ds))]]
  ;    [:div ;.h-full.w-full
  ;     [:div.text-md.bg-black.text-white.px-2.py-1.h-16.flex.items-end "boats-in-repair"]
  ;     [:div.flex.flex-col ;.w-full.h-full
  ;      (if is-debugging?
  ;        (combined-view boat-grid ds)
  ;        (simple-grid-view boat-grid))]]))
  ;
  ;(defn boats-out [ds]
  ;  (let [is-debugging? @(rf/subscribe [::subs/ui-debugging])
  ;        boat-datasource @(rf/subscribe [:boats-datasource-with-key])
  ;        boat-grid [:div.flex.grids (map #(f false [% (boat-datasource %)])
  ;                                        [] #_(list-of-boats-still-out ds))]]
  ;    [:div
  ;     [:div.text-md.bg-blue-darkest.text-white.px-2.py-1.h-20.flex.items-end
  ;      [:div.flex.flex-col
  ;       [:p.text-xl.text-grey-light "Utleide båter"]
  ;       [:p.text-xs.text-grey "boats-out"]]]
  ;     [:div.flex.flex-col ;.w-full.h-full
  ;      (if is-debugging?
  ;        (combined-view boat-grid ds)
  ;        (simple-grid-view boat-grid))]]))
  ())
