(ns panel.panel-content
  (:require [panel.arrangers :as arr]
            [re-frame.core :as rf]
            [re-frame.db :as db]
            [panel.events :as ev]))

(defn filter-helper [k1 k2]
  (rf/reg-event-db
   k1
   (fn [db [_ arg]]
     (if (= arg (k2 db))
       (dissoc db k2)
       (assoc db k2 arg))))
  (rf/reg-sub
   k1
   (fn [db _]
     (k2 db))))

(filter-helper ::stability-filter :stability-filter)

(rf/reg-event-db
 ::size-filter
 (fn [db [_ arg]]
   (if (= arg (:size-filter db))
     (dissoc db :size-filter)
     (assoc db :size-filter arg))))

(rf/reg-sub
 ::size-filter
 (fn [db _]
   (:size-filter db)))

(rf/reg-event-db
 ::keys-selection
 (fn [db [_ arg]]
   (if (= arg (:keys-toggle db))
     (dissoc db :keys-toggle)
     (assoc db :keys-toggle arg))))

(rf/reg-sub
 ::keys-selection
 (fn [db _]
   (:keys-toggle db)))

#_(rf/reg-sub
   :data
   (fn [db _]
     (:data db)))

(defn helper [k1 k2]
  (rf/reg-event-db
   k1
   (fn [db [_ arg]]
     (assoc db k2 arg)))
  (rf/reg-sub
   k1
   (fn [db _]
     (get db k2))))

(helper ::code-selection :code-sel)
(helper ::gender-selection :gender-sel)

(def default-config {:bg-normal "bg-green" :bg-select "bg-orange"})

#_(defn card-list [{:keys [busy repair selected-boat selected-size selected-stability]}]
    (let [data (rf/subscribe [::data])]

      (letfn [(filter-fn [kv] (and
                               (if-let [s busy] (= (:busy (val kv)) s) true)
                               (if-let [s repair] (= (:repair (val kv)) s) true)
                               (if-let [s selected-boat] (= (:type (val kv)) s) true)
                                (if-let [s selected-size] (= (:size (val kv)) s) true)
                               (if-let [s selected-stability] (= (:stability (val kv)) s) true)))
              (r3 ([pf i kv] ^{:key i} [:div.flex.pr-1.pb-1
                                        {:class "card-min-width card-max-width" #_"w-full  xs:w-full sm:w-1/2 md:w-1/3 lg:w-1/5 xl:w-1/6"}
                                        (pf (rand-nth [:normal :selected :busy :repair]) (key kv) (val kv))]))]
        (let [fdata  (doall (into (sorted-map-by <) (map (fn [[a b]] {a b}) (filter filter-fn @data))))]
          [:flex.flex-col
           ;{:style {:overscroll-behavior "none"}}
           [:div.flex.flex-grow.flex-no-shrink [:h1 "sOverskrift"] [:p "test"]]
           ;[:div.flex.flex-wrap.bg-grey-light.xflex-1;.flex-no-shrink
           [:div.card-grid-container
            ;{:style {:overscroll-behavior "none"}}
            (doall (map-indexed (partial r3 arr/p1) fdata))]]))))

(defn r3 [pf i [k v]] ^{:key i} [:div.grid-item
                                 {:on-click #(rf/dispatch [:ui-card-selected k])}
                                 (pf #{(when (contains? @(rf/subscribe [:ui-card-selected]) k) :selected)
                                       (when (:repair v) :repair)
                                       (when (:busy v) :busy)} k v)])

(defn r4 [pf stat i [k v]]
  "for the selection combined with stat-input"
  ^{:key i}
  [:div.grid-item
   {:on-click #(rf/dispatch [::ev/card-select k])}
   [:div
    (pf #{(when (contains? @(rf/subscribe [:ui-card-selected]) k) :selected)
          (when (:repair v) :repair)
          (when (:busy v) :busy)} k v)
    stat]])

(defn filter-fn [kv {:keys [busy repair selected-boat selected-size selected-stability]}]
  (or true
      (and
       (if-let [s busy] (= (:busy (val kv)) s) true)
       (if-let [s repair] (= (:repair (val kv)) s) true)
       (if-let [s selected-boat] (= (:type (val kv)) s) true)
       (if-let [s selected-size] (= (:size (val kv)) s) true)
       (if-let [s selected-stability] (= (:stability (val kv)) s) true))))

(rf/reg-sub
 ; note: the data-outlet
 :data
 (fn [db _]
   (->> db
        :data-source
        (take-nth 1)
        (take 123)
        (into {}))))

(defn card-list [data-source]
  (let [data (->> (filter filter-fn data-source)
                  (map (fn [[a b]] {a b}))
                  (into (sorted-map-by <)))]
    (fn []
      [:div.grids (map-indexed (partial r3 arr/p1-old-selectable) data)])))

