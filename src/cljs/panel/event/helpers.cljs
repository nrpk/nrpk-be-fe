(ns panel.event.helpers)

(def sample-synopsis
  ["skade på brygga"
   "velt i kano"
   "velt med kajakk"
   "drukning"
   "episode med gjester"
   "nøkkelvakt episode"])

(def sample-defect-synopsis
  ["Skade på lakk"
   "Skade på kjøl"
   "Brukket front"
   "Brekt i to"])

(def sample-repair-synopsis
  ["Lakkert over skaden"
   "Satt inn ny kjøl"
   "Kastet"
   "Limt sammen foran og bak"])
