; note: view
; An attempt to have a slideshow mechanism by directly
; set the state with predefined data.
(ns panel.uidecorator
  (:require [re-frame.core :as rf]
            [panel.events :as interceptor :refer [insert-ui-path]]
            [panel.subs :as subs :refer [ui-path db-path]]
            [panel.logging :as log]
            [panel.styling :as styling]))
              ;[panel.test-renderer :as test-renderer]))

(rf/reg-event-db
 ::next
 [interceptor/insert-ui-path]
 (fn [db _]
   (update db :processing-number (fnil inc 0))))

(rf/reg-event-db
 ::prev
 [interceptor/insert-ui-path]
 (fn [db _]
   (let [current (:processing-number db)]
     (if-not (zero? current)
       (update db :processing-number dec)
       db))))

(rf/reg-sub
 ::event-list
 (fn [db _]
   (subs/ui-path db :processing-number)))

(defn caption [toggle s]
  [:p.m-1 {:class (flatten [(styling/some-style-a toggle)])} s])

(defn decorate [xs]
  (let [stage (rf/subscribe [::event-list])]
    [:div
     [:div.m-1.p-5 {:class (styling/some-style-a 1)}  "Stage " @stage [:div.p-10.bfs1 ^{:key (count @stage)} xs]]]))
