(ns panel.history
  (:require [re-frame.core :as rf]
            [panel.components :as comps]
            [panel.panel-content]
            [panel.arrangers :as arr]
            [panel.events :as ev]
            [panel.panel-content :as content]))

(rf/reg-event-db
 ::record
 (fn [db [_ arg]]
   (assoc-in db [:record] arg)))

(rf/reg-sub
 ::record
 (fn [db [_]]
   (get-in db [:record])))

(rf/reg-sub
 :data-source
 (fn [db [_ boat-id]]
   (get-in db [:data-source boat-id])))

;(def is-expanded @(rf/subscribe [:in-debug]))

(defn label [c & s]
  [:div.antialiased.tracking-wide.uppercase.text-xs.font-bold.text-black.bg-yellow-lighter.rounded.inline-block.py-2.m-1.px-4
   c
   (into [:div] s)])

(rf/reg-event-db
 :x/some-demo-tag
 (fn [db [_]]
   (assoc db :x/some-demo-tag (not (get-in db [:x/some-demo-tag] false)))))

(rf/reg-sub
 :x/some-demo-tag
 (fn [db [_]]
   (get-in db [:x/some-demo-tag])))

(defn toggle [xs]
  (if @(rf/subscribe [:x/some-demo-tag])
    [label {:on-click #(rf/dispatch [:x/some-demo-tag])} "tags"]
    [:div {:on-click #(rf/dispatch [:x/some-demo-tag])} xs]))

