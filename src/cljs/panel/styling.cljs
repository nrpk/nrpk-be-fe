(ns panel.styling)

(defn some-style-a [toggle]
  [:flex
   :h-32
   :bg-grey-darker
   :text-white
   :text-3xl
                                        ;:font-bold
   :font-thin
   :tracking-wide
   :antialiased
   :px-4
   (if (even? toggle)
     [:justify-end :items-start]
     [:justify-start :items-center])])
