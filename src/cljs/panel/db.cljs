(ns panel.db
  (:require [re-frame.core :as rf]
            [clojure.pprint :refer [pprint]]
            [panel.data :as data]
            [cljs.reader]
            ;[cljc.time-clj :refer [next-time]]
            [core.aggregation :as aggr :refer [computation

                                               add-boat
                                               assign-boat-name
                                               remove-boat-name
                                               set-boat-in-repair
                                               set-boat-out-of-repair
                                               rent-start
                                               rent-end
                                               initial-model]]))

;; substitute next-time

(defn next-time [] nil)

;;
;;
;;
;; local storage

(def ls-key
  "nrpk-panel-16") ;remember: to mutate the key on structural changes in app-db

(def ->storage
  #_(rf/after #(js/localStorage.setItem ls-key %)))

#_(defn state->storage
    "writes panel-data to localStorage"
    [data]
    (println "state->storage")
    (js/localStorage.setItem ls-key (str data)))

(rf/reg-cofx
 :state<-storage
 (fn [cofx _]
   (assoc cofx :local-storage
          (into (sorted-map)
                (some->> (js/localStorage.getItem ls-key)
                         (cljs.reader/read-string))))))

;;
;;
;;
;; default data

(def default-db-data
  {:db/record nil; data/src
   :db/boats (data/local-storage-boat-db)})

(def default-ui-panel
  {:visibility-toggled? false
   :visible-token true
   :processing-number 1
   :slide-number 5
   :selection {:type :boat :list [1 2 3 4]}
   :modal {:visible true}
   :pause false})

;;
;;
;;
;;

(defn- mount-root-here []
  (let [c 45
        event-queue (concat [(add-boat c)]
                            (map add-boat (range c))
                            (map (fn [e] (assign-boat-name e "test")) (range c))
                            [(assign-boat-name 112 "herkules 1")]
                            [(assign-boat-name 1 "herkules 2")]
                            [(assign-boat-name 2 "herkules 3")]
                            [(rent-start 1 (next-time))]
                            [(rent-start 2 (next-time))]
                            [(rent-start 3 (next-time))]
                            [(rent-start 4 (next-time))]
                            [(rent-start 5 (next-time))]
                            [(set-boat-in-repair 99)]
                            [(set-boat-in-repair 98)]
                            [(rent-start 6 (next-time))]
                            [(rent-start 7 (next-time))])

        sample-model
        {:event/epoch  nil
         :errors []
         :events []
         :boats  (conj {441 {:boat/status :boat/available
                             :boat/name 441
                             :packet/time (next-time)}
                        442 {:boat/status :boat/available :packet/time (next-time)}
                        443 {:boat/status :boat/available :packet/time (next-time)}})}]
    (reduce aggr/computation sample-model event-queue)))

(rf/reg-event-fx
 :app/initialize-db
 [(rf/inject-cofx :state<-storage)]
 (fn [{:keys [db local-storage]} _]
   (println "reading storage into state: state<-storage")
   {:db (merge
         {:ui/pad-boat-number  "123"
          :ui/pad-phone-number "32112300"
          :ui/pad-amount-number "02"
          :ui/control-panel-open false
          :ui/input-switcher :field/boat-number

          :ui/person-data-for-boat [{:age 0 :gender 0 :other 1 :member 2}]

          :ui/havekey-toggle true
          :ui/phone-number-toggle false

          ;:ui/aggregation            (mount-root-here)
          :data             default-db-data
          :ui               default-ui-panel}
         {:ui/running-sim false})}))

