(ns panel.test
  (:require [panel.event :refer [random-event ts-now]]
            [panel.aggregator :as a :refer [reducer]]
            [panel.pure :refer [ppp]]
            [cljs.test :refer [deftest is]]))

(defn rent-start [n id]
  [:rent-start {:# n :id id :time (ts-now)}])

(defn rent-end [n id]
  [:rent-end {:# n :id id :time (ts-now)}])

(defn repair-in [n id & {:keys [syn]}]
  [:repair-in {:# n :id id :time (ts-now)
               :syn syn}])

(defn repair-out [n id & {:keys [syn]}]
  [:repair-out {:# n :id id :time (ts-now)
                :syn syn}])

(defn reg [n id & {:keys [syn]}]
  [:reg {:# n :id id :type :hms :syn syn :time (ts-now)}])

;;
;;
;;
;; DSL buildup

(def all-hms  :agg/reports)
(def all-boats :agg/boats)
(comment
  (do
    (defprotocol INext
      (next [this p]))

    (defrecord Next [n]
      INext
      (next [this p-fn]
        (assoc this :n (inc (:n this))
               :x ((p-fn (:n this))))))
    ;
    ;(do
    ;  (def n (map->Next {:n 1}))
    ;  (as-> (map->Next {:n 1}) $
    ;      (next $ #(rent-start % 100))
    ;
    ;  (def r (reduce
    ;           (fn [m e] (assoc m :x (next n (fn [] :x))))
    ;           {}
    ;           (range 210)

    (def existing-state {:agg/boats {100 {:status :in-use}}})

    (def new-events
      (letfn [(n-id [n] 1)]
        (let [id 100]
          [(rent-start 1 id)
           (rent-end 2 id)
           (repair-in 3 id :syn "fant noe som var galt")
           (rent-start 4 101) ; should fail
           (repair-out 5 id :syn "fikset det")
           (reg 6 _ :syn "skade på brygga, noen fikk en på tygga")
           (reg 7 _ :syn "episode med nøkkelvakt")])))

    (-> (reduce a/reducer existing-state new-events)
        all-hms
        ppp))

  (deftest some-test
    (let [r (partial reduce a/reducer)
          minimalist-state {:agg/boats {102 {:status :available}
                                        103 {:status :hold}
                                        101 {:status :available}}}]
      (is (r minimalist-state
             [(rent-start 4 1101)
              (rent-start 4 1101)
              (rent-start 4 1101)])))))

(deftest testings
  (is (= 1 1)))
