(ns panel.data
  (:require [clojure.spec.alpha :as s]
            [clojure.pprint]
            [panel.pure :refer [rand-clock pp]]))

(defn generate-series [offset size m]
  (into {} (for [n (range 0 size)]
             (let [k (+ n offset 0)]
               {k (assoc m
                         :brand (rand-int 10)
                         :product (rand-int 10)
                         :material (rand-int 2)
                         :stability (rand-int 4)
                         :type (rand-int 5)
                         :size (rand-int 4))}))))

(comment (do

           (defn generate-boats [size offset]
             (->> (range 0 size)
                  (map (partial + offset))
                  (map (fn [e]
                         {e {:id 1234
                             :product   (rand-int 10)
                             :brand     (rand-int 10)
                             :material  (rand-int 2)
                             :stability (rand-int 4)
                             :type      (rand-int 5)
                             :size      (rand-int 4)}}))))

           (generate-series 100 3 {})))

(defn local-storage-boat-db []
  (generate-series 100 4 {}))

(def src
  (letfn [(fix [t i s] {:event :repair :time t :content {:id i :status :out :syn s}})
          (rep [t i s] {:event :repair :time t :content {:status :in :id i :syn s}})
          (in [t i] {:event :rent :time t :content {:id i :status :in}})
          (out [t i] {:event :rent :time t :content {:id i :status :out}})
          (ou2 [t i st k] {:event :rent :time t :content {:id i :status :out :havekey k :stat st}})
          (hms [t s] {:event :register :time t :content {:status :hms :syn s}})]
    (let [data (concat (map-indexed (fn [i e] (ou2 (rand-clock) i {} true)) (range 4))
                       [(ou2 "12:32" 103 {:age 0 :gender 1 :extra 3} true)]
                       (out "13:00" 104)
                       (ou2 "13:200" 109 {:age 0 :gender 1 :extra 2} true)
                       (in "13:200" 109)
                       (rep "13:20" 109 "Something went wrong")
                       (out "13:01" 104)
                       (out "13:01" 304)
                       (out "13:04" 305)
                       (out "13:21" 308)
                       (out "14:14" 464)
                       (in "14:14" 105)
                       (rep "14:00" 103 "Skade på ∆")
                       (rep "16:00" 105 "Skade på lakken")
                       (rep "16:20" 106 "Skade på lakken")
                       (hms "13:00" "kort beskrivelse her")
                       (hms "13:20" "skade på brygga")
                       (in "14:12" 103)
                       (fix "21:10" 103 "limte to ganger")
                       (rep "21:12" 104 "skade")
                       (rep (rand-clock) 412 "skade i front, mulig hull")
                       (rep (rand-clock) 413 "skade i front, mulig hull")
                       (rep (rand-clock) 414 "skade i front, mulig hull")
                       (rep (rand-clock) 416 "skade i front, mulig hull")
                       (fix "22:30" 104 "limte tre ganger"))]
      data #_(letfn [(action-record [data] (map (fn [a b] {a b}) (range) data))]
               (action-record data)))))


; todo: this one is obsolete, replace with the new event-system located in


(comment

  "Set of closures to compute some
    "

  (->> src
       (map
        (comp
         (fn [[e {:keys [event time content]}]]
           {:id event
            :e e
            :status (:status i)})
         (juxt :event :content)
         first
         vals))
       (into {}))
  (->> src
       (map
        (comp
         (juxt :event :time :content)
         first
         vals))
       (map (juxt first second))

       (into []))
  (->> src
       (map (comp first vals))
       (map-indexed (fn [idx {:keys [event time content]}]
                      {(if-let [id (:id content)] id idx) event}))
       (into {}))
  (->> src
       (map first)
       (map (fn [[k {:keys [event time content] :as v}]]
              {k (case event
                   :register
                   {event (:status content)
                    :time time
                    :syn (:syn content)}
                   :rent
                   {event (:status content)
                    :time time
                    :id (:id content)}
                   :repair
                   {event (content :status)
                    :time time
                    :id (content :id)
                    :syn (:syn content)})}))
       (into {})))

(defn merge-fun!
  "temporary? merges three different event-types; register, rent and repair"
  [[k {:keys [event time content]}]]
  {k (merge {:event-id k}
            (case event
              :register {event (:status content)}
              :time time
              :syn (:syn content)
              :rent {event (:status content)
                     :time time
                     :boat-id (:id content)}
              :repair {event (content :status)
                       :time time
                       :boat-id (content :id)
                       :syn (:syn content)}

              {event :what?}))})

(defn temp [src]
  (->> src
       (map first)
       (map merge-fun!)
       (into {})))

(defn find'
  ([pred] (find' pred src))
  ([pred src]
   (->> (temp src)
        (filter (comp pred val))
        (into {}))))

;(find' :register src)

(defn list-of-boats-repair-xxx [ds]
  (reduce (fn [xs m]
            (let [{event :event
                   time :time
                   content :content} (-> m vals first)
                  {id :id
                   status :status
                   syn :syn} content]
                      ;(println (keys (first (vals m))))
              (case event
                :repair (conj xs (assoc {}
                                        event status
                                        :id id
                                        :time time
                                        :syn syn))
                          ;:content '(dissoc content :id)))
                xs)))
          [] ds))

(defn agg-set-of-boats-in-repair [r]
  ^{:meta "for debugging"}
  (case 2
    1 (into {} (reduce-kv (fn [xs k m] (assoc xs (:id m) m)) {}
                          (list-of-boats-repair-xxx r)))
    2 {:test 'r :test2 (list-of-boats-repair-xxx r)}))

(letfn [(list-of-boats-in-rent-xxx [ds]
          "transformer"
          (reduce (fn [xs m]
                    (let [{event :event
                           time :time
                           {id :id
                            status :status :as content} :content} (-> m vals first)]
                      (if (= event :rent)
                        (conj xs (assoc {}
                                        event status
                                        :time time
                                        :id id
                                        :content (dissoc content id)))
                        xs)))
                  [] ds))]
  (defn set-of-boats-out [xxs]
    (reduce-kv (fn [xs _ {:keys [rent id]}]
                 (case rent
                   :in (conj xs id)
                   :out (disj xs id)))
               #{}
               (list-of-boats-in-rent-xxx xxs))))

