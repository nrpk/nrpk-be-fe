(ns panel.pure
  (:require [clojure.pprint :refer [pprint]]
            [clojure.string :refer [upper-case capitalize split trim]]))

(defn rotate
  "Take a collection and rotates it n steps. negative n for direction.
  Executes in O(n) time."
  [n coll]
  (let [c (count coll)]
    (take c (drop (mod n c) (cycle coll)))))

(defn padNumber [x n]
  (apply str (flatten [(repeat (- n (count (str x))) "0") x])))

(defn rand-clock []
  (str  (padNumber (inc (rand-int 12)) 2)
        ":"
        (padNumber (* 15 (mod (rand-int 60) 4)) 2)))

(do
  (defn pp
    ([& e] (with-out-str
             (prn-str "HEADER")
             (prn-str (mapcat (fn [[k v]] (str k v))
                              {:map (map? e)
                               :vec (vector? e)
                               :seq (seq? e)}))
             (pprint ['header e]))))
  {1 {1 {1 2
         3 4
         4 5
         5 "6"}}})

(defn ppp [xs]
  (println (pp xs)))

(defn pp2
  ([c e] [:div.text-xs.w-auto.bg-green.overflow-hidden
          [:p.font-bold.text-red-lighter c]
          [:div.h-2]
          [:p.font-mono (with-out-str (pprint e))]]))

(def p-identity
  (fn [e] [:p e]))

(defn kebab->camel [k]
  (let [k (name k)
        c (count k)]

    (loop [i 0
           b ""
           u false]
      (if (< i c)
        (if (= "-" (nth k i))
          (recur
           (inc i)
           b
           true)
          (recur
           (inc i)
           (str b (if u (upper-case (nth k i)) (nth k i)))
           false))
        [k b]))))

(defn kebab->camel2 [k]
  (let [[a & b] (split k "-")]
    (str a (apply str (map capitalize b)))))

(comment
  (let [amount 10000
        f #(-> % (split ",") last trim)
        b1 (simple-benchmark
            [s (keyword (str "this-is-a-test--" "-e"))
             f kebab->camel]
            (f s) amount :print-fn f)
        b2 (simple-benchmark
            [s (keyword (str "this-is-a-test--" "-e"))
             f kebab->camel2]
            (f s) amount
            :print-fn f)]
    (println b1)))
    ;(println b2)))

;;(comment
;;  (pp (partition 1 3
;;                 (map (fn [_] (rand-clock)) (range 40))))
;;
;;;note: included an index to ease referral
;;  (def data
;;    (map-indexed
;;     (fn [i _] {:time (rand-clock) :index i})
;;     (range 20)))
;;
;;  (pp  data)
;;  (println "---")
;;
;;  (do
;;    (def result
;;      (->> data
;;
;;           (map-indexed (fn [i {:keys [time]}] {i time}))
;;           (into (sorted-map))))
;;
;;    (pp result))
;;
;;  (do
;;    (pp data)
;;    (map (comp count val)
;;         result)))

(defn dump [x]
  [:pre.text-mono.text-xs (with-out-str (pprint x))])
