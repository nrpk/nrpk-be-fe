(ns panel.old.eventsourcing
  (:require [clojure.pprint :refer [pprint]]
            [com.stuartsierra.component :as component]))
(comment (do
           (defprotocol IEventSourcing
             (add [c item]))

           (defrecord EventSources [extra]
             IEventSourcing
             (add [i item]
               (assoc extra  :e (assoc i :stuffs (conj item)))
               i)
             component/Lifecycle
             (start [t] (println "YO we are starting"))
             (stop [t] (println "stopp")))

           (def x (EventSources.  {:already :here}))

           (component/start x)
           (println "---")
           (add (add x "a113") "321")))

(defrecord Preferences [toggle]
    component/Lifecycle
    (start [c] c)
    (stop [c] c))

(defn new-preferences []
    (map->Preferences {:toggle true}))
