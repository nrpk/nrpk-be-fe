(ns panel.aggregator
  (:require [re-frame.core :as rf]
            [panel.subs :as subs]))

(def target-aggr {:agg/boats {1 {:name "bla bla 1"
                                 :id 1                       ;;think: is this the same as the key in the parent?
                                 :size :large
                                 :brand "jalla"
                                 :stats nil
                                 :history []}
                              2 {:name "bla bla 2"
                                 :stats {:hours 12 :rentals 10}}
                              3 {:name "bla bla 3"
                                 :id 3
                                 :size :large
                                 :stats {:stat/total-hours 4
                                         :stat/total-rentals 5
                                         :stat/defect-count 1
                                         :stat/f 4
                                         :stat/m 12
                                         :stat/below-12 1
                                         :stat/above-60 2}
                                 :status :repair
                                 :history [{:time "23:40"
                                            :syn "Noe var galt med roret"}]
                                 :time "21:00"}
                              4 {:id 4
                                 :status :ok
                                 :history [{:time "11:00" :syn "defekt kjøl"}
                                           {:time "11:05" :syn "Fikset kjølen med litt lim"}]}
                              5 {:id 5
                                 :status :repair}}
                  :error [{:unknown :empty :tail nil}]
                  :reports {:sale {}
                            :hms {}
                            :other [{:time "23:25", :id 3}]}})

(def data-events
  [[:repair-in {:id 4 :time "11:00" :syn "Defekt kjøl"}]
   [:repair-in {:id 4 :time "23:59" :syn "Flekker"}]
   [:repair-in {:id 4 :time "23:59" :syn "Sprekk i bunnen"}]
   [:repair-out {:id 4 :time "23:59:32" :syn "Vi ignorerer det og skylder på brukeren"}]
   [:rent-start {:time "03:22" :id 4}]
   [:rent-end {:time "03:24" :id 4}]
   [:new {:id 5 :name "test-subject"}]
   [:rent-start {:time "03:23" :id 5}]
   [:rent-start {:time "03:29" :id 5}]
   [:rent-end {:time "03:30" :id 5}]
   [:rent-start {:time "03:42" :id 5}]
   [:rent-end {:time "03:44" :id 5}]
   [:new {:id 1 :initiated "7 April 2020" :name "Poker Kayak 2000" :size :large :brand "fancy-brand" :type :kajak :material 0}]
   [:new {:id 5 :initiated "8 April 2020" :name "Poker Sup 3000 Pro" :size :large :brand "fancy-brand" :type :sup :material 0}]
   [:new {:id 2 :name "bla bla 2" :size :large :brand "" :material 1}]
   [:new {:id 3 :name "bla bla 3" :size :large :brand "" :material 2}]
   [:repair-out {:id 4 :time "11:05" :syn "Fikset kjølen med litt lim"}]
   [:rent-start {:time "21:00" :id 4 :stats {:gender 'f :age :below-12}}]
   [:rent-start {:time "21:02" :id 3 :stats {:gender 'f :age :below-12}}]
   [:rent-end {:time "23:20" :id 3}]
   [:rent-start {:time "23:22" :id 3}]
   [:rent-start {:time "23:23" :id 3}]
   [:rent-end {:time "23:24" :id 3}]
   [:rent-end {:time "23:25" :id 3}]
   [:rent-end {:time "23:25" :id 4}]
   [:reg {:time "123:25" :id 3 :type :party}]
   [:reg {:time "23:25" :id 3}]
   [:pling-error {:time "23:25" :id 3}]
   [:empty]
   [:empty]
   [:empty]
   [:empty]
   [:reg {:type :hms :syn "Klage på bryggen"}]
   [:reg {:type :hms :syn "Klage på bryggen"}]
   [:reg {:type :hms :syn "Klage på bryggen"}]
   [:reg {:type :hms :syn "Klage på bryggen"}]
   [:reg {:type :hms :syn "Drukning"}]
   [:reg {:type :hms :syn "Drukning"}]
   [:reg {:type :hms :syn "Drukning"}]
   [:reg {:type :hms :syn "innkjøp"}]
   [:reg {:type :sale :id 4 :value 1500}]
   [:rent-end {:time "23:24" :id 3}]
   [:rent-start {:time "10:22" :id 3}]
   [:rent-start {:time "10:22" :id 5}]
   [:rent-start {:time "13:23" :id 6}]
   [:repair-in {:id 2 :time "23:40" :syn "Noe var galt med roret"}]])

(defn- path-for [p id & ks]
  (flatten [p id ks]))

;;
;;
;;
;; helpers

(def ^:dynamic *error-reporting* true)

(defn add-error [m id e']
  (if-not *error-reporting*
    m
    (let [errors (:agg/errors m)]
      (assoc m :agg/errors
             (cons {:error e' :tail id} errors)))))

(defn is-sold? [m id]
  (= :sold (get-in m [:agg/boats id :status])))

(defn in-repair? [m id]
  (= :in-repair (get-in m [:agg/boats id :status])))

(defn is-available? [m id]
  (= :available (get-in m [:agg/boats id :status])))

(defn in-use? [m id]
  (= :in-use (get-in m [:agg/boats id :status] :in-use)))

;;
;;
;;
;; operations

(defn- start-repair [m id item]
  (letfn [(limit-list-length-to [x item]
              ;todo is this very slow? time it!
              ;conj (merge item {:status :reparert}))
            (conj x (merge item {:status :defekt})))]
    ; dont repair sold boats
    (if (is-sold? m id)
      (add-error m id "attempt to repair a sold boat with id ")
      (if (in-repair? m id)
        (add-error m id "attempt to repair a boat in repair with id ")
        (let [path (fn [& ks] (path-for :agg/boats id ks))]
          (-> m
              (assoc-in (path :status)
                        :in-repair)
              (update-in (path :history)
                         limit-list-length-to item)))))))

(defn- end-repair [m id item]
  (letfn [(limit-list-length-to [x item]
                ;todo is this very slow? time it!
              ;conj (merge item {:status :reparert}))
            (conj x (merge item {:result :repaired})))]
    (if (is-sold? m id)
      (add-error m id  "attempt to end-repair a sold boat with id ")
      (let [path (fn [& ks] (path-for :agg/boats id ks))]
        (-> m
            (assoc-in (path :status)
                      :available)
            (update-in (path :history)
                       limit-list-length-to item))))))

(defn- start-rent [m id item]
  (if-not (is-available? m id)
    (add-error m id "boat with this id is not available for rent")
    (let [path (fn [& ks] (path-for :agg/boats id ks))]
      (-> m
          ; marks the boat as used
          (assoc-in (path :status)
                    :in-use)
          ; increase the rental-counter for the boat
          (update-in (path :statistics :stat/total-rentals)
                     inc)
          ; add item to the statistics
          (assoc-in (path :statistics :log (:# item))
                    (-> item
                        (dissoc :id :#)
                        (assoc :mark "start-rent")))))))

(defn- end-rent [m id item]
  (if-not (in-use? m id)
    (add-error m id "attempt to end-rent on a boat not in use")
    (let [path (fn [& ks] (path-for :agg/boats id ks))]
      (-> m
          (assoc-in (path :status)
                    :available)
          (update-in (path :statistics :stat/total-hours)
                    ; todo: compute hours later, just inc for now
                     inc)
          (assoc-in (path :statistics :log (:# item))
                    (-> item
                        (dissoc item :id :#)
                        (assoc :mark "end-rent")))))))

(defn- new-material [m id item]
    ; don't add existing keys
  (if-let [new (get-in m [:agg/boats id])]
    (add-error m id "Attempt to new on existing material ")
    #_(update-in m [:agg/errors]
                 conj {:unrecognized (str "Attempt to new on existing material '" id " " (:name new) "'")})
    (-> m
        (assoc-in [:agg/boats id]
                  item))))

(defn- reg-event [m id item]
  (case (:type item)
    :hms (update-in m [:agg/reports :hms] conj item)
    :order m
    :sale (if (not= :sold (get-in m [:agg/boats (:id item) :status]))
            (-> m
                (assoc-in [:agg/boats (:id item) :status]
                          :sold)
                (update-in [:agg/reports :sale] conj item)
                (update-in [:agg/boats (:id item) :history] conj item))
            (add-error m id "shites"))
    ;; vs (update-in [:agg/errors] conj {:id id :error :shit})))
    ;; else
    (update-in m [:agg/report :other] conj item)))

(defn reducer [m [h t]]
  "this is at the heart-reducer of this system"
  (let [id (:id t)]
    (case h
      :rent-start (start-rent m id t)
      :rent-end (end-rent m id t)
      :reg (reg-event m id t)
      :new (new-material m id t)
      :repair-in (start-repair m id t)
      :repair-out (end-repair m id t)
      (add-error m id t))))

;;
;;
;;
;; accessors

(defn boat-statuses [r]
  (->> (:agg/boats r)
       (map (fn [[k v]] {k (:status v)}))
       (into {})))

(defn boat-alldata [r]
    ;as a sorted map to avoid the annoying movement of the cards
  (->> (:agg/boats r)
       (into {})))

(defn report-errors [r]
  (take-last 5 (:agg/errors r)))
