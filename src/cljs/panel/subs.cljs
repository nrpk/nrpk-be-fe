(ns panel.subs
  (:require [re-frame.core :as rf]))

;; note: You need to be explicit about the db namespace.
;;
;; All names in the db-namespace denotes keys in the blob
;; stored to local-store. But only inside :user-interface

(defn ui-path [db  k]
  "this will be local-storage'd"
  (-> db :ui/panel k))
(defn db-path [db k]
  "the db section in the app-db governs boats and the record"
  (-> db :db/data k))

;;
;;
;;
;;

(rf/reg-sub
 :ui-card-selected
 (fn [db _]
   (into #{} (get-in db [:ui/panel :db/ui-card-selected]))))
(rf/reg-sub
 :in-debug
 (fn [db _]
   (ui-path db :db/are-we-debugging?)))
(rf/reg-sub
 ::ui-boatfilter-selected
 (fn [db _]
   (ui-path db :db/ui-boatfilter-selected)))
(rf/reg-sub
 ::ui-toggle-help
 (fn [db _]
   (ui-path db :db/ui-toggle-help)))
(rf/reg-sub
 ::ui-debugging
 (fn [db _]
   (ui-path db :db/debugging?)))
(rf/reg-sub
 ::tab-selected
 (fn [db _]
   (ui-path db :db/sel-tab)))

;note: this is special, since :data is at {}
(rf/reg-sub
 :data-source
 (fn [db _]
   (db :data)))
(rf/reg-sub
 ::boats-datasource
 (fn [db _]
   (db-path db :db/boats)))
(rf/reg-sub
 ::boats-datasource-with-key
 (fn [db _]
   (into {} (get-in db [:db/data :db/boats]))))
(rf/reg-sub
 ::record-ds
 (fn [db _]
   (db-path db :db/record)))
(rf/reg-sub
 ::ui-toggle-display
 (fn [db _]
   (ui-path db :db/ui-toggle-display)))
(rf/reg-sub
 ::on-tick
 (fn [db _]
   (get-in db [:system/tick])))
(rf/reg-sub
 ::ui-toggle-groovebar
 (fn [db _]
   (get-in db [:ui/panel :db/ui-toggle-groovebar])))
(rf/reg-sub
 ::simulation-running?
 (fn [db _]
   (:debug/run-simulation db)))
(rf/reg-sub
 ::component
 (fn [db _]
   (ui-path db :component)))
(rf/reg-sub
 ::get-main-timer-id
 (fn [db _]
   (ui-path db :debug/timer-id)))
(rf/reg-sub
 ::pause
 (fn [db _]
   (ui-path db :pause)))
(rf/reg-sub
 ::aggregated
 (fn [db _]
   (db-path db :aggregated)))

(rf/reg-sub
  ::agg-counter
  (fn [db _]
   (db-path db :counter)))

(comment
  ; shallow ideas for code/api, create a helper that combines the sub and event registration AND let you provide a fn that is called on the value

  (def toggle #(not %))

  (defn helper-db [f {:keys [sub-key event-key path]}]
    "see usage, we only touch the db"
    (let [path ()]
      (rf/reg-sub
       sub-key
       (fn [db _]
         (get-in db path)))
      (rf/reg-event-db
       event-key
       [insert-db-path]
       (fn [db _]
         (update-in db path



                    f))))))


