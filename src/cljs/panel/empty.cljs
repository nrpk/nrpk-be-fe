(ns panel.empty
  (:require [panel.events]
            [clojure.core.match :refer [match]]
            [re-frame.core :as rf]
            #_[reagent.debug :refer [log]]))



(def thistle 'd8bfd8)

(defn background [c]
  {:style {:background (str "#" c) :color :black}})

;#_(def x
;    {:db/boats '({100 {:brand 4, :product 7, :material 1, :stability 2, :type 0, :size 2}}
;                  {101 {:brand 0, :product 6, :material 1, :stability 0, :type 4, :size 3}}
;                  {102 {:brand 0, :product 3, :material 1, :stability 3, :type 2, :size 3}}
;                  {103 {:brand 5, :product 5, :material 1, :stability 2, :type 1, :size 1}}
;                  {104 {:brand 5, :product 0, :material 0, :stability 0, :type 2, :size 0}}
;                  {105 {:brand 9, :product 4, :material 0, :stability 0, :type 3, :size 1}}),
;     :db/record '({:event "Utleie", :direction :in, :id 199, :type :rent, :time "02:30", :syn nil}
;                   {:event "Utleie", :direction :out, :id 198, :type :rent, :time "11:30", :syn nil}
;                   {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "06:30", :syn "skade på brygga"}
;                   {:event "Utleie", :direction :in, :id 196, :type :rent, :time "11:00", :syn nil}
;                   {:event "Utleie", :direction :out, :id 195, :type :rent, :time "10:00", :syn nil}
;                   {:event "Skade", :direction :in, :id 194, :type :repair, :time "08:00", :syn "Skade på kjøl"}
;                   {:event "Utleie", :direction :out, :id 193, :type :rent, :time "05:45", :syn nil}
;                   {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "03:00", :syn "nøkkelvakt episode"}
;                   {:event "Utleie", :direction :out, :id 191, :type :rent, :time "10:30", :syn nil}
;                   {:event "Utleie", :direction :out, :id 190, :type :rent, :time "04:30", :syn nil}
;                   {:event "Skade", :direction :out, :id 189, :type :repair, :time "09:15", :syn nil}
;                   {:event "Skade", :direction :in, :id 188, :type :repair, :time "10:00", :syn "Skade på kjøl"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "11:30", :syn "episode med gjester"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "04:00", :syn "nøkkelvakt episode"} {:event "Utleie", :direction :out, :id 185, :type :rent, :time "08:45", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "09:30", :syn "velt i kano"} {:event "Skade", :direction :out, :id 183, :type :repair, :time "12:45", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "12:30", :syn "nøkkelvakt episode"} {:event "Skade", :direction :in, :id 181, :type :repair, :time "10:45", :syn "Skade på kjøl"} {:event "Skade", :direction :out, :id 180, :type :repair, :time "12:00", :syn nil} {:event "Skade", :direction :out, :id 179, :type :repair, :time "12:45", :syn nil} {:event "Skade", :direction :in, :id 178, :type :repair, :time "10:15", :syn "Skade på lakk"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "02:00", :syn "episode med gjester"} {:event "Utleie", :direction :out, :id 176, :type :rent, :time "10:45", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "01:45", :syn "nøkkelvakt episode"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "09:45", :syn "velt med kajakk"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "06:00", :syn "drukning"} {:event "Skade", :direction :out, :id 172, :type :repair, :time "03:15", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "06:00", :syn "drukning"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "03:15", :syn "velt med kajakk"} {:event "Skade", :direction :out, :id 169, :type :repair, :time "08:30", :syn nil} {:event "Skade", :direction :in, :id 168, :type :repair, :time "06:15", :syn "Skade på kjøl"} {:event "Utleie", :direction :in, :id 167, :type :rent, :time "11:30", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "12:30", :syn "skade på brygga"} {:event "Skade", :direction :out, :id 165, :type :repair, :time "03:15", :syn nil} {:event "Utleie", :direction :in, :id 164, :type :rent, :time "12:15", :syn nil} {:event "Skade", :direction :in, :id 163, :type :repair, :time "12:00", :syn "Skade på kjøl"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "12:00", :syn "episode med gjester"} {:event "Skade", :direction :out, :id 161, :type :repair, :time "07:00", :syn nil} {:event "Utleie", :direction :out, :id 160, :type :rent, :time "10:45", :syn nil} {:event "Skade", :direction :out, :id 159, :type :repair, :time "11:30", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "02:45", :syn "nøkkelvakt episode"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "07:45", :syn "episode med gjester"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "12:15", :syn "skade på brygga"} {:event "Utleie", :direction :out, :id 155, :type :rent, :time "11:45", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "05:00", :syn "velt med kajakk"} {:event "Utleie", :direction :in, :id 153, :type :rent, :time "05:30", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "06:15", :syn "velt i kano"} {:event "Skade", :direction :out, :id 151, :type :repair, :time "06:15", :syn nil} {:event "Skade", :direction :in, :id 150, :type :repair, :time "07:15", :syn "Skade på kjøl"} {:event "Utleie", :direction :out, :id 149, :type :rent, :time "11:30", :syn nil} {:event "Skade", :direction :in, :id 148, :type :repair, :time "11:30", :syn "Skade på kjøl"} {:event "Skade", :direction :out, :id 147, :type :repair, :time "04:00", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "11:15", :syn "velt i kano"} {:event "Skade", :direction :in, :id 145, :type :repair, :time "10:30", :syn "Skade på lakk"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "12:30", :syn "drukning"} {:event "Skade", :direction :out, :id 143, :type :repair, :time "11:30", :syn nil} {:event "Utleie", :direction :out, :id 142, :type :rent, :time "02:15", :syn nil} {:event "Utleie", :direction :out, :id 141, :type :rent, :time "05:00", :syn nil} {:event "Skade", :direction :out, :id 140, :type :repair, :time "04:45", :syn nil} {:event "Skade", :direction :in, :id 139, :type :repair, :time "05:00", :syn "Skade på kjøl"} {:event "Skade", :direction :out, :id 138, :type :repair, :time "11:15", :syn nil} {:event "Skade", :direction :in, :id 137, :type :repair, :time "05:45", :syn "Skade på kjøl"} {:event "Utleie", :direction :out, :id 136, :type :rent, :time "03:45", :syn nil} {:event "Utleie", :direction :in, :id 135, :type :rent, :time "01:30", :syn nil} {:event "Skade", :direction :in, :id 134, :type :repair, :time "02:30", :syn "Skade på lakk"} {:event "Skade", :direction :out, :id 133, :type :repair, :time "04:30", :syn nil} {:event "Skade", :direction :in, :id 132, :type :repair, :time "11:15", :syn "Skade på kjøl"} {:event "Skade", :direction :out, :id 131, :type :repair, :time "07:00", :syn nil} {:event "Utleie", :direction :out, :id 130, :type :rent, :time "04:15", :syn nil} {:event "Skade", :direction :in, :id 129, :type :repair, :time "03:30", :syn "Skade på kjøl"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "03:30", :syn "skade på brygga"} {:event "Utleie", :direction :in, :id 127, :type :rent, :time "03:00", :syn nil} {:event "Skade", :direction :out, :id 126, :type :repair, :time "04:30", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "12:15", :syn "velt i kano"} {:event "Utleie", :direction :out, :id 124, :type :rent, :time "07:45", :syn nil} {:event "Utleie", :direction :in, :id 123, :type :rent, :time "08:15", :syn nil} {:event "Utleie", :direction :out, :id 122, :type :rent, :time "06:00", :syn nil} {:event "Skade", :direction :out, :id 121, :type :repair, :time "03:45", :syn nil} {:event "Utleie", :direction :in, :id 120, :type :rent, :time "06:45", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "07:15", :syn "drukning"} {:event "Utleie", :direction :in, :id 118, :type :rent, :time "05:45", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "03:15", :syn "drukning"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "12:45", :syn "skade på brygga"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "08:45", :syn "velt i kano"} {:event "Skade", :direction :in, :id 114, :type :repair, :time "10:15", :syn "Skade på kjøl"} {:event "Skade", :direction :in, :id 113, :type :repair, :time "05:15", :syn "Skade på lakk"} {:event "Utleie", :direction :in, :id 112, :type :rent, :time "01:15", :syn nil} {:event "Utleie", :direction :out, :id 111, :type :rent, :time "07:45", :syn nil} {:event "Skade", :direction :out, :id 110, :type :repair, :time "03:30", :syn nil} {:event "Skade", :direction :out, :id 109, :type :repair, :time "02:00", :syn nil} {:event "Utleie", :direction :out, :id 108, :type :rent, :time "09:45", :syn nil} {:event "Utleie", :direction :out, :id 107, :type :rent, :time "03:15", :syn nil} {:event "Skade", :direction :out, :id 106, :type :repair, :time "11:15", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "03:15", :syn "episode med gjester"} {:event "Skade", :direction :out, :id 104, :type :repair, :time "09:30", :syn nil} {:event "Skade", :direction :out, :id 103, :type :repair, :time "03:00", :syn nil} {:event "Skade", :direction :out, :id 102, :type :repair, :time "02:45", :syn nil} {:event "Skade", :direction :out, :id 101, :type :repair, :time "11:15", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "05:45", :syn "velt med kajakk"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "10:00", :syn "episode med gjester"} {:event "Utleie", :direction :out, :id 98, :type :rent, :time "08:15", :syn nil} {:event "Utleie", :direction :out, :id 97, :type :rent, :time "12:15", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "02:00", :syn "nøkkelvakt episode"} {:event "Skade", :direction :out, :id 95, :type :repair, :time "02:30", :syn nil} {:event "Skade", :direction :in, :id 94, :type :repair, :time "04:15", :syn "Skade på kjøl"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "04:45", :syn "episode med gjester"} {:event "Skade", :direction :out, :id 92, :type :repair, :time "07:30", :syn nil} {:event "Skade", :direction :in, :id 91, :type :repair, :time "03:00", :syn "Skade på kjøl"} {:event "Skade", :direction :out, :id 90, :type :repair, :time "03:30", :syn nil} {:event "Utleie", :direction :in, :id 89, :type :rent, :time "12:30", :syn nil} {:event "Utleie", :direction :out, :id 88, :type :rent, :time "11:15", :syn nil} {:event "Skade", :direction :in, :id 87, :type :repair, :time "11:45", :syn "Skade på lakk"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "09:00", :syn "velt i kano"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "10:00", :syn "skade på brygga"} {:event "Utleie", :direction :in, :id 84, :type :rent, :time "05:00", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "08:15", :syn "episode med gjester"} {:event "Skade", :direction :in, :id 82, :type :repair, :time "06:30", :syn "Skade på lakk"} {:event "Skade", :direction :in, :id 81, :type :repair, :time "03:15", :syn "Skade på lakk"} {:event "Skade", :direction :out, :id 80, :type :repair, :time "10:45", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "04:15", :syn "episode med gjester"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "06:00", :syn "nøkkelvakt episode"} {:event "Utleie", :direction :out, :id 77, :type :rent, :time "08:45", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "12:45", :syn "velt med kajakk"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "08:15", :syn "drukning"} {:event "Utleie", :direction :out, :id 74, :type :rent, :time "08:30", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "08:00", :syn "episode med gjester"} {:event "Utleie", :direction :in, :id 72, :type :rent, :time "04:15", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "07:15", :syn "episode med gjester"} {:event "Skade", :direction :out, :id 70, :type :repair, :time "08:00", :syn nil} {:event "Skade", :direction :out, :id 69, :type :repair, :time "03:15", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "07:45", :syn "velt med kajakk"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "03:45", :syn "episode med gjester"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "08:00", :syn "velt med kajakk"} {:event "Skade", :direction :out, :id 65, :type :repair, :time "05:45", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "04:00", :syn "nøkkelvakt episode"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "11:45", :syn "velt med kajakk"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "08:15", :syn "episode med gjester"} {:event "Utleie", :direction :in, :id 61, :type :rent, :time "12:00", :syn nil} {:event "Utleie", :direction :in, :id 60, :type :rent, :time "06:15", :syn nil} {:event "Utleie", :direction :out, :id 59, :type :rent, :time "02:00", :syn nil} {:event "Skade", :direction :in, :id 58, :type :repair, :time "10:00", :syn "Skade på lakk"} {:event "Skade", :direction :out, :id 57, :type :repair, :time "03:45", :syn nil} {:event "Utleie", :direction :in, :id 56, :type :rent, :time "01:30", :syn nil} {:event "Skade", :direction :out, :id 55, :type :repair, :time "11:45", :syn nil} {:event "Skade", :direction :in, :id 54, :type :repair, :time "09:00", :syn "Skade på kjøl"} {:event "Skade", :direction :out, :id 53, :type :repair, :time "05:45", :syn nil} {:event "Skade", :direction :in, :id 52, :type :repair, :time "07:00", :syn "Skade på kjøl"} {:event "Skade", :direction :in, :id 51, :type :repair, :time "03:45", :syn "Skade på kjøl"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "11:15", :syn "nøkkelvakt episode"} {:event "Utleie", :direction :out, :id 49, :type :rent, :time "02:30", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "11:00", :syn "nøkkelvakt episode"} {:event "Skade", :direction :out, :id 47, :type :repair, :time "10:00", :syn nil} {:event "Utleie", :direction :out, :id 46, :type :rent, :time "07:00", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "05:30", :syn "drukning"} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "10:45", :syn "episode med gjester"} {:event "Skade", :direction :in, :id 43, :type :repair, :time "11:00", :syn "Skade på lakk"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "09:30", :syn "nøkkelvakt episode"} {:event "Utleie", :direction :in, :id 41, :type :rent, :time "06:00", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "11:45", :syn "nøkkelvakt episode"} {:event "Utleie", :direction :out, :id 39, :type :rent, :time "04:00", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "12:45", :syn "episode med gjester"} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "01:15", :syn "velt med kajakk"} {:event "Skade", :direction :out, :id 36, :type :repair, :time "10:30", :syn nil} {:event "Utleie", :direction :out, :id 35, :type :rent, :time "05:15", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "02:15", :syn "drukning"} {:event "Utleie", :direction :out, :id 33, :type :rent, :time "11:00", :syn nil} {:event "Hendelse", :direction :out, :id :hms, :type :registration, :time "06:15", :syn "skade på brygga"} {:event "Skade", :direction :out, :id 31, :type :repair, :time "07:30", :syn nil} {:event "Hendelse", :direction :in, :id :hms, :type :registration, :time "08:30", :syn "velt med kajakk"} {:event "Skade", :direction :in, :id 29, :type :repair, :time "12:00", :syn "Skade på lakk"} {:event "Skade", :direction :out, :id 28, :type :repair, :time "06:45", :syn nil} {:event :rent, :time "05:45", :content {:id 0, :status :out, :havekey true, :stat {}}} {:event :rent, :time "03:45", :content {:id 1, :status :out, :havekey true, :stat {}}} {:event :rent, :time "05:30", :content {:id 2, :status :out, :havekey true, :stat {}}} {:event :rent, :time "11:45", :content {:id 3, :status :out, :havekey true, :stat {}}} {:event :rent, :time "12:32", :content {:id 103, :status :out, :havekey true, :stat {:age 0, :gender 1, :extra 3}}} {:event :rent, :time "13:00", :content {:id 104, :status :out}} {:event :rent, :time "13:200", :content {:id 109, :status :out, :havekey true, :stat {:age 0, :gender 1, :extra 2}}} {:event :rent, :time "13:200", :content {:id 109, :status :in}} {:event :repair, :time "13:20", :content {:status :in, :id 109, :syn "Something went wrong"}} {:event :rent, :time "13:01", :content {:id 104, :status :out}} {:event :rent, :time "13:01", :content {:id 304, :status :out}} {:event :rent, :time "13:04", :content {:id 305, :status :out}} {:event :rent, :time "13:21", :content {:id 308, :status :out}} {:event :rent, :time "14:14", :content {:id 464, :status :out}} {:event :rent, :time "14:14", :content {:id 105, :status :in}} {:event :repair, :time "14:00", :content {:status :in, :id 103, :syn "Skade på ∆"}} {:event :repair, :time "16:00", :content {:status :in, :id 105, :syn "Skade på lakken"}} {:event :repair, :time "16:20", :content {:status :in, :id 106, :syn "Skade på lakken"}} {:event :register, :time "13:00", :content {:status :hms, :syn "kort beskrivelse her"}} {:event :register, :time "13:20", :content {:status :hms, :syn "skade på brygga"}} {:event :rent, :time "14:12", :content {:id 103, :status :in}} {:event :repair, :time "21:10", :content {:id 103, :status :out, :syn "limte to ganger"}} {:event :repair, :time "21:12", :content {:status :in, :id 104, :syn "skade"}} {:event :repair, :time "08:30", :content {:status :in, :id 412, :syn "skade i front, mulig hull"}} {:event :repair, :time "10:15", :content {:status :in, :id 413, :syn "skade i front, mulig hull"}} {:event :repair, :time "06:45", :content {:status :in, :id 414, :syn "skade i front, mulig hull"}} {:event :repair, :time "08:30", :content {:status :in, :id 416, :syn "skade i front, mulig hull"}} {:event :repair, :time "22:30", :content {:id 104, :status :out, :syn "limte tre ganger"}}),
;     :db/stat {101 {:age 0, :gender 1}, 102 {:age 3, :gender 0, :havekey true},
;               105 {:age 2, :gender 0, :havekey true}}, :db/ui-toggle-groovebar false, :db/ui-toggle-help true})
;
;(comment
;  (println
;   (let [e [:events.type/event1 :event/rent 1 6]]
;     (match [e]
;       [[:events.type/event2 x 1 4]] (str "ok3 " x)
;       [[_ 's 1 6]] (str "ok2")
;       [[:events.type/event1 x _ _]] (str "ok1 " x)
;       :else "not"))))

(defn dialog [s]
  [:div.fixed.pin-t.pin-b.pin-l.pin-r.z-50
   {:style {:background "rgba(0,0,0,0.9)"}}
   [:div.absolute.border-2.border-black.overflow-hidden.bg-white.rounded.shadow-lg
    (merge-with
      conj
      {:style {:top "0%" :left "50%" :-webkit-transform "translateX(-50%) translateY(-2px)"}}
      {:class "w-1/3"}
      {:style {:min-width "480px"}})
    [:div.h-full.bg-red-.px-4.py-2 [:div.flex-col.flex.justify-end s]]]])

(def button-class
  {:style {:min-width "120px"}
   :class "rounded border-2 border-black px-4 py-2 bg-grey"})

;(def ctg (aget js/React "addons" "CSSTransitionGroup"))

;;
;;
;;
;;

(rf/reg-event-db
  :modal/click
  (fn [db [_ new-value]]
    (-> db
      (assoc-in [:modal :visible] false)
      (assoc-in [:modal :value] new-value))))

(rf/reg-sub
  :modal/value
  (fn [db _]
    (-> db :modal :value)))

(rf/reg-event-db
  :modal/visible
  (fn [db [_ arg]]
    (if arg (assoc-in db [:modal :visible] arg))
    (assoc-in db [:modal :visible] arg)))

(rf/reg-sub
  :modal/visible
  (fn [db _]
    (-> db :modal :visible)))

;;
;;
;;
;;

(defn render []
  (fn []
    [:div
       [:div.bg-red.p-1.mx-10
        (merge-with
          conj
          {:style {:font-size "2rem"}}
          {:style {:color :white}}
          (background thistle))
        (str @(rf/subscribe [:modal/value]))
        [:button (merge button-class {:on-click #(rf/dispatch [:modal/visible true])}) "Open"]
        [:button (merge button-class {:on-click #(rf/dispatch [:modal/visible false])}) "Close"]]
       (into [:div] (map (fn [e] [:p e]) (range 100)))
       (if @(rf/subscribe [:modal/visible])
         [dialog [:div.flex.flex-col.justify-between          ;.h-32
                  [:h1.text-center.mb-4 "Heading"]
                  [:p.text-center "Heading"]
                  [:p.text-center "Heading"]
                  [:p.text-center "Heading"]
                  [:div.flex.justify-between
                   [:button (merge button-class {:on-click #(rf/dispatch [:modal/click :ok-pressed])}) "Ok"]
                   [:button (merge button-class {:on-click #(rf/dispatch [:modal/click :cancel-pressed])}) "Cancel"]]]])]))