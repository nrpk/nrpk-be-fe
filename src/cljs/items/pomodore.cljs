(ns items.pomodore
  (:require [re-frame.core :as rf]
            [clojure.pprint :refer [pprint]]
            [cljs-time.core :as t]
            [items.core :refer [row input button]]
            [diagram.core]
            [arcs.dial-definitions :as dial-def]
            [nv.ui :as ui]
            [reagent.core :as r]
            [yd.core :as yd]
            [items.yearwheel :as yearwheel]
            [diagram.yearwheel.definitions]
            [diagram.pomodore.definitions :as definitions]))

(defn circle-diagram [{:keys [key-id size draw-fn config zero]
                       :or {size [320 320]
                            zero  0
                            draw-fn diagram.core/draw-arc}}]
  (fn []
    (let [[w h] size]
      [:svg {:key                 key-id
             :stroke-linecap      'round
             :width               "100%"
             :height              "auto"
             :view-box            (str "0 0 " w " " h)
             :preserveAspectRatio "xMidYMid meet"}
       (draw-fn {:cx    (/ w 2)
                 :cy    (/ h 2)
                 :theta [0 359]
                 :r     [90 100]
                 :zero  zero}
                config)])))

(def timer-interval-ms 1000)
(def global-timer-atom (atom nil))

(def global-timermonitor-atom (atom nil))

(def timer-func
  #(do
     ;(prn "global-timermonitor-atom " global-timermonitor-atom)
     (rf/dispatch [:pom/set-current-time (t/now)])))

(defn debug-db [db]
  (str (select-keys db [:db.pom/length :db.pom/tick :db.pom/task-description :db.pom/timer-running :db.pom/start-time])))

(rf/reg-fx :fx-start-timer
           (fn [[f timer interval-ms]]
             (prn "before" :fx-start-timer timer)
             (if @timer
               (js/clearInterval @timer))
             (reset! timer (js/setInterval f interval-ms))
             (prn "after" :fx-start-timer timer)))

(rf/reg-fx :fx-stop-timer
           (fn [[timer]]
             (prn "before" :fx-stop-timer timer)
             (js/clearInterval @timer)
             (reset! timer nil)
             (prn "after" :fx-stop-timer timer)))

(rf/reg-event-fx :pom/start-timer-monitor
                 (fn [{db :db} [_ timer]]
                   (prn :pom/start-timer-monitor)
                   {:db db
                    :fx-start-timer [timer-func timer timer-interval-ms]}))

(rf/reg-sub :sub.pom/remaining (fn [db] (:db.pom/tick db 0)))

(rf/reg-sub :sub.pom/tasks (fn [db] (:db.pom/tasks db)))

(rf/reg-sub :sub.pom/task-description (fn [db] (:db.pom/task-description db)))

(rf/reg-sub :sub.pom/timer-running (fn [db] (:db.pom/timer-running db)))

(rf/reg-sub :sub.pom/length (fn [db] (:db.pom/length db)))

(rf/reg-sub :sub.pom/allowed-to-start? (fn [db] (or
                                                 (not (empty? (:db.pom/tasks db)))
                                                 (and (not (empty? (:db.pom/task-description db)))
                                                      (empty? (:db.pom/tasks db))))))

(rf/reg-sub :sub.pom/start-time (fn [db] (:db.pom/start-time db (t/now))))

(rf/reg-sub :sub.pom/current-time (fn [db] (:db.pom/current-time db (t/now))))

(rf/reg-sub :sub.pom/toggle-view-model (fn [db] (:db.pom/toggle-view-model db false)))

(rf/reg-event-db :pom/set-current-time (fn [db [_ time]]
                                         (assoc db :db.pom/current-time time)))

(rf/reg-event-db :pom/clear-all
                 (fn [db [_]]
                   (assoc db :db.pom/tasks [])))

(rf/reg-event-db :pom/capture-task-description
                 (fn [db [_ description]]
                   (prn "Later dispatch" :pom/capture-task-description)
                   (-> db
                       (update :db.pom/tasks conj {:start-time (:db.pom/start-time db)
                                                   :length      (:db.pom/length db)
                                                   :description (if (empty? description)
                                                                  (:description (first (:db.pom/tasks db)))
                                                                  description)})
                       (assoc :db.pom/task-description ""))))

(rf/reg-event-db :pom/toggle-view-model
                 (fn [db _]
                   (update db :db.pom/toggle-view-model not)))

(rf/reg-event-fx :pomodore/stop
                 (fn [{db :db} [_ timer]]
                   (let [description (:db.pom/task-description db)]
                     (prn :pomodore/stop (debug-db db))
                     {:fx-stop-timer [timer]
                      :dispatch      [:pom/capture-task-description description]
                      :db            (assoc db :db.pom/timer-running false)})))

(rf/reg-event-fx :pomodore/clear
                 (fn [{db :db} [_ timer]]
                   {:fx-stop-timer [timer]
                    :db            (assoc db
                                          :db.pom/tasks []
                                          :db.pom/length 0
                                          :db.pom/timer-running false
                                          :db.pom/tick 0)}))

(rf/reg-event-fx :pomodore/start
                 (fn [{db :db} [_ timer interval-ms]]
                   {:fx-start-timer [#(do (prn "NORMAL TICK")
                                          (rf/dispatch [:timer-tick timer])) timer interval-ms]
                    ;:fx-stop-timer  [global-timermonitor-atom]
                    :db             (assoc db
                                           :db.pom/start-time (t/now)
                                           :db.pom/timer-running true
                                           :db.pom/tick 1)}))

(rf/reg-event-db :pomodore/set-length
                 (fn [db [_ length]]
                   (assoc db
                          :db.pom/length length)))

(rf/reg-event-fx :timer-tick (fn [{db :db} [_ timer]]
                               (prn "tick tock " (select-keys db [:db.pom/length :db.pom/timer-running :db.pom/tick :timer-tick]))
                               (if (< (:db.pom/tick db) (:db.pom/length db))
                                 {:db (update db :db.pom/tick inc)}
                                 {:db db
                                  :dispatch [:pomodore/stop timer]})))

;;
;;
;;

(rf/reg-event-db :pom/task-description (fn [db [_ msg]] (assoc db :db.pom/task-description msg)))

(def grid-by-2
  {:style {:display               :grid
           :grid-template-columns "repeat(2,minmax(160px,1fr))"
           :grid-gap              4
           :class                 [:bg-gray-800 :py-1 :w-full]}})

(def grid-by-4
  {:style {:display               :grid
           :grid-template-columns "repeat(auto-fill,minmax(260px,1fr))"
           ;:grid-template-rows    "auto"
           :grid-gap              4
           :class                 [:bg-gray-800 :py-1 :w-full]}})

(def grid-by-5-sm
  {:style {:display               :grid
           :grid-template-columns "repeat(5,minmax(32px,64px))"
           ;:grid-auto-column "minmax(32px,64px)"
           :grid-gap              4
           :class                 [:bg-gray-800 :py-1 :w-full]}})

(defn options [{:keys [timer values allowed? length running?]}]
  (let [allowed? (rf/subscribe [:sub.pom/allowed-to-start?])]
    [:div grid-by-5-sm
     (doall (for [each-length values]
              [:button {:key      (str "length-" each-length)
                        :disabled (not @allowed?)
                        :class    (flatten [:flex-grow
                                            (if (= each-length @length)
                                              [:bg-yellow-500 :text-black] [])
                                            (if @allowed? [(ui/pseudo "hover" :text-gray-100.border-gray-100)
                                                           :border-gray-500 :border :rounded-sm]
                                                [:bg-gray-700x :border-gray-900 :border :rounded-sm :text-gray-900])])
                        :on-click #(rf/dispatch [:pomodore/set-length each-length timer])} each-length]))
     (if @running?
       [:button {:class    [:border :rounded-sm]
                 :style {:grid-column "span 2/-1"}
                 :on-click #(rf/dispatch [:pomodore/stop global-timer-atom])}
        "Finish now"]
       [:button {:class    [:border :rounded-sm]
                 :style {:grid-column "span 2/-1"}
                 :on-click #(rf/dispatch [:pomodore/start global-timer-atom timer-interval-ms])}
        "Start now"])]))

(defn clock [{:keys [total-today all-tasks current-time]}
             {:keys [remaining length]}]
  (let [model-view? (rf/subscribe [:sub.pom/toggle-view-model])
        remaining (- 1 (/ remaining length))
        length (* 360 (/ length 60))
        start (* 6 (t/second current-time))
        prefs [(dial-def/construct-currentrunner {:remaining remaining
                                                  :length          length
                                                  :start           start})
               (dial-def/construct-inner-hourly-cfg {:all-tasks all-tasks
                                                     :remaining    total-today
                                                     :length'       359.99
                                                     :start        270})
               (dial-def/construct-inner-second-cfg {:all-tasks all-tasks
                                                     :remaining    total-today
                                                     :length'       359.99
                                                     :start        0})]]
        ; (* 360 (/ (y/current-seconds time) 60))]

    [:div.flex.flex-col
     [:div "Model view " (str @model-view?)]
     [:div "Remaining " (str remaining)]
     ;{:key "clock"}
     [:div.flex-grow
      (circle-diagram {:key-id "CLOCK-0"
                       :config prefs})]
     (when @model-view? [:div.flex-grow  [:div.scroll-y.flex-grow.bg-gray-800
                                          [:pre.text-xxs (with-out-str (pprint (drop 1 prefs)))]]])]))

(comment
  (do
    (reduce (fn [a e] (conj a [(apply + (last a)) e])) []
            (map :length [{:length 20} {:length 60} {:length 20}]))))

(defn clock-container [{:keys [all-tasks remaining length current-time]}]
  (clock
   {:current-time @current-time
    :total-today  1
    :all-tasks    all-tasks}
   {:remaining @remaining
    :length    @length
    :time      (t/now)}))

(defn taskdescription-input [tasks]
  (let [options {:on-change   #(rf/dispatch [:pom/task-description (-> % .-target .-value)])
                 :value       @(rf/subscribe [:sub.pom/task-description])
                 :placeholder (str "Describe the task" (if-let [t (:description (last @tasks))] (str " or continue \"" t "\"") ""))
                 :class       [:py-1 :px-2 :bg-gray-200 :rounded-sm :border-none :w-full]
                 :style       {:xwidth     " 100%"
                               :xmax-width " 100%"}}]
    [:input options]))

(defn header-container [{:keys [all-tasks model-view?]}]
  [:div.flex.justify-between.py-1.items-center
   [:div.text-base.bg-gray-900.px-2.py-1 (str all-tasks)]
   [:div.h-6 {:style {:display :grid
                      :grid-gap 4
                      ;:grid-template-columns "90px 90px"
                      :grid-template-rows "autofill"
                      :grid-template-columns "90px 90px"}}

    [:div.text-xxs {:class    (flatten [:shadow :px-2 :py-1 :rounded-sm :text-center
                                        (if @model-view? [:bg-blue-800 :border-blue-800 :border]
                                            [:bg-clear :border :border-white])])
                    :on-click #(rf/dispatch [:pom/toggle-view-model])}
     "Toggle model"]

    [:div.text-xxs {:class    [:bg-red-500 :shadow :px-2 :py-1 :rounded-sm :text-center]
                    :on-click #(rf/dispatch [:pom/clear-all])}
     "Clear All"]]])

(defn pomodore-controls [{:as opts :keys [tasks allowed? length running?]}]
  [:div.flex-grow (taskdescription-input tasks)
   [:div.my-1 {:style {:display               :grid
                       :grid-template-columns "repeat(5,minmax(auto-fit,minmax(50px,1fr)) 1fr 1fr"
                       :grid-gap              4
                       :class                 [:bg-gray-800 :py-1 :w-full]}}
    (options (merge opts {:timer    global-timer-atom
                          :values   [3 5 25 45 55]}))
    #_[:button {:class [:border :rounded-sm]
                :on-click #() #_(if @running?
                                  #(rf/dispatch [:pomodore/stop global-timer-atom])
                                  #(rf/dispatch [:pomodore/clear global-timer-atom]))}
       label]]

   [:div.bg-gray-900.p-2.flex-col.flex.border.scroll-y
    (for [[i e] (map-indexed vector (reverse @tasks))]
      [:div.flex.justify-between
       {:key i}
       [:p (:description e)]
       [:div.flex
        [:p (str (some-> (:start-time e) yd/time-str))]
        [:p.w-10.text-right (:length e)]]])]])

(defn pomodore' []
  (let [opts {:running?     (rf/subscribe [:sub.pom/timer-running])
              :allowed?     (rf/subscribe [:sub.pom/allowed-to-start?])
              :length       (rf/subscribe [:sub.pom/length])
              :current-time (rf/subscribe [:sub.pom/current-time])
              :tasks        (rf/subscribe [:sub.pom/tasks])
              :model-view?  (rf/subscribe [:sub.pom/toggle-view-model])
              :start-time   (rf/subscribe [:sub.pom/start-time])
              :remaining    (rf/subscribe [:sub.pom/remaining])
              :all-tasks    (reduce (fn [a e]
                                      (conj a [(apply + (last a)) e]))
                                    []
                                    (map :length @(rf/subscribe [:sub.pom/tasks])))}

        label (if (:running? opts) "Stop" (if (zero? (:length opts)) "Start" "Another"))]
    [:div.p-2.shadow-lg.bg-gray-700.select-none.h-full
     (header-container opts)
     (ui/header "Pomodore " [:span.text-yellow-500 (yd/long-time-str (t/now))])
     [:div grid-by-2
      (clock-container opts)
      (pomodore-controls opts #_{:allowed? allowed? :tasks tasks :length length :running? running?})]

     #_[:pre.py-1.bg-yellow-700.text-xxs "remaining: " @remaining ", length: " @length]]))

(def setup-1
  [{:class      [:text-gray-700]
    :insets     [0 0]
    :key-id     "current-runner"
    :r          [60 100]
    :theta      [0 359.9]
    :style      {:fill 'none}
    #_{:fill 'red :stroke 'green}
    :texts      [{:key-id   "t0"
                  :stepsize (/ 360 12)
                  :class    [:text-green-200]
                  :insets   [0.1 0.1]
                  :theta    [0 91]
                  :r        [80]
                  :draw-fn  (fn [x y n] (let [font-size 4
                                              y-adjust (/ font-size 2.8)]
                                          [:g {:transform (str "rotate(" n " " x " " y ")")}
                                           [:text {:x x :y y :dy (- y-adjust) :fill 'currentColor :text-anchor :middle :font-size font-size}
                                            (/ n 30)]]))
                  :style    {:stroke-width 1}}
                 {:key-id   "t1"
                  :stepsize (/ 360 24)
                  :class    [:text-orange-500]
                  :insets   [0.1 0.1]
                  :r        [100 100]
                  :draw-fn  (fn [x y n] (let [font-size 5
                                              y-adjust (/ font-size 2.8)
                                              [r dy] (cond
                                                       (= n 90) [-90 y-adjust]
                                                       (= n 270) [90 y-adjust]
                                                       (= n 180) [90 0]
                                                       (< 0 n 90) [-90 y-adjust]
                                                       (< 90 n 180) [-90 y-adjust]
                                                       (< 180 n 270) [90 y-adjust]
                                                       (< 270 n 360) [90 y-adjust]
                                                       :else [-90 0])]

                                          [:g {:transform (str "rotate(" (+ n r) " " x " " y ")")}

                                           [:text {:x x :y y :dy dy :fill 'currentColor :text-anchor :middle :font-size font-size} n]]))
                  :style    {:stroke-width 1}}]
    :ticks      [{:key-id   "t1"
                  :theta    [-90 90]
                  :stepsize (/ 360 60)
                  :class    [:text-yellow-500]
                  :insets   [0 0.95]
                  :style    {:stroke-width 0.5}}
                 {:key-id   "t1"
                  :theta    [90 180]
                  :r        [50 60]
                  :stepsize (/ 360 60)
                  :class    [:text-yellow-500]
                  :insets   [0.90 0]
                  :style    {:stroke-width 0.5}}]
    :widgets    [{:key-id   "w1"
                  :r        [40 50]
                  :kind     :triangle
                  ;:rotation-fn #(+ 180 %)
                  :style    {:stroke-width   1
                             :stroke-linecap 'butt
                             :fill           'blue
                             :stroke         'white}
                  :position (+ (/ (yd/current-hours (t/now)) 12)
                               (/ (/ (yd/current-minutes (t/now)) 60) 12))}
                 {:key-id   "w1.1"
                  :r        [30 40]
                  :kind     :circle
                  :style    {:stroke-width   1
                             :stroke-linecap 'butt
                             :fill           'blue
                             :stroke         'white}
                  :position (+ (/ (yd/current-hours (t/now)) 12)
                               (/ (/ (yd/current-minutes (t/now)) 60) 12))}
                 {:key-id   "w2"
                  :r        [10 50]
                  ;:rotation-fn #(* % 20)
                  :kind     :line
                  :style    {:stroke-width   1
                             :stroke-linecap 'butt
                             :fill           'red
                             :stroke         "#ff0"}
                  :position (/ (yd/current-minutes (t/now)) 60)}
                 {:key-id   "w3"
                  :r        [10 30]
                  ;:rotation-fn #(* % 20)
                  :kind     :line
                  :style    {:stroke-width   4
                             :stroke-linecap 'round
                             :fill           'red
                             :stroke         "#f0f"}
                  :position (/ (yd/current-seconds (t/now)) 60)}]
    :value-bars [{:key-id "vb1"
                  :style  {:stroke-width   2
                           :stroke-linecap 'round
                           :stroke         'currentColor
                           :fill           'none #_"#aef"}
                  :class  [:text-green-500]
                  :insets [0.2 0.3]
                  ;:r      [90]
                  :kind   :fill
                  :theta  [0 180]
                  :value  {:start 0.1 :end 0.1}}

                 {:key-id "vb1"
                  :style  {:stroke-width   2
                           :stroke-linecap 'round
                           :stroke         'currentColor
                           :fill           'none #_"#aef"}
                  :class  [:text-green-500]
                  :insets [0.2 0.3]
                  ;:r      [90]
                  :kind   :fill
                  :theta  [0 180]
                  :value  {:start 0.1 :end 0.1}}

                 {:key-id "vb1.1"
                  :style  {:stroke-width   1
                           :stroke-linecap 'round
                           :stroke         'currentColor
                           :fill           'none #_"#aef"}
                  :class  [:text-purple-500]
                  ;:insets [0.2 0.3]
                  ;:r     [90]
                  :kind   :stroke
                  :theta  [10 80]
                  :value  {:start 0 :end 0.1}}

                 {:style {:stroke-width   5
                          :stroke-linecap 'round
                          :stroke         'red
                          :fill           'none}
                  :class [:text-pink-300]
                  :kind  :stroke
                  :r     [110]
                  :theta [80 360]
                  :value {:start 0 :end 0}}

                 {:style {:stroke-width   3
                          :stroke-linecap 'round
                          :stroke         'white
                          :fill           'none}
                  :class [:text-pink-300]
                  :kind  :stroke
                  :r     [110]
                  :theta [80 360]
                  :value {:start 0.2 :end 0.5}}

                 {:style {:stroke-width   3
                          :stroke-linecap 'round
                          ;:stroke         'white
                          :fill           'none}
                  :class [:text-blue-400]
                  :kind  :stroke
                  :r     [110]
                  :theta [80 360]
                  :value {:start 0.7 :end 0.1}}

                 {:key-id "something-123"
                  :style  {:stroke-width   0
                           :stroke-linecap 'butt
                           ;:stroke         'white
                           :fill           "#a708"}
                  :class  [:text-blue-900]
                  :kind   :fill
                  :r      [90 120]
                  :theta  [300 360]
                  :value  {:start 0 :end 0}}

                 {:key-id "something-1234"
                  :style  {:stroke-width   1
                           :stroke-linecap 'round
                           ;:stroke         'none
                           :fill           "#0003"}
                  :class  [:text-pink-300]
                  :kind   :fill
                  :r      [90 110]
                  :theta  [195 255]
                  :value  {:start 0 :end 0}}]}])


(defn render []
  [:div.mx-1.bg-gray-900 grid-by-4
   (map #(vector :div.bg-gray-800.shadow-6.snap-start [%])
        [(circle-diagram {:key-id "ROOT-2"
                          :config definitions/simple2})

         yearwheel/rendered

         (circle-diagram {:key-id "ROOT-0"
                          :config (definitions/pomodore' "abcd")})

         (circle-diagram {:key-id "ROOT-1"
                          :config definitions/simple})

         (circle-diagram {:config (definitions/pomodore 0)})
         (circle-diagram {:config (definitions/pomodore 0)})])])

;(rf/dispatch-sync [:pom/start-timer-monitor global-timermonitor-atom])

