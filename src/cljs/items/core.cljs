(ns items.core
  (:require [re-frame.core :as rf]
            [clojure.pprint :refer [pprint]]
            [nv.ui :as ui]))

(defn row
  ([c & xs]
   [:div (merge-with conj
                     c
                     {:class [:py-4 :bg-orange-400]
                      :style {:display           :grid
                              :grid-auto-columns "auto"
                              :grid-auto-flow    :column
                              :gap               "4px 4px"}})
    xs]))

(defn button [x c]
  [:button.select-none.xbg-gray-200.xtext-gray-800.font-black.rounded.border-gray-400.xbg-gray-400.hover:bg-gray-900.hover:text-white
   (merge x {:style {:min-width "4rem" :min-height  "4rem"}}) c])

(defn spacer []
  [:div.flex-grow])

(defn air []
  [:div.h-4])

(defn text [s]
  [:div
   [:div  {:style {:margin-right :10vw}
           :class [:tracking-normal
                   ;:mr-12
                   :text-lg
                   :font-base
                   :font-serif
                   :leading-relaxed]}  s]
   (air)])

(defn h1 [s]
  [:p.text-5xl.font-thin.text-gray-900.pb-4 s])

(defn panel []
  [:div.h-screen (h1 "panel")])

(defn overview []
  [:div.bg-blue-300.text-black (h1 "overview")])

(defn grid [& xs]
  [:div.grid
   (map #(vector :div.bg-gray-200 %) xs)])

(defn content [& xs]
  [:div.py-1.sbg-yellow-500.text-gray-800 xs])

(defn ingress [& xs]
  [:div.text-gray-800.text-base.font-serif.xbg-blue-300 xs])

(defn article [xs]
  [:div.bg-green-200.xp-2.py-4
   [:p.text-green-600.font-black.text-xs "Artikkel"]
   xs])

(defn section [& xs]
  [:div.bg-green-200.px-2.py-0 #_[:p.text-red-300.font-black.text-xs "Section"] xs])

(defn input
  ([c]
   (if (map? c)
     (input c "")
     (input {} c)))
  ([c s]
   (if (:skip-label c false)
     [:div.flex

      [:input.h-12.flex.items-center.rounded-sm.bg-white.px-3
       (merge {:id          "input"
               :type        :text}
              c)]
      #_[:div.w-12.h-12.flex.items-center.justify-center.font-black.font-sans "3"]]
     [:label {:style {:_width     "100%"
                      :_max-width "100%"}} "Some label"
      [:input.h-12.rounded-sm.bg-white.py-0.px-3.w-auto.text-black
       (merge {:id          "input"
               :placeholder "placeholder"
               :type        :textbox}
              {:style {:box-sizing :border-box}}
              c)]])))

(defn numbered-box [label v]
  [:div {:class [:mx-1
                 :rounded
                 :select-none
                 :h-16
                 :border-2
                 :border-blue-200
                 :flex
                 :justify-center
                 :items-center
                 :w-full
                 :flex-col

                 :_shadow
                 :_items-center
                 :_py-1
                 :_content-box]}
   [:p.text-xs.text-blue-900.font-bold.opacity-50 label]
   [:p.text-2xl.font-serif.font-black.text-blue-900 v]])


(defn button-tiny [c msg caption]
  [:button
   (merge {:on-click #(rf/dispatch msg)} c)
   caption])

(defn button-pad [msg class caption]
  [:button.rounded-sm.font-black.text-4xl.py-3.shadow-4
   (merge class {:on-click #(rf/dispatch msg)}) caption])

(defn button-pad-clean [msg class caption]
  [:button.w-full.h-full
   {:class    class
    :on-click #(rf/dispatch msg)}
   caption])
