;(ns items.pomodore-definitions
;  (:require [yd.core :as y]
;            [arcs.dial-definitions :as dial-def]
;            [items.yearwheel-definitions]
;            [cljs-time.core :as t]))
;
;(def pomodore
;  [{:key-id     "abc"
;    :theta      [0 359.99]
;    :r          [50 100]
;    :skip       false
;    :style      {:fill   'green
;                 :stroke 'white}
;    :class      :text-red-500}])
;
;(defn config [k draw-fn cfgs]
;  (let [[cx cy] [320 320]
;        c {:cx   (/ cx 2)
;           :cy   (/ cy 2)
;           :zero 0 #_(- -7.5 90 #_(y/date-time->yd (t/now)))}]
;    (dial-def/svg k [cx cy] (draw-fn c cfgs))))
;
;(defn all-hours [{:keys [r]}]
;  (let [items 24]
;    {:value-bars-text
;     (mapv (fn [index]
;             (let [sectors-in-circle (/ 360 items)
;                   sector-size (* index sectors-in-circle)]
;               {:key-id  (str "all-hours-" sectors-in-circle)
;                :r       r
;                :kind    :stroke
;                :draw-fn (items.yearwheel-definitions/draw-fn' {:font-size 10
;                                                                ;:type :ferris
;                                                                :f         (fn [_] [:tspan.font-black (inc index)])})
;                :class   (if (or (<= 0 index 6) (<= 22 index)) :text-red-500 :text-white)
;                ;:color-fn (fn [e] (if (< 0 e 6) :text-red-500 :text-white))
;                :style   {:stroke-width 0.5
;                          :fill         'none
;                          :stroke       'currentColor}
;                :theta   [sector-size (+ sector-size sectors-in-circle)]}))
;           (range items))}))
;
;(defn r-pull [r offset]
;  (map #(- % offset) r))
;
;(defn draw-fn''
;  [{:keys [type i dy f font-size]
;    :or {type :other
;         i 0
;         dy 0
;         font-size 3}}]
;  (fn [{:keys [x y deg key-id path class]}]
;    (let [rotate-fn (case type
;                      :ferris (fn [deg] (- 360 deg))
;                      :quarterly-rotated (fn [deg] (cond (< 0 deg 90) 0
;                                                         (< 90 deg 270) -180
;                                                         ;(< 270 deg 360) -180
;                                                         :else 0))
;                      (fn [_] 0))
;          y-adjust (/ font-size 2.8)]
;      [:g {:class     class
;           :transform (str "rotate(" (rotate-fn (mod deg 360)) "  " x " " y ")")}
;       [:text {:fontSize font-size
;               :dy       (+ dy y-adjust)}
;        [:defs [:path {:id key-id :d path :fill 'red :stroke-width 1 :stroke 'red}]]
;        [:textPath {:href        (str "#" key-id)
;                    :path path
;                    :fill        'currentColor
;                    :textAnchor  :middle
;                    :startOffset :50%}
;         (f deg)]]])))
;
;(defn all-12-hours [{:keys [r theta]}]
;  (let [[r-i r-o] r
;        height (- r-o r-i)
;        items 24
;        round 12
;        step-height (/ height round)
;        sector-size (/ 360 round)]
;     (mapv (fn [index]
;             (let [start (* index sector-size)
;                   end (+ start sector-size)]
;               {:key-id       (str "all-12-hours/" index)
;                :insets       [0.05 0.05]
;                :bar-h-insets [0.02 0.02]
;                :r (r-pull r (- (* step-height index)))
;                #_(if (or (< 3 index 6)
;                          (< 8 index 10)) (r-pull r -10) r)
;                :kind         :fill
;                :draw-fn      (draw-fn''
;                                {:font-size 9
;                                 :dy        -2
;                                 ;:type      :quarterly-rotated
;                                 :f         (fn [deg] [:tspan.font-serif.font-black.text-black deg #_(inc index)])})
;                :class        (if (or (< 50 index 76)
;                                      (< 22 index 32)
;                                      (< 0 index 6)) :text-pink-500 :text-white)
;                ;:color-fn (fn [e] (if (< 0 e 6) :text-red-500 :text-white))
;                :style        {:stroke-width 0.5
;                               :fill         'currentColor
;                               :stroke       'currentColor}
;                :theta        [start end]}))
;           (range 0 items))))
;
;(comment (do (all-12-hours {:sr [50 60] :theta [0 90]})))
;
;(def base {;:key-id "abc2asd"
;           :theta  [30 330]
;           :r      [85 90]
;           :skip   true
;           :style  {:fill   'red
;                    :stroke 'currentColor}
;           :class  [:text-pink-500 :opacity-25]})
;
;(def simple'
;  (merge-with into
;              ;(assoc base :key-id key-id)
;              {:value-bars [{:key-id  "kaldt-i-vannet"
;                             :r       [60 80]
;                             :theta   [100 220]
;                             :dy      20
;                             :class   [:text-green-300 :opacity-75]
;                             :value   {:start 0.0 :end 0.01}
;                             :style   {:stroke-width 0.5
;                                       :fill         'red}
;                             :kind    :fill
;                             :draw-fn (items.yearwheel-definitions/draw-fn "Kaldt i vannet")}]}
;              items.yearwheel-definitions/widgets
;              {:value-bars-text (all-12-hours {:r [50 70]})}
;              #_{:value-bars-text (into [] (flatten (mapv (fn [e] (all-12-hours {:r e}))
;                                                          (mapv vec (partition 2 1 (range 100 136 30))))))}))
;
;(def simple
;  (merge-with into
;              base
;              #_(assoc base :key-id key-id)
;              {:value-bars [{:key-id  "kaldt-i-vannet"
;                             :r       [60 80]
;                             :theta   [100 220]
;                             :dy      20
;                             :class   [:text-green-300 :opacity-75]
;                             :value   {:start 0.0 :end 0.01}
;                             :style   {:stroke-width 0.5
;                                       :fill         'red}
;                             :kind    :fill
;                             :draw-fn (items.yearwheel-definitions/draw-fn "Kaldt i vannet")}]}
;              items.yearwheel-definitions/widgets
;              {:value-bars-text (all-12-hours {:r [80 105]})}
;              #_{:value-bars-text (into [] (flatten (mapv (fn [e] (all-12-hours {:r e}))
;                                                          (mapv vec (partition 2 1 (range 100 136 30))))))}))
;
;(defn all-12-hours' [{:keys [r theta]}]
;  (let [[r-i r-o] r
;        height (- r-o r-i)
;        items 4
;        round 12
;        step-height (/ height round)
;        sector-size (/ 360 round)]
;    (mapv (fn [index]
;            (let [start (* index sector-size)
;                  end (+ start sector-size)]
;              {:key-id  (str "all-12-hours/" index)
;               :insets  [0.05 0.05]
;               ;:bar-h-insets [0.02 0.02]
;               :zero    (* -1 sector-size)
;               :r       (if (< index 1) (r-pull r -13) r)
;               :kind    :fill
;               :draw-fn (draw-fn''
;                          {:font-size 15
;                           :dy        0
;                           :f         (fn [deg] [:tspan.font-sans.font-black.text-black (inc index)])})
;               :class   [:opacity-50 (if (or (< 50 index 76)
;                                             (< 22 index 32)
;                                             (< 0 index 6)) :text-pink-500 :text-white)]
;               ;:color-fn (fn [e] (if (< 0 e 6) :text-red-500 :text-white))
;               :style   {:stroke-width 0.5
;                         :fill         'currentColor
;                         :stroke       'currentColor}
;               :theta   [start end]}))
;          (range items))))
;
;(def simple2 (merge-with into
;                         base
;                         {:value-bars-text [{:key-id  "kaldt-i-vannet"
;                                             :zero 40
;                                             :r       [60 80]
;                                             :theta   [180 359]
;
;                                             :class   [:text-green-300 :opacity-75]
;                                             ;:value   {:start 0.01 :end 0.01}
;                                             :style   {:stroke-width 2
;                                                       :fill         'none
;                                                       :stroke 'white}
;                                             :kind    :fill
;                                             :draw-fn (draw-fn''
;                                                        {:font-size 10
;                                                         ;:dy      -13
;                                                         :f (fn [_] "Kaldt i vannet")})}]}
;                         items.yearwheel-definitions/widgets
;                         {:ticks [{:zero     7.5
;                                   :class    [:text-teal-900]
;                                   :style    {:stroke-width 0.5}
;                                   :insets   [0 0]
;                                   :r        [80 83]
;                                   :theta    [0 359.9]
;                                   :stepsize 3}
;
;                                  {:zero     7.5
;                                   :class    [:text-teal-600]
;                                   :style    {:stroke-width 1}
;                                   :insets   [0 0]
;                                   :r        [80 85]
;                                   :theta    [0 359.9]
;                                   :stepsize 15}
;
;                                  {:class    [:text-yellow-600]
;                                   :style    {:stroke-width 0.5}
;                                   :insets   [0 0]
;                                   :r        [70 73]
;                                   :theta    [0 60.1]
;                                   :stepsize 3}
;                                  {:draw?-fn pos?
;                                   :class    [:text-yellow-100]
;                                   :style    {:stroke-width 0.5}
;                                   :insets   [0 0]
;                                   :r        [70 78]
;                                   :theta    [60 180.1]
;                                   :stepsize 3}]}
;                         {:value-bars-text (all-12-hours' {:r [90 130]})}
;                         {:fixed [:circle {:r 50 :stroke 'red :stroke-width 2 :fill 'red}]}
;                         #_{:value-bars-text (into [] (flatten (mapv (fn [e] (all-12-hours {:r e}))
;                                                                     (mapv vec (partition 2 1 (range 100 136 30))))))}))
;
;(defn pomodore' [k]
;  (merge-with into
;              {:key-id k
;               :theta  [0 359.99]
;               :r      [85 90]
;
;               :skip   true
;               :style  {:fill   'none
;                        :stroke 'currentColor}
;               :class  [:text-red-500 :opacity-50]}
;
;              {:fixed {:circle {:r                65
;                                :fill             'none
;                                :stroke-width     2
;                                :xstroke          'currentColor
;                                :class            [:opacity-50 :text-white]
;                                :stroke-linecap   'square
;                                :stroke           'red
;                                :stroke-dasharray "2,4"}}}
;
;              {:ticks [{:zero     7.5
;                        :class    [:text-teal-900]
;                        :style    {:stroke-width 0.5}
;                        :insets   [0 0]
;                        :r        [80 83]
;                        :theta    [0 359.9]
;                        :stepsize 3}
;
;                       {:zero     7.5
;                        :class    [:text-teal-600]
;                        :style    {:stroke-width 1}
;                        :insets   [0 0]
;                        :r        [80 85]
;                        :theta    [0 359.9]
;                        :stepsize 15}
;
;                       {:class    [:text-yellow-600]
;                        :style    {:stroke-width 0.5}
;                        :insets   [0 0]
;                        :r        [70 73]
;                        :theta    [0 60.1]
;                        :stepsize 3}
;                       {:draw?-fn pos?
;                        :class    [:text-yellow-100]
;                        :style    {:stroke-width 0.5}
;                        :insets   [0 0]
;                        :r        [70 78]
;                        :theta    [60 180.1]
;                        :stepsize 3}]}
;
;              items.yearwheel-definitions/widgets
;              (-> items.yearwheel-definitions/water-temperature
;                  (assoc-in [:graph-bars 0 :r] [60 50])
;                  (assoc-in [:graph-bars 0 :insets] [0.02 0]))
;              (all-hours {:r [80 120]})
;              (update-in items.yearwheel-definitions/calendar [:texts :r] (partial map #(+ 12 %)))))
