(ns items.yearwheel
  (:require [arcs.dial-definitions :as dial-def]
            [diagram.core]
            [diagram.yearwheel.definitions]))

(defn rendered []
  (fn []
    (diagram.core/circle-diagram
      {:key-id "items.yearwheel"
       :config diagram.yearwheel.definitions/yearwheel})))
