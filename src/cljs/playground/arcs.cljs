(ns playground.arcs
  (:require [arcs.dial-definitions :as dial-def]))

(defn pomodore-config [draw-fn & cfgs]
  (let [[cx cy] [200 200]
        context {:cx   (/ cx 2)
                 :cy   (/ cy 2)
                 :zero 0}]
    (dial-def/svg "pomodore" [cx cy] (map (partial draw-fn context) cfgs))))

(def value-bars :value-bars)

(def widgets
  {:class      [:text-gray-750]
   :insets     [0 0]
   :key-id     "widgets"
   :r          [10 11]
   :zero 0 ;todo:
   :theta [0 359.9]
   :style {:fill 'green} ; :<---

   :widgets [{:key-id "w1"
              :kind :triangle
              :style {:stroke-width   0.4
                      :stroke-linecap 'butt
                      :fill 'red
                      :stroke         'yellow}
              :position 1}
             {:key-id "w2"
              :kind :box
              :style {:stroke-width   0.1
                      :stroke-linecap 'butt
                      :fill 'none
                      :stroke         'white}
              :position 0.5}
             {:key-id "w2.5"
              :kind :box
              :style {:stroke-width   0.1
                      :stroke-linecap 'butt
                      :fill 'none
                      :stroke         'white}
              :position 0.3}
             {:key-id "w3"
              :kind :circle
              :style {:stroke-width   0.4
                      :stroke-linecap 'butt
                      :fill 'blue
                      :stroke         'white}
              :position 0.25}]})

(def pomodore-current-runner
  {:class      [:text-gray-750]
   :insets     [0 0]
   :key-id     "current-runner"
   :r          [35 45]
   :zero 0 ;todo:
   :theta [0 180]
   :style {:fill 'blue} ; :<---
   :widgets  [{:key-id "w1"
               :kind :circle
               :style {:stroke-width   1
                       :stroke-linecap 'butt
                       :fill 'lightblue
                       :stroke         'white}
               :position 0.1}]

   :ticks      [{:key-id "a" ;todo: auto-generate keys
                 :stepsize 15
                 :class    [:text-black]
                 :insets   [0 0.5]
                 :style    {:stroke-width 0.2
                            :stroke 'black}}
                {:key-id "b"
                 :stepsize 15
                 :class    [:text-green-700]
                 :insets   [1 -0.2]
                 :style    {:stroke-width 1
                            :stroke 'red}}
                {:key-id "b2"
                 :stepsize 10
                 :class    [:text-blue-500]
                 :insets   [0.3 0.3]
                 :style    {:stroke-width 1}}

                #_{:key-id "c"
                   :stepsize 6.8
                   :class    [:text-yellow-800]
                   :insets   [0.2 0.2]
                   :style    {:stroke-width 2}}]
   value-bars [{:key-id "v1"
                :class [:text-pink-500]
                :style {:stroke-width   1
                        :stroke-linecap 'butt
                        :fill 'lightblue
                        :stroke         'none}
                :kind  :fill
                :range [0 0.5]
                :insets [0.2 0.2]

                :r-inner 0
                :r-outer 0}

               {:key-id "v1.1"
                :class [:text-pink-500]
                :style {:stroke-width   1
                        :stroke-linecap 'butt
                        :fill 'yellow
                        :stroke         'green}
                :kind  :fill
                :range [0.6 0.2]
                :insets [1 -0.2]

                :r-inner 0
                :r-outer 0}

               {:key-id "v2"
                :class [:text-purple-500]
                :style {:stroke-width   2
                        :stroke-linecap 'butt
                        :fill 'none
                        :stroke         'orange}
                :kind  :stroke
                :range   [0.5 0]
                :insets [0 0]
                :r-inner 0
                :r-outer 0}
               {:key-id "v3"
                :class [:text-purple-500]
                :style {:stroke-width   2
                        :stroke-linecap 'butt
                        :fill 'purple
                        :stroke         'none}
                :kind  :fill
                :range   [0.8 0]
                :insets [0 0.7]
                :r-inner 0
                :r-outer 0}

               #_{:style {:stroke-width   :3%
                          :stroke-linecap 'butt}
                  :class [:text-pink-300]
                  :kind  :stroke
                  :value {:start 1.1 :end -1}}]})

(def remaining 20)
(def length 180) ;todo: end instead of length?
(def start 20) ;todo: begin instead of start? (to enable relative positioning)

(defn current-runner [{:keys [start length remaining]}]
  (-> pomodore-current-runner
      ;(assoc :theta [start (+ start length)])
      (assoc-in [:value-bars 0 :value] {:start 0 :end remaining})))

(def prefs [(dial-def/construct-inner-second-cfg {:all-tasks []
                                                  :remaining    44
                                                  :length'       359.99
                                                  :start        0})
            (dial-def/construct-inner-hourly-cfg {:all-tasks []
                                                  :remaining 23
                                                  :length'   359.99
                                                  :start     270})
            (current-runner {:start     start
                             :remaining remaining
                             :length    length})
            widgets])
