(ns playground.sub
  (:require [integrant.core :as ig]
            [clojure.pprint :refer [pprint]]
            [re-frame.core :as rf]))

(defmethod ig/init-key ::app-db [_ {:keys [value-fn]}]
  [:div.mt-3.text-gray-500
   [:div.px-6.flex.justify-between
    [:p.text-xs "App-db"]
    [:p.text-xs "00/00/00"]]
   [:pre.bg-gray-800.text-gray-500.text-xxs.px-6.py-4
    {:style {:overflow-y :scroll}}
    (with-out-str (pprint @(value-fn)))]])

(defmethod ig/init-key ::toggler [_ {:keys [path title subs-fn change-fn]}]
  [:div.w-full
   [:label.select-none.flex.items-baseline.px-2
    [:input.border.text-black.bg-gray-400
     {:name        "some name"
      :type        "checkbox"
      :checked     @(subs-fn path)
      :placeholder ""
      :on-change   #(change-fn path %)}]
    [:p.px-1 {:class (if @(subs-fn path) [:text-gray-900] [:text-gray-500])} title]]])

(defmethod ig/init-key ::field [_ {:keys [path subs-fn change-fn]}]
  [:div.mt-3.px-0
   [:p.text-xs path]
   [:input.border.p-1.text-black.bg-gray-400.w-full
    {:type        "password"
     :value       @(subs-fn path)
     :placeholder ""
     :on-change   #(change-fn path %)}]])

(defmethod ig/init-key ::text-field [_ {:keys [title path subs-fn change-fn]}]
  [:div.mt-3.px-0
   [:p.text-xs.px-2.text-blue-500 (or title "Tiele")]
   [:input.rounded.px-2.border.py-1.text-black.bg-white.w-full
    {:type        "text"
     :value       @(subs-fn path)
     :placeholder "enter some text"
     :on-change   #(change-fn path %)}]])

(defmethod ig/init-key ::field2 [_ {:keys [path subs-fn change-fn]}]
  [:div.mt-3
   [:p.text-xs path]
   [:input.border.p-1.text-black.bg-gray-400
    {:type        "date"
     :value       @(subs-fn path)
     :placeholder ""
     :on-change   #(change-fn path  %)}]])

(defmethod ig/init-key ::component [_ {:keys [title]}]
  [:div.mt-3
   [:p.text-xl (or title "notitle")]])

(defn item [{:keys [record]}]
  [:div.bg-gray-400.text-xl.p-1.rounded-sm.flex [:p.w-full.h-full.center record]])

(defmethod ig/init-key ::contents [_ {:keys [title stuff click-fn]}]
  [:div.mt-3
   [:p.text-xl (or title "no-title")]
   [:div.h-32.border.bg-white.text-black {:style {:overflow-y :scroll}}
    [:ol {:style {:display :grid
                  :grid-gap :2px
                  :grid-template-columns "repeat(auto-fit,minmax(100px, 1fr))"}}
     (for [each (stuff)]
       ^{:key each} [:div {:on-click #(click-fn each)}
                     [item {:record each}]])]]])
