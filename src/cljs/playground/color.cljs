(ns playground.color
  (:require [clojure.string :as string]
            [nv.ui :as ui]))

(defn translate [color]
  (if (some #{color} #{'black 'white})
    {:fg (str "text-" color)
     :bg (str "bg-" color)
     :bd (str "border-" color)}
    (let [bgf #(keyword (str "bg-" color "-" %1))
          fgf #(keyword (str "text-" color "-" %1))
          bdf #(keyword (str "border-" color "-" %1))]
      {:bg-1 (bgf 100)
       :bg-2 (bgf 200)
       :bg-3 (bgf 300)
       :bg-4 (bgf 400)
       :bg-5 (bgf 500)
       :bg-6 (bgf 600)
       :bg-7 (bgf 700)
       :bg-8 (bgf 800)
       :bg-9 (bgf 900)

       :fg-1 (fgf 100)
       :fg-2 (fgf 200)
       :fg-3 (fgf 300)
       :fg-4 (fgf 400)
       :fg-5 (fgf 500)
       :fg-6 (fgf 600)
       :fg-7 (fgf 700)
       :fg-8 (fgf 800)
       :fg-9 (fgf 900)

       :bd-1 (bdf 700)
       :bd-2 (bdf 500)
       :bd-3 (bdf 300)})))

(defn square-blocks
  [size]
  {:display :grid
   :grid-gap 4
   :grid-template-columns (str "repeat(auto-fit,minmax(" size "px," size "px)")
   :grid-auto-rows (str size "pk")})

(defn render "playground.color"
  []
  [:div.scroll-y.snap-x
   [:div (merge {:style (merge (square-blocks 100) {:justify-content :center})})
    (let [b (fn [& r] [:div.text-xxs.center [:div r]])
          item (fn [b [c fg bg bd]] [:div.m-1.snap-start.border-2.rounded-full
                                     {:class [(bd (translate c))
                                              (bg (translate c))
                                              (fg (translate c))]}
                                     (b [:p c] [:p fg] [:p bg] [:p bd])])
          data (for [c '[red #_#_#_#_#_#_#_#_green indigo purple pink gray green teal blue orange yellow]
                     bg (map #(keyword (str 'bg- %)) (range 1 10 3))
                     fg (map #(keyword (str 'fg- %)) (range 1 10 3))
                     bd [:bd-1 :bd-2 :bd-3]]
                 [c fg bg bd])]
      (map (partial item b) data))]])
