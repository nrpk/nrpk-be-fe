(ns playground.demo
  (:require [nv.ui :as ui]
            [re-frame.core :as rf]
            [nv.core :as nv]
            [integrant.core :as ig]
            [items.pomodore]
            [nv.components.calendar]
            [nv.components.helpful-text]
            [playground.components]
            [playground.color]
            [yd.core :as y]))

(defn a-button-bar [s on-click-fn path selection-style]
  (for [e (range 1 6)]
    [:a.font-sans.font-bold

     {:key (str "a-button-bar-" e)
      :on-click #(on-click-fn path e)
      :style {:width  :2rem
              :height :2rem}
      :class (apply str (if (= s e)
                          (interpose " " (map name [selection-style :rounded-full :cursor-default]))
                          (interpose " " (map name [:text-gray-700 (ui/pseudo "hover" "text-white cursor-pointer")]))))}
     [:div.flex.justify-center.items-center.align-center.h-full
      [:p e]]]))

;; event-helpers

(def toggle-question-visibility
  (fn [db _] (update db :debug/questions not)))

(def toggle-size
  (fn [db _]
    (if (:debug/questions db)
      ; only allow update when the q-form is visible
      (update db :debug/toggle-size not)
      db)))

(nv/reg-toggle :debug/questions toggle-question-visibility)
(nv/reg-toggle :debug/toggle-size toggle-size)

;;

(defn connect-ig [g cfg]
  [:div {:style {:grid-area g}
         :key g} cfg])

(defn main-content [{:as sr :keys [role scene]}]
  [:div
   {:key "main-content"}
   (case role
     1 [connect-ig "content" (:system/pomodore (ig/init nv.core/config))]
     2 [connect-ig "content" (:system/yearwheel (ig/init nv.core/config))]
     3 [:div {:style {:grid-area "content"}} (:system/root2 (ig/init nv.core/config))]
     4 (connect-ig "content" (:system/root4 (ig/init nv.core/config)))
     5 [:div.h-full {:style {:grid-area "content"}} [:div.z-50.bg-gray-200 (nv/render)]]
     6 [:div {:style {:grid-area "content"}} (playground.components/help-screen sr)]
     7 [:div {:style {:grid-area "content"}} (:playground/color (ig/init nv.core/config))   #_[:div.h-full.scroll-y {:style {:grid-area "content"}} (playground.components/help-screen role scene)]])])

(defn bottom-bar [role on-click-fn]
  (ui/g-area-noscroll "role" [:div.bg-gray-800.text-gray-100.flex.items-end.h-full.px-2.pb-2.z-10
                              {:key "bottom-bar"}
                              (a-button-bar role on-click-fn :debug/screenset-role  "border-4 border-orange-700")]))

(defn left-bar [scene on-click-fn]
  (ui/g-area-noscroll "scene" [:div.bg-gray-800.pt-2.flex.flex-col.items-center
                               {:key "left-bar"}
                               (a-button-bar scene on-click-fn :debug/screenset-scene  "border-4 border-green-600")]))

(defn bottom-left-bar [help-visible?]
  (ui/g-area-noscroll "question"
                      [:div.bg-gray-800.text-gray-100.z-50.select-none.cursor-pointer
                       {:key "bottom-left-bar"}
                       [:div {:on-click #(rf/dispatch [:debug/questions])
                              :class    (flatten [(if help-visible? [:bg-yellow-500]
                                                      [:bg-yellow-600 :opacity-50])
                                                  ["text-2xl center font-bold text-black"]])
                              :style    {:width  :3rem
                                         :height :3rem}} "?"]]))

(defn render "playground.demo"
  [on-click-fn]
  (let [scene (rf/subscribe [:debug/screenset-scene])
        role (rf/subscribe [:debug/screenset-role])
        help-visible? (rf/subscribe [:debug/questions])]
    (fn []
      (let [scene-role {:scene @scene :role @role}]
        [:div.h-screen
         [:div.h-full.bg-gray-800.text-white
          {:style {:display               :grid
                   :grid-template-rows    "1fr auto"
                   :grid-template-columns "auto 1fr"
                   :grid-template-areas   "'scene    content'
                                           'question role'"}}
          (when @help-visible? (nv.components.helpful-text/render scene-role))
          (left-bar (:scene scene-role) on-click-fn)
          (bottom-left-bar @help-visible?)
          [:div.scroll-y.snap-y.bg-gray-900 (main-content scene-role)]
          (bottom-bar (:role scene-role) on-click-fn)]]))))
