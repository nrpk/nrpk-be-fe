(ns playground.core1
  (:require [integrant.core :as ig]
            [re-frame.core :as rf]
            [clojure.pprint :refer [pprint]]
            [playground.sub :as sub]
            [clojure.string :as string]
            [nv.ui :as ui]))

(defonce data (atom {:data1 (string/capitalize (apply str (interpose " " (take 30 (repeatedly #(rand-nth ["footer" "føter" "feet"]))))))
                     :data2 (string/capitalize (apply str (interpose " " (take 50 (repeatedly #(rand-nth ["footer" "føter" "feet"]))))))}))

(rf/reg-sub ::app-db
            (fn [db _]
              (dissoc db :current-route)))

(rf/reg-sub :field-change
            (fn [db [_ path]]
              (get-in db path "")))

(def change-fn
  (fn [path input]
    (rf/dispatch [:field-change path (-> input .-target .-value)])))

(def toggle-fn
  (fn [path _input]
    (rf/dispatch [:toggle path])))

(defn subs-fn' [path]
  (rf/subscribe [:field-change path]))

(rf/reg-event-db :field-change
                 (fn [db [_ path value]]
                   (assoc-in db path value)))

(rf/reg-event-db :toggle
                 (fn [db [_ path value]]
                   (update-in db path not)))

(defmethod ig/init-key ::stuff [_ opts]
  [:div.text-5xl (:title opts) (:timestamp opts)])

(defmethod ig/init-key ::timestamp [_ {:keys [test]}]
  [:div.text-black.text-sm.bg-gray-200.p-1.inline.mx-1 "TIME " test])

(defmethod ig/init-key ::input-field [fld {:keys [path subs-fn]}]
  [:div
   [:p.text-xs "Input fields " fld]
   [:input.border.p-1.text-black.bg-gray-400
    {:type        "text"
     :value       @(subs-fn path)
     :placeholder "<Enter something>"
     :on-change   #(change-fn path %)}]])

(defmethod ig/init-key ::base-input [_ {:keys [on-change path]}]
  [:div
   [:p path]
   [:input.border.p-1.text-black.bg-gray-100
    {:type      "text"
     :on-change (partial on-change path)}]])

;; layout-concerns only
(defmethod ig/init-key ::root [_ {:keys [sub-component title header input1 input2 input3 contents toggler app-db] :as opts}]
  [:div.text-red-400.p-4.rounded.border.bg-gray-100.m-10.shadow-5.select-none
   [:div.flex-row
    [:div.flex.justify-between input3 [:div.flex.clip-x toggler (:toggler2 opts) (:toggler3 opts)]]
    [:div.xflex.xflex-grow.border input2]
    [:div.xflex.xflex-grow.border input2]
    [:div.xflex.xflex-grow.border input2]]
   sub-component
   [:div.text-xs.text-blue-500 (or title "title-1")]
   (:group opts)
   (:special-group opts [:div.h-16.bg-gray-400.text-gray-600 [:p.center "NADA"]])
   header
   input1
   contents
   app-db])

(defmethod ig/init-key ::list [_ {:keys [title contents item-fn footer]}]
  [:div.mt-3.py-3.text-blue-500
   [:p.text-xs.text-blue-500.px-6.pb-1 (or title "no-title")]
   [:div.xpx-20.h-full.text-black.bg-white.snap-x {:style {:overflow-x :scroll}}
    [:ol {:style {:display            :grid
                  :padding-right      :2rem
                  :grid-gap           :10px
                  :grid-template-rows "auto"
                  :grid-auto-flow     :column
                  :scroll-padding     :50%
                  :grid-auto-columns  "minmax(200px,calc(90%))"}}
     (for [each contents]
       ^{:key each}
       [:div.snap-center.font-serif (item-fn each)])]]
   [:div.pt-5
    [:h2.pl-6.pr-8..font-bold.italic.text-base "Header 2"]
    [:div.pl-6.pr-8..pb-3.text-base.text-blue-800.font-serif
     (:data1 @data)]
    [:div.pl-6.pr-8.bg-blue-200.py-4.text-xs.text-gray-900.font-serif
     [:h1.font-bold.font-sans.mb-1 "Header 1"]
     (:data2 @data)]]])

(defmethod ig/init-key ::group [_ {:keys [caption content] :as opts}]
  [:div caption content])

(derive ::sub/toggler-1 ::sub/toggler)
(derive ::sub/toggler-2 ::sub/toggler)
(derive ::sub/toggler-3 ::sub/toggler)
(derive ::toggler-4-grp ::group)
(derive ::sub/toggler-4 ::sub/toggler)
(derive ::special-group ::group)
(derive ::sub/toggle-5 ::sub/toggler)
(derive ::sub/toggle-6 ::sub/toggler)

;;
;;
;;

(defmethod ig/init-key ::header [_ {:keys [menu
                                           menu-state-fn
                                           path
                                           path-click-fn
                                           menu-click-fn
                                           role-click-fn]}]
  [:div.xfixed.xtop-0.w-full.shadow-6 ;{:style {:max-width 320}}
   [:div.text-gray-100.text-xs.h-12.bg-gray-500.flex.justify-between.z-0
    (let [bg-colors '[:bg-green-700 :bg-green-600 :bg-green-500 :bg-green-400 :bg-green-300 :bg-green-200 :bg-green-100]
          fg-colors '[:text-white :text-white :text-black :text-black :text-black :text-black :text-black]]
      (for [[idx [path-elem [bg fg]]] (map-indexed vector
                                                   (mapv vector
                                                         path
                                                         (mapv vector bg-colors fg-colors)))]
        [:div.bg-red-900
         [:button.m-0.px-4.h-full {:on-click #(path-click-fn idx)
                                   :class    [fg bg]} [:p path-elem]]]))
    [:div.flex.flex-grow]
    [:div.flex.justify-between
     [:button.m-0.px-4.bg-gray-700.h-full {:on-click role-click-fn} [:p "role"]]
     [:button.m-0.px-4.bg-gray-800.h-full.outline-none {:class    (if @menu-state-fn :bg-gray-900 :bg-black)
                                                        :on-click menu-click-fn} [:p "menu"]]]]
   (when @menu-state-fn menu)])

(def separator
  [:div.border-b-2.border-dashed.border-gray-800.my-5])

(defn menus [items items2]
  (let [f (fn [e] (condp = e
                    :sep separator
                    :space [:div.h-4 ""]
                    (if (vector? e)
                      [:a {:href ""} [:p.text-gray-400 (first e)]]
                      [:a {:href ""} [:p.text-gray-700 (str e)]])))]
    [:div.flex.justify-between
     [:div.flex-col.w-full
      (map f items)]
     [:div.w-20]
     [:div.flex-col.w-full
      (map f items2)]]))

(def textblock
  [:div.block.bg-blue-900.mt-4.rounded.shadow-4
   [:div.pl-6.pr-8.py-6.text-blue-300
    [:h2.font-bold.italic.text-base.mb-3 "Header 2"]
    [:div.text-base.text-blue-200.font-serif
     (:data1 @data)]]])

(def menu
  :div.block.bg-gray-900.p-1.text-gray-600.px-6.py-6.text-xs)

(defmethod ig/init-key ::menu [_ {:keys [] :as opts}]
  (let [items '[This That Other Whatever :sep AtLast "Til slutt"]
        items2 '[This That :sep Other Whatever AtLas :sep "Login" "Log ut" :space ["Endre passord"]]]
    [menu
     (menus items items2)
     textblock]))

(defmethod ig/init-key ::root-alt-2 [_ {:keys [title header contents menu] :as opts}]
  [:div.text-blue-500.bg-gray-100.z-0 ;.mt-12
   header

   (:xyz opts)
   [:div.px-6 [:div.text-xl.uppercase.text-gray-500.py-4.mt-8 (or title "title-1")]]
   (:group opts ui/placeholder)
   ;(:special-group opts placeholder)
   [:div.pt-5
    [:h2.pl-6.pr-8..font-bold.italic.text-base "Header 2"]
    [:div.pl-6.pr-8..pb-3.text-base.text-blue-800.font-serif
     (:data2 @data)]
    #_[:div.pl-6.pr-8.bg-blue-200.py-4.text-xs.text-gray-900.font-serif
       [:h1.font-bold.font-sans.mb-1 "Header 1"]
       (interpose " "  (take 30 (repeatedly #(rand-nth ["footer" "føter" "feet"]))))]]
   [:div.px-4 (:input-1 opts)]
   (:button-row opts)
   contents
   [:div.px-4 (:input-2 opts)]
   [:div.py-4
    [:div.px-4 (:check-1 opts)]
    [:div.px-4 (:check-2 opts)]
    [:div.px-4 (:check-3 opts)]]])

(defmethod ig/init-key ::button-row [_ {:keys [title contents] :as opts}]
  [:div
   [:div.py-2.px-4 {:style {:display :grid
                            :justify-content :end
                            :grid-auto-flow :column
                            :grid-auto-columns "100px"
                            :grid-gap :4px}}
    [:button.py-1.border-2.border-green-700.rounded.w-24.m-1.font-bold.text-white.bg-green-500 "Send"]
    [:button.rounded.w-24.m-1.text-red-500.border-2.border-red-700 "Cancel"]]])

(derive ::text1 ::sub/text-field)
(derive ::text2 ::sub/text-field)

(derive ::some-toggle-1 ::sub/toggler)
(derive ::some-toggle-2 ::sub/toggler)
(derive ::some-toggle-3 ::sub/toggler)

(declare config)

(rf/reg-sub :menu-open
            (fn [db _]
              (:menu-open db false)))

(rf/reg-event-db
 :menu-click
 (fn [db _]
   (update db :menu-open not)))

(derive :system/root-alt-2 ::root-alt-2)

(def alt-config
  {:system/root-alt-2    {:title      [:p.font-thin.tracking-wider "Proper hierarchies"]
                          :contents   (ig/ref ::list)
                          :input-1    (ig/ref ::text1)
                          :input-2    (ig/ref ::text2)
                          :button-row (ig/ref ::button-row)
                          :check-1    (ig/ref ::some-toggle-1)
                          :check-2    (ig/ref ::some-toggle-2)
                          :check-3    (ig/ref ::some-toggle-3)
                          :header     (ig/ref ::header)

                          :xyz        (ig/ref ::sub/app-db)}

   ::menu          {}

   ::header        {:path          '[home a b]
                    :menu          (ig/ref ::menu)
                    :path-click-fn #(prn %)
                    :menu-click-fn #(rf/dispatch [:menu-click])
                    :menu-state-fn (rf/subscribe [:menu-open])
                    :role-click-fn #()}

   ::button-row    {}

   ::sub/app-db    {:value-fn (fn [] (rf/subscribe [::app-db]))}

   ::text1         {:path      [:root :input-field-3]
                    :caption   "testasd"
                    :subs-fn   subs-fn'
                    :change-fn change-fn}

   ::text2         {:path      [:root :input-field-2]
                    :title     "Fill in"
                    :subs-fn   subs-fn'
                    :change-fn change-fn}

   ::some-toggle-1 {:path      [:root :toggler-xyz]
                    :title     "Super special toggles"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}

   ::some-toggle-2 {:path      [:root :toggler-xyz-2]
                    :title     "Super special toggles 2"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}

   ::some-toggle-3 {:path      [:toggler-xyz-2]
                    :title     "Super special toggles 2"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}

   ::sub/field     {:path      [:root :input-field-1]
                    :title     "test"
                    :subs-fn   subs-fn'
                    :change-fn change-fn}

   ::list          {:title    "snapping h-list"
                    :contents (range 10)
                    :footer   [:span "Sample #footer here. "]
                    :item-fn  (fn [a] [:div.text-blue-100.bg-blue-400.py-4
                                       [:div.flex-col
                                        [:p.center "test"]
                                        [:p.center.text-xs.text-blue-300 (str "item " a)]]])}})

(def config
  {::root-alt-x2    {:title    "standard-form"
                     :contents (ig/ref ::list)}

   ::list          {:title "::List"
                    :contents (range 16)
                    :item-fn #(vector :p (str "item " %))}

   ::root          {:header        (ig/ref ::stuff)
                    :input1        (ig/ref ::input-field)
                    :input2        (ig/ref ::sub/field)
                    :input3        (ig/ref ::sub/field2)
                    :sub-component (ig/ref ::sub/component)
                    :toggler       (ig/ref ::sub/toggler-1)
                    :toggler2      (ig/ref ::sub/toggler-2)
                    :toggler3      (ig/ref ::sub/toggler-3)
                    :contents      (ig/ref ::sub/contents)
                    :app-db        (ig/ref ::sub/app-db)
                    :special-group (ig/ref ::special-group)
                    :group         (ig/ref ::toggler-4-grp)}

   ::special-group {:caption "Group caption 2"
                    :content (ig/ref ::sub/toggle-5)}

   ::sub/toggle-5  {:path      [:root :toggler-xyz]
                    :title     "Super special toggle"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}

   ::sub/toggle-6  {:path      [:root :toggler-xyz2]
                    :title     "Super special toggle 2"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}

   ::toggler-4-grp {:caption "Group caption 2"
                    :content (ig/ref ::sub/toggler-4)}

   ::sub/toggler-4 {:path      [:root :toggler-special]
                    :title     "Toggle something"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}

   ::sub/app-db    {:value-fn (fn [] (rf/subscribe [::app-db]))}
   ::sub/toggler-1 {:path      [:root :toggler]
                    :title     "Toggle something"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}
   ::sub/toggler-3 {:path      [:root :toggler3]
                    :title     "Toggle something"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}
   ::sub/toggler-2 {:path      [:root :toggler2]
                    :title     "Toggle something2"
                    :subs-fn   subs-fn'
                    :change-fn toggle-fn}
   ::sub/contents  {:title    "CONTENTS"
                    :stuff    (fn filler [] (range 20))
                    :click-fn #(prn (str %))}
   ::sub/component {:title [:div.text-black.bg-yellow-400.inline.p-2.rounded.shadow-4.m-12 "Test title"]}
   ::sub/field2    {:path      [:root :input-field-3]
                    :subs-fn   subs-fn'
                    :change-fn change-fn}
   ::sub/field     {:path      [:root :input-field-2]
                    :title     "test"
                    :subs-fn   subs-fn'
                    :change-fn change-fn}
   ::input-field   {:path    [:root :input-field-1]
                    :subs-fn subs-fn'}
   ::stuff         {:timestamp (ig/ref ::timestamp)
                    :title     "Some title"}
   ::timestamp     {:test :20:10}})

;; there can be configs from any source, eg. receiving edn via htt
(defn render []
  [:div
   (::root-alt-2 (ig/init (merge-with merge alt-config {})) ui/placeholder)])
