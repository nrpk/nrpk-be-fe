(ns playground.components
  (:require [nv.ui :as ui]
            [nv.components.calendar]
            [playground.color]))

(defn filter-box []
  [:div.w-full.text-black.xh-auto.z-0.h-32.bg-red-500
   #_[:div.h-32.debug
      [:p "Masse tekst her"]]
   [:div.p-2
    {:style {:height :auto}
     :class [:bg-gray-200]}

    [:h1.a-1 "Filter"]

    #_(ui/grid' {:grid-gap              1
                 :grid-auto-rows        "auto"
                 :grid-template-rows    "1rem"
                 :grid-template-columns "repeat(auto-fill,minmax(30%,1fr))"}
                [ui/list-item-sm "Noen" 123]
                [ui/list-item-sm "Erfarne" 123])
    ui/space
    #_(ui/grid' {:grid-gap              1
                 :grid-auto-rows        "auto"
                 :grid-template-rows    "1rem"
                 :grid-template-columns "repeat(auto-fill,minmax(9%,1fr))"}
                (map ui/list-item-sm (range 10))
                (map ui/list-item-sm (reverse (range 10))))]])

(defn calendar-view []
  [:div (merge {:style (merge (playground.color/square-blocks 220) {:justify-content :center})})
   #_{:style {:display         :grid
              :justify-content :center
              :font-size       :100%}}
   ;[:div.flex.flex-wrap.bg-gray-200.border-black.border.w-auto


   ;(map (fn [_] [:div.w-full.z-50.sticky.top-0 ui/placeholder]) (range 10))

   (for [e (range 12)]
     #_[:div.w-fullx.w-128.h-64.p-1 ui/placeholder]
     (nv.components.calendar/placeholder-cal e))])

(defn help-screen [{:as sr :keys [role scene]}]
  [:div.bg-gray-400.mr-2
   (nth [[:div.center.flex-col.bg-teal-900
          [:h1.text-6xl ""]
          [:p.text-xl.font-black "Under opplæring"] [:p.text-sm.tracking-widest.font-thin "Under opplæring"]
          [:p.text-lg.font-black "Under opplæring"]
          [:p.text-base.font-black "Under opplæring"]
          [:p.text-sm.font-black "Under opplæring"]
          [:p.text-xs.font-black "Under opplæring"]
          [:p.text-xxs.font-black "Under opplæring"]]
         [:div.center.flex-col.bg-green-800
          [:h1.text-6xl "Vaktliste"]
          [:p.text-xl "Nøkkelvakt"]]
         [:div.center.flex-col.bg-orange-800
          [:h1.text-6xl "Vaktliste"]
          [:p.text-xl "Admin"]]
         [:div.center.flex-col.bg-purple-900
          [:h1.text-6xl "Vaktliste"]
          [:p.text-xl "Æresmedlem"]]
         (calendar-view)
         [:div.flex-col.bg-black.px-10.scroll-y.shadow-4 {:style {:font-size "calc(5vw + 20px)"}}
          [:div {:style {:height :30vh}}]
          [:h1.center.font-black.text-gray-900.mb-10  "Vaktliste"]
          [:div.flex.justify-center
           [:section
            (for [e (range 1)]
              [:div.text-base.text-gray-400.flex
               [:div.text-green-500.border.flex.items-center

                "A"
                [:div.w-10.text-blue-300 (ui/circle #{:burst :fill})]]
               [:div.text-red-500.w-32.flex.border
                (ui/circle #{:mini :fill})
                (ui/circle #{:fill})
                (ui/circle #{:burst :fill})]
               ;[:span.text-red-500 (ui/circle #{:fill :burst})]
               ;[:span.text-green-500 (ui/circle #{:fill})]
               ;[:span.text-green-500 (ui/circle #{})]
               ;[:span.text-green-500 (ui/circle #{:filled :burst})]
               ;(* 3 (inc (mod (inc (rand-int 100)) 10)))
               ;" --- "
               #_[:span.text-gray-400 (nv.datum/random-full-name!)]])]]
          [:div {:style {:height :30vh}}]]]

        (dec role)
        [:p.center.text-xl.bg-gray-900.text-gray-800 "use-case" [:span.text-yellow-400.px-1.underline scene " : " role] "undefined"])])
