(ns app.boat-selector-grid
  (:require
   [cljs.spec.alpha :as s]))

(defn grid-area
  "docstring"
  [c & xs]
  [:div (merge {:key (str "gridarea-" c)} c) xs])

(defn grid-row [c & xs]
  [:div.grid-row c xs])

(defn grid-col [c & xs]
  [:div.p-2.bg-red-600 [:div.grid-col c xs]])

(s/def ::grid-row'
  (s/and
   map?
   (s/keys :req-un [::type])
   #_(s/+
      (s/cat :type1 keyword?))))

(defn grid-row' [opts & xs]
  (if (s/valid? ::grid-row' opts)
    [:div

     (case (:type opts)
       :a {:style {:display               :grid
                   :grid-template-columns "3rem"}}
       :b {:class [:p-2]
           :style {:display           :grid
                   :grid-auto-flow    :column
                   :grid-auto-columns "10rem"
                   :grid-gap :0.5vw}})

     xs]
    (throw (js/Error. (s/explain  ::grid-row' opts)))))

(defn on-grid-f [gridarea & f]
	[:div;._h-full._scroll-y.border-2.border-red-500.border-dotted
	 {:style {:key gridarea
						:grid-area gridarea
						:display :grid}}
	 [:div f]])

(defn on-grid [gridarea & xs]
  [:div                                           ;.border-dotted;._h-full._scroll-y.border-2.border-red-500.border-dotted
	 {:style {:key       (str gridarea (count xs))
						:display   :grid
						:grid-area gridarea}}
   xs])

(defn on-grid-key [k gridarea & xs]
  [:div                                           ;.border-dotted;._h-full._scroll-y.border-2.border-red-500.border-dotted
   {:style {:key       (str k)
            :display   :grid
            :grid-area gridarea}}
   xs])

; note: overflow visible on all on-grid'
(defn on-grid-class [gridarea & xs]
  [:div {:class gridarea :style {:key gridarea
                                 :overflow :hidden}} xs])
