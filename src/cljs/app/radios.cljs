(ns app.radios
	(:require [app.boat-selector-grid :refer [grid-row]]
						[re-frame.core :as rf]
						[app.boat-selector-grid :as g]))

(defn reg-big-function [{:keys [domain default]}]
  (do
    (rf/reg-event-db :evt-selected [rf/debug] (fn handler [db [_ domain value]]
                                     (assoc-in db domain value)))
    (rf/reg-sub :sub-selected (fn sub [db _]
                                (get-in db domain default)))))

(defn big-function [{:keys [domain table disp-fn]}]
  (let [current-value @(rf/subscribe [:sub-selected])
        state {:selected [:bg-gray-900
                          :text-gray-100
                          :border-green-500
                          :border-4]
               :normal   [:bg-gray-500 :text-black :border-4 :border-red-500]}]
    (g/grid-row'
			{:type :b}
     (for [[k v] table]
       [:button.border.rounded {:key      (str domain "-" k)
                                :on-click #(disp-fn k)
                                :style    {:font-size :2vw}
                                :class    (flatten [:m-0 :p-0
                                                    (if (= current-value k)
                                                      (state :selected)
                                                      (state :normal)) []])} v]))))

(defn big-func-frontend [{:keys [domain] :as opts}]
  (reg-big-function opts)
  (big-function (merge {:disp-fn #(rf/dispatch [:evt-selected domain %])}
                       opts)))
