(ns app.view.debug
	(:require [re-frame.core :as rf]
						[clojure.pprint :refer [pprint]]
						[app.boat-selector-grid :as g]))

#_(defn debug []
		[:div {:key "debug"}
		 [:pre.text-pink-600.text-xs
			{:key "debug-a"
			 :style {:grid-column "1/4"}}
			(pp (dissoc @(subscribe [:app.boat-selector/root])
									; exclude these
									:re-pressed.core/keydown
									:gql-result-1 :gql-result-2 :re-graph :route :timer-value))]
		 [:div.h-2]
		 [:pre.text-black.text-xs.bg-white
			{:key "debug-b"
			 :style {:grid-column "1/4"}
			 :class [:area-abc]}
			(pp (select-keys @(subscribe [:app.boat-selector/root]) [:focus :checkout :selection :valid?]))]])

(defn debug []
	(when-let [root @(rf/subscribe [:app.boat-selector/selected])]
		(g/on-grid "2/1/-2/-2" [:pre.text-xs (with-out-str (pprint root))])))
