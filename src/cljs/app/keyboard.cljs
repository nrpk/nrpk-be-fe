(ns app.keyboard
  (:require [re-frame.core :refer [dispatch dispatch-sync]]
            [re-pressed.core :as rp]
            [app.keyboard-definitions :as k]
            [app.boat-selector :as selector]

            [app.boat-selector.admin-toolbar :as toolbar]))

(defn- define-key [shortcut & msg]
  [(vec (first msg))
   [{:keyCode shortcut}]])

#_(add-tap #(println (with-out-str (cljs.pprint/pprint %))))

(defn keydata []
  (into [] (concat
            (map (fn [e] (define-key (+ k/Number0 e) [::selector/append e])) (range 10))
            (map #(apply define-key ((juxt first rest) %))
                 [[k/A ::selector/toggle-wide]
                  [k/Q ::selector/set-mode :out]
                  [k/W ::selector/set-mode :status]
                  [k/E ::selector/set-mode :in]
                  [k/R ::selector/set-mode :dial]
                  [k/K ::selector/toggle-have-key]
                  [k/O ::selector/toggle-sleep-over]
                  [k/S ::toolbar/get-next-color-fx]
                  [k/T ::selector/set-mode :playground]
                  [k/Z ::selector/set-experiment-mode 1]
                  [k/RETURN ::selector/select-current]
                  [k/BACKSPACE ::selector/backspace]
                  [k/C ::selector/clear]
                  [k/TAB :next-input]
                  [k/K :toggle-havekey]
                  [k/N ::selector/set-focus :phonenumber]
                  [k/B ::selector/set-focus :boatnumber]]))))

(defn default-shortcuts []
  (dispatch-sync [::rp/add-keyboard-event-listener "keydown"])
  (dispatch [::rp/set-keydown-rules
             {:event-keys           (keydata)
              :clear-keys           [[{:keyCode k/ESC}]
                                     [{:keyCode k/G
                                       :ctrlKey true}]]
              :prevent-default-keys [{:keyCode k/RETURN}
                                     {:keyCode k/G
                                      :ctrlKey true}]
              :always-listen-keys   []}]))
