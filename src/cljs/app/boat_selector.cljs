(ns app.boat-selector
  (:require
   [cljs.spec.alpha :as s]
   [expound.alpha :as expound]
   [items.core :as w :refer [row]]
   [re-frame.core :as rf :refer [subscribe dispatch reg-sub reg-event-db]]
   [items.pomodore]
   [frontend.mainpage]
   [app.boat-selector-grid :as g]
   [app.boat-selector-card :as c]
   [arcs.comps]
   [app.boat-selector.spec]
   [app.view.debug]
   [arcs.dial-definitions :as dial-def]
   [app.boat-selector.admin-toolbar :as admin-toolbar]
   [app.initial-db :as db]))

(def field-table
  {:boatnumber :boatnumber/value
   :phonenumber :phonenumber/value})

;;; helpers ;;;

(defn get-value [field] (subscribe [::value field]))
(defn set-focus [field]  (dispatch [::set-focus field]))
(defn active-field [db] (field-table (db :focus)))

(def bg-input-owner-color (fn [{:keys [focus]} _]
                            (condp = focus
                              :phonenumber [:bg-pink-700
                                            :text-pink-200]
                              :boatnumber [:bg-green-700
                                           :text-green-200]
                              [])))

;;; subscriptions 																																;;;

(reg-sub ::total-avail-boats (fn [db _] (count (:boats db))))
(reg-sub ::current-recognized-item
         (fn [_]
           [(subscribe [:app.core/boats-matching-input])
            (subscribe [::value :boatnumber])])
         (fn [[avail curr]]
           (let [c (apply str curr)
                 n (js/parseInt c)]
             (when (= 3 (count c))
               (if-not (empty? avail)
                 (if (contains? (set (keys avail)) n)
                   n
                   nil)
                 nil)))))
(reg-sub ::bg-input-owner-color bg-input-owner-color)
(reg-sub ::selected-count (fn [db _] (count (:selection db))))
(reg-sub ::selected (fn [db _] (:selection db #_{1234 {}})))
(reg-sub ::field (fn [db [_ idx field]] (get-in db [:selection idx :stats field])))
(reg-sub ::value (fn [db [_ args]] (apply str (reverse (get db (field-table args))))))
(reg-sub ::focus (fn [db _] (:focus db :boatnumber)))
(reg-sub ::root (fn [db _] db))
(reg-sub ::error-msg (fn [db _] (:error-msg db)))
(reg-sub ::mode (fn [db _] (:mode db :out)))
(reg-sub ::timer-value (fn [db _] (db :timer-value)))
(reg-sub ::wide (fn [db _] (db :wide-mode false)))
(reg-sub ::experiment-mode (fn [db _] (db :experiment-mode 0)))
(reg-sub ::boats
         (fn [_ _]
           db/data
           #_{100 {}
              101 {}
              102 {}
              103 {}
              104 {}
              105 {}
              106 {}}))
(reg-sub :app.core/boats-matching-input
         (fn [_]
           (subscribe [::value :boatnumber]))
         (fn [bn]
           ; todo: why can it be empty?
           (if (empty? bn)
             nil
             (let [boatnumber-str (apply str bn)]
               (filter (fn [[boatnumber-key _]]
                         (= (subs (str boatnumber-key) 0 (count boatnumber-str))
                            boatnumber-str))
                       db/data)))))
(reg-sub ::have-key (fn [db _] (db :have-key false)))
(reg-sub ::sleep-over (fn [db _] (db :sleep-over false)))

;;; events                                                                         ;;;

(defn- validate
  "just ask spec if m is valid"
  [m]
  (s/valid? ::selection-spec m))

(reg-event-db ::select-current
              (fn [db _]
                (let [boatnumber-str (apply str (reverse (get db (field-table :boatnumber))))
                      boatnumber   (js/parseInt boatnumber-str)]
                  (if (= 3 (count boatnumber-str))
                    (-> db
                        ;todo: use a vector with the keys instead of the hash-map to pertain ordering
                        (assoc-in [:selection boatnumber] {:stats nil})
                        (dissoc (active-field db)))
                    db))))
(reg-event-db ::checkout
              (fn [db _] (let [sel (:selection db)]
                           (if (validate sel)
                             (assoc db :error-msg nil
                                    :selection nil)
                             (assoc db :error-msg (str "x: " (expound/expound ::selection-spec (:selection db))))))))
(reg-event-db ::validate
              (fn [db _] (assoc db :valid? (validate (:selection db)))))
(reg-event-db ::checkin
              (fn [db _] db))
(reg-event-db ::reset-selection
              (fn [db [_]]
                (assoc db :selection {})))
(reg-event-db ::set
              (fn [db [_ boatnumber field v]] (assoc-in db [:selection boatnumber :stats field] v)))
(reg-event-db ::toggle
              [rf/debug]
              ;; clicking on the right column
              (fn [db [_ boatnumber]]
                (if (get-in db [:selection boatnumber])
                  (update-in db [:selection] dissoc boatnumber)
                  (assoc-in db [:selection boatnumber] {:stats {}}))))
(reg-event-db ::select
              (fn [db [_ boatnumber]]
                (assoc-in db [:selection boatnumber] {:stats {}})))
(reg-event-db ::cancel-selection
              (fn [db [_ boatnumber]]
                (update db :selection dissoc boatnumber)))
(reg-event-db ::set-focus
              (fn [db [_ args]] (assoc db :focus args)))
(reg-event-db ::append
              (fn [db [_ value]]
                (update db (active-field db) conj value)))
(reg-event-db ::clear
              (fn [db _] (dissoc db (active-field db))))
(reg-event-db ::backspace
              (fn [db _] (update db (active-field db) pop)))
(reg-event-db ::set-mode
              (fn [db [_ mode]] (prn ">> ::set-mode " mode) (assoc db :mode mode)))
(reg-event-db ::toggle-wide
              (fn [db [_]] (update db :wide-mode not)))
(reg-event-db ::set-wide
              (fn [db [_ wide?]] (prn ">> ::set-wide " wide?) (assoc db :wide-mode wide?)))
(reg-event-db ::set-experiment-mode
              (fn [db [_]] (assoc db :experiment-mode (if (= 0 (db :experiment-mode)) 1 0))))
(reg-event-db ::toggle-have-key
              (fn [db [_]] (update db :have-key not)))
(reg-event-db ::toggle-sleep-over
              (fn [db [_]] (update db :sleep-over not)))

;;; ad-hoc HOC's																																		;;;


(defn input-panel
  [{:keys [key label focused? value-token when-clicked max-length] :or {key "some-key"}}]
  (let [v (apply str @(get-value value-token))
        [active-bg active-fg] @(subscribe [::bg-input-owner-color])]
        ;; satifies-input? (= max-length (count v))

    [:button.rounded.flex.flex-col.justify-center.items-center.border-2
     {:key      key
      :class    (if focused? [:_bg-gray-950 active-bg active-fg]
                    [:border-gray-700])
      :on-click when-clicked}

     [:div.px-1.text-base
      {:class (if focused? :text-gray-100 :text-gray-700)} label]

     ;content
     [:div.px-2.w-full.font-mono.text-xl.flex.justify-center.font-black
      {:class [:text-white]}
      [:span
       {:class [(if (> (count v) max-length) :text-orange-500 :text-yellow-300)]} v]]]))

(defn- mode-button [[m s]]
  (let [mode @(subscribe [::mode])
        active (= mode m)]
    [:div.items-end.flex.justify-end.p-2.text-xl.font-black
     {:on-click #(rf/dispatch [::set-mode m])
      :class    (if active
                  [:bg-teal-500
                   :text-black]
                  [:bg-gray-700
                   :text-gray-500])}
     s]))

(defn mode-buttons []
  (let [data {:out        "Ut"
              ;:status     "Status"
              :in         "Inn"}]
              ;:dial       "Dial"
              ;:playground "Lekekasse"

    [:div.bg-gray-900
     {:style
      {:display               :grid
       :grid-template-columns "repeat(2,1fr)"
       :grid-template-rows    "auto"
       :grid-auto-columns     "auto"
       :grid-auto-rows        "auto"
       :grid-gap              :4px}}
     (for [e data]
       [mode-button e])]))

;;;

(defn epoch-window [data-sources]
  (let [mode @(subscribe [::mode])
        timer-value @(subscribe [::timer-value])
        epoch (fn [n] (/ (mod (- timer-value 10000) n) n))
        experimental-mode @(subscribe [::experiment-mode])]
    [:div.text-blue-600.bg-gray-700.overflow-hidden.scroll-y.shadow-4.w-full
     {:key (str "epoch-window")
      :style {;:min-width :10rem
              :display :flex}}
     [:div.p-2
      [:p.text-white mode]
      [:p (str "epoch " (epoch 10))]
      [:p (str "experimental mode " experimental-mode)]]
     (let [timer2 0.16
           timer1 0.2
           [x y :as cxy] [400 400]]
       [:div.w-full.h-full.bg-green-900.border.p-4.border-box
        [:div.bg-red-900.w-full.h-full.clip
         (dial-def/svg "sample-simple-timeline" cxy
                       (->> data-sources
                            (map #(assoc % :r (mapv (partial * 0.6) (% :r))))
                            (map (partial arcs.dial/draw-arc {:cx   (/ x 2)
                                                              :cy   (/ y 2)
                                                              :zero 0}))))]])

     #_(dial-def/sample-simple-timeline arcs.dial/draw-arc
                                        {:timer1 (epoch 10)
                                         :timer2 (epoch 20)})]))

;;;

(defn list-of-available-boats [a]
  (let [total @(subscribe [:app.core/total-avail-boats])]
    (if (empty? a)
      [:div.center.h-full.text-4xl.font-black.text-gray-700 (str total " båter")]
      (g/grid-col (doall (map-indexed c/right-card a))))))

(defn confirm-action-button []
  (row {:class [:h-full :bg-gray-900]}
       (w/button {:key      "ferdig"
                  :class    [:text-xl
                             :w-full
                             :h-full
                             :rounded-sm
                             :bg-green-500
                             :text-white
                             :font-bold
                             :border-green-600
                             :border-4]
                             ;:shadow-4


                  :on-click #(dispatch [::checkout])} "Ferdig")
       (w/button {:key      "avbryt"
                  :class    [:text-xl
                             :w-full
                             :h-full
                             :rounded-sm
                             :bg-gray-900
                             :text-gray-100
                             :font-bold
                             :border-red-600
                             :border-4]
                             ;:shadow-4

                  :on-click #(dispatch [::checkout])} "Avbryt")))

(defn list-of-selected-boats []
  (let [selected-map (subscribe [::selected])
        current @(subscribe [::current-recognized-item])
        contains-selected (get @selected-map current)]
    (fn []
      [:div.scroll-y.bg-gray-900 {:style {:display :grid
                                          :grid-auto-rows "minmax(144px,144px)"
                                          :grid-gap :5px}}
       (concat
        (when (and (not (nil? current))
                   (not contains-selected))
          [(c/left-card-preview 0 current)])
        (map-indexed (fn [idx [k _]]
                       [c/left-card (and (= k current)
                                         contains-selected) idx k])
                     @selected-map))])))

(defn field [token f]
  (w/button-pad-clean [] [:bg-gray-700
                          :text-white
                          :font-bold
                          :text-xl]
                      (f @(subscribe [token]))))

(defn render-status []
  (let [boats (subscribe [::boats])]
    (fn []
      [:div.scroll-y
       {:style {:display               :grid
                :grid-template-columns "repeat(auto-fit,minmax(200px,1fr))"
                :grid-auto-rows        "minmax(144px,188px)"
                :grid-gap              5}}
       (for [boat @boats]
         (c/status-card :_index boat))])))

(def non-selection-style
  [:border-2 :border-gray-950 :bg-gray-800 :text-gray-500 :rounded])

(defn selection-style [base-fg-color base-bg-color base-border-color]
  [:border-2  base-fg-color base-bg-color base-border-color :rounded])

(defn user-data-input []
  (let [current-focus @(subscribe [::focus])
        have-key @(subscribe [::have-key])
        sleep-over @(subscribe [::sleep-over])]
    [:div {:style {:display            :grid
                   :grid-gap           5
                   :grid-auto-rows     "auto"
                   :grid-template-columns "repeat(2,1fr)"
                   :grid-template-rows "repeat(3,minmax(44px,80px))"}}

     (g/on-grid "1/1"
                (input-panel
                 {:key          "telefonnummer"
                  :label        "Telefonnummer"
                  :focused?     (= :phonenumber current-focus)
                  :value-token  :phonenumber
                  :when-clicked #(set-focus :phonenumber)
                  :max-length   8}))

     (g/on-grid "2/2"
                [:button.text-xl.font-bold.p-1.outline-none
                 {:on-click #(dispatch [::toggle-sleep-over])
                  :class (if sleep-over (selection-style :text-gray-900 :bg-orange-600 :border-orange-300) non-selection-style)}
                 [:p "Overnatting"]])

     (g/on-grid "2/1"
                [:button.h-full.text-xl.font-bold.h-full.p-1.border-2.outline-none
                 {:on-click #(dispatch [::toggle-have-key])
                  :class (if have-key (selection-style :text-gray-900 :bg-yellow-600 :border-yellow-300) non-selection-style)}
                 "Har med nøkkel"])

     (g/on-grid "1/2"
                (input-panel {:key           "båtnummer"
                              :label        "Båt-nummer"
                              :focused?     (= :boatnumber current-focus)
                              :value-token  :boatnumber
                              :when-clicked #(set-focus :boatnumber)
                              :max-length   3}))

     (g/on-grid "3/2/4/3"
                [:button.text-xl.font-bold.p-1.rounded
                 {:class (if false [:border-2 :text-gray-500] non-selection-style)}
                 [:p "Beskjed"]]
                #_[:textarea.py-2.z-50.w-full.font-bold.px-3.py-0.m-0.border-4.border-blue-300.text-xl.font-sans {:placeholder "Kommentar"}])]))

(defn user-data-input-filter []
  (let [current-focus @(subscribe [::focus])]
    [:div {:style {:display            :grid
                   :grid-gap           5
                   :grid-auto-rows     "auto"
                   :grid-template-columns "repeat(2,1fr)"
                   :grid-template-rows "repeat(3,minmax(44px,80px))"}}

     (g/on-grid "1/1"
                (input-panel
                 {:key          "telefonnummer"
                  :label        "Telefonnummer"
                  :focused?     (= :phonenumber current-focus)
                  :value-token  :phonenumber
                  :when-clicked #(set-focus :phonenumber)
                  :max-length   8}))

     (g/on-grid "2/2"
                [:button.text-xl.font-bold.p-1
                 {:class (if true [:border-2 :text-gray-500] [:bg-gray-700])}
                 [:p "Overnatting"]])

     (g/on-grid "2/1"
                [:button.h-full.text-xl.font-bold.h-full.p-1.border-2
                 {:class (if false [:bg-yellow-300] [:text-gray-500 :_bg-gray-700])}
                 "Har med nøkkel"])

     (g/on-grid "1/2"
                (input-panel {:key           "båtnummer"
                              :label        "Båt-nummer"
                              :focused?     (= :boatnumber current-focus)
                              :value-token  :boatnumber
                              :when-clicked #(set-focus :boatnumber)
                              :max-length   3}))

     #_(g/on-grid "3/2/4/3"
                  [:button.text-xl.font-bold.p-1
                   {:class (if true [:border-2 :text-gray-500] [:bg-gray-700])}
                   [:p "Beskjed"]]
                  #_[:textarea.py-2.z-50.w-full.font-bold.px-3.py-0.m-0.border-4.border-blue-300.text-xl.font-sans {:placeholder "Kommentar"}])]))

(defn signal []
  [:div.text-yellow-300.relative.flex.items-center.justify-center.shadows
   [:img {:src "img/nrpk-logo-transp.png"}]
   (comment
     [:div.z-50.text-white.w-full.text-xl.justify-center.flex "s"]
     [:div.text-green-500.w-full.absolute.p-1 {:style {:padding-bottom :30px}} (admin-toolbar/triangle)]
     [:div.text-red-500.w-full.absolute.p-1 (admin-toolbar/circle 1)])])
