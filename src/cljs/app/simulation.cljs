(ns app.simulation
  (:require [re-frame.core :as rf]
             [cljs.core.async :refer [>! <! put! chan timeout]])
  (:require-macros [cljs.core.async :refer [go go-loop]]))

;; debugging tools

(defonce timers (atom nil))
(def seed 10000)

(when @timers
  (js/clearInterval @timers))

(rf/reg-event-db
 ::timer-tick
 (fn [db [_ seed]]
   (assoc db :timer-value (inc (:timer-value db seed)))))

(reset! timers (js/setInterval #(rf/dispatch [::timer-tick seed]) 1000))

(comment
  (do
    (let [ch (chan 5)]
      (go (while true
            (let [d (<! ch)]
              (print "Yo" d))))
      (doseq [e (range 10)]
       (put! ch e))
      (print "end")
      (put! ch (timeout 2500))
      (put! ch "here"))))
