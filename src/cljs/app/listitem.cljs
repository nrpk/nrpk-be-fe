(ns app.listitem)

(defn- dark-item-event [xs]
  [:div.bg-blue-700.border-blue-900.border-2
   [:div.text-red-500.p-1.text-xs :event]
   [:div.text-gray-900.p-1.text-xs "yy" #_(map str (keys xs))]
   [:div.text-gray-500.p-1.text-xs "xx" #_(str (keys (val xs)))]])

(defn- dark-item [xs]
  [:div.bg-gray-950.border-red-500.border-2
   [:div.text-red-500.p-1.text-xs.text-right (key xs)]
   [:div.text-gray-700.p-1.text-xs (map str (keys (val xs)))]
   [:div.text-gray-500.p-1.text-xs (str (keys (val xs)))]])

(defn light-item [xs]
  [:div.bg-gray-400.center
   [:div.text-black.text-xl.text-right.px-2 (key xs)]])

(defn simple-item [xs]
  [:div.bg-gray-400.center.h-32
   [:div.text-black.text-xl.text-right.px-2 (key xs)]])

(defn card [[k v]]
	[:div.bg-gray-400.center
	 [:div.text-black.text-xl.text-right.px-2 (str k v)]])

(defn listitem [e]
  [:<>
   (light-item e) (dark-item e)])

(declare inset)

(defn unassigned-item
	"this could be an event, the info-card of a boat or anything else"
	[{:keys [k v] :as all}]
	(inset [:div.bg-red-900.text-gray-100.text-xxs.px-2.py-1
					[:p "unassigned itm"]
					[:p (interpose ", " [ k (when (map? v) "v is a map") v  ])]
					(for [e (keys all)]
						[:p.text-red-400.font-black.text-xl e])]))

(defn- grid-col [c & xs]
  [:div.bg-gray-800.scroll-y
   c
   xs])

(defn list-col [itm xs]
  (grid-col {
             :style {:display               :grid
                     :grid-template-columns "auto 1fr"
                     :grid-auto-rows        "80px"
                     :grid-gap              :1px}} (map itm xs)))

(defn list-col-simple [{:keys [item-fn empty-fn]
                        :or   {item-fn  unassigned-item
                               empty-fn (constantly "EMPTY")}} xs]
  (if (empty? xs)
    (empty-fn)
    (grid-col {:style {:height :auto
                       :display               :grid
                       :grid-template-columns "1fr 1fr 1fr"
                       :grid-auto-rows        "auto"
                       :grid-gap              :4px}}
              (map item-fn xs))))

(defn inset [& xs]
  [:div.p-1.bg-gray-950 {:style {:overflow  :visible}} xs])

(defn inset' [& xs]
  [:div.p-4.bg-gray-950  xs])

(defn scrollable-inset [& xs]
  (inset' [:div.scroll-y xs]))
