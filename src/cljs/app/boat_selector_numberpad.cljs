(ns app.boat-selector-numberpad
  (:require [re-frame.core :as rf]
            ;todo: move reg-event and reg-sub's into this
            [app.boat-selector :as bsel]))

(defn- key-for-char [e]
  (condp = e
    (js/parseInt e) :number
    'clear :clear
    'return :return
    :back))

(defn data [e]
  {:number [[::bsel/append e]
            {:class [:bg-black :opacity-75]}
            (str e)]
   :clear  [[::bsel/clear]
            {:class [:bg-yellow-500 :text-black :opacity-75]}
            "Cl"]
   :return [[::bsel/select-current]
            {:class [:bg-black :opacity-75]
             :style {:grid-area "1/1/1/span 3"}}
            (str "Velg #" (apply str @(rf/subscribe [::bsel/value :boatnumber])))]
   :back   [[::bsel/backspace]
            {:class [:bg-red-500 :text-black :opacity-75]}
            "#"]})

(defn numberpad []
  [:div.grid-numberpad
   {:key   "numberpad-input"}
   (for [e '[7 8 9 4 5 6 1 2 3 0 bs clear]]
     (let [[msg class caption] ((key-for-char e) (data e))]
       (if (= e 'return)
				 ; todo: remove the hardcoded 3
         (when (= 3 (count @(rf/subscribe [::bsel/value :boatnumber])))
           [:button.text-white (merge class {:on-click #(rf/dispatch msg)}) caption])
         [:button {:class    [:border-2 :border-gray-500 :rounded-sm :bg-gray-700 :text-2xl :font-black :text-yellow-500]
                   :on-click #(rf/dispatch msg)}
          caption])))])

(rf/reg-sub ::dummy
            (fn [_]
              [:bg-blue-800 :text-yellow-500]))
