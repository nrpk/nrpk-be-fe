(ns app.db
  (:require [app.boat-selector.renderer]
            [app.simulation]
            [app.aggregation :as agg]
            [app.packet.builder :as b]
            [app.initial-db :as db]
            [re-frame.core :as rf]
            [markdown.core :as md]))

(rf/reg-event-db ::initialize-db
                 (fn [db _]
                   (-> (merge db
                              {:root {:input-field-1 "inpt1"}}
                              {:mode :out}
                              (reduce agg/computation
                                      agg/initial-model
                                      (for [[k v] db/data]
                                        (b/add-boat' k v))))
                    ;(assoc :events [])
)))

(comment
  (do
    (md/mdToHtml "# FOO")))
