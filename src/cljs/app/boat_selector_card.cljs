(ns app.boat-selector-card
  (:require [re-frame.core :as rf]
            [items.core :as w]
            [cljs.spec.alpha :as s]))

(def age-groups ["barn" "ungdom" "voksen" "pensjonist"])

(def gender-groups ["mann" "kvinne"])

(s/check-asserts false)

(def card-details
  (s/assert :app.boat-selector/card-details
            {:left-card           {:container [:text-gray-900]}

             :right-card-normal   {:container [:bg-gray-100 :text-gray-900 :shadow-4 :border-red-500 :border-2]
                                   :content   [:bg-gray-100 :text-gray-900]}

             :right-card-selected {:container [:text-gray-800 :bg-gray-900 :border :rounded-lg :border-black]
                                   :content   [:text-gray-600 :bg-gray-900 :rounded-lg]}

             :right-card-defect   {:container [:ml-2 :bg-red-900 :text-red-600 :border-black :border-2]
                                   :content   [:bg-red-800 :text-red-500]}
             :status-card-normal  {:container [:bg-gray-100 :text-gray-900]
                                   :content   [:bg-gray-600 :text-gray-900]}}))

(defn- close-button [k]
  [:div.px-2 (w/button-tiny {:class [:px-1 :border-none :w-8 :h-8 :shadow- :rounded-full :text-gray-100 :bg-gray-950]}
                            [:app.boat-selector/cancel-selection k]
                            [:svg {:xmlns "http://www.w3.org/2000/svg" :width "100%" :height "100%" :viewBox "0 0 24 24" :fill "none" :stroke "currentColor" :stroke-width "4" :stroke-linecap "round" :stroke-linejoin "round"}
                             [:line {:x1 "18" :y1 "6" :x2 "6" :y2 "18"}] [:line {:x1 "6" :y1 "6" :x2 "18" :y2 "18"}]])])

(defn right-card [idx [k v]]
  (let [boatnumber k
        selected? (contains? @(rf/subscribe [:app.boat-selector/selected]) boatnumber)
        special-case (= 1 (:special v))
        container #(get-in card-details [% :container])
        content #(get-in card-details [% :content])]
    [:div.w-full.select-none.rounded-lg
     {:key      (str "card-" idx)
      :on-click (when-not special-case
                  #(rf/dispatch [:app.boat-selector/toggle boatnumber]))
      :class    (container (if special-case
                             :right-card-defect
                             (if selected?
                               :right-card-selected
                               :right-card-normal)))}
		 ;; container
     (if special-case
       [:div.pt-2.px-2.h-48
        [:div.flex.justify-between.items-start.flex-col
         [:p.text-4xl.font-sans.font-bold.text-xs k]

				 ;; content
         [:div
          {:key   boatnumber
           :class (content (if special-case
                             :right-card-defect
                             (if selected? :right-card-selected :right-card-normal)))}
          "Error description"]]]

       [:div.pt-2.px-2
        [:div.flex.justify-between.items-start.flex-col
         [:p.text-4xl.font-sans.font-bold.text-xs k]
         [:div
          {:key   boatnumber
           :class (content (if special-case
                             :right-card-defect
                             (if selected? :right-card-selected :right-card-normal)))}
          "Some Content"]]])]))

(defn- helper [idx groups path disp-fn]
  (let [x @(rf/subscribe path)]
    [:div
     (for [[idx' caption] (map-indexed #(vector %1 %2) groups)]
       [:button {:key      (str "button-" idx' "-" idx)
                 :on-click #(disp-fn idx')
                 :class    (flatten [:py-1 :px-1 :mr-1 :mb-1
                                     (if (= idx' x)
                                       [:bg-gray-900 :text-gray-100]
                                       [:text-gray-900 :bg-gray-300])])}
        caption])]))

(defn group [idx k g field]
  (helper idx g [:app.boat-selector/field k field]
          #(rf/dispatch [:app.boat-selector/set k field %])))

;; core
(letfn [(card [xs highlight idx]
          [:div.w-full.shadow-4.rounded-sm.bg-gray-200
           {:key   (str "card-" idx)
            :class [(if highlight :bg-orange-500 :bg-gray-200)]}
           xs])
        (header [xs k]
                [:div.pt-2.font-bold.text-xs
                 [:div.flex.justify-between.items-center
                  [:p.text-4xl.font-sans.text-gray-900.px-2 k]
                  (close-button k)]
                 xs])
        (stats [idx k]
               [:div.w-full.rounded-sm
                {:key   (str "card-" idx)
                 :style {:display               :grid
                         :auto-flow             :row
                         :grid-template-columns :1fr}
                 :class [:bg-gray-100 :px-2 :py-1]}
                (group idx k age-groups :app.boat-selector/age)
                (group idx k gender-groups :app.boat-selector/gender)])]

  (defn left-card [highlight idx k]
    (-> (stats idx k)
        (header k)
        (card highlight idx))))

(defn left-card-preview [idx k]
  [:div.w-full.shadow-4.rounded-sm.bg-gray-200.opacity-25
   {:key (str "card-" idx)}
   [:div.pt-1.font-bold.text-xs
    [:div.flex.justify-between.items-center
     [:p.text-4xl.font-sans.text-gray-500.px-2 k]]
    [:div.w-full.rounded-sm.bg-gray-100.p-1
     {:key (str "card-" idx)}
     [:div {:class [:px-2 :py-1]}
      (group idx k age-groups :app.boat-selector/age)
      (group idx k gender-groups :app.boat-selector/gender)]]]])

(defn status-card [_idx [boatnumber entity]]
  [:div.w-full.select-none.rounded-sm
   {:key      (str "card-" boatnumber)
    :on-click #(rf/dispatch [:app.boat-selector/toggle boatnumber])
    :class    (condp = (:special entity)
                1 [:bg-red-800]
                2 [:bg-blue-700]
                3 [:bg-green-700]
                [:bg-black :text-white])}
	 ;; container
   [:div.bg-gray-100.text-black.h-full.p-2 {:style {:display :grid
                             :grid-template-columns "fr fr"
                             :grid-template-rows "fr fr fr"}}
    [:p.h-full.text-2xl.font-black boatnumber]
    [:div.text-xl.rounded-full.w-8.h-8.center.font-bold
     {:style {:justify-self :end}
      :class (flatten (case (:material entity)
                        0 [:text-white :bg-green-800]
                        [:text-black  :bg-blue-600]))}
     (inc (:level entity))]

    [:div.text-xxs {:style {:align-self :start
                            :grid-column "1/span 2"
                            :justify-self :start}}
     (:description entity)]
    ;[:div.text-xl {:style {:align-self :end}} (:category entity)]

    [:div.text-xl {:style {:align-self :end}} (:category entity)]
    [:div.text-xl {:style {:align-self :end :justify-self :end}} (str (:weight entity))]

    #_[:p (str entity)]]
   #_(condp = (:special entity)
       1 [:div {:class [:p-1]
                :style {:display               :grid
                        :grid-template-columns "1fr 2fr"
                        :grid-template-rows    :1fr
                        :grid-auto-flow        :columns}}
          [:div.text-xs
           [:div.text-xs.font-bold boatnumber]
           [:div.text-red-500.text-xxs "Defekt"]]
          [:div.text-xxs
           [:div "21. August 2019"]
           [:div "39 dager"]]]
       2 [:div {:class [:p-1 :bg-green-800]
                :style {:display               :grid
                        :grid-template-columns "1fr 2fr"
                        :grid-template-rows    :1fr
                        :grid-auto-flow        :columns}}
          [:div.text-xxs
           [:div.text-xs.font-bold boatnumber]
           [:div.text-gray-500.text-xxs "Utlånt"]]
          [:div.text-xxs.flex.justify-center.flex-col.items-end.bg-gray-900.p-1
           [:div.text-white "14:00->"]
           [:div.text-yellow-600 "2:30"]
           (let [r (rand-int 50)]
             [:svg {:width   "100%"
                    :height  :auto
                    :viewBox "0 0 100 10"}
              [:rect {:x 0 :y 0 :width 100 :height 10 :fill 'black}]
						;;todo: implement a graph
              [:path {:d "M0 10H5V5H5v10h5H15" :stroke 'white}]
						;;[:line {:x1 0 :y1 10 :x2 100 :y2 10 :stroke 'black}]
              [:rect {:x      r
                      :y      4
                      :width  (+ r (rand-int 50))
                      :height 4
                      :stroke 'none
                      :fill   'white}]])]]
       3 [:div {:class [:bg-blue-700 :h-full]} "3"]
       [:div.flex.justify-between.items-start.h-full.p-1.bg-gray-700.
        [:div.text-xs.font-sans.font-bold.text-black boatnumber]
        [:div.text-xxs.text-gray-300
         {:key boatnumber}
         "2:45"]])])
