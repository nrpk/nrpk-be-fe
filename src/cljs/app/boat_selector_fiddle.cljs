(ns app.boat-selector-fiddle
  (:require [app.boat-selector :as bsel]
            [cljs.pprint :refer [pprint]]))

(defn cmd-set-age [id age]
  [:selector/set id :selector/age age])

(defn cmd-set-gender [id gender]
  [:selector/set id :selector/gender gender])

(def program1
  [[::reset-selection]
   [::set-focus :phonenumber]
   [::clear]
   [::set-focus :boatnumber]
   [::clear]
	 ;;[::append 1]
	 ;;[::append 2]
	 ;;[::append 3]
	 ;;[::set 209 ::age 0]
	 ;;[::set 209 ::gender 1]

	 ;[::set 209 ::agea 11]

   (cmd-set-age 202 1)
   (cmd-set-gender 202 1)

	 ;;[::set 202 ::age 1]
	 ;;[::set 202 ::gender 1] ;test comment

   [::set 2031 ::gender 1]
   [::set 2031 ::age 1]

   [::set 2 ::age 1]
   [::set 2 ::gender 1]

	 ;;[::select 3331]

	 ;;[::select 203]

	 ;;[::toggle 33321]
	 ;;[::toggle 203]
	 ;;[::select 202]
	 ;;[::select 490]
	 ;;[::cancel-selection 203]
])

(comment
  (do
					 ;;(add-tap #(prn %))
					 ;(remove-tap #(prn %))

    (cmd [:clear nil
          :set-focus 1])))

#_(let [storage (atom 0)]
    (defn loggit [e]
      (prn (with-out-str (cljs.pprint/pprint e)))))

(comment
  (loggit {:a 1}))



;(reset! tapset (atom nil))
;@tapset


(comment
  (defn out [e] (prn (with-out-str (pprint e))))
  (add-tap out)
  (remove-tap out))


	(defn cmd [& a]
    (keep (fn [[k v]]
            (prn k)
            (case k
              :set-focus [::bsel/set-focus v]
              :clear [::bsel/clear v]
              :mode [::bsel/set-mode v]
              :set-wide [::bsel/set-wide v]
              :append [::bsel/append v]
              (conj [:error] {:unknown-cmd {k v}})))
          (partition 2 a)))

	(def program2
    (-> (into []
              cat [;(cmd :test-step [1 2 3])
                   (cmd :mode :out)
									 (cmd :set-focus :boatnumber)
                   (cmd :clear nil)
                   (cmd :set-wide true)
                   (cmd :append 12)
                   (map #(vector ::bsel/select %) [129 199 200])
                   #_(cmd :set-focus :phonenumber
                        :other-stuff :high
                        :clear 123
                        :unknown 321
                        :unknownsdasd 321)])))
	;(filter #(not= :error (first %)) program2)

(comment
  [[:bsel/reset-selection]
   (map #(vector :bsel/select %) [129 199 200])
   (cmd :set-focus :phonenumber
        :other-stuff :high
        :clear 123)
   (map #(vector ::bsel/append %) (range 10))
   [[::bsel/set-focus :boatnumber]
    [::bsel/clear]
    [::bsel/set-mode :status]
    [::bsel/set-wide false]]])

(pprint program2)
