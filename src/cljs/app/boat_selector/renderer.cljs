(ns app.boat-selector.renderer
  (:require [re-frame.core :refer [subscribe reg-sub]]
            [cljs.core.match :refer [match]]
            [arcs.dial]
            [app.listitem :as li]
            [app.boat-selector-grid :as g]
            [app.boat-selector :as bsel]
            [app.boat-selector.admin-toolbar :as admin-toolbar]
            [app.boat-selector.dial-data :as dial-data]
            [app.boat-selector-numberpad :as numberpad]
            [arcs.dial-definitions :as dial-def]
            [re-frame.core :as rf]
            [app.boat-selector-card :as c]))

(def $toolbar "toolbar")
(def $topline "top-line")
(def $split "split")
(def $last "last")

(defn- build
  "builds a string like a/b/c/d for use with grid"
  [& xs]
  (apply str (interpose "/" xs)))

(defn available-boats []
  (let [available-boats @(subscribe [:app.core/boats-matching-input])]
    [:div.scroll-y
     {:style {:display               :grid
              :grid-template-columns "repeat(auto-fit,minmax(200px,1fr))"
              :grid-auto-rows        "minmax(144px,188px)"
              :grid-gap              5}}
    (for [e available-boats]
      (c/status-card 0 e))]))

    #_[:div.scroll-y.p-1.bg-red-500
     (li/list-col-simple
       {:empty-fn (constantly [:div.h-full.center.bg-blue-400. [:p..text-white.text-xl "Finner ingenting"]])
        :item-fn  c/status-card}
       available-boats)]


(def windows
  "lookup-table of components"
	;todo: add key
  {::status-wide         {:grid    "top-line/content/last/split"
                          :content [bsel/render-status]}

   ::status              {:grid    "top-line/content/last/toolbar"
                          :content [bsel/render-status]}

   ::available-boats     {:grid    (build $topline $split $last $toolbar)
                          :content [available-boats]}

   ::confirmation        {:grid    "last/content/bottom/split"
                          :content (bsel/confirm-action-button)}

   ::input               {:grid    "top-line/left/mid-split/content"
                          :content [bsel/user-data-input]}

   ::input-filter        {:grid    "top-line/left/mid-split/content"
                          :content [bsel/user-data-input-filter]}

   ::numberpad           {:grid    "mid-split/left/last/content"
                          :content [numberpad/numberpad]}

   ::numberpad-large     {:grid    "mid-split/left/bottom/content"
                          :content [:div.flex-col.flex.justify-end [numberpad/numberpad]]}

   ::selection-list      {:grid    "top-line/content/last/split"
                          :content [bsel/list-of-selected-boats]}

   ::selection-list-wide {:grid    "top-line/content/last/toolbar"
                          :content [bsel/list-of-selected-boats]}

   ::admin-toolbar       {:grid    "top-line/toolbar/last/right"
                          :content [li/inset [admin-toolbar/toolbar]]}

   ::debug               {:grid    "1/2"
                          :content [:pre.z-50.text-xxs.text-white.w-128 {:key "debug-out"}
                                    (with-out-str
                                      (cljs.pprint/pprint (dial-def/sample-simple-timeline arcs.dial/draw-arc {})))]}
   ::signal              {:grid    "top/toolbar/top-line/right"
                          :content (bsel/signal)}

   ::mode-buttons        {:grid    "top/left/top-line/content"
                          ; "g-mode-buttons"
                          :content [bsel/mode-buttons]}

   ::empty-epoch         {:grid    "top/split/top-line/toolbar"
                          :content [:div.bg-gray-900]}

   ::epoch-fullscreen    {:grid    "top-line/left/last/toolbar"
                          :content [bsel/epoch-window
                                    [dial-data/data-source-1
                                     dial-data/data-source-2
                                     dial-data/data-source-3
                                     dial-data/data-source-4]]}

   ::epoch-tall          {:grid    "top/split/last/toolbar"
                          :content [bsel/epoch-window
                                    [dial-data/data-source-1
                                     dial-data/data-source-2
                                     dial-data/data-source-3
                                     dial-data/data-source-4]]}
   ::epoch               {:grid    "top/split/top-line/toolbar"
                          :content [bsel/epoch-window
                                    [dial-data/data-source-1
                                     dial-data/data-source-2
                                     dial-data/data-source-3
                                     dial-data/data-source-4]]}})

(defn helper [z]
	;"check the :grid of x"
  (match [(:grid z) (:grid-name z) (:content z)]
    [grid nil x] (g/on-grid grid x)
    [nil cls x]  (g/on-grid-class cls x)
    :else [:pre.text-white.text-xxs "ERROR"]))

(defn screen-out [wide?]
  (->> [::mode-buttons
        (if wide? ::selection-list ::selection-list-wide)
        ::input
        ::numberpad-large
        (when wide? ::available-boats)
        ::epoch
        ::admin-toolbar
        ::signal
        ::confirmation]
       (filter some?)
       (map #(-> windows % helper))))

(defn screen-in [wide?]
  (->> [::mode-buttons
        (if wide? ::selection-list ::selection-list-wide)
        ::input
        ::numberpad-large
        (when wide? ::available-boats)
        ::signal
        ::admin-toolbar
        ::epoch
        ::confirmation]
       (filter some?)
       (map #(-> windows % helper))))

(defn screen-status [wide?]
  (->> [::mode-buttons
        (if wide? ::status-wide ::status)
        ::input-filter
        ::admin-toolbar
        ::numberpad-large
        ::signal
        (if wide? ::epoch-tall ::epoch)]
       (map #(-> windows % helper))))

(defn screen-dial []
  [:<>
   (g/on-grid "top-line/left/last/toolbar" (arcs.dial/render-sample))
   (->> [::admin-toolbar
         ::mode-buttons
         ::signal
         ::confirmation]
        (map #(-> windows % helper)))])

(defn playground [wide?]
  (case wide?
    true (->> [::mode-buttons
               (comment
                 #_(if wide? ::selection-list ::selection-list-wide)
                 ::input
                 ::numberpad
                 #_(if wide? ::available-boats)
                 ::epoch
                 ::confirmation)
               ::admin-toolbar
               ::empty-epoch
               ::signal
               ;::input
               ;::numberpad-large
               ::epoch-fullscreen]
              (filter some?)
              (map #(-> windows % helper)))
    (screen-in false)))

(reg-sub ::message (fn [db [_]] (:message db)))

(defn render "a construction yard"
  []
  (let [mode (subscribe [::bsel/mode])
        wide-mode? (subscribe [::bsel/wide])
        ;boats @(subscribe [::bsel/boats])
        ;available-boats @(subscribe [:app.core/boats-matching-input])
        ;total @(subscribe [:app.core/total-avail-boats])
        ]
    (fn []
      [:div.grid-page-numberpad.h-full._border-box.bg-gray-800 ;;.w-screen.h-screen
       {:style {:display               :grid
                :grid-template-columns "[left-margin] 0px [left] 320px [content] 1fr [split] 1fr [toolbar] 80px [right] 0 [right-margin]"
                :grid-template-rows    "[top-margin] 0px [top] 80px [top-line] auto [mid-split] 1fr [last] minmax(auto, 80px) [bottom] 0 [bottom-margin]"
                :gap                   :10px}}

       (comment
         (g/on-grid "top/left-margin" [:div.bg-orange-500 "1"])
         (g/on-grid "top-line/left-margin" [:div.bg-orange-600.h-full.flex-1.flex-grow.flex-col "2"])
         (g/on-grid "mid-split/left-margin" [:div.bg-orange-700.h-full.flex-grow.flex-col "3"])
         (g/on-grid "last/left-margin" [:div.bg-orange-800.h-full.flex "4"])
         (g/on-grid "bottom/left-margin" [:div.bg-orange-900])

         (g/on-grid "top-margin/left" [:div.bg-green-600 "a"])
         (g/on-grid "top-margin/content" [:div.bg-green-700 "b"])
         (g/on-grid "top-margin/split" [:div.m-0.bg-green-800 "c"])
         (g/on-grid "top-margin/toolbar" [:div.m-0.bg-green-800 "d"]))
       ;(g/on-grid "top-margin/right" [:div.m-0.bg-green-900])


       (g/on-grid "last/split/bottom/toolbar" [:div.center.text-white.font-bold.text-xl.bg-gray-900 @(subscribe [::message])])

       (let [w @wide-mode?]
         (case @mode
           :out (screen-out w)
           :status (screen-status w)
           :in (screen-in w)
           :dial (screen-dial)
           (playground w)))])))

(defn render' []
  (let [s (rf/subscribe [::admin-toolbar/next-color-fx])]
    (fn []
      [:div.snap-y.scroll-y.h-full
       {:style {:filter (case @s 0 "brightness(1.1)"
                                 1 "invert(100%)"
                                 2 "hue-rotate(60deg)"
                                 3 "blur(4px)"
                                 "grayscale(100%)")}}
       [:div.h-full.w-screen.border-box.snap-start
        [render]]
       [:div.h-full.w-screen.snap-start.bg-gray-700.flex.p-2.flex.flex-grow.w-full.debug
        [bsel/epoch-window
         [dial-data/data-source-1
          dial-data/data-source-2
          dial-data/data-source-3
          dial-data/data-source-4]]
        #_[:div.h-full "sEXTRA"]]
       #_[:div.h-screen.snap-start.bg-gray-900
        [:div.h-full "sEXTRA 2"]]])))
