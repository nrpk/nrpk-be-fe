(ns app.boat-selector.spec
  (:require [cljs.spec.alpha :as s]
            [app.boat-selector-card :as c]))

(s/def ::boatnumber int?)
(s/def ::age #(<= 0 % (count c/age-groups)))
(s/def ::gender #(<= 0 % (count c/gender-groups)))
(s/def ::stats-map (s/and
										 ;(s/map-of keyword? int?)
                    (s/map-of keyword? (s/map-of keyword? int?))
                    (s/every-kv keyword? (s/keys :req [::age ::gender]))))
(s/def ::selection-spec (s/and #(> (count %) 0)
                               (s/every-kv int? ::stats-map)))
(s/def ::header (s/keys :req-un [::header]))
(s/def ::content (s/keys :req-un [::content]))

;; todo: figure out this one
(s/def ::card-details
  (s/and map?
         (s/map-of ::header (s/cat :a vector?))
         (s/keys :req-un [::left-card
                          ::right-card-normal
                          ::right-card-selected
                          ::right-card-defect])))
;;(s/def ::header-and-content (s/keys :req-un [::header ::content]))

(s/check-asserts false)
