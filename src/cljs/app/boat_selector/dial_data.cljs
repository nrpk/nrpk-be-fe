(ns app.boat-selector.dial-data)

;;; value-helpers

(defn value-start [n]
  {:start n :end 0})

(defn value-end [n]
  {:start 0 :end n})

(defn value-start-end [s e]
  {:start s :end (- 1 e)})

(def every-minute {:stepsize    10
                   :class       [:text-white]
                   :outer-inset 0.3
                   :inner-inset 0.1
                   :key-id "every-15-deg"
                   :style       {:stroke-width 10}})

(def every-5-minute {:stepsize    (/ 360 3)
                     :class       [:text-white]
                     :outer-inset 0
                     :inner-inset 0
                     :style       {:stroke-width 0.5}})

(def every-15-minute {:stepsize     20
                      :class       [:text-black]
                      :insets       [-0.1 0.9]
                      :key-id       "every-15-minute"
                      :style        {:stroke-width 5}})

(def data-source-3
  {:r           [200 330]
   :theta       [0 259]
   :data-series {:min      0
                 :max      100
                 :length   20
                 :stepsize 15
                 :data-fn  (fn [n] (/ (* 50 (Math/abs (Math/cos (/ n 20))))
                                      1))}
   :class       [:text-pink-700]
   :outer-inset 0.1
   :inner-inset 0.1
   :insets      [0.1 0.1]
   :key-id      "sample-simple-timeline-4"
   :ticks       [{:stepsize 13
                  :class    [:text-white]
                  :insets   [0.9 0]
                  :key-id   "every-15-deg"
                  :style    {:stroke-width 2}}]
   :value-bars  [{:style {:stroke-width   30
                          :stroke-linecap 'butt}
                  :class [:text-gray-900]
                  :kind  :stroke
                  :value {:start 0.8 :end 0.19}}
                 {:style {:stroke-width   30
                          :stroke-linecap 'butt}
                  :class [:text-orange-900]
                  :kind  :stroke
                  :value {:start 0.2 :end 0.3}}
                 {:style {:stroke-width   10
                          :stroke-linecap 'butt}
                  :class [:text-orange-500]
                  :kind  :stroke
                  :value {:start 0 :end (- 1 0.9)}}]})

(def data-source-4
  {:class       [:text-orange-400]
   :outer-inset 0.2
   :inner-inset 0.2

   :key-id      "sample-simple-timeline-2"
   :r           [50 100]
   :theta       [100 290]
   :ticks       [every-15-minute]
   :value-bars  [{:style {:stroke         'red
                          :stroke-width   5
                          :stroke-linecap 'butt}
                  :kind  :stroke
                  :value {:start 0 :end 0.2}}]})

(def data-source-2
  {:r           [330 360]
   :theta       [0 259]
   :data-series {:min      0
                 :max      10
                 :length   120
                 :stepsize 15
                 :data-fn  (fn [n] (/ (* 50 (Math/abs (Math/cos (/ n 20))))
                                      1))}
   :class       [:text-pink-700]
   :outer-inset 0.1
   :inner-inset 0.1
   :insets      [0.1 0.1]
   :key-id      "sample-simple-timeline-3"
   :ticks       [{:stepsize 13
                  :class    [:text-white]
                  :insets   [0.9 0]
                  :key-id   "every-15-deg"
                  :style    {:stroke-width 2}}]
   :value-bars  [{:style {:stroke-width   13
                          :stroke-linecap 'butt}
                  :class [:text-gray-100]
                  :kind  :stroke
                  :value {:start 0.1 :end 0.19}}
                 {:style {:stroke-width   30
                          :stroke-linecap 'butt}
                  :class [:text-orange-900]
                  :kind  :stroke
                  :value {:start 0.2 :end 0.3}}
                 {:style {:stroke-width   11
                          :stroke-linecap 'butt}
                  :class [:text-orange-500]
                  :kind  :stroke
                  :value {:start 0 :end (- 1 0.9)}}]})

(def data-source-1
  (let [timer1 1 timer2 2]
    {:class       [:text-blue-900]
     :insets      [0.1 0.1]
     :key-id      "sample-simple-timeline-1"
     :r           [100 120]
     :theta       [-20 200]
     :style       {:stroke 'black :stroke-width 2 :fill 'none}
     :ticks       [{:stepsize 3
                    :class    [:text-white]
                    :insets   [0.1 0.8]
                    :key-id   "every-15-deg"
                    :style    {:stroke-width 2}}]
     :data-series {:min      0
                   :max      100
                   :length   20
                   :stepsize 5
                   :data-fn  (fn [n] (/ (* 50 (Math/abs (Math/cos (/ n 20))))
                                        1))}
     :value-bars  [{:style       {:stroke-width   5
                                  :stroke-linecap 'butt}
                    :outer-inset 0.5
                    :class       [:text-gray-600]
                    :kind        :stroke
                    :value       (value-start-end timer2
                                                  (- 1 (/ timer1 10)))}

                   {:style       {:stroke-width   10
                                  :stroke-linecap 'butt}
                    :outer-inset 0.5
                    :inner-inset 0.1
                    :class       [:text-gray-500]
                    :kind        :stroke
                    :value       (value-start-end timer1
                                                  (+ 0.1 timer1))}]}))
