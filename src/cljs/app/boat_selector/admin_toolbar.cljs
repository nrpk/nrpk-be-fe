(ns app.boat-selector.admin-toolbar
  (:require [re-frame.core :as rf]))

(rf/reg-event-db ::undefined (fn [db [_ s]] (assoc db :message (str "undefined " s))))

(rf/reg-event-db ::get-next-color-fx (fn [db _] (update db :color-toggle #(if (< % 4) (inc %) 0))))

;:app-boat-selector.admin-toolbar/color-toggle
(rf/reg-sub ::next-color-fx (fn [db _] (:color-toggle db 0)))

(defn square [_] [:svg {:viewBox "0 0 10 10"}
                  [:rect {:x 1 :y 1 :width 8 :height 8 :stroke 'currentColor :stroke-width 1}]])

(defn triangle
  ([]
   [:svg {:viewBox "0 0 10 10"}
    [:path {:d "M1 9h8L5 2z" :stroke 'currentColor :stroke-width 1}]])
  ([state]
   [:div
    state
    [:svg {:viewBox "0 0 10 10"}
     [:path {:d "M1 9h8L5 2z" :stroke 'currentColor :stroke-width 1}]]]))

(defn circle [state]
  [:div {:class (if state [:text-red-600] [:text-black])}
   [:svg {:viewBox "0 0 10 10" :width "auto"}
    [:circle {:cx   5 :cy 5 :r 4 :stroke "rgba(0,0,0,0.5)" :stroke-width 1
              :fill 'currentColor #_(if wide-mode? 'currentColor 'none)}]]])

(defn circle-badge [_]
  [:div
   [:svg.content-box.m-1 {:viewBox "0 0 10 10" :widths "100%"}
    [:circle {:cx   5 :cy 5 :r 3 :stroke "rgba(0,0,0,0.5)" :stroke-width 1
              :fill 'currentColor}]]
   [:div.absolute.top-0.right-0
    [:div.m-0.p-0.text-base.text-green-200.bg-pink-900.font-black.rounded-full.w-8.h-8.center.shadow-4.border
     {:style {:margin "-30% 30%"}}
     (inc (rand-int 20))]]])

(defn toolbar []
  (let [data [[triangle {:msg [::get-next-color-fx]
                         :state  @(rf/subscribe [::next-color-fx])}]
              [square {:msg [::undefined "square"]}]
              [circle {:state @(rf/subscribe [:app.boat-selector/wide])
                       :msg [:app.boat-selector/toggle-wide]}]
              [circle-badge {:msg [::undefined "badge"]}]]]
    [:div
     {:style {:display               :grid
              :grid-template-columns "repeat(auto-fill,1fr)"
              :grid-auto-rows        :auto
              :grid-gap              5}}
     (for [[e {:keys [msg state] :or {msg [::undefined "error"]  state false}}] data]
       [:div.relative
        [:button.w-full.h-full
         {:class    [:p-2
                     :bg-teal-900
                     :shadow-4
                     :h-12 :w-12
                     :rounded-sm]
          :on-click #(rf/dispatch msg)}
         (e state)
         ;badge
]])]))
