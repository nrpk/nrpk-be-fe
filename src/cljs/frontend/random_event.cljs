(ns frontend.random-event
  (:require [re-frame.core :as rf]
            [clojure.string :as string]
            [core.aggregation :as aggr]
            [frontend.time2 :refer [next-time]]
            [panel.logging :as log]))

(defn- make-add-boat [{:keys [boats n retries]}]
  (loop [id (rand-int n)
         r retries]
    (if (not (get boats id))
      (aggr/add-boat id)
      (if (zero? r)
        (aggr/nothing-happens id)
        (recur (rand-int n) (dec r))))))

(defn- make-set-boat-in-repair [{:keys [retries n boats]}]
  (loop [id (rand-int n)
         r retries]
    (if (not (zero? r))
 ; fixme
      (if (= :boat/busy (:boat/status (get boats id)))
        (aggr/set-boat-in-repair id)
   ;(aggr/assign-boat-name id (rand-nth (string/split "this is the name" #" ")))
        (do
     ;(log/i "cannot set a ")
          (recur (rand-int n) (dec r)))))))

(defn- make-assign-boat-name [{:keys [retries n boats]}]
  (loop [id (rand-int n) r retries]
    ;(log/i (keys boats) id r)
    (if (not (zero? r))
      (if (contains? (set (keys boats)) id)
        (aggr/assign-boat-name id (rand-nth (string/split "this is a long string with different names and such" #" ")))
        (do
     ;(log/i "shit")
          (recur (rand-int n) (dec r)))))))

(defn random-event [_]
  (let [aggregation (rf/subscribe [:aggregation])
        boats (:boats @aggregation)
        n 3
        retries n
        config {:boats boats
                :n n
                :retries retries}
        i (rand-nth
						[(make-add-boat config)
						 (aggr/rent-start (-> config :n rand-int) (next-time))
						 (aggr/rent-end (rand-int n) (next-time))
						 (aggr/set-boat-in-repair (rand-int n))
						 (aggr/set-boat-out-of-repair (rand-int n))

						 ;(make-set-boat-in-repair config)
						 ;(make-assign-boat-name config)
						 ])]
    ;(log/i "aggr " (:boats @aggr))
    (log/i "RANDOMEVENT " (str i))
    i))

