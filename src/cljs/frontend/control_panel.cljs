(ns frontend.control-panel
  (:require [re-frame.core :as rf]
            [frontend.timers :refer [stop-timer exec-with-timer]]
            [core.appearance :refer [ccp c-merged->class c-class]]
            [frontend.random-event :refer [random-event]]
						[frontend.q :as q]))

(rf/reg-event-db
 :close-control-panel
 (fn [db _]
    ;(println :close-control-panel (:ui/control-panel-open db))
   (assoc db :ui/control-panel-open false)))

(rf/reg-event-db
 :open-control-panel
 (fn [db _]
   (assoc db :ui/control-panel-open true)))

(rf/reg-event-db
 :toggle-control-panel
 (fn [db _]
   (update db :ui/control-panel-open not)))

(rf/reg-event-db
 :system/restart
 (fn [db _] ()
   (rf/dispatch-sync [:system/pause])
   (assoc db :ui/aggregation nil
              ;fixme correct?
          :ui/running-sim false)))

(rf/reg-event-db
 :system/pause
 (fn [db _] ()
   (prn :pause-system (:ui/running-sim db))
    ; fixme sideeffect
   (if (:ui/running-sim db)
		 (stop-timer)
		 (exec-with-timer random-event))
   (update db :ui/running-sim not)))

(rf/reg-sub
 :generator/running?
 (fn [db _] ()
   (get db :ui/running-sim false)))

(rf/reg-sub
 :control-panel-open
 (fn [db _]
   (get db :ui/control-panel-open false)))

; todo quick refactor use interleave (interpose?)
(letfn [(tiny-icon [w]
          [:div (ccp :icon/tiny) (panel.icons/icon' w)])
        (button
         ([command content]
          (button command content :button.style/normal))
					;---
         ([command content style]
          [:button.px-2.py-2.border {:style {}} content]))

        (button-pause
         [running? at-start-epoch?]
         (let [label (if running?
                       (if at-start-epoch?
                         "start"
                         [:div
                          [:p.text-center "kjører"]
                          [:p "klikk for å pause"]])
                       (if at-start-epoch?
                         "start"
                         "fortsett"))]
           (button :system/pause
                   [:div (c-class [:xy/center] {:class (when running? "blinking")}) label])))
        (button-sendlog
         []
         (button :system/send-log! "send log..." :button.style/opens-dialog))

        (button-close
         []
         (button :close-control-panel
                 (tiny-icon :double-chevron-down)))

        (button-nullstill
         [epoch]
         (button :system/restart
                 "nullstill epoch"
                 (if (zero? epoch)
                   :button.style/disabled
                   :button.style/danger)))]
  (defn render [aggr state]
    (fn []
      (let [generator-running? (rf/subscribe [:generator/running?])
            epoch (get @aggr :event/epoch 0)
            at-start-epoch? (or (= 1 epoch)
                                (nil? epoch)
                                (zero? epoch))]
        [:div.fixed.bottom-0.right-0.mx-10.bg-gray-800.pt-2.rounded-t.opacity-90.z-50
						; note: this is crazy over-complicated, replace!
						;(ccp :class/control-panel-window)
         (when @state
           [:div.mb-4.text-white.bg-gray-900.p-2
            (interpose [:div.my-2]
                       [button-close
                        (button-pause @generator-running? at-start-epoch?)
                        (button-nullstill epoch)
                        button-sendlog
                        [:div.flex.w-full.justify-between.h-12
                         (interpose [:div.mx-1]
                                    [(button :nop
                                             (tiny-icon :sun))
                                     (button :nop
                                             (tiny-icon :chat-bubble))
                                     (button :nop
                                             (tiny-icon :return-up))])]
                        [:div.flex.w-full.justify-between.h-12
                         (interpose [:div.mx-px]
                                    [(button :nop (tiny-icon :circle-minus))
                                     (button :nop  (tiny-icon :circle-minus))])]])])
						;;running epoch
         [:div.text-xs.font-mono.font-bold.bg-gray-900.text-white
          {:on-click #(rf/dispatch [:toggle-control-panel])}

          [:div.flex.px-2.pt-2
           [:div.leading-tight.flex-grow
            [:p "Epoch "]
            (if at-start-epoch?
              [:div (str epoch)]
              [:p epoch "/"
               [:span.text-grey-dark.xtext-xs (count (:errors @aggr))] "/"
               [:span.text-xxsx.text-blue (take 4 (str (/ (count (:errors @aggr)) epoch)))]])]
           [:div (ccp :icon/tiny) (if @state (panel.icons/icon' :double-chevron-down) (panel.icons/icon' :double-chevron-up))]]]]))))



