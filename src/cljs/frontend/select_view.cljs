(ns frontend.select-view
  (:require [re-frame.core :as rf]
						[panel.events :as events2]
            [clojure.pprint :refer [pprint]]
            [core.appearance :refer [cfg-class ccp m-into c-merged->class]]
            [panel.icons :refer [icons]]
            [frontend.common :as common]
            [frontend.textfield :refer [text-field]]
            [frontend.numberpad :refer [number-input-pad button-pad button enabler-fn]]))

(declare card-content)

(defn- button-2lines
  ([a & c]
   (merge {:content [:div.text-center.flex.flex-col.justify-around.h-full.py-2 c]
           :class [:h-16]
           :action #(rf/dispatch a)})))

(defn year-group []
  (let [g [(button-2lines []
                          [:p.text-white "ungdom"]
                          [:p.text-xs.text-grey.font-thin "13 - 30"])
           (button-2lines []
                          [:p.text-white "voksen"]
                          [:p.text-xs.text-grey.font-thin "40 – 63"])
           (button-2lines []
                          [:p.text-white "eldre"]
                          [:p.text-xs.text-grey.font-thin "duddilledum"])]]
    [:div.w-full
     (button-pad 1 (partition 3 3 g))]))

(defn other-group []
  (let [g [(button "min gruppe" {} [])
           (button "bare deg" {} [])
           (button "kurs..." {} [])
           {:class [:invisible]}]]
    [:div
     [:p (cfg-class :group-header) "hvem låner?"]
     (button-pad 1 (partition 2 2 g))]))

(defn data-group [data]
  (let [l (count data)
        g (map #(button % {} []) data)]
    [:div
     (button-pad 1 (partition l l g))]))

(defn gender-group []
  (let [g [(button "kvinne" {} [])
           (button "mann" {} [])]]
    [:div.w-full (cfg-class :well)
     (button-pad 1 (partition 2 2 g))]))

(rf/reg-sub
 :phone-number-toggle
 (fn [db _]
   (:ui/phone-number-toggle db)))

(rf/reg-sub
 :havekey-toggle
 (fn [db _] (:ui/havekey-toggle db)))

(rf/reg-event-db
 :toggle-leave-number
  ;[events2/insert-db-path]
 (fn [db _]
   (if (:ui/phone-number-toggle db)
     (-> db
         (assoc :ui/input-switcher-previous (:ui/input-switcher db))
         (update :ui/phone-number-toggle not)
         (assoc :ui/input-switcher (:ui/input-switcher-previous db)))
     (-> db
         (assoc :ui/input-switcher-previous (:ui/input-switcher db))
         (update :ui/phone-number-toggle not)
         (assoc :ui/input-switcher :field/phone-number)
         #_(assoc :ui/pad-phone-number "")))))

(rf/reg-event-db
 :toggle-havekey
  ;[events2/insert-db-path]
 (fn [db _]
   (update db :ui/havekey-toggle not)))

(defn havekey-group []
  (let [y (rf/subscribe [:havekey-toggle])
        x (rf/subscribe [:phone-number-toggle])
        g [(button (if @y "Har med Nøkkel" "Har ikke med nøkkel")
                   [:h-16
                    (if @y :bg-orange-light :bg-black)
                    (if @y :text-black :text-white) :shadow-lg  :border-orange-light :border-4 :text-center :rounded]
                   [:toggle-havekey])

           (button (if @x "Ikke legg igjen telefonnummer" "Legg igjen telefonnummer")
                   [:h-16 :_mx-2 (when @x :bg-blue) (if @x :text-black :text-white) :border-blue :border-4 :rounded :text-center]
                   [:toggle-leave-number])]]
    [:div.w-full.p-4 (cfg-class :well)
     (button-pad 1 (partition 2 1 g))]))

;;
;;
;;
;; inputs

(rf/reg-event-db
 :clicked-on-phone-number
  ;[events2/insert-db-path]
 (fn [db _]
   (assoc db :ui/input-switcher :field/phone-number)))

(rf/reg-event-db
 :clicked-on-boat-number
  ;[events2/insert-db-path]
 (fn [db _]
   (assoc db :ui/input-switcher :field/boat-number)))

(rf/reg-event-db
 :clicked-on-amount-number
  ;[events2/insert-db-path]
 (fn [db _]
   (assoc db :ui/input-switcher :field/amount-number)))

(rf/reg-sub :input-switcher #(:ui/input-switcher %))

(rf/reg-event-db
 :clicked-on-number-pad
 [events2/insert-db-path]
 (fn [db [_ k]]
   (let [k (str k)]
     ;(log/i k (dissoc db :stuff :data))
     (case (:ui/input-switcher db)
       :field/phone-number (assoc db
                                  :ui/pad-phone-number (apply str (take 8 (str (get db :ui/pad-phone-number "") k))))
       :field/boat-number (assoc db
                                 :ui/pad-boat-number (apply str (take-last 3 (str (get db :ui/pad-boat-number "") k))))

       :field/amount-number (assoc db
                                   :ui/pad-amount-number (apply str (take-last 2 (str (get db :ui/pad-amount-number "") k))))
       db))))

(rf/reg-event-db
 :clicked-on-kill-input
 [events2/insert-db-path]
 (fn [db _]
   (let [f (case (:ui/input-switcher db)
             :field/phone-number :ui/pad-phone-number
             :field/boat-number :ui/pad-boat-number
             :field/amount-number :ui/pad-amount-number
             nil)]
     (if f
       (assoc db f (subs (f db) 0 (dec (count (f db)))))
       db))))

;;
;;
;;
;; outputs

(rf/reg-sub :pad-boat-number (fn [db _] (:ui/pad-boat-number db)))

(rf/reg-sub :pad-phone-number (fn [db _] (:ui/pad-phone-number db)))

(rf/reg-sub :pad-amount-number (fn [db _] (:ui/pad-amount-number db)))

(rf/reg-sub :havekey-toggle (fn [db _] (:ui/havekey-toggle db)))

;;
;;
;;
;; views

(rf/reg-event-db
 :add-person-to-boat
 (fn [db _]
   (update db :ui/person-data-for-boat conj {})))

(rf/reg-sub
 :person-data-for-boat
 (fn [db _]
   (:ui/person-data-for-boat db)))

(rf/reg-event-db
 :detail-group-clicked
 (fn [db [_ {:keys [idx field value]}]]
   (println "DETAILS " idx field value)
   (update-in db [:ui/person-data-for-boat idx field] (constantly value))))

(comment
  (let [db {:x []
            :f [{:a 1} {:b 2}]}]
    (println (:f db))
    (update db :f #(assoc (:f db) 0 {:c 'x})))
  ())

(defn- vec-remove
  "remove element in collection"
  [coll pos]
  (vec (concat (subvec coll 0 pos) (subvec coll (inc pos)))))

(rf/reg-event-db
 :remove-person-data
 (fn [db [_ arg]]
   (println "REMOVE " arg)
   ;(assoc db  : (vec-remove))))
   ;(keep-indexed #(if (not= %1 arg) %2) (db :ui/person-data-for-boat))
   (assoc db :ui/person-data-for-boat (vec-remove (db :ui/person-data-for-boat) arg))))

;;
;;
;;
;;

(def color-array
  [:bg-yellow-lighter :bg-purple-lighter :bg-teal-lighter  :bg-green-lighter :bg-red-lighter :bg-blue-lighter])

(defn bg-color [i]
  (first (drop i (cycle color-array))))

(defn- render-age-group [person-i age-data  person-data _]
  (let [bg (fn [i] (if (= i (:age person-data)) (bg-color person-i) :bg-grey-light))]
    [:div.flex.justify-between.h-12
     (interpose [:div.w-2px]
                (doall (->> age-data
                            (map-indexed (fn [i [k [top bottom]]]
                                           [common/button'
                                            {:key     (str person-i ",age," i)
                                             :value    1
                                             :on-click #(rf/dispatch [:detail-group-clicked {:idx person-i
                                                                                             :field :age
                                                                                             :value k}])
                                             :class    [(bg i)
                                                        :rounded-sm :p-1 :text-xxs :flex-grow (str "w-" "1/" (count age-data))]}
                                            [:div
                                             ;[:p (str person-i ",age," i)]
                                             ;[:p.bg-black.text-white.text-xxs (str person-data)]
                                             [:p.font-bold top]
                                             [:p bottom]]])))))]))

(defn- render-gender-group [person-i gender-data person-data _]
  [:div.flex.justify-between.h-12
   (interpose [:div.w-2px]
              (doall (for [[k v] gender-data]
                       [common/button''
                        {:key      (str person-i ",gender," k)
                         :on-click #(rf/dispatch [:detail-group-clicked {:idx person-i :field :gender :value k}])
                         :class    [(if (= k (:gender person-data)) (bg-color person-i) :bg-grey-light)
                                    :rounded-sm :p-1 :text-xs :flex-grow (str "w-" "1/" (count gender-data))]}
                        [:p.font-bold v]])))])

(defn- render-membership-group [person-i membership-data person-data]
  (let [column-width (str "w-1/" (count membership-data))
        bg (fn [k] (if (= k (:member person-data)) (bg-color person-i) :bg-grey-light))]
    [:div.flex.justify-between.h-12
     (interpose [:div.w-2px]
                (doall (for [[k v] membership-data]
                         [common/button''
                          {:key      (str person-i ",membersince," k)
                           :on-click #(rf/dispatch [:detail-group-clicked {:idx person-i :field :member :value k}])
                           :class    [(bg k) :rounded-sm :p-1 :text-xs :flex-grow column-width]}
                          [:p.font-bold v]])))]))

(defn- render-other-group [person-i other-data-1 person-data]
  (let [column-width (str "w-" "1/" (count other-data-1))]
    [:div.flex.justify-between.h-12
     (interpose [:div.w-2px]
                (doall (for [[k v] other-data-1]
                         [common/button''
                          {:key (str person-i ",other," k)
                           :on-click #(rf/dispatch [:detail-group-clicked {:idx person-i :field :other :value k}])
                           :class    [(if (= k (:other (get person-data person-i))) (bg-color person-i) :bg-grey-light)
                                      :rounded-sm :p-1 :text-xs :flex-grow column-width]}
                          [:p.font-bold v]])))]))

(defn statistics-input-pad [{:keys [show-label padding] :or {padding false show-label true}}]
  (let [color-array [:bg-yellow-lighter :bg-purple-lighter :bg-teal-lighter  :bg-green-lighter :bg-red-lighter :bg-blue-lighter]
        bg-color (fn [i] (first (drop i (cycle color-array))))
        person-data (rf/subscribe [:person-data-for-boat])] ; todo: naming
    [:div.rounded-b-lg
     (merge-with into
                 (cfg-class :light-well)
                 (when padding {:class [:p-4]}))
     (when show-label
       [:div.text-white.py-4 "Vi vil gjerne vite litt om deg for å kunne gjøre utvalget av utleie-materiale bedre tilpasset medlemmene."])

     (let [age-data {0 ["Barn" "-12"]
                     1 ["Ungdom" "13 - 19"]
                     2 ["Voksen" "20 - "]}

           gender-data {0 "Kvinne"
                        1 "Mann"}
           membership-data {0 "Medlem i år"
                            1 "i fjor"
                            2 "lenge siden"}
           other-data-1 {0 "A"
                         1 "B"
                         2 "C"
                         3 "D"}]

       (doall (->> @person-data
                   (map-indexed
                    (fn [person-i each]
                      ^{:key (str "person-i " person-i)}

                      [:div.bg-white
                       [:div.flex.h-full.flex-grow.bg-blue-darker
                         ; numbering
                        [:div.w-10.flex.items-center.justify-center.bg-white.text-grey-dark
                         [:p.font-bold.text-2xl (inc person-i)]]

                        [:div.w-2px]

                         ; one data-item
                        [:div.flex-col.flex-grow
                         (interpose [:div.h-2px]
                                    [(render-age-group person-i age-data each bg-color)
                                     (render-gender-group person-i gender-data each bg-color)
                                     (render-membership-group person-i membership-data each)])]
                            ;(render-other-group person-i other-data-1 each)])]

                        [:div.w-2px]

                         ; remove-button
                        (common/delete-button'
                         {:on-click #(rf/dispatch [:remove-person-data person-i])
                          :class (flatten [:w-10 :_rounded-r (if (> (count @person-data) 1)
                                                               [:bg-red :text-white]
                                                               [:bg-blue-lighter :text-blue-light])])})]

                       [:div.h-1]])))))

     [common/button'
      {:on-click #(rf/dispatch [:add-person-to-boat])
       :class [:flex :w-full :justify-center :items-center :h-12 :bg-black :text-white :rounded-b-lg]}
      [:div.w-6.h-6 (icons :plus)]]]))

(defn- content-fake []
  {"200" {:type 1 :kind 1 :material 0 :stability 1 :brand "the 200"}
   "100" {:type 1 :kind 1 :material 0 :stability 1}
   "101" {:type 1 :kind 1 :material 0 :stability 1}
   "102" {:type 1 :kind 1 :material 0 :stability 1}
   "103" {:type 123 :kind 1 :material 0 :stability 1}
   "104" {:type 2 :kind 1 :material 0 :stability 1}
   "105" {:type 2 :kind 2 :material 0 :stability 1}
   "106" {:type 2 :kind 2 :material 0 :stability 1}
   "107" {:type 3 :kind 2 :material 0 :stability 1}
   "108" {:type 3 :kind 1 :material 0 :stability 2 :brand "Valley" :name "Valley Sirona" :dimension {:width "123cm" :length "400cm"} :description "Passer for små personer"}
   "109" {:type 3 :kind 2 :material 0 :stability 2}
   "110" {:type 4 :kind 2 :material 0 :stability 1}
   "111" {:type 4 :kind 2 :material 0 :stability 1}
   "112" {:type 5 :kind 1 :material 0 :stability 1}
   "113" {:type 5 :kind 1 :material 0 :stability 1}
   "114" {:type 5 :kind 1 :material 0 :stability 1}
   "115" {:type 5 :kind 1 :material 0 :stability 1}
   "116" {:type 6 :kind 1 :material 0 :stability 1}
   "117" {:type 6 :kind 1 :material 1 :stability 5 :name "Testname"}
   "118" {:type 6 :kind 1 :name "Testname" :brand "Testbrand" :material 0 :stability 2}
   "119" {:type 6 :kind 1 :material 3 :stability 4 :dimension {:width "100cm"}}
   "123" {:type 6 :kind 1 :material 0 :stability 3 :dimension {:width "123cm" :length "400cm"}}})

(defn padbar []
  (let [input-switch (rf/subscribe [:input-switcher])
        phone-visible (rf/subscribe [:phone-number-toggle])
        phone {:caption     "Telefon-nummer"
               :value       (rf/subscribe [:pad-phone-number])
               :length      8
               :fok         :field/phone-number
               :dispatch-fn [:clicked-on-phone-number]
               :sep-rule    #{2 4}}
        data [{:caption     "Båt-nummer"
               :value       (rf/subscribe [:pad-boat-number])
               :length      3
               :fok         :field/boat-number
               :dispatch-fn [:clicked-on-boat-number]}
              #_{:caption     "Antall båter"
                 :value       (rf/subscribe [:pad-amount-number])
                 :length      2
                 :fok         :field/amount-number
                 :dispatch-fn [:clicked-on-amount-number]}]]

    (fn []
      [:div.flex.flex-col.justify-between.h-full.mt-4._mx-auto.overflow-auto._w-128
       {:style {:min-width "24rem"}}

       (for [e data]
         [text-field e])

       [:div.flex.flex-grow]

       (when @phone-visible
         [:div.mt-4 [text-field phone]])

       [havekey-group]

       [:div.mb-4 [number-input-pad
                   (content-fake)
                   (case @input-switch
                     :field/boat-number frontend.numberpad/enabler-fn
                     :field/phone-number frontend.numberpad/enabler-phone-fn
                     (constantly true))]]])))

(defn- similar-to [id m]
  (let [baseline-type (:type (get m id))]
    (if baseline-type
      (let [filter-fn (fn [e] (= baseline-type (:type (val e))))]
        ;; type indicates a class, boats of same brand/model has the same type
        (dissoc (into {} (filter filter-fn m)) id)))))

(comment
  (get (content-fake) "100")
  (get (content-fake) "108")
  (similar-to "103" (content-fake))
  (similar-to "102" (content-fake))
  (similar-to "100w" (content-fake))
  ())

(defn badge-format [item]
  [:div.flex.justify-center.items-center.border
   {:class [(condp = (:material item)
              0 "bg-blue text-blue-lightest border-blue-light"
              1 "bg-green text-green-lightest border-green-light"
              "bg-black text-white border-grey")]}
   (:stability item)])

(defn- transform [[k item]]
  "converts an MapEntry into a familiar shape to be picked up by the cards"
  {:id        k
   :kind      (nth ["Kano" "Kajakk" "Sup" "Robåt" "Surfski"] (:kind item) (str "Annet " (:kind item)))
   :type      (:type item)
   :name      (:name item)
   :brand     (:brand item)
   :dimension (:dimension item)
   :weight "-120kg"

   ;:badge {:stability (:stability item)
   ;        :material (:material item)
   :description (:description item)
   :stability (:stability item)
   :material  (:material item)})

; clicked on item, what next?

(rf/reg-event-db
 :select-item-something
 (fn [db [_ arg]]
   (println arg)
   db))

(defn badge-2 [item]
  [:div.w-full.h-full.border.rounded-full.flex.items-center.justify-center
   {:class [(condp = (:material item)
              0 "bg-blue text-blue-lightest border-blue-light"
              1 "bg-green text-green-lightest border-green-light"
              "bg-black text-white border-grey")]}
   [:p.-mt-2 (:stability item)]])

(defn card-content [{:keys [left highlight] :or {left true highlight true}}
                    {:keys [id kind brand name dimension description] :as item}]
  [:div.text-black.font-bold.items-center.flex.h-full.bg-white.w-full
   [:div.flex-col.w-full
    [:div.leading-tight
     (let [data [[:div.h-auto.flex.items-center
                  [:div.flex.justify-between.items-center.w-full.mb-2
                   {:class [(if-not left :flex-row-reverse)]}
                   [:p.text-3xl.font-hairline.font-serif id]
                   [:div kind]
                   [:div.h-10.w-10.flex.items-center.text-3xl.font-serif (badge-2 item)]]]
                 [:div (apply str (interpose " / " (keep identity [name brand])))]
                 [:p (apply str (interpose " / " (keep identity [(:width dimension) (:length dimension)])))]
                 [:p "Vekt mellom 50 og 120 kg"]
                 [:p description]]]
       ^{:key id} (interpose [:div.h-1] data))]]])

(defn each-card-right [highlight item {:keys [extra-input] :or {extra-input nil}}]
  (let [{:keys [id] :as transformed-item} (transform item)]
    ^{:key id}
    [:div.flex.mb-4.select-none

     ; add sign
     (common/add-button {:class [:w-12 :rounded-l-lg]
                         :on-click #(rf/dispatch [:select-item-something id])})

     ; card-content
     [:div.rounded-r.w-full.bg-white
      ;{:class [(if highlight "bg-white" "bg-white")]}
      [:div.m-2 (card-content {:highlight highlight
                               :left false} transformed-item)]
      (when extra-input
        [:div.bg-grey-dark.p-2
         extra-input])]]))

;;
;;
;;

(defn card-content' [{:keys [id kind brand name dimension description] :as item}]
  [:div.text-black.font-bold.items-center.flex.h-full.bg-white.w-full
   [:div.flex-col.w-full
    [:div.leading-tight
     (let [data [[:div.h-auto.flex.items-center
                  [:div.flex.justify-between.items-center.w-full.mb-2
                   {:class []}
                   [:p.text-3xl.font-hairline.font-serif id]
                   [:div kind]
                   [:div.h-10.w-10.flex.items-center.text-3xl.font-serif (badge-2 item)]]]
                 [:div (apply str (interpose " / " (keep identity [name brand])))]
                 [:p (apply str (interpose " / " (keep identity [(:width dimension) (:length dimension)])))]
                 [:p "Vekt mellom 50 og 120 kg"]
                 [:p description]]]
       ^{:key id} (interpose [:div.h-1] data))]]])

(defn each-card-left [item]
  (let [{:keys [id] :as transformed-item} (transform item)]
    ^{:key id}
    [:div.flex.flex-col
     [:div.flex.mb-1.select-none

      ; card-content
      [:div.flex.rounded-tl-lg.w-full.bg-white
       ;{:class [(if highlight "bg-white" "bg-white")]}
       [:div.m-2.w-full
        (card-content'  transformed-item)]]

      (common/delete-button'
       {:class [:w-12 :rounded-tr-lg]
        :on-click #(rf/dispatch [:select-item-something id])})]
     [statistics-input-pad {:show-label false}]]))


;;
;;
;;


(declare toggle-button)

(defn left-side []
  "out queue"
  (let [data (content-fake)]
    [:div.flex.flex-col.w-full.justify-between.h-full.overflow-auto
     [:div
      {:class ["hidden sm:flex"]}
      [:div.overflow-auto.w-full
       (each-card-left  (first data))]]

     [:div.flex.flex-grow]

     ;[statistics-input-pad {:show-label false}]
     (let [b :div.w-full.border-4.bg-black.text-white.border-black.items-center.flex.justify-center.select-none.rounded.h-16]
       [:div.flex
        (interpose [:div.w-2]
                   [[common/button'
                     {:class [:rounded :border-4 :border-black :bg-green :text-white]
                      :on-click #(println "test")}
                     [:div.flex.items-center.justify-around.w-full
                      [:div.w-10.h-10 (icons :open-checkmark)]
                      [:div "Ferdig"]]]
                    [common/button'
                     {:class [:rounded :border-4 :border-black :bg-red :text-white]
                      :on-click #(println "test")}
                     [:div.flex.items-center.justify-around.w-full
                      [:div.w-10.h-10 (icons :cross-out)]
                      [:div "Avbryt"]]]])])]))

(defn right-side []
  (let [data (content-fake)
        pad-boat-number (rf/subscribe [:pad-boat-number])
        p @pad-boat-number
        exact-result (get data p)
        similar-result (similar-to p data)]
    [:div.flex-col.m-4
     [:div.w-full
      {:class ["hidden md:flex"]}
      [:div.overflow-auto.w-full
       (if (get data @pad-boat-number)
         [:div
          (each-card-right true
                           (MapEntry. p exact-result nil)
                           {}
                           #_{:extra-input [statistics-input-pad {:padding false :show-label false}]})

          (when-not (empty? similar-result)
            [:<>
             [:p.h-10.text-nrpk-light.text-base.flex.items-end.my-4 (str (count similar-result) " andre lignende båter")]
             (map (fn [e] (each-card-right false e {})) similar-result)])])]]]))

(rf/reg-event-db
 :x ; todo: naming!
 (fn [db [_ arg]]
   (println "x " arg)
   (let [current (:panel-mode db)]
     (if (= current arg)
       (assoc db :panel-mode nil)
       (assoc db :panel-mode arg)))))

(rf/reg-sub
 :x-mode
 (fn [db _]
   (:panel-mode db)))

(defn- toggle-button [current-mode caption state-const]
  (let [b :div.w-full.h-full.border-2.border-black.items-center.flex.justify-center.select-none.rounded]
    [b {:class (flatten [(if (= current-mode state-const) :bg-red :bg-black)])
        :on-click #(rf/dispatch [:x state-const])} caption]))

(defn- header [_]
  (let [current @(rf/subscribe [:x-mode])]
    [:div.bg-grey.text-white.h-16.flex.justify-around.items-center.p-1
     (interpose [:div.w-4]
                [[toggle-button current "Jeg er Nøkkelvakt" :mode-1]
                 [toggle-button current "Jeg skal holde Kurs" :mode-2]
                 [toggle-button current "Jeg er under Opplæring" :mode-3]])]))

(defn render []
			[:<>
			 [:div.h-full.flex.w-full.flex-col
				(ccp :snap/start)
				[:div.flex.h-full.bg-black
				 [:div.w-full.bg-blue-light.h-full.p-4 [left-side]]
				 [padbar]
				 [:div.bg-nrpk.overflow-y-auto.w-full [right-side]]]]])

