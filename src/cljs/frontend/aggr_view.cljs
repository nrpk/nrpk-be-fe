(ns frontend.aggr-view
  (:require [re-frame.core :as rf]
            [core.appearance :refer [ccp cfg-class c-merged->class c-class]]))

(defn- single-pad [idx idy {:keys [action caption content class]}]
  (let [class (merge-with into
                          (cfg-class :push-button)
                          {:class class})]
    [:div.flex.select-none
     (merge class
            {:key      (str idx ":" idy)
             :on-click #(rf/dispatch action)})
     (when caption caption)
     (when content content)]))

(defn button [caption cls & action]
  "warning, this is a data-constructor"
  (merge {:caption caption
          :class   (:class cls)
          :action  action}))

(defn button-pad [key rr]
  [:div.flex.flex-col
   [:div.flex-grow
    [:div.flex.flex-col.flex-grow
     {:key key}
     (let [f (fn each-line [i three]
               {:key (str i "-" key)}
               [:div.flex
                {:key i
                 :class [:w-full :flex-row :h-full :text-white]}
                (map-indexed (partial single-pad i) three)])]
       (map-indexed f rr))]]])

;;
;;
;;
;;

(defn card [class idx [k boat]]
  ^{:key idx}
  [:div.select-none
   {:class class
    :style {:scroll-snap-align              "start"
            "-webkit-scroll-snap-coordinate" "50% 50%"}}
   [:div.h-32.xborder.text-xxs.flex-grow.m-1.shadow.px.p-1.overflow-hidden.justify-between.flex-col.flex
    {:on-click #(rf/dispatch [:click-card k])
     :class    (case (:boat/type boat)
                 0 [:bg-red-lighter]
                 1 [:bg-blue-lighter]
                 2 [:bg-yellow-lighter]
                 3 [:bg-purple-lighter]
                 4 [:bg-green-lighter]
                 [:bg-white :shadow])}
    [:p.font-bold.text-base k]
    [:div.flex-grow.bg-green-lightest.my-2px.p-2px.text-base "type " (:type boat '?)]
    [:div.flex-grow.bg-red-lightest.p-2px.text-base
     (c-merged->class :xy/bottom-right) (interpose ", " (keys boat))]]])

(def card-type-1 (partial card ["bg-grey-lighter w-1/3 xs:w-full xs:bg-white"]))

(defn cards
  ; todo: feature, let the user decide the size of the cells, gradually introduce new elements as you gain more space for each cell.
  ([boat-pool]
   (cards boat-pool card-type-1))
  ([boat-pool card-fn]
   [:div
    [:div.flex.flex-wrap
     (map-indexed card-fn boat-pool)]]))

(defn button-x [caption cls & action]
  "warning, this is a data-constructor"
  (merge {:caption caption
          :class   (:class cls)
          :action  action}))

(defn cards-filter []
  ;todo: filter
  (let [c2 (c-class [:button/toggle] {:class [:bg-green :text-black]})
        table [(button-x "kayakk" c2 [:pad-toggle :kayak])
               (button-x "kano" c2 [:pad-toggle :kano])
               (button-x "sup" c2 [:pad-toggle :sup])
               (button-x "surfski" c2 [:pad-toggle :surski])
               (button-x "robåt" c2 [:pad-toggle :robåt])]]

    [:div.m-1
     {:class [:flex :bg-red_ :justify-between]}
     (interpose [:div {:class [:mx-2px]}]
                (map
                 (fn [e] [:div (c-class [:button/toggle] {}) (:caption e)])
                 table))]))

(comment
  (do

    (defn big-function
      [domain table dispatch-fn subs-fn]
      (letfn [(set-value [v] (dispatch-fn [domain v]))
              (check [value k] (= value k))]
        (let [current-value (if (subs-fn domain :somefield)
                              :a :b)
             ;@(rf/subscribe [value])
              state {:selected [:bg-black :text-white]
                     :normal   [:bg-white :text-black]}]
          (for [[k v] table]
            [:button {:on-click (set-value [domain k])
                      :class    (if (check current-value k) (state :selected) (state :normal))} v]))))

    (let [data {:kayak   "Kayakk"
                :kano    "Kano"
                :sup     "Sup"
                :surfski "Surfski"
                :robåt   "Robåt"}]
      (big-function :component-123/card-filter
                    data
                    (fn [msg] (rf/dispatch msg))
                    (fn [domain v] (rf/subscribe [domain v]))))))

;;
;;
;;
;;


(defn status-header [aggregate]
  (let [count-only (comp count (partial aggr/boat-status-filter @aggregate))
        f (fn [idx [header cls content]]
            ^{:key idx} ;fixme
            [:div
             [:div  header]
             [:div  content]])]
    [:div.bg-nrpk-dark.mb-px.p-2
     [:p.text-red-500 (str "A " (keys @aggregate) (:event/epoch @aggregate))]
     [:p.text-red-500 (str "B " (keys @(rf/subscribe [:db/boats])) (:event/epoch @aggregate))]
     [:div.mb-px.flex.justify-between.h-32
      (map-indexed f
                   [["utlånt" :class/status-button (count-only :boat/busy)]
                    ["ledige" :class/status-button (count-only :boat/available)]
                    ["ikke i bruk" :class/status-button-dimmed (count-only :boat/in-repair)]
                    ["totalt" :class/status-button (count (:boats @aggregate))]])]]))

(defn- more-reasonable-format-for-data [[k {:boat/keys [status name]}]]
  {:id     (-> k)
   :status status
   :name   name})

(defn- format-one-row [{:keys [id status name]}]
  ^{:key id}
  [:div.flex.text-xxs
   [:div.w-10.bg-black.text-white.mb-1.p-1 (str id)]
   [:div.w-32.bg-blue.text-white.mb-1.p-1 (str status)]
   [:div.w-32.bg-green.text-white.mb-1.p-1 (str (count name)  " " name)]])

(defn content []
  [:p "CONTENT"]
  #_(let [aggr (rf/subscribe [:aggregation])]
      [:div.h-screen.bg-white                                 ;.overflow-hidden
       (ccp :snap/start)
       [:div.h-full.overflow-y-scroll
        [status-header aggr]
        [:div.flex.h-full.justify-between
         [:div.flex-grow {:class ["w-1/2"]}
          [cards-filter]
          (cards (sort (:boats @aggr)))]
         [:div.flex.bg-grey-light.flex-col
          {:class ["w-1/4"]}
          (map format-one-row (map more-reasonable-format-for-data (:boats @aggr)))
          (map-indexed (fn [idx e] ^{:key idx} [:p.text-xxs (:error/reason-id e)])
                       (take 10 (:errors @aggr)))]]]]))

(comment
  (format-one-row {:id 1 :status :s :name 'name}))
