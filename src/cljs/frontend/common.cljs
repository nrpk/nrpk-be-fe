(ns frontend.common
  (:require [panel.icons :refer [icons]]))

;;
;;
;;
;;

(def delete-button
  [:div.bg-red.h-12.text-white (icons :cross-out)])

(defn add-button [class]
  [:div.flex.items-center.justify-center.bg-black.text-white._w-8.px-2
   class
   [:div.w-auto.h-auto
    (icons :open-checkmark)]])

(defn delete-button' [class]
  [:div.flex.items-center.justify-center.bg-red-dark.text-white._w-8.px-2
   class
   [:div.w-auto.h-auto
     (icons :cross-out)]])

(def delete-button-config
  {:symbol (icons :cross-out)
   :symbol-size {:width :w-10
                 :height :h-10}
   :default [:bg-red :text-white]
   :disabled [:bg-black :text-black-light]})

(defn button' [class content]
  [:div.flex.items-center.justify-center.select-none.flex-grow class content])

(defn button'' [class content]
  [:div.flex.items-center.justify-center.select-none.flex-grow class content])

(defn base-button [enabler-fn pad-boat-number config caption]
  [:div
   {:class (flatten [:rounded
                     :flex
										 :w-24
                     :select-none
                     :items-center
                     :justify-center
                     :text-5xl
                     (if (enabler-fn caption pad-boat-number)
                       [:text-white :bg-grey-dark]
                       [:text-grey-darkest :opacity-75 :bg-black-light])])
    :on-click #(config caption)}
   caption])
