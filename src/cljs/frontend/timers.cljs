(ns frontend.timers
  (:require [panel.logging :as log]
            [re-frame.core :as rf]
            [frontend.definitions :as definitions]))

(defonce speed (atom definitions/timer-speed))
(defonce timer (atom nil))

(defn exec-with-timer [e-fn]
  (do
    (log/i ">" "create-timer")
    (if @timer
      (do
        (log/i ">>" "clear the existing")
        (js/clearInterval @timer)
        (reset! timer nil)))
    (log/i ">" "setup")
    (let [f #(rf/dispatch [:store/send! (for [_ (range definitions/buffer-size)]
																			 (e-fn))])]
      (reset! timer (js/setInterval f @speed)))))

(defn stop-timer []
  (log/i ">" "stop-timer")
  (if
   @timer
    (js/clearInterval @timer)
    (log/i "tried to stop timer, but none was found")))
