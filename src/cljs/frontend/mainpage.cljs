(ns frontend.mainpage
  (:require [re-frame.core :as rf]
            [arcs.dial-definitions]))

(declare timer)

(defonce timer (atom nil))

(rf/reg-sub :pomodore/length (fn [db] (:pomodore/length db 0)))

(rf/reg-sub :pomodore/tick-value (fn [db] (:tick db 0)))

(rf/reg-sub :pom/timer-running (fn [db] (:pom/timer-running db false)))
