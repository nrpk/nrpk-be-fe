(ns frontend.typography
  (:require [cljs.pprint :refer [pprint]]
            [frontend.entity :as entity :refer [flip Adornment]]))

(def data
  [:div.bg-green.flex.flex-dcol.h-full.flex-grow.items-baseline
	 [:div.flex.justify-end.mx-2.flex-grow.items-baseline.py-4.text-xl
		(interpose [:div.w-2]
							 [[:div.xflex-grow.items-center.py-4.text-xl "sample"]
								[:div.flex.text-xxs "xss"]
								[:div.flex.text-xs "xs"]
								[:div.flex.text-sm "sm"]
								[:div.flex.text-base "base"]
								[:div.flex.text-lg "lg"]
								[:div.flex.text-xl "xl"]
								[:div.flex.text-2xl "2xl"]
								[:div.flex.text-3xl "3xl"]
								[:div.flex.text-4xl "4xl"]
								[:div.flex.text-5xl "5xl"]])]])

(def dummy
  (let [data {:A (entity/constructionFunction) :B (entity/constructionFunction)}]
    [:<>
     [:div "Some description here"]
     [:div.my-2
      [:pre.font-mono.text-white.text-base.mx-2.bg-blue-darker.p-1
       (with-out-str (pprint data))]]]))

