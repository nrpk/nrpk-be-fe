(ns frontend.view
  (:require [clojure.pprint :refer [pprint]]
            [core.aggregation :as aggr :refer [error-messages]]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [core.appearance :refer [cfg-class ccp m-into c-merged->class c-class]]
            [core.language :refer [no]]
            [core.blocks :refer [vb hb textbox nice-box]]
            [frontend.typography]
		;[core.time :refer [next-time]]
						;;[panel.icons]

            ;[frontend.first-page-view :as first-page-view]
            [frontend.select-view :as select-view]
            [core.aggregation :as aggr :refer [computation
                                               add-boat
                                               assign-boat-name
                                               remove-boat-name
                                               set-boat-in-repair
                                               set-boat-out-of-repair
                                               rent-start
                                               rent-end
                                               initial-model]]
						;;[panel.logging :as log]
            [frontend.control-panel :as control-panel]
						;;[frontend.definitions :as definitions]
            [frontend.aggr-view :as aggr-view]
            [arcs.comps :as stuff]
            [arcs.dialxsampledata :as sampledata]
            [panel.data :as data]
            [frontend.q :as q]))

(def proper-vector
	;;todo: missing case ["x"]
  #(if (vector? %)
     %
     (if (map? %)
       ["invisible" %]
       [% {}])))

(defn pp [x]
  (-> x pprint with-out-str))

(rf/reg-event-db
 :click-other-pad
 (fn [db [_ k]] (println (str  k)) db))

(rf/reg-event-db
 :click-card
 (fn [db [_ k]]
   (println k)
   db))

(rf/reg-sub
 :db/boats
 (fn [db _]
   (into {} (get-in db [:data :db/boats]))))

;;
;;
;; Hiccup-products

(defn debounce
  "Returns a function that will call f only after threshold has passed without new calls
  to the function. Calls prep-fn on the args in a sync way, which can be used for things like
  calling .persist on the event object to be able to access the event attributes in f"
  ([threshold f] (debounce threshold f (constantly nil)))
  ([threshold f prep-fn]
   (let [t (atom nil)]
     (fn [& args]
       (when @t (js/clearTimeout @t))
       (apply prep-fn args)
       (reset! t (js/setTimeout #(do
                                   (reset! t nil)
                                   (apply f args))
                                threshold))))))

(defn a-series-of-events [listener-fn]
  [:div.antialiased._overflow-x-scroll.sw-full.border-4
   [:div.flex.overflow-x-scroll.w-full
    {:id    "scroller"
     :style {:scroll-snap-type "x mandatory"}}
    (map-indexed
     (fn [i e]
       ^{:key i}
       [:div.flex-no-shrink
        {:class [:border "w-1/2" :border-red :mx-px]
         :style {:scroll-snap-align "center"}}
        [:div.flex-col.flex.bg-black.text-grey-dark.h-full.p-1.text-xxs.justify-between
         [:div.flex.justify-between.items-center
          [:p (str i " " (:epoch e))]
          [:p (str "2" (:epoch e))]]
         [:div.flex.justify-between.items-center
          [:p (str "3" (:epoch e))]
          [:p (str "ab" (:epoch e))]]]])
     (repeat 51 {:epoch 1}))]

   (when @listener-fn
     (do
       ;(@listener-fn)
       [:p 'Offset @listener-fn]))
   [:p (str listener-fn)]])

(defn safe-component-mounted? [component]
  (try (boolean (r/dom-node component)) (catch js/Object _ false)))

;;
;;
;; main view

(rf/reg-sub
 :aggregation
 (fn [db [_]]
   (:ui/aggregation db)))

(rf/reg-sub
 :aggregation-root
 (fn [db [_]]
   (into {}
         (->> db (map (fn [[k v]]
                        (vector k (first (cond-> nil
                                           (boolean? v) (conj v)
                                           (number? v) (conj v)
                                           (string? v) (conj v)
                                           (map? v) (conj (str (count v) " items"))
                                           (vector? v) [(conj (flatten v) "...")])))))))))

(rf/reg-sub
 :aggregation/errors
 (fn [db [_]]
   (:errors (:ui/aggregation db))))

(rf/reg-event-db
 :store/send!
 [panel.events/insert-db-path]
 (fn [db [_ event]] ;fixme sideffect -> co-fx
   (assoc db :ui/aggregation (reduce aggr/computation (:ui/aggregation db) event))))
