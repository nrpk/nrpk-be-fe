(ns frontend.time2
  (:require
   [cljs-time.core :as t]
   [cljs-time.format :as f]
   [panel.logging :as log]))

#_(defn time-f
    ([n]
     (->> n
          (f/unparse
           (case true ; just a switch to avoid an indirection
             true (f/formatter "mm:ss 'and' SSS")
             false (f/formatter "yyyy/MM/dd hh:mm:ss")))))
    ([n e]
     (->> (t/plus n (t/seconds e))
          (f/unparse
           (case true ; just a switch to avoid an indirection
             true (f/formatter "mm:ss 'and' SSS")
             false (f/formatter "yyyy/MM/dd hh:mm:ss"))))))

(comment
  (time-f {:config {:format "s"}} (t/now) (t/minutes 3)))

(def defaultConfig
  {:config {:format ">> mm:s"}})

(defn- time-f [{:keys [config] :or {config {:format ">> mm:s"}} } dt duration]
  (->> (t/plus dt duration)
       (f/unparse
         (f/formatter (:format config)))))

(defn set-next-time [config dt]
  (let [start-track (atom 0)]
    (fn helper [xxx]
      (let [new-value (swap! start-track inc)]
        (str (time-f config dt (t/seconds new-value)))))))

(comment
  ; note: this code is ignore by the compiler but you can still invoke it to become familiar with how the above functions work
  (let [x (set-next-time {:config {:format "hh:mm"}} (t/now))]
    (println "thing is" (x 'xxx))))

#_(defn set-next-time [start-from-time]
    (let [a (atom 0)]
      (fn []
        (do
          (swap! a inc)
          (str (time-f start-from-time @a) "ms")))))

(defn set-next-time-now
  ([]
   (set-next-time {} (t/now)))
  ([config]
   (set-next-time config (t/now))))

(def now
  (str (f/unparse (f/formatters :rfc822) (t/now))))

(def next-time
  (set-next-time {:format "sM"} (t/now)))
