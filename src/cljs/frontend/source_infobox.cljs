(ns frontend.source-infobox)

(def project-source-infobox
	[:div.h-24.text-black.text-base.font-bold.font-sans
	 (merge-with into
							 (c-merged->class :xy/center :large)
							 {:class [:flex-grow :no-underline]
								:href  "https://gitlab.com/nrpk/herweb"})

	 ;[:a.text-black.text-base.font-bold.font-sans.no-underline "for"]
	 [:img.w-16.mx-2
		{:src "img/nrpk-logo.png"}]

	 [:a.text-black.text-base.font-bold.font-sans.no-underline
		{:href "https://gitlab.com/nrpk/herweb"}
		"https://gitlab.com/nrpk/herweb (kildekode)"]])
