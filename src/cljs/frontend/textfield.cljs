(ns frontend.textfield
  (:require [re-frame.core :as rf]
            [core.appearance :refer [cfg-class ccp m-into c-merged->class]]))

(defn number-display [sep-rule length string]
  (let [underscores (repeat (- length (count (str @string))) "_")
        display-string (concat @string underscores)]
    [:div.tracking-wide.text-4xl.font-mono.font-bold.mx-2
     (cfg-class :cell)
     [:div
      (for [i (range length)]
        [:span (if (contains? sep-rule i)
                 {:class [:pr-2]}
                 {:class [:pr-0]})
         (nth display-string i)])]]))

(defn text-field [{:keys [caption length fok sep-rule  value dispatch-fn] :or {sep-rule #{}}}]
  (let [active-field (rf/subscribe [:input-switcher])]
    (fn []
      (let [input-empty? (zero? (count @value))
            has-focus? (= @active-field fok)
            input-complete? (= length (count @value))
            label-style {:class (flatten [(when (and has-focus? input-complete?)
                                            [:text-nrpk-light :text-green])
                                          (when (and has-focus? (not input-complete?))
                                            [:text-nrpk :text-red-dark])
                                          (when (and (not has-focus?) input-complete?)
                                            [:text-nrpk])])}
            number-style (concat [:border-4 :rounded :text-white :border-solid]
                                 (if has-focus?
                                   (if input-complete?
                                     [:border-green]
                                     (if input-empty?
                                       [:bg-red-darkest :border-red]
                                       [:bg-black-dark :border-red]))
                                   (if input-complete?
                                     [:border-green-darker]
                                     [:bg-black-dark :border-transparent])))]
        [:div.mx-4.mb-4.p-px
         {:class number-style
          :on-click #(rf/dispatch dispatch-fn)}
         [:div.text-xs.p-1
          [:div label-style  caption]
          [number-display sep-rule length value dispatch-fn]]]))))
