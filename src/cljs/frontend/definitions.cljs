(ns frontend.definitions)

(def timer-speed 1250)
(def buffer-size 10)

(def UP 38) ;; goog.events.KeyCodes.UP
(def DOWN 40) ;; goog.events.KeyCodes.DOWN
(def LEFT 37) ;; goog.events.KeyCodes.LEFT
(def RIGHT 39) ;; goog.events.KeyCodes.RIGHT

(def BACKSPACE 8) ;goog.events.KeyCodes.BACKSPACE
(def TAB 9) ;goog.events.KeyCodes.TAB

(def N0 48)

(def W 87)
(def A 65)
(def B 66)
(def D 68)
(def E 69)
(def G 71)

(def ESC 27)

(def K 75)
(def L 76)
(def M 77)
(def N 78)
(def O 79)
(def P 80)
(def R 82)
(def S 83)
(def T 84)

(def F1 112)
(def F2 113)
(def F3 114)
(def F4 115)
