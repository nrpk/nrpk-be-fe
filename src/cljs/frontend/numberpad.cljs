(ns frontend.numberpad
	(:require [re-frame.core :as rf]
						[panel.icons :refer [icons]]
						[core.appearance :refer [cfg-class ccp m-into c-merged->class]]
						[frontend.common :as common]))

(defn button [c cls a]
  (merge {:caption c
          :class   cls
          :action  #(rf/dispatch a)}))

(defn- single-pad [idx idy e]
  (let [class (merge-with into (cfg-class :push-button) {:class (:class e)})]
    [:div.flex.select-none.h-16.text-white (merge
                                            {:key      (str idx ":" idy)
                                             :on-click #(when (:action e) ((:action e)))}
                                            class)
     (when (:caption e)
       (:caption e))
     (when (:content e)
       (:content e))]))

(defn button-pad [idx rr]
  [:div.flex.flex-col
   [:div.flex-grow
    [:div.flex.flex-col.flex-grow
     {:key idx}
     (let [f (fn each-line [idx three]
               {:key idx}
               [:div.flex
                {:key idx
                 :class [:w-full :flex-row :h-full]}
                (map-indexed (partial single-pad idx) three)])]
       (map-indexed f rr))]]])

;;
;;
;;
;;

(defn enabler-fn [serials input pad-boat-number]
  (let [c (count @pad-boat-number)]
    (if (= 3 c)
      false
      (let [digit (into #{} (distinct (map #(nth % c)
                                           (filter #(= (or @pad-boat-number "") (subs % 0 c)) serials))))]
        (contains? digit input)))))

;; input-enabler for phone-input
(defn enabler-phone-fn [serials input pad-phone-number]
			(let [c (count @pad-phone-number)]
					 (if (= 8 c)
						 false
						 true
						 #_(let [digit (into #{} (distinct (map #(nth % c)
																									(filter #(= (or @pad-phone-number "") (subs % 0 c)) serials))))]
									(contains? digit input)))))

;;
;;
;;
;;

(defn number-input-pad-2 []
  (let [r2 (concat [{:class [:invisible]}
                    {:caption "0"
                     :class   (:class (ccp :regular-button))
                     :action  #(rf/dispatch [:clicked-on-number-pad 0])}
                    {:caption (icons :delete)
                     :class   (:class (merge-with into
                                                  (ccp :clear-button)
                                                  {:class [:py-3]}))
                     :action  #(rf/dispatch [:clicked-on-kill-input])}]
                   (map (fn [e] {:caption e
                                 :class   (:class (ccp :regular-button))
                                 :action  #(rf/dispatch [:clicked-on-number-pad e])}) (range 1 10)))
        rr (->> r2
                (partition 3 3 [{:class [:invisible]}
                                {:class [:invisible]}
                                {:class [:invisible]}])
                reverse)]
    [:div.w-auto.p-4.bg-black-lighter.rounded
     [button-pad 0 rr]]))

;; todo rewrite this piece
(defn number-input-pad [data enabler-fn]
  (let [pad-boat-number (rf/subscribe [:pad-boat-number])
        serials (keys data)
        c1 #(rf/dispatch [:clicked-on-number-pad %])
        c2 #(rf/dispatch [:clicked-on-kill-input])
        button (partial common/base-button (partial enabler-fn serials) pad-boat-number c1)]
    [:div.flex.justify-center
     [:div.p-4._bg-blue.w-64.h-96
      [:div.grid.grid-3._bg-green.w-full.h-full.justify-center
       [button "7"]
       [button "8"]
       [button "9"]
       [button "4"]
       [button "5"]
       [button "6"]
       [button "1"]
       [button "2"]
       [button "3"]
       [:div]  
       [button "0"]

       (common/delete-button'
         {:on-click c2
          :class (flatten [:w-full_ :bg-red :text-white :rounded :m-4
                           (if (zero? (count @pad-boat-number))
                             ;[:bg-blue-lighter :text-blue-light]
                             [:opacity-25]
                             [:opacity-1])])})]]]))


