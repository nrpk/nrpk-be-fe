(ns back-httpkit.core
  (:require
   [back-httpkit.handler :as handler]
   [back-httpkit.nrepl :as nrepl]
   [luminus.http-server :as http]
   [luminus-migrations.core :as migrations]
   [back-httpkit.config :refer [env]]
   [clojure.tools.cli]
   [clojure.tools.logging :as log]
   [mount.core :as mount])
  (:gen-class))

(defn index-by [coll key-fn]
  (into {} (map (juxt key-fn identity) coll)))

;; log uncaught exceptions in threads
(Thread/setDefaultUncaughtExceptionHandler
 (reify Thread$UncaughtExceptionHandler
   (uncaughtException [_ thread ex]
     (log/error {:what :uncaught-exception
                 :exception ex
                 :where (str "Uncaught exception on" (.getName thread))}))))

(def cli-options
  [["-p" "--port PORT" "Port number"
    :parse-fn #(Integer/parseInt %)]])

(mount/defstate ^{:on-reload :noop} http-server
  :start
  (http/start
   (-> env
        ; Attach the handler/app and associate it with the :handler
       (assoc  :handler (handler/app))
       (update :io-threads #(or % (* 2 (.availableProcessors (Runtime/getRuntime)))))
       (update :port #(or (-> env :options :port) %))))
  :stop
  (http/stop http-server))

(mount/defstate ^{:on-reload :noop} repl-server
  :start
  ; only starts when the :nrepl-port is set in env
  (when (env :nrepl-port)
    (nrepl/start {:bind (env :nrepl-bind)
                  :port (env :nrepl-port)}))
  :stop
  (when repl-server
    (nrepl/stop repl-server)))

(defn stop-app []
  (doseq [component (:stopped (mount/stop))]
    (log/info component "stopped"))
  (shutdown-agents))

(defn start-app [args]
  (doseq [component (-> args
                        ;; TODO: Why do I need to specify the namespace here when it is
                        ;; explicitly referred to at the top?
                        ;; Cursive/idea is not happy when leaving out the namespace.
                        (clojure.tools.cli/parse-opts cli-options)
                        mount/start-with-args
                        :started)]
    (log/info component "started"))
  (log/info ":database-url -> " (-> env :database-url))
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-app)))

(defn -main [& args]
  (prn "inside main")
  (mount/start #'back-httpkit.config/env)
  (cond
    (nil? (:database-url env))
    (do
      (prn "Database config not found!")
      (log/error "Database configuration not found, "
                 ":database-url env variable must be set before running")
      (System/exit 1))
    (some #{"init"} args)
    (do
      (migrations/init (select-keys env [:database-url :init-script]))
      (System/exit 0))
    (migrations/migration? args)
    (do
      (migrations/migrate args (select-keys env [:database-url]))
      (System/exit 0))
    :else
    (start-app args)))

