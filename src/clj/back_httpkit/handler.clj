(ns back-httpkit.handler
  (:require
   [back-httpkit.middleware :as middleware]
   [back-httpkit.layout :refer [error-page]]
   [back-httpkit.routes.home :refer [home-routes other-routes]]
   [back-httpkit.routes.services :refer [service-routes]]
   [back-httpkit.routes.oauth :refer [oauth-routes]]
   [reitit.swagger-ui :as swagger-ui]
   [reitit.ring :as ring]
   [reitit.spec :as spec]
   [reitit.dev.pretty :as pretty]
   [ring.middleware.content-type :refer [wrap-content-type]]
   [ring.middleware.webjars :refer [wrap-webjars]]
   [back-httpkit.env :refer [defaults]]
   [mount.core :as mount]))

(mount/defstate init-app
  :start ((or (:init defaults) (fn [])))
  :stop  ((or (:stop defaults) (fn []))))

; todo: remove legacy stuff
; (s/def ::role #{:admin :user})
; (s/def ::roles (s/coll-of ::role :into #{}))

(mount/defstate app-routes
  "Starting the web-server with these routes"
  :start
  (ring/ring-handler
   (ring/router
    [(home-routes)
     (other-routes)
     (service-routes)
     (oauth-routes)]
    {:validate  spec/validate
     :exception pretty/exception})
   (ring/routes
    (swagger-ui/create-swagger-ui-handler
     {:path   "/swagger-ui"
      :url    "/api/swagger.json"
      :config {:validator-url nil}})
    (ring/create-resource-handler
     {:path "/"})
    (wrap-content-type
     (wrap-webjars (constantly nil)))
    (ring/create-default-handler
     {:not-found
      (constantly (error-page {:status 404, :title "404 - Page not found"}))
      :method-not-allowed
      (constantly (error-page {:status 405, :title "405 - Not allowed"}))
      :not-acceptable
      (constantly (error-page {:status 406, :title "406 - Not acceptable"}))}))))

;gets in Luminus http/start
(defn app []
  (middleware/wrap-base #'app-routes))
