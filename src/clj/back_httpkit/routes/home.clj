(ns back-httpkit.routes.home
  (:require
   [back-httpkit.layout :as layout]
   [back-httpkit.db.core :as db]
   [org.httpkit.client :as client]
   [clojure.java.io :as io]
   [back-httpkit.middleware :as middleware]
   [ring.util.http-response :as response]
   [clojure.tools.logging :as log]
   [reitit.dev.pretty :as pretty]
   [markdown.core :as md :refer [md-to-html]]
   [markdown.transformers :as mt]
   [hiccup.core :as hiccup :refer [html]]
   [instaparse.core :as insta]
   [arcs.carcs :refer [render]]))

(defn home-page
  [request]
  (layout/render request "home.html"))

(defn website
  [request]
  (layout/render request "index.html"))

;;; transformers ;;;

(defn capitalize [text state]
  (prn "text " text)
  (prn "state " (keys state))
  [(.toUpperCase text) state])

(defn replacement
  [text state]
  ;(prn state)
  [text state])

(defn list-all-files []
  (.listFiles (io/file (io/resource "nrpk-tekst"))))

(defn grab-everything []
  (let [files (for [e (filter #(.isFile %) (list-all-files))]
                (-> (str "nrpk-tekst/" (.getName e))
                    io/resource
                    slurp
                    (md/md-to-html-string :footnotes? true
                                          :replacement-transformers (into [replacement] mt/transformer-vector))))]
    files))

(defn- doc-requests
  [request filename]
  (let [args (get-in request [:path-params :args])
        parsed-html (-> (str "docs/" filename)
                        io/resource
                        slurp
                        (md/md-to-html-string :footnotes? true
                                              :replacement-transformers (into [replacement] mt/transformer-vector)))]
    ;(log/trace args)
    (-> (response/ok
         (selmer.parser/render-file
          "inject.html"
          {:content (html [:div
                           (render "x")
                           [:section.art1 parsed-html]])}))
        (response/header "Content-Type" "text/html; charset=utf-8"))))

(declare insta-parse-process to-html)

(defn other-routes []
  [""
   {:exceptions pretty/exception
    :middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/insta" {:get (fn [_] (->
                            (response/ok
                             (html [:div (to-html "# some markup\n")]))
                            (response/header "Content-Type" "text/html; charset=utf-8")))}]])

(defn home-routes []
  [""
   {:exceptions pretty/exception
    :middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]

   (comment
     {:get (fn [_] (->
                    (response/ok
                     (selmer.parser/render-file
                      "inject.html"
                      {:content (html [:div
                                       (-> (str "docs/css-test.md")
                                           io/resource
                                           slurp
                                           (md/md-to-html-string :footnotes? true
                                                                 :replacement-transformers (into [replacement] mt/transformer-vector)))])}))
                    (response/header "Content-Type" "text/html; charset=utf-8")))})

   ["/test" {:get (fn [_] (->
                           (response/ok
                            (selmer.parser/render-file
                             "inject.html"
                             {:content (html [:section.art1
                                              (-> (str "docs/css-test.md")
                                                  io/resource
                                                  slurp
                                                  (md/md-to-html-string :footnotes? true
                                                                        :replacement-transformers (into [replacement] mt/transformer-vector)))])}))
                           (response/header "Content-Type" "text/html; charset=utf-8")))}]

   ["/graphiql" {:get (fn [request] (layout/render request "graphiql.html"))}]
   ["/docs/{args}" {:get doc-requests}]
   ["/wapp" {:get (fn [_] (->
                           (response/ok
                            (selmer.parser/render-file
                             "wapp.html"
                             {:content "nada"}
                             #_{:content (html [:div
                                                (render "x")
                                                (map (fn [e] [:section.art1 e])
                                                     (grab-everything))
                                                #_(map [:section.art1 "e"])])}))
                           (response/header "Content-Type" "text/html; charset=utf-8")))}]
   ["/everything" {:get (fn [_] (->
                                 (response/ok
                                  (selmer.parser/render-file
                                   "inject.html"
                                   {:content (html [:div
                                                    (render "x")
                                                    (map (fn [e] [:section.art1 e])
                                                         (grab-everything))
                                                    #_(map [:section.art1 "e"])])}))
                                 (response/header "Content-Type" "text/html; charset=utf-8")))}]
   ["/website:id" {:get {:path-params [:id]
                         :handler     (fn [{{:keys [id]} :path-params :as request}]
                                        (doc-requests request (str "nrpk" id ".md")))}}]])

(def parse-md
  (insta/parser
   "<Blocks> = (Paragraph | Header | List | Ordered | Code | Rule)+
   Header = Line Headerline Blankline+
   <Headerline> = h1 | h2
   h1 = '='+
   h2 = '-'+
   List = Listline+ Blankline+
   Listline = Listmarker Whitespace+ Word (Whitespace Word)* EOL
   <Listmarker> = <'+' | '*' | '-'>
   Ordered = Orderedline+ Blankline+
   Orderedline = Orderedmarker Whitespace* Word (Whitespace Word)* EOL
   <Orderedmarker> = <#'[0-9]+\\.'>
   Code = Codeline+ Blankline+
   Codeline = <Space Space Space Space> (Whitespace | Word)* EOL
   Rule = Ruleline Blankline+
   <Ruleline> = <'+'+ | '*'+ | '-'+>
   Paragraph = Line+ Blankline+
   <Blankline> = Whitespace* EOL
   <Line> = Linepre Word (Whitespace Word)* Linepost EOL
   <Linepre> = (Space (Space (Space)? )? )?
   <Linepost> = Space?
   <Whitespace> = #'(\\ | \\t)+'
   <Space> = ' '
   <Word> = #'\\S+'
   <EOL> = <'\\n'>"))

(def span-elems
  [[#"!\[(\S+)\]\((\S+)\)" (fn [[n href]] (html [:img {:src href :alt n}]))]
   [#"\[(\S+)\]\((\S+)\)"  (fn [[n href]] (html [:a {:href href} n]))]
   [#"`(\S+)`"             (fn [s] (html [:code s]))]
   [#"\*\*(\S+)\*\*"       (fn [s] (html [:strong s]))]
   [#"__(\S+)__"           (fn [s] (html [:strong s]))]
   [#"\*(\S+)\*"           (fn [s] (html [:em s]))]
   [#"_(\S+)_"             (fn [s] (html [:em s]))]])

(defn- parse-span [s]
  (let [res (first (filter (complement nil?)
                           (for [[regex func] span-elems]
                             (let [groups (re-matches regex s)]
                               (if groups (func (drop 1 groups)))))))]
    (if (nil? res) s res)))

(defn- output-html [blocks]
  (reduce str
          (for [b blocks]
            (case (first b)
              :List (html [:ul (for [li (drop 1 b)] [:li (apply str (map parse-span (drop 1 li)))])])
              :Ordered (html [:ol (for [li (drop 1 b)] [:li (apply str (map parse-span (drop 1 li)))])])
              :Header (html [(first (last b)) (apply str (map parse-span (take (- (count b) 2) (drop 1 b))))])
              :Code (html [:pre [:code (apply str (interpose "<br />" (for [line (drop 1 b)] (apply str (drop 1 line)))))]])
              :Rule (html [:hr])
              :Paragraph (html [:p (apply str (map parse-span (drop 1 b)))])))))

(def markdown-to-html (comp output-html parse-md))

(defn to-html
  "Parses markup into HTML."
  [markup]
  (html [:pre [:code.text-xl (str [1 2 3])]]))

