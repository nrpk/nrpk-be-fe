(ns back-httpkit.routes.services.graphql
  (:require
   [com.walmartlabs.lacinia.util :refer [attach-resolvers]]
   [com.walmartlabs.lacinia.schema :as schema]
   [com.walmartlabs.lacinia :as lacinia]
   [clojure.data.json :as json]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [ring.util.http-response :refer :all]
   [mount.core :refer [defstate]]
   [clojure.tools.logging :as log]))

(defn get-hero [context args value]
  (let [data  [{:id 1000
                :name "Luke"
                :home_planet "Tatooine"
                :appears_in ["NEWHOPE" "EMPIRE" "JEDI"]}
               {:id 2000
                :name "Lando Calrissian"
                :home_planet "Socorro"
                :appears_in ["EMPIRE" "JEDI"]}]]
    (first data)))

(defstate compiled-schema
  :start
  (-> "graphql/schema.edn"
      io/resource
      slurp
      edn/read-string
      (attach-resolvers {:get-hero get-hero
                         :get-droid (constantly {})})
      schema/compile))

(defn format-params [query]
  (let [parsed (json/read-str query)] ;;-> placeholder - need to ensure query meets graphql syntax
    (str "query { hero(id: \"1000\") { name appears_in }}")))

(defn execute-request [{:keys [query variables] :as payload}]
  ; where would I get a context from?
  (let [context nil]
    (-> (lacinia/execute compiled-schema (str query) variables context))))

(comment
  (defn execute-request [query]
    (let [_query "{ hero {id name}}"
          vars nil
          context nil]
      (if (empty? query)
        (do
          (log/error (str "QUERY was empty, trying std query=<" _query ">"))
          (-> (lacinia/execute compiled-schema _query vars context)
              (json/write-str))))
      (do (log/info (str "query='" query "'"))
          (-> (lacinia/execute compiled-schema query vars context)
              (json/write-str))))))

(comment
  (json/write-str (lacinia/execute compiled-schema "query { hero {id name}}" nil nil)))
