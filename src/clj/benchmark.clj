(ns benchmark
  (:require [diagram.core :as dc]
            [diagram.definititions :as dd]))

;(use 'criterium.core)

;(bench (Thread/sleep 10));)

;(with-progress-reporting (quick-benchmark (+ 1 2 3) {} #_{:verbose true :os true}))

(with-out-str (time (= 1 2 3)))

(defn render []
  (map #(vector :div.bg-gray-800.shadow-6.snap-start [%])
       [(dc/circle-diagram {:key-id "ROOT-2"
                            :config dd/simple2})

        yearwheel/rendered

        (dc/circle-diagram {:key-id "ROOT-0"
                            :config (dc/pomodore' "abcd")})

        (dc/circle-diagram {:key-id "ROOT-1"
                            :config dd/simple})

        (dc/circle-diagram {:config (dd/pomodore 0)})
        (dc/circle-diagram {:config (dd/pomodore 0)})]))

