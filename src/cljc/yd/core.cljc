(ns yd.core
  (:require
    #?@(:cljs [[cljs-time.format :as f]
               [cljs-time.core :as t]
               [cljs-time.coerce :as c]
               [cljs-time.periodic :as p]
               [cljs-time.instant :as i]]
        :clj  [[clj-time.format :as f]
               [clj-time.core :as t]
               [clj-time.coerce :as c]
               [clj-time.periodic :as p]
               [clj-time.instant :as i]])))

(def offset (t/date-time 2019 1 1))

(def month-names ["Januar" "Februar" "Mars" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Desember"])

(def day-names (map str '[Mandag Tirsdag Onsdag Torsdag Fredag Lørdag Søndag]))

(def ^:private yd->date
     (memoize (fn [offset yd] (t/plus offset (t/days yd)))))

(def ^:private date-time->yd
     (memoize (fn [dt]
                "sum all the days of the former months and todays month-day"
                (let [month (t/month dt)]
                  (reduce (fn [days month]
                            (+ days (t/number-of-days-in-the-month 2019 month)))
                          (dec (t/day dt))
                          (range (dec month)))))))

(defn y [n]
  "zero based, this is the public function to use!"
  (yd->date offset n))

(defn year [yd]
  (t/year (y yd)))

(defn month-name [yd]
  (nth month-names (dec (t/month yd))))

(defn short-number-date [yd]
  (f/unparse (f/formatter "dd/MM") (y yd)))

(defn short-month-name [yd]
  (-> (month-name yd)
      (subs 0 3)))

(defn yd->short-month-name [yd]
  (-> (month-name (y yd))
      (subs 0 3)))

(defn days-in-month [year month]
  (t/number-of-days-in-the-month (t/date-time year month)))

(defn month->total-month-days [n]
  (prn n (t/number-of-days-in-the-month (t/year offset) n))
  (if (zero? n)
    0
    (inc (t/number-of-days-in-the-month (t/year offset) n))))

(defn day-name [n]
  (nth day-names (dec (t/day-of-week n))))

(defn week [yd]
  (t/week-number-of-year yd))

(comment
  (t/week-number-of-year (y 1)))

(defn first-week-day?-yd [n]
  (= 1 (t/day-of-week (y n))))

(defn first-weekday-in-month [year month]
  (t/day-of-week (t/date-time year month)))

(defn first-month-day?-yd [n]
  (= 1 (t/day (y n))))

(defn convert->yd [year month day]
  (f/unparse {:format-str "D"} (t/date-time year month day)))

(defn dt->day-number-of-week [dt]
  (t/day-of-week dt))

(defn yd->day-number-of-week [yd]
  (t/day-of-week (y yd)))

(defn yd->week [yd]
  (t/week-number-of-year (y yd)))

(defn yd->short-day-name [yd]
  (-> (nth day-names (dec (yd->day-number-of-week yd)))))

(defn str-date [dt]
  (f/parse {:format-str "yyyy-MM-dd"} dt))

(defn date-str [dt]
  (f/unparse {:format-str "yyyy-MM-dd"} dt))

(defn time-str [dt]
  (f/unparse {:format-str "hh:mm"} dt))

(defn long-time-str [dt]
  (f/unparse {:format-str "hh:mm:ss"} dt))

(defn current-seconds [dt]
  (t/second dt))

(defn current-minutes [dt]
  (t/minute dt))

(defn current-hours [dt]
  (t/hour dt))

(defn str-date-str [s]
  (-> s date-str str-date))

(defn yd->date-str [yd]
  (date-str (y yd)))

(defn yd->date-sans-str [yd]
  (f/unparse {:format-str "dd/MM"} (y yd)))

(defn month-reducer [year]
  (reduce (fn [a e]
            (conj a [(apply + (last a)) e]))
          []
          (map (fn [e] (days-in-month year (inc e))) (range 12))))

(comment
  (do
    (yd->date-str 0)))

(comment
  (do
    (defn convert->yd [year month day]
      (f/unparse {:format-str "MMMM d D"} (t/date-time year month day)))
    (t/day (t/date-time 2019 2 28))
    (convert->yd 2019 2 28)
    (date-time->yd (t/date-time 2019 11 15))
    (date-time->yd (t/date-time 2020 2 300)))
  (dt->day-number-of-week (t/date-time 2019 11 13))

  (assert (= (t/day-of-week (t/date-time 2019 11 13))
             (yd->day-number-of-week (date-time->yd (t/date-time 2019 11 13)))
             (yd->day-number-of-week (date-time->yd (t/date-time 2019 11 13))))))

(comment
  (comment
    (->> (range 3)
         (map (partial yd->date offset))
         (map (juxt t/week-number-of-year
                    t/day
                    t/year
                    t/day-of-week
                    ;t/first-day-of-the-month
                    t/number-of-days-in-the-month
                    #(f/unparse {:format-str "D"} %)
                    short-month-name
                    month-name
                    year))))

  (comment
    (comment
      (month-name (yd 123))
      (week (yd 361))
      (day-name (yd 1))
      (map (juxt short-month-name month-name day-name short-day-name week year) (map yd (range 20))))

    (t/day-of-week (yd 0))
    (t/week-number-of-year (yd 240))
    (t/month (yd 340))
    (year 40)
    (-> (month-name 50)
        (subs 0 3))
    (short-month-name

      190)))
