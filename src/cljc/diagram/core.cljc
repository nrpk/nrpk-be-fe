(ns diagram.core
  (:require
    [clojure.pprint :refer [pprint]]
    #?(:cljs [arcs.spec])
    ;[arcs.events]
    #?(:cljs [cljs.spec.alpha :as s]
       :clj [clojure.spec.alpha :as s])
    #_[nv.yd :as y]))

(defn polar-to-cartesian
  ([{:keys [zero cx cy]} radius angle]
   (let [angle-in-rad (* (- (- angle zero) 90) (/ Math/PI 180))]
     [(+ cx (*  radius (Math/cos angle-in-rad)))
      (+ cy (*  radius (Math/sin angle-in-rad)))])))

(defn pp [label x]
  (println (str label ">:"))
  #_(println (with-out-str (pprint [(str label ">:") x]))))

(def dev-default-context {:cx 1 :cy 1 :zero 0})

(def standard-context {:cx 100 :cy 100})

(comment
  (do
    (draw-tick-range dev-default-context {:color-fn (fn [i] (if (odd? i) :text-green :text-red))
                                          :theta    [0 20]
                                          :r [90 100]
                                          :class :text-white})))

(def default-tick-style {:stroke 'currentColor
                         :stroke-linecap 'butt
                         :stroke-width 1
                         :fill 'none})

(s/def ::filled (s/keys :req-un [::r
                                 ::theta
                                 ::key-id]
                        :opt-un [::style
                                 ::class]))

(s/def ::draw-ticks (s/keys :req-un [;::r
                                     ::key-id
                                     ;::sector-start
                                     ;::sector-end
                                     ::stepsize]
                            :opt-un [::style
                                     ::class]))

(defn- stroked-path->string [context
                             {:keys [theta  r]}]
  (let [f (partial polar-to-cartesian context)
        [theta-end theta-start] theta
        large-arc-flag (if (<= (- theta-end theta-start) 180) 0 1)
        sweep-flag 1

        [r-inner r-outer] r
        r-mid (/ (+ r-inner r-outer) 2)

        [sector-x2 sector-y2] (f r-mid theta-start)
        [sector-x1 sector-y1] (f r-mid theta-end)]

    (str "M" sector-x1 "," sector-y1 " "
         "A" r-mid "," r-mid ","
         "0," large-arc-flag "," sweep-flag
         sector-x2 " " sector-y2)))

(defn- stroked [context
                {:keys [theta key-id r class style]}]
  (let [f (partial polar-to-cartesian context)
        [theta-start theta-end] theta
        large-arc-flag (if (<= (- theta-end theta-start) 180) 0 1)
        sweep-flag 0

        [r-inner r-outer] r
        r-mid (/ (+ r-inner r-outer) 2)

        [sector-x1 sector-y1] (f r-mid theta-end)
        [sector-x2 sector-y2] (f r-mid theta-start)

        s (str "M" sector-x1 "," sector-y1 " "
               "A" r-mid "," r-mid ","
               "0," large-arc-flag "," sweep-flag
               sector-x2 " " sector-y2)]
    [:g {:key   key-id
         :class class}

     [:defs [:path (merge {:id key-id :d s} style)]]
     [:use {"xlink:href" (str "#" key-id)}]]))

;todo: define key for text on a path

(defn- filled [context
               {:as all :keys [key-id r theta style class insets]}]
  (if-not (s/valid? ::filled all)
    (prn "SHITE")
    #_(throw (js/Error. (with-out-str (pprint (map :pred (:cljs.spec.alpha/problems (s/explain-data ::filled all)))))))
    (let [f (partial polar-to-cartesian context)
          [theta-start theta-end] theta
          ;todo: inner <-> outer
          sweep-flag-outer 0
          sweep-flag-inner 1
          large-arc-flag (if (<= (- theta-end theta-start) 180) 0 1)
          [r-i r-o] r
          r-diff (- r-o r-i)
          [r-inner r-outer] [(+ r-i (* (first insets) r-diff))
                             (- r-o (* (last insets) r-diff))]

          [x-end-angle y-end-angle] (f r-outer theta-start)
          [x-start-angle y-start-angle] (f r-outer theta-end)

          [x-end-angle' y-end-angle'] (f r-inner theta-start)
          [x-start-angle' y-start-angle'] (f r-inner theta-end)

          s (str "M" x-start-angle "," y-start-angle " "
                 "A" r-outer "," r-outer ","
                 "0," large-arc-flag "," sweep-flag-outer
                 x-end-angle "," y-end-angle
                 "L" x-end-angle' "," y-end-angle' " "
                 "A" r-inner "," r-inner ","
                 "0," large-arc-flag "," sweep-flag-inner
                 x-start-angle' "," y-start-angle' " Z")]

      [:g {:key   key-id
           :class class}
       #_[:path {:d s :fill 'red :stroke 'green :stroke-width 2}]
       [:path (merge {:id key-id :d s} style)]])))

(defn- draw-tick-range
  [{:as context :keys [theta]}
   {:as all :keys [key-id drop-first r-inner r-outer class stepsize   color-fn  draw?-fn zero]
    :or   {zero 0
           drop-first 0}}]
  (let [[start end] (if (:theta all) (:theta all) theta)
        start (+ start zero) ; offset
        sector-range (- end start)
        polar-func (partial polar-to-cartesian context)
        path-fn (fn [e]
                  [:path {:d      (let [[x1 y1] (polar-func r-inner (+ e start))
                                        [x2 y2] (polar-func r-outer (+ e start))]
                                    (str "M" x1 "," y1
                                         "L" x2 "," y2 " "))
                          :class  (if color-fn
                                    (color-fn e) class)
                          :stroke 'currentColor}])
        r (drop drop-first (range 0 sector-range (if (nil? stepsize) 2 stepsize)))]
    (into [] (concat
               [:g {:key   (str "draw_tick_range-" key-id)
                    :class (if color-fn
                             (color-fn 1) class)}]
               (if draw?-fn
                 (mapv path-fn (filter draw?-fn r))
                 (mapv path-fn r))))))

(defn- draw-ticks [{:as context :keys [r]}
                   {:as all :keys [insets key-id style]}]
  (let [r (if-let [r' (:r all)] r' r)
        [r-inner r-outer] r
        r-diff (- r-outer r-inner)
        [insets-inner insets-outer] insets
        [r-inner-w-insets r-outer-w-insets]  [(+ r-inner (* insets-inner r-diff))
                                              (- r-outer (* insets-outer r-diff))]]
    [:g (merge style {:key (str "draw_ticks-" key-id)})
     (draw-tick-range context (merge all {:r-inner r-inner-w-insets
                                          :r-outer r-outer-w-insets}))]))

(defn- draw-bars-range
  [{:as context :keys [theta]}
   {:as all :keys [key-id drop-first r-inner r-outer class stepsize value-fn color-fn draw?-fn]
    :or   {drop-first 0
           draw?-fn (fn [_] true)}}]
  (let [[start end] (if (:theta all) (:theta all) theta)
        ;draw?-fn (if draw?-fn draw?-fn (fn [_] true))
        sector-range (- end start)
        polar-func (partial polar-to-cartesian context)
        diff (- r-outer r-inner)
        path-fn (fn [yd]
                  (if (draw?-fn (value-fn yd))
                    [:path {:d      (let [[x1 y1] (polar-func r-inner (+ yd start))
                                          [x2 y2] (polar-func (+ r-inner (* diff (/ (value-fn yd) 30))) (+ yd start))]
                                      (str "M" x1 "," y1
                                           "L" x2 "," y2 " "))
                            :class  (if color-fn
                                      (color-fn (value-fn yd) class))
                            :stroke 'currentColor}]))

        r (drop drop-first (range 0 sector-range (if (nil? stepsize) 2 stepsize)))]
    (into [] (concat
               [:g {:key   (str "draw_bars_range/" key-id)
                    :_class (if color-fn
                              (color-fn 1) class)}]
               (mapv path-fn r)))))

(defn- draw-bars
  "a bar-graph aligned on the arc"
  [{:as context :keys [r theta]}
   {:as all :keys [insets key-id style class]}]
  (let [r (if (:r all) (:r all) r)
        [r-inner r-outer] r
        r-diff (- r-outer r-inner)
        [insets-inner insets-outer] insets
        [r-inner-w-insets r-outer-w-insets]  [(+ r-inner (* insets-inner r-diff))
                                              (- r-outer (* insets-outer r-diff))]]
    [:g (merge {:class class} style {:key (str "draw_ticks/" key-id)})
     (draw-bars-range context (merge all {:r-inner r-inner-w-insets
                                          :r-outer r-outer-w-insets}))]))

(defn- draw-texts-range
  [{:as context :keys [theta zero]}
   {:as all :keys [key-id drop-first r-inner r-outer class stepsize style draw-fn]
    :or   {drop-first 0}}]
  (let [theta (if (:theta all) (:theta all) theta)
        [start end] theta
        polar-func (partial polar-to-cartesian context)
        ;_r (drop drop-first (range 0 sector-range (if (nil? stepsize) 2 stepsize)))
        r (:items all)]
    [:g {:key (str "draw-texts-range/" key-id)
         :class class}
     (if draw-fn (map (fn [[k v]]
                        (let [[x y] (polar-func (- r-inner 10) (+ v start))]
                          (draw-fn {:x x :y y :key-id k :deg (- v zero)}))) r))]))

(defn- draw-texts [{:as context :keys [r]}
                   {:as all :keys [insets key-id style]}]
  (let [r (if-let [r' (:r all)] r' r)
        [r-inner r-outer] r
        r-diff (- r-outer r-inner)
        [insets-inner insets-outer] insets
        [r-inner-w-insets r-outer-w-insets] [(+ r-inner (* insets-inner r-diff))
                                             (- r-outer (* insets-outer r-diff))]]
    [:g (merge style {:key (str "draw-texts/" key-id)})
     (draw-texts-range context (merge all {:r-inner r-inner-w-insets
                                           :r-outer r-outer-w-insets}))]))

(defn- draw-widget [{:as context :keys [r cx cy theta zero]}
                    {:as all :keys [insets key-id kind style position]}]
  (let [r (if (all :r) (all :r) r)
        r (if (vector? r) r [r r])
        f (partial polar-to-cartesian context)
        [r-inner r-outer] r
        r-diff (- r-outer r-inner)

        [x y] (f r-inner (+ 0 position))
        [x2 y2] (f r-outer (+ 0 position))]
    (case kind
      :triangle [:g {:key       key-id
                     :transform (str "rotate(" (- position zero) "," x2 "," y2 ")")}
                 (let [s (str "M" x2 "," y2 "l" (Math/abs (/ r-diff 6)) "," r-diff "h -" (Math/abs (/ r-diff 3)) "z")]
                   [:path (merge {:d s} style)])]
      :line [:g {:key key-id}
             [:line (merge {:x1 x :y1 y :x2 x2 :y2 y2 :stroke 'white} style)]]
      :box [:g {:key key-id} [:path (merge {:d (str "M" x "," y ",l1,5 h-2z")} style)]]
      [:g {:key key-id} [:circle (merge {:cx x :cy y :r 3} style)]])))

(defn- bars [context {:as value-bar :keys [style class]}]
  (let [style (if-let [s style] s (:style context))
        theta (if-let [t (:theta value-bar)] t (:theta context))
        r (if (:r value-bar) (:r value-bar) (:r context))
        diff (- (last theta) (first theta))
        theta-start (first theta)
        [start end] [(* diff (get-in value-bar [:value :start]))
                     ;(* diff (first (:range value-bar)))
                     (- diff (* diff (last (:range value-bar))))]
        xyz (* (- 1 (get-in value-bar [:value :end]))
               end)]
    [:g
     {:key (:key-id value-bar)}
     (condp = (:kind value-bar)
       :stroke (stroked context (assoc value-bar
                                       :style style
                                       :class class
                                              :theta (map (partial + theta-start)
                                                          [start
                                                           xyz])
                                              :r (if (:r value-bar) (:r value-bar) (:r context))))
       :fill (filled context (assoc value-bar
                                    :style style
                                    :class class
                                           :theta (map (partial + theta-start)
                                                       [start
                                                        xyz])
                                           :r r)))]))

(defn- bars-with-text [context
                       {:as value-bar :keys [class theta draw-fn key-id bar-h-insets zero]}]
  (let [theta (if-let [t theta] t (:theta context))
        [t-s t-e] (map #(+ % zero) theta)
        r (if (:r value-bar) (:r value-bar) (:r context))
        [r-inner r-outer] r
        r-height (/ (- r-outer r-inner) 2)
        polar-func (partial polar-to-cartesian context)
        bar-width (- t-e t-s)
        t-mid (/ bar-width 2)
        t-left-margin (* bar-width (first bar-h-insets))
        t-right-margin (* bar-width (last bar-h-insets))
        [start end] [t-left-margin
                     (* (- 1 (get-in value-bar [:value :end]))
                        (- bar-width t-right-margin))]

        path' (fn [k]
                (stroked-path->string context (assoc value-bar
                                                     :key-id k
                                                     :style {:fill 'none}
                                                            :theta (map #(- (+ % t-s) t-mid) [start end])
                                                            :r (if (:r value-bar) (:r value-bar) (:r context)))))
        value-bar' (fn [k] (assoc value-bar
                                  :key-id (str k "/helperx")
                                  :theta (map #(- (+ % t-s) t-mid)
                                              [start
                                               end])
                                         :r (if (:r value-bar) (:r value-bar) (:r context))))
        draw-fn' (fn [k path]
                   (map (fn [theta-mid]
                          (let [[x y] (polar-func (+ r-inner r-height) (+ theta-mid))]
                            (draw-fn {:x x :y y :deg theta-mid :key-id k :path path :class class})))
                        [(+ t-s (/ bar-width 2))]))]

    [:g {:key key-id}
     (condp = (:kind value-bar)
       :stroke (stroked context (value-bar' (str key-id "/xxx")))
       :fill (filled context (value-bar' (str key-id "/helperx"))))
     (when draw-fn (draw-fn' (str key-id "/helper-stroke") (path' (str key-id "/helper-strokey"))))]))

(defn draw-arc
  [context
   {:as   all
    :keys [key-id skip value-bars graph-bars widgets r theta ticks texts value-bars-text fixed]
    :or   {key-id "key-id"}}]
  (when (some? r)
    (let [context (merge context {:r r :theta theta})]
      [:<>
       (if-not skip
         (filled context (update all :key-id str "/root-filled")))

       (when (some? value-bars-text)
         (let [f (partial bars-with-text context)]
           (if (vector? value-bars-text)
             (for [[i e] (map-indexed vector value-bars-text)]
               (f (assoc e :key-id (str key-id "/value-bars-text/" i))))
             (f (assoc value-bars-text :key-id (str "value-bars-text"))))))

       (when value-bars
         (for [[i e] (map-indexed vector value-bars)]
           (bars (assoc context :index i) e)))

       (when (some? ticks)
         (let [f (partial draw-ticks context)]
           (if (vector? ticks)
             (for [[i e] (map-indexed vector ticks)]
               (f (assoc e :index 2
                         :key-id (str "ticks/" i))))
             (f (assoc ticks :index 2 :key-id (str "ticks"))))))

       (when (some? graph-bars)
         (if (vector? graph-bars)
           (for [[i e] (map-indexed vector graph-bars)]
             (draw-bars context (assoc e :key-id (str "graph-bars/" i))))
           (draw-bars context (assoc texts :key-id (str "graph-bars")))))

       (when (some? texts)
         (if (vector? texts)
           (for [[i e] (map-indexed vector texts)]
             (draw-texts context (assoc e :key-id (str "texts/" i))))
           (draw-texts context (assoc texts :key-id (str "texts")))))

       (when (some? widgets)
         (if (vector? widgets)
           (for [[i e] (map-indexed vector widgets)]
             (draw-widget context (assoc e :key-id (str "widget/" i))))
           (draw-widget context (assoc widgets :key-id (str "widget")))))

       [:circle (merge context {:r 40 :stroke 'black :fill 'none :stroke-width 2})]

       (when fixed
         [:circle (merge context (:circle fixed))])])))

(defn circle-diagram [{:keys [key-id size draw-fn config zero]
                       :or {size [320 320]
                            zero  0
                            draw-fn draw-arc}}]
  (fn []
    (let [[w h] size]
      [:svg {:key                 key-id
             :stroke-linecap      'round
             :width               "100%"
             :height              "auto"
             :view-box            (str "0 0 " w " " h)
             :preserveAspectRatio "xMidYMid meet"}
       (draw-fn {:cx    (/ w 2)
                 :cy    (/ h 2)
                 :theta [0 359]
                 :r     [90 100]
                 :zero  zero}
                config)])))
