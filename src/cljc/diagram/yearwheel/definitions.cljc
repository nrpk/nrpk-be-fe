(ns diagram.yearwheel.definitions
  (:require
    [yd.core :as y]
    #?@(:cljs [[cljs-time.core :as t]]
        :clj [[clj-time.core :as t]])))

(declare water-temperature)

(def fake-months-reduced
  (map vector (iterate inc 1) [[0 31]
                               [31 28]
                               [59 31]
                               [90 61]
                                  ;[120 31]
                               [151 30]
                               [181 31]
                               [212 31]
                               [243 30]
                               [273 31]
                               [304 30]
                               [334 31]]))

(def real-months-reduced
  (map vector (iterate inc 1)
       (y/month-reducer 2019)))

(def date-catalog
     ;todo: find another key
  "take the below map and convert to a map where the value is the year-day"
  (let [f #(->> %
                (apply t/date-time)
                y/date-time->yd)]
    (reduce-kv (fn [r k v]
                 (conj r {k (f v)})) {}
               {:styremøte           [2019 11 22]
                :styremøte-1         [2020 1 20]
                :styremøte-2         [2019 10 18]
                :årsmøte             [2019 3 19]
                :vår-møte-nv         [2019 4 28]
                :vår-dugnad          [2019 5 4]
                :høst-dugnad         [2019 10 11]
                :season-start        [2019 5 10]
                :normal-season-start [2019 6 20]
                :normal-season-end   [2019 9 10]
                :season-end          [2019 10 26]})))

(comment
  date-catalog
  "
  {:styremøte-1 19,
   :styremøte-2 291,
   :styremøte 325,
   :vår-møte-nv 72,
   :normal-season-end 252,
   :normal-season-start 170,
   :season-end 268,
   :vår-dugnad 124,
   :årsmøte 80,
   :høst-dugnad 284,
   :season-start 130}
  ")

(defn draw-fn
  ([s]
   (draw-fn 0 0 (fn [_] s)))
  ([i dy f]
   (fn [_ _ _ key-id path]
     (let [font-size 2
           y-adjust (/ font-size 2.8)]
       [:g
        [:text {:fontSize font-size
                :dy       (+ dy y-adjust)}
         [:defs [:path {:id key-id :d path :fill 'white :stroke-width 1 :stroke 'white}]]
         [:textPath {:href        (str "#" key-id)
                     :fill        'white
                     :textAnchor  :middle
                     :startOffset :50%}
          [:tspan (f i)]]]]))))

(defn draw-fn'
  [{:keys [type i dy f font-size]
    :or {type :other
         i 0
         dy 0
         font-size 3}}]
  (fn [{:keys [x y deg key-id path class]}]
    (let [rotate-fn (case type
                      :ferris (fn [deg] (- 360 deg))
                      :quarterly-rotated (fn [deg] (cond (< 0 deg 90) 0
                                                         (< 90 deg 180) -180
                                                         (< 180 deg 270) 180
                                                         :else 0))
                      (fn [_] 0))
          y-adjust (/ font-size 2.8)]
      [:g {:class class}
       {:transform (str "rotate(" (rotate-fn deg) "  " x " " y ")")}
       [:text {:fontSize font-size
               :dy       (+ dy y-adjust)}
        [:defs [:path {:id key-id :d path :fill 'red :stroke-width 1 :stroke 'red}]]
        [:textPath {:href        (str "#" key-id)
                    :path path
                    :fill        'currentColor
                    :textAnchor  :middle
                    :startOffset :50%}
         (f deg)]]])))

(def widgets {:widgets [{:key-id   "w1"
                         :r        43
                         :kind     :circle
                         :style    {:stroke-width   1
                                    :stroke-linecap 'butt
                                    :fill           'none
                                    :stroke         'red}
                         :position (* 360 (+ (/ (y/current-hours (t/now)) 12)
                                             (/ (/ (y/current-minutes (t/now)) 60) 12)))}
                        {:key-id   "w1.1"
                         :r        [90 80]
                         :kind     :triangle
                         :style    {:stroke-width   0.25
                                    :stroke-linecap 'butt
                                    :fill           'blue
                                    :stroke         'white}
                         :position (* 360 (+ (/ (y/current-hours (t/now)) 12)
                                             (/ (/ (y/current-minutes (t/now)) 60) 12)))}
                        #_{:key-id   "w2"
                           :r        [10 30]
                           :kind     :line
                           :style    {:stroke-width   3
                                      :stroke-linecap 'round
                                      :fill           'lightgreen
                                      :stroke         'lightgreen}
                           :position (* 360 (/ (y/current-minutes (t/now)) 60))}
                        {:key-id   "w3"
                         :r        [60 45]
                         :kind     :triangle
                         :style    {:stroke-width   0.5
                                    :stroke-linecap 'round
                                    :fill           'red
                                    :stroke         'yellow}
                         :position 15 #_(/ (y/current-seconds (t/now)) 60)}]})

(defn adder [j n]
  (fn [e] (+ (+ (/ (j) 50) n) e)))

(defn reach [start goal turns]
  (take turns (map identity (iterate (adder #(- 5 (rand-int 10)) (/ (- goal start) turns)) start))))

(comment
  (reach 30 -5 20))

;(map #(rand-int 10) (range 10))

(def water-temperature-data
  (mapv vector
        (iterate inc 0)
        (concat
         (take 60 (repeatedly (constantly -1)))
         (reach -1 12 60)
         (reach 12 22 30)
         (reach 22 15 25)
         (reach 15 25 20)
         (reach 25 15 15)
         (reach 15 4 35)
         (reach 4 -1 60)
         (take 60 (repeatedly (constantly -1))))))

(def water-temperatur-avg-7
  (into {} (map (fn [[yd temp] avg-temp-7] {yd {:temp temp :avg-7 avg-temp-7}}) water-temperature-data
                (->> (partition 7 water-temperature-data)
                     (map #(/ (reduce (fn [a [_ e]] (+ a e)) 0 %) 7))
                     (map #(repeat 7 %)) (flatten)))))

(def water-temp-aggr
  {:max (apply max (map second water-temperature-data))
   :min (apply min (map second water-temperature-data))
   :data water-temperatur-avg-7})

(comment
  (do
    (map (fn [a b] {(first a) {:temp (second a) :avg-7 b}}) water-temperature-data
         (->> (partition 7 water-temperature-data)
              (map #(/ (reduce (fn [a [yd e]] (+ a e)) 0 %) 7))
              (map #(repeat 7 %)) (flatten)))

    #_(/ (reduce + (map second (first (partition 7 water-temperature-data)))) 7)))

(def ring-ordering
  {:statistics        {:r [60 70]}
   :all-months        {:r [80 90] :dy -13}
   :periods-sector    {:r [60 84]}
   :tic               {:r [47 50]}
   :water-temperature {:r [90 100]}
   :calendar          {:r [115 140]}
   :weeks             {:r  [75 85]
                       :dy -2.5}})

(def water-temperature {:graph-bars [{:zero         0
                                      :class        [:text-blue-700 :_opacity-50]
                                      :style        {:stroke-width   0.4
                                                     :stroke-linecap :square
                                                     :stroke         'currentColor}
                                      :insets       [0.1 0.2]
                                      :r            (-> ring-ordering :water-temperature :r)
                                      :theta        [0 360]
                                      :normalize-fn (fn [v])
                                      :draw?-fn     (fn [v] (< 0 v))
                                      :color-fn     (fn [v] (cond
                                                              (< v 0) :text-white
                                                              (< 0 v 5) :text-red-400
                                                              (< 5 v 10) :text-red-300
                                                              (< 10 v 15) :text-purple-300
                                                              :else :text-purple-500))
                                      :value-fn     (fn [yd] (-> (:data water-temp-aggr)
                                                                 (get yd)
                                                                 :avg-7))
                                      :stepsize     1}]})

(def weeks {:value-bars-text (mapv (fn [week-number]
                                     {:key-id  (str "week-" week-number)
                                      :r       (-> ring-ordering :weeks :r)
                                      :insets  [0 0]
                                      :value   {:start 0.0 :end 0.01}
                                      :kind    :fill
                                      :draw-fn (draw-fn' {:dy        (get-in ring-ordering [:weeks :dy] -20)
                                                          :font-size 2
                                                          :f         (fn [_] [:tspan.font-bold.text-black {:fill 'white}  week-number])})
                                      :class   [(nth (cycle [:text-gray-600 :text-gray-900]) week-number) :opacity-25]
                                      :style   {:stroke-width 0.5
                                                ;:stroke       'red
                                                :fill         'currentColor}
                                      :theta   [(* week-number 7) (+ (* week-number 7) 7)]})
                                   (range 0 52))})

(def calendar {:texts {:key-id  "texts"
                       :r       (-> ring-ordering :calendar :r)
                       :class   [:text-green-200]
                       :insets  [0.1 0.1]
                       :theta   [0 360]
                       :draw-fn (fn [{:keys [x y deg key-id]}]
                                  (let [font-size 3
                                        y-adjust (/ font-size 2.8)]
                                    [:g {:transform (str "rotate(" (+ 90 deg) "  " x " " y ")")}
                                     [:text {:x           x
                                             :y           y
                                             :dy          (- y-adjust)
                                             :fill        'currentColor
                                             :text-anchor :end
                                             :font-size   font-size}
                                      ;todo: rotation of text on quadrant 2 and 4
                                      (cond
                                        (< 0 deg 180) [:tspan [:tspan.font-bold.text-red-500 (y/yd->date-sans-str (+ deg))] " " key-id]
                                        :else [:tspan key-id " " [:tspan.font-bold.text-yellow-500 (y/yd->date-sans-str (+ deg))]])]]))

                       :items   date-catalog
                       :style   {:stroke-width 1}}})

(def tic {:ticks [{:zero     0
                   :class    [:text-yellow-600]
                   :style    {:stroke-width 0.5}

                   :draw?-fn (fn [yd] (get #{2 3 4 6 7} (y/yd->day-number-of-week yd)))
                   :color-fn (fn [yd] (case (y/yd->day-number-of-week yd)
                                        2 :text-red-500
                                        3 :text-red-500
                                        4 :text-red-500
                                        6 :text-red-500
                                        7 :text-red-500
                                        :text-black))
                   :insets   [0 0]
                   :r        (-> ring-ordering :tic :r)
                   :theta    [(y/date-time->yd (t/date-time 2019 5 10))
                              (y/date-time->yd (t/date-time 2019 10 26))]
                   :stepsize 1}]})

(def statistics
  {:graph-bars [{:zero     0
                 :r        (-> ring-ordering :statistics :r)
                 :class    [:text-yellow-500]
                 :style    {:stroke-linecap :butt
                            :stroke-width   0.5
                            :stroke         'currentColor}
                 :insets   [0 0]
                 :theta    [180 250]
                 :value-fn #(/ (rand-int 20) 1)
                 :stepsize 1}
                {:zero     0
                 :r        (-> ring-ordering :statistics :r)
                 :class    [:text-yellow-700]
                 :style    {:stroke-width   0.5
                            :stroke-linecap :butt
                            :stroke         'currentColor}
                 :insets   [0 0]
                 :theta    [90 180]
                 :value-fn #(/ (rand-int 20) 1)
                 :stepsize 1}
                {:zero     0
                 :r        (map #(- % 10) (reverse (-> ring-ordering :statistics :r)))
                 :class    [:text-pink-400]
                 :style    {:stroke-width   0.5
                            :stroke-linecap :butt}
                 :insets   [0 0]
                 :theta    [90 180]
                 :value-fn #(/ (rand-int 20) 1)
                 :stepsize 1}]})

(def all-months
  {:value-bars-text
   (mapv (fn [[month-number [year-day days-in-month]]]
           {:key-id  (str "months-" days-in-month)
            :r       (-> ring-ordering :all-months :r)
               ;:insets  [0.1 0.1]
            :value   {:start 0.0 :end 0.01}
            :kind    :fill
            :draw-fn (draw-fn' {:font-size 3
                                :dy (get-in ring-ordering [:all-months :dy] 0)
                                :f         (fn [_] [:tspan.font-black (nth y/month-names (dec month-number))])})
            :class   [(nth (cycle [:text-blue-300 :text-blue-300
                                   :text-green-300 :text-green-300 :text-green-300
                                   :text-yellow-300 :text-yellow-300 :text-yellow-300
                                   :text-orange-500 :text-orange-500 :text-orange-500
                                   :text-blue-300]) (dec month-number)) :opacity-50]
            :style   {:stroke-width 0.5
                      :fill         'currentColor}
            :theta   [year-day (+ year-day days-in-month)]})
         (drop-last 0 (drop 0 real-months-reduced)))})

(def b {:value-bars [{:key-id  (str "month-")
                      ;:value   {:start 0.01 :end 0.01}
                      :theta [0 90]
                      :r [50 85]
                      :class [:text-red-500]
                      :style {:stroke-width 0.5
                              :stroke-dasharray "3,1"
                              :stroke       'black
                              :fill         'currentColor}
                      :kind    :fill}
                     {:key-id  (str "month2-")
                      ;:value   {:start 0.01 :end 0.01}
                      :theta [90 180]
                      :r [45 80]
                      :class [:text-red-300]
                      :style {:stroke-width 0.5
                              :stroke-dasharray "5,1"
                              :stroke       'black
                              :fill         'currentColor}
                      :kind    :fill}]})

(def kaldt-i-vannet
  {:value-bars-text [{:key-id  "kaldt-i-vannet"
                      :r       (-> ring-ordering :periods-sector :r)
                      :theta   (mapv date-catalog [:season-start :normal-season-start])
                      :dy 0
                      :class   [:text-red-300 :opacity-75]
                      :value   {:start 0.0 :end 0.01}
                      :style   {:stroke-width 0.5
                                :fill         'currentColor}
                      :kind    :fill
                      :draw-fn (draw-fn 0 0 (fn [_] "Kaldt i vannet"))}]})

(def hele-sesongen
  {:value-bars-text [{:key-id  (str "hele-sesongen")
                      :r       (-> ring-ordering :periods-sector :r)
                      :theta   (mapv date-catalog [:normal-season-start :normal-season-end])
                      :class   [:text-yellow-700 :opacity-75]
                      :value   {:start 0.0 :end 0.01}
                      :style   {:stroke-width 0.5
                                :fill         'currentColor}
                      :kind    :fill
                      :draw-fn (draw-fn 0 0 (fn [_] "Normal åpningstid"))}]})

(def forkorted-tid
  {:value-bars-text [{:key-id  (str "forkorted-tid")
                      :r       (-> ring-ordering :periods-sector :r)
                      :theta   (mapv date-catalog [:normal-season-end :season-end])
                      :class   [:text-blue-500 :opacity-75]
                      :style   {:stroke-width 0.5
                                :fill         'currentColor}
                      :kind    :fill
                         ;:draw-fn (draw-fn month-number 0 #(nth y/month-names (dec %)))
                      :draw-fn (draw-fn' {:font-size 3 :dy 4 :f  (fn [_] "Kortere åpningstid")})}]})

(def yearwheel [(merge-with into
                            {:key-id          "abc"
                             :theta           [330 130]
                             :r               [40 110]
                             :skip            true
                             :class           [:text-blue-800 :opacity-50]
                             :style           {:stroke-width     0.5
                                               :stroke-dasharray "1,3"
                                               :fill             'currentColor
                                               :stroke           'black}}
                            statistics
                            all-months
                            calendar
                            kaldt-i-vannet
                            hele-sesongen
                            forkorted-tid
                            tic
                            weeks
                            widgets
                            water-temperature)])
