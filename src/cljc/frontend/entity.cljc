(ns frontend.entity
  (:require #?(:clj [clojure.pprint :refer [pprint]]
               :cljs [cljs.pprint :refer [pprint]])
            #?(:clj [clojure.core.match]
               :cljs [cljs.core.match :refer [match]])))

(defn flip [f & r]
  (f (last r) (butlast r)))

(defprotocol EntityProtocol
  "this is what it is"
  (invoke [this state] "The invoke method")
  (invoke2 [this state] "Another kind of invoke method"))

(defrecord Adornment [final-function f] EntityProtocol
  (invoke [t st] (-> t
                     (update :state conj st)
                     f))
  (invoke2 [t st] (-> t
                      (update :state conj st)
                      (update :ugh #(-> (conj % {:p "invoke2" :st/st st})))
                      (final-function st))))

(defn ad1 [x]
  (update x :i (fnil inc 600)))

(defn ad2 [e f]
  (update e :y (fnil conj []) ['p f]))

(defn constructionFunction []
  (map->Adornment {:final-function ad2
                   :f              ad1})(comment
                                          (do

                                            (let [m {:a 2}
                                                  f (fn [kw]
                                                      (-> kw
                                                          m
                                                          (or {(keyword kw) 1})))]
                                              (f :a)))))
(comment (do
           (doseq [n (range 1 101)]
             (prn
              (match [(mod n 3) (mod n 5)]
                [0 0] "FizzBuzz"
                [0 _] "Fizz"
                [_ 0] "Buzz"
                :else n)))))

