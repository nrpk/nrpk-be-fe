(ns core.appearance
  #?(:cljs (:require [panel.logging])))

#?(:cljs (def log panel.logging/i))
#?(:clj (def log println))

(defn cfg-class
  ; intent: The intent is to replace this with my composed class in tailwind.js
  ([] (cfg-class :proto))
  ([theme]
   "takes a keyword, returns a class-vector"
   {:class
    (get {:well           [:bg-grey-darkest]
          :light-well     [:bg-grey-lightest]
          :group-header   [:font-bold :text-yellow :py-1 :p-1 :mb-2]
          :toggle-button  [:w-32 :text-xxs :bg-blue-light :text-black]

          :cell           [:select-none
                           :flex-col
                           :flex-grow
                           :h-12
                           :flex
                           :justify-center
                           :items-end
                           :text-xl]

          :button-display [:flex
                           :justify-center
                           :h-12
                           :items-center
                           :text-xl
                           :font-bold
                           :text-black-lighter
                           :bg-black]
          :button         [:text-grey-darkest :bg-grey-darker :text-5xl]

          :push-button    [:h-12
                           :text-base
                           :bg-black
                           :flex-grow
                           :text-black-lighter
                           ;:text-white
                           :font-bold
                           :justify-center
                           :w-full
                           :items-center
                           :opacity-75
                           :rounded-sm
                           :m-2px]

          :placeholder    [:text-xl
                           :flex
                           :flex-col
                           :items-stretch
                           :justify-center
                           :flex-grow
                           :opacity-1
                           :bg-grey-darker
                           :font-base
                           :text-grey-darkest]

          :an-infobox     [:text-xl :bg-grey :text-black-lighter]
          :list-content
          [:overflow-hidden
           :bg-black :text-white :my-1 :py-1 :px-2 :h-6
           :code
           :font-mono
           :text-xxs
           :w-full]
          :larger         [:p-5 :border-white :border :m-1]
          :h-dist         [:flex :bg-orange :justify-between :p-2 :text-black]
          :hero           [:h-64
                           :my-32
                           :text-5xl :text-center :flex :items-center :font-black :bg-white :shadow]
          :hero2          [:h-64
                           :my-32
                           :text-5xl :text-center :flex :items-center :font-black :bg-yellow :shadow]

          :hero3          [:h-64
                           :my-32
                           :text-5xl :text-center :flex :items-center :font-black :bg-grey :shadow]

          :code           [:font-mono :text-xs :leading-none :tracking-base :text-yellow :bg-black-lighter]
          :bluish         [:bg-blue :text-black]
          :reddish        [:bg-red :text-white :font-serif :font-black]
          :pink           [:bg-pink :text-yellow]

          :nice           [:bg-orange :text-blue :mx-4 :border-4 :p-2]
          :inactive       [:bg-grey-darkest :text-black-lighter :font-bold :rounded :m-1 :border-2 :border-grey-darker]
          :ex1            [:bg-orange :text-blue]
          :large          [:py-2 :px-5 :text-5xl :rounded :m-1 :border-2]
          :blancko        [:my-3 :mx-1 :text-black-lighter :bg-grey-darker :border-grey-darkest :opacity-1 :antialiased :border-2 :p-10 :rounded :shadow]}
         theme
         [:bg-white :text-grey-light :p-2 :text-xs] #_[:bg-red-darker :text-red :text-xs])}))

(def styles
  {:flex-grow [:flex :flex-grow]

   :class/control-panel-window [:fixed
                                :pin-b
                                :pin-r
                                :select-none
                                :border-4
                                :border-black
                                :rounded-t
                                :mx-4
                                :w-48
                                :bg-black
                                :z-50
                                :px-1
                                :py-2]
   :button/normal [:flex :items-center :justify-center
                   :h-12
                   :text-base
                   ;"hover:bg-grey-darker "
                   ;:flex-grow
                   ;:p-2
                   :bg-black-lighter
                   ;:justify-center
                   ;:items-center
                   :icon/normal
                   :rounded-sm]

   :button/toggle [:h-12 :flex :items-end :justify-end
                   :p-1

                   ;:text-base
                   :rounded-sm
                   :w-32 :text-xxs :bg-white :text-black
                   :text-xxs
                   :shadow]


   ;fixme naming


   :button.style/normal        [:text-grey-lighter :font-bold :bg-black-lighter]
   :button.style/opens-dialog  [:text-white :font-bold :bg-black-lighter]
   :button.style/danger        [:bg-red-light :text-yellow :font-bold]
   :button.style/disabled      [:bg-black-lighter :text-black :font-bold "hover:bg-black-lighter"]

   :icon/tiny                  [:w-10 :h-6]
   :icon/normal                [:w-12 :text-red :bg-black-lighter]

   :field-header               [:text-xs :whitespace-no-wrap]
   :style/number-header        [:text-yellow-darker :p-2px]
   :field-sub                  [:font-serif :flex :items-center :justify-center :h-full :text-3xl :font-bold]
   :class/large                [:text-5xl :font-bold :font-serif :m-px]
   :class/other                [:bg-yellow :text-red]
   :class/status-button-dimmed [:text-grey :bg-grey-lighter :h-full :flex-grow "w-1/6" :justify-between :flex-col :flex :mr-px :mb-px :p-2]
   :class/status-button        [:text-white :bg-nrpk-light :h-full :flex-grow "w-1/6" :justify-between :flex-col :flex :mr-2px :mb-px :p-2]
   :class/informal             [:text-3xl :font-bold :font-sans :m-px]
   :class/nrpk                 [:bg-nrpk]
   :class/useless              [:text-lg :font-bold :font-sans :m-px]

   :text-nrpk-dark             [:text-xxs :text-black]
   :style/prelimenary          [:bg-yellow]
   :class/easy-to-read         [:bg-black :text-white :p-1]
   :class/no-content-to-read   [:text-black :bg-black-light]
   :xy/top-left                [:flex :items-start :justify-start]

   :xy/bottom-right            [:flex :items-end :justify-end]
   :xy/center                  [:flex :items-center :justify-center :w-full  :h-full]
   :w-wide                     [:w-64]
   :w-full [:w-full :flex-grow]

   :large                      [:text-3xl]
   :bg-nrpk                    [:text-orange-darker :bg-orange :flex :items-center :justify-center :h-32 :class/large :text-xl :font-thin :font-mono :m-px :border_ :rounded-sm]
   :tag                        [:text-white :bg-orange :rounded-sm :_rounded-r-full :px-4 :py-px :my-px]
   :a                          [:text-white :bg-black]
   :hero                       [:text-yellow-light :font-normal :font-serif :italics :text-center]
   :small                      [:text-xxs]
   :code-font                  [:font-mono :font-thing :text-sm]
   :cropped                    [:overflow-hidden :border-white :border-2]
   :confirm-button             [:bg-green :text-black :text-3xl]
   :clear-button               [:bg-red-light :text-red-lighter :text-3xl]
   :regular-button             [:bg-black :text-nrpk :text-4xl :font-bold]})

(declare c-merged->class)

(defn ccp [tag]
  "I don't know what I think about this *"
  ;(if (vector? tag)
    ;(c-merged->class tag)
    (case tag
      :snap/mandatory {:style {:scroll-snap-type "y mandatory"
                               "-webkit-scroll-snap-type" "y mandatory"
                               "-webkit-scroll-snap-destination" "0% 0%"}}
      :snap/proximity    {:style {:scroll-snap-type "y proximity"
                                  "-webkit-scroll-snap-type" "y proximity"
                                  "-webkit-scroll-snap-destination" "0% 0%"}}
      :snap/start {:style {:scroll-snap-align "start"
                           "-webkit-scroll-snap-coordinate" "0% 0%"}}

      (let [s (get styles tag)]
        (if (nil? s)
          (do
            (log "hmm " tag)
            {:class tag})
          {:class s}))))

(comment
  (do
    (println (ccp :xy/bottom-right))))
    ;=> {:class [:flex :items-end :justify-end]}))

(defn m-into [& xs]
  "silly but useful shortcut"
  (apply (partial merge-with into) xs))

(defn c-merged->class [& tags]
  "very useful, find another way to express it"
  {:class (reduce #(concat (:class (ccp %2)) %1) [] tags)})

(defn c-class [class-tags m]
  (let [class-tags (if (vector? class-tags) class-tags [class-tags])]
    (merge-with into (apply c-merged->class class-tags) m)))
