(ns core.super
  (:require [core.blocks :as blocks]))

(defn ipad-frame [content]
  "wrapper for dimensions"
  [:div.overflow-y-scroll.bg-blue-light.h-full {:style {}} content]);

(defn empty-frame [x]
  [:div {:id "emptyframe" :style {:height "0px"}} x])

(defn debug-frame [{:keys [running] :or {running false}} x]
  [:div
   ;[:div.text-white (str "x" running)]
   [:div.bg-black.h-auto.m-0.p-4
    {:style {:right    10
             :top      10
             ;:height :100%
             ;:max-height :98vh
             :bottom   10
             :left     150
             :overflow :scroll
             :position :fixed}
     ;:width :80vw}
     :class [:p-1]}
    [:div._h-full.flex.flex-col.flex-grow_ x]
    [blocks/nice-box x]
    [:br]
    (map (fn [e] ^{:key e} [blocks/nice-box {:content (str "stuff " e) :id e}])
         (range 50))]])