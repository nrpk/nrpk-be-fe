(ns core.language)

(defn no [kw]
  (get {:entry/course "Kurs..."
        :entry/group "Gruppe"
        :entry/alone "Alene"
        :available "Ledig"
        :busy "Opptatt"
        :errors "Feil"
        :events "Hendelser"
        :epoch "epoke"
        true "Sant"
        false "Nope"} kw kw))