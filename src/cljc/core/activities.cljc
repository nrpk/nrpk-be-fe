(ns core.activities
  (:require [core.aggregation :as aggr]
            [clojure.pprint :refer [pprint]]
            ;[panel.pure :refer [pp2]]
            [core.aggregation :refer [computation initial-model]]
            ;[core.time :refer [next-time set-next-time-now]]
            
            [core.aggregation-dispatch :as dispatch]))

#?(:clj
   (defn threw [text & structs])
   :cljs
   (defn threw [text & structs]
     (throw (js/Error
             (str text "\n\n\n"
                  (with-out-str (mapcat pprint structs)))))))

;(declare rent-start' rent-end' reg-event')

(comment
  (defprotocol IPacketType
    "defines all the events you can send to the machine"
    (rent-start- [this m])
    (rent-end- [this m])
    (reg-event- [this m]))

  (defrecord PacketFactory []
    IPacketType
    (rent-start- [_ {:keys [boat-id time stats]}]
      {:func    dispatch/start-renting
       :boat-id boat-id
       :type    :rent-start
       :time    time
       :stats   stats})

    (rent-end- [_ {:keys [boat-id time stats]}]
      {:func    dispatch/end-renting

       :boat-id boat-id
       :type    :rent-end
       :time    time})

    ;; the problem with this is that reg-event- just does too much
    (reg-event- [_ {:keys [time type content]}]
      (println type)
      (case (:action/type type)
        :type/buy {:func        dispatch/register-event
                   :action/type type
                   :time        time
                   :content     content}
        :type/sell {}

        {:func        dispatch/register-event
         :action/type :unknowns
         :time        time
         :content     content})))

  ; Where do we start this? (def activity (->PacketFactory))

  ;;
  ;;
  ;; constructors

  (defn rent-start [m]
    (rent-start- activity m))

  (defn rent-end [m]
    (rent-end- activity m))

  (defn reg-event [factory m]
    (reg-event- factory (assoc m :action/type :type/buy)))

  (defn reg-insert-boat []
    (reg-event- activity (merge {:boat/brand  "Valley Sirona"
                                 :boat/kind   23
                                 :action/type :type/insert
                                 :syn         "TESTING"})))

  (defn reg-buy [t]
    "buy a thing, and register it with an id"
    (reg-event- activity (merge {:time        t
                                 :action/type :type/buy
                                 :syn         "TESTING"})))

  (defn reg-sell [m]
    "sell a thing, and mark it as :boat.status/sold"
    (reg-event- activity {:boat.status :boat.status/sold}))

  (do
    (set-next-time-now)
    (def factory (->PacketFactory))

    ; throw the events at some machine that consume them
    (let [event-list
          [(reg-event factory {:time (next-time)})]]

      (def st (reduce aggr/computation {} event-list)))
    st))


