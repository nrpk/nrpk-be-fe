(ns core.time
  (:require
            [clj-time.core :as t]
            [clj-time.format :as f]
            #_(:cljs [panel.logging :as log])))
;
;(defn time-f
;  ([n]
;   (->> n
;        (f/unparse
;          (case true ; just a switch to avoid an indirection
;            true (f/formatter "mm:ss.SSS")
;            false (f/formatter "yyyy/MM/dd hh:mm:ss")))))
;  ([n e]
;   (->> (t/plus n (t/seconds e))
;        (f/unparse
;          (case true ; just a switch to avoid an indirection
;            true (f/formatter "mm:ss.SSS")
;            false (f/formatter "yyyy/MM/dd hh:mm:ss"))))))
;
;(defn set-next-time [start-from-time]
;  (let [a (atom 0)]
;    (fn []
;      (do
;        (swap! a inc)
;        (time-f start-from-time @a)))))
;
;(defn set-next-time-now []
;  (set-next-time (t/now)))
;
;(def next-time
;  (set-next-time (t/now)))
