(ns core.blocks
  (:require [clojure.pprint :refer [pprint]]
            [core.appearance :refer [cfg-class]]))

(defn p [& xs]
  [:div.w-64.flex.justify-between (interpose [:p " "] (map (fn [e] [:span.xw-64 e]) xs))])

(defn hb [c & xs]
  [:div.flex.flex-row.justify-between.items-center.h-full c xs])

(defn vb
  ([xs]
   (vb {:class [:border-black :border-2 :bg-red :p-10]} xs))
  ([c & xs]
   [:div.flex.flex-col.h-full.flex-grow c xs]))

(defn container
  ([=>html xs]
   [:div (=>html xs)]))

(defn textbox
  ([x]
   (textbox  x {}))
  ([x  c]
   ^{:key x}
   [:div (merge-with into
                     (cfg-class :large)
                     c
                     {:class "w-20 h-20 items-center justify-center flex flex-col"})
    (when (:label c)
      [:div.text-xs.text-grey-darkest.bg-grey-dark.px-1.pt-2 (:label c)])
    [:div x]]))

(defn textbox'
  ([x class caption]
   ^{:key x}
   [:div #_(merge-with into
                       ;(cfg-class :large)
                       class)
                     ;{:class "w-20 h-20 items-center justify-center flex flex-col"})
    [:div class caption]]))

(defn pre [xs]
  [:pre.font-mono.py-2 (with-out-str (pprint xs))])

;(defn pre [xs]
;  [:div [:pre.font-mono.text-xs xs]])

(defn x-container
  ([f xs]
   (x-container {} f xs))
  ([{:keys [s1 s2 s3]} fn->html xs]
   [:div.flex.justify-start.overflow-scroll.h-auto
    (map (fn [r] (fn->html r {:section1 (cfg-class s1)
                              :section2 (cfg-class s2)
                              :section3 (cfg-class s3)}))
         xs)]))

(defn scrollable-container'
  ([f xs]
   [:div.flex.justify-start.overflow-scroll.h-auto
    {:style {:scroll-snap-type "x mandatory"
             "-webkit-scroll-snap-type" "mandatory"
             "-webkit-scroll-snap-destination" "50% 50%"}}
    (map-indexed (fn [idx r] [:div
                              {:style {:scroll-snap-align "start"
                                       "-webkit-scroll-snap-coordinate" "0% 0%"}}
                              (f idx r)])
                 xs)]))

(defn nice-box
  "prototypical box"
  ([]
   (nice-box {:content "empty" :id 0}))
  ([{:keys [id content]}]
   ^{:key id}
   [:div.w-64.h-auto.bg-red-dark.text-white.m-0.p-1.opacity-50
    {:class [:mb-1]}
    [hb {}
     [:div id]
     [:div content]]]))
