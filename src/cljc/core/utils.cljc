(ns core.utils
  (:require [clojure.pprint :refer [pprint]]))

(defn ppr [x]
  (-> x pprint with-out-str))
