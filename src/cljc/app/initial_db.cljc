(ns app.initial-db
  (:require [clojure.pprint :refer [pprint]]
            [clojure.string :refer [capitalize]]
            #?(:clj [clojure.core :refer [subs defprotocol]]
               :cljs [cljs.core :refer [subs defprotocol]])))

(defprotocol IO
  (to-map [s])
  (to-string [s]))

(def base-category-map
  {0 "sup"
   1 "kajakk"
   2 "kano"
   3 "surfski"
   4 "robåt"})

(defrecord Boat [base-category
                 material
                 level2
                 name
                 brand
                 level
                 type
                 dimensions
                 weight-class]

  IO
  (to-map [_] {:name name})
  (to-string [_] (str (capitalize (get base-category-map base-category :nada)) ", " name)))

(def root-map
  {100 {:type 3 :acquired :?}
   116 {:type 15 :acquired :?}
   121 {:type 29 :acquired 2002}
   123 {:type 65 :acquired :?}
   140 {:type 29 :acquired 2004}
   141 {:type 29 :acquired 2004}
   142 {:type 30 :acquired 2004}
   143 {:type 30 :acquired 2005}
   144 {:type 30 :acquired 2005}
   145 {:type 29 :acquired 2005}
   146 {:type 29 :acquired 2005}
   147 {:type 15 :acquired 2005}
   148 {:type 15 :acquired 2005}
   154 {:type 28 :acquired 2005}
   155 {:type 28 :acquired 2005}
   156 {:type 29 :acquired 2005}
   157 {:type 29 :acquired 2005}
   158 {:type 29 :acquired 2005}
   159 {:type 29 :acquired 2005}
   164 {:type 15 :acquired 2006}
   165 {:type 15 :acquired 2006}
   166 {:type 15 :acquired 2006}
   169 {:type 17 :acquired 2006}
   170 {:type 17 :acquired 2006}
   171 {:type 10 :acquired 2006}
   176 {:type 39 :acquired 2006}
   178 {:type 17 :acquired 2007}
   179 {:type 17 :acquired 2007}
   180 {:type 17 :acquired 2007}
   181 {:type 17 :acquired 2007}
   182 {:type 39 :acquired 2007}
   184 {:type 26 :acquired 2007}
   185 {:type 16 :acquired 2007}
   186 {:type 16 :acquired 2007}
   189 {:type 62 :acquired 2007}
   190 {:type 40 :acquired 2007}
   191 {:type 40 :acquired 2007}
   192 {:type 40 :acquired 2007}
   193 {:type 40 :acquired 2007}
   194 {:type 40 :acquired 2007}
   195 {:type 40 :acquired 2008}
   196 {:type 40 :acquired 2008}
   197 {:type 40 :acquired 2008}
   198 {:type 40 :acquired 2008}
   208 {:type 41 :acquired :?}
   222 {:type 37 :acquired 1999}
   224 {:type 41 :acquired 2001}
   233 {:type 45 :acquired 2007}
   234 {:type 45 :acquired 2007}
   235 {:type 43 :acquired 2007}
   236 {:type 43 :acquired 2007}
   237 {:type 44 :acquired 2007}
   240 {:type 47 :acquired 2009}
   241 {:type 47 :acquired 2009}
   243 {:type 47 :acquired 2010}
   244 {:type 47 :acquired 2010}
   411 {:type 0 :acquired 2008}
   412 {:type 4 :acquired 2008}
   413 {:type 4 :acquired 2008}
   414 {:type 18 :acquired 2008}
   415 {:type 18 :acquired 2008}
   418 {:type 39 :acquired 2008}
   419 {:type 16 :acquired 2008}
   420 {:type 16 :acquired 2008}
   421 {:type 20 :acquired 2008}
   424 {:type 1 :acquired 2009}
   426 {:type 35 :acquired 2009}
   427 {:type 12 :acquired 2009}
   428 {:type 12 :acquired 2009}
   433 {:type 6 :acquired 2010}
   434 {:type 1 :acquired 2010}
   435 {:type 35 :acquired 2010}
   438 {:type 24 :acquired 2010}
   439 {:type 24 :acquired 2010}
   440 {:type 19 :acquired 2011}
   443 {:type 32 :acquired 2011}
   444 {:type 32 :acquired 2011}
   445 {:type 34 :acquired 2011}
   446 {:type 34 :acquired 2011}
   447 {:type 6 :acquired 2012}
   448 {:type 35 :acquired 2012}
   449 {:type 38 :acquired 2012}
   451 {:type 27 :acquired 2012}
   452 {:type 13 :acquired 2012}
   453 {:type 16 :acquired 2012}
   454 {:type 61 :acquired 2013}
   455 {:type 9 :acquired 2013}
   456 {:type 11 :acquired 2013}
   457 {:type 31 :acquired 2013}
   458 {:type 36 :acquired 2013}
   460 {:type 5 :acquired 2014}
   461 {:type 7 :acquired 2014}
   462 {:type 22 :acquired 2014}
   463 {:type 33 :acquired 2014}
   464 {:type 13 :acquired 2014}
   465 {:type 14 :acquired 2014}
   466 {:type 8 :acquired 2015}
   467 {:type 8 :acquired 2015}
   468 {:type 8 :acquired 2015}
   469 {:type 8 :acquired 2015}
   470 {:type 3 :acquired 2015}
   471 {:type 60 :acquired 2015}
   486 {:type 23 :acquired :?}
   487 {:type 6 :acquired :?}
   488 {:type 25 :acquired :?}
   489 {:type 2 :acquired :?}
   490 {:type 21 :acquired :?}
   494 {:type 63 :acquired 2018}
   600 {:type 48 :acquired 2011}
   601 {:type 48 :acquired 2011}
   602 {:type 48 :acquired 2011}
   603 {:type 49 :acquired 2011}
   604 {:type 50 :acquired 2012}
   605 {:type 51 :acquired :?}
   606 {:type 51 :acquired :?}
   607 {:type 52 :acquired 2013}
   608 {:type 52 :acquired 2013}
   609 {:type 53 :acquired 2013}
   610 {:type 53 :acquired 2013}
   611 {:type 54 :acquired 2013}
   612 {:type 54 :acquired 2014}
   613 {:type 55 :acquired 2014}
   614 {:type 55 :acquired 2014}
   615 {:type 56 :acquired 2014}
   616 {:type 57 :acquired 2015}
   617 {:type 58 :acquired 2015}
   621 {:type 55 :acquired :?}
   622 {:type 55 :acquired :?}
   245 {:type 64 :acquired 2018}
   246 {:type 66 :acquired 2018}})

(def boat-map
     {0  (Boat. 1 0 0 "Aquanaut HV" "Valley" 0 2 :57|536 :?)
      1  (Boat. 1 0 0 "Aquanaut LV" "Valley" 0 2 :55|521 :60-70)
      2  (Boat. 1 1 1 "Arrow Nuka" "Zegul" 1 2 :32|505 :50-80)
      3  (Boat. 1 0 0 "Arrow Play" "Zegul" 0 2 :53.5|517 :?)
      4  (Boat. 1 0 0 "Avocet" "Valley" 0 6 :56|488 :?)
      5  (Boat. 1 1 1 "Baffin P1" "Boréal Design" 1 3 :63.5|430 :-75)
      6  (Boat. 1 1 1 "Baffin P2" "Boréal Design" 1 4 :56.5|518 :65-100)
      7  (Boat. 1 1 1 "Baffin P3" "Boréal Design" 1 5 :58.5|536 :90-)
      8  (Boat. 1 0 0 "Islander Bolero" "Islander Kayaks" 0 9 :62|445 :?)
      9  (Boat. 1 0 0 "Brittany Riot" "Riot Kayaks" 0 4 :55|483 :?)
      10 (Boat. 1 0 0 "Cape Horn" "Wilderness Systems" 0 6 :56|442 :?)
      11 (Boat. 1 1 5 "Citius 48" "Citius" 7 15 :48|520 :?)
      12 (Boat. 1 1 2 "Citius 51" "Citius" 4 15 :51|520 :40-110)
      13 (Boat. 1 1 1 "Citius 60" "Citius" 1 14 :60|520 :70-120)
      14 (Boat. 1 1 2 "Citius JR" "Citius" 3 15 :44|480 :?)
      15 (Boat. 1 1 1 "Coastline" "Olsen Boats" 1 14 :60|? :?)
      16 (Boat. 1 1 2 "Coastrunner" "Olsen Boats" 2 15 :55|? :?)
      17 (Boat. 1 0 0 "Delta" "Dagger" 0 9 :?|34' :?)
      18 (Boat. 1 0 0 "Dirigo" "Old Town" 0 10 :?|? :?)
      19 (Boat. 1 1 1 "Duett K2" "Struer" 1 0 :?|? :?)
      20 (Boat. 1 1 2 "Elio Bull" "Elio" 4 15 :51|? :?)
      21 (Boat. 3 1 2 "Epic V10" "Epic" 2 13 :48|610 :?)
      22 (Boat. 3 1 1 "Epic V8" "Epic" 1 13 :56|548 :?)
      23 (Boat. 1 0 0 "Epsilon" "Boréal Design" 0 4 :59|518 :?)
      24 (Boat. 1 0 0 "Esperanto K2" "Boréal Design" 0 0 :?|? :90-)
      25 (Boat. 1 0 0 "Etain 17.5" "Valley" 0 4 :54|531 :65-100)
      26 (Boat. 1 0 0 "Hasle Excursion" "Hasle" 0 5 :57|320 :?)
      27 (Boat. 1 0 0 "PRIJON Kodiak PE" "Prijon Kodiak" 0 5 :59|520 :?)
      28 (Boat. 1 1 1 "Lightning" "Pyranha" 2 7 :53|421 :30-55)
      29 (Boat. 1 0 0 "Loon 160T" "Old Town" 0 9 :?|? :?)
      30 (Boat. 1 0 0 "Nantucket" "Old Town" 0 18 :66|450 :?)
      31 (Boat. 1 0 0 "Necky Zoar" "Necky Kayak" 0 5 :63.5|430 :?)
      32 (Boat. 1 1 2 "Nelo 55" "Nelo" 2 15 :55|520 :-100)
      33 (Boat. 3 1 3 "Nelo 560" "Nelo" 5 13 :45|560 :-120)
      34 (Boat. 1 1 1 "Nelo 60" "Nelo" 1 14 :60|520 :?)
      35 (Boat. 1 0 1 "Nordkapp" "Valley" 1 2 :53|548 :?) ;; sjekk materialet
      36 (Boat. 1 0 0 "Prijon HV" "Prijon GmbH" 0 2 :58|520 :?)
      37 (Boat. 1 1 1 "Reynard" "Reynard" 2 12 :? :?)
      38 (Boat. 1 0 0 "Scorpio LV" "P&H Sea Kayaks" 0 3 :56|516 :?)
      39 (Boat. 1 0 0 "Solo" "DAG RTM" 0 9 :70|330 :?)
      40 (Boat. 1 0 0 "Tsunami" "Wilderness Systems" 0 6 :54|355 :?)
      41 (Boat. 2 0 0 "Hasle 530PE" "Hasle" 0 17 :?|? :?)
      42 (Boat. 2 0 0 "Mad River Explorer 16" "Mad River" :? 16 :?|16' :?)
      43 (Boat. 2 1 0 "Adirondack" "We-No-Nah" 0 16 :?|16' :?)
      44 (Boat. 2 0 0 "Wilderness" "We-No-Nah" 0 12 :'27|15'4 :?)
      45 (Boat. 2 1 0 "Ally" "West System" :? 16 :?|17' :?)
      46 (Boat. 2 1 0 "Ally" "West System" :? 16 :?|16.5' :?)
      47 (Boat. 2 0 0 "Mad River Reflection 17 RX" "Mad River" 0 16 :?|17' :?) ;; sjekk materialet
      48 (Boat. 0 1 0 "Atlas AST White" "Starboard" 1 8 :12'0|33 :?)
      49 (Boat. 0 1 0 "Technora" "Starboard" 1 8 :10'5|30 :?)
      50 (Boat. 0 1 0 "Drive-Wood" "Starboard" 1 8 :10'5|30 :?)
      51 (Boat. 0 1 0 "Atlas AST Wood" "Starboard" 1 8 :12'6|? :?)
      52 (Boat. 0 0 0 "Oppblåsbar SUP Starboard" "Starboard" 0 8 :12'6|30 :?)
      53 (Boat. 0 0 0 "Oppblåsbar SUP Starboard" "Starboard" 0 8 :9'|30' :?)
      54 (Boat. 0 1 1 "Starboard Sprint" "Starboard" 4 8 :9'4|30'5 :-120) ;; customized
      55 (Boat. 0 1 0 "Starboard Touring" "Starboard" :? 8 :14'30|? :?)
      56 (Boat. 0 0 0 "Astro Junior" "Starboard" 0 8 :10'6|25 :?)
      57 (Boat. 0 0 0 "Astro Tiki" "Starboard" 0 8 :8'|'28 :-50)
      58 (Boat. 0 0 0 "Astro Whopper" "Starboard" 0 8 :10'|0'35 :-120)
      59 (Boat. 2 1 0 "Sandpiper RX" "We-No-Nah" 1 12 :?|? :?)
      60 (Boat. 1 1 1 "Zegul Greenland T" "Zegul" 1 1 :54|540 :?)
      61 (Boat. 1 1 1 "Seabird Qanik" "Seabird Design" 2 1 :52|546 :?)
      62 (Boat. 1 0 0 "Pungo 120" "Wilderness Systems" 0 9 :12'|0'29 :-147)
      63 (Boat. 1 0 0 "Valley Sirona" "Valley" 0 2 :54.5|483 :40-70)
      64 (Boat. 2 0 1 "Covert 10.5 Solo" "Silverbirch" 3 12 :10.5'|? :?)
      65 (Boat. 1 0 0 "Freedom-Sit on Top" "Perception" 0 11 :?|? :?)
      66 (Boat. 2 0 2 "Firefly DL tandem" "Silverbirch" 0 16 :?|? :?)})

(def description-map
     {0  "Relativt stabil kajakk med senkekjøl. Passer til større personer. Liten cockpitåpning."
      1  "Relativt stabil kajakk med senkekjøl. Passer til personer av middels størrelse. Liten cockpitåpning."
      2  "Smal kajakk med senkekjøl."
      3  "Havkajakk for mellomstore padlere. Kajakken har senkekjøl."
      4  "Stabil kajakk med senkekjøl. Passer til ungdom / små personer. Liten cockpitåpning."
      5  "Havkajakk med senkekjøl. For små personer."
      6  "Havkajakk med senkekjøl. For personer av medium størrelse."
      7  "Havkajakk med senkekjøl. For store personer."
      8  "Stødig rekreasjonskajakk med senkekjøl. Passer også store personer. Stor cockpitåpning med polstrede lårstøtter."
      9  "Stødig havkajakk med senkekjøl for små personer."
      10 "Stødig havkajakk for ungdom/små personer."
      11 "Krevende treningsracer. Bare for erfarne kajakkpadlere."
      12 "Krevende treningsracer. Bare for erfarne kajakkpadlere."
      13 "Lettpadlet og stabil glassfiberkajakk. Anbefales også nybegynnere. Stor cockpitåpning. Passer til store og små personer."
      14 "Treningsracer for barn mellom 7 og 12 år. OBS: De som bruker denne må vite hvordan den tømmes uten at den ødelegges!"
      15 "Lettpadlet og stabil glassfiberkajakk. Anbefales også nybegynnere. Stor cockpitåpning. Passer til store og små personer."
      16 "Krevende og relativt ustabil treningsracer for erfarne padlere. Må IKKE forveksles med den stabile Coastline."
      17 "Spesielt godt egnet for nybegynnere. Særdeles stabil. Stor cockpitåpning. Plass til barn eller hund i tillegg."
      18 "Spesielt godt egnet for nybegynnere. Særdeles stabil. Stor cockpitåpning. Plass til barn på eget sete."
      19 "Egnet for litt erfarne padlere. Båten er lang og må behandles forsiktig. Pass på roret!"
      20 "Krevende treningsracer. Bare for erfarne kajakkpadlere."
      21 "En noe krevende surfski for de som synes V8 er for kjedelig."
      22 "Stødig surfski som svarer godt på rorutslag. Selvlensende."
      23 "Stødig, bred kajakk med ror."
      24 "Stødig kajakk med ror. Har plass til barn, hund eller ekstra bagasje i midtre lasterom."
      25 "Relativt stabil havkajakk. For medium til store personer."
      26 "Relativt stabil kajakk med ror. Samme båt som Expedition, men større og romsligere cockpit."
      27 "Svært stødig kajakk med ror. Passer store personer."
      28 "Egnet for barn 7-13 år som har padlet en god del. Relativt ustødig. Behandles forsiktig. Pass på roret! Dette er IKKE en lekebåt."
      29 "Spesielt godt egnet for nybegynnere. Mer stabil enn kano. Plass til to voksne og barn, hund eller bagasje i tillegg. Setene kan justeres for bedre stabilitet."
      30 "Spesielt godt egnet for nybegynnere. Meget stabil med stor cockpitåpning."
      31 "Svært stødig kajakk med ror. Passer store personer, men lav under knestøtte."
      32 "For de som behersker 60 cm treningskajakker og vil utvikle sine ferdigheter videre. Oppleves mindre stødig enn Coastrunner."
      33 "Svært krevende og rask surfski. For de som har tenkt å ta steget fra treningsracer (Citius 48) til racerkajakk."
      34 "Lettpadlet og stabil kajakk. Stor cockpitåpning. Passer for store og små personer, også nybegynnere."
      35 "Leken (noe ustabil) kajakk med senkekjøl. Passer til personer av middels størrelse. Liten cockpitåpning."
      36 "Stødig kajakk med ror. Passer for store personer."
      37 "Smal, retningsstabil kano i glassfiber. Ikke egnet for uerfarne kanopadlere."
      38 "Stødig kajakk med ror. For små personer."
      39 "Spesielt godt egnet for nybegynnere. Meget stabil. Retningsstabil med senkekjølen nede."
      40 "Egnet for barn 5-14 år med maksimum vekt 50 kg. Stødig og retningsstabil."
      41 "Stødig 3-seters kano."
      43 "Relativt stabil turkano. Må behandles forsiktig. Av– og pålasting skal skje mens kanoen flyter på vannet."
      44 "Lett og relativt stabil turkano for en person. Også egnet for nye solopadlere. Produsert i Royalex."
      47 "Lett og stabil 2-seters familiekano. Produsert i Royalex."
      48 "Meget stabil. Egnet for turpadling, lek og bading."
      49 "Ingen beskrivelse"
      50 "Ingen beskrivelse"
      51 "Ingen beskrivelse"
      52 "Ingen beskrivelse"
      53 "Ingen beskrivelse"
      54 "Ingen beskrivelse"
      55 "Ingen beskrivelse"
      56 "Ingen beskrivelse"
      57 "Ingen beskrivelse"
      58 "Ingen beskrivelse"
      59 "Ingen beskrivelse"
      60 "Glassfiberkajakk med senkekjøl. Trang cockpit, men er høyere under fordekket enn Qanik. Ligger svært lavt i vannet. Knekkspant gjør at den svinger lett når båten kantes. OBS: Det kan være vanskelig å løsne spruttrekk!"
      61 "Glassfiberkajakk med senkekjøl. Passer til små og middels store padlere. Knekkspant gjør at den svinger lett når båten kantes."
      62 "Stabil rekreasjonskajakk utstyrt for fisking. Stangholder foran og bak cockpit. Åren kan festes i klips på siden."
      63 "Havkajakk med senkekjøl. Har relativt liten cockpit og passer derfor best til små personer."
      66 "1/2-seter kano som er morsom og lettpadlet. Kan oppleves noe ustabil med to store personer. Pakk vanntett! Som 1-seter kan du bruke kanoen baklengs – legg 6-10 kilo last i baugen for balanse."})

(def data
     (into {} (map (fn [[k v]] (let [bm (boat-map (:type v))
                                     [w l] (clojure.string/split (name (:dimensions bm)) "|")]
                                 {k {:acquired   (:acquired v)
                                     :material   (:material bm)
                                     :level      (:level2 bm)
                                     :category   (base-category-map (:base-category bm))
                                     :name       (:name bm)
                                     :brand      (:brand bm)
                                     :dimensions {:width w :length l}
                                     :weight     (:weight-class bm)
                                     :description (get description-map (:type v))}}))
                   root-map)))

(comment





  (defn beginning-string
    ([a] a)
    ([string length]
     (when string (subs string 0 (min (fnil length 0) (count string))))))

  (comment
    (do
      (defn procedure [m]
        (letfn [(f [coll [k {:keys [type acquired]}]]
                  (let [desc (description-map type)
                        boat (boat-map type)
                        category (:base-category boat)
                        new-record {:category (base-category-map category)
                                    :brand    (:brand boat)}
                        old-record {:desc          (beginning-string desc)
                                    :boat/name     (str k)
                                    :type          type
                                    :acquired/year acquired}]
                    (assoc coll k (merge old-record new-record))))]
          (reduce f {} m)))

      (def new (take 5 (repeatedly #(rand-nth (vec (procedure root-map))))))

      (let [f (comp #(select-keys % [:boat/name
                                     :acquired/year
                                     :name
                                     :brand
                                     :type
                                     :desc]) val)]
        (take-last 5 (map f new))))))

  ;(aggr/add-boat 1)


