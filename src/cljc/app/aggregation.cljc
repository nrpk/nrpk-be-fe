(ns app.aggregation
  (:require [clojure.pprint :refer [pprint]]
            [frontend.time2 :refer [next-time set-next-time-now]]))

(defn status-available? [st path]
  "returns the availability-status of an item"
  (= :boat/available (get-in st (flatten [path :boat/status]))))

(defn- error-path []
  [:errors])

(defn error-messages [n]
  (get {1 "Error: Cannot rent item when it is not available."
        2 "Error: Cannot dismiss an item not in rent."
        3 "Non-existing boat"
        4 "Wat?"
        5 "Attempt to add boat with existing :boat/id"}
       n (str "unknown error " n)))

(defn boat-status-filter [st search]
  (filter (fn [[_ v]] (= search (get-in v [:boat/status]))) (:boats st)))

(def initial-model
  {:event/epoch 0
   :errors []
   :events []
   :boats {}})

;;
;;
;; helpers for the reducer

(defn- add-time-stamp [packet]
  (conj packet {:event/time ""}))

(defn- add-to-errors [st error]
  (update-in st (error-path) conj error))

(defn register-event [st {:keys [epoch event] :as all}]
  (update-in st [:events]
             conj {:event/epoch epoch :event event}))

;; helpers for create-event

(defn- get-log [st boat-id]
  (get-in st [:boats boat-id :log] []))

(defn- boats-path [id]
  [:boats id])

(defn- stats-path [id]
  [:boats id :stats])

;;
;;
;; Create a proper packet for the event. An event
;; is some fields plus a packet contained in a vector.

(defmulti create-event
  "These functions are called from the reducer to mutate
  the system-state in the order of the epochs. They
  are attached to each event in their respective map.

  What about the success/failure scheme, when do I need this?
  Can the event just pass and end up in an error-list as long
  as the :event/epoch was increased?

  Every interaction that creates events also mutates
  the epoch, even the failed events like user-errors
  etc – will mutate and increase the epoch."
  (fn [command st kv] command))

(defmethod create-event :boat/add [command
                                   {:keys [boats] :as st}
                                   {:boat/keys [id] :as kv}]
  #_(println "command >> " command (count boats))
  ; make sure this id does not exist
  (if (not (contains? (set (map key boats)) id))
    (assoc-in st (boats-path id) kv)
    (update-in st [:errors]
               conj (assoc kv
                           :error/reason-id (error-messages 5)
                           :attempt command
                           :boat/id id))))

(defmethod create-event :boat/rent-start [command st {:boat/keys [id]
                                                      :event/keys [epoch time] :as kv}]
  (let [path (boats-path id)
        available? (status-available? st path)
        existing? (get-in st path)]
    (if existing?
      (if available?
        (update-in st path
                   merge {:boat/status :boat/busy
                          :event/epoch epoch
                          :event/time  time
                          :boat/stats  (conj (get-in st (stats-path id) []) {:stuff 'abc})
                          :boat/log    (conj (get-log st id) kv)})
        (update-in st [:errors] conj (assoc kv :error/reason-id 1
                                            :boat/id id
                                            :attempt/kv kv
                                            :attempt/command command
                                            :event/epoch epoch
                                            :boat/status :boat/not-available)))
      (update-in st [:errors] conj (assoc kv
                                          :error/reason-id 3
                                          :boat/id id
                                          :attempt/kv kv
                                          :attempt/command command)))))

(declare rent-start computation)

(comment
  (reduce computation initial-model (rent-start 1 (next-time)))
  #_(create-event
     (rent-start 1 (next-time))))

(defmethod create-event :boat/rent-end [command st {:boat/keys [id] :keys [epoch time] :as kv}]
  (let [path (boats-path id)
        ; todo: this has no effect yet
        packet (select-keys kv [:epoch :type :time :boat-id])
        busy? (= :boat/busy (get-in st (flatten [path :boat/status])))]
    ; to end renting, the item must be rented
    (if busy?
      (update-in st path merge
                 {:boat/status :boat/available
                  ;:event/epoch epoch
                  :time/since  time
                  :boat/stats  (conj (get-in st (stats-path id) [])
                                     {:stuff-done 'done})
                  :boat/log    (conj (get-log st id) packet)})
      (update-in st [:errors]
                 conj (assoc kv
                             :error/reason-id (error-messages 2) ; (add-error st id :not-found)
                             :attempt command
                             :boat/id id)))))

(defmethod create-event :boat/make-active [command st {:boat/keys [id name-id] :event/keys [epoch] :as kv}]
  (assert (not (nil? name)) (str "should have a name field in: " kv))
  (if (get-in st (boats-path id))
    (update-in st (boats-path id) assoc
               :boat/status :boat/available ; (make-boat-available st id)
               :boat/name-id name-id
               :event/epoch epoch)
    (update-in st [:errors]
               conj (assoc kv
                           :error/reason-id (error-messages 3)
                           :event/epoch epoch
                           :attempt command
                           :boat/id id))))

(defmethod create-event :boat/make-inactive [command st {:boat/keys [id] :event/keys [epoch] :as kv}]
  (if (get-in st (boats-path id))
    (update-in st (boats-path id) assoc
               :boat/status :boat/unavailable ; (make-boat-available st id)
               :event/epoch epoch)
    (update-in st [:errors]
               conj (assoc kv
                           :error/reason-id (error-messages 3)
                           :event/epoch epoch
                           :attempt command
                           :boat/id id))))

(defmethod create-event :boat/set-in-repair [command st {:boat/keys [id] :event/keys [time epoch] :as kv}]
  (if (get-in st (boats-path id))
    (update-in st (boats-path id) assoc
               :event/epoch epoch
               :event/time time
               :boat/status :boat/in-repair)
    (update-in st [:errors]
               conj (assoc kv
                           :error/reason-id (error-messages 3) ; (add-error st id :not-found)
                           :event/epoch epoch
                           :attempt command
                           :boat/id id))))

(defmethod create-event :boat/set-out-of-repair  [command st {:boat/keys [id] :event/keys [time epoch] :as kv}]
  (if (get-in st (boats-path id))
    (update-in st (boats-path id) assoc
               :event/epoch epoch
               :event/time time
               :boat/status :boat/available)
    (update-in st [:errors]
               conj (assoc kv
                           :error/reason-id (error-messages 3) ; (add-error st id :not-found)
                           :event/epoch epoch
                           :attempt command
                           :boat/id id))))

(defmethod create-event :nothing/happens  [command st kv]
  (println "caught unknown command " command kv)
  st)

(defmethod create-event :default  [command st kv]
  ;(log/e "caught unknown command " command kv)
  (update-in st [:errors]
             conj (assoc kv :attempt command :system.state/before (dissoc st :errors))))



;;
;;
;;
;;

#?(:cljs (defn assert' [_ _])
   :clj (defn assert' [expr msg]
          (when-not expr
            (print "Thrown")
            (throw (Exception. msg)))))

(defn computation [st [command kv :as event]]
  (assert' (and (vector? event) (= 2 (count event)))
           (str "This is not a vector: " event))
  (let [epoch (:event/epoch st)]
    (-> (create-event command st (assoc kv :event/epoch epoch))
        (register-event {:epoch epoch :event event})
        (update :event/epoch inc))))

(comment (do
           (computation initial-model (nothing-happens))

   ;(computation initial-model (nothing-happens)))
           (ifn? create-event)
           (create-event :boat/rent-start initial-model {:args 123})
           ()))

#_(defn computation'
    "This is the reducer function taking the current-state and a command-with-arguments-tuple."
    [st [command kv :as event]]
    (assert' (and (vector? event) (= 2 (count event))) (str "This is not a vector: " event))
    (when true
      (println 'command  command 'kv kv 'st st))
    (update (create-event command st (assoc kv :event/epoch (:event/epoch st)))
            :event/epoch inc))

#_(do
    (reduce computation {} [(add-boat 1010 2)]))


;(reduce computation initial-model (map (fn [e] (add-boat e)) (range 10)))


(comment
  (computation initial-model (add-boat 2))
  (let [event [:a {}]]
    (assert' (and (vector? event) (= 2 (count event))) (str "This is not a vector: " event)))

  (let [command (nothing-happens 1 2)
        st initial-model
        kv {}]
    (create-event command st (assoc kv :event/epoch (:event/epoch

                                                     st)))))
