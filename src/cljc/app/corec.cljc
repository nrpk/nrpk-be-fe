(ns app.corec)

(comment
	(def xf (comp (filter odd?)
								(map inc)
								(take 13)))

	(transduce xf + 21 (range 10))

	(into [] xf (range 130))

	(eduction xf (range 130))

	(into [] (comp (map inc) (take 13)) (range 100),

				(transduce
					; the instruction
					(comp (map str)
								(map (fn [e] [:div {:class (inc e)} (str e " & " e)])))
					; method
					conj

					;collection
					(range 10))

				(set (into () cat [(range 4) (range 5)]))

				(take 6 (dedupe (into () cat [(range 4) (range 5)])))

				(->> (transduce (comp (map str) (map (fn [e] [:div {:class (inc e)} (str e " & " e)]))) conj (range 10))
						 (comp (map vector))
						 (take 4)

						 (map #(vector :embed %)))


				(eduction (comp (map (fn [e] [:div e]))) (range 10))
				(into [:div] (comp (map (fn [e] [:p e]))) (range 10))
				[:div [for [e (range 10)] '[:p e]]]

				(map (apply identity (comp (map (fn [e] [:p e])))) (range 10))

				(sequence xf (range 30))

				(= 1 2)

				(reduce + 0 (eduction (comp (map (fn [e] [:div e]))) (range 10)))
				(eduction (comp (map (fn [e] [:div e]))) (range 10))
				(sequence [])

				(transduce (comp cat cat (map inc)) + [[[1] [2 3] [[4]]]])

				(do
					(ns scratch-6.core
						(:require [clojure.pprint :refer [pprint]]
											[cljs-time.core :as t]
											[cljs-time.coerce :as tc]
											[cljs-time.format :as f]))

					(defn day-of-year-0 [d]
						(js/parseInt (f/unparse (f/formatter "D") d)))

					(defn day-of-year [d]
						"alternative to `day-of-year-0`, since the literal 'D' appearantly has no effect"
						(+ (* 7
									(js/parseInt (f/unparse (f/formatter "w") d)))
							 (js/parseInt (f/unparse (f/formatter "e") d))))

					(defn day-of-week-from-day-of-year [d])
					; season
					(def season {:first-day (tc/to-long (t/date-time 2019 5 21))
											 :last-day  (tc/to-long (t/date-time 2019 10 14))})

					(defn season-range [s]
						(range (:first-day s) (:last-day s)))

					(def only-active-days
						(filter (fn [e] (contains? #{2 3 4 6 7} (t/day-of-week (tc/from-long e))))))

					(pprint (eduction (comp only-active-days) (season-range season)))
					())

				(= 1 2))

	(do
		(defn my-thing "does nothing useful" [x]
			(map (fn [e] (vector :a (str e) x)) '(this is a test)))

		(into [] cat [[1 2 3]
									[1 2 3]])

		(my-thing 4))
	)
