(ns app.packet.builder)

(defn nothing-happens [& _]
  [:nothing/happens {}]) ;note: <- forgot {}, hard to find!

;;; Packet-builders ;;;

(defn add-boat
  "packet builder for boats, helper that returns the proper map of adding a boat"
  ([id]
   (add-boat id 2)) ;todo: change hardcoded value
  ([id type]
   [:boat/add
    {:boat/id id
     :boat/type type}]))

(defn add-boat'
  ([id kv]
   [:boat/add
    (merge
     {:boat/id id}
     kv)]))

(defn assign-boat-name [id n]
  [:boat/make-active
   {:boat/id   id
    :boat/name-id n}])

(defn remove-boat-name [id]
  [:boat/make-inactive
   {:boat/id id}])

(defn set-boat-in-repair [id]
  [:boat/set-in-repair
   {:boat/id id}])

(defn set-boat-out-of-repair [id]
  [:boat/set-out-of-repair
   {:boat/id id}])

(defn rent-start [id t]
  [:boat/rent-start
   {:boat/id    id
    :event/time t}])

(defn rent-end [id t]
  [:boat/rent-end
   {:boat/id    id
    :event/time t}])
