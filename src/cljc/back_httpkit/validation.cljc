(ns back-httpkit.validation
  (:require [struct.core :as st]
            [clojure.test :refer [deftest is testing use-fixtures]]
            [clojure.pprint :refer [pprint cl-format]]))

(comment
  (defn my-fix [f]
    (println "before " f)
    (f)
    (println "after"))

  (use-fixtures :once my-fix)

  (use-fixtures :each my-fix)

  (deftest this
    (testing "taht")
    (is (= 1 1)))

  (clojure.test/run-tests))

(comment
  (do
		(declare present)

		; if order of validation matters, use a vector instead of a map
		(def +scheme+
			{:name [st/required st/string]
			 :year [st/required st/number]})

		; how can I use this? These are some contrived examples:
		(def run
			(-> (let [r1 (-> {} (st/validate +scheme+))
								input-2 {:name "Pete" :year 1971}
								r2 (-> input-2 (st/validate +scheme+)
											 ; passes
											 ((juxt #(if-not first % :ok) second)))]
						{:test-1 ["will fail and require all fields" r1]
						 :test-2 ["a more involved use" r2]})
					present))
		run

		; printer for the above
		(defn present [xs]
			(select-keys xs [:test-2 :test-1])
			(for [e (keys xs)]
				(let [[title content] (e xs)]
					[:div
					 [:h1 title]
					 [:p content]
					 [:pre (cl-format nil "~c[and]~:c~%~c" 1 12 [1 2 3])]])))))
