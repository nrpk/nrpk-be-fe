const postcssNesting = require('postcss-nested');

const cssnano = require('cssnano');
const postcss_color_mod = require('postcss-color-mod-function');
const postcss_preset_env = require('postcss-preset-env');
const postcss_Import = require('postcss-import');
const postcss_Url = require('postcss-url');
const purgecss = require('@fullhuman/postcss-purgecss');

const production = process.env.NODE_ENV;
console.log("production? ", production == 'production')

module.exports = {
   plugins: [
      postcssNesting(), postcss_Import(), postcss_Url(), require('tailwindcss')('./tailwind.config.js'),
      postcss_preset_env({
         stage: 0,
         autoprefixer: {
            grid: true
         }
      }),
      postcss_color_mod(),
      cssnano({autoprefixer: false, preset: ['default']}),
      // todo: wont work in production because purge will take everything
      !production && purgecss({
         content: [
            '../**/*.html', '../**/*.svelte', '../**/*.cljs'
         ],
         defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
      })
   ]
};
